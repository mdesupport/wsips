﻿USE [MdeCommunication]
GO
CREATE USER [MdeWSIPS] FOR LOGIN [MdeWSIPS] WITH DEFAULT_SCHEMA=[dbo]
GO
EXEC sp_addrolemember 'db_owner', 'MdeWSIPS'
GO