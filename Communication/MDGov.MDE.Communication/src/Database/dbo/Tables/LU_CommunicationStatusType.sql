﻿CREATE TABLE [dbo].[LU_CommunicationStatusType]
(
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Key] [varchar](11) NULL,
	[Description] [varchar](255) NULL,
	[Sequence] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](100) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [XPKLU_CommunicationStatusType] PRIMARY KEY CLUSTERED ([Id] ASC)
)
