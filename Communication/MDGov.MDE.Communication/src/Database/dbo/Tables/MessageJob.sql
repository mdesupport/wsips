﻿CREATE TABLE [dbo].[MessageJob]
(
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BaseMessageId] [int] NOT NULL,
	[CommunicationTypeId] [int] NOT NULL,
	[ContactId] [int] NOT NULL,
	[CommunicationStatusTypeId] [int] NOT NULL,
	[FailedAttempts] [int] NULL,
	[Address] [varchar](100) NOT NULL,
	[SentDateTime] [datetime] NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](100) NOT NULL,
	CONSTRAINT [XPKMessageJob] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_MessageJob_BaseMessage] FOREIGN KEY ([BaseMessageId]) REFERENCES [dbo].[BaseMessage]([Id]), 
    CONSTRAINT [FK_MessageJob_LU_CommunicationType] FOREIGN KEY ([CommunicationTypeId]) REFERENCES [dbo].[LU_CommunicationType]([Id]), 
    CONSTRAINT [FK_MessageJob_LU_CommunicationStatusType] FOREIGN KEY ([CommunicationStatusTypeId]) REFERENCES [dbo].[LU_CommunicationStatusType]([Id])
)
