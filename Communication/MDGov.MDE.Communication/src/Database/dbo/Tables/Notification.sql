﻿CREATE TABLE [dbo].[Notification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	[NotificationText] [varchar](1000) NULL,
	[PermitId] INT NULL,
	[StartDate] DATETIME2 NULL,
	[EndDate] DATETIME2 NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] DATETIME2 NOT NULL,
	[LastModifiedBy] [varchar](100) NOT NULL,
	[LastModifiedDate] DATETIME2 NOT NULL,
	CONSTRAINT [XPKNotification] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Notification_LU_NotificationType] FOREIGN KEY ([NotificationTypeId]) REFERENCES [dbo].[LU_NotificationType]([Id])
)