﻿CREATE TABLE [dbo].[LU_NotificationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [varchar](10) NULL,
	[Description] [varchar](255) NULL,
	[Sequence] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](100) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	CONSTRAINT [XPKLU_NotificationType] PRIMARY KEY CLUSTERED ([Id] ASC)
) 
