﻿CREATE TABLE [dbo].[NotificationContact]
(
	[Id] INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL, 
    [NotificationId] INT NOT NULL, 
    [ContactId] INT NOT NULL, 
    [IsViewed] BIT NOT NULL DEFAULT(0),
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] DATETIME2 NOT NULL,
	[LastModifiedDate] DATETIME2 NOT NULL,
	[LastModifiedBy] [varchar](100) NOT NULL,
	CONSTRAINT [XPKNotificationContact] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_NotificationContact_Notification] FOREIGN KEY ([NotificationId]) REFERENCES [dbo].[Notification] ([Id]) ON DELETE CASCADE
)
