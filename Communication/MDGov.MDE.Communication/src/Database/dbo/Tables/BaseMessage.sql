﻿CREATE TABLE [dbo].[BaseMessage]
(
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Subject] [varchar](255) NULL,
	[Body] [varchar](max) NULL,
	[ApplicationId] [int] NULL,
	[RequestDateTime] [datetime] NULL,
	[From] [varchar](100) NULL,
	[Complete] [bit] NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](100) NOT NULL,
	CONSTRAINT [XPKBaseMessage] PRIMARY KEY CLUSTERED ([Id] ASC)
)
