﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_NotificationType]

SELECT '100', 'System', 10, 1, 'admin', GETDATE(), 'admin', GETDATE() UNION All
SELECT '200', 'Individual', 20, 1, 'admin', GETDATE(), 'admin', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_NotificationType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;