﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_CommunicationType]

SELECT '100', 'Email', 10, 1, 'admin', GETDATE(), 'admin', GETDATE() UNION All
SELECT '200', 'Text', 20, 1, 'admin', GETDATE(), 'admin', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_CommunicationType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;