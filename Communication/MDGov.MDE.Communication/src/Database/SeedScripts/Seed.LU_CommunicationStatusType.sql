﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_CommunicationStatusType]

SELECT '100', 'In Queue', 10, 1, 'admin', GETDATE(), 'admin', GETDATE() UNION All
SELECT '200', 'Successfully Sent', 20, 1, 'admin', GETDATE(), 'admin', GETDATE() UNION All
SELECT '300', 'Unknown Recipient', 30, 1, 'admin', GETDATE(), 'admin', GETDATE() UNION All
SELECT '400', 'Unknown Failure', 40, 1, 'admin', GETDATE(), 'admin', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_CommunicationStatusType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;