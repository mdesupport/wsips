﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Controllers.NotificationContactController
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Communication.Service.Controllers
{
  public class NotificationContactController : ApiController
  {
    private readonly IRepository<NotificationContact> _notificationContactRepository;

    public NotificationContactController(IRepository<NotificationContact> notificationContactRepository)
    {
      this._notificationContactRepository = notificationContactRepository;
    }

    [HttpGet]
    public IEnumerable<NotificationContact> GetAll(string includeProperties)
    {
      return (IEnumerable<NotificationContact>) this._notificationContactRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<NotificationContact> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<NotificationContact>) this._notificationContactRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public NotificationContact GetById(int id, string includeProperties)
    {
      return this._notificationContactRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._notificationContactRepository.Count(filters);
    }

    [HttpPost]
    public int Save(NotificationContact entity)
    {
      return this._notificationContactRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._notificationContactRepository.Delete(id);
    }
  }
}
