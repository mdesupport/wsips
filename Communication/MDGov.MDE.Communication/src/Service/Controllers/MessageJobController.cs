﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Controllers.MessageJobController
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Communication.Service.Controllers
{
  public class MessageJobController : ApiController
  {
    private readonly IRepository<MessageJob> _messageJobRepository;
    private readonly IValidator<MessageJob> _messageJobValidator;

    public MessageJobController(IRepository<MessageJob> messageJobRepository, IValidator<MessageJob> messageJobValidator)
    {
      this._messageJobRepository = messageJobRepository;
      this._messageJobValidator = messageJobValidator;
    }

    [HttpPost]
    public IEnumerable<MessageJob> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<MessageJob>) this._messageJobRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(MessageJob entity)
    {
      entity.CommunicationStatusTypeId = new int?(1);
      entity.CommunicationTypeId = 1;
      this._messageJobValidator.ValidateAndThrow<MessageJob>(entity);
      return this._messageJobRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._messageJobRepository.Delete(id);
    }
  }
}
