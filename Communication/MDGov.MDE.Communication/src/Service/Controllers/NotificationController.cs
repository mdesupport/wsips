﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Controllers.NotificationController
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Communication.Service.Controllers
{
  public class NotificationController : ApiController
  {
    private readonly IRepository<Notification> _notificationRepository;

    public NotificationController(IRepository<Notification> notificationRepository)
    {
      this._notificationRepository = notificationRepository;
    }

    [HttpGet]
    public IEnumerable<Notification> GetAll(string includeProperties)
    {
      return (IEnumerable<Notification>) this._notificationRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Notification> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Notification>) this._notificationRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public Notification GetById(int id, string includeProperties)
    {
      return this._notificationRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._notificationRepository.Count(filters);
    }

    [HttpPost]
    public int Save(Notification entity)
    {
      return this._notificationRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._notificationRepository.Delete(id);
    }
  }
}
