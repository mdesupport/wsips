﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Controllers.BaseMessageController
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Communication.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;
using StructureMap;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Helpers.Common;

namespace MDGov.MDE.Communication.Service.Controllers
{
  public class BaseMessageController : ApiController
  {
    private readonly IRepository<BaseMessage> _baseMessageRepository;
    private readonly IValidator<BaseMessage> _baseMessageValidator;

    public BaseMessageController(IRepository<BaseMessage> baseMessageRepository, IValidator<BaseMessage> baseMessageValidator)
    {
      this._baseMessageRepository = baseMessageRepository;
      this._baseMessageValidator = baseMessageValidator;
    }

    public IEnumerable<string> GetAll()
    {
      return (IEnumerable<string>) new string[2]
      {
        "value1",
        "value2"
      };
    }

    [HttpGet]
    public BaseMessage GetById(int id, string includeProperties)
    {
      return this._baseMessageRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(BaseMessage entity)
    {
        entity.ApplicationId = new int?(1);
      entity.RequestDateTime = new DateTime?(DateTime.Now);
      this._baseMessageValidator.ValidateAndThrow<BaseMessage>(entity);
      return this._baseMessageRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._baseMessageRepository.Delete(id);
    }
  }
}
