﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Controllers.MessageController
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using FluentValidation;
using FluentValidation.Results;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Communication.Service.Controllers
{
  public class MessageController : ApiController
  {
    private readonly IRepository<BaseMessage> _baseMessageRepository;
    private readonly IRepository<MessageJob> _messageJobRepository;
    private readonly IRepository<Notification> _notificationRepository;
    private readonly IRepository<NotificationContact> _notificationContactRepository;
    private readonly IValidator<Message> _messageValidator;
    private readonly IValidator<BaseMessage> _baseMessageValidator;
    private readonly IValidator<MessageJob> _messageJobValidator;
    private readonly IValidator<Notification> _notificationValidator;
    private readonly IValidator<NotificationContact> _notificationContactValidator;
    private readonly IService<Contact> _contactService;

    public MessageController(IRepository<BaseMessage> baseMessageRepository, IRepository<MessageJob> messageJobRepository, IRepository<Notification> notificationRepository, IRepository<NotificationContact> notificationContactRepository, IValidator<Message> messageValidator, IValidator<BaseMessage> baseMessageValidator, IValidator<MessageJob> messageJobValidator, IValidator<Notification> notificationValidator, IValidator<NotificationContact> notificationContactValidator, IService<Contact> contactService)
    {
      this._baseMessageRepository = baseMessageRepository;
      this._messageJobRepository = messageJobRepository;
      this._notificationRepository = notificationRepository;
      this._notificationContactRepository = notificationContactRepository;
      this._messageValidator = messageValidator;
      this._baseMessageValidator = baseMessageValidator;
      this._messageJobValidator = messageJobValidator;
      this._notificationValidator = notificationValidator;
      this._notificationContactValidator = notificationContactValidator;
      this._contactService = contactService;
    }

    [HttpPost]
    public int Save(Message entity)
    {
      this._messageValidator.ValidateAndThrow<Message>(entity);
      List<ValidationResult> source1 = new List<ValidationResult>();
      IEnumerable<Contact> range = this._contactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("@0.Contains(outerIt.Id)", new object[1]
        {
          (object) entity.ContactIds
        })
      }, "ContactCommunicationMethods");
      IEnumerable<Contact> source2 = range.Where<Contact>((Func<Contact, bool>) (c => c.EmailNotification));
      IEnumerable<Contact> source3 = range.Where<Contact>((Func<Contact, bool>) (c => c.ApplicationNotification));
      if (source2.Any<Contact>())
      {
        BaseMessage baseMessage = new BaseMessage();
        baseMessage.Subject = entity.Subject;
        baseMessage.Body = entity.Body;
        baseMessage.From = entity.From;
        baseMessage.ApplicationId = new int?(1);
        baseMessage.RequestDateTime = new DateTime?(DateTime.Now);
        baseMessage.LastModifiedBy = entity.LastModifiedBy;
        ValidationResult validationResult1 = this._baseMessageValidator.Validate(baseMessage);
        if (validationResult1.IsValid)
        {
          int num = this._baseMessageRepository.Save(baseMessage);
          foreach (Contact contact in source2)
          {
            MessageJob messageJob = new MessageJob();
            messageJob.BaseMessageId = num;
            messageJob.CommunicationTypeId = 1;
            messageJob.ContactId = new int?(contact.Id);
            messageJob.CommunicationStatusTypeId = new int?(1);
            ContactCommunicationMethod communicationMethod = contact.ContactCommunicationMethods.SingleOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (cm => cm.CommunicationMethodId == 3));
            if (communicationMethod != null)
              messageJob.Address = communicationMethod.CommunicationValue;
            messageJob.LastModifiedBy = entity.LastModifiedBy;
            ValidationResult validationResult2 = this._messageJobValidator.Validate(messageJob);
            if (validationResult2.IsValid)
              this._messageJobRepository.Save(messageJob);
            else
              source1.Add(validationResult2);
          }
        }
        else
          source1.Add(validationResult1);
      }
      if (source3.Any<Contact>())
      {
        Notification notification = new Notification();
        notification.NotificationTypeId = new int?(entity.NotificationTypeId);
        notification.NotificationText = entity.NotificationText;
        notification.PermitId = new int?(entity.PermitId);
        notification.StartDate = entity.StartDate;
        notification.EndDate = entity.EndDate;
        notification.Active = new bool?(true);
        notification.LastModifiedBy = entity.LastModifiedBy;
        ValidationResult validationResult1 = this._notificationValidator.Validate(notification);
        if (validationResult1.IsValid)
        {
          int num = this._notificationRepository.Save(notification);
          foreach (Contact contact in source3)
          {
            NotificationContact notificationContact = new NotificationContact();
            notificationContact.NotificationId = num;
            notificationContact.ContactId = contact.Id;
            notificationContact.LastModifiedBy = entity.LastModifiedBy;
            ValidationResult validationResult2 = this._notificationContactValidator.Validate(notificationContact);
            if (validationResult2.IsValid)
              this._notificationContactRepository.Save(notificationContact);
            else
              source1.Add(validationResult2);
          }
        }
        else
          source1.Add(validationResult1);
      }
      if (source1.Any<ValidationResult>((Func<ValidationResult, bool>) (res => !res.IsValid)))
        throw new ValidationException(source1.SelectMany<ValidationResult, ValidationFailure>((Func<ValidationResult, IEnumerable<ValidationFailure>>) (res => (IEnumerable<ValidationFailure>) res.Errors)));
      return 0;
    }
  }
}
