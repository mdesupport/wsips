﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.FilterConfig
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using MDGov.MDE.WSIPS.Communication.Service.Helpers.Filters;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace MDGov.MDE.Communication.Service
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add((object) new HandleErrorAttribute());
    }

    public static void RegisterHttpFilters(HttpFilterCollection filters)
    {
      filters.Add((IFilter) new HandleExceptionAttribute());
    }
  }
}
