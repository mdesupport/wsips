﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.App_Start.StructuremapMvc
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using MDGov.MDE.Communication.Service.DependencyResolution;
using StructureMap;
using System.Web.Http;
using System.Web.Mvc;

namespace MDGov.MDE.Communication.Service.App_Start
{
  public static class StructuremapMvc
  {
    public static void Start()
    {
      IContainer container = IoC.Initialize();
      DependencyResolver.SetResolver((object) new StructureMapDependencyResolver(container));
      GlobalConfiguration.Configuration.DependencyResolver = (System.Web.Http.Dependencies.IDependencyResolver) new StructureMapDependencyResolver(container);
    }
  }
}
