﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.WebApiConfig
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using System.Web.Http;

namespace MDGov.MDE.Communication.Service
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{action}/{id}", (object) new
      {
        id = RouteParameter.Optional
      });
    }
  }
}
