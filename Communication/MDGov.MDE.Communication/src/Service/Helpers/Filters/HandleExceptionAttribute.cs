﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Communication.Service.Helpers.Filters.HandleExceptionAttribute
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MDGov.MDE.WSIPS.Communication.Service.Helpers.Filters
{
  public class HandleExceptionAttribute : ExceptionFilterAttribute
  {
    public override void OnException(HttpActionExecutedContext actionExecutedContext)
    {
      throw new HttpResponseException(actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An exception has been caught", actionExecutedContext.Exception));
    }
  }
}
