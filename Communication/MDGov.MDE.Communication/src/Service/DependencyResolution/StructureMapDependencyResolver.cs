﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.DependencyResolution.StructureMapDependencyResolver
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using StructureMap;
using System;
using System.Web.Http.Dependencies;

namespace MDGov.MDE.Communication.Service.DependencyResolution
{
  public class StructureMapDependencyResolver : StructureMapDependencyScope, IDependencyResolver, IDependencyScope, IDisposable
  {
    public StructureMapDependencyResolver(IContainer container)
      : base(container)
    {
    }

    public IDependencyScope BeginScope()
    {
      return (IDependencyScope) new StructureMapDependencyResolver(this.Container.GetNestedContainer());
    }
  }
}
