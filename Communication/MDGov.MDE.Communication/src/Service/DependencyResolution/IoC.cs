﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.DependencyResolution.IoC
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using MDGov.MDE.Communication.BusinessLayer.DependencyResolution;
using StructureMap;
using StructureMap.Graph;
using System;

namespace MDGov.MDE.Communication.Service.DependencyResolution
{
  public static class IoC
  {
    public static IContainer Initialize()
    {
      ObjectFactory.Initialize((Action<IInitializationExpression>) (x =>
      {
        x.Scan((Action<IAssemblyScanner>) (scan =>
        {
          scan.TheCallingAssembly();
          scan.WithDefaultConventions();
        }));
        x.AddRegistry<BusinessLayerRegistry>();
        x.AddRegistry<ServiceLayerRegistry>();
      }));
      return ObjectFactory.Container;
    }
  }
}
