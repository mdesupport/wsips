﻿using MDGov.MDE.Communication.Service.App_Start;
using System.Reflection;
using System.Runtime.InteropServices;
using WebActivator;

[assembly: ComVisible(false)]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyTrademark("")]
[assembly: Guid("1c8ecc1c-08c5-43b6-8650-c0e04ccf8d00")]
[assembly: PreApplicationStartMethod(typeof (StructuremapMvc), "Start")]
[assembly: AssemblyTitle("Service")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("JMT")]
[assembly: AssemblyProduct("Service")]
[assembly: AssemblyCopyright("Copyright © JMT 2012")]
[assembly: AssemblyVersion("1.0.0.0")]
