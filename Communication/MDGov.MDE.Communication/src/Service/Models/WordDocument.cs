﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Models.WordDocument
// Assembly: MDGov.MDE.Communication.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 03F6FC6E-E1FB-44E3-9E4F-284B7886B3AB
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.Service.dll

using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.Communication.Service.Models
{
  public class WordDocument
  {
    public int DocumentByteId { get; set; }

    public int PermitId { get; set; }

    public int? PermitStatusId { get; set; }

    public int TableId { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public int? DocumentTypeId { get; set; }

    public bool SaveToFileSystem { get; set; }

    public bool SaveToDatabase { get; set; }

    public string SaveToLocation { get; set; }

    public int TemplateTypeId { get; set; }

    public IEnumerable<LU_Template> Templates { get; set; }
  }
}
