﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Model.NotificationContact
// Assembly: MDGov.MDE.Communication.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 36223616-8834-454C-B9A5-EAD23DBF9888
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDGov.MDE.Communication.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Communication.Model
{
  public class NotificationContact : IUpdatableEntity
  {
    public int Id { get; set; }

    public int NotificationId { get; set; }

    public int ContactId { get; set; }

    public bool IsViewed { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public virtual Notification Notification { get; set; }
  }
}
