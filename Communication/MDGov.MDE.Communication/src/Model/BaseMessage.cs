﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Model.BaseMessage
// Assembly: MDGov.MDE.Communication.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 36223616-8834-454C-B9A5-EAD23DBF9888
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDGov.MDE.Communication.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Communication.Model
{
  public class BaseMessage : IUpdatableEntity
  {
    public int Id { get; set; }

    public string Subject { get; set; }

    public string Body { get; set; }

    public int? ApplicationId { get; set; }

    public DateTime? RequestDateTime { get; set; }

    public string From { get; set; }

    public bool Complete { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public virtual ICollection<MessageJob> MessageJobs { get; set; }

    public BaseMessage()
    {
      this.MessageJobs = (ICollection<MessageJob>) new HashSet<MessageJob>();
    }
  }
}
