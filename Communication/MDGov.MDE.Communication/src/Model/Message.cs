﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Model.Message
// Assembly: MDGov.MDE.Communication.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 36223616-8834-454C-B9A5-EAD23DBF9888
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDGov.MDE.Communication.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Communication.Model
{
  public class Message : IUpdatableEntity
  {
    public List<int> ContactIds { get; set; }

    public string Subject { get; set; }

    public string Body { get; set; }

    public string From { get; set; }

    public int NotificationTypeId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public int PermitId { get; set; }

    public string NotificationText { get; set; }

    public int Id { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }
  }
}
