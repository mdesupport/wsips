﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Model.MessageJob
// Assembly: MDGov.MDE.Communication.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 36223616-8834-454C-B9A5-EAD23DBF9888
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDGov.MDE.Communication.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Communication.Model
{
  public class MessageJob : IUpdatableEntity
  {
    public int Id { get; set; }

    public int BaseMessageId { get; set; }

    public int CommunicationTypeId { get; set; }

    public int? ContactId { get; set; }

    public int? CommunicationStatusTypeId { get; set; }

    public int? FailedAttempts { get; set; }

    public string Address { get; set; }

    public DateTime? SentDateTime { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public virtual BaseMessage BaseMessage { get; set; }

    public virtual LU_CommunicationStatusType LU_CommunicationStatusType { get; set; }

    public virtual LU_CommunicationType LU_CommunicationType { get; set; }
  }
}
