﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Model.ModelContext
// Assembly: MDGov.MDE.Communication.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 36223616-8834-454C-B9A5-EAD23DBF9888
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDGov.MDE.Communication.Model.dll

using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MDGov.MDE.Communication.Model
{
  public class ModelContext : DbContext
  {
    public DbSet<BaseMessage> BaseMessages { get; set; }

    public DbSet<MDGov.MDE.Communication.Model.LU_CommunicationStatusType> LU_CommunicationStatusType { get; set; }

    public DbSet<MDGov.MDE.Communication.Model.LU_CommunicationType> LU_CommunicationType { get; set; }

    public DbSet<MDGov.MDE.Communication.Model.LU_NotificationType> LU_NotificationType { get; set; }

    public DbSet<MessageJob> MessageJobs { get; set; }

    public DbSet<NotificationContact> NotificationContacts { get; set; }

    public DbSet<Notification> Notifications { get; set; }

    public ModelContext()
      : base("name=ModelContext")
    {
      this.Configuration.ProxyCreationEnabled = false;
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      throw new UnintentionalCodeFirstException();
    }
  }
}
