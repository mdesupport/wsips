﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.WSIPSService
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using System.ComponentModel;
using System.ServiceProcess;

namespace MDGov.MDE.Communication.WindowsService
{
  public class WSIPSService : ServiceBase
  {
    private IContainer components = (IContainer) null;
    private ScheduleTasks _scheduleTasks;

    public WSIPSService()
    {
      this.InitializeComponent();
    }

    protected override void OnStart(string[] args)
    {
      StructureMapBootstrapper.Initialize();
      this._scheduleTasks = new ScheduleTasks();
      if (this._scheduleTasks.PollingEnabled)
        this._scheduleTasks.StartServicePolling();
      if (!this._scheduleTasks.NonPollingEnabled)
        return;
      this._scheduleTasks.StartServiceNonPolling();
    }

    protected override void OnStop()
    {
      try
      {
        this._scheduleTasks.StopServicePolling();
        this._scheduleTasks.StopServiceNonPolling();
      }
      catch
      {
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.ServiceName = "MDEWindowsService";
    }
  }
}
