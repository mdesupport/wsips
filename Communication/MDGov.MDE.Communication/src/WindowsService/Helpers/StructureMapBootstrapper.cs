﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.StructureMapBootstrapper
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Helpers.DocumentUtility.Interface;
using MDGov.MDE.Common.Helpers.OpenXml;
using MDGov.MDE.Common.Helpers.OpenXml.Interface;
using MDGov.MDE.Common.Logging;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Communication.Service.Services;
using StructureMap;
using System;
using System.Data.Entity;

namespace MDGov.MDE.Communication.WindowsService
{
  public static class StructureMapBootstrapper
  {
    public static void Initialize()
    {
      ObjectFactory.Initialize((Action<IInitializationExpression>) (c =>
      {
        c.For<DbContext>().Use<ModelContext>();
        c.For<IRepository<ErrorLog>>().Use((Func<IContext, IRepository<ErrorLog>>) (ctx => (IRepository<ErrorLog>) new Repository<ErrorLog>(ctx.GetInstance<LoggingContext>())));
        c.For(typeof (IRepository<>)).Use(typeof (Repository<>));
        c.For(typeof (IService<>)).Use(typeof (ServiceBase<>));
        c.For(typeof (IUpdatableService<>)).Use(typeof (UpdatableService<>));
        c.For<IBaseMessageService>().Use<BaseMessageService>();
        c.For<IMessageJobService>().Use<MessageJobService>();
        c.For<IErrorLogging>().Use<ErrorLogging>();
        c.For<IOpenXmlWordHelper>().Use<OpenXmlHelper.WordHelper>();
        c.For<IDocumentUtility>().Use<MDGov.MDE.Common.Helpers.DocumentUtility.DocumentUtility>();
      }));
    }
  }
}
