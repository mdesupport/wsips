﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.Helpers.Communications
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

namespace MDGov.MDE.Communication.WindowsService.Helpers
{
  public class Communications
  {
    public enum CommunicationStatusTypes
    {
      Unknown,
      InQueue,
      SuccessfullySent,
      UnknownRecepient,
      UnknownFailure,
    }
  }
}
