﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.FileFunctions
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using System.Text.RegularExpressions;

namespace MDGov.MDE.Communication.WindowsService
{
  public class FileFunctions
  {
    public static string ClearInvalidFilenameChars(string Filename)
    {
      return new Regex("[\\\\/:?'=<>|]").Replace(Filename, " ");
    }

    public static string AddBackSlashIfNeeded(string Filename)
    {
      if (Filename.EndsWith("\\"))
        return Filename;
      return Filename + "\\";
    }

    public static string AddForwardSlashIfNeeded(string Filename)
    {
      if (Filename.EndsWith("/"))
        return Filename;
      return Filename + "/";
    }
  }
}
