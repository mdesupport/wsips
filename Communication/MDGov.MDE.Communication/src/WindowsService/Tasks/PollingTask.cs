﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.PollingTask
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using DocumentFormat.OpenXml.Packaging;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Helpers.DocumentUtility.Interface;
using MDGov.MDE.Common.Helpers.OpenXml.Interface;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Communication.Service.Models;
using MDGov.MDE.Communication.Service.Services;
using MDGov.MDE.Communication.WindowsService.Helpers;
using MDGov.MDE.Communication.WindowsService.Models;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Mail;
using System.Reflection;

namespace MDGov.MDE.Communication.WindowsService
{
  public class PollingTask : IPollingTask
  {
    private readonly List<string> _documentErrors = new List<string>();
    public readonly string DocumentFolder = ConfigurationManager.AppSettings["DocumentsDirectory"];
    public readonly string TemplateFolder = ConfigurationManager.AppSettings["TemplatesDirectory"];
    private readonly IErrorLogging _errorLogging;
    private readonly IOpenXmlWordHelper _wordHelper;
    private readonly IDocumentUtility _documentUtility;
    private readonly IService<Permit> _permitService;
    private readonly IService<PermitContact> _permitContactService;
    private readonly IService<LU_Template> _templateService;
    private readonly IUpdatableService<DocumentByte> _documentByteService;
    private readonly IUpdatableService<DocumentJob> _documentJobService;
    private readonly IUpdatableService<BaseMessage> _baseMessageService;
    private readonly IUpdatableService<MessageJob> _messageJobService;
    private readonly IService<ContactCommunicationMethod> _contactCommunicationService;

    public PollingTaskTypes PollingTaskType { get; set; }

    public event PollingTask.MessageEventHandler Message;

    public PollingTask(int intervalMultiplier, IErrorLogging errorLogging, IOpenXmlWordHelper wordHelper, IDocumentUtility documentUtility, IService<Permit> permitService, IService<PermitContact> permitContactService, IService<LU_Template> templateService, IUpdatableService<DocumentByte> documentByteService, IUpdatableService<DocumentJob> documentJobService, IUpdatableService<BaseMessage> baseMessageService, IUpdatableService<MessageJob> messageJobService, IService<ContactCommunicationMethod> contactCommunicationService)
      : base(intervalMultiplier)
    {
      this._errorLogging = errorLogging;
      this._wordHelper = wordHelper;
      this._documentUtility = documentUtility;
      this._permitService = permitService;
      this._permitContactService = permitContactService;
      this._templateService = templateService;
      this._documentByteService = documentByteService;
      this._documentJobService = documentJobService;
      this._baseMessageService = baseMessageService;
      this._messageJobService = messageJobService;
      this._contactCommunicationService = contactCommunicationService;
    }

    public override TaskResult PerformTask()
    {
      if (this._inProcess)
        return TaskResult.Process_Busy;
      try
      {
        switch (this.PollingTaskType)
        {
          case PollingTaskTypes.ProcessLoggedMessages:
            this.SendMessage(MethodBase.GetCurrentMethod().Name, this.PollingTaskType.ToString(), false);
            return this.ProcessLoggedMessages();
          case PollingTaskTypes.GenerateDocument:
            this.SendMessage(MethodBase.GetCurrentMethod().Name, this.PollingTaskType.ToString(), false);
            return this.GenerateDocument();
          default:
            return TaskResult.No_Error;
        }
      }
      catch (Exception ex)
      {
        if (this.Message != null)
          this.SendMessage(MethodBase.GetCurrentMethod().Name, ex.Message, false);
        return TaskResult.Error_Occurred;
      }
      finally
      {
        this._inProcess = false;
      }
    }

    private void SendMessage(string callingFunction, string message, bool verbose)
    {
      if (message == null)
        return;
      this.Message(callingFunction, message, verbose);
    }

    private TaskResult ProcessLoggedMessages()
    {
      try
      {
        EmailService instance1 = ObjectFactory.GetInstance<EmailService>();
        IBaseMessageService instance2 = ObjectFactory.GetInstance<IBaseMessageService>();
        IMessageJobService instance3 = ObjectFactory.GetInstance<IMessageJobService>();
        List<BaseMessage> currentBaseMessages = instance2.GetCurrentBaseMessages();
        if (currentBaseMessages.Count <= 0)
          return TaskResult.No_Error;
        foreach (BaseMessage baseMessage in currentBaseMessages)
        {
          string body = baseMessage.Body;
          int num = 0;
          List<MessageJob> byBaseMessageId = instance3.GetByBaseMessageId(baseMessage.Id, false);
          foreach (MessageJob messageJob1 in byBaseMessageId)
          {
            Communications.CommunicationStatusTypes communicationStatusTypes = instance1.SendMessage(baseMessage, messageJob1, true);
            if (communicationStatusTypes == Communications.CommunicationStatusTypes.SuccessfullySent)
            {
              messageJob1.SentDateTime = new DateTime?(DateTime.Now);
              ++num;
            }
            else
            {
              int? failedAttempts;
              if (!messageJob1.FailedAttempts.HasValue)
              {
                messageJob1.FailedAttempts = new int?(1);
              }
              else
              {
                MessageJob messageJob2 = messageJob1;
                failedAttempts = messageJob2.FailedAttempts;
                int? nullable = failedAttempts.HasValue ? new int?(failedAttempts.GetValueOrDefault() + 1) : new int?();
                messageJob2.FailedAttempts = nullable;
              }
              failedAttempts = messageJob1.FailedAttempts;
              if ((failedAttempts.GetValueOrDefault() < 3 ? 0 : (failedAttempts.HasValue ? 1 : 0)) != 0)
                ++num;
            }
            messageJob1.CommunicationStatusTypeId = new int?((int) communicationStatusTypes);
            messageJob1.LastModifiedDate = DateTime.Now;
            messageJob1.LastModifiedBy = "MDEService";
            instance3.Save(messageJob1);
          }
          if (byBaseMessageId.Count == num)
          {
            baseMessage.Complete = true;
            baseMessage.LastModifiedBy = "MDEService";
            baseMessage.LastModifiedDate = DateTime.Now;
            instance2.Save(baseMessage);
          }
        }
        return TaskResult.No_Error;
      }
      catch (Exception ex)
      {
        this._errorLogging.Log(ex);
        return TaskResult.Error_Occurred;
      }
    }

    private TaskResult GenerateDocument()
    {
      try
      {
        int result = 100;
        List<PumpagePermit> pumpagePermitList1 = new List<PumpagePermit>();
        Dictionary<int, string> source = new Dictionary<int, string>();
        DocumentJob docJob = this._documentJobService.GetRange(0, 1, "CreatedDate", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("Active == True", new object[0])
        }, (string) null).FirstOrDefault<DocumentJob>();
        if (docJob == null)
          return TaskResult.No_Error;
        int? nullable = docJob.Status;
        if ((nullable.GetValueOrDefault() != 1 ? 0 : (nullable.HasValue ? 1 : 0)) != 0)
          return TaskResult.No_Error;
        int.TryParse(ConfigurationManager.AppSettings["BatchSize"], out result);
        docJob.Status = new int?(1);
        this._documentJobService.Save(docJob);
        List<Permit> list1 = this._permitService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("PermitStatusId == " + (object) 46, new object[0])
        }, "PermitConditions.ConditionCompliances").ToList<Permit>();
        nullable = docJob.TemplateId;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        int? local = nullable;
        // ISSUE: explicit reference operation
        int valueOrDefault1 = (local).GetValueOrDefault();
        // ISSUE: explicit reference operation
        if ((local).HasValue)
        {
          switch (valueOrDefault1)
          {
            case 34:
              using (List<Permit>.Enumerator enumerator = list1.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  Permit current = enumerator.Current;
                  PermitCondition permitCondition = current.PermitConditions.FirstOrDefault<PermitCondition>((Func<PermitCondition, bool>) (c =>
                  {
                    int? reportingPeriodId = c.ConditionReportingPeriodId;
                    int num1;
                    if ((reportingPeriodId.GetValueOrDefault() != 1 ? 0 : (reportingPeriodId.HasValue ? 1 : 0)) != 0)
                    {
                      int? standardConditionTypeId = c.StandardConditionTypeId;
                      if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0 && c.RequiresSelfReporting.GetValueOrDefault())
                      {
                        num1 = c.ConditionCompliances.Any<ConditionCompliance>((Func<ConditionCompliance, bool>) (cc =>
                        {
                          int num2;
                          if (cc.PermitConditionComplianceStatusId == 3 || cc.PermitConditionComplianceStatusId == 4)
                          {
                            DateTime dateTime = cc.ComplianceReportingEndDate;
                            int year1 = dateTime.Year;
                            dateTime = DateTime.Today;
                            int year2 = dateTime.Year;
                            num2 = year1 == year2 ? 1 : 0;
                          }
                          else
                            num2 = 0;
                          return num2 != 0;
                        })) ? 1 : 0;
                        goto label_4;
                      }
                    }
                    num1 = 0;
label_4:
                    return num1 != 0;
                  }));
                  if (permitCondition != null)
                  {
                    List<PumpagePermit> pumpagePermitList2 = pumpagePermitList1;
                    PumpagePermit pumpagePermit1 = new PumpagePermit();
                    pumpagePermit1.Id = current.Id;
                    pumpagePermit1.Condition = permitCondition;
                    PumpagePermit pumpagePermit2 = pumpagePermit1;
                    nullable = docJob.TemplateId;
                    int valueOrDefault2 = nullable.GetValueOrDefault();
                    pumpagePermit2.TemplateId = valueOrDefault2;
                    PumpagePermit pumpagePermit3 = pumpagePermit1;
                    pumpagePermitList2.Add(pumpagePermit3);
                  }
                }
                break;
              }
            case 35:
            case 56:
              using (List<Permit>.Enumerator enumerator = list1.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  Permit current = enumerator.Current;
                  PermitCondition permitCondition = current.PermitConditions.FirstOrDefault<PermitCondition>((Func<PermitCondition, bool>) (c =>
                  {
                    int? reportingPeriodId = c.ConditionReportingPeriodId;
                    int num1;
                    if ((reportingPeriodId.GetValueOrDefault() != 3 ? 0 : (reportingPeriodId.HasValue ? 1 : 0)) != 0)
                    {
                      int? standardConditionTypeId = c.StandardConditionTypeId;
                      if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0 && c.RequiresSelfReporting.GetValueOrDefault())
                      {
                        num1 = c.ConditionCompliances.Any<ConditionCompliance>((Func<ConditionCompliance, bool>) (cc =>
                        {
                          int num2;
                          if (cc.PermitConditionComplianceStatusId == 3 || cc.PermitConditionComplianceStatusId == 4)
                          {
                            DateTime dateTime = cc.ComplianceReportingEndDate;
                            int year1 = dateTime.Year;
                            dateTime = DateTime.Today;
                            int year2 = dateTime.Year;
                            if (year1 == year2)
                            {
                              dateTime = cc.ComplianceReportingDueDate;
                              int year3 = dateTime.Year;
                              dateTime = DateTime.Today;
                              int year4 = dateTime.Year;
                              num2 = year3 == year4 ? 1 : 0;
                              goto label_5;
                            }
                          }
                          num2 = 0;
label_5:
                          return num2 != 0;
                        })) ? 1 : 0;
                        goto label_6;
                      }
                    }
                    num1 = 0;
label_6:
                    return num1 != 0;
                  }));
                  if (permitCondition != null)
                  {
                    List<PumpagePermit> pumpagePermitList2 = pumpagePermitList1;
                    PumpagePermit pumpagePermit1 = new PumpagePermit();
                    pumpagePermit1.Id = current.Id;
                    pumpagePermit1.Condition = permitCondition;
                    PumpagePermit pumpagePermit2 = pumpagePermit1;
                    nullable = docJob.TemplateId;
                    int valueOrDefault2 = nullable.GetValueOrDefault();
                    pumpagePermit2.TemplateId = valueOrDefault2;
                    PumpagePermit pumpagePermit3 = pumpagePermit1;
                    pumpagePermitList2.Add(pumpagePermit3);
                  }
                }
                break;
              }
            case 36:
              using (List<Permit>.Enumerator enumerator = list1.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  Permit current = enumerator.Current;
                  PermitCondition permitCondition = current.PermitConditions.FirstOrDefault<PermitCondition>((Func<PermitCondition, bool>) (c =>
                  {
                    int? reportingPeriodId = c.ConditionReportingPeriodId;
                    int num1;
                    if ((reportingPeriodId.GetValueOrDefault() != 3 ? 0 : (reportingPeriodId.HasValue ? 1 : 0)) != 0)
                    {
                      int? standardConditionTypeId = c.StandardConditionTypeId;
                      if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0 && c.RequiresSelfReporting.GetValueOrDefault())
                      {
                        num1 = c.ConditionCompliances.Any<ConditionCompliance>((Func<ConditionCompliance, bool>) (cc =>
                        {
                          int num2;
                          if (cc.PermitConditionComplianceStatusId == 3 || cc.PermitConditionComplianceStatusId == 4)
                          {
                            DateTime dateTime = cc.ComplianceReportingEndDate;
                            int year1 = dateTime.Year;
                            dateTime = DateTime.Today;
                            int year2 = dateTime.Year;
                            if (year1 == year2)
                            {
                              dateTime = cc.ComplianceReportingDueDate;
                              int year3 = dateTime.Year;
                              dateTime = DateTime.Today;
                              int year4 = dateTime.Year;
                              num2 = year3 > year4 ? 1 : 0;
                              goto label_7;
                            }
                          }
                          num2 = 0;
label_7:
                          return num2 != 0;
                        })) ? 1 : 0;
                        goto label_8;
                      }
                    }
                    num1 = 0;
label_8:
                    return num1 != 0;
                  }));
                  if (permitCondition != null)
                  {
                    List<PumpagePermit> pumpagePermitList2 = pumpagePermitList1;
                    PumpagePermit pumpagePermit1 = new PumpagePermit();
                    pumpagePermit1.Id = current.Id;
                    pumpagePermit1.Condition = permitCondition;
                    PumpagePermit pumpagePermit2 = pumpagePermit1;
                    nullable = docJob.TemplateId;
                    int valueOrDefault2 = nullable.GetValueOrDefault();
                    pumpagePermit2.TemplateId = valueOrDefault2;
                    PumpagePermit pumpagePermit3 = pumpagePermit1;
                    pumpagePermitList2.Add(pumpagePermit3);
                  }
                }
                break;
              }
            case 55:
              using (List<Permit>.Enumerator enumerator = list1.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  Permit current = enumerator.Current;
                  PermitCondition permitCondition = current.PermitConditions.FirstOrDefault<PermitCondition>((Func<PermitCondition, bool>) (c =>
                  {
                    int? reportingPeriodId = c.ConditionReportingPeriodId;
                    int num1;
                    if ((reportingPeriodId.GetValueOrDefault() != 1 ? 0 : (reportingPeriodId.HasValue ? 1 : 0)) != 0)
                    {
                      int? standardConditionTypeId = c.StandardConditionTypeId;
                      if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0 && c.RequiresSelfReporting.GetValueOrDefault())
                      {
                        num1 = c.ConditionCompliances.Any<ConditionCompliance>((Func<ConditionCompliance, bool>) (cc =>
                        {
                          int num2;
                          if (cc.PermitConditionComplianceStatusId == 3 || cc.PermitConditionComplianceStatusId == 4)
                          {
                            DateTime dateTime = cc.ComplianceReportingEndDate;
                            int year = dateTime.Year;
                            dateTime = DateTime.Today;
                            int num3 = dateTime.Year - 1;
                            num2 = year == num3 ? 1 : 0;
                          }
                          else
                            num2 = 0;
                          return num2 != 0;
                        })) ? 1 : 0;
                        goto label_9;
                      }
                    }
                    num1 = 0;
label_9:
                    return num1 != 0;
                  }));
                  if (permitCondition != null)
                  {
                    List<PumpagePermit> pumpagePermitList2 = pumpagePermitList1;
                    PumpagePermit pumpagePermit1 = new PumpagePermit();
                    pumpagePermit1.Id = current.Id;
                    pumpagePermit1.Condition = permitCondition;
                    PumpagePermit pumpagePermit2 = pumpagePermit1;
                    nullable = docJob.TemplateId;
                    int valueOrDefault2 = nullable.GetValueOrDefault();
                    pumpagePermit2.TemplateId = valueOrDefault2;
                    PumpagePermit pumpagePermit3 = pumpagePermit1;
                    pumpagePermitList2.Add(pumpagePermit3);
                  }
                }
                break;
              }
            case 57:
              using (List<Permit>.Enumerator enumerator = list1.GetEnumerator())
              {
                while (enumerator.MoveNext())
                {
                  Permit current = enumerator.Current;
                  PermitCondition permitCondition = current.PermitConditions.FirstOrDefault<PermitCondition>((Func<PermitCondition, bool>) (c =>
                  {
                    int? reportingPeriodId = c.ConditionReportingPeriodId;
                    int num1;
                    if ((reportingPeriodId.GetValueOrDefault() != 3 ? 0 : (reportingPeriodId.HasValue ? 1 : 0)) != 0)
                    {
                      int? standardConditionTypeId = c.StandardConditionTypeId;
                      if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0 && c.RequiresSelfReporting.GetValueOrDefault())
                      {
                        num1 = c.ConditionCompliances.Any<ConditionCompliance>((Func<ConditionCompliance, bool>) (cc =>
                        {
                          int num2;
                          if (cc.PermitConditionComplianceStatusId == 3 || cc.PermitConditionComplianceStatusId == 4)
                          {
                            DateTime dateTime = cc.ComplianceReportingEndDate;
                            int year1 = dateTime.Year;
                            dateTime = DateTime.Today;
                            int num3 = dateTime.Year - 1;
                            if (year1 == num3)
                            {
                              dateTime = cc.ComplianceReportingDueDate;
                              int year2 = dateTime.Year;
                              dateTime = DateTime.Today;
                              int year3 = dateTime.Year;
                              num2 = year2 == year3 ? 1 : 0;
                              goto label_10;
                            }
                          }
                          num2 = 0;
label_10:
                          return num2 != 0;
                        })) ? 1 : 0;
                        goto label_11;
                      }
                    }
                    num1 = 0;
label_11:
                    return num1 != 0;
                  }));
                  if (permitCondition != null)
                  {
                    List<PumpagePermit> pumpagePermitList2 = pumpagePermitList1;
                    PumpagePermit pumpagePermit1 = new PumpagePermit();
                    pumpagePermit1.Id = current.Id;
                    pumpagePermit1.Condition = permitCondition;
                    PumpagePermit pumpagePermit2 = pumpagePermit1;
                    nullable = docJob.TemplateId;
                    int valueOrDefault2 = nullable.GetValueOrDefault();
                    pumpagePermit2.TemplateId = valueOrDefault2;
                    PumpagePermit pumpagePermit3 = pumpagePermit1;
                    pumpagePermitList2.Add(pumpagePermit3);
                  }
                }
                break;
              }
          }
        }
        foreach (PumpagePermit pumpagePermit in pumpagePermitList1)
        {
          IEnumerable<PermitContact> range = this._permitContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("PermitId == " + (object) pumpagePermit.Id, new object[0])
          }, "Contact");
          PermitContact permitContact = range.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
          {
            int? contactTypeId = c.ContactTypeId;
            return contactTypeId.GetValueOrDefault() == 22 && contactTypeId.HasValue;
          })) ?? range.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c => c.IsPermittee));
          if (permitContact != null)
          {
            //string str = permitContact.Contact.IsBusiness ? permitContact.Contact.BusinessName : permitContact.Contact.LastName;
            string str = string.IsNullOrEmpty(permitContact.Contact.BusinessName) ? 
                  permitContact.Contact.LastName + ", " + permitContact.Contact.FirstName : permitContact.Contact.BusinessName;
            source.Add(pumpagePermit.Id, str);
          }
        }
        List<int> list2 = source.OrderBy<KeyValuePair<int, string>, string>((Func<KeyValuePair<int, string>, string>) (i => i.Value)).Select<KeyValuePair<int, string>, int>((Func<KeyValuePair<int, string>, int>) (key => key.Key)).ToList<int>();
        int num4 = list2.Count / result;
        int count = list2.Count % result;
        List<LU_Template> luTemplateList = new List<LU_Template>()
        {
          this._templateService.GetById(docJob.TemplateId.GetValueOrDefault(), (string) null)
        };
        string str1 = string.Format("PumpageReports_{0}.zip", (object)DateTime.Now.ToString("yyyyMMddhhmm"));
        try
        {
          for (int index = 0; index < num4; ++index)
          {
            IEnumerable<int> permitIds = list2.Skip<int>(index * result).Take<int>(result);
            string baseFileName = "PumpageReports_Part" + (index + 1).ToString((IFormatProvider) CultureInfo.InvariantCulture);
            WordDocument documentGroup = this.GenerateDocumentGroup((IEnumerable<LU_Template>) luTemplateList, permitIds, baseFileName, true);
            if (documentGroup == null)
            {
              docJob.Status = new int?(3);
              docJob.Active = new bool?(false);
              this._documentJobService.Save(docJob);
              return TaskResult.Error_Occurred;
            }
            PollingTask.AddFileToZip(this.DocumentFolder + str1, this.DocumentFolder + documentGroup.Title);
            File.Delete(this.DocumentFolder + documentGroup.Title);
          }
            if (count > 0)
            {
                IEnumerable<int> permitIds = list2.Skip<int>(num4 * result).Take<int>(count);
                string baseFileName = "PumpageReports_Part" + (num4 + 1).ToString((IFormatProvider)CultureInfo.InvariantCulture);
                WordDocument documentGroup = this.GenerateDocumentGroup((IEnumerable<LU_Template>)luTemplateList, permitIds, baseFileName, true);
                if (documentGroup == null)
                {
                    docJob.Status = new int?(3);
                    docJob.Active = new bool?(false);
                    this._documentJobService.Save(docJob);
                    return TaskResult.Error_Occurred;
                }
                PollingTask.AddFileToZip(this.DocumentFolder + str1, this.DocumentFolder + documentGroup.Title);
                File.Delete(this.DocumentFolder + documentGroup.Title);
            }
          byte[] numArray = File.ReadAllBytes(this.DocumentFolder + str1);
          DocumentByte entity2 = new DocumentByte()
          {
            Document = numArray,
            MimeType = "application/zip"
          };
          WordDocument wordDocument = new WordDocument()
          {
            Title = str1,
            DocumentByteId = this._documentByteService.Save(entity2)
          };
          File.Delete(this.DocumentFolder + str1);
          if (this._documentErrors.Any<string>())
          {
            foreach (string documentError in this._documentErrors)
              this._errorLogging.Log(documentError, LogMessageType.Error);
            docJob.Status = new int?(3);
            docJob.Active = new bool?(false);
            this._documentJobService.Save(docJob);
            return TaskResult.Error_Occurred;
          }
          docJob.Status = new int?(2);
          docJob.DocumentByteId = new int?(wordDocument.DocumentByteId);
          docJob.Active = new bool?(false);
          this._documentJobService.Save(docJob);
          ContactCommunicationMethod communicationMethod = this._contactCommunicationService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("ContactId == " + (object) docJob.ContactId, new object[1]
            {
              (object) new DynamicFilter("IsPrimaryEmail == 1", new object[0])
            })
          }, (string) null).FirstOrDefault<ContactCommunicationMethod>();
          int num1 = this._baseMessageService.Save(new BaseMessage()
          {
            Body = "Your document is ready for printing. Please click <a href=\"" + ConfigurationManager.AppSettings["DocumentDownloadPath"] + (object) wordDocument.DocumentByteId + "\">here</a> to download.",
            Subject = "Document Ready for Printing",
            From = ConfigurationManager.AppSettings["DocumentServiceEmailFrom"],
            Complete = false
          });
          this._messageJobService.Save(new MessageJob()
          {
            ContactId = docJob.ContactId,
            Address = communicationMethod == null ? (string) null : communicationMethod.CommunicationValue,
            BaseMessageId = num1,
            CommunicationTypeId = 1,
            CommunicationStatusTypeId = 1
          });
          return TaskResult.No_Error;
        }
        catch (Exception ex)
        {
          docJob.Status = new int?(3);
          docJob.Active = new bool?(false);
          this._documentJobService.Save(docJob);
          this._errorLogging.Log(ex);
          return TaskResult.Error_Occurred;
        }
      }
      catch (Exception ex)
      {
        this._errorLogging.Log(ex);
        return TaskResult.Error_Occurred;
      }
    }

    private bool SendEmailMessage(string fromAddress, string toAddress, string subject, string body, bool isHtml, bool AllowRetries, List<Attachment> emailAttachments)
    {
      if (fromAddress == string.Empty)
        throw new Exception("The sending email address is required.");
      if (toAddress == string.Empty)
        throw new Exception("The receiving email address is required.");
      if (subject == string.Empty)
        throw new Exception("The subject of the email is required.");
      if (body == string.Empty)
        throw new Exception("The body of the email is required.");
      try
      {
        IBaseMessageService instance1 = ObjectFactory.GetInstance<IBaseMessageService>();
        IMessageJobService instance2 = ObjectFactory.GetInstance<IMessageJobService>();
        int baseMessageId = instance1.Save(new BaseMessage()
        {
          ApplicationId = new int?(1),
          Body = body,
          Complete = false,
          From = fromAddress,
          LastModifiedBy = "MDEService",
          LastModifiedDate = DateTime.Now,
          RequestDateTime = new DateTime?(DateTime.Now),
          Subject = subject
        });
        if (baseMessageId <= 0)
          throw new Exception("An error occurred attempting to save the Base Message.");
        BaseMessage byId1 = instance1.GetById(baseMessageId);
        int messageJobId = instance2.Save(new MessageJob()
        {
          BaseMessageId = baseMessageId,
          Address = toAddress,
          CommunicationTypeId = 1,
          LastModifiedBy = "MDEService",
          LastModifiedDate = DateTime.Now
        });
        if (messageJobId <= 0)
          throw new Exception("An error occurred attempting to save the Message Job.");
        MessageJob byId2 = instance2.GetById(messageJobId);
        Communications.CommunicationStatusTypes communicationStatusTypes = new EmailService(this._errorLogging).SendMessage(byId1, byId2, true, emailAttachments);
        if (communicationStatusTypes == Communications.CommunicationStatusTypes.SuccessfullySent)
        {
          byId2.CommunicationStatusTypeId = new int?((int) communicationStatusTypes);
          byId2.LastModifiedBy = "MDEService";
          byId2.LastModifiedDate = DateTime.Now;
          byId2.SentDateTime = new DateTime?(DateTime.Now);
          instance2.Save(byId2);
          byId1.LastModifiedBy = "MDEService";
          byId1.LastModifiedDate = DateTime.Now;
          byId1.Complete = true;
          instance1.Save(byId1);
          return true;
        }
        byId2.CommunicationStatusTypeId = new int?((int) communicationStatusTypes);
        byId2.LastModifiedBy = "MDEService";
        byId2.LastModifiedDate = DateTime.Now;
        byId2.FailedAttempts = new int?(1);
        instance2.Save(byId2);
        if (!AllowRetries)
        {
          byId1.LastModifiedBy = "MDEService";
          byId1.LastModifiedDate = DateTime.Now;
          byId1.Complete = true;
          instance1.Save(byId1);
        }
      }
      catch (Exception ex)
      {
        this._errorLogging.Log(ex);
      }
      return false;
    }

    public WordDocument GenerateDocumentGroup(IEnumerable<LU_Template> templates, IEnumerable<int> permitIds, string baseFileName, bool saveToFileSystem = false)
    {
      this._documentErrors.Clear();
      try
      {
        int num1 = 1;
        List<string> stringList = new List<string>();
        List<byte[]> numArrayList = new List<byte[]>();
        string str1 = string.Format("{0}{1}-temp.docx", (object) this.DocumentFolder, (object) DateTime.Now.ToString("yyyyMMddhhmm"));
        List<LU_Template> source = new List<LU_Template>();
        foreach (int permitId in permitIds)
        {
          Permit byId = this._permitService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties, PermitConditions");
          if (byId != null)
          {
            PermitCondition permitCondition = byId.PermitConditions.FirstOrDefault<PermitCondition>((Func<PermitCondition, bool>) (c =>
            {
              int? reportingPeriodId = c.ConditionReportingPeriodId;
              int num;
              if ((reportingPeriodId.GetValueOrDefault() != 1 ? 0 : (reportingPeriodId.HasValue ? 1 : 0)) != 0)
              {
                int? standardConditionTypeId = c.StandardConditionTypeId;
                num = standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0);
              }
              else
                num = 0;
              return num != 0;
            }));
            if (permitCondition != null)
            {
              int? pumpageReportTypeId = permitCondition.PumpageReportTypeId;
              if ((pumpageReportTypeId.GetValueOrDefault() != 2 ? 0 : (pumpageReportTypeId.HasValue ? 1 : 0)) != 0)
              {
                source.Clear();
                source.Add(this._templateService.GetById(53, (string) null));
              }
              else
                source = templates.ToList<LU_Template>();
            }
            else
              source = templates.ToList<LU_Template>();
            foreach (byte[] buffer in source.Select<LU_Template, byte[]>((Func<LU_Template, byte[]>) (template => File.ReadAllBytes(this.TemplateFolder + template.FileName))))
            {
              using (MemoryStream memoryStream = new MemoryStream())
              {
                memoryStream.Write(buffer, 0, buffer.Length);
                using (WordprocessingDocument documentFromTemplate = this._wordHelper.GetDocumentFromTemplate(memoryStream))
                {
                  IEnumerable<string> contentControlTags = (IEnumerable<string>) this._wordHelper.GetAllContentControlTags(documentFromTemplate);
                  WordDocument wordDocument = new WordDocument();
                  IDictionary<string, object> templateVariables = this._documentUtility.GetPermitTemplateVariables(contentControlTags, byId, (List<string>) null, (string) null, 0, false);
                  this._wordHelper.ReplaceContentControls(documentFromTemplate, templateVariables, false);
                  documentFromTemplate.Close();
                  wordDocument.SaveToFileSystem = false;
                  wordDocument.SaveToLocation = string.Format("{0}{1}-{2}.docx", (object) this.DocumentFolder, (object) byId.Id, (object) num1);
                  this.SaveToFileSystem(wordDocument.SaveToLocation, memoryStream.ToArray());
                }
                stringList.Add(string.Format("{0}-{1}.docx", (object) byId.Id, (object) num1));
                numArrayList.Add(memoryStream.ToArray());
              }
              ++num1;
            }
          }
        }
        File.Copy(this.TemplateFolder + "blank.docx", str1);
        string str2 = string.Format("{0}_{1}_{2}.docx", (object) baseFileName, (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm"));
        foreach (string str3 in stringList)
          File.Delete(this.DocumentFolder + str3);
        File.Delete(str1);
        byte[] buffer1 = this._wordHelper.OpenAndCombine((IList<byte[]>) numArrayList);
        DocumentByte entity = new DocumentByte()
        {
          Document = buffer1,
          MimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        };
        WordDocument wordDocument1 = new WordDocument()
        {
          Title = str2,
          DocumentByteId = this._documentByteService.Save(entity)
        };
        if (saveToFileSystem)
          this.SaveToFileSystem(this.DocumentFolder + str2, buffer1);
        return wordDocument1;
      }
      catch (Exception ex)
      {
        this._documentErrors.Add(ex.Message);
        this._errorLogging.Log(ex);
        return (WordDocument) null;
      }
    }

    private static void AddFileToZip(string zipName, string fileToAdd)
    {
      ZipArchiveMode mode = File.Exists(zipName) ? ZipArchiveMode.Update : ZipArchiveMode.Create;
      using (ZipArchive destination = ZipFile.Open(zipName, mode))
        destination.CreateEntryFromFile(fileToAdd, Path.GetFileName(fileToAdd), CompressionLevel.Fastest);
    }

    private void SaveToFileSystem(string fileName, byte[] buffer)
    {
      try
      {
        File.WriteAllBytes(fileName, buffer);
      }
      catch (Exception ex)
      {
        this._errorLogging.Log(ex);
      }
    }

    public delegate void MessageEventHandler(string CallingFunction, string Message, bool Verbose);
  }
}
