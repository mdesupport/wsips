﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.NonPollingTask
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Communication.Service.Services;
using MDGov.MDE.Communication.WindowsService.Helpers;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Reflection;

namespace MDGov.MDE.Communication.WindowsService
{
  public class NonPollingTask : INonPollingTask
  {
    private IErrorLogging _errorLogging;

    public NonPollingTaskTypes NonPollingTaskType { get; set; }

    public event NonPollingTask.MessageEventHandler MessageHandler;

    public NonPollingTask(int intervalMultiplier, IErrorLogging errorLogging)
      : base(intervalMultiplier)
    {
      this._errorLogging = errorLogging;
    }

    public override TaskResult PerformTask()
    {
      TaskResult taskResult = TaskResult.No_Error;
      if (this._inProcess)
        return TaskResult.Process_Busy;
      try
      {
        this._inProcess = true;
        this.SendMessage(MethodBase.GetCurrentMethod().Name, this.NonPollingTaskType.ToString(), false);
        if (this.NonPollingTaskType == NonPollingTaskTypes.GenerateConditionCompliance)
          taskResult = this.GenerateConditionCompliance();
      }
      catch (Exception ex)
      {
        if (this.MessageHandler != null)
          this.SendMessage(MethodBase.GetCurrentMethod().Name, this.BuildErrorMessage(ex), false);
        taskResult = TaskResult.Error_Occurred;
      }
      finally
      {
        if (taskResult == TaskResult.No_Error)
          this._lastTaskTime = DateTime.Now;
        this._inProcess = false;
      }
      return taskResult;
    }

    private TaskResult GenerateConditionCompliance()
    {
      SqlConnection connection = (SqlConnection) null;
      try
      {
        connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MdeWaterPermitting"].ToString());
        connection.Open();
        SqlCommand sqlCommand = new SqlCommand("sp_GenerateConditionComplianceRecords", connection);
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.Parameters.Add(new SqlParameter("@today", SqlDbType.DateTime, 8));
        sqlCommand.Parameters[0].Value = (object) Convert.ToDateTime(DateTime.Now.ToShortDateString());
        sqlCommand.ExecuteNonQuery();
        return TaskResult.No_Error;
      }
      catch (Exception ex)
      {
        return TaskResult.Error_Occurred;
      }
      finally
      {
        if (connection != null && connection.State != ConnectionState.Closed)
          connection.Close();
      }
    }

    private bool SendEmailMessage(string fromAddress, string toAddress, string subject, string body, bool isHtml, bool AllowRetries, List<Attachment> emailAttachments)
    {
      if (fromAddress == string.Empty)
        throw new Exception("The sending email address is required.");
      if (toAddress == string.Empty)
        throw new Exception("The receiving email address is required.");
      if (subject == string.Empty)
        throw new Exception("The subject of the email is required.");
      if (body == string.Empty)
        throw new Exception("The body of the email is required.");
      try
      {
        IBaseMessageService instance1 = ObjectFactory.GetInstance<IBaseMessageService>();
        IMessageJobService instance2 = ObjectFactory.GetInstance<IMessageJobService>();
        int baseMessageId = instance1.Save(new BaseMessage()
        {
          ApplicationId = new int?(1),
          Body = body,
          Complete = false,
          From = fromAddress,
          LastModifiedBy = "MDEService",
          LastModifiedDate = DateTime.Now,
          RequestDateTime = new DateTime?(DateTime.Now),
          Subject = subject
        });
        if (baseMessageId <= 0)
          throw new Exception("An error occurred attempting to save the Base Message.");
        BaseMessage byId1 = instance1.GetById(baseMessageId);
        int messageJobId = instance2.Save(new MessageJob()
        {
          BaseMessageId = baseMessageId,
          Address = toAddress,
          CommunicationTypeId = 1,
          LastModifiedBy = "MDEService",
          LastModifiedDate = DateTime.Now
        });
        if (messageJobId <= 0)
          throw new Exception("An error occurred attempting to save the Message Job.");
        MessageJob byId2 = instance2.GetById(messageJobId);
        Communications.CommunicationStatusTypes communicationStatusTypes = new EmailService(this._errorLogging).SendMessage(byId1, byId2, true, emailAttachments);
        if (communicationStatusTypes == Communications.CommunicationStatusTypes.SuccessfullySent)
        {
          byId2.CommunicationStatusTypeId = new int?((int) communicationStatusTypes);
          byId2.LastModifiedBy = "MDEService";
          byId2.LastModifiedDate = DateTime.Now;
          byId2.SentDateTime = new DateTime?(DateTime.Now);
          instance2.Save(byId2);
          byId1.LastModifiedBy = "MDEService";
          byId1.LastModifiedDate = DateTime.Now;
          byId1.Complete = true;
          instance1.Save(byId1);
          return true;
        }
        byId2.CommunicationStatusTypeId = new int?((int) communicationStatusTypes);
        byId2.LastModifiedBy = "MDEService";
        byId2.LastModifiedDate = DateTime.Now;
        byId2.FailedAttempts = new int?(1);
        instance2.Save(byId2);
        if (!AllowRetries)
        {
          byId1.LastModifiedBy = "MDEService";
          byId1.LastModifiedDate = DateTime.Now;
          byId1.Complete = true;
          instance1.Save(byId1);
        }
      }
      catch (Exception ex)
      {
        this._errorLogging.Log(ex);
      }
      return false;
    }

    private void SendMessage(string callingFunction, string message, bool verbose)
    {
      if (message == null)
        return;
      this.MessageHandler(callingFunction, message, verbose);
    }

    public delegate void MessageEventHandler(string CallingFunction, string Message, bool Verbose);
  }
}
