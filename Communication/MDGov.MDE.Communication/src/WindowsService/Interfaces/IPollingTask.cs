﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.IPollingTask
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using System;

namespace MDGov.MDE.Communication.WindowsService
{
  public abstract class IPollingTask : Task
  {
    protected DateTime _startTime;
    protected DateTime _stopTime;
    protected int _interval;

    public DateTime StartTime
    {
      get
      {
        return this._startTime;
      }
      set
      {
        this._startTime = value;
      }
    }

    public DateTime StopTime
    {
      get
      {
        return this._stopTime;
      }
      set
      {
        this._stopTime = value;
      }
    }

    public int Interval
    {
      get
      {
        return this._interval;
      }
      set
      {
        this._interval = value;
      }
    }

    public IPollingTask(int intervalMultiplier)
      : base(intervalMultiplier)
    {
    }

    public override bool ReadyForTask()
    {
      return this._startTime.TimeOfDay < DateTime.Now.TimeOfDay && DateTime.Now.TimeOfDay < this._stopTime.TimeOfDay && (this._lastTaskTime == DateTime.MinValue || this._lastTaskTime.AddMilliseconds((double) this._interval) < DateTime.Now);
    }
  }
}
