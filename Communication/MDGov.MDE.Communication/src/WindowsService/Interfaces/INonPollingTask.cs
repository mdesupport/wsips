﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.INonPollingTask
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using System;

namespace MDGov.MDE.Communication.WindowsService
{
  public abstract class INonPollingTask : Task
  {
    public DateTime StartTime { get; set; }

    public INonPollingTask(int intervalMultiplier)
      : base(intervalMultiplier)
    {
    }

    public override bool ReadyForTask()
    {
      int num = this._lastTaskTime.DayOfYear;
      if (this._lastTaskTime.DayOfYear > DateTime.Now.DayOfYear)
        num = 0;
      return DateTime.Now.DayOfYear > num && this.StartTime.TimeOfDay <= DateTime.Now.TimeOfDay;
    }
  }
}
