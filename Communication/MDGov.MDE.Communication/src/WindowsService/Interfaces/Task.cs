﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.Task
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using System;
using System.Text;

namespace MDGov.MDE.Communication.WindowsService
{
  public abstract class Task
  {
    protected DateTime _lastTaskTime;
    protected int _intervalMultipler;
    protected bool _inProcess;

    public DateTime LastTaskTime
    {
      get
      {
        return this._lastTaskTime;
      }
      set
      {
        this._lastTaskTime = value;
      }
    }

    public bool InProcess
    {
      get
      {
        return this._inProcess;
      }
    }

    public Task(int intervalMultiplier)
    {
      this._intervalMultipler = intervalMultiplier;
    }

    public abstract TaskResult PerformTask();

    public abstract bool ReadyForTask();

    protected string BuildErrorMessage(Exception ex)
    {
      StringBuilder stringBuilder = new StringBuilder();
      for (Exception exception = ex; exception != null; exception = exception.InnerException)
        stringBuilder.AppendLine(exception.Message);
      stringBuilder.AppendLine();
      stringBuilder.AppendLine(ex.StackTrace);
      return stringBuilder.ToString();
    }
  }
}
