﻿using System;

namespace MDGov.MDE.Communication.WindowsService
{    
    public interface ITaskProcessor
    {
        TaskResult Process();
    }
}