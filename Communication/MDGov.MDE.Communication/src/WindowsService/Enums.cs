﻿namespace MDGov.MDE.Communication.WindowsService
{

    public enum LogLevelTypes
    {
        None = 0,
        Default = 1,
        Verbose = 2
    }

    public enum NonPollingTaskTypes
    {
        None = 0,
        GenerateConditionCompliance = 1,
    }
    
    public enum ParseResult
    {
        Success = 0,
        Failure = 1
    }

    public enum PollingTaskTypes
    {
        None = 0,
        ProcessLoggedMessages = 1,
        GenerateDocument = 3
    }

    public enum TaskResult
    {
        No_Error = 0,
        Error_Occurred = 1,
        Process_Busy = 2
    }
}
