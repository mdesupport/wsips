﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Services.IBaseMessageService
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Communication.Model;
using System.Collections.Generic;
using System.ServiceModel;

namespace MDGov.MDE.Communication.Service.Services
{
  [ServiceContract]
  public interface IBaseMessageService
  {
    List<BaseMessage> GetCurrentBaseMessages();

    int Save(BaseMessage model);

    BaseMessage GetById(int baseMessageId);
  }
}
