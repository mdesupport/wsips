﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Services.MessageJobService
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using System.Collections.Generic;

namespace MDGov.MDE.Communication.Service.Services
{
  public class MessageJobService : IMessageJobService
  {
    private IRepository<MessageJob> _repository;

    public MessageJobService(IRepository<MessageJob> repository)
    {
      this._repository = repository;
    }

    public int Save(MessageJob model)
    {
      return this._repository.Save(model);
    }

    public MessageJob GetById(int messageJobId)
    {
      return this._repository.GetById(messageJobId, (string) null);
    }

    public List<MessageJob> GetByBaseMessageId(int baseMessageid, bool includeSent)
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (includeSent)
      {
        dynamicFilterList.Add(new DynamicFilter("BaseMessageId == @0", new object[1]
        {
          (object) baseMessageid
        }));
        return new List<MessageJob>((IEnumerable<MessageJob>) this._repository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) dynamicFilterList, (string) null));
      }
      dynamicFilterList.Add(new DynamicFilter("BaseMessageId == @0", new object[1]
      {
        (object) baseMessageid
      }));
      dynamicFilterList.Add(new DynamicFilter("SentDateTime == null", new object[0]));
      dynamicFilterList.Add(new DynamicFilter("(FailedAttempts == null ? 0 : FailedAttempts) < 3", new object[0]));
      return new List<MessageJob>((IEnumerable<MessageJob>) this._repository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) dynamicFilterList, (string) null));
    }
  }
}
