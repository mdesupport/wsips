﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.Services.DocumentService
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.Communication.WindowsService.Services
{
  public class DocumentService : IDocumentService
  {
    private readonly IRepository<Permit> _permitRepository;
    private readonly IRepository<PermitContact> _permitContactRepository;
    private readonly IRepository<LU_Template> _templateRepository;
    private readonly IRepository<DocumentByte> _documentByteRepository;
    private readonly IRepository<DocumentJob> _documentJobRepository;
    private readonly IRepository<BaseMessage> _baseMessageRepository;
    private readonly IRepository<MessageJob> _messageJobRepository;
    private readonly IRepository<ContactCommunicationMethod> _contactCommunicationMethodRepository;

    public DocumentService(IRepository<Permit> permitRepository, IRepository<PermitContact> permitContactRepository, IRepository<LU_Template> templateRepository, IRepository<DocumentByte> documentByteRepository, IRepository<DocumentJob> documentJobRepository, IRepository<BaseMessage> baseMessageRepository, IRepository<MessageJob> messageJobRepository, IRepository<ContactCommunicationMethod> contactCommunicationMethodRepository)
    {
      this._permitRepository = permitRepository;
      this._permitContactRepository = permitContactRepository;
      this._templateRepository = templateRepository;
      this._documentByteRepository = documentByteRepository;
      this._documentJobRepository = documentJobRepository;
      this._baseMessageRepository = baseMessageRepository;
      this._messageJobRepository = messageJobRepository;
      this._contactCommunicationMethodRepository = contactCommunicationMethodRepository;
    }

    public Permit GetPermitById(int id, string includeProperties)
    {
      return this._permitRepository.GetById(id, includeProperties);
    }

    public LU_Template GetTemplateById(int id, string includeProperties)
    {
      return this._templateRepository.GetById(id, includeProperties);
    }

    public IEnumerable<Permit> GetPermitsRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Permit>) this._permitRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public IEnumerable<PermitContact> GetPermitContactsRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitContact>) this._permitContactRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public IEnumerable<DocumentJob> GetDocumentJobRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<DocumentJob>) this._documentJobRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public IEnumerable<ContactCommunicationMethod> GetCommunicationMethodRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<ContactCommunicationMethod>) this._contactCommunicationMethodRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Save(DocumentByte model)
    {
      return this._documentByteRepository.Save(model);
    }

    public int Save(DocumentJob model)
    {
      return this._documentJobRepository.Save(model);
    }

    public int Save(BaseMessage model)
    {
      return this._baseMessageRepository.Save(model);
    }

    public int Save(MessageJob model)
    {
      return this._messageJobRepository.Save(model);
    }
  }
}
