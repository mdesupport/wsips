﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Services.EmailService
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Communication.WindowsService.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace MDGov.MDE.Communication.Service.Services
{
  public class EmailService
  {
    private IErrorLogging _errorLogging;

    public EmailService(IErrorLogging errorLogging)
    {
      this._errorLogging = errorLogging;
    }

    public Communications.CommunicationStatusTypes SendMessage(BaseMessage baseMessage, MessageJob messageJob, bool isHTML)
    {
      try
      {
        return this.Send(baseMessage.From, messageJob.Address, "", "", baseMessage.Subject, baseMessage.Body, isHTML, (List<Attachment>) null);
      }
      catch (Exception ex)
      {
        this._errorLogging.Log(ex);
        return Communications.CommunicationStatusTypes.UnknownFailure;
      }
    }

    public Communications.CommunicationStatusTypes SendMessage(BaseMessage baseMessage, MessageJob messageJob, bool isHTML, List<Attachment> emailAttachments)
    {
      try
      {
        return this.Send(baseMessage.From, messageJob.Address, "", "", baseMessage.Subject, baseMessage.Body, isHTML, emailAttachments);
      }
      catch (Exception ex)
      {
        this._errorLogging.Log(ex);
        return Communications.CommunicationStatusTypes.UnknownFailure;
      }
    }

    private Communications.CommunicationStatusTypes Send(string from, string to, string cc, string bcc, string subject, string body, bool isHtml, List<Attachment> emailAttachments)
    {
      string[] tos = new string[0];
      string[] ccs = new string[0];
      string[] strArray = new string[0];
      using (MailMessage message = new MailMessage(from, to))
      {
        message.Subject = subject;
        message.Body = body;
        message.IsBodyHtml = isHtml;
        if (!string.IsNullOrEmpty(cc))
          ccs = cc.Split(new char[1]{ ',' }, StringSplitOptions.RemoveEmptyEntries);
        if (!string.IsNullOrEmpty(bcc))
          strArray = bcc.Split(new char[1]{ ',' }, StringSplitOptions.RemoveEmptyEntries);
        foreach (string address in ((IEnumerable<string>) ccs).Where<string>((Func<string, bool>) (item => !EmailService.IsPresent(item, (IEnumerable<string>) tos))))
          message.CC.Add(new MailAddress(address));
        foreach (string address in ((IEnumerable<string>) strArray).Where<string>((Func<string, bool>) (item => !EmailService.IsPresent(item, (IEnumerable<string>) tos) && !EmailService.IsPresent(item, (IEnumerable<string>) ccs))))
          message.Bcc.Add(new MailAddress(address));
        if (emailAttachments != null)
        {
          foreach (Attachment emailAttachment in emailAttachments)
            message.Attachments.Add(emailAttachment);
        }
        try
        {
          using (SmtpClient smtpClient = new SmtpClient())
          {
            smtpClient.Send(message);
            return Communications.CommunicationStatusTypes.SuccessfullySent;
          }
        }
        catch (SmtpFailedRecipientException ex)
        {
          this._errorLogging.Log((Exception) ex);
          return Communications.CommunicationStatusTypes.UnknownRecepient;
        }
        catch (Exception ex)
        {
          this._errorLogging.Log(ex);
          return Communications.CommunicationStatusTypes.UnknownFailure;
        }
      }
    }

    private static bool IsPresent(string mail, IEnumerable<string> mails)
    {
      return mails.Any<string>((Func<string, bool>) (item => item == mail));
    }
  }
}
