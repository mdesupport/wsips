﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.Services.IDocumentService
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.Communication.WindowsService.Services
{
  public interface IDocumentService
  {
    Permit GetPermitById(int id, string includeProperties);

    LU_Template GetTemplateById(int id, string includeProperties);

    IEnumerable<Permit> GetPermitsRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties);

    IEnumerable<PermitContact> GetPermitContactsRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties);

    IEnumerable<DocumentJob> GetDocumentJobRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties);

    int Save(DocumentByte model);

    int Save(DocumentJob model);

    int Save(BaseMessage model);

    int Save(MessageJob model);
  }
}
