﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.Service.Services.BaseMessageService
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Communication.Service.Services
{
  public class BaseMessageService : IBaseMessageService
  {
    private IRepository<BaseMessage> _repository;

    public BaseMessageService(IRepository<BaseMessage> repository)
    {
      this._repository = repository;
    }

    public BaseMessage GetById(int baseMessageId)
    {
      return this._repository.GetById(baseMessageId, (string) null);
    }

    public List<BaseMessage> GetCurrentBaseMessages()
    {
      return new List<BaseMessage>((IEnumerable<BaseMessage>) this._repository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new List<DynamicFilter>()
      {
        new DynamicFilter("Complete == false", new object[0]),
        new DynamicFilter("(RequestDateTime == null ? @0 : RequestDateTime) <= @0", new object[1]
        {
          (object) DateTime.Now
        })
      }, (string) null));
    }

    public int Save(BaseMessage model)
    {
      return this._repository.Save(model);
    }
  }
}
