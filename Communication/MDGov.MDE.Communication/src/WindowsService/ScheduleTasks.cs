﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.WindowsService.ScheduleTasks
// Assembly: MDEWindowsService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A8A35F3-75C1-452A-94B0-A4193BA5C1EA
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\Website Backups\Prod\WSIPS_Prod_backup_20170509\WSIPS\Services\MDEWindowsService\MDEWindowsService.exe

using MDGov.MDE.Common.Logging.Interface;
using StructureMap;
using StructureMap.Pipeline;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace MDGov.MDE.Communication.WindowsService
{
  public class ScheduleTasks
  {
    private const int INTERVAL_MULTIPLIER = 1000;
    private const int STARTUP_TIME = 100;
    private LogLevelTypes _logLevel;
    private string _loggingDirectory;
    private Timer _logTimer;
    private int _loggingInterval;
    private ConcurrentQueue<ScheduleTasks.QueuedMessage> _messageQueue;
    private bool _allowMultipleProcesses;
    private int _numberOfProcesses;
    private PollingTask[] _pollingTasks;
    private bool _pollingEnabled;
    private int _pollingInterval;
    private Timer _pollingTimer;
    private NonPollingTask[] _nonPollingTasks;
    private bool _nonPollingEnabled;
    private int _nonPollingInterval;
    private Timer _nonPollingTimer;

    public bool PollingEnabled
    {
      get
      {
        return this._pollingEnabled;
      }
    }

    public bool NonPollingEnabled
    {
      get
      {
        return this._nonPollingEnabled;
      }
    }

    public ScheduleTasks()
    {
      this.GetConfiguration();
      this.SetupLogPolling();
      this._messageQueue = new ConcurrentQueue<ScheduleTasks.QueuedMessage>();
      if (this._pollingEnabled)
        this.SetupPollingTasks();
      if (!this._nonPollingEnabled)
        return;
      this.SetupNonPollingTasks();
    }

    private void GetConfiguration()
    {
      try
      {
        this._loggingDirectory = ConfigurationManager.AppSettings["LoggingDirectory"];
        switch (ConfigurationManager.AppSettings["LogLevel"].ToLower())
        {
          case "verbose":
            this._logLevel = LogLevelTypes.Verbose;
            break;
          case "default":
            this._logLevel = LogLevelTypes.Default;
            break;
          case "none":
            this._logLevel = LogLevelTypes.None;
            break;
          default:
            this._logLevel = LogLevelTypes.Default;
            break;
        }
        int result1;
        if (!int.TryParse(ConfigurationManager.AppSettings["LogPollingInterval"], out result1))
          throw new Exception("The 'LogPollingInterval' value in the configuration file is missing or invalid.");
        this._loggingInterval = result1 * 1000;
        bool result2;
        if (!bool.TryParse(ConfigurationManager.AppSettings["AllowMultipleProcesses"], out result2))
          throw new Exception("The 'AllowMultipleProcesses' value in the configuration file is missing or invalid.");
        this._allowMultipleProcesses = result2;
        if (!bool.TryParse(ConfigurationManager.AppSettings["PollingTasksEnabled"], out result2))
          throw new Exception("The 'PollingTasksEnabled' value in the configuration file is missing or invalid.");
        this._pollingEnabled = result2;
        if (!int.TryParse(ConfigurationManager.AppSettings["PollingInterval"], out result1))
          throw new Exception("The 'PollingInterval' value in the configuration file is missing or invalid.");
        this._pollingInterval = result1 * 1000;
        if (!bool.TryParse(ConfigurationManager.AppSettings["NonPollingTasksEnabled"], out result2))
          throw new Exception("The 'NonPollingTasksEnabled' value in the configuration file is missing or invalid.");
        this._nonPollingEnabled = result2;
        if (!int.TryParse(ConfigurationManager.AppSettings["NonPollingInterval"], out result1))
          throw new Exception("The 'NonPollingInterval' value in the configuration file is missing or invalid.");
        this._nonPollingInterval = result1 * 1000;
      }
      catch (Exception ex)
      {
        throw new Exception("An error occurred attempting to load the application settings from the configuration file.", ex);
      }
    }

    private void SetupLogPolling()
    {
      if (!Directory.Exists(this._loggingDirectory))
        Directory.CreateDirectory(this._loggingDirectory);
      this._logTimer = new Timer(new TimerCallback(this.WriteLog), (object) this, -1, -1);
      this._logTimer.Change(100, this._loggingInterval);
    }

    private void WriteLog(object sender)
    {
      string path = this._loggingDirectory.TrimEnd('\\') + (object) '\\' + ConfigurationManager.AppSettings["LogFileName"].Replace("%DATE%", DateTime.Today.ToString("yyyy-MM-dd"));
      if (!File.Exists(path))
        File.Create(path);
      try
      {
        StringBuilder stringBuilder = new StringBuilder();
        ScheduleTasks.QueuedMessage result;
        while (this._messageQueue.TryDequeue(out result))
        {
          if (string.IsNullOrEmpty(result.Message))
          {
            result.Message = "EMPTY_MESSAGE";
            result.CallingFunction = "EMPTY_CALLINGFUNCTION";
          }
          switch (this._logLevel)
          {
            case LogLevelTypes.Default:
              if (!result.Verbose)
              {
                stringBuilder.Append(this._messageQueue.Count.ToString() + " -- " + this.BuildMessage(result));
                break;
              }
              break;
            case LogLevelTypes.Verbose:
              stringBuilder.Append(this._messageQueue.Count.ToString() + " -- " + this.BuildMessage(result));
              break;
          }
        }
        if (stringBuilder.Length <= 0)
          return;
        File.AppendAllText(path, stringBuilder.ToString());
      }
      catch (Exception ex)
      {
        EventLog.WriteEntry("MDE_SERVICE", ex.Message);
      }
    }

    private void SetupPollingTasks()
    {
      try
      {
        NameValueCollection nameValueCollection = new NameValueCollection();
        NameValueCollection section = (NameValueCollection) ConfigurationManager.GetSection("Schedule/PollingTasks");
        if (section != null)
        {
          Array.Resize<PollingTask>(ref this._pollingTasks, section.Count);
          for (int index = 0; index <= section.Count - 1; ++index)
          {
            PollingTask instance = ObjectFactory.GetInstance<PollingTask>(new ExplicitArguments((IDictionary<string, object>) new Dictionary<string, object>()
            {
              {
                "intervalMultiplier",
                (object) 1000
              }
            }));
            instance.Message += new PollingTask.MessageEventHandler(this.LogMessage);
            switch (section.Keys[index].ToLower())
            {
              case "processloggedmessages":
                instance.PollingTaskType = PollingTaskTypes.ProcessLoggedMessages;
                break;
              case "generatedocument":
                instance.PollingTaskType = PollingTaskTypes.GenerateDocument;
                break;
            }
            instance.LastTaskTime = DateTime.MinValue;
            string str1 = section[index];
            char[] chArray1 = new char[1]{ ';' };
            foreach (string str2 in str1.Split(chArray1))
            {
              char[] chArray2 = new char[1]{ '=' };
              string[] strArray = str2.Split(chArray2);
              DateTime result1;
              switch (strArray[0].ToLower())
              {
                case "starttime":
                  if (!DateTime.TryParse(strArray[1], out result1))
                    throw new Exception("An illegal start time has been found.");
                  instance.StartTime = result1;
                  break;
                case "stoptime":
                  if (!DateTime.TryParse(strArray[1], out result1))
                    throw new Exception("An illegal stop time has been found.");
                  instance.StopTime = result1;
                  break;
                case "pollinginterval":
                  int result2;
                  if (!int.TryParse(strArray[1], out result2))
                    throw new Exception("An illegal polling interval has been found.");
                  instance.Interval = result2 * 1000;
                  break;
              }
            }
            this._pollingTasks[index] = instance;
          }
        }
        if (this._pollingTasks.GetLongLength(0) <= 0L)
          return;
        this._pollingTimer = new Timer(new TimerCallback(this.Service_Polling), (object) this, -1, -1);
      }
      catch (Exception ex)
      {
        throw new Exception("An error occurred attempting to load the data tasks from the configuration file.", ex);
      }
    }

    private void SetupNonPollingTasks()
    {
      try
      {
        NameValueCollection nameValueCollection = new NameValueCollection();
        NameValueCollection section = (NameValueCollection) ConfigurationManager.GetSection("Schedule/NonPollingTasks");
        if (section != null)
        {
          Array.Resize<NonPollingTask>(ref this._nonPollingTasks, section.Count);
          for (int index = 0; index <= section.Count - 1; ++index)
          {
            this.LogMessage(MethodBase.GetCurrentMethod().Name, "2", true);
            IErrorLogging instance = ObjectFactory.GetInstance<IErrorLogging>();
            this.LogMessage(MethodBase.GetCurrentMethod().Name, "3", true);
            NonPollingTask nonPollingTask = new NonPollingTask(1000, instance);
            this.LogMessage(MethodBase.GetCurrentMethod().Name, "4", true);
            nonPollingTask.MessageHandler += new NonPollingTask.MessageEventHandler(this.LogMessage);
            this.LogMessage(MethodBase.GetCurrentMethod().Name, "5", true);
            switch (section.Keys[index].ToLower())
            {
              case "generateconditioncompliance":
                nonPollingTask.NonPollingTaskType = NonPollingTaskTypes.GenerateConditionCompliance;
                nonPollingTask.LastTaskTime = DateTime.MinValue;
                string str1 = section[index];
                char[] chArray1 = new char[1]{ ';' };
                foreach (string str2 in str1.Split(chArray1))
                {
                  char[] chArray2 = new char[1]{ '=' };
                  string[] strArray = str2.Split(chArray2);
                  switch (strArray[0].ToLower())
                  {
                    case "processtime":
                      DateTime result;
                      if (!DateTime.TryParse(strArray[1], out result))
                        throw new Exception("An illegal start time has been found.");
                      nonPollingTask.StartTime = result;
                      break;
                  }
                }
                this._nonPollingTasks[index] = nonPollingTask;
                continue;
              default:
                throw new Exception("An illegal non-polling task has been found in the config file.");
            }
          }
        }
        if (this._nonPollingTasks.GetLongLength(0) <= 0L)
          return;
        this._nonPollingTimer = new Timer(new TimerCallback(this.Service_NonPolling), (object) this, -1, -1);
      }
      catch (Exception ex)
      {
        this.LogMessage(MethodBase.GetCurrentMethod().Name, "An error occurred attempting to load the non-polling tasks from the configuration file: " + (object) ex, false);
      }
    }

    public void StartServicePolling()
    {
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "DB Service is being started", false);
      this._pollingTimer.Change(100, this._pollingInterval);
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "DB Service has been started", false);
    }

    public void StopServicePolling()
    {
      this._pollingTimer.Change(-1, -1);
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "DB Service has been stopped", false);
    }

    private void Service_Polling(object sender)
    {
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "DB Polling starting", true);
      if (!this._allowMultipleProcesses && this._numberOfProcesses > 0)
      {
        this.LogMessage(MethodBase.GetCurrentMethod().Name, "Service is busy", true);
      }
      else
      {
        ++this._numberOfProcesses;
        try
        {
          foreach (PollingTask pollingTask in this._pollingTasks)
          {
            if (pollingTask.ReadyForTask())
            {
              this.LogMessage(MethodBase.GetCurrentMethod().Name, "Datatask fires", true);
              switch (pollingTask.PerformTask())
              {
                case TaskResult.No_Error:
                  this.LogMessage(MethodBase.GetCurrentMethod().Name, "Datatask returns 'No Error'", true);
                  break;
                case TaskResult.Error_Occurred:
                  this.LogMessage(MethodBase.GetCurrentMethod().Name, "Datatask returns 'Error'", true);
                  break;
                case TaskResult.Process_Busy:
                  this.LogMessage(MethodBase.GetCurrentMethod().Name, "Datatask returns 'Process Busy'", true);
                  break;
              }
            }
          }
        }
        catch
        {
        }
        finally
        {
          --this._numberOfProcesses;
        }
        this.LogMessage(MethodBase.GetCurrentMethod().Name, "Polling Service is complete", true);
      }
    }

    public void StartServiceNonPolling()
    {
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Service is being started", false);
      this._nonPollingTimer.Change(100, this._nonPollingInterval);
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Service has been started", false);
    }

    public void StopServiceNonPolling()
    {
      this._nonPollingTimer.Change(-1, -1);
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Service has been stopped", false);
    }

    private void Service_NonPolling(object sender)
    {
      this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Service firing", true);
      if (!this._allowMultipleProcesses && this._numberOfProcesses > 0)
      {
        this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Service is busy", true);
      }
      else
      {
        ++this._numberOfProcesses;
        try
        {
          foreach (NonPollingTask nonPollingTask in this._nonPollingTasks)
          {
            if (nonPollingTask.ReadyForTask())
            {
              this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Task fires - " + nonPollingTask.NonPollingTaskType.ToString(), true);
              switch (nonPollingTask.PerformTask())
              {
                case TaskResult.No_Error:
                  this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Task returns 'No Error'", true);
                  break;
                case TaskResult.Error_Occurred:
                  this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Task returns 'Error'", true);
                  break;
                case TaskResult.Process_Busy:
                  this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Task returns 'Process Busy'", true);
                  break;
              }
            }
          }
        }
        catch
        {
        }
        finally
        {
          --this._numberOfProcesses;
        }
        this.LogMessage(MethodBase.GetCurrentMethod().Name, "Non-Polling Service is complete", true);
      }
    }

    private void LogMessage(string CallingFunction, string Message, bool Verbose)
    {
      try
      {
        this._messageQueue.Enqueue(new ScheduleTasks.QueuedMessage()
        {
          CallingFunction = CallingFunction,
          Message = Message,
          Verbose = Verbose,
          MessageTime = DateTime.Now
        });
      }
      catch (Exception ex)
      {
        EventLog.WriteEntry("MDE_SERVICE", ex.Message);
      }
    }

    private string BuildMessage(ScheduleTasks.QueuedMessage message)
    {
      return message.MessageTime.ToString() + ": (" + message.CallingFunction + ") " + (message.Verbose ? "VERBOSE: " : "") + message.Message + "\r\n";
    }

    private struct QueuedMessage
    {
      public string CallingFunction;
      public string Message;
      public DateTime MessageTime;
      public bool Verbose;
    }
  }
}
