﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.BusinessLayer.Validators.BaseMessageValidator
// Assembly: MDGov.MDE.Communication.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E709A685-F327-424F-8C52-C907D020E611
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Communication.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Communication.BusinessLayer.Validators
{
  public class BaseMessageValidator : AbstractValidator<BaseMessage>
  {
    public BaseMessageValidator()
    {
      this.RuleFor<string>((Expression<Func<BaseMessage, string>>) (t => t.From)).NotEmpty<BaseMessage, string>().WithMessage<BaseMessage, string>("Message 'From' field cannot be empty.");
      this.RuleFor<string>((Expression<Func<BaseMessage, string>>) (t => t.Body)).NotEmpty<BaseMessage, string>().WithMessage<BaseMessage, string>("Message Body cannot be empty.");
      this.RuleFor<string>((Expression<Func<BaseMessage, string>>) (t => t.Subject)).NotEmpty<BaseMessage, string>().WithMessage<BaseMessage, string>("Message Subject cannot be empty.");
      this.RuleFor<string>((Expression<Func<BaseMessage, string>>) (t => t.LastModifiedBy)).NotEmpty<BaseMessage, string>();
    }
  }
}
