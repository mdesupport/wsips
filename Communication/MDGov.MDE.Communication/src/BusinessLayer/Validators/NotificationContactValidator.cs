﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.BusinessLayer.Validators.NotificationContactValidator
// Assembly: MDGov.MDE.Communication.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E709A685-F327-424F-8C52-C907D020E611
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Communication.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Communication.BusinessLayer.Validators
{
  public class NotificationContactValidator : AbstractValidator<NotificationContact>
  {
    public NotificationContactValidator()
    {
      this.RuleFor<string>((Expression<Func<NotificationContact, string>>) (t => t.LastModifiedBy)).NotEmpty<NotificationContact, string>();
    }
  }
}
