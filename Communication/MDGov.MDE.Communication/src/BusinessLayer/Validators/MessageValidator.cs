﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.BusinessLayer.Validators.MessageValidator
// Assembly: MDGov.MDE.Communication.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E709A685-F327-424F-8C52-C907D020E611
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Communication.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MDGov.MDE.Communication.BusinessLayer.Validators
{
  public class MessageValidator : AbstractValidator<Message>
  {
    public MessageValidator()
    {
      this.RuleFor<List<int>>((Expression<Func<Message, List<int>>>) (t => t.ContactIds)).NotNull<Message, List<int>>().NotEmpty<Message, List<int>>().WithMessage<Message, List<int>>("Message 'ContactIds' field cannot be null or empty");
      this.RuleFor<string>((Expression<Func<Message, string>>) (t => t.Subject)).NotEmpty<Message, string>().WithMessage<Message, string>("Message 'Subject' field cannot be null or empty");
      this.RuleFor<string>((Expression<Func<Message, string>>) (t => t.Body)).NotEmpty<Message, string>().WithMessage<Message, string>("Message 'Body' field cannot be null or empty");
      this.RuleFor<string>((Expression<Func<Message, string>>) (t => t.From)).NotEmpty<Message, string>().WithMessage<Message, string>("Message 'From' field cannot be null or empty");
      this.RuleFor<int>((Expression<Func<Message, int>>) (t => t.NotificationTypeId)).NotEmpty<Message, int>().WithMessage<Message, int>("Message 'NotificationTypeId' field must have a valid type value");
      this.RuleFor<string>((Expression<Func<Message, string>>) (t => t.NotificationText)).NotEmpty<Message, string>().WithMessage<Message, string>("Message 'NotificationText' field cannot be null or empty");
      this.RuleFor<string>((Expression<Func<Message, string>>) (t => t.LastModifiedBy)).NotEmpty<Message, string>();
    }
  }
}
