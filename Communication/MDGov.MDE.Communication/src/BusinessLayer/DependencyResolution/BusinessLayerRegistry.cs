﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Communication.BusinessLayer.DependencyResolution.BusinessLayerRegistry
// Assembly: MDGov.MDE.Communication.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E709A685-F327-424F-8C52-C907D020E611
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Communication.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Communication.BusinessLayer.Validators;
using MDGov.MDE.Communication.Model;
using StructureMap.Configuration.DSL;

namespace MDGov.MDE.Communication.BusinessLayer.DependencyResolution
{
  public class BusinessLayerRegistry : Registry
  {
    public BusinessLayerRegistry()
    {
      this.For<IValidator<Message>>().Use<MessageValidator>();
      this.For<IValidator<BaseMessage>>().Use<BaseMessageValidator>();
      this.For<IValidator<MessageJob>>().Use<MessageJobValidator>();
      this.For<IValidator<Notification>>().Use<NotificationValidator>();
      this.For<IValidator<NotificationContact>>().Use<NotificationContactValidator>();
    }
  }
}
