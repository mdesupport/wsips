﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.BusinessLayer.Validators.UserProfileValidator
// Assembly: MDGov.MDE.Membership.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E334AB81-1921-46AD-AF49-A0EEAB9EB604
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Membership.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Membership.BusinessLayer.Validators
{
  internal class UserProfileValidator : AbstractValidator<UserProfile>
  {
    public UserProfileValidator()
    {
      this.RuleFor<string>((Expression<Func<UserProfile, string>>) (t => t.UserName)).NotEmpty<UserProfile, string>().WithMessage<UserProfile, string>("Username cannot be empty.");
    }
  }
}
