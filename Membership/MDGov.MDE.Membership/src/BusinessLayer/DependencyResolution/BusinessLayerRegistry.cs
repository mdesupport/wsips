﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.BusinessLayer.DependencyResolution.BusinessLayerRegistry
// Assembly: MDGov.MDE.Membership.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E334AB81-1921-46AD-AF49-A0EEAB9EB604
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Membership.BusinessLayer.Validators;
using MDGov.MDE.Membership.Model;
using StructureMap.Configuration.DSL;

namespace MDGov.MDE.Membership.BusinessLayer.DependencyResolution
{
  public class BusinessLayerRegistry : Registry
  {
    public BusinessLayerRegistry()
    {
      this.For<IValidator<UserProfile>>().Use<UserProfileValidator>();
    }
  }
}
