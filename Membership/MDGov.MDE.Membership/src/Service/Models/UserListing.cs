﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Models.UserListing
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using System.Collections.Generic;

namespace MDGov.MDE.Membership.Service.Models
{
  public class UserListing
  {
    public int UserId { get; set; }

    public string UserName { get; set; }

    public string LastName { get; set; }

    public int UserTypeId { get; set; }

    public string UserType { get; set; }

    public string Email { get; set; }

    public bool Approved { get; set; }

    public string PrincipalUser { get; set; }

    public virtual IEnumerable<string> Roles { get; set; }

    public string Password { get; set; }
  }
}
