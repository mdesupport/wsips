﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Models.UserTypeListing
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

namespace MDGov.MDE.Membership.Service.Models
{
  public class UserTypeListing
  {
    public int Id { get; set; }

    public string Description { get; set; }
  }
}
