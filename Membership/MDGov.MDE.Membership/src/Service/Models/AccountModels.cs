﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Models.UsersContext
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using System.Data.Entity;

namespace MDGov.MDE.Membership.Service.Models
{
  public class UsersContext : DbContext
  {
    public DbSet<UserProfile> UserProfiles { get; set; }

    public DbSet<LU_UserType> UserTypes { get; set; }

    public DbSet<MDGov.MDE.Membership.Service.Models.webpages_Membership> webpages_Membership { get; set; }

    public UsersContext()
      : base("ModelContext")
    {
    }
  }
}
