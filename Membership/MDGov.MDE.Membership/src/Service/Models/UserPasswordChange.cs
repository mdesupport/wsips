﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Models.UserPasswordChange
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

namespace MDGov.MDE.Membership.Service.Models
{
  public class UserPasswordChange
  {
    public string UserName { get; set; }

    public string OldPassword { get; set; }

    public string NewPassword { get; set; }

    public string ResetToken { get; set; }
  }
}
