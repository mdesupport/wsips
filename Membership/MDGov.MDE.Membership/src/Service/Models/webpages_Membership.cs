﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Models.webpages_Membership
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MDGov.MDE.Membership.Service.Models
{
  [Table("webpages_Membership")]
  public class webpages_Membership
  {
    [Key]
    public int UserId { get; set; }

    public DateTime? CreateDate { get; set; }

    public string ConfirmationToken { get; set; }

    public bool? IsConfirmed { get; set; }

    public DateTime? LastPasswordFailureDate { get; set; }

    public int PasswordFailuresSinceLastSuccess { get; set; }

    public string Password { get; set; }

    public DateTime? PasswordChangedDate { get; set; }

    public string PasswordSalt { get; set; }

    public string PasswordVerificationToken { get; set; }

    public DateTime? PasswordVerificationTokenExpirationDate { get; set; }
  }
}
