﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Helpers.InitializeSimpleMembershipAttribute
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using MDGov.MDE.Membership.Service.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WebMatrix.WebData;

namespace MDGov.MDE.Membership.Service.Helpers
{
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
  public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
  {
    private static object _initializerLock = new object();
    private static InitializeSimpleMembershipAttribute.SimpleMembershipInitializer _initializer;
    private static bool _isInitialized;

    public override void OnActionExecuting(HttpActionContext actionContext)
    {
      LazyInitializer.EnsureInitialized<InitializeSimpleMembershipAttribute.SimpleMembershipInitializer>(ref InitializeSimpleMembershipAttribute._initializer, ref InitializeSimpleMembershipAttribute._isInitialized, ref InitializeSimpleMembershipAttribute._initializerLock);
    }

    private class SimpleMembershipInitializer
    {
      public SimpleMembershipInitializer()
      {
        Database.SetInitializer<UsersContext>((IDatabaseInitializer<UsersContext>) null);
        try
        {
          using (UsersContext usersContext = new UsersContext())
          {
            if (!usersContext.Database.Exists())
                ((IObjectContextAdapter)usersContext).ObjectContext.CreateDatabase();
          }
          WebSecurity.InitializeDatabaseConnection("ModelContext", "UserProfile", "UserId", "UserName", false);
        }
        catch (Exception ex)
        {
          throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
        }
      }
    }
  }
}
