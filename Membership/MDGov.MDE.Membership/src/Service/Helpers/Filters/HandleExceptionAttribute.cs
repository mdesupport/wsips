﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Membership.Service.Helpers.Filters.HandleExceptionAttribute
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MDGov.MDE.WSIPS.Membership.Service.Helpers.Filters
{
  public class HandleExceptionAttribute : ExceptionFilterAttribute
  {
    public override void OnException(HttpActionExecutedContext actionExecutedContext)
    {
      throw new HttpResponseException(actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An exception has been caught", actionExecutedContext.Exception));
    }
  }
}
