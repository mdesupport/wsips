﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Helpers.Configuration.ServiceConfig
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using System.Configuration;

namespace MDGov.MDE.Membership.Service.Helpers.Configuration
{
  public class ServiceConfig : ConfigurationSection
  {
    [ConfigurationCollection(typeof (ServiceConfig.EndpointCollection))]
    [ConfigurationProperty("endpoints", IsDefaultCollection = true)]
    public ServiceConfig.EndpointCollection Endpoints
    {
      get
      {
        return this["endpoints"] as ServiceConfig.EndpointCollection;
      }
    }

    public static string FromEmail
    {
      get
      {
        return ServiceConfig.GetAppSetting("FromEmailAddrress");
      }
    }

    public static string PasswordLength
    {
      get
      {
        return ServiceConfig.GetAppSetting("PasswordLength");
      }
    }

    public static string BaseConfirmationUrl
    {
      get
      {
        return ServiceConfig.GetAppSetting("BaseConfirmationUrl");
      }
    }

    public static string ChangePasswordUrl
    {
      get
      {
        return ServiceConfig.GetAppSetting("ChangePasswordUrl");
      }
    }

    public static string ResetPasswordUrl
    {
      get
      {
        return ServiceConfig.GetAppSetting("ResetPasswordUrl");
      }
    }

    public static ServiceConfig GetConfig()
    {
      return (ServiceConfig) ConfigurationManager.GetSection("serviceConfig") ?? new ServiceConfig();
    }

    private static string GetAppSetting(string key)
    {
      return ConfigurationManager.AppSettings[key];
    }

    public class Endpoint : ConfigurationElement
    {
      [ConfigurationProperty("type", IsKey = true, IsRequired = true)]
      public string Type
      {
        get
        {
          return this["type"] as string;
        }
      }

      [ConfigurationProperty("uri", IsRequired = true)]
      public string Uri
      {
        get
        {
          return this["uri"] as string;
        }
      }
    }

    public class EndpointCollection : ConfigurationElementCollection
    {
      public ServiceConfig.Endpoint this[int index]
      {
        get
        {
          return this.BaseGet(index) as ServiceConfig.Endpoint;
        }
        set
        {
          if (this.BaseGet(index) != null)
            this.BaseRemoveAt(index);
          this.BaseAdd(index, (ConfigurationElement) value);
        }
      }

      public ServiceConfig.Endpoint this[string key]
      {
        get
        {
          return this.BaseGet((object) key) as ServiceConfig.Endpoint;
        }
        set
        {
          if (this.BaseGet((object) key) != null)
            this.BaseRemove((object) key);
          this.BaseAdd((ConfigurationElement) value);
        }
      }

      protected override ConfigurationElement CreateNewElement()
      {
        return (ConfigurationElement) new ServiceConfig.Endpoint();
      }

      protected override object GetElementKey(ConfigurationElement element)
      {
        return (object) ((ServiceConfig.Endpoint) element).Type;
      }
    }
  }
}
