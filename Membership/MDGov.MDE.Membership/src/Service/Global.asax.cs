﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.WebApiApplication
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MDGov.MDE.Membership.Service
{
  public class WebApiApplication : HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      WebApiConfig.Register(GlobalConfiguration.Configuration);
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      FilterConfig.RegisterHttpFilters(GlobalConfiguration.Configuration.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }
  }
}
