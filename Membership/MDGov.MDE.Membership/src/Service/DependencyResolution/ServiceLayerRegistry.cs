﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.DependencyResolution.ServiceLayerRegistry
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Logging;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Membership.Model;
using StructureMap;
using StructureMap.Configuration.DSL;
using System;
using System.Data.Entity;

namespace MDGov.MDE.Membership.Service.DependencyResolution
{
  public class ServiceLayerRegistry : Registry
  {
    public ServiceLayerRegistry()
    {
      this.For(typeof (IRepository<>)).Use(typeof (Repository<>));
      this.For<DbContext>().Use<ModelContext>();
      this.For<IRepository<ErrorLog>>().Use((Func<IContext, IRepository<ErrorLog>>) (ctx => (IRepository<ErrorLog>) new Repository<ErrorLog>(ctx.GetInstance<LoggingContext>())));
      this.For(typeof (IService<>)).Use(typeof (ServiceBase<>));
      this.For(typeof (IUpdatableService<>)).Use(typeof (UpdatableServiceBase<>));
      this.For<IErrorLogging>().Use<ErrorLogging>();
    }
  }
}
