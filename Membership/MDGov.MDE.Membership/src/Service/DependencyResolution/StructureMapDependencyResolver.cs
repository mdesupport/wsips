﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.DependencyResolution.StructureMapDependencyResolver
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using StructureMap;
using System;
using System.Web.Http.Dependencies;

namespace MDGov.MDE.Membership.Service.DependencyResolution
{
  public class StructureMapDependencyResolver : StructureMapDependencyScope, IDependencyResolver, IDependencyScope, IDisposable
  {
    public StructureMapDependencyResolver(IContainer container)
      : base(container)
    {
    }

    public IDependencyScope BeginScope()
    {
      return (IDependencyScope) new StructureMapDependencyResolver(this.Container.GetNestedContainer());
    }
  }
}
