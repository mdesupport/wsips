﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.DependencyResolution.IoC
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Membership.BusinessLayer.DependencyResolution;
using StructureMap;
using StructureMap.Graph;
using System;

namespace MDGov.MDE.Membership.Service.DependencyResolution
{
  public static class IoC
  {
    public static IContainer Initialize()
    {
      ObjectFactory.Initialize((Action<IInitializationExpression>) (x =>
      {
        x.Scan((Action<IAssemblyScanner>) (scan =>
        {
          scan.TheCallingAssembly();
          scan.WithDefaultConventions();
          scan.AddAllTypesOf(typeof (IRepository<>));
          scan.ConnectImplementationsToTypesClosing(typeof (IRepository<>));
        }));
        x.AddRegistry<BusinessLayerRegistry>();
        x.AddRegistry<ServiceLayerRegistry>();
      }));
      return ObjectFactory.Container;
    }
  }
}
