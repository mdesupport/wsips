﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.App_Start.StructuremapMvc
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using MDGov.MDE.Membership.Service.DependencyResolution;
using StructureMap;
using System.Web.Http;
using System.Web.Mvc;

namespace MDGov.MDE.Membership.Service.App_Start
{
  public static class StructuremapMvc
  {
    public static void Start()
    {
      IContainer container = IoC.Initialize();
      DependencyResolver.SetResolver((object) new StructureMapDependencyResolver(container));
      GlobalConfiguration.Configuration.DependencyResolver = (System.Web.Http.Dependencies.IDependencyResolver) new StructureMapDependencyResolver(container);
    }
  }
}
