﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.FilterConfig
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using MDGov.MDE.WSIPS.Membership.Service.Helpers.Filters;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace MDGov.MDE.Membership.Service
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add((object) new HandleErrorAttribute());
    }

    public static void RegisterHttpFilters(HttpFilterCollection filters)
    {
      filters.Add((IFilter) new HandleExceptionAttribute());
    }
  }
}
