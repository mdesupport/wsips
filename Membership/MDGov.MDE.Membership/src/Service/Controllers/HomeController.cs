﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Controllers.HomeController
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Membership.Model;
using System.Web.Mvc;

namespace MDGov.MDE.Membership.Service.Controllers
{
  public class HomeController : Controller
  {
    private readonly IRepository<LU_UserType> _userTypesRepository;

    public HomeController(IRepository<LU_UserType> userTypesRepository)
    {
      this._userTypesRepository = userTypesRepository;
    }

    public ActionResult Index()
    {
      return (ActionResult) this.View();
    }
  }
}
