﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Service.Controllers.AccountController
// Assembly: MDGov.MDE.Membership.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18620C2C-F579-4D8E-8D3E-D81D50A01481
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Service.dll

using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Membership.Service.Helpers;
using MDGov.MDE.Membership.Service.Helpers.Configuration;
using MDGov.MDE.Membership.Service.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Security;
using WebMatrix.WebData;

namespace MDGov.MDE.Membership.Service.Controllers
{
  [InitializeSimpleMembership]
  public class AccountController : ApiController
  {
    private readonly IUpdatableService<BaseMessage> _messageService;
    private readonly IUpdatableService<MessageJob> _messageJobService;

    public AccountController(IUpdatableService<BaseMessage> messageService, IUpdatableService<MessageJob> messageJobService)
    {
      this._messageService = messageService;
      this._messageJobService = messageJobService;
    }

    public IEnumerable<string> Get()
    {
      return (IEnumerable<string>) new string[2]
      {
        "value1",
        "value2"
      };
    }

    public string Get(int id)
    {
      return "value";
    }

    [HttpGet]
    public bool IsConfirmed(string userName)
    {
      if (WebSecurity.UserExists(userName))
        return WebSecurity.IsConfirmed(userName);
      return false;
    }

    [HttpGet]
    public bool HasSystemGeneratedPassword(string userName)
    {
      if (WebSecurity.UserExists(userName))
        return WebSecurity.GetPasswordChangedDate(userName) == WebSecurity.GetCreateDate(userName);
      return false;
    }

    [HttpGet]
    public int PasswordFailures(string userName)
    {
      return WebSecurity.GetPasswordFailuresSinceLastSuccess(userName);
    }

    [HttpGet]
    public IEnumerable<string> GetRoles()
    {
      return (IEnumerable<string>) Roles.GetAllRoles();
    }

    [HttpGet]
    public IEnumerable<string> GetUserRoles(string userName)
    {
      List<string> stringList = new List<string>();
      foreach (string allRole in Roles.GetAllRoles())
      {
        string role = allRole;
        string[] usersInRole = Roles.FindUsersInRole(role, userName);
        stringList.AddRange(((IEnumerable<string>) usersInRole).Where<string>((Func<string, bool>) (item => item.ToLower() == userName.ToLower())).Select<string, string>((Func<string, string>) (item => role)));
      }
      return (IEnumerable<string>) stringList;
    }

    [HttpGet]
    public IEnumerable<int> GetUsersInRole(string role)
    {
      return (IEnumerable<int>) ((IEnumerable<string>) Roles.GetUsersInRole(role)).Select<string, int>(new Func<string, int>(this.GetUserIdByName)).ToList<int>();
    }

    [HttpGet]
    public int GetUserIdByName(string userName)
    {
      if (!WebSecurity.UserExists(userName))
        return 0;
      return WebSecurity.GetUserId(userName.ToLower());
    }

    [HttpPost]
    public bool UpdateUserName(UserListing userToUpdate)
    {
      bool flag = false;
      using (UsersContext usersContext = new UsersContext())
      {
        UserProfile userProfile = usersContext.UserProfiles.SingleOrDefault<UserProfile>((Expression<Func<UserProfile, bool>>) (u => u.UserId == userToUpdate.UserId));
        if (userProfile != null)
        {
          userProfile.UserName = userToUpdate.UserName;
          usersContext.SaveChanges();
          flag = true;
        }
      }
      return flag;
    }

    [HttpPost]
    public bool UpdateUserInfo(UserListing userToUpdate)
    {
      bool flag = false;
      using (UsersContext usersContext = new UsersContext())
      {
        UserProfile userProfile = usersContext.UserProfiles.SingleOrDefault<UserProfile>((Expression<Func<UserProfile, bool>>) (u => u.UserId == userToUpdate.UserId));
        if (userProfile != null)
        {
          if (userToUpdate.UserName != null)
            userProfile.UserName = userToUpdate.UserName;
          if (!string.IsNullOrEmpty(userToUpdate.LastName))
            userProfile.LastName = userToUpdate.LastName;
          usersContext.SaveChanges();
          flag = true;
        }
      }
      return flag;
    }

    [HttpGet]
    public int GetUserTypeId(string userType)
    {
      LU_UserType luUserType = new UsersContext().UserTypes.SingleOrDefault<LU_UserType>((Expression<Func<LU_UserType, bool>>) (u => u.Description == userType));
      if (luUserType == null)
        return 0;
      return luUserType.Id;
    }

    [HttpGet]
    public UserListing GetUserById(int id)
    {
      UsersContext usersContext = new UsersContext();
      UserProfile user = usersContext.UserProfiles.SingleOrDefault<UserProfile>((Expression<Func<UserProfile, bool>>) (u => u.UserId == id));
      if (user != null)
      {
        LU_UserType luUserType = usersContext.UserTypes.SingleOrDefault<LU_UserType>((Expression<Func<LU_UserType, bool>>) (u => u.Id == user.UserTypeId));
        if (luUserType != null)
          return new UserListing()
          {
            UserId = id,
            UserName = user.UserName,
            UserTypeId = user.UserTypeId,
            UserType = luUserType.Description
          };
      }
      return (UserListing) null;
    }

    [HttpGet]
    public IEnumerable<UserTypeListing> GetUserTypes()
    {
      return (IEnumerable<UserTypeListing>) new UsersContext().UserTypes.Where<LU_UserType>((Expression<Func<LU_UserType, bool>>) (u => u.Active == (bool?) true)).Select<LU_UserType, UserTypeListing>((Expression<Func<LU_UserType, UserTypeListing>>) (item => new UserTypeListing()
      {
        Id = item.Id,
        Description = item.Description
      })).ToList<UserTypeListing>();
    }

    [HttpGet]
    public bool ConfirmAccount(string token)
    {
      return WebSecurity.ConfirmAccount(token);
    }

    [HttpPost]
    public bool IsInRole(UserRole userRole)
    {
      return ((IEnumerable<string>) Roles.GetRolesForUser(userRole.UserName)).Any<string>((Func<string, bool>) (r => ((IEnumerable<string>) userRole.Roles).Contains<string>(r)));
    }

    [HttpPost]
    public bool Validate(Login login)
    {
      return WebSecurity.Login(login.UserName, login.Password, false);
    }

    [HttpPost]
    public IEnumerable<UserListing> GetUserRange(UserFilter filter)
    {
      UsersContext usersContext = new UsersContext();
      IQueryable<UserProfile> source = usersContext.UserProfiles.Where<UserProfile>((Expression<Func<UserProfile, bool>>) (u => filter.Ids.Contains<int>(u.UserId)));
      if (filter.Name != null)
        source = source.Where<UserProfile>((Expression<Func<UserProfile, bool>>) (u => u.UserName.Contains(filter.Name)));
      int? typeId = filter.TypeId;
      if ((typeId.GetValueOrDefault() != 0 ? 1 : (!typeId.HasValue ? 1 : 0)) != 0)
        source = source.Where<UserProfile>((Expression<Func<UserProfile, bool>>) (u => (int?) u.UserTypeId == filter.TypeId));
      List<UserListing> userListingList = new List<UserListing>();
      foreach (UserProfile userProfile in (IEnumerable<UserProfile>) source)
      {
        UserProfile user = userProfile;
        UserListing userListing1 = new UserListing();
        userListing1.UserId = user.UserId;
        userListing1.UserName = user.UserName;
        userListing1.LastName = user.LastName;
        userListing1.UserTypeId = user.UserTypeId;
        userListing1.UserType = usersContext.UserTypes.FirstOrDefault<LU_UserType>((Expression<Func<LU_UserType, bool>>) (u => u.Id == user.UserTypeId)).Description;
        userListing1.Roles = this.GetUserRoles(user.UserName);
        UserListing userListing2 = userListing1;
        if (string.IsNullOrEmpty(filter.Role))
          userListingList.Add(userListing2);
        else if (filter.Role.ToLower() == "mde staff" && !userListing2.Roles.Any<string>() && userListing2.UserTypeId == 1)
          userListingList.Add(userListing2);
        else if (Roles.FindUsersInRole(filter.Role, user.UserName).Length > 0)
          userListingList.Add(userListing2);
      }
      return (IEnumerable<UserListing>) userListingList;
    }

    [HttpPost]
    public string CreateAccount(UserListing user)
    {
      string token = string.Empty;
      if (!WebSecurity.UserExists(user.UserName))
      {
        string randomPassword = this.GetRandomPassword(ServiceConfig.PasswordLength);
        int userTypeId = this.GetUserTypeId(user.UserType);
        token = WebSecurity.CreateUserAndAccount(user.UserName.ToLower(), randomPassword, (object) new
        {
          UserTypeId = userTypeId
        }, !user.Approved);
        if (user.Approved)
          this.SendNotificationEmail(user.UserName, user.Email, randomPassword, user.PrincipalUser);
        else
          this.SendConfirmationEmail(user.UserName, user.Email, randomPassword, token, user.PrincipalUser);
      }
      return token;
    }

    [HttpPost]
    public string CreateAccountWithPassword(UserListing user)
    {
      string token = string.Empty;
      if (!WebSecurity.UserExists(user.UserName))
      {
        int userTypeId = this.GetUserTypeId(user.UserType);
        token = WebSecurity.CreateUserAndAccount(user.UserName.ToLower(), user.Password, (object) new
        {
          UserTypeId = userTypeId
        }, !user.Approved);
        if (user.Approved)
          this.SendNotificationEmail(user.UserName, user.Email, user.Password, user.PrincipalUser);
        else
          this.SendConfirmationEmail(user.UserName, user.Email, user.Password, token, user.PrincipalUser);
      }
      return token;
    }

    [HttpPost]
    public void AssignRoles(UserRole userRole)
    {
      string lower = userRole.UserName.ToLower();
      string[] array = new List<string>(this.GetUserRoles(lower)).ToArray();
      if (array.Length > 0)
        Roles.RemoveUserFromRoles(lower, array);
      Roles.AddUserToRoles(lower, userRole.Roles);
    }

    [HttpPost]
    public bool Confirm(string confirmationToken)
    {
      return WebSecurity.ConfirmAccount(confirmationToken);
    }

    [HttpPost]
    public bool ChangePassword(UserPasswordChange user)
    {
      return WebSecurity.ChangePassword(user.UserName, user.OldPassword, user.NewPassword);
    }

    [HttpPost]
    public bool ChangePasswordEmail(UserListing user)
    {
      if (WebSecurity.GetUserId(user.UserName) > 0)
        return this.SendPasswordChangeEmail(user.UserName, user.PrincipalUser);
      return false;
    }

    [HttpPost]
    public bool ResetPassword(UserPasswordChange user)
    {
      return WebSecurity.ResetPassword(user.ResetToken, user.NewPassword);
    }

    [HttpPost]
    public bool ResetPasswordEmail(UserListing user)
    {
      if (WebSecurity.GetUserId(user.UserName) > 0)
        return this.SendPasswordResetEmail(user.UserName, user.PrincipalUser);
      return false;
    }

    [HttpPost]
    public bool ReSendConfirmationEmail(UserListing user)
    {
      UsersContext usersContext = new UsersContext();
      int userId = usersContext.UserProfiles.FirstOrDefault<UserProfile>((Expression<Func<UserProfile, bool>>) (u => u.UserName == user.UserName)).UserId;
      string confirmationToken = usersContext.webpages_Membership.SingleOrDefault<webpages_Membership>((Expression<Func<webpages_Membership, bool>>) (u => u.UserId == userId)).ConfirmationToken;
      this.SendConfirmationEmail(user.UserName, user.UserName, this.GetRandomPassword(ServiceConfig.PasswordLength), confirmationToken, user.PrincipalUser);
      return true;
    }

    [HttpGet]
    public bool Delete(int id)
    {
      try
      {
        UserListing userById = this.GetUserById(id);
        if (userById == null)
          return false;
        userById.Roles = this.GetUserRoles(userById.UserName);
        if (userById.Roles != null)
          Roles.RemoveUserFromRoles(userById.UserName, userById.Roles.ToArray<string>());
        using (UsersContext usersContext = new UsersContext())
        {
          UserProfile entity1 = usersContext.UserProfiles.FirstOrDefault<UserProfile>((Expression<Func<UserProfile, bool>>) (u => u.UserId == id));
          webpages_Membership entity2 = usersContext.webpages_Membership.FirstOrDefault<webpages_Membership>((Expression<Func<webpages_Membership, bool>>) (u => u.UserId == id));
          usersContext.webpages_Membership.Remove(entity2);
          usersContext.UserProfiles.Remove(entity1);
          usersContext.SaveChanges();
        }
        return true;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    private void SendNotificationEmail(string userName, string email, string password, string principalUser)
    {
      string path = HostingEnvironment.MapPath("~/Content/EmailTemplates/RegistrationNotificationEmail.html");
      if (path == null)
        return;
      string str = File.ReadAllText(path).Replace("{username}", userName);
      int num = this._messageService.Save(new BaseMessage()
      {
        Body = str,
        Subject = "Registration Notification",
        From = ServiceConfig.FromEmail,
        LastModifiedBy = principalUser
      });
      this._messageJobService.Save(new MessageJob()
      {
        Address = email,
        BaseMessageId = num,
        LastModifiedBy = principalUser
      });
    }

    private void SendConfirmationEmail(string userName, string email, string password, string token, string principalUser)
    {
      string path = HostingEnvironment.MapPath("~/Content/EmailTemplates/RegistrationConfirmationEmail.html");
      if (path == null)
        return;
      string str1 = File.ReadAllText(path);
      string newValue = string.Format("<a href=\"{0}\">Click to confirm your registration</a>", (object) (ServiceConfig.BaseConfirmationUrl + HttpUtility.UrlEncode(token)));
      string str2 = str1.Replace("{username}", userName).Replace("{confirmationLink}", newValue);
      int num = this._messageService.Save(new BaseMessage()
      {
        Body = str2,
        Subject = "Registration Notification",
        From = ServiceConfig.FromEmail,
        LastModifiedBy = principalUser
      });
      this._messageJobService.Save(new MessageJob()
      {
        Address = email,
        BaseMessageId = num,
        LastModifiedBy = principalUser
      });
    }

    private bool SendPasswordChangeEmail(string email, string principalUser)
    {
      string path = HostingEnvironment.MapPath("~/Content/EmailTemplates/ChangePasswordEmail.html");
      string changePasswordUrl = ServiceConfig.ChangePasswordUrl;
      if (path == null)
        return false;
      string str = File.ReadAllText(path).Replace("{changePasswordLink}", changePasswordUrl);
      int num = this._messageService.Save(new BaseMessage()
      {
        Body = str,
        Subject = "Change Your Password",
        From = ServiceConfig.FromEmail,
        LastModifiedBy = principalUser
      });
      return this._messageJobService.Save(new MessageJob()
      {
        Address = email,
        BaseMessageId = num,
        LastModifiedBy = principalUser
      }) != 0;
    }

    private bool SendPasswordResetEmail(string email, string principalUser)
    {
      string path = HostingEnvironment.MapPath("~/Content/EmailTemplates/ResetPasswordEmail.html");
      string newValue = ServiceConfig.ResetPasswordUrl + WebSecurity.GeneratePasswordResetToken(email, 1440);
      if (path == null)
        return false;
      string str = File.ReadAllText(path).Replace("{resetPasswordLink}", newValue);
      int num = this._messageService.Save(new BaseMessage()
      {
        Body = str,
        Subject = "Reset Your Password",
        From = ServiceConfig.FromEmail,
        LastModifiedBy = principalUser
      });
      return this._messageJobService.Save(new MessageJob()
      {
        Address = email,
        BaseMessageId = num,
        LastModifiedBy = principalUser
      }) != 0;
    }

    private string GetRandomPassword(string length)
    {
      int result;
      return Guid.NewGuid().ToString().Replace("-", "").Substring(0, int.TryParse(length, out result) ? result : 10);
    }
  }
}
