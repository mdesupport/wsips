﻿using MDGov.MDE.Membership.Service.App_Start;
using System.Reflection;
using System.Runtime.InteropServices;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof (StructuremapMvc), "Start")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyTitle("Service")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("JMT")]
[assembly: AssemblyProduct("Service")]
[assembly: AssemblyCopyright("Copyright © JMT 2012")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("7518fe8f-0146-40d5-99b7-b5ef8a44ec60")]
[assembly: AssemblyVersion("1.0.0.0")]
