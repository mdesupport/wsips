﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[webpages_Roles]

SELECT 'Operational Administrator' UNION ALL
SELECT 'IT Administrator' UNION ALL
SELECT 'Division Chief' UNION ALL
SELECT 'Permit Supervisor' UNION ALL
SELECT 'Project Manager' UNION ALL
SELECT 'Administrative Specialist' UNION ALL
SELECT 'Secretary' UNION ALL
SELECT 'Compliance / Enforcement Manager' UNION ALL
SELECT 'Compliance / Enforcement Staff' UNION ALL
SELECT 'WSIPS eCommerce Member' UNION ALL
SELECT 'External Authority' UNION ALL
SELECT 'Applicant / Permittee' UNION ALL
SELECT 'MDE Staff'

COMMIT;
RAISERROR (N'[dbo].[webpages_Roles]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
