﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_UserType]

SELECT 'Internal', 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 'External Authority', 2, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 'Public', 3, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_UserType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;