﻿CREATE TABLE [dbo].[LU_UserType]
(
	[Id] INT IDENTITY (1, 1) NOT NULL, 
    [Description] VARCHAR(255) NULL, 
    [Sequence] INT NULL, 
    [Active] BIT NULL, 
    [CreatedBy] VARCHAR(100) NOT NULL, 
    [CreatedDate] DATETIME NOT NULL, 
    [LastModifiedBy] VARCHAR(100) NOT NULL, 
    [LastModifiedDate] DATETIME NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC)
)
