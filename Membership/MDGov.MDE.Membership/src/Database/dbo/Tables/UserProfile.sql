﻿CREATE TABLE [dbo].[UserProfile] (
    [UserId]   INT            IDENTITY (1, 1) NOT NULL,
    [UserName] NVARCHAR (MAX) NULL,
    [UserTypeId] INT NULL, 
    [LastName] VARCHAR(40) NULL, 
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

