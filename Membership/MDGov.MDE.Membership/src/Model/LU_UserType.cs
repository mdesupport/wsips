﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Model.LU_UserType
// Assembly: MDGov.MDE.Membership.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 26614F40-DBCA-4FC2-BBF8-C07B5A3DE18E
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Membership.Model
{
  public class LU_UserType : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool? Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }
  }
}
