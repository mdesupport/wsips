﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Model.webpages_Membership
// Assembly: MDGov.MDE.Membership.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 26614F40-DBCA-4FC2-BBF8-C07B5A3DE18E
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Model.dll

using System;

namespace MDGov.MDE.Membership.Model
{
  public class webpages_Membership
  {
    public int UserId { get; set; }

    public DateTime? CreateDate { get; set; }

    public string ConfirmationToken { get; set; }

    public bool? IsConfirmed { get; set; }

    public DateTime? LastPasswordFailureDate { get; set; }

    public int PasswordFailuresSinceLastSuccess { get; set; }

    public string Password { get; set; }

    public DateTime? PasswordChangedDate { get; set; }

    public string PasswordSalt { get; set; }

    public string PasswordVerificationToken { get; set; }

    public DateTime? PasswordVerificationTokenExpirationDate { get; set; }
  }
}
