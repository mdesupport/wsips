﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Model.ModelContext
// Assembly: MDGov.MDE.Membership.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 26614F40-DBCA-4FC2-BBF8-C07B5A3DE18E
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Model.dll

using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MDGov.MDE.Membership.Model
{
  public class ModelContext : DbContext
  {
    public DbSet<UserProfile> UserProfiles { get; set; }

    public DbSet<MDGov.MDE.Membership.Model.webpages_Membership> webpages_Membership { get; set; }

    public DbSet<MDGov.MDE.Membership.Model.webpages_OAuthMembership> webpages_OAuthMembership { get; set; }

    public DbSet<MDGov.MDE.Membership.Model.webpages_Roles> webpages_Roles { get; set; }

    public DbSet<MDGov.MDE.Membership.Model.webpages_UsersInRoles> webpages_UsersInRoles { get; set; }

    public DbSet<MDGov.MDE.Membership.Model.C__RefactorLog> C__RefactorLog { get; set; }

    public DbSet<MDGov.MDE.Membership.Model.LU_UserType> LU_UserType { get; set; }

    public ModelContext()
      : base("name=ModelContext")
    {
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      throw new UnintentionalCodeFirstException();
    }
  }
}
