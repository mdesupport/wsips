﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Membership.Model.UserProfile
// Assembly: MDGov.MDE.Membership.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 26614F40-DBCA-4FC2-BBF8-C07B5A3DE18E
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Membership\bin\MDGov.MDE.Membership.Model.dll

using System.Collections.Generic;

namespace MDGov.MDE.Membership.Model
{
  public class UserProfile
  {
    public int UserId { get; set; }

    public string UserName { get; set; }

    public int? UserTypeId { get; set; }

    public string LastName { get; set; }

    public virtual ICollection<MDGov.MDE.Membership.Model.webpages_UsersInRoles> webpages_UsersInRoles { get; set; }

    public UserProfile()
    {
      this.webpages_UsersInRoles = (ICollection<MDGov.MDE.Membership.Model.webpages_UsersInRoles>) new HashSet<MDGov.MDE.Membership.Model.webpages_UsersInRoles>();
    }
  }
}
