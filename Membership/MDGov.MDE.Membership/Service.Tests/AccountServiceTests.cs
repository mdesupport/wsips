﻿using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Membership.Service.Controllers;
using MDGov.MDE.Membership.Service.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Service.Tests
{
    [TestClass]
    public class AccountServiceTests
    {
        private Mock<IUpdatableService<BaseMessage>> _messageService;
        private Mock<IUpdatableService<MessageJob>> _messageJobService;
        
        public AccountController CreateAccountController()
        {
            var message = new BaseMessage { Id = 1, ApplicationId = 1 };
            var messageJob = new MessageJob { Id = 1, Address = "telavsky@jmt.com"};

            _messageService = new Mock<IUpdatableService<BaseMessage>>();
            _messageService.Setup(x => x.Save(message)).Returns(0);

            _messageJobService = new Mock<IUpdatableService<MessageJob>>();
            _messageJobService.Setup(x => x.Save(messageJob)).Returns(0);

            return new AccountController(_messageService.Object, _messageJobService.Object);
        }

        [TestMethod]
        [Ignore]
        [InitializeSimpleMembership]
        public void IsConfirmed_ValidUserName_ReturnsTrue()
        {
            const string userName = "Tester";
            var controller = CreateAccountController();

            var result = controller.IsConfirmed(userName);

            Assert.AreNotEqual(result, true);
        }
    }
}
