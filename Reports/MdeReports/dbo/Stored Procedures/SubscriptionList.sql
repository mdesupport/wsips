﻿

CREATE PROCEDURE [dbo].[SubscriptionList]
(	@Date datetime)
AS
BEGIN

	-- exec [SubscriptionList] '11/1/2013'

	-- get the beginning of the month
	DECLARE @BeginDate datetime
	SET @BeginDate = convert(datetime, cast(datepart(month, @Date) as varchar(2)) + '/01/' + cast(datepart(year, @Date) as varchar(4)), 101)
	
	-- go through the whole month
	DECLARE @EndDate Datetime
	SET @EndDate = DateAdd(month, 1, @BeginDate)
	
	select	c.Description CountyName, p.PermitNumber, p.RevisionNumber, isnull(p.Description, '') PermitDescription
	from	MdeWaterPermitting..PermitStatusHistory psh
		inner join MdeWaterPermitting..Permit p on p.Id = Psh.PermitId 
		inner join MdeWaterPermitting..PermitCounty pc on pc.PermitId = p.Id
		inner join MdeWaterPermitting..LU_County c on c.Id = pc.CountyId 
		inner join MdeWaterPermitting..LU_PermitStatus lps on lps.Id = p.PermitStatusId 
	where	psh.PermitStatusId = 9					-- PermitStatusID  became 9 during the selected time period
		AND	psh.createddate >= @BeginDate			-- status change occurred after the begin date
		AND psh.createddate <= @EndDate				-- status change occurred before the end date
		AND p.PermitTypeId <> 5						-- not an exemption
		AND	lps.PermitCategoryId in (2,4)			-- PermitCategoryID = 2 or 4 
		AND p.PermitStatusId not in (60,61,66,67)	-- current Permitstatusid not in (60, 61, 66, 67)
		AND p.RequiresAdvertising = 1				-- RequiresAdvertising = 1 

END
