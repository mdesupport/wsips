﻿



CREATE PROCEDURE [dbo].[OnlinePayments]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN

	-- exec [OnlinePayments] '10/2/2012', '3/2/2014'

	--DECLARE	@StartDate varchar(10)
	--DECLARE @EndDate varchar(10)

	--set @StartDate = '3/20/2012'
	--set @EndDate = '3/20/2014'
	
	DECLARE @StartDateDT Datetime
	DECLARE @EndDateDT Datetime

	-- The report needs to show transaction dates that match the credit card company reports
	-- where a transaction date goes from 7:30pm to 7:29pm of the following day
	set @StartDateDT = dateadd(mi, -270, @StartDate)
	set @EndDateDT = dateadd(S, 70199, @EndDate)

	SELECT	@StartDateDT StartDate, @EndDateDT EndDate, PaidDate, cs.Id TransactionId, cs.Amount, p.PermitNumber, product.Description 
	FROM	MDEPayment.dbo.ContactSale cs 
		LEFT OUTER JOIN MdeWaterPermitting.dbo.Permit p on cs.PermitId = p.Id 
		INNER JOIN MDEPayment.dbo.Product product on cs.ProductId = product.Id
	WHERE	PaidDate is not null
		and PaidDate between @StartDateDT and @EndDateDT
		and ContactSaleStatusId = 2 -- paid
		and IsOnlinePayment = 1
	order by PaidDate
	
END






