﻿


CREATE PROCEDURE [dbo].[InterestedPartyList]
(@permitName varchar(24))
AS
BEGIN

	-- exec [InterestedPartyList] 'FR1901G001(03)'
	
	select	p.permitname, pc.contactid as 'ID NBR', isnull(c.FirstName + ' ','') + isnull(c.LastName, '') as 'INTERESTED PARTY NAME',
			isnull(C.BusinessName,'') AS 'COMPANY/ORGANIZATION NAME',c.Address1,c.Address2,c.City,
			lus.Description as State,c.ZipCode,c.CountryId,c.CreatedBy,c.CreatedDate
	from	MdeWaterPermitting..permit p 
		inner join MdeWaterPermitting..PermitContact pc on pc.PermitId = p.id
		inner join MdeWaterPermitting..PermitContactInformation c on c.Id = pc.PermitContactInformationId 
		left outer join MdeWaterPermitting..LU_State lus on  lus.id = c.StateId
	where	p.PermitName like @permitName and pc.ContactTypeId = 15
		 
END


