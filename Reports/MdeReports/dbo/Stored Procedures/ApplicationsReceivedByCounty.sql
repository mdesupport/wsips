﻿
CREATE PROCEDURE [dbo].[ApplicationsReceivedByCounty]
(@BeginDate datetime,
@EndDate datetime)
AS
BEGIN

	-- exec [ApplicationsReceivedByCounty] '01/01/2012', '08/01/2012'

	-- go through the whole day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	SELECT	t.CountyId, 
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser
	INTO    #permits
	FROM	MdeWaterPermitting..Permit p 
		INNER JOIN (SELECT	PermitId, min(CountyId) CountyId
					FROM	MdeWaterPermitting..PermitCounty
					GROUP BY PermitId) t on t.PermitId = p.Id
		INNER JOIN MdeWaterPermitting..LU_County c on c.Id = t.CountyId
	WHERE	p.Id in (	SELECT	PermitId
					FROM	MdeWaterPermitting..PermitStatusHistory psh
					WHERE	PermitStatusId = 3
						and CreatedDate between @BeginDate and @EndDate)
		

	SELECT	t.CountyId, c.Description CountyName,
			isnull(cntTotal.Total, 0) cntTotal, 
			isnull(cntLarge.Total, 0) cntLarge,
			isnull(cntSmall.Total, 0) cntSmall
	FROM	#permits t
			LEFT OUTER JOIN (SELECT	CountyId, COUNT(*) Total FROM #permits GROUP BY CountyId) cntTotal on cntTotal.CountyId = t.CountyId
			LEFT OUTER JOIN (SELECT	CountyId, COUNT(*) Total FROM #permits WHERE LargeUser = 'Y' GROUP BY CountyId) cntLarge on cntLarge.CountyId = t.CountyId
			LEFT OUTER JOIN (SELECT	CountyId, COUNT(*) Total FROM #permits WHERE LargeUser = 'N' GROUP BY CountyId) cntSmall on cntSmall.CountyId = t.CountyId
			INNER JOIN MdeWaterPermitting..LU_County c on c.Id = t.CountyId
	GROUP BY t.CountyId,c.Description ,
			isnull(cntTotal.Total, 0) , 
			isnull(cntLarge.Total, 0) ,
			isnull(cntSmall.Total, 0) 
	ORDER BY c.Description 
END


