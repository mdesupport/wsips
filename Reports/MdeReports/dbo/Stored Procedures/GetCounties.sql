﻿

CREATE PROCEDURE [dbo].[GetCounties]
AS
BEGIN
	
	-- exec [GetCounties]

	SELECT	Id, Description Name, Sequence
	FROM	MdeWaterPermitting..LU_County
	WHERE	Active = 1
	UNION
	SELECT	null, '- All Counties -', 0
	ORDER BY Sequence
	
END

