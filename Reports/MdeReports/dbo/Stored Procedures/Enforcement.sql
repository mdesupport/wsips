﻿



CREATE PROCEDURE [dbo].[Enforcement]
(@StartDate Datetime,
@EndDate Datetime,
@ViolationTypeId int = null)
AS
BEGIN

--DECLARE @StartDate Datetime
--DECLARE @EndDate Datetime
--DECLARE @ViolationTypeId int

--SET @StartDate = '1/1/2014'
--SET @EndDate = '3/1/2014'
--SET @ViolationTypeId = null

	-- go through the whole day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	SELECT	p.PermitNumber, 
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			[dbo].[GetTopTypeOfUse](p.Id) TypeOfUse,
			v.FileNumber, vt.Description ViolationType,
			convert(varchar(12), v.CreatedDate, 101) ViolationDate,
			isnull(v.Status,'') Comments,
			case when ResolutionDate is null then 'No' else 'Yes' end Resolved,
			isnull(et.Description,'') EnforcementAction,
			isnull(convert(varchar(12), e.ActionDate, 101), '') EnforcementDateOfAction,
			isnull(convert(varchar(12), e.ResponseReceivedDate, 101), '')  EnforcementReceivedDate
			-- ,p.Id PermitId, v.Id ViolationId, pve.Id PVEId, e.Id EnforcementId
	FROM	MdeWaterPermitting.dbo.Permit p
		INNER JOIN MdeWaterPermitting.dbo.Violation v on p.Id = v.PermitId
		INNER JOIN MdeWaterPermitting.dbo.LU_ViolationType vt on vt.Id = v.ViolationTypeId
		LEFT OUTER JOIN MdeWaterPermitting.dbo.PermitViolationEnforcement pve on v.Id = pve.ViolationId 
		LEFT OUTER JOIN MdeWaterPermitting.dbo.Enforcement e on e.Id = pve.EnforcementId
		LEFT OUTER JOIN MdeWaterPermitting.dbo.LU_EnforcementAction et on et.Id = e.EnforcementActionId
	WHERE	v.CreatedDate between @StartDate and @EndDate
		and v.ViolationTypeId = isnull(@ViolationTypeId, v.ViolationTypeId)
	ORDER BY p.PermitNumber

END
