﻿



CREATE PROCEDURE [dbo].[PumpageReportSummaryByPermit]
(@PermitName varchar(24),
@BeginYear int = null,
@EndYear int = null)
AS
BEGIN

	-- exec [PumpageReportSummaryByPermit] 'DO1992G057(01)'
	-- exec [PumpageReportSummaryByPermit] 'PG1952G001'
	-- exec [PumpageReportSummaryByPermit] 'CO1988G021', 1999, 2007
	-- exec [PumpageReportSummaryByPermit] 'AA2005G020(01)'
	
	SELECT	Id PermitId, PermitNumber, PermitName, ReportYear, HighMonth, HighMonthAvgGalPerDay,
			case when [1] is null and [7] is null then null else AnnualAverageOverusePercentage end AnnualAverageOverusePercentage,
			case when [1] is null and [7] is null then null else AnnualAverageUsage end AnnualAverageUsage,
			case when [1] is null and [7] is null then null else MonthlyMaximumOverusePercentage end MonthlyMaximumOverusePercentage
			, AnnualTotalUsage,
			--isnull([1], 0) Month1,isnull([2], 0) Month2,isnull([3], 0) Month3,isnull([4], 0) Month4,isnull([5], 0) Month5,isnull([6], 0) Month6,
			--isnull([7], 0) Month7,isnull([8], 0) Month8,isnull([9], 0) Month9,isnull([10], 0) Month10,isnull([11], 0) Month11,isnull([12], 0) Month12
			[1] Month1,[2] Month2,[3] Month3,[4] Month4,[5] Month5,[6] Month6,
			[7] Month7,[8] Month8,[9] Month9,[10] Month10,[11] Month11,[12] Month12
	FROM	(
		SELECT	p.Id, p.PermitNumber, p.RevisionNumber, p.PermitName, MonthId, Gallons, year(cc.ComplianceReportingEndDate) ReportYear, max(cc.AnnualAverageOverusePercentage) AnnualAverageOverusePercentage, 
				max(cc.AnnualTotalUsage) AnnualTotalUsage, max(cc.AnnualAverageUsage) AnnualAverageUsage,
				max(cc.MonthlyMaximumOverusePercentage) MonthlyMaximumOverusePercentage,
				max(cc.HighMonth) HighMonth, max(cc.HighMonthAvgGalPerDay) HighMonthAvgGalPerDay
		from   MdeWaterPermitting..Permit p
			inner join MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14
			inner join MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId 
			left join MdeWaterPermitting..PumpageReport pr on pr.ConditionComplianceId = cc.Id and pr.Active = 1
			left join MdeWaterPermitting..PumpageReportDetail prd on prd.PumpageReportId = pr.Id
		WHERE	p.PermitName = case when charindex('(', @PermitName) > 0 or charindex('/', @PermitName) > 0 then @PermitName else p.PermitName end
			and p.PermitNumber = case when charindex('(', @PermitName) = 0 and charindex('/', @PermitName) = 0 then @PermitName else p.PermitNumber end
			and pc.RequiresSelfReporting = 1
			and isnull(@BeginYear, year(cc.ComplianceReportingEndDate)) <= year(cc.ComplianceReportingEndDate)
			and isnull(@EndYear, year(cc.ComplianceReportingEndDate)) >= year(cc.ComplianceReportingEndDate)
			and isnull(@EndYear, year(getdate()) - 1) >= year(cc.ComplianceReportingDueDate) - 1
		group by p.Id, p.PermitNumber, p.RevisionNumber, p.PermitName, MonthId, Gallons, year(cc.ComplianceReportingEndDate)
	) AS SourceTable
	PIVOT
	(
	SUM(Gallons)
	FOR MonthId IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
	) as pvt
	ORDER BY ReportYear, RevisionNumber 

END



