﻿


CREATE PROCEDURE [dbo].[MailInPayment]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN

	-- exec [MailInPayment] '10/2/2012', '2/4/2014'

	-- go through the whole day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	SELECT	@StartDate StartDate, @EndDate EndDate, PaidDate, p.Id TransactionId, CheckNumber,  
			cs.Amount, p.PermitNumber, product.Description 
	FROM	MDEPayment.dbo.ContactSale cs
		LEFT OUTER JOIN MdeWaterPermitting.dbo.Permit p on cs.PermitId = p.Id 
		INNER JOIN MDEPayment.dbo.Product product on cs.ProductId = product.Id
	WHERE	PaidDate is not null
		and PaidDate between @StartDate and @EndDate
		and ContactSaleStatusId = 2 -- paid
		and IsOnlinePayment = 0
	order by PaidDate
	
END





