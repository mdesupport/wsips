﻿-- ====================================================================
-- Author:           Dominic Cilento/Jon Harrison
-- Create date: 2013-04-02
-- Description:      Generates a Report based on the input list of PermitIds
-- ====================================================================
CREATE PROCEDURE [dbo].[PermitSearchResultReport]
       @ReportParameterId  int
       
AS
BEGIN

       -- exec [PermitSearchResultReport] 9

       DECLARE @PermitIds nvarchar(max);

       SELECT @PermitIds = ReportParameterValue 
       FROM   MdeWaterPermitting..ReportParameter
       WHERE  Id = @ReportParameterId;

    SELECT 
            IsNull(p.RamsWanActid, '')  ACTID
            , CASE
                    WHEN IsNull(RevisionNumber, '') <> ''
                    THEN p.PermitNumber + '(' + RevisionNumber + ')'
                    ELSE IsNull(p.PermitNumber, '')
            END AS PermitNumber
            , isnull(dbo.GetApplicantName(p.Id),'') ApplicantName
                     , isnull(dbo.GetPermitteeName(p.Id),'') PermitteeName
                     , isnull(dbo.GetPermitContactAddress(dbo.[GetPermitteeContactId](p.Id)), '') PermitteeAddress
                     , isnull(dbo.GetContactEmailAddress(dbo.[GetPermitteeEmailContactId](p.Id)), '') PermitteeEmail
                     , isnull(p.PWSID,'') PWSID
                     , Isnull(L_PC.Description,'') PermitCategory
            , IsNull(L_PS.Description, '') PermitStatus
            , IsNull(CONVERT(VARCHAR(10), p.ReceivedDate, 101), '') DateReceived
            , IsNull(CONVERT(VARCHAR(10), p.EffectiveDate, 101), '') EffectiveDate
            , IsNull(CONVERT(VARCHAR(10), p.ExpirationDate, 101), '') ExpirationDate
                     , IsNull(CONVERT(VARCHAR(10), p.AppropriationDate, 101), '') FirstAppropriationDate
                     , case when L_PS.[PermitCategoryId] in (2,3,4,5) then DATEDIFF(dd, p.ReceivedDate, getdate()) else 0 end DaysPending
                     , case when L_PC.Id <> 7 then IsNull(CONVERT(VARCHAR(10), p.DateOut, 101), '') else '' end DatePermitActivated
                     , isnull(CONVERT(varchar(10), dbo.GetPermitStatusChangeDate(p.Id, 49), 101), '') DatePermitInactivated
                     , case when L_PC.Id = 7 then IsNull(CONVERT(VARCHAR(10), p.DateOut, 101), '') else '' end DateExemptionActivated
            , IsNull(cty.Description, '') County
                     , IsNull(pwg.AvgGalPerDay, 0) + IsNull(pws.AvgGalPerDay, 0) AllocatedAverageGPD 
                     , IsNull(pwg.MaxGalPerDay, 0) + IsNull(pws.MaxGalPerDay, 0) ReqAllocGPDMax
                     , IsNull(p.Location, '') Location
                     , IsNull(gwd.TaxMapNumber, isnull(swd.TaxMapNumber, '')) TaxMapNumber
            , IsNull(gwd.TaxMapBlockNumber, isnull(swd.TaxMapBlockNumber, '')) TaxMapBlockNumber
            , IsNull(gwd.TaxMapParcel, isnull(swd.TaxMapParcel, '')) TaxMapParcel
                     , IsNull(L_wp1.Description, '') UseType1
            , IsNull(wp1.UsePercent, '') UseType1Percent
            , IsNull(L_wp2.Description, '') UseType2
            , IsNull(wp2.UsePercent, '') UseType2Percent
            , IsNull(L_wp3.Description, '') UseType3
            , IsNull(wp3.UsePercent, '') UseType3Percent
            , IsNull(L_wp4.Description, '') UseType4
            , IsNull(wp4.UsePercent, '') UseType4Percent
                     , IsNull(L_wt.Description, '') FreshwaterOrSaltwater
					 , IsNull(L_tt.Description, '') Tidal
                     , IsNull(L_aq.[Key], '') AquiferCode
            , IsNull(L_aq.Description, '') AquiferName
                     , isnull(gwd.WellTagNumber, '') WellTagNumber
                     , IsNull(L_ss.[Key], '') StreamCode
            , IsNull(L_ss.Description, '') StreamName
                     , IsNull(L_BasinSaltWater.[Key], IsNull(L_BasinGroundWater.[Key], '')) BasinCode
                     , IsNull(L_BasinSaltWater.Description, IsNull(L_BasinGroundWater.Description, '')) BasinName
                     , IsNull(p.USGSTopoMap, '')  USGSTopoMap
                     , IsNull(p.ADCPageLetterGrid, '') + '-' + IsNull(p.ADCXCoord, '') + IsNull(p.ADCYCoord, '') ADCMapInformation
                     , IsNull(p.Remarks, '')  Remarks
                     , isnull(L_crp.Description, '') Reporting
                     , case when isnull(p.AquiferTest, 0) = 1 then 'Yes' else 'No' end AquiferTest
                     , case when isnull(p.WaterQualityAnalysis, 0) = 1 then 'Yes' else 'No' end WaterQualityTest
                     , isnull(dbo.GetPermitContactAddress(dbo.GetPermitContactIdByContactTypeId(p.Id, 22)),'') PumpageMailingAddress 
                     , isnull(L_prt.Description, '') PumpageFormType

                     --, isnull(dbo.[GetContactName](pcontact.ContactId), '') AS ProjectManager
            --, isnull(dbo.[GetPermitContactNameByContactTypeId](p.Id, 24), '') AS Supervisor
            --, IsNull(L_AT.Description, '') AS ApplicationType
                     --, CASE 
            --        WHEN gwd.PermitId is not null
            --        THEN 'Ground'
            --        WHEN swd.PermitId is not null
            --        THEN 'Surface'
            --        ELSE NULL
            --END AS SurfaceOrGround
                     
            
    FROM   MdeWaterPermitting..Permit p
                     LEFT OUTER JOIN MdeWaterPermitting..PermitContact pcontact on p.Id = pcontact.PermitId and pcontact.ContactTypeId = 20 and pcontact.IsPrimary = 1
            LEFT OUTER JOIN MdeWaterPermitting..LU_PermitStatus L_PS on L_PS.Id = p.PermitStatusId
                     LEFT OUTER JOIN MdeWaterPermitting..LU_PermitCategory L_PC on L_PC.Id = L_PS.PermitCategoryId 
            --LEFT OUTER JOIN MdeWaterPermitting..LU_ApplicationType L_AT on L_AT.Id = p.ApplicationTypeId
            LEFT OUTER JOIN MdeWaterPermitting..LU_WaterType L_wt ON L_wt.Id = p.WaterTypeId
            LEFT OUTER JOIN MdeWaterPermitting..LU_TidalType L_tt ON L_tt.Id = p.TidalTypeId
                     LEFT OUTER JOIN MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and pc.StandardConditionTypeId = 14
                     LEFT OUTER JOIN MdeWaterPermitting..LU_ConditionReportingPeriod L_crp on L_crp.Id = pc.ConditionReportingPeriodId  
                     LEFT OUTER JOIN MdeWaterPermitting..LU_PumpageReportType L_prt on L_prt.Id = pc.PumpageReportTypeId 
            LEFT OUTER JOIN
                    (SELECT       pwg.PermitId, TaxMapNumber, TaxMapParcel, TaxMapBlockNumber, WellTagNumber, WatershedId  
                    FROM   MdeWaterPermitting..PermitWithdrawalGroundwater pwg
                        INNER JOIN    MdeWaterPermitting..PermitWithdrawalGroundwaterDetail pwgd
                                ON pwgd.PermitWithdrawalGroundwaterId = pwg.Id
                                        AND    pwgd.Id = (   SELECT min(Id)
                                                                    FROM       MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
                                                                    WHERE  PermitWithdrawalGroundwaterId = pwgd.PermitWithdrawalGroundwaterId)) gwd ON gwd.PermitId = p.Id
            LEFT OUTER JOIN
                    (SELECT       pws.PermitId, TaxMapNumber, TaxMapParcel, TaxMapBlockNumber, StateStreamId, WatershedId 
                    FROM   MdeWaterPermitting..PermitWithdrawalSurfacewater pws
                        INNER JOIN    MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail pwsd
                                ON pwsd.PermitWithdrawalSurfacewaterId = pws.Id
                                        AND    pwsd.Id = (   SELECT min(Id)
                                                                    FROM       MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail
                                                                    WHERE  PermitWithdrawalSurfacewaterId = pwsd.PermitWithdrawalSurfacewaterId)) swd ON swd.PermitId = p.Id
            LEFT OUTER JOIN
                    (SELECT       pc.PermitId, luc.Description
                    FROM   MdeWaterPermitting..PermitCounty pc
                        INNER JOIN MdeWaterPermitting..LU_County luc ON luc.Id = pc.CountyId) cty 
                    ON cty.PermitId = p.Id
            LEFT OUTER JOIN MdeWaterPermitting..PermitWaterDispersement pwd on pwd.PermitId = p.Id and pwd.Id = (SELECT min(id) FROM MdeWaterPermitting..PermitWaterDispersement pwd2 WHERE pwd2.PermitId = p.Id)
            LEFT OUTER JOIN MdeWaterPermitting..LU_Aquifer L_aq on L_aq.Id = p.AquiferId
            LEFT OUTER JOIN MdeWaterPermitting..LU_AquiferType L_aqt on L_aqt.Id = l_aq.AquiferTypeId 
            LEFT OUTER JOIN MdeWaterPermitting..LU_StateStream L_ss on L_ss.Id = swd.StateStreamId
            LEFT OUTER JOIN MdeWaterPermitting..LU_Watershed L_BasinSaltWater on L_BasinSaltWater.Id = swd.WatershedId
                     LEFT OUTER JOIN MdeWaterPermitting..LU_Watershed L_BasinGroundWater on L_BasinGroundWater.Id = gwd.WatershedId
            LEFT OUTER JOIN MdeWaterPermitting..PermitWaterWithdrawalPurpose wp1 on wp1.PermitId = p.Id and wp1.Sequence = 1
            LEFT OUTER JOIN MdeWaterPermitting..LU_WaterWithdrawalPurpose L_wp1 on L_wp1.Id = wp1.WaterWithdrawalPurposeId 
            LEFT OUTER JOIN MdeWaterPermitting..PermitWaterWithdrawalPurpose wp2 on wp2.PermitId = p.Id and wp2.Sequence = 2
            LEFT OUTER JOIN MdeWaterPermitting..LU_WaterWithdrawalPurpose L_wp2 on L_wp2.Id = wp2.WaterWithdrawalPurposeId 
            LEFT OUTER JOIN MdeWaterPermitting..PermitWaterWithdrawalPurpose wp3 on wp3.PermitId = p.Id and wp3.Sequence = 3
            LEFT OUTER JOIN MdeWaterPermitting..LU_WaterWithdrawalPurpose L_wp3 on L_wp3.Id = wp3.WaterWithdrawalPurposeId 
            LEFT OUTER JOIN MdeWaterPermitting..PermitWaterWithdrawalPurpose wp4 on wp4.PermitId = p.Id and wp4.Sequence = 4
            LEFT OUTER JOIN MdeWaterPermitting..LU_WaterWithdrawalPurpose L_wp4 on L_wp4.Id = wp4.WaterWithdrawalPurposeId 
            LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
                     LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater pws on pws.PermitId = p.Id
       
    WHERE p.Id IN (SELECT * FROM dbo.udfSplitIDs(@PermitIds, ','))
    ORDER BY PermitNumber; -- original

    -- DELETE FROM ReportParameter WHERE Id = @ReportParameterId;

END