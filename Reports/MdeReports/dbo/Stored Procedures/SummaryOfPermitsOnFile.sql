﻿


CREATE PROCEDURE [dbo].[SummaryOfPermitsOnFile]
AS
BEGIN

	-- exec [SummaryOfPermitsOnFile]

	SELECT	ps.[Key], ps.Description, count(*) cntTotal
	FROM	MdeWaterPermitting..Permit p
		INNER JOIN MdeWaterPermitting..LU_PermitStatus ps on p.PermitStatusId = ps.Id
	WHERE	 ps.PermitCategoryId = 6
	GROUP BY ps.[Key],ps.Description
	UNION 
	SELECT	'PP' [Key], 'Pending Permits' Descrip, 
		(select count(*)
		FROM	MdeWaterPermitting..Permit p
			INNER JOIN MdeWaterPermitting..LU_PermitStatus ps on p.PermitStatusId = ps.Id
		WHERE	 ps.PermitCategoryId IN (2,3,4))
	UNION
	SELECT	'AP' [Key], 'Applications' Descrip, 
		(SELECT count(*) cntTotal 
		FROM	MdeWaterPermitting..Permit p	
		WHERE	PermitStatusId = 3
			and ID not in (SELECT isnull(RefId,0) from MdeWaterPermitting..Permit )) cntTotal
	ORDER BY 1
END


