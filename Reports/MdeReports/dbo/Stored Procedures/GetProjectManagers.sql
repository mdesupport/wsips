﻿


CREATE PROCEDURE [dbo].[GetProjectManagers]
AS
BEGIN

	-- exec [GetProjectManagers]
	
	SELECT	t.Id, dbo.GetContactName(t.Id) Name, c.LastName 
	FROM	(SELECT	DISTINCT ContactId Id
			FROM	MdeWaterPermitting..PermitContact 
			WHERE	ContactTypeId = 20) t
		INNER JOIN MdeWaterPermitting..Contact c on c.Id = t.Id
	UNION
	SELECT	null, '- All Project Managers -', '-'
	ORDER BY LastName
	
END


