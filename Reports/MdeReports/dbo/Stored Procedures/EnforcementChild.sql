﻿




CREATE PROCEDURE [dbo].[EnforcementChild]
(@ViolationId int)
AS
BEGIN

--DECLARE @ViolationId int

--SET @ViolationId = 7

	-- exec [EnforcementChild] 7

	SELECT	isnull(et.Description,'No Action Taken') EnforcementAction,
			isnull(convert(varchar(20), e.ActionDate, 101), '') EnforcementDateOfAction,
			isnull(convert(varchar(20), e.ResponseReceivedDate, 101), '')  EnforcementReceivedDate,
			e.Comments
	FROM	MdeWaterPermitting.dbo.Violation v
		LEFT OUTER JOIN MdeWaterPermitting.dbo.PermitViolationEnforcement pve on v.Id = pve.ViolationId 
		LEFT OUTER JOIN MdeWaterPermitting.dbo.Enforcement e on e.Id = pve.EnforcementId
		LEFT OUTER JOIN MdeWaterPermitting.dbo.LU_EnforcementAction et on et.Id = e.EnforcementActionId
	WHERE	v.Id = @ViolationId
	ORDER BY e.ActionDate desc,e.ResponseReceivedDate desc

END

