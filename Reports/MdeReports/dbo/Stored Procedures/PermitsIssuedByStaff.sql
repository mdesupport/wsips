﻿




CREATE PROCEDURE [dbo].[PermitsIssuedByStaff]
(@ProjectManagerId int,
 @PermitTypeId int,
 @BeginDate datetime,
 @EndDate datetime)
AS
BEGIN

--DECLARE @ProjectManagerId int
--DECLARE @PermitTypeId int
--DECLARE @BeginDate datetime
--DECLARE @EndDate datetime

--SET @ProjectManagerId = null
--SET @PermitTypeId = null
--SET @BeginDate = '6/30/2012'
--SET @EndDate = '7/30/2012'

	-- exec [PermitsIssuedByStaff] null, null, '6/30/2012', '7/30/2012'

	-- go through the whole day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	if @BeginDate is not null and @EndDate is not null
	BEGIN
		
		SELECT	p.Id PermitId, p.PermitName PermitNumber, 
				isnull(dbo.GetContactName(pc.ContactId), 'Unknown') ProjectManagerName, 
				isnull(c.LastName, ' ') ProjectManagerLastName,
				isnull(pc.ContactId, 0) ProjectManagerId, 
				isnull(dbo.GetApplicantName(p.Id), '') ApplicantName,
				p.PermitTypeId, 
				case p.PermitTypeId 
					when 2 then 'Y' 
					when 4 then 'Y' 
					else 'N' end LargeUser,
				case p.ApplicationTypeId 
					when 1 then 'N' 
					when 2 then 'C' 
					when 3 then 'R' 
					when 4 then 'E' 
					when 5 then 'E' 
					else 'E' end ApplicationType,
				convert(varchar(12), DateOut,101) ActiveDate,
				DATEDIFF(d, isnull(p.ReceivedDate, DateOut), DateOut) Days,
				--convert(varchar(12), DateOut,101) ActiveDate,
				--DATEDIFF(d, isnull(p.ReceivedDate, DateOut), DateOut) Days,
				p.Remarks
		INTO #applications
		FROM	MdeWaterPermitting..Permit p
			LEFT OUTER JOIN MdeWaterPermitting..PermitContact pc on p.Id = pc.PermitId 
				and pc.ContactTypeId = 20
				and pc.IsPrimary = 1
			LEFT OUTER JOIN MdeWaterPermitting..Contact c on c.Id = pc.ContactId 
			--INNER JOIN (SELECT	isnull(dbo.GetPermitStatusChangeDate(Id, 46), 
			--	case 
			--		when datediff(d, effectivedate, dateout) between -31 and 31
			--		then dateout 
			--		else effectiveDate end) ActiveDate, Id, ReceivedDate, EffectiveDate, DateOut
			--			FROM	MdeWaterPermitting..Permit
			--			WHERE	EffectiveDate is not null
			--				and	EffectiveDate >= DATEADD(d,-31,@BeginDate) 
			--				and EffectiveDate <= DATEADD(d,31,@EndDate)) t on t.Id = p.Id and ActiveDate BETWEEN @BeginDate and dateadd(mi,1439, @EndDate)
		WHERE	p.PermitNumber is not null
			and PermitTypeId = isnull(@PermitTypeId, PermitTypeId)
			and isnull(dbo.GetPermitStatusChangeDate(PermitId, 46), '1/1/1900') between @begindate and @enddate

		SELECT	t.*, 
				isnull(cntTotal.Total, 0) cntTotal, 
				
				isnull(cntLargeCount.Total, 0) cntLargeCount,
				isnull(cntLargeCountNew.Total, 0) cntLargeCountNew,
				isnull(cntLargeCountRenewed.Total, 0) cntLargeCountRenewed,
				isnull(cntLargeCountChanged.Total, 0) cntLargeCountChanged,

				isnull(cnt730LargeCount.Total, 0) cnt730LargeCount,
				isnull(cnt730LargeCountNew.Total, 0) cnt730LargeCountNew,
				isnull(cnt730LargeCountRenewed.Total, 0) cnt730LargeCountRenewed,
				isnull(cnt730LargeCountChanged.Total, 0) cnt730LargeCountChanged,
				
				isnull(cnt30LargeCount.Total, 0) cnt30LargeCount,
				isnull(cnt30LargeCountNew.Total, 0) cnt30LargeCountNew,
				isnull(cnt30LargeCountRenewed.Total, 0) cnt30LargeCountRenewed,
				isnull(cnt30LargeCountChanged.Total, 0) cnt30LargeCountChanged,

				isnull(cnt90LargeCount.Total, 0) cnt90LargeCount,
				isnull(cnt90LargeCountNew.Total, 0) cnt90LargeCountNew,
				isnull(cnt90LargeCountRenewed.Total, 0) cnt90LargeCountRenewed,
				isnull(cnt90LargeCountChanged.Total, 0) cnt90LargeCountChanged,

				isnull(cntSmallCount.Total, 0) cntSmallCount,
				isnull(cntSmallCountNew.Total, 0) cntSmallCountNew,
				isnull(cntSmallCountRenewed.Total, 0) cntSmallCountRenewed,
				isnull(cntSmallCountChanged.Total, 0) cntSmallCountChanged,

				isnull(cnt120SmallCount.Total, 0) cnt120SmallCount,
				isnull(cnt120SmallCountNew.Total, 0) cnt120SmallCountNew,
				isnull(cnt120SmallCountRenewed.Total, 0) cnt120SmallCountRenewed,
				isnull(cnt120SmallCountChanged.Total, 0) cnt120SmallCountChanged,

				isnull(cnt30SmallCount.Total, 0) cnt30SmallCount,
				isnull(cnt30SmallCountNew.Total, 0) cnt30SmallCountNew,
				isnull(cnt30SmallCountRenewed.Total, 0) cnt30SmallCountRenewed,
				isnull(cnt30SmallCountChanged.Total, 0) cnt30SmallCountChanged,

				isnull(cnt90SmallCount.Total, 0) cnt90SmallCount,
				isnull(cnt90SmallCountNew.Total, 0) cnt90SmallCountNew,
				isnull(cnt90SmallCountRenewed.Total, 0) cnt90SmallCountRenewed,
				isnull(cnt90SmallCountChanged.Total, 0) cnt90SmallCountChanged

		FROM	#applications t
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications GROUP BY ProjectManagerId) cntTotal on cntTotal.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'Y' GROUP BY ProjectManagerId) cntLargeCount on cntLargeCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'Y' and ApplicationType = 'N' GROUP BY ProjectManagerId) cntLargeCountNew on cntLargeCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'Y' and ApplicationType = 'R' GROUP BY ProjectManagerId) cntLargeCountRenewed on cntLargeCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'Y' and ApplicationType = 'C' GROUP BY ProjectManagerId) cntLargeCountChanged on cntLargeCountChanged.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 730 and LargeUser = 'Y' GROUP BY ProjectManagerId) cnt730LargeCount on cnt730LargeCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 730 and LargeUser = 'Y' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt730LargeCountNew on cnt730LargeCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 730 and LargeUser = 'Y' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt730LargeCountRenewed on cnt730LargeCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 730 and LargeUser = 'Y' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt730LargeCountChanged on cnt730LargeCountChanged.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'Y' GROUP BY ProjectManagerId) cnt30LargeCount on cnt30LargeCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'Y' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt30LargeCountNew on cnt30LargeCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'Y' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt30LargeCountRenewed on cnt30LargeCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'Y' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt30LargeCountChanged on cnt30LargeCountChanged.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'Y' GROUP BY ProjectManagerId) cnt90LargeCount on cnt90LargeCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'Y' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt90LargeCountNew on cnt90LargeCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'Y' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt90LargeCountRenewed on cnt90LargeCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'Y' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt90LargeCountChanged on cnt90LargeCountChanged.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'N' GROUP BY ProjectManagerId) cntSmallCount on cntSmallCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'N' and ApplicationType = 'N' GROUP BY ProjectManagerId) cntSmallCountNew on cntSmallCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'N' and ApplicationType = 'R' GROUP BY ProjectManagerId) cntSmallCountRenewed on cntSmallCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'N' and ApplicationType = 'C' GROUP BY ProjectManagerId) cntSmallCountChanged on cntSmallCountChanged.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 120 and LargeUser = 'N' GROUP BY ProjectManagerId) cnt120SmallCount on cnt120SmallCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 120 and LargeUser = 'N' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt120SmallCountNew on cnt120SmallCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 120 and LargeUser = 'N' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt120SmallCountRenewed on cnt120SmallCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 120 and LargeUser = 'N' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt120SmallCountChanged on cnt120SmallCountChanged.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'N' GROUP BY ProjectManagerId) cnt30SmallCount on cnt30SmallCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'N' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt30SmallCountNew on cnt30SmallCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'N' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt30SmallCountRenewed on cnt30SmallCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 30 and LargeUser = 'N' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt30SmallCountChanged on cnt30SmallCountChanged.ProjectManagerId = t.ProjectManagerId

				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'N' GROUP BY ProjectManagerId) cnt90SmallCount on cnt90SmallCount.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'N' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt90SmallCountNew on cnt90SmallCountNew.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'N' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt90SmallCountRenewed on cnt90SmallCountRenewed.ProjectManagerId = t.ProjectManagerId
				LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days < 90 and LargeUser = 'N' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt90SmallCountChanged on cnt90SmallCountChanged.ProjectManagerId = t.ProjectManagerId



		WHERE	t.ProjectManagerId = isnull(@ProjectManagerId, t.ProjectManagerId)
		ORDER BY t.ProjectManagerLastName, t.PermitNumber 

	END
	-- drop table #applications

END






