﻿




CREATE PROCEDURE [dbo].[PumpageReportMissing]
AS
BEGIN

	-- exec [PumpageReportMissing]

	SELECT	p.Id, p.PermitName, EffectiveDate, ExpirationDate, 
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			dbo.GetPermitteeAddress(p.Id) PermitteeAddress,
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser, cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay
		
	FROM	(SELECT	PermitNumber, max(RevisionNumber) RevisionNumber--, min(PermitStatus) PermitStatus, 
			FROM	(SELECT	PermitNumber, PermitConditionComplianceStatusId, RevisionNumber, ConditionComplianceId--, case PermitStatusId when 46 then 0 else 1 end PermitStatus 
					FROM	(SELECT	ROW_NUMBER() OVER ( PARTITION BY p1.PermitNumber ORDER BY RevisionNumber desc, cc.ComplianceReportingDueDate DESC ) AS 'RowNumber',
									p1.PermitNumber, cc.PermitConditionComplianceStatusId, p1.RevisionNumber, cc.Id ConditionComplianceId --, p1.PermitStatusId  
							FROM	MdeWaterPermitting..Permit p1
								inner join MdeWaterPermitting..PermitCondition pc on pc.PermitId = p1.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1
								inner join MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId and cc.ComplianceReportingDueDate <= getdate() 
							) t
					WHERE	RowNumber <= 2
					--order by PermitNumber
					) u
	
			WHERE	PermitConditionComplianceStatusId in (3,4) 
			GROUP BY PermitNumber
			HAVING COUNT(*) = 2
			) v
		INNER JOIN	MdeWaterPermitting..Permit p on p.PermitNumber = v.PermitNumber and p.RevisionNumber = v.RevisionNumber 
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id
	WHERE	-- PermitStatus = 0 and
			PermitStatusId = 46
	ORDER BY p.PermitNumber ASC
	--order by PermitName

	/*
	SELECT	p.Id, p.PermitName, EffectiveDate, ExpirationDate, 
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			dbo.GetPermitteeAddress(p.Id) PermitteeAddress,
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser, cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay
		
	FROM	(SELECT	PermitId 
			FROM	(SELECT	PermitId, PumpageReportId --, ComplianceReportingDueDate, ConditionComplianceId
					FROM	(SELECT	ROW_NUMBER() OVER ( PARTITION BY p1.Id ORDER BY cc.ComplianceReportingDueDate DESC ) AS 'RowNumber',
									p1.Id PermitId,  pr.Id PumpageReportId --, cc.ComplianceReportingDueDate, cc.Id ConditionComplianceId
							FROM	MdeWaterPermitting..Permit p1
								inner join MdeWaterPermitting..PermitCondition pc on pc.PermitId = p1.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1
								inner join MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId and cc.ComplianceReportingDueDate <= getdate() 
								left outer join MdeWaterPermitting..PumpageReport pr on pr.ConditionComplianceId = cc.Id and pr.Active = 1
							WHERE	p1.PermitStatusId = 46 --and p1.PermitTypeId in (2,4)
							) t
					WHERE	RowNumber <= 2
					) u
	
			WHERE	PumpageReportId is null 
			GROUP BY PermitId
			HAVING COUNT(*) = 2
			) v
		INNER JOIN	MdeWaterPermitting..Permit p on p.Id = v.PermitId 
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id
	--WHERE	ExpirationDate > getdate()
	ORDER BY ExpirationDate ASC
	*/

END



