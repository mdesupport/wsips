﻿


 



CREATE PROCEDURE [dbo].[SummaryOfPermitsIssuedByStaff]
(@ProjectManagerId int,
 @PermitTypeId int,
 @BeginDate datetime,
 @EndDate datetime)
AS
BEGIN

--DECLARE @ProjectManagerId int
--DECLARE @PermitTypeId int
--DECLARE @BeginDate datetime
--DECLARE @EndDate datetime

--SET @ProjectManagerId = null
--SET @PermitTypeId = null
--SET @BeginDate = '12/1/2013'
--SET @EndDate = '12/6/2013'

	-- exec [SummaryOfPermitsIssuedByStaff] null, null, '12/1/2013', '12/6/2013'

	-- go through the whole day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	if @BeginDate is not null and @EndDate is not null
	BEGIN
		
		SELECT	isnull(dbo.GetContactName(pc.ContactId), 'Unknown') ProjectManagerName, 
				isnull(pc.ContactId, 0) ProjectManagerId, 
				p.PermitTypeId, 
				case p.PermitTypeId 
					when 2 then 'Y' 
					when 4 then 'Y' 
					else 'N' end LargeUser,
				isnull(c.LastName, ' ') LastName
		INTO #applications
		FROM	MdeWaterPermitting..Permit p
			LEFT OUTER JOIN MdeWaterPermitting..PermitContact pc on p.Id = pc.PermitId 
				and pc.ContactTypeId = 20
				and pc.IsPrimary = 1
			LEFT OUTER JOIN MdeWaterPermitting..Contact c on c.Id = pc.ContactId 
		WHERE	p.PermitNumber is not null
			and PermitTypeId = isnull(@PermitTypeId, PermitTypeId)
			and isnull(dbo.GetPermitStatusChangeDate(PermitId, 46), '1/1/1900') between @begindate and @enddate

		-- add in the current project managers
		SELECT	isnull(dbo.GetContactName(pm.ContactId), 'Unknown') ProjectManagerName,
				pm.ContactId ProjectManagerId, cast(0 as int) cntTotal, 
				cast(0 as int) cntLargeCount, cast(0 as int) cntSmallCount,
				c.LastName
		FROM	MdeWaterPermitting..ProjectManager pm
			INNER JOIN MdeWaterPermitting..Contact c on c.Id = pm.ContactId
		WHERE	pm.ContactId not in (select ProjectManagerId from #applications)
			and pm.ContactId = case when @ProjectManagerId is null then pm.ContactId else -1 end -- if the user selects a specific pm, then we don't want to join to the project managers table
		union
		SELECT	ProjectManagerName,t.ProjectManagerId,
				count(*) cntTotal,
				sum(case when LargeUser = 'Y' then 1 else 0 end) cntLargeCount,
				sum(case when LargeUser = 'N' then 1 else 0 end) cntSmallCount,
				LastName
		FROM	#applications t
		WHERE	t.ProjectManagerId = isnull(@ProjectManagerId, t.ProjectManagerId)
		GROUP BY ProjectManagerName,t.ProjectManagerId, LastName
		ORDER BY LastName

	END
	 --drop table #applications

END
