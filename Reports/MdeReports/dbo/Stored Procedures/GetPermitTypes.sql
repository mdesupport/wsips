﻿

CREATE PROCEDURE [dbo].[GetPermitTypes]
AS
BEGIN
	
	-- exec [GetPermitTypes]

	SELECT	Id, Description Name, Sequence
	FROM	MdeWaterPermitting..LU_PermitType 
	WHERE	Active = 1
	UNION
	SELECT	null, '- All Permit Types -', 0
	ORDER BY Sequence
	
END

