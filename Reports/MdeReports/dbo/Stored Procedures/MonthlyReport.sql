﻿



CREATE PROCEDURE [dbo].[MonthlyReport]
(	@BeginDate datetime,
	@EndDate datetime)
AS
BEGIN

	-- exec [MonthlyReport] '7/1/2013', '7/31/2013'
	
	declare @p1 int
	declare @p2 int
	declare @p3 int
	declare @p4 int
	declare @p5 int
	declare @p6 int

	-- go through the whole day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	-- applications received
	select	@p1 =count(distinct(p.Id))
	from	MdeWaterPermitting..PermitStatusHistory psh
		inner join MdeWaterPermitting..Permit p on p.Id = Psh.PermitId 
	where	psh.PermitStatusId = 3 
		AND	psh.createddate >= @BeginDate 
		AND psh.createddate <= @EndDate
		AND p.PermitTypeId <> 5

	-- applications withdrawn
	select	@p2 =count(distinct(p.Id))
	from	MdeWaterPermitting..PermitStatusHistory psh
		inner join MdeWaterPermitting..Permit p on p.Id = Psh.PermitId 
	where	psh.PermitStatusId IN (2,51,57,60,63,66)
		AND	psh.createddate >= @BeginDate 
		AND psh.createddate <= @EndDate
		AND p.PermitTypeId <> 5

	-- permits issued
	select	@p3 =count(distinct(p.Id))
	from	MdeWaterPermitting..PermitStatusHistory psh
		inner join MdeWaterPermitting..Permit p on p.Id = Psh.PermitId 
	where	psh.PermitStatusId = 46 
		AND	psh.createddate >= @BeginDate 
		AND psh.createddate <= @EndDate
		AND p.PermitTypeId <> 5

	-- permits inactivated
	select	@p4 =count(distinct(p.Id))
	from	MdeWaterPermitting..PermitStatusHistory psh
		inner join MdeWaterPermitting..Permit p on p.Id = Psh.PermitId 
	where	psh.PermitStatusId = 49
		AND	psh.createddate >= @BeginDate 
		AND psh.createddate <= @EndDate
		AND p.PermitTypeId <> 5

	-- applications denied
	select	@p5 =count(distinct(p.Id))
	from	MdeWaterPermitting..PermitStatusHistory psh
		inner join MdeWaterPermitting..Permit p on p.Id = Psh.PermitId 
	where	psh.PermitStatusId IN (47,53,61,64,67)
		AND	psh.createddate >= @BeginDate 
		AND psh.createddate <= @EndDate
		AND p.PermitTypeId <> 5

	-- applications pending
	select	@p6 = count(distinct(p.Id)) --	p.id, statusid, p.PermitStatusId 
	from	(
			select	p.Id, isnull(MdeReports.[dbo].[GetPermitStatusForPermitByDate](@enddate, p.Id), 0) StatusId
			from	MdeWaterPermitting..Permit p
			where	
			--isnull(ReceivedDate, @enddate) <= @enddate -- received prior to the end of the reporting period
				--and isnull(EffectiveDate, @enddate) >= @enddate -- effective after the reporting period
				PermitTypeId <> 5
				) t
	inner join MdeWaterPermitting..Permit p on p.Id = t.Id 
	where	statusId in (select id from MdeWaterPermitting.[dbo].[LU_PermitStatus] where PermitCategoryId in (2,3,4) and (id !=60 or id !=61)) 

	--select	@p6 = count(distinct(p.Id)) --	p.id, statusid, p.PermitStatusId 
	--from	(select	p.Id, isnull(MdeReports.[dbo].[GetPermitStatusForPermitByDate](@endDate, p.Id), 0) StatusId
	--		from	MdeWaterPermitting..Permit p
	--		where	isnull(ReceivedDate, @endDate) <= @endDate -- received prior to the end of the reporting period
	--			and isnull(EffectiveDate, @endDate) >= @endDate -- effective after the reporting period
	--			and PermitTypeId <> 5) t
	--	inner join MdeWaterPermitting..Permit p on p.Id = t.Id 
	--where	StatusId = any(select id from MdeWaterPermitting.[dbo].[LU_PermitStatus] where PermitCategoryId = 2 and id !=60 and id !=61 )  

	--select  @p6 =count(distinct(permitid))
	--from	MdeWaterPermitting..PermitStatusHistory
	--where	PermitStatusId = any(select id from MdeWaterPermitting.[dbo].[LU_PermitStatus] where PermitCategoryId = 2 and id !=60 and id !=61 )  
	--	AND	(createddate >= @beginDate AND createddate <= @endDate)

	select	@p1 as ApplicationsReceived,@p2 as ApplicationsWithdrawn, @p3 as PermitsIssued, 
			@p4 as PermitsInactivated , @p5 as ApplicationsDenied, @p6 as ApplicationsPending 

END



