﻿



CREATE PROCEDURE [dbo].[RequiresComplianceReporting]
(@StandardConditionTypeId int = null)
AS
BEGIN

	-- exec [RequiresComplianceReporting] 14

	SELECT	p.Id, p.PermitName, EffectiveDate, ExpirationDate, PWSID,
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			dbo.GetPermitteeAddress(p.Id) PermitteeAddress,
			case p.PermitTypeId
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser, cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay,
			sct.Description ConditionType
	FROM	MdeWaterPermitting..Permit p 
		INNER JOIN MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = isnull(@StandardConditionTypeId,StandardConditionTypeId)
		INNER JOIN MdeWaterPermitting..LU_StandardConditionType sct on sct.Id = pc.StandardConditionTypeId
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id
	WHERE	PermitStatusId = 46 and pc.RequiresSelfReporting = 1 -- only active permits that require self reporting
	ORDER BY PermitNumber

END


