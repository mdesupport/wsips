﻿



CREATE PROCEDURE [dbo].[OverAppropriation]
(@Year int)
AS
BEGIN

--DECLARE @YEAR int
--SET @YEAR = 2012

	SELECT	p.PermitNumber, 
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			[dbo].[GetTopTypeOfUse](p.Id) TypeOfUse,
			isnull([dbo].[GetSupplementalAndCombinedPermits](p.Id), '') SupplementalPermits,
			t.AvgGalPerDay PermittedAllocation,
			t.AnnualAverageUsage AvgGalPerDayUsage,
			t.AnnualAverageOverusePercentage 
	FROM	(SELECT p.Id PermitId, cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay,
					cc.AnnualAverageOverusePercentage , cc.AnnualAverageUsage  
			FROM	MdeWaterPermitting..Permit p 
				INNER JOIN MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1 
				INNER JOIN MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId 
				LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
				LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id
			WHERE	year(cc.ComplianceReportingEndDate) = @Year
			GROUP BY p.Id, pwg.AvgGalPerDay, psw.AvgGalPerDay,cc.AnnualAverageOverusePercentage, cc.AnnualAverageUsage ) T
		INNER JOIN MdeWaterPermitting..Permit p on p.Id = t.PermitId 
	WHERE	AvgGalPerDay > 50000
		and AnnualAverageOverusePercentage > 10
	ORDER BY PermitNumber

END
