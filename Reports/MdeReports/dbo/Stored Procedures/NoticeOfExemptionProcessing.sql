﻿




CREATE PROCEDURE [dbo].[NoticeOfExemptionProcessing]
AS
BEGIN
	
	-- exec [ExemptionReport]

	select	ApplicationTypeId, exMonth, exYear, exDate, exCount
	into #tmp
	from	(
			select	ApplicationTypeId, 
					month(psh.StatusChangeDate) exMonth, year(psh.StatusChangeDate) exYear, 
					cast(cast(month(psh.StatusChangeDate) as char(2)) + '/01/' + cast(year(psh.StatusChangeDate) as char(4)) as datetime) exDate, 
					count(*) exCount
			from	dbo.GetLatestPermitStatusHistoryDateByPermit(52) psh
				inner join MdeWaterPermitting..Permit p on psh.PermitId = p.Id
			group by ApplicationTypeId, month(psh.StatusChangeDate), year(psh.StatusChangeDate)
			) t 

	select	exMonth, exYear, exDate
			,isnull((select exCount from #tmp u where u.ApplicationTypeId = 4 and u.exDate = t.exDate),0) NewExemptions
			,isnull((select exCount from #tmp u where u.ApplicationTypeId = 5 and u.exDate = t.exDate),0) ExistingPermits
			, (select count(*) from dbo.GetLatestPermitStatusHistoryDateByPermit(52) where StatusChangeDate < dateadd(month,1, t.exDate)) RunningTotal
	from	#tmp t
	group by exMonth, exYear, exDate
	order by exDate DESC
	
END


