﻿

/* ------------------------------------------------------------------------------------------------------------------
Query for any permits that:
  - Have a permitstatusid = 46 
  - Have an associated permitcondition with standardconditiontypeid = 14
  - No conditioncompliance records are valid for the current date

  For any permits that meet the above:
	- Insert a new condition compliance record for that permitconditionid
	- Set start date, end date, and due date considering the conditionreportingperiodid value (annual or semi-annual)

Query for any permits that:
  - Have a permitstatusid = 50 (superseded) 
  - Have an associated permitcondition with standardconditiontypeid = 14
  - One ore more conditioncompliance records for that permitconditionid have a complianceduedate > current date
  - status is pending (don't delete ones that were possibly reported prior to due date)

  For any permits that meet the above:
	- Delete the conditions that have a complianceduedate > current date
------------------------------------------------------------------------------------------------------------------  */


CREATE PROCEDURE [dbo].[GenerateConditionComplianceRecords]
(@today Datetime)
AS
BEGIN

	-- exec [GenerateConditionComplianceRecords] '2/3/2014'
	
	-- create records
	INSERT INTO MdeWaterPermitting..ConditionCompliance
	SELECT	4 [PermitConditionComplianceStatusId], pc.Id[PermitConditionId], 
			case  
				when pc.ConditionReportingPeriodId = 1 then '01/01/' + cast(year(@today) as char(4)) -- annual
				when pc.ConditionReportingPeriodId = 3 and month(@today) < 7 then '01/01/' + cast(year(@today) as char(4)) -- semi-annual, first period of year
				else '07/01/' + cast(year(@today) as char(4))  -- semi-annual, second period of the year
			end [ComplianceReportingStartDate], 
			case
				when pc.ConditionReportingPeriodId = 1 then '12/31/' + cast(year(@today) as char(4)) -- annual
				when pc.ConditionReportingPeriodId = 3 and month(@today) < 7 then '06/30/' + cast(year(@today) as char(4)) -- semi-annual, first period of year
				else '12/31/' + cast(year(@today) as char(4))  -- semi-annual, second period of the year
			end [ComplianceReportingEndDate],
			case
				when pc.ConditionReportingPeriodId = 1 then '01/31/' + cast((year(@today) + 1) as char(4)) -- annual
				when pc.ConditionReportingPeriodId = 3 and month(@today) < 7 then '07/31/' + cast(year(@today) as char(4)) -- semi-annual, first period of year
				else '01/31/' + cast((year(@today) + 1) as char(4)) -- semi-annual, second period of the year
			end [ComplianceReportingDueDate], 
			0 [AnnualAverageUsage], 0 [AnnualAverageOverusePercentage], 0 [MonthlyMaximumOverusePercentage], null [AnnualTotalUsage], 
			null [ReportAverageUsage], null [ReportTotalUsage], null [HighMonth], null [HighMonthAvgGalPerDay], 
			'GenerateConditionComplianceRecords' [CreatedBy], getdate() [CreatedDate], 
			'GenerateConditionComplianceRecords' [LastModifiedBy], getdate() [LastModifiedDate]
	--SELECT	pc.PermitId, pc.ConditionReportingPeriodId, pc.Id PermitConditionId
	FROM	MdeWaterPermitting..Permit p
		inner join MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1 -- is supposed to report
		left outer join MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId and @today between cc.ComplianceReportingStartDate and cc.ComplianceReportingEndDate 
	WHERE	p.PermitStatusId = 46 -- active permits
		and cc.Id is null -- no condition compliance records found

	-- delete records
	DELETE 
	FROM	MdeWaterPermitting..ConditionCompliance
	WHERE	ID in (
				SELECT	cc.Id
				-- SELECT	pc.PermitId, pc.ConditionReportingPeriodId, pc.Id PermitConditionId, cc.ComplianceReportingStartDate, cc.ComplianceReportingEndDate, cc.ComplianceReportingDueDate 
				FROM	MdeWaterPermitting..Permit p
					inner join MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1 -- is supposed to report
					left outer join MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId 
				WHERE	p.PermitStatusId = 50 -- active permits
					and cc.Id is not null -- no condition compliance records found		
					and cc.ComplianceReportingDueDate > @today
					and cc.PermitConditionComplianceStatusId = 4 -- still in the pending status
				)
END

