﻿


CREATE PROCEDURE [dbo].[ApplicationsListingByCounty]
(@CountyId int,
 @PermitTypeId int)
AS
BEGIN

	-- exec [ApplicationsListingByCounty] null, null
	
	SELECT	pc.CountyId,
			c.Description CountyName, 
			p.PermitNumber + ' /' + isnull(p.RevisionNumber, '') PermitNumber, 
			isnull(dbo.GetApplicantName(p.Id), '') ApplicantName,
			isnull(dbo.GetContactInitials(pcontact.ContactId), 0) ProjectManagerInitials, 
			p.PermitTypeId,
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser,
			case p.ApplicationTypeId 
				when 1 then 'N' 
				when 2 then 'C' 
				when 3 then 'R' 
				when 4 then 'E' 
				when 5 then 'E' 
				else 'E' end ApplicationType,
			isnull(convert(varchar(12), p.ReceivedDate, 101), '') DateReceived,
			case p.RequiresAdvertising  
				when 1 then 'Y'
				else 'N' end HasAdvertised,
			DATEDIFF(d, isnull(p.ReceivedDate, getdate()), getdate()) Days,
			p.Remarks
	INTO    #applications
	FROM	MdeWaterPermitting..Permit p
		LEFT OUTER JOIN MdeWaterPermitting..PermitContact pcontact on p.Id = pcontact.PermitId 
			and pcontact.ContactTypeId = 20
			and pcontact.IsPrimary = 1
		INNER JOIN MdeWaterPermitting..LU_PermitStatus ps on ps.Id = p.PermitStatusId 
		INNER JOIN MdeWaterPermitting..PermitCounty pc on pc.PermitId = p.Id
		INNER JOIN MdeWaterPermitting..LU_County c on c.Id = pc.CountyId
	WHERE	p.PermitNumber is not null
		and	ps.PermitCategoryId in (1,2,3,4,5)

	SELECT	t.*, 
			isnull(cntTotal.Total, 0) cntTotal, 
			isnull(cntLarge.Total, 0) cntLarge,
			isnull(cntSmall.Total, 0) cntSmall
	FROM	#applications t
			LEFT OUTER JOIN (SELECT	CountyId, COUNT(*) Total FROM #applications GROUP BY CountyId) cntTotal on cntTotal.CountyId = t.CountyId
			LEFT OUTER JOIN (SELECT	CountyId, COUNT(*) Total FROM #applications WHERE LargeUser = 'Y' GROUP BY CountyId) cntLarge on cntLarge.CountyId = t.CountyId
			LEFT OUTER JOIN (SELECT	CountyId, COUNT(*) Total FROM #applications WHERE LargeUser = 'N' GROUP BY CountyId) cntSmall on cntSmall.CountyId = t.CountyId
	WHERE	t.PermitTypeId = isnull(@PermitTypeId, t.PermitTypeId)
		and t.CountyId = isnull(@CountyId, t.CountyId)
	ORDER BY t.CountyName, t.PermitNumber 
	 
END


