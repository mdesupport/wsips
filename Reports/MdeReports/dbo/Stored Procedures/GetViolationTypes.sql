﻿



CREATE PROCEDURE [dbo].[GetViolationTypes]
AS
BEGIN

	-- exec [GetViolationTypes]
	
	SELECT	Id, Description, Sequence
	FROM	MdeWaterPermitting.dbo.LU_ViolationType 
	UNION
	SELECT	null, '- All Violations -', 0
	ORDER BY Sequence, Description
	
END



