﻿


CREATE PROCEDURE [dbo].[GetStandardConditionTypes]
AS
BEGIN
	
	-- exec [GetStandardConditionTypes]

	SELECT	sct.Id, sct.Description Name, sct.Sequence
	FROM	MdeWaterPermitting..LU_StandardConditionType sct
		INNER JOIN [MdeWaterPermitting].[dbo].[LU_StandardCondition] sc on sc.StandardConditionTypeId = sct.Id
	WHERE	sct.Active = 1
		and sc.RequiresSelfReporting = 1
	UNION
	SELECT	null, '- All Standard Reporting Conditions -', 0
	ORDER BY Sequence
	
END


