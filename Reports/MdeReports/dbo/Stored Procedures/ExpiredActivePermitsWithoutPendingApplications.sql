﻿

CREATE PROCEDURE [dbo].[ExpiredActivePermitsWithoutPendingApplications]
AS
BEGIN

	-- exec [ExpiredActivePermitsWithoutPendingApplications] 
	
	SELECT	p.Id, p.PermitName, EffectiveDate, ExpirationDate, 
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			dbo.GetPermitteeAddress(p.Id) PermitteeAddress,
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser,
			cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay
	FROM	MdeWaterPermitting..Permit p
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id
	WHERE	p.PermitNumber is not null
		and	p.PermitStatusId = 46	-- active permit
		and p.Id not in (select id from MdeWaterPermitting..Permit p2 where p2.RefId is not null) -- No permit record exists in the system that has this permit's permitid in its refid column
		and p.ExpirationDate < getdate() -- permit that has expired
	ORDER BY ExpirationDate asc, PermitName

END





