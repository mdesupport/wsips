﻿


CREATE PROCEDURE [dbo].[FailedToReport]
(@StandardConditionTypeId int = null,
@BeginYear int = null,
@EndYear int = null)
AS
BEGIN

	-- exec [FailedToReport] 14, 2012, 2013

	SELECT	p.Id, p.PermitName, EffectiveDate, ExpirationDate, PWSID,
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			dbo.GetPermitteeAddress(p.Id) PermitteeAddress,
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser, cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay,
			cc.ComplianceReportingEndDate, sct.Description ConditionType
	FROM	MdeWaterPermitting..Permit p 
		INNER JOIN MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = isnull(@StandardConditionTypeId,StandardConditionTypeId)
		INNER JOIN MdeWaterPermitting..LU_StandardConditionType sct on sct.Id = pc.StandardConditionTypeId
		INNER JOIN MdeWaterPermitting..ConditionCompliance cc on cc.PermitConditionId = pc.Id
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
		LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id
	WHERE	p.PermitStatusId = 46   -- only active permits
		and pc.RequiresSelfReporting = 1 -- only permits that require self reporting
		and cc.PermitConditionComplianceStatusId in (3,4)	-- the condition compliance is either pending or overdue
		and year(cc.ComplianceReportingEndDate) >= isnull(@BeginYear,year(cc.ComplianceReportingEndDate))
		and year(cc.ComplianceReportingEndDate) <= isnull(@EndYear,year(cc.ComplianceReportingEndDate))  -- the reporting period is between the specified beginning and ending years
		and cc.ComplianceReportingDueDate <= getdate() -- must be something that is already due
	ORDER BY PermitNumber

END

