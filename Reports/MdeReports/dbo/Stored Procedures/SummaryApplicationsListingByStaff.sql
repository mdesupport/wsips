﻿





CREATE PROCEDURE [dbo].[SummaryApplicationsListingByStaff]
(@ProjectManagerId int,
 @PermitTypeId int)
AS
BEGIN

	-- exec [SummaryApplicationsListingByStaff] null, null
	
	SELECT	dbo.GetContactName(pcontact.ContactId) ProjectManagerName, 
			isnull(pcontact.ContactId, 0) ProjectManagerId, 
			c.Lastname ProjectManagerLastName,
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser
	INTO #applications
	FROM	MdeWaterPermitting..Permit p
		LEFT OUTER JOIN MdeWaterPermitting..PermitContact pcontact on p.Id = pcontact.PermitId 
			and pcontact.ContactTypeId = 20
			and pcontact.IsPrimary = 1
		INNER JOIN MdeWaterPermitting..LU_PermitStatus ps on ps.Id = p.PermitStatusId 
		INNER JOIN MdeWaterPermitting..Contact c on c.Id = pcontact.ContactId 
	WHERE	ps.PermitCategoryId in (2,3,4,5)
		and PermitTypeId = isnull(@PermitTypeId, PermitTypeId)
		and isnull(pcontact.ContactId, 0) = isnull(@ProjectManagerId, isnull(pcontact.ContactId, 0))

	SELECT	t.ProjectManagerName, 
			t.ProjectManagerLastName,
			isnull(cntTotal.Total, 0) cntTotal, 
			isnull(cntLarge.Total, 0) cntLarge,
			isnull(cntSmall.Total, 0) cntSmall
	FROM	#applications t
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications GROUP BY ProjectManagerId) cntTotal on cntTotal.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'Y' GROUP BY ProjectManagerId) cntLarge on cntLarge.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'N' GROUP BY ProjectManagerId) cntSmall on cntSmall.ProjectManagerId = t.ProjectManagerId
	GROUP BY t.ProjectManagerName, 
			t.ProjectManagerLastName,
			isnull(cntTotal.Total, 0) , 
			isnull(cntLarge.Total, 0) ,
			isnull(cntSmall.Total, 0) 
	ORDER BY t.ProjectManagerLastName

END





