﻿





CREATE PROCEDURE [dbo].[ApplicationsListingByStaff]
(@ProjectManagerId int,
 @PermitTypeId int)
AS
BEGIN

	-- exec [ApplicationsListingByStaff] null, null
	
	SELECT	p.PermitNumber + ' /' + isnull(p.RevisionNumber, '') PermitNumber, 
			dbo.GetContactName(pcontact.ContactId) ProjectManagerName, 
			isnull(c.LastName, ' ') ProjectManagerLastName,
			isnull(pcontact.ContactId, 0) ProjectManagerId, 
			isnull(dbo.GetApplicantName(p.Id), '') ApplicantName,
			p.PermitTypeId, 
			case p.PermitTypeId 
				when 2 then 'Y' 
				when 4 then 'Y' 
				else 'N' end LargeUser,
			case p.ApplicationTypeId 
				when 1 then 'N' 
				when 2 then 'C' 
				when 3 then 'R' 
				when 4 then 'E' 
				when 5 then 'E' 
				else 'E' end ApplicationType,
			isnull(convert(varchar(12), p.ReceivedDate, 101), '') DateReceived,
			case p.RequiresAdvertising  
				when 1 then 'Y'
				else 'N' end HasAdvertised,
			DATEDIFF(d, isnull(p.ReceivedDate, getdate()), getdate()) Days,
			p.Remarks
	INTO #applications
	FROM	MdeWaterPermitting..Permit p
		LEFT OUTER JOIN MdeWaterPermitting..PermitContact pcontact on p.Id = pcontact.PermitId 
			and pcontact.ContactTypeId = 20
			and pcontact.IsPrimary = 1
		LEFT OUTER JOIN MdeWaterPermitting..Contact c on c.Id = pcontact.ContactId 
		INNER JOIN MdeWaterPermitting..LU_PermitStatus ps on ps.Id = p.PermitStatusId 
	WHERE	p.PermitNumber is not null
		and	ps.PermitCategoryId in (1,2,3,4,5)

	SELECT	t.*, 
			isnull(cntTotal.Total, 0) cntTotal, 
			isnull(cntLarge.Total, 0) cntLarge,
			isnull(cntSmall.Total, 0) cntSmall,
			isnull(cnt730LargeCount.Total, 0) cnt730LargeCount,
			isnull(cnt730LargeCountNew.Total, 0) cnt730LargeCountNew,
			isnull(cnt730LargeCountRenewed.Total, 0) cnt730LargeCountRenewed,
			isnull(cnt730LargeCountChanged.Total, 0) cnt730LargeCountChanged,
			isnull(cnt120SmallCount.Total, 0) cnt120SmallCount,
			isnull(cnt120SmallCountNew.Total, 0) cnt120SmallCountNew,
			isnull(cnt120SmallCountRenewed.Total, 0) cnt120SmallCountRenewed,
			isnull(cnt120SmallCountChanged.Total, 0) cnt120SmallCountChanged
	FROM	#applications t
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications GROUP BY ProjectManagerId) cntTotal on cntTotal.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'Y' GROUP BY ProjectManagerId) cntLarge on cntLarge.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE LargeUser = 'N' GROUP BY ProjectManagerId) cntSmall on cntSmall.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 730 and LargeUser = 'Y' GROUP BY ProjectManagerId) cnt730LargeCount on cnt730LargeCount.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 730 and LargeUser = 'Y' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt730LargeCountNew on cnt730LargeCountNew.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 730 and LargeUser = 'Y' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt730LargeCountRenewed on cnt730LargeCountRenewed.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 730 and LargeUser = 'Y' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt730LargeCountChanged on cnt730LargeCountChanged.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 120 and LargeUser = 'N' GROUP BY ProjectManagerId) cnt120SmallCount on cnt120SmallCount.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 120 and LargeUser = 'N' and ApplicationType = 'N' GROUP BY ProjectManagerId) cnt120SmallCountNew on cnt120SmallCountNew.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 120 and LargeUser = 'N' and ApplicationType = 'R' GROUP BY ProjectManagerId) cnt120SmallCountRenewed on cnt120SmallCountRenewed.ProjectManagerId = t.ProjectManagerId
			LEFT OUTER JOIN (SELECT	ProjectManagerId, COUNT(*) Total FROM #applications WHERE Days > 120 and LargeUser = 'N' and ApplicationType = 'C' GROUP BY ProjectManagerId) cnt120SmallCountChanged on cnt120SmallCountChanged.ProjectManagerId = t.ProjectManagerId
	WHERE	t.ProjectManagerId = isnull(@ProjectManagerId, t.ProjectManagerId)
		and	t.PermitTypeId = isnull(@PermitTypeId, t.PermitTypeId)
	ORDER BY t.ProjectManagerLastName, t.PermitNumber 

END





