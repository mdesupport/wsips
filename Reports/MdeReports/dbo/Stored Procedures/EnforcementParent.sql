﻿



CREATE PROCEDURE [dbo].[EnforcementParent]
(@StartDate Datetime,
@EndDate Datetime,
@ViolationTypeId int = null)
AS
BEGIN

--DECLARE @StartDate Datetime
--DECLARE @EndDate Datetime
--DECLARE @ViolationTypeId int

--SET @StartDate = '1/1/2014'
--SET @EndDate = '3/1/2014'
--SET @ViolationTypeId = null

	-- go through the whole day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	SELECT	p.PermitNumber, 
			dbo.GetPermitteeName(p.Id) PermitteeName, 
			[dbo].[GetTopTypeOfUse](p.Id) TypeOfUse,
			v.FileNumber, vt.Description ViolationType,
			convert(varchar(12), v.CreatedDate, 101) ViolationDate,
			isnull(v.Status,'') Comments,
			case when ResolutionDate is null then 'No' else 'Yes' end Resolved,
			v.Id ViolationId
			-- ,p.Id PermitId, v.Id ViolationId, pve.Id PVEId, e.Id EnforcementId
	FROM	MdeWaterPermitting.dbo.Permit p
		INNER JOIN MdeWaterPermitting.dbo.Violation v on p.Id = v.PermitId
		INNER JOIN MdeWaterPermitting.dbo.LU_ViolationType vt on vt.Id = v.ViolationTypeId
	WHERE	v.CreatedDate between @StartDate and @EndDate
		and v.ViolationTypeId = isnull(@ViolationTypeId, v.ViolationTypeId)
	ORDER BY p.PermitNumber

END
