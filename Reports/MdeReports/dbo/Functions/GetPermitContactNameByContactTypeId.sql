﻿
CREATE FUNCTION [dbo].[GetPermitContactNameByContactTypeId](@permitId int, @contactTypeId int)
RETURNS varchar(122)
AS
BEGIN

	DECLARE @result varchar(122)

	SELECT	TOP 1 @result = dbo.GetPermitContactName(app.PermitContactInformationId)
	FROM	MdeWaterPermitting..PermitContact app 
	WHERE	PermitId = @permitId and app.ContactTypeId = @contactTypeId
	ORDER BY app.Id desc

	RETURN @result

END
