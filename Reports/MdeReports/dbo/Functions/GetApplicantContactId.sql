﻿
CREATE FUNCTION [dbo].[GetApplicantContactId](@permitId int)
RETURNS int
AS
BEGIN

	DECLARE @result int

	SELECT	TOP 1 @result = app.PermitContactInformationId
	FROM	MdeWaterPermitting..PermitContact app 
	WHERE	PermitId = @permitId and app.IsApplicant = 1 and app.IsPrimary = 1

	RETURN @result

END
