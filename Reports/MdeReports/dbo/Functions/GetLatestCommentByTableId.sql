﻿
CREATE FUNCTION [dbo].[GetLatestCommentByTableId](@refId int, @RefTableId int)
RETURNS varchar(122)
AS
BEGIN

	DECLARE @result varchar(122)

	SELECT	TOP 1 @result = Comment
	FROM	MdeWaterPermitting..Comment  
	WHERE	RefId = @refId 
		and RefTableId = @RefTableId
	ORDER BY Id DESC 

	RETURN @result

END
