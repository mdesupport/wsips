﻿
CREATE FUNCTION [dbo].[GetPermitContactName](@PermitContactInformationId int)
RETURNS varchar(122)
AS
BEGIN

	DECLARE @result varchar(122)

	if @PermitContactInformationId is null

		SET @result = ''

	else

		SELECT	@result = replace(ltrim(rtrim(case isBusiness when 1 then BusinessName else isnull(FirstName,'') + ' ' + isnull(MiddleInitial,'') + ' ' + isnull(LastName,'') end)), '  ', ' ')
		FROM	MdeWaterPermitting..PermitContactInformation 
		WHERE	Id = @PermitContactInformationId

	RETURN @result

END
