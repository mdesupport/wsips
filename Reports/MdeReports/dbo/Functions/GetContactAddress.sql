﻿
CREATE FUNCTION [dbo].[GetContactAddress](@ContactId int)
RETURNS varchar(270)
AS
BEGIN

	DECLARE @result varchar(270)

	if @ContactId is null

		SET @result = ''

	else

		select	@result = isnull(Address1 + ' ', '') + isnull(Address2 + ' ', '') + isnull(City + ', ', '') + isnull(s.[Key] + ' ', '') + isnull(ZipCode, '') 
		from	MdeWaterPermitting..Contact c
			left outer join MdeWaterPermitting..LU_State s on s.Id = c.StateId 
		where	c.id = @ContactId

	RETURN @result

END
