﻿
CREATE FUNCTION [dbo].[GetPermitStatusChangeDate](@PermitId int, @PermitStatusId int)
RETURNS datetime
AS
BEGIN

	DECLARE @result datetime

	SELECT	TOP 1 @result = CreatedDate
	FROM	MdeWaterPermitting..PermitStatusHistory 
	WHERE	PermitId = @PermitId
		and PermitStatusId = @PermitStatusId
	ORDER BY CreatedDate desc

	RETURN @result

END
