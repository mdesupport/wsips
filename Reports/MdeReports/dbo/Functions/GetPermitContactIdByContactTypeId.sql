﻿
CREATE FUNCTION [dbo].[GetPermitContactIdByContactTypeId](@permitId int, @contactTypeId int)
RETURNS int
AS
BEGIN

	DECLARE @result int

	SELECT	TOP 1 @result = app.PermitContactInformationId
	FROM	MdeWaterPermitting..PermitContact app 
	WHERE	PermitId = @permitId and app.ContactTypeId = @contactTypeId
	ORDER BY app.Id desc

	RETURN @result

END
