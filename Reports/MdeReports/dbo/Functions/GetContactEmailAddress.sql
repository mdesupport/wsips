﻿

CREATE FUNCTION [dbo].[GetContactEmailAddress](@ContactId int)
RETURNS varchar(50)
AS
BEGIN

	DECLARE @result varchar(122)

	if @ContactId is null

		SET @result = ''

	else

		SELECT	@result = CommunicationValue
		FROM	MdeWaterPermitting..ContactCommunicationMethod 
		WHERE	ContactId = @ContactId
			and IsPrimaryEmail = 1

	RETURN @result

END

