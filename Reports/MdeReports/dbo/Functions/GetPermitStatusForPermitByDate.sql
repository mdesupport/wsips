﻿

CREATE FUNCTION [dbo].[GetPermitStatusForPermitByDate](@EndDate datetime, @permitId int)
RETURNS int
AS
BEGIN

	-- go through the end of the day
	SET @EndDate = DateAdd(minute, 1439, @EndDate)

	DECLARE @result int

	select	top 1 @result = psh.PermitStatusId
	from	MdeWaterPermitting..PermitStatusHistory psh
	where	PermitId = @PermitId
		and CreatedDate <= @EndDate 
	order by CreatedDate Desc, PermitStatusId desc

	RETURN @result

END

