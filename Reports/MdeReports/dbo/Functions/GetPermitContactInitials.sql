﻿
CREATE FUNCTION [dbo].[GetPermitContactInitials](@PermitContactInformationId int)
RETURNS varchar(10)
AS
BEGIN

	DECLARE @result varchar(10)

	if @PermitContactInformationId is null

		SET @result = ''

	else

		SELECT	@result = replace(case isBusiness 
							when 1 then substring(BusinessName,1,3) 
							else substring(isnull(FirstName,' '), 1,1) + ' ' + substring(isnull(MiddleInitial,' '), 1,1) + ' ' + substring(isnull(LastName,' '), 1,1) end, ' ', '')
		FROM	MdeWaterPermitting..PermitContactInformation 
		WHERE	Id = @PermitContactInformationId

	RETURN @result

END
