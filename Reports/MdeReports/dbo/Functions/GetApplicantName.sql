﻿
CREATE FUNCTION [dbo].[GetApplicantName](@permitId int)
RETURNS varchar(122)
AS
BEGIN

	DECLARE @result varchar(122)

	SELECT	TOP 1 @result = dbo.GetPermitContactName(app.PermitContactInformationId)
	FROM	MdeWaterPermitting..PermitContact app 
	WHERE	PermitId = @permitId and app.IsApplicant = 1 and app.IsPrimary = 1

	RETURN @result

END
