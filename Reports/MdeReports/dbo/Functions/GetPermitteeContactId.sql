﻿
CREATE FUNCTION [dbo].[GetPermitteeContactId](@permitId int)
RETURNS int
AS
BEGIN

	DECLARE @result int

	SELECT	TOP 1 @result = app.PermitContactInformationId
	FROM	MdeWaterPermitting..PermitContact app 
	WHERE	PermitId = @permitId and app.IsPermittee = 1 and app.IsPrimary = 1

	RETURN @result

END
