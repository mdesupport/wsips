﻿

CREATE FUNCTION [dbo].[GetTopTypeOfUse](@permitId int)
RETURNS varchar(max)
AS
BEGIN

	DECLARE @result varchar(max)

	SELECT	top 1 @result =  lu.Description
	FROM	[MdeWaterPermitting].[dbo].[PermitWaterWithdrawalPurpose] p
		INNER JOIN [MdeWaterPermitting].[dbo].LU_WaterWithdrawalPurpose lu on lu.Id = p.[WaterWithdrawalPurposeId]
	WHERE	--p.Sequence = 1
			p.PermitId = @permitid
	ORDER BY p.UsePercent desc 

	RETURN @result

END

