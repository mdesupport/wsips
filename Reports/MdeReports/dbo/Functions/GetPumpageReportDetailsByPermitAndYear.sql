﻿

CREATE FUNCTION [dbo].[GetPumpageReportDetailsByPermitAndYear](@PermitId int, @Year int)

RETURNS @output TABLE 
(PermitId int, [Year] int, AnnualAverageOverusePercentage float, AnnualTotalUsage float, AnnualAverageUsage float, MonthlyMaximumOverusePercentage float,
Month1 int, Month2 int, Month3 int, Month4 int, Month5 int, Month6 int,
Month7 int, Month8 int, Month9 int, Month10 int, Month11 int, Month12 int)
AS
BEGIN

	insert into @output
	SELECT	@PermitId, @Year, AnnualAverageOverusePercentage, AnnualTotalUsage, AnnualAverageUsage, MonthlyMaximumOverusePercentage,
			[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]
	FROM	(
		SELECT	MonthId, Gallons, max(cc.AnnualAverageOverusePercentage) AnnualAverageOverusePercentage, 
				max(cc.AnnualTotalUsage) AnnualTotalUsage, max(cc.AnnualAverageUsage) AnnualAverageUsage,
				max(cc.MonthlyMaximumOverusePercentage) MonthlyMaximumOverusePercentage
		from   MdeWaterPermitting..Permit p
			inner join MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14
			inner join MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId and year(compliancereportingenddate) = @Year
			inner join MdeWaterPermitting..PumpageReport pr on pr.ConditionComplianceId = cc.Id and pr.Active = 1
			inner join MdeWaterPermitting..PumpageReportDetail prd on prd.PumpageReportId = pr.Id
		WHERE	p.id = @PermitId
		group by MonthId, Gallons
	) AS SourceTable
	PIVOT
	(
	SUM(Gallons)
	FOR MonthId IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
	) as pvt
	
	return 

	/*
	select	*
	from	MdeReports.dbo.[GetPumpageReportDetailsByPermitAndYear](72, 2012) t
	*/
	/*
		select	top 100 p.id --1,2,3,4,5,6,7,8,9,10,11,12
	from   MdeWaterPermitting..Permit p
		inner join MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14
		inner join MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId and year(compliancereportingenddate) = 2012--@year
		left outer join MdeWaterPermitting..PumpageReport pr on pr.ConditionComplianceId = cc.Id and pr.Active = 1
		left outer join MdeWaterPermitting..PumpageReportDetail prd on prd.PumpageReportId = pr.Id
	where	prd.id is not null
	group by p.id having count(*) > 6
	*/

END
