﻿

CREATE FUNCTION [dbo].[GetSupplementalAndCombinedPermits](@permitId int)
RETURNS varchar(max)
AS
BEGIN

	DECLARE @result varchar(max)

	select    @result = COALESCE(@result + ', ', '') + cast(p.PermitNumber as varchar(12))
	from   MdeWaterPermitting.dbo.PermitSupplementalGroup psg
		inner join MdeWaterPermitting.dbo.Permit p on p.Id = psg.PermitId 
	where  supplementalgroupid in
		(select       supplementalgroupid
		from   MdeWaterPermitting.dbo.PermitSupplementalGroup
		where  permitid = @permitId)
				and SupplementalTypeId = 2
		and p.Id <> @permitId

	RETURN @result

END

