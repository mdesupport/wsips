﻿

CREATE FUNCTION [dbo].[GetPermitteeAddress](@permitId int)
RETURNS varchar(122)
AS
BEGIN

	DECLARE @result varchar(122)

	SELECT	TOP 1 @result = dbo.GetPermitContactAddress(app.PermitContactInformationId)
	FROM	MdeWaterPermitting..PermitContact app 
	WHERE	PermitId = @permitId and app.IsPermittee = 1 and app.IsPrimary = 1

	RETURN @result

END

