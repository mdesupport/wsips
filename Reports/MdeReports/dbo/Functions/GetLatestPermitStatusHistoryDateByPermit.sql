﻿




CREATE FUNCTION [dbo].[GetLatestPermitStatusHistoryDateByPermit](@PermitStatusId int)

RETURNS @output TABLE 
(PermitId int, StatusChangeDate datetime)
AS
BEGIN

	insert into @output
	select	PermitId, max(CreatedDate) StatusChangeDate
	from	MdeWaterPermitting..PermitStatusHistory psh
	where	psh.PermitSTatusId = @PermitStatusId
	group by PermitId
	
	return 

END



