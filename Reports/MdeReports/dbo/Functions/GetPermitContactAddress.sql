﻿
CREATE FUNCTION [dbo].[GetPermitContactAddress](@PermitContactInformationId int)
RETURNS varchar(270)
AS
BEGIN

	DECLARE @result varchar(270)

	if @PermitContactInformationId is null

		SET @result = ''

	else

		select	@result = isnull(Address1 + ' ', '') + isnull(Address2 + ' ', '') + isnull(City + ', ', '') + isnull(s.[Key] + ' ', '') + isnull(ZipCode, '') 
		from	MdeWaterPermitting..PermitContactInformation c
			left outer join MdeWaterPermitting..LU_State s on s.Id = c.StateId 
		where	c.id = @PermitContactInformationId

	RETURN @result

END
