#Copy files to web directory

#First, remove the existing files
# Canton Group - do not remove and start from scratch
#"Removing existing directories"
#"..."

#Get-Childitem -path \\applications.jmt.corp.local\Websites\Build\WSIPS\Services\Communication | remove-item -recurse -force

#"Remove complete"

#Then, copy over the new files
"Copying new files"
"..."

#Copy-Item "$Env:TF_BUILD_DROPLOCATION\_PublishedWebsites\Service\*" \\applications.jmt.corp.local\Websites\Build\WSIPS\Services\Communication -recurse -force

$sourcePath = "\\tsclient\C\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS Code\Communication\MDGov.MDE.Communication\src\Service"
$destPath = "C:\Websites\Dev\WSIPS\Services\Communication"

Copy-Item "$sourcePath\favicon.ico" $destPath -force
Copy-Item "$sourcePath\Global.asax" $destPath -force
Copy-Item "$sourcePath\packages.config" $destPath -force

Copy-Item "$sourcePath\bin\*" "$destPath\bin" -recurse -force
Copy-Item "$sourcePath\Content\*" "$destPath\Content" -recurse -force
Copy-Item "$sourcePath\Images\*" "$destPath\Images" -recurse -force
Copy-Item "$sourcePath\Scripts\*" "$destPath\Scripts" -recurse -force
Copy-Item "$sourcePath\Views\*" "$destPath\Views" -recurse -force

"Copy complete"