#Copy files to build web directory

#First, remove the existing files
# Canton Group - do not remove and start from scratch
#"Removing existing directories"
#"..."

#Get-Childitem -path \\applications.jmt.corp.local\Websites\Build\WSIPS\Services\Ecommerce | remove-item -recurse -force

#"Remove complete"

#Then, copy over the new files
"Copying new files"
"..."

#Copy-Item "$Env:TF_BUILD_DROPLOCATION\_PublishedWebsites\EcommerceWebService\*" \\applications.jmt.corp.local\Websites\Build\WSIPS\Services\Ecommerce -recurse -force

$sourcePath = "\\tsclient\C\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS Code\Payment\MDGov.MDE.Payment\src\EcommerceWebService"
$destPath = "C:\Websites\Dev\WSIPS\Services\Ecommerce"

Copy-Item "$sourcePath\favicon.ico" $destPath -force
Copy-Item "$sourcePath\Global.asax" $destPath -force
Copy-Item "$sourcePath\packages.config" $destPath -force

Copy-Item "$sourcePath\Areas\HelpPage\HelpPage.css" "$destPath\Areas\HelpPage" -recurse -force
Copy-Item "$sourcePath\Areas\HelpPage\Views\*" "$destPath\Areas\HelpPage\Views" -recurse -force
Copy-Item "$sourcePath\bin\*" "$destPath\bin" -recurse -force
Copy-Item "$sourcePath\Content\*" "$destPath\Content" -recurse -force
Copy-Item "$sourcePath\Images\*" "$destPath\Images" -recurse -force
Copy-Item "$sourcePath\Scripts\*" "$destPath\Scripts" -recurse -force
Copy-Item "$sourcePath\Views\*" "$destPath\Views" -recurse -force

"Copy complete"