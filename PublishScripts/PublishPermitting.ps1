#Copy files to build web directory

#First, remove the existing files
# Canton Group - do not remove and start from scratch
#"Removing existing directories"
#"..."

#Get-Childitem -path \\applications.jmt.corp.local\Websites\Build\WSIPS\Services\Permitting | remove-item -recurse -force

#"Remove complete"

#Then, copy over the new files
"Copying new directories"
"..."

#Copy-Item "$Env:TF_BUILD_DROPLOCATION\_PublishedWebsites\Service\*" \\applications.jmt.corp.local\Websites\Build\WSIPS\Services\Permitting -recurse -force

$sourcePath = "\\tsclient\C\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS Code\Permitting\MDGov.MDE.Permitting\src\Service"
$destPath = "C:\Websites\Dev\WSIPS\Services\Permitting"

Copy-Item "$sourcePath\favicon.ico" $destPath -force
Copy-Item "$sourcePath\Global.asax" $destPath -force
Copy-Item "$sourcePath\packages.config" $destPath -force

Copy-Item "$sourcePath\bin\*" "$destPath\bin" -recurse -force
Copy-Item "$sourcePath\Content\*" "$destPath\Content" -recurse -force
Copy-Item "$sourcePath\Images\*" "$destPath\Images" -recurse -force
Copy-Item "$sourcePath\Scripts\*" "$destPath\Scripts" -recurse -force
Copy-Item "$sourcePath\Views\*" "$destPath\Views" -recurse -force

"Copy complete"