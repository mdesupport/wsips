#Copy files to build web directory

#First, remove the existing files
# Canton Group - do not remove and start from scratch
#"Removing existing directories"
#"..."

#Get-Childitem -path \\applications.jmt.corp.local\Websites\Build\WSIPS\Web\InternalWeb | where { $_ -notlike "arcgis" -and $_ -notlike "mdimap" } | remove-item -recurse -force
#Get-Childitem -path \\applications.jmt.corp.local\Websites\Build\WSIPS\Web\ExternalWeb | where { $_ -notlike "arcgis" -and $_ -notlike "mdimap" } | remove-item -recurse -force

#"Remove complete"

#Then, copy over the new files
"Copying new files"
"..."

#Copy-Item "$Env:TF_BUILD_DROPLOCATION\_PublishedWebsites\InternalWeb\*" \\applications.jmt.corp.local\Websites\Build\WSIPS\Web\InternalWeb -recurse -force

$sourcePath = "\\tsclient\C\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS Code\Portal\MDGov.MDE.WSIPS.Portal\src\InternalWeb"
$destPath = "C:\Websites\Dev\WSIPS\Web\InternalWeb"

Copy-Item "$sourcePath\favicon.ico" $destPath -force
Copy-Item "$sourcePath\Global.asax" $destPath -force
Copy-Item "$sourcePath\packages.config" $destPath -force
Copy-Item "$sourcePath\T4MVC.tt" $destPath -force
Copy-Item "$sourcePath\T4MVC.tt.hooks.t4" $destPath -force
Copy-Item "$sourcePath\T4MVC.tt.settings.xml" $destPath -force

Copy-Item "$sourcePath\Areas\Administration\Views\*" "$destPath\Areas\Administration\Views" -recurse -force
Copy-Item "$sourcePath\Areas\Ecommerce\Views\*" "$destPath\Areas\Ecommerce\Views" -recurse -force
Copy-Item "$sourcePath\Areas\Permitting\Views\*" "$destPath\Areas\Permitting\Views" -recurse -force
Copy-Item "$sourcePath\Areas\Reporting\Views\*" "$destPath\Areas\Reporting\Views" -recurse -force
Copy-Item "$sourcePath\bin\*" "$destPath\bin" -recurse -force
Copy-Item "$sourcePath\Content\*" "$destPath\Content" -recurse -force
Copy-Item "$sourcePath\Images\*" "$destPath\Images" -recurse -force
Copy-Item "$sourcePath\Scripts\*" "$destPath\Scripts" -recurse -force
Copy-Item "$sourcePath\Views\*" "$destPath\Views" -recurse -force

# Canton Group - not publishing external website yet
#Copy-Item "$Env:TF_BUILD_DROPLOCATION\_PublishedWebsites\ExternalWeb\*" \\applications.jmt.corp.local\Websites\Build\WSIPS\Web\ExternalWeb -recurse -force

"Copy complete"

# Now, remove the authentication section of the web.config that's required for local development
#"Removing Authentication section of web.config"
#"..."

#$webConfigFilePath = "\\applications.jmt.corp.local\Websites\Build\WSIPS\Web\InternalWeb\Web.config"
#[xml]$webConfig = Get-Content $webConfigFilePath

#$securityNode = $webConfig.configuration.'system.webServer'.security
#if ($securityNode.authentication)
#{
#    [Void]$securityNode.RemoveChild($securityNode.authentication)
#}

#"Remove complete"

#"Saving Web.config"
#"..."

#$webConfig.Save($webConfigFilePath)

#"Save complete"