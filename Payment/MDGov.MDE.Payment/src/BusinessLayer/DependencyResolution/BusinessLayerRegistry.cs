﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.BusinessLayer.DependencyResolution.BusinessLayerRegistry
// Assembly: MDGov.MDE.Payment.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2D877849-54C6-4A15-9A0C-CA25C4326B21
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Payment.BusinessLayer.Validation;
using MDGov.MDE.Payment.Model;
using StructureMap.Configuration.DSL;

namespace MDGov.MDE.Payment.BusinessLayer.DependencyResolution
{
  public class BusinessLayerRegistry : Registry
  {
    public BusinessLayerRegistry()
    {
      this.For<IValidator<GraduatedProductRule>>().Use<GraduatedProductRuleValidator>();
      this.For<IValidator<ContactSale>>().Use<ContactSaleValidator>();
      this.For<IValidator<Product>>().Use<ProductValidator>();
    }
  }
}
