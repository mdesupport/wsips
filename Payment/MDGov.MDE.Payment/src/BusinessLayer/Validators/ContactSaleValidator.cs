﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.BusinessLayer.Validation.ContactSaleValidator
// Assembly: MDGov.MDE.Payment.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2D877849-54C6-4A15-9A0C-CA25C4326B21
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Payment.Model;
using System;
using System.Collections;
using System.Linq.Expressions;

namespace MDGov.MDE.Payment.BusinessLayer.Validation
{
  public class ContactSaleValidator : AbstractValidator<ContactSale>
  {
    public ContactSaleValidator()
    {
      this.RuleFor<int>((Expression<Func<ContactSale, int>>) (t => t.ContactId)).NotEmpty<ContactSale, int>();
      this.RuleFor<string>((Expression<Func<ContactSale, string>>) (t => t.LastModifiedBy)).NotEmpty<ContactSale, string>();
      this.RuleFor<DateTime?>((Expression<Func<ContactSale, DateTime?>>) (t => t.PaidDate)).NotNull<ContactSale, DateTime?>().When<ContactSale, DateTime?>((Func<ContactSale, bool>) (t => t.ContactSaleStatusId == 2), ApplyConditionTo.AllValidators);
      this.RuleFor<int>((Expression<Func<ContactSale, int>>) (t => t.ContactSaleStatusId)).Equal<ContactSale, int>(2, (IEqualityComparer) null).When<ContactSale, int>((Func<ContactSale, bool>) (t => t.PaidDate.HasValue), ApplyConditionTo.AllValidators);
      this.RuleFor<bool?>((Expression<Func<ContactSale, bool?>>) (t => t.IsOnlinePayment)).NotNull<ContactSale, bool?>().When<ContactSale, bool?>((Func<ContactSale, bool>) (t => t.PaidDate.HasValue), ApplyConditionTo.AllValidators);
    }
  }
}
