﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.BusinessLayer.Validation.ProductValidator
// Assembly: MDGov.MDE.Payment.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2D877849-54C6-4A15-9A0C-CA25C4326B21
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Payment.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Payment.BusinessLayer.Validation
{
  public class ProductValidator : AbstractValidator<Product>
  {
    public ProductValidator()
    {
      this.RuleFor<int>((Expression<Func<Product, int>>) (t => t.ProductTypeId)).NotEmpty<Product, int>();
      this.RuleFor<string>((Expression<Func<Product, string>>) (t => t.Description)).NotEmpty<Product, string>();
      this.RuleFor<string>((Expression<Func<Product, string>>) (t => t.LastModifiedBy)).NotEmpty<Product, string>();
    }
  }
}
