﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.BusinessLayer.Validation.GraduatedProductRuleValidator
// Assembly: MDGov.MDE.Payment.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2D877849-54C6-4A15-9A0C-CA25C4326B21
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Payment.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Payment.BusinessLayer.Validation
{
  public class GraduatedProductRuleValidator : AbstractValidator<GraduatedProductRule>
  {
    public GraduatedProductRuleValidator()
    {
      this.RuleFor<int>((Expression<Func<GraduatedProductRule, int>>) (t => t.GraduatedProductId)).NotEmpty<GraduatedProductRule, int>();
      this.RuleFor<int>((Expression<Func<GraduatedProductRule, int>>) (t => t.ProductRuleOperatorId)).NotEmpty<GraduatedProductRule, int>();
      this.RuleFor<Decimal>((Expression<Func<GraduatedProductRule, Decimal>>) (t => t.Amount)).NotEmpty<GraduatedProductRule, Decimal>();
      this.RuleFor<string>((Expression<Func<GraduatedProductRule, string>>) (t => t.LastModifiedBy)).NotEmpty<GraduatedProductRule, string>();
    }
  }
}
