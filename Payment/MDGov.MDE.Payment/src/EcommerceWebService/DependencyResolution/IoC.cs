﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.DependencyResolution.IoC
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Payment.BusinessLayer.DependencyResolution;
using StructureMap;
using StructureMap.Graph;
using System;

namespace MDGov.MDE.Payment.EcommerceWebService.DependencyResolution
{
  public static class IoC
  {
    public static IContainer Initialize()
    {
      ObjectFactory.Initialize((Action<IInitializationExpression>) (x =>
      {
        x.Scan((Action<IAssemblyScanner>) (scan =>
        {
          scan.TheCallingAssembly();
          scan.WithDefaultConventions();
        }));
        x.AddRegistry<BusinessLayerRegistry>();
        x.AddRegistry<ServiceLayerRegistry>();
      }));
      return ObjectFactory.Container;
    }
  }
}
