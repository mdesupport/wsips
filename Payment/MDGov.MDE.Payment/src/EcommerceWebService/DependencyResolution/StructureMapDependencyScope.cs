﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.DependencyResolution.StructureMapDependencyScope
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using Microsoft.Practices.ServiceLocation;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;

namespace MDGov.MDE.Payment.EcommerceWebService.DependencyResolution
{
  public class StructureMapDependencyScope : ServiceLocatorImplBase, IDependencyScope, IDisposable
  {
    protected readonly IContainer Container;

    public StructureMapDependencyScope(IContainer container)
    {
      if (container == null)
        throw new ArgumentNullException("container");
      this.Container = container;
    }

    public void Dispose()
    {
      this.Container.Dispose();
    }

    public new object GetService(Type serviceType)
    {
      if (serviceType == (Type) null)
        return (object) null;
      try
      {
        return serviceType.IsAbstract || serviceType.IsInterface ? this.Container.TryGetInstance(serviceType) : this.Container.GetInstance(serviceType);
      }
      catch
      {
        return (object) null;
      }
    }

    public IEnumerable<object> GetServices(Type serviceType)
    {
      return this.Container.GetAllInstances(serviceType).Cast<object>();
    }

    protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
    {
      return this.Container.GetAllInstances(serviceType).Cast<object>();
    }

    protected override object DoGetInstance(Type serviceType, string key)
    {
      if (!string.IsNullOrEmpty(key))
        return this.Container.GetInstance(serviceType, key);
      if (!serviceType.IsAbstract && !serviceType.IsInterface)
        return this.Container.GetInstance(serviceType);
      return this.Container.TryGetInstance(serviceType);
    }
  }
}
