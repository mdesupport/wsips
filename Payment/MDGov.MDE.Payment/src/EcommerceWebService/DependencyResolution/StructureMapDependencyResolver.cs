﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.DependencyResolution.StructureMapDependencyResolver
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using StructureMap;
using System;
using System.Web.Http.Dependencies;

namespace MDGov.MDE.Payment.EcommerceWebService.DependencyResolution
{
  public class StructureMapDependencyResolver : StructureMapDependencyScope, IDependencyResolver, IDependencyScope, IDisposable
  {
    public StructureMapDependencyResolver(IContainer container)
      : base(container)
    {
    }

    public IDependencyScope BeginScope()
    {
      return (IDependencyScope) new StructureMapDependencyResolver(this.Container.GetNestedContainer());
    }
  }
}
