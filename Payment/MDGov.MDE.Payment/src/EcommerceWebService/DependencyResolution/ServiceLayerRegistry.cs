﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.DependencyResolution.ServiceLayerRegistry
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Logging;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Payment.Model;
using StructureMap;
using StructureMap.Configuration.DSL;
using System;
using System.Data.Entity;

namespace MDGov.MDE.Payment.EcommerceWebService.DependencyResolution
{
  public class ServiceLayerRegistry : Registry
  {
    public ServiceLayerRegistry()
    {
      this.For(typeof (IRepository<>)).Use(typeof (Repository<>));
      this.For<DbContext>().Use<ModelContext>();
      this.For<IRepository<ErrorLog>>().Use((Func<IContext, IRepository<ErrorLog>>) (ctx => (IRepository<ErrorLog>) new Repository<ErrorLog>(ctx.GetInstance<LoggingContext>())));
      this.For(typeof (IService<>)).Use(typeof (ServiceBase<>));
      this.For(typeof (IUpdatableService<>)).Use(typeof (UpdatableServiceBase<>));
      this.For<IErrorLogging>().Use<ErrorLogging>();
    }
  }
}
