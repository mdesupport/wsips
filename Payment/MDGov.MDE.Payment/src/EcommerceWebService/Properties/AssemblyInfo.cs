﻿using MDGov.MDE.Payment.EcommerceWebService.App_Start;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using WebActivator;

[assembly: AssemblyTitle("Service")]
[assembly: PreApplicationStartMethod(typeof (StructuremapMvc), "Start")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("JMT")]
[assembly: AssemblyProduct("Service")]
[assembly: AssemblyCopyright("Copyright © JMT 2012")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("bc0475a3-13a2-40fd-9664-4670d94e9a92")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
