﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.WebApiConfig
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{action}/{id}", (object) new
      {
        id = RouteParameter.Optional
      });
    }
  }
}
