﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.App_Start.StructuremapMvc
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Payment.EcommerceWebService.DependencyResolution;
using StructureMap;
using System.Web.Http;
using System.Web.Mvc;

namespace MDGov.MDE.Payment.EcommerceWebService.App_Start
{
  public static class StructuremapMvc
  {
    public static void Start()
    {
      IContainer container = IoC.Initialize();
      DependencyResolver.SetResolver((object) new StructureMapDependencyResolver(container));
      GlobalConfiguration.Configuration.DependencyResolver = (System.Web.Http.Dependencies.IDependencyResolver) new StructureMapDependencyResolver(container);
    }
  }
}
