﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Controllers.GraduatedProductRuleController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Payment.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService.Controllers
{
  public class GraduatedProductRuleController : ApiController
  {
    private readonly IRepository<GraduatedProductRule> _graduatedProductRuleRepository;
    private readonly IValidator<GraduatedProductRule> _graduatedProductRuleValidator;

    public GraduatedProductRuleController(IRepository<GraduatedProductRule> graduatedProductRuleRepository, IValidator<GraduatedProductRule> graduatedProductRuleValidator)
    {
      this._graduatedProductRuleRepository = graduatedProductRuleRepository;
      this._graduatedProductRuleValidator = graduatedProductRuleValidator;
    }

    [HttpGet]
    public IEnumerable<GraduatedProductRule> GetAll(string includeProperties)
    {
      return (IEnumerable<GraduatedProductRule>) this._graduatedProductRuleRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<GraduatedProductRule> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<GraduatedProductRule>) this._graduatedProductRuleRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public GraduatedProductRule GetById(int id, string includeProperties)
    {
      return this._graduatedProductRuleRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._graduatedProductRuleRepository.Count(filters);
    }

    [HttpPost]
    public int Save(GraduatedProductRule entity)
    {
      this._graduatedProductRuleValidator.ValidateAndThrow<GraduatedProductRule>(entity);
      return this._graduatedProductRuleRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._graduatedProductRuleRepository.Delete(id);
    }
  }
}
