﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Controllers.ProductController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Payment.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService.Controllers
{
  public class ProductController : ApiController
  {
    private readonly IRepository<Product> _productRepository;
    private readonly IValidator<Product> _productValidator;

    public ProductController(IRepository<Product> productRepository, IValidator<Product> productValidator)
    {
      this._productRepository = productRepository;
      this._productValidator = productValidator;
    }

    [HttpGet]
    public IEnumerable<Product> GetAll(string includeProperties)
    {
      return (IEnumerable<Product>) this._productRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Product> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Product>) this._productRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public Product GetById(int id, string includeProperties)
    {
      return this._productRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._productRepository.Count(filters);
    }

    [HttpPost]
    public int Save(Product entity)
    {
      this._productValidator.ValidateAndThrow<Product>(entity);
      if (entity.Active && (entity.ProductTypeId == 1 || entity.ProductTypeId == 2))
      {
        List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
        dynamicFilterList.Add(new DynamicFilter("ProductTypeId == @0", new object[1]
        {
          (object) entity.ProductTypeId
        }));
        if (entity.ApplicationTypeId.HasValue)
          dynamicFilterList.Add(new DynamicFilter("ApplicationTypeId == @0", new object[1]
          {
            (object) entity.ApplicationTypeId.Value
          }));
        if (entity.PermitTypeId.HasValue)
          dynamicFilterList.Add(new DynamicFilter("PermitTypeId == @0", new object[1]
          {
            (object) entity.PermitTypeId.Value
          }));
        foreach (Product entity1 in this._productRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) dynamicFilterList, (string) null).ToList<Product>())
        {
          entity1.Active = false;
          this._productRepository.Save(entity1);
        }
      }
      return this._productRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._productRepository.Delete(id);
    }
  }
}
