﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Controllers.GraduatedProductRuleGridModelController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Payment.Model;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService.Controllers
{
  public class GraduatedProductRuleGridModelController : ApiController
  {
    private readonly IRepository<GraduatedProductRule> _graduatedProductRuleRepository;

    public GraduatedProductRuleGridModelController(IRepository<GraduatedProductRule> graduatedProductRuleRepository)
    {
      this._graduatedProductRuleRepository = graduatedProductRuleRepository;
    }

    [HttpPost]
    public IEnumerable<GraduatedProductRuleGridModel> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<GraduatedProductRuleGridModel>) this._graduatedProductRuleRepository.GetRange(skip, take, orderBy, filters, includeProperties).Select<GraduatedProductRule, GraduatedProductRuleGridModel>((Expression<Func<GraduatedProductRule, GraduatedProductRuleGridModel>>) (pr => new GraduatedProductRuleGridModel()
      {
        Id = pr.Id,
        Rule = pr.LU_ProductRuleOperator.Description + " " + SqlFunctions.StringConvert((double?) (double) pr.ProductRuleOperand).Trim(),
        Amount = pr.Amount
      }));
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._graduatedProductRuleRepository.Count(filters);
    }
  }
}
