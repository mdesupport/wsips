﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Controllers.ProductGridModelController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Payment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService.Controllers
{
  public class ProductGridModelController : ApiController
  {
    private readonly IRepository<Product> _productRepository;

    public ProductGridModelController(IRepository<Product> productRepository)
    {
      this._productRepository = productRepository;
    }

    [HttpPost]
    public IEnumerable<ProductGridModel> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      includeProperties = "LU_ProductType,GraduatedProductRules";
      return this._productRepository.GetRange(skip, take, orderBy, filters, includeProperties).AsEnumerable<Product>().Select<Product, ProductGridModel>((Func<Product, ProductGridModel>) (product => new ProductGridModel()
      {
        Id = product.Id,
        ProductType = product.LU_ProductType.Description,
        Description = product.Description,
        Amount = product.IsGraduated ? product.GraduatedProductRules.Sum<GraduatedProductRule>((Func<GraduatedProductRule, Decimal>) (pr => pr.Amount)) : product.Amount.GetValueOrDefault(),
        CanBeActivated = !product.IsGraduated || product.GraduatedProductRules.Any<GraduatedProductRule>(),
        EffectiveDate = product.EffectiveDate.HasValue ? product.EffectiveDate.Value.ToShortDateString() : string.Empty,
        LastModified = product.LastModifiedDate.ToShortDateString(),
        Active = product.Active
      }));
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._productRepository.Count(filters);
    }
  }
}
