﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Controllers.ProductRuleOperatorController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Payment.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService.Controllers
{
  public class ProductRuleOperatorController : ApiController
  {
    private readonly IRepository<LU_ProductRuleOperator> _productRuleOperatorRepository;

    public ProductRuleOperatorController(IRepository<LU_ProductRuleOperator> productRuleOperatorRepository)
    {
      this._productRuleOperatorRepository = productRuleOperatorRepository;
    }

    [HttpGet]
    public IEnumerable<LU_ProductRuleOperator> GetAll(string includeProperties)
    {
      return (IEnumerable<LU_ProductRuleOperator>) this._productRuleOperatorRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_ProductRuleOperator> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_ProductRuleOperator>) this._productRuleOperatorRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public LU_ProductRuleOperator GetById(int id, string includeProperties)
    {
      return this._productRuleOperatorRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._productRuleOperatorRepository.Count(filters);
    }
  }
}
