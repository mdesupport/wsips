﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Controllers.ContactSaleController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Payment.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService.Controllers
{
  public class ContactSaleController : ApiController
  {
    private readonly IRepository<ContactSale> _contactSaleRepository;
    private readonly IValidator<ContactSale> _contactSaleValidator;

    public ContactSaleController(IRepository<ContactSale> contactSaleRepository, IValidator<ContactSale> contactSaleValidator)
    {
      this._contactSaleRepository = contactSaleRepository;
      this._contactSaleValidator = contactSaleValidator;
    }

    [HttpGet]
    public IEnumerable<ContactSale> GetAll(string includeProperties)
    {
      return (IEnumerable<ContactSale>) this._contactSaleRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<ContactSale> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<ContactSale>) this._contactSaleRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public ContactSale GetById(int id, string includeProperties)
    {
      return this._contactSaleRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._contactSaleRepository.Count(filters);
    }

    [HttpPost]
    public int Save(ContactSale entity)
    {
      this._contactSaleValidator.ValidateAndThrow<ContactSale>(entity);
      return this._contactSaleRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._contactSaleRepository.Delete(id);
    }
  }
}
