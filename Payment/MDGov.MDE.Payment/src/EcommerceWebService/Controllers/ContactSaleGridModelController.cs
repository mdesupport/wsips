﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Controllers.ContactSaleGridModelController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Payment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Payment.EcommerceWebService.Controllers
{
  public class ContactSaleGridModelController : ApiController
  {
    private readonly IRepository<ContactSale> _contactSaleRepository;

    public ContactSaleGridModelController(IRepository<ContactSale> contactSaleRepository)
    {
      this._contactSaleRepository = contactSaleRepository;
    }

    [HttpPost]
    public IEnumerable<ContactSaleGridModel> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      includeProperties = "Product,LU_ContactSaleStatus";
      return this._contactSaleRepository.GetRange(skip, take, orderBy, filters, includeProperties).AsEnumerable<ContactSale>().Select<ContactSale, ContactSaleGridModel>((Func<ContactSale, ContactSaleGridModel>) (invoice => new ContactSaleGridModel()
      {
        Id = invoice.Id,
        PermitNumber = invoice.PermitName,
        Description = invoice.Product.Description,
        Amount = invoice.Amount,
        InvoiceDate = invoice.CreatedDate.ToShortDateString(),
        Status = invoice.LU_ContactSaleStatus.Description,
        PaidDate = invoice.PaidDate.HasValue ? invoice.PaidDate.Value.ToShortDateString() : (string) null
      }));
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._contactSaleRepository.Count(filters);
    }
  }
}
