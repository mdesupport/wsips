﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.XmlDocumentationProvider
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Xml.XPath;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public class XmlDocumentationProvider : IDocumentationProvider
  {
    private const string _methodExpression = "/doc/members/member[@name='M:{0}']";
    private const string _parameterExpression = "param[@name='{0}']";
    private XPathNavigator _documentNavigator;

    public XmlDocumentationProvider(string documentPath)
    {
      if (documentPath == null)
        throw new ArgumentNullException("documentPath");
      this._documentNavigator = new XPathDocument(documentPath).CreateNavigator();
    }

    public virtual string GetDocumentation(HttpActionDescriptor actionDescriptor)
    {
      XPathNavigator methodNode = this.GetMethodNode(actionDescriptor);
      if (methodNode != null)
      {
        XPathNavigator xpathNavigator = methodNode.SelectSingleNode("summary");
        if (xpathNavigator != null)
          return xpathNavigator.Value.Trim();
      }
      return (string) null;
    }

    public virtual string GetDocumentation(HttpParameterDescriptor parameterDescriptor)
    {
      ReflectedHttpParameterDescriptor parameterDescriptor1 = parameterDescriptor as ReflectedHttpParameterDescriptor;
      if (parameterDescriptor1 != null)
      {
        XPathNavigator methodNode = this.GetMethodNode(parameterDescriptor1.ActionDescriptor);
        if (methodNode != null)
        {
          string name = parameterDescriptor1.ParameterInfo.Name;
          XPathNavigator xpathNavigator = methodNode.SelectSingleNode(string.Format((IFormatProvider) CultureInfo.InvariantCulture, "param[@name='{0}']", new object[1]
          {
            (object) name
          }));
          if (xpathNavigator != null)
            return xpathNavigator.Value.Trim();
        }
      }
      return (string) null;
    }

    private XPathNavigator GetMethodNode(HttpActionDescriptor actionDescriptor)
    {
      ReflectedHttpActionDescriptor actionDescriptor1 = actionDescriptor as ReflectedHttpActionDescriptor;
      if (actionDescriptor1 == null)
        return (XPathNavigator) null;
      return this._documentNavigator.SelectSingleNode(string.Format((IFormatProvider) CultureInfo.InvariantCulture, "/doc/members/member[@name='M:{0}']", new object[1]
      {
        (object) XmlDocumentationProvider.GetMemberName(actionDescriptor1.MethodInfo)
      }));
    }

    private static string GetMemberName(MethodInfo method)
    {
      string str = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0}.{1}", new object[2]
      {
        (object) method.DeclaringType.FullName,
        (object) method.Name
      });
      ParameterInfo[] parameters = method.GetParameters();
      if (parameters.Length != 0)
      {
        string[] array = ((IEnumerable<ParameterInfo>) parameters).Select<ParameterInfo, string>((Func<ParameterInfo, string>) (param => XmlDocumentationProvider.GetTypeName(param.ParameterType))).ToArray<string>();
        str += string.Format((IFormatProvider) CultureInfo.InvariantCulture, "({0})", new object[1]
        {
          (object) string.Join(",", array)
        });
      }
      return str;
    }

    private static string GetTypeName(Type type)
    {
      if (!type.IsGenericType)
        return type.FullName;
      Type genericTypeDefinition = type.GetGenericTypeDefinition();
      Type[] genericArguments = type.GetGenericArguments();
      string fullName = genericTypeDefinition.FullName;
      return string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0}{{{1}}}", new object[2]
      {
        (object) fullName.Substring(0, fullName.IndexOf('`')),
        (object) string.Join(",", ((IEnumerable<Type>) genericArguments).Select<Type, string>((Func<Type, string>) (t => XmlDocumentationProvider.GetTypeName(t))).ToArray<string>())
      });
    }
  }
}
