﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.Models.HelpPageApiModel
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using System.Web.Http.Description;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.Models
{
  public class HelpPageApiModel
  {
    public ApiDescription ApiDescription { get; set; }

    public IDictionary<MediaTypeHeaderValue, object> SampleRequests { get; private set; }

    public IDictionary<MediaTypeHeaderValue, object> SampleResponses { get; private set; }

    public Collection<string> ErrorMessages { get; private set; }

    public HelpPageApiModel()
    {
      this.SampleRequests = (IDictionary<MediaTypeHeaderValue, object>) new Dictionary<MediaTypeHeaderValue, object>();
      this.SampleResponses = (IDictionary<MediaTypeHeaderValue, object>) new Dictionary<MediaTypeHeaderValue, object>();
      this.ErrorMessages = new Collection<string>();
    }
  }
}
