﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.Controllers.HelpController
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.Models;
using System.Web.Http;
using System.Web.Mvc;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.Controllers
{
  public class HelpController : Controller
  {
    public HttpConfiguration Configuration { get; private set; }

    public HelpController()
    {
      this.Configuration = GlobalConfiguration.Configuration;
    }

    public ActionResult Index()
    {
      return (ActionResult) this.View((object) this.Configuration.Services.GetApiExplorer().ApiDescriptions);
    }

    public ActionResult Api(string apiId)
    {
      if (!string.IsNullOrEmpty(apiId))
      {
        HelpPageApiModel helpPageApiModel = this.Configuration.GetHelpPageApiModel(apiId);
        if (helpPageApiModel != null)
          return (ActionResult) this.View((object) helpPageApiModel);
      }
      return (ActionResult) this.View("Error");
    }
  }
}
