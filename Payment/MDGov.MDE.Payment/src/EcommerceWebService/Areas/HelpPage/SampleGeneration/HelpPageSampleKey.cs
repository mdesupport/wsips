﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.HelpPageSampleKey
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http.Headers;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public class HelpPageSampleKey
  {
    public string ControllerName { get; private set; }

    public string ActionName { get; private set; }

    public MediaTypeHeaderValue MediaType { get; private set; }

    public HashSet<string> ParameterNames { get; private set; }

    public Type ParameterType { get; private set; }

    public MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection? SampleDirection { get; private set; }

    public HelpPageSampleKey(MediaTypeHeaderValue mediaType, Type type)
    {
      if (mediaType == null)
        throw new ArgumentNullException("mediaType");
      if (type == (Type) null)
        throw new ArgumentNullException("type");
      this.ControllerName = string.Empty;
      this.ActionName = string.Empty;
      this.ParameterNames = new HashSet<string>((IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
      this.ParameterType = type;
      this.MediaType = mediaType;
    }

    public HelpPageSampleKey(MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection sampleDirection, string controllerName, string actionName, IEnumerable<string> parameterNames)
    {
      if (!Enum.IsDefined(typeof (MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection), (object) sampleDirection))
        throw new InvalidEnumArgumentException("sampleDirection", (int) sampleDirection, typeof (MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection));
      if (controllerName == null)
        throw new ArgumentNullException("controllerName");
      if (actionName == null)
        throw new ArgumentNullException("actionName");
      if (parameterNames == null)
        throw new ArgumentNullException("parameterNames");
      this.ControllerName = controllerName;
      this.ActionName = actionName;
      this.ParameterNames = new HashSet<string>(parameterNames, (IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
      this.SampleDirection = new MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection?(sampleDirection);
    }

    public HelpPageSampleKey(MediaTypeHeaderValue mediaType, MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection sampleDirection, string controllerName, string actionName, IEnumerable<string> parameterNames)
    {
      if (mediaType == null)
        throw new ArgumentNullException("mediaType");
      if (!Enum.IsDefined(typeof (MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection), (object) sampleDirection))
        throw new InvalidEnumArgumentException("sampleDirection", (int) sampleDirection, typeof (MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection));
      if (controllerName == null)
        throw new ArgumentNullException("controllerName");
      if (actionName == null)
        throw new ArgumentNullException("actionName");
      if (parameterNames == null)
        throw new ArgumentNullException("parameterNames");
      this.ControllerName = controllerName;
      this.ActionName = actionName;
      this.MediaType = mediaType;
      this.ParameterNames = new HashSet<string>(parameterNames, (IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
      this.SampleDirection = new MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection?(sampleDirection);
    }

    public override bool Equals(object obj)
    {
      HelpPageSampleKey helpPageSampleKey = obj as HelpPageSampleKey;
      if (helpPageSampleKey == null || !string.Equals(this.ControllerName, helpPageSampleKey.ControllerName, StringComparison.OrdinalIgnoreCase) || !string.Equals(this.ActionName, helpPageSampleKey.ActionName, StringComparison.OrdinalIgnoreCase) || this.MediaType != helpPageSampleKey.MediaType && (this.MediaType == null || !this.MediaType.Equals((object) helpPageSampleKey.MediaType)) || !(this.ParameterType == helpPageSampleKey.ParameterType))
        return false;
      MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection? sampleDirection1 = this.SampleDirection;
      MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.SampleDirection? sampleDirection2 = helpPageSampleKey.SampleDirection;
      if ((sampleDirection1.GetValueOrDefault() != sampleDirection2.GetValueOrDefault() ? 0 : (sampleDirection1.HasValue == sampleDirection2.HasValue ? 1 : 0)) != 0)
        return this.ParameterNames.SetEquals((IEnumerable<string>) helpPageSampleKey.ParameterNames);
      return false;
    }

    public override int GetHashCode()
    {
      int num = this.ControllerName.ToUpperInvariant().GetHashCode() ^ this.ActionName.ToUpperInvariant().GetHashCode();
      if (this.MediaType != null)
        num ^= this.MediaType.GetHashCode();
      if (this.SampleDirection.HasValue)
        num ^= this.SampleDirection.GetHashCode();
      if (this.ParameterType != (Type) null)
        num ^= this.ParameterType.GetHashCode();
      foreach (string parameterName in this.ParameterNames)
        num ^= parameterName.ToUpperInvariant().GetHashCode();
      return num;
    }
  }
}
