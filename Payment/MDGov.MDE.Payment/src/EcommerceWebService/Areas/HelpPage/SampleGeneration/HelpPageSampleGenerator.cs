﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.HelpPageSampleGenerator
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http.Description;
using System.Xml.Linq;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public class HelpPageSampleGenerator
  {
    public IDictionary<HelpPageSampleKey, Type> ActualHttpMessageTypes { get; internal set; }

    public IDictionary<HelpPageSampleKey, object> ActionSamples { get; internal set; }

    public IDictionary<Type, object> SampleObjects { get; internal set; }

    public HelpPageSampleGenerator()
    {
      this.ActualHttpMessageTypes = (IDictionary<HelpPageSampleKey, Type>) new Dictionary<HelpPageSampleKey, Type>();
      this.ActionSamples = (IDictionary<HelpPageSampleKey, object>) new Dictionary<HelpPageSampleKey, object>();
      this.SampleObjects = (IDictionary<Type, object>) new Dictionary<Type, object>();
    }

    public IDictionary<MediaTypeHeaderValue, object> GetSampleRequests(ApiDescription api)
    {
      return this.GetSample(api, SampleDirection.Request);
    }

    public IDictionary<MediaTypeHeaderValue, object> GetSampleResponses(ApiDescription api)
    {
      return this.GetSample(api, SampleDirection.Response);
    }

    public virtual IDictionary<MediaTypeHeaderValue, object> GetSample(ApiDescription api, SampleDirection sampleDirection)
    {
      if (api == null)
        throw new ArgumentNullException("api");
      string controllerName = api.ActionDescriptor.ControllerDescriptor.ControllerName;
      string actionName = api.ActionDescriptor.ActionName;
      IEnumerable<string> parameterNames = api.ParameterDescriptions.Select<ApiParameterDescription, string>((Func<ApiParameterDescription, string>) (p => p.Name));
      Collection<MediaTypeFormatter> formatters;
      Type type = this.ResolveType(api, controllerName, actionName, parameterNames, sampleDirection, out formatters);
      Dictionary<MediaTypeHeaderValue, object> dictionary = new Dictionary<MediaTypeHeaderValue, object>();
      foreach (KeyValuePair<HelpPageSampleKey, object> allActionSample in this.GetAllActionSamples(controllerName, actionName, parameterNames, sampleDirection))
        dictionary.Add(allActionSample.Key.MediaType, HelpPageSampleGenerator.WrapSampleIfString(allActionSample.Value));
      if (type != (Type) null && !typeof (HttpResponseMessage).IsAssignableFrom(type))
      {
        object sampleObject = this.GetSampleObject(type);
        foreach (MediaTypeFormatter formatter in formatters)
        {
          foreach (MediaTypeHeaderValue supportedMediaType in formatter.SupportedMediaTypes)
          {
            if (!dictionary.ContainsKey(supportedMediaType))
            {
              object sample = this.GetActionSample(controllerName, actionName, parameterNames, type, formatter, supportedMediaType, sampleDirection);
              if (sample == null && sampleObject != null)
                sample = this.WriteSampleObjectUsingFormatter(formatter, sampleObject, type, supportedMediaType);
              dictionary.Add(supportedMediaType, HelpPageSampleGenerator.WrapSampleIfString(sample));
            }
          }
        }
      }
      return (IDictionary<MediaTypeHeaderValue, object>) dictionary;
    }

    public virtual object GetActionSample(string controllerName, string actionName, IEnumerable<string> parameterNames, Type type, MediaTypeFormatter formatter, MediaTypeHeaderValue mediaType, SampleDirection sampleDirection)
    {
      object obj;
      if (!this.ActionSamples.TryGetValue(new HelpPageSampleKey(mediaType, sampleDirection, controllerName, actionName, parameterNames), out obj))
      {
        if (!this.ActionSamples.TryGetValue(new HelpPageSampleKey(mediaType, sampleDirection, controllerName, actionName, (IEnumerable<string>) new string[1]
        {
          "*"
        }), out obj) && !this.ActionSamples.TryGetValue(new HelpPageSampleKey(mediaType, type), out obj))
          return (object) null;
      }
      return obj;
    }

    public virtual object GetSampleObject(Type type)
    {
      object obj;
      if (!this.SampleObjects.TryGetValue(type, out obj))
        obj = new ObjectGenerator().GenerateObject(type);
      return obj;
    }

    public virtual Type ResolveType(ApiDescription api, string controllerName, string actionName, IEnumerable<string> parameterNames, SampleDirection sampleDirection, out Collection<MediaTypeFormatter> formatters)
    {
      if (!System.Enum.IsDefined(typeof (SampleDirection), (object) sampleDirection))
        throw new InvalidEnumArgumentException("sampleDirection", (int) sampleDirection, typeof (SampleDirection));
      if (api == null)
        throw new ArgumentNullException("api");
      Type type;
      if (!this.ActualHttpMessageTypes.TryGetValue(new HelpPageSampleKey(sampleDirection, controllerName, actionName, parameterNames), out type))
      {
        if (!this.ActualHttpMessageTypes.TryGetValue(new HelpPageSampleKey(sampleDirection, controllerName, actionName, (IEnumerable<string>) new string[1]
        {
          "*"
        }), out type))
        {
          switch (sampleDirection)
          {
            case SampleDirection.Request:
              ApiParameterDescription parameterDescription = api.ParameterDescriptions.FirstOrDefault<ApiParameterDescription>((Func<ApiParameterDescription, bool>) (p => p.Source == ApiParameterSource.FromBody));
              type = parameterDescription == null ? (Type) null : parameterDescription.ParameterDescriptor.ParameterType;
              formatters = api.SupportedRequestBodyFormatters;
              goto label_18;
            default:
              type = api.ActionDescriptor.ReturnType;
              formatters = api.SupportedResponseFormatters;
              goto label_18;
          }
        }
      }
      Collection<MediaTypeFormatter> collection = new Collection<MediaTypeFormatter>();
      foreach (MediaTypeFormatter formatter in (Collection<MediaTypeFormatter>) api.ActionDescriptor.Configuration.Formatters)
      {
        if (HelpPageSampleGenerator.IsFormatSupported(sampleDirection, formatter, type))
          collection.Add(formatter);
      }
      formatters = collection;
label_18:
      return type;
    }

    public virtual object WriteSampleObjectUsingFormatter(MediaTypeFormatter formatter, object value, Type type, MediaTypeHeaderValue mediaType)
    {
      if (formatter == null)
        throw new ArgumentNullException("formatter");
      if (mediaType == null)
        throw new ArgumentNullException("mediaType");
      object obj = (object) string.Empty;
      MemoryStream memoryStream = (MemoryStream) null;
      HttpContent content = (HttpContent) null;
      try
      {
        if (formatter.CanWriteType(type))
        {
          memoryStream = new MemoryStream();
          content = (HttpContent) new ObjectContent(type, value, formatter, mediaType);
          formatter.WriteToStreamAsync(type, value, (Stream) memoryStream, content, (TransportContext) null).Wait();
          memoryStream.Position = 0L;
          string str = new StreamReader((Stream) memoryStream).ReadToEnd();
          if (mediaType.MediaType.ToUpperInvariant().Contains("XML"))
            str = HelpPageSampleGenerator.TryFormatXml(str);
          else if (mediaType.MediaType.ToUpperInvariant().Contains("JSON"))
            str = HelpPageSampleGenerator.TryFormatJson(str);
          obj = (object) new TextSample(str);
        }
        else
          obj = (object) new InvalidSample(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "Failed to generate the sample for media type '{0}'. Cannot use formatter '{1}' to write type '{2}'.", (object) mediaType, (object) formatter.GetType().Name, (object) type.Name));
      }
      catch (Exception ex)
      {
        obj = (object) new InvalidSample(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "An exception has occurred while using the formatter '{0}' to generate sample for media type '{1}'. Exception message: {2}", (object) formatter.GetType().Name, (object) mediaType.MediaType, (object) ex.Message));
      }
      finally
      {
        if (memoryStream != null)
          memoryStream.Dispose();
        if (content != null)
          content.Dispose();
      }
      return obj;
    }

    private static string TryFormatJson(string str)
    {
      try
      {
        return JsonConvert.SerializeObject(JsonConvert.DeserializeObject(str), Newtonsoft.Json.Formatting.Indented);
      }
      catch
      {
        return str;
      }
    }

    private static string TryFormatXml(string str)
    {
      try
      {
        return XDocument.Parse(str).ToString();
      }
      catch
      {
        return str;
      }
    }

    private static bool IsFormatSupported(SampleDirection sampleDirection, MediaTypeFormatter formatter, Type type)
    {
      switch (sampleDirection)
      {
        case SampleDirection.Request:
          return formatter.CanReadType(type);
        case SampleDirection.Response:
          return formatter.CanWriteType(type);
        default:
          return false;
      }
    }

    private IEnumerable<KeyValuePair<HelpPageSampleKey, object>> GetAllActionSamples(string controllerName, string actionName, IEnumerable<string> parameterNames, SampleDirection sampleDirection)
    {
      HashSet<string> parameterNamesSet = new HashSet<string>(parameterNames, (IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
      foreach (KeyValuePair<HelpPageSampleKey, object> actionSample in (IEnumerable<KeyValuePair<HelpPageSampleKey, object>>) this.ActionSamples)
      {
        HelpPageSampleKey sampleKey = actionSample.Key;
        if (string.Equals(controllerName, sampleKey.ControllerName, StringComparison.OrdinalIgnoreCase) && string.Equals(actionName, sampleKey.ActionName, StringComparison.OrdinalIgnoreCase))
        {
          if (sampleKey.ParameterNames.SetEquals((IEnumerable<string>) new string[1]
          {
            "*"
          }) || parameterNamesSet.SetEquals((IEnumerable<string>) sampleKey.ParameterNames))
          {
            SampleDirection sampleDirection1 = sampleDirection;
            SampleDirection? sampleDirection2 = sampleKey.SampleDirection;
            if ((sampleDirection1 != sampleDirection2.GetValueOrDefault() ? 0 : (sampleDirection2.HasValue ? 1 : 0)) != 0)
              yield return actionSample;
          }
        }
      }
    }

    private static object WrapSampleIfString(object sample)
    {
      string text = sample as string;
      if (text != null)
        return (object) new TextSample(text);
      return sample;
    }
  }
}
