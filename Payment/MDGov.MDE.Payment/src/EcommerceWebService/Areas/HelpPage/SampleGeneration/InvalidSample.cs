﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.InvalidSample
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public class InvalidSample
  {
    public string ErrorMessage { get; private set; }

    public InvalidSample(string errorMessage)
    {
      if (errorMessage == null)
        throw new ArgumentNullException("errorMessage");
      this.ErrorMessage = errorMessage;
    }

    public override bool Equals(object obj)
    {
      InvalidSample invalidSample = obj as InvalidSample;
      if (invalidSample != null)
        return this.ErrorMessage == invalidSample.ErrorMessage;
      return false;
    }

    public override int GetHashCode()
    {
      return this.ErrorMessage.GetHashCode();
    }

    public override string ToString()
    {
      return this.ErrorMessage;
    }
  }
}
