﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.ImageSample
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public class ImageSample
  {
    public string Src { get; private set; }

    public ImageSample(string src)
    {
      if (src == null)
        throw new ArgumentNullException("src");
      this.Src = src;
    }

    public override bool Equals(object obj)
    {
      ImageSample imageSample = obj as ImageSample;
      if (imageSample != null)
        return this.Src == imageSample.Src;
      return false;
    }

    public override int GetHashCode()
    {
      return this.Src.GetHashCode();
    }

    public override string ToString()
    {
      return this.Src;
    }
  }
}
