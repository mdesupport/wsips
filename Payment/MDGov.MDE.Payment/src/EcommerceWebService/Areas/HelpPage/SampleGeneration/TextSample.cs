﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.TextSample
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public class TextSample
  {
    public string Text { get; private set; }

    public TextSample(string text)
    {
      if (text == null)
        throw new ArgumentNullException("text");
      this.Text = text;
    }

    public override bool Equals(object obj)
    {
      TextSample textSample = obj as TextSample;
      if (textSample != null)
        return this.Text == textSample.Text;
      return false;
    }

    public override int GetHashCode()
    {
      return this.Text.GetHashCode();
    }

    public override string ToString()
    {
      return this.Text;
    }
  }
}
