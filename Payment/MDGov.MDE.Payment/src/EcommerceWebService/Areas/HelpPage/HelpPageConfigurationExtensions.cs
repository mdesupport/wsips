﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.HelpPageConfigurationExtensions
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public static class HelpPageConfigurationExtensions
  {
    private const string ApiModelPrefix = "MS_HelpPageApiModel_";

    public static void SetDocumentationProvider(this HttpConfiguration config, IDocumentationProvider documentationProvider)
    {
      config.Services.Replace(typeof (IDocumentationProvider), (object) documentationProvider);
    }

    public static void SetSampleObjects(this HttpConfiguration config, IDictionary<Type, object> sampleObjects)
    {
      config.GetHelpPageSampleGenerator().SampleObjects = sampleObjects;
    }

    public static void SetSampleRequest(this HttpConfiguration config, object sample, MediaTypeHeaderValue mediaType, string controllerName, string actionName)
    {
      config.GetHelpPageSampleGenerator().ActionSamples.Add(new HelpPageSampleKey(mediaType, SampleDirection.Request, controllerName, actionName, (IEnumerable<string>) new string[1]
      {
        "*"
      }), sample);
    }

    public static void SetSampleRequest(this HttpConfiguration config, object sample, MediaTypeHeaderValue mediaType, string controllerName, string actionName, params string[] parameterNames)
    {
      config.GetHelpPageSampleGenerator().ActionSamples.Add(new HelpPageSampleKey(mediaType, SampleDirection.Request, controllerName, actionName, (IEnumerable<string>) parameterNames), sample);
    }

    public static void SetSampleResponse(this HttpConfiguration config, object sample, MediaTypeHeaderValue mediaType, string controllerName, string actionName)
    {
      config.GetHelpPageSampleGenerator().ActionSamples.Add(new HelpPageSampleKey(mediaType, SampleDirection.Response, controllerName, actionName, (IEnumerable<string>) new string[1]
      {
        "*"
      }), sample);
    }

    public static void SetSampleResponse(this HttpConfiguration config, object sample, MediaTypeHeaderValue mediaType, string controllerName, string actionName, params string[] parameterNames)
    {
      config.GetHelpPageSampleGenerator().ActionSamples.Add(new HelpPageSampleKey(mediaType, SampleDirection.Response, controllerName, actionName, (IEnumerable<string>) parameterNames), sample);
    }

    public static void SetSampleForType(this HttpConfiguration config, object sample, MediaTypeHeaderValue mediaType, Type type)
    {
      config.GetHelpPageSampleGenerator().ActionSamples.Add(new HelpPageSampleKey(mediaType, type), sample);
    }

    public static void SetActualRequestType(this HttpConfiguration config, Type type, string controllerName, string actionName)
    {
      config.GetHelpPageSampleGenerator().ActualHttpMessageTypes.Add(new HelpPageSampleKey(SampleDirection.Request, controllerName, actionName, (IEnumerable<string>) new string[1]
      {
        "*"
      }), type);
    }

    public static void SetActualRequestType(this HttpConfiguration config, Type type, string controllerName, string actionName, params string[] parameterNames)
    {
      config.GetHelpPageSampleGenerator().ActualHttpMessageTypes.Add(new HelpPageSampleKey(SampleDirection.Request, controllerName, actionName, (IEnumerable<string>) parameterNames), type);
    }

    public static void SetActualResponseType(this HttpConfiguration config, Type type, string controllerName, string actionName)
    {
      config.GetHelpPageSampleGenerator().ActualHttpMessageTypes.Add(new HelpPageSampleKey(SampleDirection.Response, controllerName, actionName, (IEnumerable<string>) new string[1]
      {
        "*"
      }), type);
    }

    public static void SetActualResponseType(this HttpConfiguration config, Type type, string controllerName, string actionName, params string[] parameterNames)
    {
      config.GetHelpPageSampleGenerator().ActualHttpMessageTypes.Add(new HelpPageSampleKey(SampleDirection.Response, controllerName, actionName, (IEnumerable<string>) parameterNames), type);
    }

    public static HelpPageSampleGenerator GetHelpPageSampleGenerator(this HttpConfiguration config)
    {
      return (HelpPageSampleGenerator) config.Properties.GetOrAdd((object) typeof (HelpPageSampleGenerator), (Func<object, object>) (k => (object) new HelpPageSampleGenerator()));
    }

    public static void SetHelpPageSampleGenerator(this HttpConfiguration config, HelpPageSampleGenerator sampleGenerator)
    {
      config.Properties.AddOrUpdate((object) typeof (HelpPageSampleGenerator), (Func<object, object>) (k => (object) sampleGenerator), (Func<object, object, object>) ((k, o) => (object) sampleGenerator));
    }

    public static HelpPageApiModel GetHelpPageApiModel(this HttpConfiguration config, string apiDescriptionId)
    {
      string str = "MS_HelpPageApiModel_" + apiDescriptionId;
      object apiModel;
      if (!config.Properties.TryGetValue((object) str, out apiModel))
      {
        ApiDescription apiDescription = config.Services.GetApiExplorer().ApiDescriptions.FirstOrDefault<ApiDescription>((Func<ApiDescription, bool>) (api => string.Equals(api.GetFriendlyId(), apiDescriptionId, StringComparison.OrdinalIgnoreCase)));
        if (apiDescription != null)
        {
          HelpPageSampleGenerator pageSampleGenerator = config.GetHelpPageSampleGenerator();
          apiModel = (object) HelpPageConfigurationExtensions.GenerateApiModel(apiDescription, pageSampleGenerator);
          config.Properties.TryAdd((object) str, apiModel);
        }
      }
      return (HelpPageApiModel) apiModel;
    }

    private static HelpPageApiModel GenerateApiModel(ApiDescription apiDescription, HelpPageSampleGenerator sampleGenerator)
    {
      HelpPageApiModel apiModel = new HelpPageApiModel();
      apiModel.ApiDescription = apiDescription;
      try
      {
        foreach (KeyValuePair<MediaTypeHeaderValue, object> sampleRequest in (IEnumerable<KeyValuePair<MediaTypeHeaderValue, object>>) sampleGenerator.GetSampleRequests(apiDescription))
        {
          apiModel.SampleRequests.Add(sampleRequest.Key, sampleRequest.Value);
          HelpPageConfigurationExtensions.LogInvalidSampleAsError(apiModel, sampleRequest.Value);
        }
        foreach (KeyValuePair<MediaTypeHeaderValue, object> sampleResponse in (IEnumerable<KeyValuePair<MediaTypeHeaderValue, object>>) sampleGenerator.GetSampleResponses(apiDescription))
        {
          apiModel.SampleResponses.Add(sampleResponse.Key, sampleResponse.Value);
          HelpPageConfigurationExtensions.LogInvalidSampleAsError(apiModel, sampleResponse.Value);
        }
      }
      catch (Exception ex)
      {
        apiModel.ErrorMessages.Add(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "An exception has occurred while generating the sample. Exception Message: {0}", new object[1]
        {
          (object) ex.Message
        }));
      }
      return apiModel;
    }

    private static void LogInvalidSampleAsError(HelpPageApiModel apiModel, object sample)
    {
      InvalidSample invalidSample = sample as InvalidSample;
      if (invalidSample == null)
        return;
      apiModel.ErrorMessages.Add(invalidSample.ErrorMessage);
    }
  }
}
