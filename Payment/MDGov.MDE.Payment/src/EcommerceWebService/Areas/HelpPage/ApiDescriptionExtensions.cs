﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.ApiDescriptionExtensions
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System.Text;
using System.Web;
using System.Web.Http.Description;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public static class ApiDescriptionExtensions
  {
    public static string GetFriendlyId(this ApiDescription description)
    {
      string[] strArray = description.RelativePath.Split('?');
      string str1 = strArray[0];
      string str2 = (string) null;
      if (strArray.Length > 1)
        str2 = string.Join("_", HttpUtility.ParseQueryString(strArray[1]).AllKeys);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendFormat("{0}-{1}", (object) description.HttpMethod.Method, (object) str1.Replace("/", "-").Replace("{", "").Replace("}", ""));
      if (str2 != null)
        stringBuilder.AppendFormat("_{0}", (object) str2);
      return stringBuilder.ToString();
    }
  }
}
