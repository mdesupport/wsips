﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage.HelpPageAreaRegistration
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System.Web.Http;
using System.Web.Mvc;

namespace MDGov.MDE.Payment.EcommerceWebService.Areas.HelpPage
{
  public class HelpPageAreaRegistration : AreaRegistration
  {
    public override string AreaName
    {
      get
      {
        return "HelpPage";
      }
    }

    public override void RegisterArea(AreaRegistrationContext context)
    {
      context.MapRoute("HelpPage_Default", "Help/{action}/{apiId}", (object) new
      {
        controller = "Help",
        action = "Index",
        apiId = UrlParameter.Optional
      });
      HelpPageConfig.Register(GlobalConfiguration.Configuration);
    }
  }
}
