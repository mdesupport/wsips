﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.EcommerceWebService.Helpers.Filters.HandleExceptionAttribute
// Assembly: MDGov.MDE.Payment.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EB97B0E-BC71-4CC0-BF3A-BCB21C065D0B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Service.dll

using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MDGov.MDE.Payment.EcommerceWebService.Helpers.Filters
{
  public class HandleExceptionAttribute : ExceptionFilterAttribute
  {
    public override void OnException(HttpActionExecutedContext actionExecutedContext)
    {
      throw new HttpResponseException(actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An exception has been caught", actionExecutedContext.Exception));
    }
  }
}
