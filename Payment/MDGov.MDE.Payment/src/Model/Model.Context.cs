﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.Model.ModelContext
// Assembly: MDGov.MDE.Payment.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2123DDC9-0DFA-417F-8422-D6EBBAACCE0A
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Model.dll

using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MDGov.MDE.Payment.Model
{
  public class ModelContext : DbContext
  {
    public DbSet<MDGov.MDE.Payment.Model.LU_ApplicationType> LU_ApplicationType { get; set; }

    public DbSet<MDGov.MDE.Payment.Model.LU_PaymentType> LU_PaymentType { get; set; }

    public DbSet<MDGov.MDE.Payment.Model.LU_PermitType> LU_PermitType { get; set; }

    public DbSet<MDGov.MDE.Payment.Model.LU_ProductRuleOperator> LU_ProductRuleOperator { get; set; }

    public DbSet<MDGov.MDE.Payment.Model.LU_ProductType> LU_ProductType { get; set; }

    public DbSet<Product> Products { get; set; }

    public DbSet<TableList> TableLists { get; set; }

    public DbSet<MDGov.MDE.Payment.Model.LU_ContactSaleStatus> LU_ContactSaleStatus { get; set; }

    public DbSet<ContactSale> ContactSales { get; set; }

    public DbSet<GraduatedProductRule> GraduatedProductRules { get; set; }

    public ModelContext()
      : base("name=ModelContext")
    {
      this.Configuration.ProxyCreationEnabled = false;
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      throw new UnintentionalCodeFirstException();
    }
  }
}
