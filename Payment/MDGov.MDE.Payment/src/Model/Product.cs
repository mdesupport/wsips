﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.Model.Product
// Assembly: MDGov.MDE.Payment.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2123DDC9-0DFA-417F-8422-D6EBBAACCE0A
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Payment.Model
{
  public class Product : IUpdatableEntity
  {
    public int Id { get; set; }

    public int ProductTypeId { get; set; }

    public int? ApplicationTypeId { get; set; }

    public int? PermitTypeId { get; set; }

    public bool IsGraduated { get; set; }

    public Decimal? Amount { get; set; }

    public string Description { get; set; }

    public DateTime? EffectiveDate { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_ApplicationType LU_ApplicationType { get; set; }

    public virtual LU_PermitType LU_PermitType { get; set; }

    public virtual LU_ProductType LU_ProductType { get; set; }

    public virtual ICollection<ContactSale> ContactSales { get; set; }

    public virtual ICollection<GraduatedProductRule> GraduatedProductRules { get; set; }

    public Product()
    {
      this.ContactSales = (ICollection<ContactSale>) new HashSet<ContactSale>();
      this.GraduatedProductRules = (ICollection<GraduatedProductRule>) new HashSet<GraduatedProductRule>();
    }
  }
}
