﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.Model.ProductGridModel
// Assembly: MDGov.MDE.Payment.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2123DDC9-0DFA-417F-8422-D6EBBAACCE0A
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Model.dll

using System;

namespace MDGov.MDE.Payment.Model
{
  public class ProductGridModel
  {
    public int Id { get; set; }

    public string ProductType { get; set; }

    public string Description { get; set; }

    public Decimal Amount { get; set; }

    public bool CanBeActivated { get; set; }

    public string EffectiveDate { get; set; }

    public string LastModified { get; set; }

    public bool Active { get; set; }
  }
}
