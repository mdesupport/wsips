﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.Model.LU_ProductRuleOperator
// Assembly: MDGov.MDE.Payment.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2123DDC9-0DFA-417F-8422-D6EBBAACCE0A
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Payment.Model
{
  public class LU_ProductRuleOperator : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool? Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<GraduatedProductRule> GraduatedProductRules { get; set; }

    public LU_ProductRuleOperator()
    {
      this.GraduatedProductRules = (ICollection<GraduatedProductRule>) new HashSet<GraduatedProductRule>();
    }
  }
}
