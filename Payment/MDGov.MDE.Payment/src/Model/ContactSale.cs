﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.Model.ContactSale
// Assembly: MDGov.MDE.Payment.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2123DDC9-0DFA-417F-8422-D6EBBAACCE0A
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Payment.Model
{
  public class ContactSale : IUpdatableEntity
  {
    public int Id { get; set; }

    public int ContactId { get; set; }

    public int? PermitId { get; set; }

    public string PermitName { get; set; }

    public int? ProductId { get; set; }

    public int ContactSaleStatusId { get; set; }

    public string OverriddenBy { get; set; }

    public DateTime? OverriddenDate { get; set; }

    public Decimal Amount { get; set; }

    public bool? IsOnlinePayment { get; set; }

    public string CheckNumber { get; set; }

    public DateTime? DepositDate { get; set; }

    public DateTime? PaidDate { get; set; }

    public short? PaymentFailCode { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_ContactSaleStatus LU_ContactSaleStatus { get; set; }

    public virtual Product Product { get; set; }
  }
}
