﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.Model.ContactSaleGridModel
// Assembly: MDGov.MDE.Payment.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2123DDC9-0DFA-417F-8422-D6EBBAACCE0A
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Model.dll

using System;

namespace MDGov.MDE.Payment.Model
{
  public class ContactSaleGridModel
  {
    public int Id { get; set; }

    public string PermitNumber { get; set; }

    public string Description { get; set; }

    public Decimal Amount { get; set; }

    public string InvoiceDate { get; set; }

    public string Status { get; set; }

    public string PaidDate { get; set; }

    public bool HasRelatedPenalty { get; set; }
  }
}
