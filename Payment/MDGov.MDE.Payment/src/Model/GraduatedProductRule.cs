﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Payment.Model.GraduatedProductRule
// Assembly: MDGov.MDE.Payment.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2123DDC9-0DFA-417F-8422-D6EBBAACCE0A
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Ecommerce\bin\MDGov.MDE.Payment.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Payment.Model
{
  public class GraduatedProductRule : IUpdatableEntity
  {
    public int Id { get; set; }

    public int GraduatedProductId { get; set; }

    public int ProductRuleOperatorId { get; set; }

    public long ProductRuleOperand { get; set; }

    public Decimal Amount { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_ProductRuleOperator LU_ProductRuleOperator { get; set; }

    public virtual Product Product { get; set; }
  }
}
