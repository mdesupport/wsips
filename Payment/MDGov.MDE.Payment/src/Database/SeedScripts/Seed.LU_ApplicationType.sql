﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ApplicationType] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_ApplicationType] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, 'NA', 'New Permit Application', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, 'MOD', 'Modification to Permit', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, 'RE', 'Renewal', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, 'NEX', 'New Exemption Application', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, 'PEX', 'Existing Permit to Exemption', 50, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_ApplicationType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ApplicationType] OFF;