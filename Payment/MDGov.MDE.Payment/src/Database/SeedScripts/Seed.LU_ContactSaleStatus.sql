﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ContactSaleStatus] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_ContactSaleStatus] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL,'Invoice Generated', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL,'Paid', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL,'Payment Failed', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL,'Refund', 40, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_ContactSaleStatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ContactSaleStatus] OFF;