﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ProductRuleOperator] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_ProductRuleOperator] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL,'>', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL,'<', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL,'>=', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL,'<=', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL,'=', 50, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_ProductRuleOperator]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ProductRuleOperator] OFF;