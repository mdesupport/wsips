﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ProductType] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_ProductType] (Id, [Key], Description, Sequence, CanBeGraduated, PcaCode, AobjCode, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL,'Water Appropriation Application Fee', 10, 1, 13792, 5823, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL,'Water Appropriation Usage Fee', 20, 1, 13793, 7197, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL,'Water Appropriation Advertisement Fee', 30, 0, 43551, 801, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL,'Water Appropriation Penalty', 40, 0, 13794, 7338, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_ProductType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ProductType] OFF;