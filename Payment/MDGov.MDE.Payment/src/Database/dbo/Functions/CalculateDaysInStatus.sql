﻿CREATE FUNCTION [dbo].[CalculateDaysInStatus](@StartDate datetime,
	@EndDate datetime, @PermitId int, @PermitStatusId int)
RETURNS int
AS
BEGIN

--DECLARE @StartDate datetime
--DECLARE @EndDate datetime
--DECLARE @PermitId int
--DECLARE @PermitStatusId int

--SET @StartDate = '8/1/2012'
--SET @EndDate = '7/31/2013'
--SET @PermitId = 1
--SET	@PermitStatusId = 46

--select dbo.CalculateDaysInStatus('8/1/2012', '7/31/2013', 1, 46)

	DECLARE @days int
	SET @days = 0

	-- change the end date to go through the entire day
	SET @EndDate = DateAdd(d, 1, @EndDate)

	DECLARE @TempTable TABLE (Id int IDENTITY (1,1), CreateDate datetime, 
		PermitStatusHistoryId int, PermitStatusId int )

	INSERT INTO @TempTable
	SELECT	CreateDate,Id, PermitStatusId
	FROM (	SELECT	TOP 1 @StartDate CreateDate, Id, PermitStatusId
			FROM	[MdeWaterPermitting].[dbo].[PermitStatusHistory]
			WHERE	PermitId = @PermitId
				and CreatedDate < @StartDate
			ORDER BY CreatedDate  DESC, PermitStatusId DESC
			UNION ALL
			SELECT	cast(convert(varchar(10),[CreatedDate], 101) as datetime), Id, PermitStatusId
			FROM	[MdeWaterPermitting].[dbo].[PermitStatusHistory]
			WHERE	permitid = @PermitId
				and CreatedDate <= @Enddate and CreatedDate >= @StartDate	
			UNION ALL
			SELECT	@EndDate, 0,  0) t
	ORDER BY CreateDate

	select	@days = @days + datediff(d, CreateDate, (select CreateDate from @TempTable where id = td.Id + 1 ))
	from	@TempTable td
	where	td.Id < (select max(id) from @TempTable) 
		and PermitStatusId = @PermitStatusId

	--select * from @TempTable
	return	@days

END
GO