﻿CREATE FUNCTION [dbo].[CalculateGraduatedProductFee](@productId int, @usage bigint)
returns money
AS
BEGIN

	-- select dbo.CalculateGraduatedProductFee(37, 100000)

	DECLARE @GraduatedProductRule1 money
	DECLARE @GraduatedProductRule2 money
	DECLARE @GraduatedProductRule3 money
	DECLARE @GraduatedProductRule4 money
	DECLARE @GraduatedProductRule5 money
	
	SELECT @GraduatedProductRule1 = sum(Amount)
	FROM	GraduatedProductRule
	WHERE	ProductRuleOperatorId = 1
		and GraduatedProductId = @productId
		and	@usage > ProductRuleOperand

	SELECT @GraduatedProductRule2 = sum(Amount)
	FROM	GraduatedProductRule
	WHERE	ProductRuleOperatorId = 2
		and GraduatedProductId = @productId
		and	@usage < ProductRuleOperand

	SELECT @GraduatedProductRule3 = sum(Amount)
	FROM	GraduatedProductRule
	WHERE	ProductRuleOperatorId = 3
		and GraduatedProductId = @productId
		and	@usage >= ProductRuleOperand

	SELECT @GraduatedProductRule4 = sum(Amount)
	FROM	GraduatedProductRule
	WHERE	ProductRuleOperatorId = 4
		and GraduatedProductId = @productId
		and	@usage <= ProductRuleOperand

	SELECT @GraduatedProductRule5 = sum(Amount)
	FROM	GraduatedProductRule
	WHERE	ProductRuleOperatorId = 5
		and GraduatedProductId = @productId
		and	@usage = ProductRuleOperand

	return	isnull(@GraduatedProductRule1,0) + isnull(@GraduatedProductRule2,0) + isnull(@GraduatedProductRule3,0) +
			isnull(@GraduatedProductRule4,0) + isnull(@GraduatedProductRule5,0)

END	
GO
