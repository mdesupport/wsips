﻿CREATE FUNCTION [dbo].[CalculateProductFee](@ProductTypeId int, @PermitTypeId int, @ApplicationTypeId int, @usage int)
returns money
AS
BEGIN

	-- select dbo.CalculateGraduatedProductFee(28, 10000)

	DECLARE @sum money
	SET @sum = 0

	SELECT @sum = sum(amount)
	FROM	Product
	WHERE	ProductTypeId = @ProductTypeId
		and isnull(PermitTypeId, 0) = isnull(@PermitTypeId,0)
		and isnull(ApplicationTypeId, 0) = isnull(@ApplicationTypeId,0)
		and Active = 1
		and EffectiveDate <= getdate()
		and isGraduated = 0

	SELECT @sum = @sum + sum(dbo.CalculateGraduatedProductFee(Product.Id, @usage))
	FROM	Product
	WHERE	ProductTypeId = @ProductTypeId
		and isnull(PermitTypeId, 0) = isnull(@PermitTypeId,0)
		and isnull(ApplicationTypeId, 0) = isnull(@ApplicationTypeId,0)
		and Active = 1
		and EffectiveDate <= getdate()
		and isGraduated = 1

	return @sum

END	
GO