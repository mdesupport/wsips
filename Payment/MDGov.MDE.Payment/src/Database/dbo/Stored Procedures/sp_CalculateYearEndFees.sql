﻿CREATE PROCEDURE [dbo].[sp_CalculateYearEndFees]
(@Year int)
AS
BEGIN

	-- exec sp_CalculateYearEndFees 2013

	-- DECLARE @Year int
	DECLARE @StartDate datetime
	DECLARE @EndDate datetime

	-- SET @Year = 2013
	SET @StartDate = '1/1/' + cast(@Year as char(4))
	SET @EndDate = '12/31/' + cast(@Year as char(4))

	-- drop table #permits
		
	SELECT	MdeReports.dbo.GetPermitteeContactId(p.Id) ContactId, p.Id PermitId, p.PermitName, p.PermitTypeId,
		round((cast(NumberOfDaysActive as money) / cast(365 as money)), 2,0) Prorate , 
		isnull(pwg.AvgGalPerDay , 0) + isnull(pws.AvgGalPerDay , 0) AvgGalPerDay
		--,isnull(pwg.AvgGalPerDay , 0) + isnull(pws.AvgGalPerDay , 0) AvgGalPerDay
		--, NumberOfDaysActive,  p.PermitTypeId, p.EffectiveDate, p.ExpirationDate, 
	INTO #permits
	FROM	MdeWaterPermitting..Permit p
	INNER JOIN (SELECT	p.Id PermitId, -- p.EffectiveDate, p.ExpirationDate,
						case 
							when year(p.EffectiveDate) = @Year 
								then dbo.CalculateDaysInStatus(p.EffectiveDate, @EndDate, p.Id, 46)
							else
								dbo.CalculateDaysInStatus(@StartDate, @EndDate, p.Id, 46)
						end NumberOfDaysActive
				FROM	MdeWaterPermitting..Permit p
				WHERE	-- the permit has been effective prior to the end of the year
						p.EffectiveDate <= @EndDate
						-- permit cannot be exempt
					and p.IsExemptFromFees = 0
						-- don't include exemptions
					and isnull(p.ApplicationTypeId,0) in (0,1,2,3)) pTemp on p.Id = pTemp.PermitId and pTemp.NumberOfDaysActive > 0
	LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id 
	LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater pws on pws.PermitId = p.Id 

	-- calculate non-graduated fees
	INSERT INTO ContactSale
	SELECT	t.ContactId, t.PermitId, t.PermitName, t.ProductId, 1 ContactSaleStatusId, null OverriddenBy, null [OverriddenDate],
		t.Amount, null [IsOnlinePayment], null [CheckNumber], null [DepositDate], null [PaidDate], null [PaymentFailCode],
		'Fee Calculation Service' [CreatedBy], getdate() [CreatedDate],
		'Fee Calculation Service' [LastModifiedBy], getdate() [LastModifiedDate], @Year UsageFeeYear
	FROM	(
		SELECT	p.ContactId, p.PermitId, p.PermitName, prod.Id ProductId, prod.Amount * p.Prorate Amount
		FROM	#permits p
			inner join	Product prod 
				on p.PermitTypeId = isnull(prod.PermitTypeId,0)
					and prod.Active = 1
					and prod.EffectiveDate <= @EndDate
					and prod.IsGraduated = 0 ) t
	LEFT OUTER JOIN ContactSale cs on cs.PermitId = t.PermitId 
		and cs.ProductId = t.ProductId
		and cs.UsageFeeYear = @Year
	WHERE	t.Amount > 0
	and cs.Id is null

	INSERT INTO ContactSale
	SELECT	t.ContactId, t.PermitId, t.PermitName, t.ProductId, 1 ContactSaleStatusId, null OverriddenBy, null [OverriddenDate],
		t.Amount, null [IsOnlinePayment], null [CheckNumber], null [DepositDate], null [PaidDate], null [PaymentFailCode],
		'Fee Calculation Service' [CreatedBy], getdate() [CreatedDate],
		'Fee Calculation Service' [LastModifiedBy], getdate() [LastModifiedDate], @Year UsageFeeYear
	FROM	(
		SELECT	p.ContactId, p.PermitId, p.PermitName, prod.Id ProductId, dbo.CalculateGraduatedProductFee(prod.Id, p.AvgGalPerDay) * p.Prorate Amount
		FROM	#permits p
			inner join	Product prod 
				on p.PermitTypeId = isnull(prod.PermitTypeId,0)
					and prod.Active = 1
					and prod.EffectiveDate <= @EndDate
					and prod.IsGraduated = 1 ) t
	LEFT OUTER JOIN ContactSale cs on cs.PermitId = t.PermitId 
		and cs.ProductId = t.ProductId
		and cs.UsageFeeYear = @Year
	WHERE	t.Amount > 0
	and cs.Id is null

END
GO