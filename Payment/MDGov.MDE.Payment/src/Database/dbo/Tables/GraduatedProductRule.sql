﻿CREATE TABLE [dbo].[GraduatedProductRule] (
    [Id]                    INT             IDENTITY (1, 1) NOT NULL,
	[GraduatedProductId]    INT             NOT NULL,
	[ProductRuleOperatorId] INT             NOT NULL,
	[ProductRuleOperand]    BIGINT             NOT NULL,
    [Amount]                SMALLMONEY      NOT NULL,
    [CreatedBy]             VARCHAR (100)   NOT NULL,
    [CreatedDate]           DATETIME2       NOT NULL,
    [LastModifiedBy]        VARCHAR (100)   NOT NULL,
    [LastModifiedDate]      DATETIME2       NOT NULL,
    CONSTRAINT [XPKGraduatedProductRule] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_GraduatedProductRule_Product] FOREIGN KEY ([GraduatedProductId]) REFERENCES [dbo].[Product]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_GraduatedProductRule_LU_ProductRuleOperator] FOREIGN KEY ([ProductRuleOperatorId]) REFERENCES [dbo].[LU_ProductRuleOperator]([Id])
);

