﻿CREATE TABLE [dbo].[LU_PaymentType] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Key]              VARCHAR (10)  NULL,
    [Description]      VARCHAR (50)  NOT NULL,
    [Sequence]         INT           NULL,
    [Active]           BIT           NOT NULL,
	[CreatedBy]        VARCHAR (100) NOT NULL,
	[CreatedDate]      DATETIME2     NOT NULL,
	[LastModifiedBy]   VARCHAR (100) NOT NULL,
	[LastModifiedDate] DATETIME2     NOT NULL
    CONSTRAINT [XPKLU_PaymentType] PRIMARY KEY CLUSTERED ([Id] ASC)
);