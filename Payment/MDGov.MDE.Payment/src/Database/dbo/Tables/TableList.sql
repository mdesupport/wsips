﻿CREATE TABLE [dbo].[TableList] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [TableName]        VARCHAR (50)  NULL,
    [IsLookUp]         BIT           NULL,
    [IsEditableLookup] BIT           NULL,
	[CreatedBy]        VARCHAR (100) NOT NULL,
	[CreatedDate]      DATETIME2     NOT NULL,
	[LastModifiedBy]   VARCHAR (100) NOT NULL,
	[LastModifiedDate] DATETIME2     NOT NULL
    CONSTRAINT [XPKTableList] PRIMARY KEY CLUSTERED ([Id] ASC)
);

