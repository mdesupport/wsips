RELEASE NOTES

MDE WSIPS v3.8.0.0
UAT Release Date : 12/17/2017

Issue 105 - General Permit Screen Spacing
    Fixed:
	- Moved labels and form components

Issue 134 - Add Freshwater/Saltwater Drop Down and Tidal Check Box Fields
    Fixed:
	- Created LU_TidalType table and added records
	- Added TidalTypeId and foreign key constraint to Permit table
	- Added drop-down fields and set default values

Issue 135 - Export Tidal and Freshwater/Saltwater Fields
    Fixed:
	- Added Tidal field to PermitSearchResultReport stored procedure (Freshwater/Saltwater was already included)
	- Added Tidal field to PermitSearchResultReport SQL Server report (Freshwater/Saltwater was already included)

Issue 168 - Pumpage Report Summary By Permit does not calculate the annual average correctly when the version number is removed
    Fixed:
	- PumpageReportSummaryByPermit stored procedure to use left join instead of inner join on PumpageReport table and 
	  to ignore pending reports

Issue 169 - Acre inches is being calculated wrong in 2014
    Fixed:
	- Updated to most recent version of jquery Mask Money (older version had a bug when displaying anything other 
	  than 2 decimal places)
	- Made calculated fields on report screen read-only
	- Updated incorrect report values and recalculated affected report totals and percentages


