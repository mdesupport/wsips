﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.Configuration.Config
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using System.Configuration;

namespace MDGov.MDE.Common.Helpers.Configuration
{
  public static class Config
  {
    public static string LogsPath
    {
      get
      {
        return Config.GetAppSetting("LogsPath");
      }
    }

    public static string AppName
    {
      get
      {
        return Config.GetAppSetting("AppName");
      }
    }

    public static int LogMessageLevel
    {
      get
      {
        return Config.GetAppSetting("LogMessageLevel") != null ? int.Parse(Config.GetAppSetting("LogMessageLevel")) : 0;
      }
    }

    private static string GetAppSetting(string key)
    {
      return ConfigurationManager.AppSettings[key];
    }
  }
}
