﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.Formatter
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace MDGov.MDE.Common.Helpers
{
  public static class Formatter
  {
    public static IEnumerable<CustomCondition> ReplaceVariables(IEnumerable<CustomCondition> conditions, Permit permit, PermitWithdrawalSurfacewater surface, PermitWithdrawalGroundwater ground)
    {
      conditions = (IEnumerable<CustomCondition>) conditions.ToList<CustomCondition>();
      foreach (CustomCondition condition in conditions)
      {
        foreach (string str1 in (IEnumerable<string>) Regex.Matches(condition.ConditionText.Replace(Environment.NewLine, ""), "\\[([^]]*)\\]").Cast<Match>().Select<Match, string>((Func<Match, string>) (x => x.Groups[1].Value)).ToList<string>())
        {
          switch (str1)
          {
            case "Groundwater.AvgGalPerDay":
              condition.ConditionText = ground != null ? condition.ConditionText.Replace("[" + str1 + "]", string.Format("{0:n0}", (object) ground.AvgGalPerDay)) : "Not Specified".ToUpper();
              break;
            case "Groundwater.MaxGalPerDay":
              condition.ConditionText = ground != null ? condition.ConditionText.Replace("[" + str1 + "]", string.Format("{0:n0}", (object) ground.MaxGalPerDay)) : "Not Specified".ToUpper();
              break;
            case "Surfacewater.AvgGalPerDay":
              condition.ConditionText = surface != null ? condition.ConditionText.Replace("[" + str1 + "]", string.Format("{0:n0}", (object) surface.AvgGalPerDay)) : "Not Specified".ToUpper();
              break;
            case "Surfacewater.MaxGalPerDay":
              condition.ConditionText = surface != null ? condition.ConditionText.Replace("[" + str1 + "]", string.Format("{0:n0}", (object) surface.MaxGalPerDay)) : "Not Specified".ToUpper();
              break;
            case "SourceDescription":
              condition.ConditionText = condition.ConditionText.Replace("[" + str1 + "]", permit.SourceDescription == null ? "Unknown".ToUpper() : permit.SourceDescription.ToString((IFormatProvider) CultureInfo.InvariantCulture));
              break;
            case "UseDescription":
              condition.ConditionText = condition.ConditionText.Replace("[" + str1 + "]", permit.UseDescription == null ? "Unknown".ToUpper() : permit.UseDescription.ToString((IFormatProvider) CultureInfo.InvariantCulture));
              break;
            case "Location":
              condition.ConditionText = condition.ConditionText.Replace("[" + str1 + "]", permit.Location == null ? "Unknown".ToUpper() : permit.Location.ToString((IFormatProvider) CultureInfo.InvariantCulture));
              break;
            case "OneTimeReporting.DueDate":
              DateTime? dueDate = condition.DueDate;
              if (dueDate.HasValue)
              {
                CustomCondition customCondition = condition;
                string conditionText = condition.ConditionText;
                string oldValue = "[" + str1 + "]";
                dueDate = condition.DueDate;
                string newValue = dueDate.Value.ToString("MM/dd/yyyy");
                string str2 = conditionText.Replace(oldValue, newValue);
                customCondition.ConditionText = str2;
                break;
              }
              break;
          }
        }
      }
      return conditions;
    }

    public static DateTime AddBusinessDays(DateTime current, int days)
    {
      int num1 = Math.Sign(days);
      int num2 = Math.Abs(days);
      for (int index = 0; index < num2; ++index)
      {
        do
        {
          current = current.AddDays((double) num1);
        }
        while (current.DayOfWeek == DayOfWeek.Saturday || current.DayOfWeek == DayOfWeek.Sunday);
      }
      return current;
    }

    public static string FormatDate(DateTime date)
    {
        return date.ToString("MMMM d, yyyy");
    }
  }
}
