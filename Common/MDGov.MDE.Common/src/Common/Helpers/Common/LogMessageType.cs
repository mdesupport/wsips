﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.Common.LogMessageType
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

namespace MDGov.MDE.Common.Helpers.Common
{
  public enum LogMessageType
  {
    Information = 1,
    Warning = 2,
    Error = 3,
  }
}
