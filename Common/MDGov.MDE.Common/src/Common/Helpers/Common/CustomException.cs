﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.Common.CustomException
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using System;

namespace MDGov.MDE.Common.Helpers.Common
{
  public class CustomException : Exception
  {
    private string _stackTrace;

    public override string StackTrace
    {
      get
      {
        return this._stackTrace + base.StackTrace;
      }
    }

    public CustomException(ApiException ex)
      : base(ex.ExceptionMessage)
    {
      this._stackTrace = ex.StackTrace;
    }
  }
}
