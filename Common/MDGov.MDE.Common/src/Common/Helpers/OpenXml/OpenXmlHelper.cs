﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.OpenXml.OpenXmlHelper
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using MDGov.MDE.Common.Helpers.OpenXml.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace MDGov.MDE.Common.Helpers.OpenXml
{
  public class OpenXmlHelper
  {
    private static bool _imageAdded;

    public class WordHelper : IOpenXmlWordHelper
    {
      private int AddListPart(WordprocessingDocument document)
      {
        MainDocumentPart mainDocumentPart = document.MainDocumentPart;
        NumberingDefinitionsPart openXmlPart = mainDocumentPart.NumberingDefinitionsPart;
        int num = 0;
        if (openXmlPart == null)
          openXmlPart = mainDocumentPart.AddNewPart<NumberingDefinitionsPart>();
        if (mainDocumentPart.NumberingDefinitionsPart.Numbering == null)
        {
          new Numbering().Save(openXmlPart);
        }
        else
        {
          foreach (AbstractNum element in openXmlPart.Numbering.Elements<AbstractNum>())
          {
            if (element.AbstractNumberId.HasValue && (int) element.AbstractNumberId > num)
              num = (int) element.AbstractNumberId;
          }
          ++num;
        }
        return num;
      }

      private static void AddImagePart(WordprocessingDocument document, string imageName, byte[] imageData)
      {
        using (BinaryWriter binaryWriter = new BinaryWriter(document.MainDocumentPart.AddNewPart<ImagePart>("image/jpeg", imageName).GetStream()))
        {
          binaryWriter.Write(imageData);
          binaryWriter.Flush();
        }
        OpenXmlHelper._imageAdded = true;
      }

      private static Bitmap CreateBitamp(byte[] imageData)
      {
        Bitmap bitmap;
        using (MemoryStream memoryStream = new MemoryStream())
        {
          memoryStream.Write(imageData, 0, imageData.Length);
          bitmap = new Bitmap((Stream) memoryStream);
        }
        return bitmap;
      }

      private static DocumentFormat.OpenXml.Wordprocessing.Drawing CreateDrawing(string imageName, Bitmap image)
      {
        long num1 = (long) ((double) image.Width / (double) image.HorizontalResolution * 914400.0);
        long num2 = (long) ((double) image.Height / (double) image.VerticalResolution * 914400.0);
        OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[1];
        OpenXmlElement[] openXmlElementArray2 = openXmlElementArray1;
        int index1 = 0;
        OpenXmlElement[] openXmlElementArray3 = new OpenXmlElement[5]
        {
          (OpenXmlElement) new Extent()
          {
            Cx = (Int64Value) num1,
            Cy = (Int64Value) num2
          },
          (OpenXmlElement) new EffectExtent()
          {
            LeftEdge = (Int64Value) 19050L,
            TopEdge = (Int64Value) 0L,
            RightEdge = (Int64Value) 9525L,
            BottomEdge = (Int64Value) 0L
          },
          (OpenXmlElement) new DocProperties()
          {
            Id = (UInt32Value) 1U,
            Name = (StringValue) "Inline Text Wrapping Picture",
            Description = (StringValue) "http://schemas.openxmlformats.org/drawingml/2006/picture"
          },
          (OpenXmlElement) new DocumentFormat.OpenXml.Drawing.Wordprocessing.NonVisualGraphicFrameDrawingProperties(new OpenXmlElement[1]
          {
            (OpenXmlElement) new GraphicFrameLocks()
            {
              NoChangeAspect = (BooleanValue) true
            }
          }),
          null
        };
        OpenXmlElement[] openXmlElementArray4 = openXmlElementArray3;
        int index2 = 4;
        OpenXmlElement[] openXmlElementArray5 = new OpenXmlElement[1];
        OpenXmlElement[] openXmlElementArray6 = openXmlElementArray5;
        int index3 = 0;
        OpenXmlElement[] openXmlElementArray7 = new OpenXmlElement[1];
        OpenXmlElement[] openXmlElementArray8 = openXmlElementArray7;
        int index4 = 0;
        OpenXmlElement[] openXmlElementArray9 = new OpenXmlElement[3]
        {
          (OpenXmlElement) new DocumentFormat.OpenXml.Drawing.Pictures.NonVisualPictureProperties(new OpenXmlElement[2]
          {
            (OpenXmlElement) new DocumentFormat.OpenXml.Drawing.Pictures.NonVisualDrawingProperties()
            {
              Id = (UInt32Value) 0U,
              Name = (StringValue) imageName
            },
            (OpenXmlElement) new DocumentFormat.OpenXml.Drawing.Pictures.NonVisualPictureDrawingProperties()
          }),
          (OpenXmlElement) new DocumentFormat.OpenXml.Drawing.Pictures.BlipFill(new OpenXmlElement[2]
          {
            (OpenXmlElement) new Blip()
            {
              Embed = (StringValue) imageName
            },
            (OpenXmlElement) new Stretch(new OpenXmlElement[1]
            {
              (OpenXmlElement) new FillRectangle()
            })
          }),
          null
        };
        OpenXmlElement[] openXmlElementArray10 = openXmlElementArray9;
        int index5 = 2;
        OpenXmlElement[] openXmlElementArray11 = new OpenXmlElement[2];
        OpenXmlElement[] openXmlElementArray12 = openXmlElementArray11;
        int index6 = 0;
        OpenXmlElement[] openXmlElementArray13 = new OpenXmlElement[2];
        OpenXmlElement[] openXmlElementArray14 = openXmlElementArray13;
        int index7 = 0;
        Offset offset1 = new Offset();
        offset1.X = (Int64Value) 0L;
        offset1.Y = (Int64Value) 0L;
        Offset offset2 = offset1;
        openXmlElementArray14[index7] = (OpenXmlElement) offset2;
        OpenXmlElement[] openXmlElementArray15 = openXmlElementArray13;
        int index8 = 1;
        Extents extents1 = new Extents();
        extents1.Cx = (Int64Value) num1;
        extents1.Cy = (Int64Value) num2;
        Extents extents2 = extents1;
        openXmlElementArray15[index8] = (OpenXmlElement) extents2;
        Transform2D transform2D = new Transform2D(openXmlElementArray13);
        openXmlElementArray12[index6] = (OpenXmlElement) transform2D;
        openXmlElementArray11[1] = (OpenXmlElement) new PresetGeometry(new OpenXmlElement[1]
        {
          (OpenXmlElement) new AdjustValueList()
        })
        {
          Preset = (EnumValue<ShapeTypeValues>) ShapeTypeValues.Rectangle
        };
        DocumentFormat.OpenXml.Drawing.Pictures.ShapeProperties shapeProperties = new DocumentFormat.OpenXml.Drawing.Pictures.ShapeProperties(openXmlElementArray11);
        openXmlElementArray10[index5] = (OpenXmlElement) shapeProperties;
        DocumentFormat.OpenXml.Drawing.Pictures.Picture picture = new DocumentFormat.OpenXml.Drawing.Pictures.Picture(openXmlElementArray9);
        openXmlElementArray8[index4] = (OpenXmlElement) picture;
        GraphicData graphicData = new GraphicData(openXmlElementArray7)
        {
          Uri = (StringValue) "http://schemas.openxmlformats.org/drawingml/2006/picture"
        };
        openXmlElementArray6[index3] = (OpenXmlElement) graphicData;
        Graphic graphic = new Graphic(openXmlElementArray5);
        openXmlElementArray4[index2] = (OpenXmlElement) graphic;
        Inline inline = new Inline(openXmlElementArray3)
        {
          DistanceFromTop = (UInt32Value) 0U,
          DistanceFromBottom = (UInt32Value) 0U,
          DistanceFromLeft = (UInt32Value) 0U,
          DistanceFromRight = (UInt32Value) 0U
        };
        openXmlElementArray2[index1] = (OpenXmlElement) inline;
        return new DocumentFormat.OpenXml.Wordprocessing.Drawing(openXmlElementArray1);
      }

      private void FillProperties(SdtElement contentControl, DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties)
      {
        IEnumerable<SdtContentBlock> list = (IEnumerable<SdtContentBlock>) contentControl.Descendants<SdtContentBlock>().ToList<SdtContentBlock>();
        if (list.Any<SdtContentBlock>() && list.First<SdtContentBlock>().Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().Any<DocumentFormat.OpenXml.Wordprocessing.RunProperties>())
          runProperties.Append(new OpenXmlElement[1]
          {
            list.First<SdtContentBlock>().Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().First<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().CloneNode(true)
          });
        if (!contentControl.SdtProperties.Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().Any<DocumentFormat.OpenXml.Wordprocessing.RunProperties>())
          return;
        runProperties.Append(new OpenXmlElement[1]
        {
          contentControl.SdtProperties.Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().First<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().CloneNode(true)
        });
      }

      private void FillProperties(SdtElement contentControl, DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties, DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties paragraphProperties)
      {
        IEnumerable<SdtContentBlock> list = (IEnumerable<SdtContentBlock>) contentControl.Descendants<SdtContentBlock>().ToList<SdtContentBlock>();
        if (list.Any<SdtContentBlock>())
        {
          if (list.First<SdtContentBlock>().Descendants<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>().Any<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>())
            paragraphProperties.Append(new OpenXmlElement[1]
            {
              list.First<SdtContentBlock>().Descendants<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>().First<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>().CloneNode(true)
            });
          if (list.First<SdtContentBlock>().Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().Any<DocumentFormat.OpenXml.Wordprocessing.RunProperties>())
            runProperties.Append(new OpenXmlElement[1]
            {
              list.First<SdtContentBlock>().Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().First<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().CloneNode(true)
            });
        }
        if (contentControl.SdtProperties.Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().Any<DocumentFormat.OpenXml.Wordprocessing.RunProperties>())
          runProperties.Append(new OpenXmlElement[1]
          {
            contentControl.SdtProperties.Descendants<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().First<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().CloneNode(true)
          });
        if (!contentControl.SdtProperties.Descendants<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>().Any<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>())
          return;
        paragraphProperties.Append(new OpenXmlElement[1]
        {
          contentControl.SdtProperties.Descendants<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>().First<DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties>().CloneNode(true)
        });
      }

      private static IEnumerable<SdtElement> GetAllContentControls(WordprocessingDocument document)
      {
        List<SdtElement> sdtElementList = new List<SdtElement>();
        sdtElementList.AddRange((IEnumerable<SdtElement>) document.MainDocumentPart.Document.Descendants<SdtElement>().ToList<SdtElement>());
        foreach (AlternativeFormatImportPart formatImportPart in document.MainDocumentPart.AlternativeFormatImportParts.ToList<AlternativeFormatImportPart>())
        {
          if (formatImportPart.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml")
          {
            using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(document.Package.GetPart(formatImportPart.Uri).GetStream(FileMode.Open, FileAccess.ReadWrite), true))
              sdtElementList.AddRange((IEnumerable<SdtElement>) wordprocessingDocument.MainDocumentPart.Document.Descendants<SdtElement>().ToList<SdtElement>());
          }
        }
        foreach (HeaderPart headerPart in document.MainDocumentPart.HeaderParts)
        {
          if (headerPart.Header != null)
            sdtElementList.AddRange((IEnumerable<SdtElement>) headerPart.Header.Descendants<SdtElement>().ToList<SdtElement>());
        }
        foreach (FooterPart footerPart in document.MainDocumentPart.FooterParts)
        {
          if (footerPart.Footer != null)
            sdtElementList.AddRange((IEnumerable<SdtElement>) footerPart.Footer.Descendants<SdtElement>().ToList<SdtElement>());
        }
        return (IEnumerable<SdtElement>) sdtElementList;
      }

      private string GetTagValue(SdtElement contentControl)
      {
        return contentControl.SdtProperties.GetFirstChild<Tag>() == null ? string.Empty : contentControl.SdtProperties.GetFirstChild<Tag>().Val.Value;
      }

      public void ReplaceContentControl(SdtBlock sdtBlock, DocumentFormat.OpenXml.Wordprocessing.Drawing drawing)
      {
        DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph = new DocumentFormat.OpenXml.Wordprocessing.Paragraph();
        DocumentFormat.OpenXml.Wordprocessing.Run run = new DocumentFormat.OpenXml.Wordprocessing.Run();
        this.SetProperties(paragraph, run, (SdtElement) sdtBlock);
        run.AppendChild<DocumentFormat.OpenXml.Wordprocessing.Drawing>(drawing);
        paragraph.AppendChild<DocumentFormat.OpenXml.Wordprocessing.Run>(run);
        sdtBlock.Parent.ReplaceChild<SdtElement>((OpenXmlElement) paragraph, (SdtElement) sdtBlock);
      }

      private void ReplaceContentControl(SdtBlock sdtBlock, string text)
      {
        DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph = new DocumentFormat.OpenXml.Wordprocessing.Paragraph();
        DocumentFormat.OpenXml.Wordprocessing.Run run = new DocumentFormat.OpenXml.Wordprocessing.Run();
        this.SetProperties(paragraph, run, (SdtElement) sdtBlock);
        string[] strArray = Regex.Split(text, "\\r\\n");
        for (int index = 0; index < strArray.Length; ++index)
        {
          run.AppendChild<DocumentFormat.OpenXml.Wordprocessing.Text>(new DocumentFormat.OpenXml.Wordprocessing.Text(strArray[index]));
          if (strArray.Length > 1 && index < strArray.Length - 1)
            run.AppendChild<CarriageReturn>(new CarriageReturn());
        }
        paragraph.AppendChild<DocumentFormat.OpenXml.Wordprocessing.Run>(run);
        sdtBlock.Parent.ReplaceChild<SdtElement>((OpenXmlElement) paragraph, (SdtElement) sdtBlock);
      }

      private void ReplaceContentControl(SdtRun sdtRun, string text)
      {
        DocumentFormat.OpenXml.Wordprocessing.Run run = new DocumentFormat.OpenXml.Wordprocessing.Run();
        this.SetProperties(run, (SdtElement) sdtRun);
        string[] strArray = Regex.Split(text, "\\r\\n");
        for (int index = 0; index < strArray.Length; ++index)
        {
          run.AppendChild<DocumentFormat.OpenXml.Wordprocessing.Text>(new DocumentFormat.OpenXml.Wordprocessing.Text(strArray[index]));
          if (strArray.Length > 1 && index < strArray.Length - 1)
            run.AppendChild<CarriageReturn>(new CarriageReturn());
        }
        sdtRun.Parent.ReplaceChild<SdtElement>((OpenXmlElement) run, (SdtElement) sdtRun);
      }

      private void ReplaceContentControl(SdtCell sdtCell, string text)
      {
        DocumentFormat.OpenXml.Wordprocessing.Run run = new DocumentFormat.OpenXml.Wordprocessing.Run();
        this.SetProperties(run, (SdtElement) sdtCell);
        string[] strArray = Regex.Split(text, "\\r\\n");
        for (int index = 0; index < strArray.Length; ++index)
        {
          run.AppendChild<DocumentFormat.OpenXml.Wordprocessing.Text>(new DocumentFormat.OpenXml.Wordprocessing.Text(strArray[index]));
          if (strArray.Length > 1 && index < strArray.Length - 1)
            run.AppendChild<CarriageReturn>(new CarriageReturn());
        }
        sdtCell.Parent.ReplaceChild<SdtElement>((OpenXmlElement) run, (SdtElement) sdtCell);
      }

      private void ReplaceContentControl(SdtBlock sdtBlock, IList<string> listItems, bool conditionsFormat = false)
      {
        List<OpenXmlElement> openXmlElementList1 = new List<OpenXmlElement>();
        for (int index1 = 0; index1 < listItems.Count; ++index1)
        {
          if (conditionsFormat)
          {
            List<OpenXmlElement> openXmlElementList2 = openXmlElementList1;
            OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[2];
            OpenXmlElement[] openXmlElementArray2 = openXmlElementArray1;
            int index2 = 0;
            OpenXmlElement[] openXmlElementArray3 = new OpenXmlElement[1];
            OpenXmlElement[] openXmlElementArray4 = openXmlElementArray3;
            int index3 = 0;
            OpenXmlElement[] openXmlElementArray5 = new OpenXmlElement[2]
            {
              (OpenXmlElement) new NumberingLevelReference()
              {
                Val = (Int32Value) 0
              },
              null
            };
            OpenXmlElement[] openXmlElementArray6 = openXmlElementArray5;
            int index4 = 1;
            NumberingId numberingId1 = new NumberingId();
            numberingId1.Val = (Int32Value) 1;
            NumberingId numberingId2 = numberingId1;
            openXmlElementArray6[index4] = (OpenXmlElement) numberingId2;
            NumberingProperties numberingProperties = new NumberingProperties(openXmlElementArray5);
            openXmlElementArray4[index3] = (OpenXmlElement) numberingProperties;
            DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties paragraphProperties = new DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties(openXmlElementArray3);
            openXmlElementArray2[index2] = (OpenXmlElement) paragraphProperties;
            OpenXmlElement[] openXmlElementArray7 = openXmlElementArray1;
            int index5 = 1;
            OpenXmlElement[] openXmlElementArray8 = new OpenXmlElement[2]
            {
              (OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.RunProperties(),
              null
            };
            OpenXmlElement[] openXmlElementArray9 = openXmlElementArray8;
            int index6 = 1;
            DocumentFormat.OpenXml.Wordprocessing.Text text1 = new DocumentFormat.OpenXml.Wordprocessing.Text(listItems[index1]);
            text1.Space = (EnumValue<SpaceProcessingModeValues>) SpaceProcessingModeValues.Preserve;
            DocumentFormat.OpenXml.Wordprocessing.Text text2 = text1;
            openXmlElementArray9[index6] = (OpenXmlElement) text2;
            DocumentFormat.OpenXml.Wordprocessing.Run run = new DocumentFormat.OpenXml.Wordprocessing.Run(openXmlElementArray8);
            openXmlElementArray7[index5] = (OpenXmlElement) run;
            DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(openXmlElementArray1);
            openXmlElementList2.Add((OpenXmlElement) paragraph);
          }
          else
          {
            List<OpenXmlElement> openXmlElementList2 = openXmlElementList1;
            OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[1];
            OpenXmlElement[] openXmlElementArray2 = openXmlElementArray1;
            int index2 = 0;
            OpenXmlElement[] openXmlElementArray3 = new OpenXmlElement[2]
            {
              (OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.RunProperties(),
              null
            };
            OpenXmlElement[] openXmlElementArray4 = openXmlElementArray3;
            int index3 = 1;
            DocumentFormat.OpenXml.Wordprocessing.Text text1 = new DocumentFormat.OpenXml.Wordprocessing.Text(listItems[index1]);
            text1.Space = (EnumValue<SpaceProcessingModeValues>) SpaceProcessingModeValues.Preserve;
            DocumentFormat.OpenXml.Wordprocessing.Text text2 = text1;
            openXmlElementArray4[index3] = (OpenXmlElement) text2;
            DocumentFormat.OpenXml.Wordprocessing.Run run = new DocumentFormat.OpenXml.Wordprocessing.Run(openXmlElementArray3);
            openXmlElementArray2[index2] = (OpenXmlElement) run;
            DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(openXmlElementArray1);
            openXmlElementList2.Add((OpenXmlElement) paragraph);
          }
          if (conditionsFormat && index1 == 3)
            openXmlElementList1.Add((OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new OpenXmlElement[1]
            {
              (OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.Run(new OpenXmlElement[1]
              {
                (OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.Break()
                {
                  Type = (EnumValue<BreakValues>) BreakValues.Page
                }
              })
            }));
        }
        SdtContentBlock sdtContentBlock = new SdtContentBlock();
        sdtContentBlock.Append((IEnumerable<OpenXmlElement>) openXmlElementList1);
        sdtBlock.Parent.ReplaceChild<SdtElement>((OpenXmlElement) sdtContentBlock, (SdtElement) sdtBlock);
      }

      private void ReplaceContentControl(SdtRun sdtRun, IList<string> listItems)
      {
        List<OpenXmlElement> list = listItems.Select<string, DocumentFormat.OpenXml.Wordprocessing.Paragraph>((Func<string, DocumentFormat.OpenXml.Wordprocessing.Paragraph>) (t =>
        {
          OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[2]
          {
            (OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties(),
            null
          };
          OpenXmlElement[] openXmlElementArray2 = openXmlElementArray1;
          int index1 = 1;
          OpenXmlElement[] openXmlElementArray3 = new OpenXmlElement[2]
          {
            (OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.RunProperties(),
            null
          };
          OpenXmlElement[] openXmlElementArray4 = openXmlElementArray3;
          int index2 = 1;
          DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text(t)
          {
            Space = (EnumValue<SpaceProcessingModeValues>) SpaceProcessingModeValues.Preserve
          };
          openXmlElementArray4[index2] = (OpenXmlElement) text;
          DocumentFormat.OpenXml.Wordprocessing.Run run = new DocumentFormat.OpenXml.Wordprocessing.Run(openXmlElementArray3);
          openXmlElementArray2[index1] = (OpenXmlElement) run;
          return new DocumentFormat.OpenXml.Wordprocessing.Paragraph(openXmlElementArray1);
        })).Cast<OpenXmlElement>().ToList<OpenXmlElement>();
        SdtContentBlock sdtContentBlock = new SdtContentBlock();
        sdtContentBlock.Append((IEnumerable<OpenXmlElement>) list);
        sdtRun.Parent.ReplaceChild<SdtElement>((OpenXmlElement) sdtContentBlock, (SdtElement) sdtRun);
      }

      private void SetProperties(DocumentFormat.OpenXml.Wordprocessing.Run run, SdtElement contentControl)
      {
        DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties = new DocumentFormat.OpenXml.Wordprocessing.RunProperties();
        this.FillProperties(contentControl, runProperties);
        run.RunProperties = (DocumentFormat.OpenXml.Wordprocessing.RunProperties) runProperties.CloneNode(true);
      }

      private void SetProperties(DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph, DocumentFormat.OpenXml.Wordprocessing.Run run, SdtElement contentControl)
      {
        DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties = new DocumentFormat.OpenXml.Wordprocessing.RunProperties();
        DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties paragraphProperties = new DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties();
        this.FillProperties(contentControl, runProperties, paragraphProperties);
        run.RunProperties = (DocumentFormat.OpenXml.Wordprocessing.RunProperties) runProperties.CloneNode(true);
        paragraph.ParagraphProperties = (DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties) paragraphProperties.CloneNode(true);
      }

      public IList<string> GetAllContentControlTags(WordprocessingDocument document)
      {
        IEnumerable<SdtElement> allContentControls = OpenXmlHelper.WordHelper.GetAllContentControls(document);
        IList<string> source = (IList<string>) new List<string>();
        foreach (SdtElement contentControl in allContentControls)
        {
          if (contentControl == null)
            return source;
          source.Add(this.GetTagValue(contentControl));
        }
        return (IList<string>) source.Distinct<string>().ToList<string>();
      }

      public WordprocessingDocument GetDocumentFromTemplate(MemoryStream memoryStream)
      {
        WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open((Stream) memoryStream, true);
        wordprocessingDocument.ChangeDocumentType(WordprocessingDocumentType.Document);
        return wordprocessingDocument;
      }

      public WordprocessingDocument GetDocument(MemoryStream memoryStream)
      {
        return WordprocessingDocument.Open((Stream) memoryStream, true);
      }

      public void ReplaceBodyPattern(WordprocessingDocument document, string pattern, string replaceWith)
      {
        string end;
        using (StreamReader streamReader = new StreamReader(document.MainDocumentPart.GetStream()))
          end = streamReader.ReadToEnd();
        string str = new Regex(pattern).Replace(end, replaceWith);
        using (StreamWriter streamWriter = new StreamWriter(document.MainDocumentPart.GetStream(FileMode.Create)))
          streamWriter.Write(str);
      }

      public void ReplaceBodyText(WordprocessingDocument document, string replace, string replaceWith)
      {
        string end;
        using (StreamReader streamReader = new StreamReader(document.MainDocumentPart.GetStream()))
          end = streamReader.ReadToEnd();
        string str = end.Replace(replace, replaceWith);
        using (StreamWriter streamWriter = new StreamWriter(document.MainDocumentPart.GetStream(FileMode.Create)))
          streamWriter.Write(str);
      }

      public void ReplaceContentControls(WordprocessingDocument document, IDictionary<string, object> contentControlValues, bool conditionsFormat = false)
      {
        if (contentControlValues == null)
          return;
        foreach (SdtElement allContentControl in OpenXmlHelper.WordHelper.GetAllContentControls(document))
        {
          string tagValue = this.GetTagValue(allContentControl);
          object obj;
          if (!string.IsNullOrWhiteSpace(tagValue) && contentControlValues.TryGetValue(tagValue, out obj) && obj != null)
          {
            if (obj is byte[])
            {
              OpenXmlHelper.WordHelper.AddImagePart(document, tagValue, (byte[]) obj);
              Bitmap bitamp = OpenXmlHelper.WordHelper.CreateBitamp((byte[]) obj);
              DocumentFormat.OpenXml.Wordprocessing.Drawing drawing = OpenXmlHelper.WordHelper.CreateDrawing(tagValue, bitamp);
              if (allContentControl is SdtBlock)
                this.ReplaceContentControl((SdtBlock) allContentControl, drawing);
            }
            else if (obj is string)
            {
              if (allContentControl is SdtBlock)
                this.ReplaceContentControl((SdtBlock) allContentControl, (string) obj);
              else if (allContentControl is SdtRun)
                this.ReplaceContentControl((SdtRun) allContentControl, (string) obj);
              else if (allContentControl is SdtCell)
                this.ReplaceContentControl((SdtCell) allContentControl, (string) obj);
            }
            else if (obj.GetType().GetGenericTypeDefinition() == typeof (List<>))
            {
              bool conditionsFormat1 = tagValue == "Conditions";
              if (allContentControl is SdtBlock)
                this.ReplaceContentControl((SdtBlock) allContentControl, (IList<string>) obj, conditionsFormat1);
              else if (allContentControl is SdtRun)
                this.ReplaceContentControl((SdtRun) allContentControl, (IList<string>) obj);
            }
            else
              break;
          }
        }
      }

      public void SaveAndCloseDocument(WordprocessingDocument document)
      {
        document.MainDocumentPart.Document.Save();
        document.Close();
      }

      public byte[] OpenAndCombine(IList<byte[]> documents)
      {
        MemoryStream memoryStream = new MemoryStream();
        memoryStream.Write(documents[0], 0, documents[0].Length);
        memoryStream.Position = 0L;
        using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open((Stream) memoryStream, true))
        {
          XElement xelement1 = XElement.Parse(wordprocessingDocument.MainDocumentPart.Document.Body.OuterXml);
          for (int index = 1; index < documents.Count; ++index)
          {
            XElement xelement2 = XElement.Parse(WordprocessingDocument.Open((Stream) new MemoryStream(documents[index]), true).MainDocumentPart.Document.Body.OuterXml);
            xelement1.Add((object) xelement2);
            wordprocessingDocument.MainDocumentPart.Document.Body = new Body(xelement1.ToString());
            wordprocessingDocument.MainDocumentPart.Document.Save();
            wordprocessingDocument.Package.Flush();
          }
        }
        byte[] array = memoryStream.ToArray();
        memoryStream.Close();
        memoryStream.Dispose();
        return array;
      }
    }
  }
}
