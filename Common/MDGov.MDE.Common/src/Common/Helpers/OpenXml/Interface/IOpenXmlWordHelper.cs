﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.OpenXml.Interface.IOpenXmlWordHelper
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using DocumentFormat.OpenXml.Packaging;
using System.Collections.Generic;
using System.IO;

namespace MDGov.MDE.Common.Helpers.OpenXml.Interface
{
  public interface IOpenXmlWordHelper
  {
    IList<string> GetAllContentControlTags(WordprocessingDocument document);

    WordprocessingDocument GetDocumentFromTemplate(MemoryStream memoryStream);

    WordprocessingDocument GetDocument(MemoryStream memoryStream);

    void ReplaceContentControls(WordprocessingDocument document, IDictionary<string, object> contentControlValues, bool conditionsFormat = false);

    void ReplaceBodyPattern(WordprocessingDocument document, string pattern, string replaceWith);

    void ReplaceBodyText(WordprocessingDocument document, string replace, string replaceWith);

    void SaveAndCloseDocument(WordprocessingDocument document);

    byte[] OpenAndCombine(IList<byte[]> documents);
  }
}
