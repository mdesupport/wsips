﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.DocumentUtility.DocumentUtility
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Helpers.DocumentUtility.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace MDGov.MDE.Common.Helpers.DocumentUtility
{
  public class DocumentUtility : IDocumentUtility
  {
    private readonly IService<PermitCondition> _permitConditionService;
    private readonly IService<LU_Aquifer> _aquiferService;
    private readonly IService<LU_State> _stateService;
    private readonly IService<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterService;
    private readonly IService<PermitWithdrawalGroundwater> _permitWithdrawalGroundwaterService;
    private readonly IService<PermitWithdrawalSurfacewaterDetail> _permitWithdrawalSurfacewaterDetailService;
    private readonly IService<PermitWithdrawalGroundwaterDetail> _permitWithdrawalGroundwaterDetailService;
    private readonly IService<LU_Watershed> _watershedService;
    private readonly IService<ContactCommunicationMethod> _communicationMethodService;
    private readonly IService<LU_Title> _titleService;
    private readonly IService<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeService;
    private readonly IService<PermitParcel> _permitParcelService;
    private readonly IService<LU_County> _countyService;
    private readonly IService<Contact> _contactService;
    private readonly IService<PermitContactInformation> _permitContactService;
    private readonly IService<Document> _documentService;

    public DocumentUtility(IService<PermitCondition> permitConditionService, IService<LU_Aquifer> aquiferService, IService<LU_State> stateService, IService<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterService, IService<PermitWithdrawalGroundwater> permitWithdrawalGroundwaterService, IService<PermitWithdrawalSurfacewaterDetail> permitWithdrawalSurfacewaterDetailService, IService<PermitWithdrawalGroundwaterDetail> permitWithdrawalGroundwaterDetailService, IService<LU_Watershed> watershedService, IService<ContactCommunicationMethod> communicationMethodService, IService<LU_Title> titleService, IService<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeService, IService<PermitParcel> permitParcelService, IService<LU_County> countyService, IService<Contact> contactService, IService<PermitContactInformation> permitContactService, IService<Document> documentService)
    {
      this._permitConditionService = permitConditionService;
      this._aquiferService = aquiferService;
      this._stateService = stateService;
      this._permitWithdrawalSurfacewaterService = permitWithdrawalSurfacewaterService;
      this._permitWithdrawalGroundwaterService = permitWithdrawalGroundwaterService;
      this._permitWithdrawalSurfacewaterDetailService = permitWithdrawalSurfacewaterDetailService;
      this._permitWithdrawalGroundwaterDetailService = permitWithdrawalGroundwaterDetailService;
      this._watershedService = watershedService;
      this._communicationMethodService = communicationMethodService;
      this._titleService = titleService;
      this._permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
      this._permitParcelService = permitParcelService;
      this._countyService = countyService;
      this._contactService = contactService;
      this._permitContactService = permitContactService;
      this._documentService = documentService;
    }

    public IDictionary<string, object> GetPermitTemplateVariables(IEnumerable<string> keys, Permit permit, List<string> docs, string digitalSignatureFolder, int contactId, bool finalPermit = false)
    {
      string empty = string.Empty;
      string upper = "Not Specified".ToUpper();
      TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
      List<string> source1 = new List<string>();
      PermitWithdrawalSurfacewater surface = this._permitWithdrawalSurfacewaterService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
      }, (string) null).FirstOrDefault<PermitWithdrawalSurfacewater>();
      PermitWithdrawalGroundwater ground = this._permitWithdrawalGroundwaterService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
      }, (string) null).FirstOrDefault<PermitWithdrawalGroundwater>();
      Dictionary<string, object> dictionary1 = new Dictionary<string, object>();
      try
      {
        int num1;
        int? nullable;
        DateTime dateTime1;
        byte[] array;
        foreach (string key1 in keys)
        {
          switch (key1)
          {
            case "DraftPermitNumber":
              dictionary1.Add(key1, (object) string.Format("{0}({1})", (object) permit.PermitNumber.ToUpper(), (object) permit.RevisionNumber));
              break;
            case "PermitNumber":
            case "PagePermitNumber":
              string str1 = upper;
              if (!string.IsNullOrEmpty(permit.PermitName))
                str1 = permit.PermitName;
              dictionary1.Add(key1, (object) str1);
              break;
            case "SubscriptionSummary":
              string subSummary = upper;
              if (!string.IsNullOrEmpty(permit.Description))
                  subSummary = permit.Description;
              dictionary1.Add(key1, (object)subSummary);
              break;
            case "ProjectName":
              string projName = upper;
              if (!string.IsNullOrEmpty(permit.ProjectName))
                  projName = permit.ProjectName;
              dictionary1.Add(key1, (object)projName);
              break;
            case "PWSID":
              object obj = (object) (permit.PWSID ?? empty);
              dictionary1.Add(key1, obj);
              break;
            case "RegistrationCode":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              PermitContact permitContact1 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c => c.IsPermittee));
              if (permitContact1 != null)
              {
                Dictionary<string, object> dictionary2 = dictionary1;
                string key2 = key1;
                num1 = permitContact1.ContactId;
                string str2 = num1.ToString((IFormatProvider) CultureInfo.InvariantCulture);
                dictionary2.Add(key2, (object) str2);
                break;
              }
              break;
            case "PermitAquifer":
              nullable = permit.AquiferId;
              string str3;
              if (nullable.HasValue)
              {
                IService<LU_Aquifer> aquiferService = this._aquiferService;
                nullable = permit.AquiferId;
                int id = nullable.Value;
                // ISSUE: variable of the null type
                str3 = aquiferService.GetById(id, null).Description;
              }
              else
                str3 = upper;
              string str4 = str3;
              dictionary1.Add(key1, (object) str4);
              break;
            case "IssueDate":
              if (permit.DateOut.HasValue)
              {
                dictionary1.Add(key1, (object) Formatter.FormatDate(permit.DateOut.Value));
                break;
              }
              break;
            case "EffectiveDate":
              if (permit.EffectiveDate.HasValue)
              {
                dictionary1.Add(key1, (object) Formatter.FormatDate(permit.EffectiveDate.Value));
                break;
              }
              break;
            case "ExpirationDate":
              if (permit.ExpirationDate.HasValue)
              {
                dictionary1.Add(key1, (object) Formatter.FormatDate(permit.ExpirationDate.Value));
                break;
              }
              break;
            case "FirstAppropriation":
              if (permit.AppropriationDate.HasValue)
              {
                dictionary1.Add(key1, (object) Formatter.FormatDate(permit.AppropriationDate.Value));
                break;
              }
              break;
            case "DateApplicationReceived":
              dictionary1.Add(key1, (object) Formatter.FormatDate(DateTime.Now));
              break;
            case "DatePacketSent":
              string str5 = !permit.ReceivedDate.HasValue ? upper : Formatter.FormatDate(permit.ReceivedDate.Value);
              dictionary1.Add(key1, (object) str5);
              break;
            case "30DaysFromNow":
              Dictionary<string, object> dictionary3 = dictionary1;
              string key3 = key1;
              dateTime1 = DateTime.Now;
              string str6 = Formatter.FormatDate(dateTime1.AddDays(30.0));
              dictionary3.Add(key3, (object) str6);
              break;
            case "30BusinessDaysFromNow":
              dictionary1.Add(key1, (object) Formatter.FormatDate(Formatter.AddBusinessDays(DateTime.Now, 30)));
              break;
            case "60DaysFromNow":
              Dictionary<string, object> dictionary4 = dictionary1;
              string key4 = key1;
              dateTime1 = DateTime.Now;
              string str7 = Formatter.FormatDate(dateTime1.AddDays(60.0));
              dictionary4.Add(key4, (object) str7);
              break;
            case "CurrentYear":
              Dictionary<string, object> dictionary5 = dictionary1;
              string key5 = key1;
              dateTime1 = DateTime.Now;
              num1 = dateTime1.Year;
              string str8 = num1.ToString((IFormatProvider) CultureInfo.InvariantCulture);
              dictionary5.Add(key5, (object) str8);
              break;
            case "PreviousYear":
              Dictionary<string, object> dictionary6 = dictionary1;
              string key6 = key1;
              dateTime1 = DateTime.Now;
              dateTime1 = dateTime1.AddYears(-1);
              num1 = dateTime1.Year;
              string str9 = num1.ToString((IFormatProvider) CultureInfo.InvariantCulture);
              dictionary6.Add(key6, (object) str9);
              break;
            case "2YearsAgoYear":
              Dictionary<string, object> dictionary7 = dictionary1;
              string key7 = key1;
              dateTime1 = DateTime.Now;
              dateTime1 = dateTime1.AddYears(-2);
              num1 = dateTime1.Year;
              string str10 = num1.ToString((IFormatProvider) CultureInfo.InvariantCulture);
              dictionary7.Add(key7, (object) str10);
              break;
            case "Location":
              string str11 = permit.Location ?? upper;
              dictionary1.Add(key1, (object) str11);
              break;
            case "PermitCounty":
              if (!permit.PermitCounties.Any<PermitCounty>())
              {
                source1.Add("Counties for the permit are not specified");
                break;
              }
              if (permit.PermitCounties.FirstOrDefault<PermitCounty>() != null)
              {
                string description = this._countyService.GetById(permit.PermitCounties.FirstOrDefault<PermitCounty>().CountyId, (string) null).Description;
                string str2;
                if (description != null)
                {
                  if (description.ToUpper() != "BALTIMORE CITY")
                    description += " County";
                  str2 = textInfo.ToTitleCase(description.ToLower());
                }
                else
                  str2 = upper;
                dictionary1.Add(key1, (object) str2);
                break;
              }
              source1.Add("County for the permit is not specified");
              break;
            case "HealthDepartmentName":
              if (!permit.PermitCounties.Any<PermitCounty>())
              {
                source1.Add("Counties for the permit are not specified");
                break;
              }
              if (permit.PermitCounties.FirstOrDefault<PermitCounty>() != null)
              {
                string description = this._countyService.GetById(permit.PermitCounties.FirstOrDefault<PermitCounty>().CountyId, (string) null).Description;
                string str2;
                if (description != null)
                {
                  switch (description.ToUpper())
                  {
                    case "BALTIMORE CITY":
                      str2 = textInfo.ToTitleCase(description.ToLower()) + " Health Department";
                      break;
                    case "BALTIMORE":
                      str2 = textInfo.ToTitleCase(description.ToLower()) + " Department of Environmental Protection and Sustainability";
                      break;
                    case "MONTGOMERY":
                      str2 = textInfo.ToTitleCase(description.ToLower()) + " Department of Permitting Services";
                      break;
                    case "WORCESTER":
                      str2 = textInfo.ToTitleCase(description.ToLower()) + " Environmental Programs";
                      break;
                    default:
                      str2 = textInfo.ToTitleCase(description.ToLower()) + " County Health Department";
                      break;
                  }
                }
                else
                  str2 = upper;
                dictionary1.Add(key1, (object) str2);
                break;
              }
              source1.Add("County for the permit is not specified");
              break;
            case "PrimaryUse":
              PermitWaterWithdrawalPurpose withdrawalPurpose = this._permitWaterWithdrawalPurposeService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
              }, "LU_WaterWithdrawalPurpose").FirstOrDefault<PermitWaterWithdrawalPurpose>();
              if (withdrawalPurpose.LU_WaterWithdrawalPurpose != null)
              {
                dictionary1.Add(key1, (object) (withdrawalPurpose.LU_WaterWithdrawalPurpose.Description ?? upper));
                break;
              }
              dictionary1.Add(key1, (object) upper);
              break;
            case "TaxParcel":
              PermitParcel permitParcel1 = this._permitParcelService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
              }, (string) null).FirstOrDefault<PermitParcel>();
              dictionary1.Add(key1, permitParcel1 != null ? (object) string.Format("{0}, {1}, {2}", (object) permitParcel1.TaxMapNumber, (object) permitParcel1.TaxMapBlockNumber, (object) permitParcel1.TaxMapParcel) : (object) upper);
              break;
            case "TaxMapNumber":
              PermitParcel permitParcel2 = this._permitParcelService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
              }, (string) null).FirstOrDefault<PermitParcel>();
              dictionary1.Add(key1, permitParcel2 != null ? (object) permitParcel2.TaxMapNumber : (object) upper);
              break;
            case "TaxMapParcel":
              PermitParcel permitParcel3 = this._permitParcelService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
              }, (string) null).FirstOrDefault<PermitParcel>();
              dictionary1.Add(key1, permitParcel3 != null ? (object) permitParcel3.TaxMapParcel : (object) upper);
              break;
            case "UseDescription":
              if (permit.UseDescription != null)
              {
                dictionary1.Add(key1, (object) (permit.UseDescription ?? upper));
                break;
              }
              dictionary1.Add(key1, (object) upper);
              break;
            case "SourceDescription":
              if (permit.SourceDescription != null)
              {
                dictionary1.Add(key1, (object) (permit.SourceDescription ?? upper));
                break;
              }
              dictionary1.Add(key1, (object) upper);
              break;
            case "RequestedWithdrawalAmount":
              string str12 = string.Format("{0:n0}", (object) (surface == null ? ground.AvgGalPerDay : surface.AvgGalPerDay));
              if (str12 == empty)
                str12 = upper;
              dictionary1.Add(key1, (object) str12);
              break;
            case "PermitType":
              string str13 = upper;
              if (surface == null)
              {
                if (ground != null)
                  str13 = "Groundwater";
              }
              else if (surface != null)
                str13 = "Surface Water";
              dictionary1.Add(key1, (object) str13);
              break;
            case "AlgGal":
              string str14 = upper;
              if (surface == null)
              {
                if (ground != null)
                  str14 = string.Format("{0:n0}", (object) ground.AvgGalPerDay);
              }
              else if (surface != null)
                str14 = string.Format("{0:n0}", (object) surface.AvgGalPerDay);
              dictionary1.Add(key1, (object) str14);
              break;
            case "MaxGal":
              string str15 = upper;
              if (surface == null)
              {
                if (ground != null)
                  str15 = string.Format("{0:n0}", (object) ground.MaxGalPerDay);
              }
              else if (surface != null)
                str15 = string.Format("{0:n0}", (object) surface.MaxGalPerDay);
              dictionary1.Add(key1, (object) str15);
              break;
            case "Surfacewater.AvgGal":
              if (surface == null)
              {
                source1.Add("No Surfacewater information specified.");
                break;
              }
              string str16 = surface.AvgGalPerDay != 0L ? string.Format("{0:n0}", (object) surface.AvgGalPerDay) : upper;
              dictionary1.Add(key1, (object) str16);
              break;
            case "Surfacewater.MaxGal":
              if (surface == null)
              {
                source1.Add("No Surfacewater information specified.");
                break;
              }
              string str17 = surface.MaxGalPerDay != 0L ? string.Format("{0:n0}", (object) surface.MaxGalPerDay) : upper;
              dictionary1.Add(key1, (object) str17);
              break;
            case "Groundwater.AvgGal":
              if (ground == null)
              {
                source1.Add("No Groundwater information specified.");
                break;
              }
              string str18 = ground.AvgGalPerDay != 0L ? string.Format("{0:n0}", (object) ground.AvgGalPerDay) : upper;
              dictionary1.Add(key1, (object) str18);
              break;
            case "Groundwater.MaxGal":
              if (ground != null)
                ;
              string str19 = ground.MaxGalPerDay != 0L ? string.Format("{0:n0}", (object) ground.MaxGalPerDay) : upper;
              dictionary1.Add(key1, (object) str19);
              break;
            case "ExceededPercentage":
              double num2 = 0.0;
              //DateTime b;
              // ISSUE: explicit reference operation
              // ISSUE: variable of a reference type
              //DateTime& local1 = @b;
              dateTime1 = DateTime.Now;
              dateTime1 = dateTime1.AddYears(-1);
              DateTime dateTime2 = new DateTime(dateTime1.Year, 1, 1);
              // ISSUE: explicit reference operation
              //^local1 = dateTime2;
              //DateTime e;
              // ISSUE: explicit reference operation
              // ISSUE: variable of a reference type
              //DateTime& local2 = @e;
              dateTime1 = DateTime.Now;
              dateTime1 = dateTime1.AddYears(-1);
              DateTime dateTime3 = new DateTime(dateTime1.Year, 12, 31);
              // ISSUE: explicit reference operation
              //^local2 = dateTime3;
              foreach (ConditionCompliance conditionCompliance in this._permitConditionService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
              }, "ConditionCompliances").Where<PermitCondition>((Func<PermitCondition, bool>)(c => c.ConditionCompliances.Any<ConditionCompliance>())).Select<PermitCondition, ICollection<ConditionCompliance>>((Func<PermitCondition, ICollection<ConditionCompliance>>)(c => c.ConditionCompliances)).Select<ICollection<ConditionCompliance>, ConditionCompliance>((Func<ICollection<ConditionCompliance>, ConditionCompliance>)(compliance => compliance.Where<ConditionCompliance>((Func<ConditionCompliance, bool>)(c => c.ComplianceReportingStartDate >= dateTime2 && c.ComplianceReportingEndDate <= dateTime3)).FirstOrDefault<ConditionCompliance>((Func<ConditionCompliance, bool>)(c => c.AnnualAverageOverusePercentage > 0.0)))).Where<ConditionCompliance>((Func<ConditionCompliance, bool>)(temp => temp != null)))
                num2 = conditionCompliance.AnnualAverageOverusePercentage;
              dictionary1.Add(key1, (object) (num2 * 100.0).ToString((IFormatProvider) CultureInfo.InvariantCulture));
              break;
            case "BasinCode":
              string str20 = upper;
              if (ground == null)
              {
                IEnumerable<PermitWithdrawalSurfacewaterDetail> range = this._permitWithdrawalSurfacewaterDetailService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
                {
                  new DynamicFilter("PermitWithdrawalSurfacewaterId == " + (object) surface.Id, new object[0])
                }, "LU_Watershed");
                if (range != null)
                {
                  PermitWithdrawalSurfacewaterDetail surfacewaterDetail = range.FirstOrDefault<PermitWithdrawalSurfacewaterDetail>();
                  if (surfacewaterDetail != null && surfacewaterDetail.LU_Watershed != null)
                    str20 = surfacewaterDetail.LU_Watershed.Key;
                }
              }
              else
              {
                IEnumerable<PermitWithdrawalGroundwaterDetail> range = this._permitWithdrawalGroundwaterDetailService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
                {
                  new DynamicFilter("PermitWithdrawalGroundwaterId == " + (object) ground.Id, new object[0])
                }, "LU_Watershed");
                if (range != null)
                {
                  PermitWithdrawalGroundwaterDetail groundwaterDetail = range.FirstOrDefault<PermitWithdrawalGroundwaterDetail>();
                  if (groundwaterDetail != null && groundwaterDetail.LU_Watershed != null)
                    str20 = groundwaterDetail.LU_Watershed.Key;
                }
              }
              dictionary1.Add(key1, (object) str20);
              break;
            case "BasinName":
              string str21 = upper;
              if (ground == null)
              {
                IEnumerable<PermitWithdrawalSurfacewaterDetail> range = this._permitWithdrawalSurfacewaterDetailService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
                {
                  new DynamicFilter("PermitWithdrawalSurfacewaterId == " + (object) surface.Id, new object[0])
                }, "LU_Watershed");
                if (range != null)
                {
                  PermitWithdrawalSurfacewaterDetail surfacewaterDetail = range.FirstOrDefault<PermitWithdrawalSurfacewaterDetail>();
                  if (surfacewaterDetail != null && surfacewaterDetail.LU_Watershed != null)
                    str21 = surfacewaterDetail.LU_Watershed.Description;
                }
              }
              else
              {
                IEnumerable<PermitWithdrawalGroundwaterDetail> range = this._permitWithdrawalGroundwaterDetailService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
                {
                  new DynamicFilter("PermitWithdrawalGroundwaterId == " + (object) ground.Id, new object[0])
                }, "LU_Watershed");
                if (range != null)
                {
                  PermitWithdrawalGroundwaterDetail groundwaterDetail = range.FirstOrDefault<PermitWithdrawalGroundwaterDetail>();
                  if (groundwaterDetail != null && groundwaterDetail.LU_Watershed != null)
                    str21 = groundwaterDetail.LU_Watershed.Description;
                }
              }
              dictionary1.Add(key1, (object) str21);
              break;
            case "Permittee":
            case "ApplicantName":
              string str22 = upper;
              if (permit.PermitContacts == null)
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              PermitContact permitContact2 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c => c.IsPermittee));
              if (permitContact2 != null)
              {
                IService<PermitContactInformation> permitContactService = this._permitContactService;
                nullable = permitContact2.PermitContactInformationId;
                int id = nullable.Value;
                // ISSUE: variable of the null type
                PermitContactInformation byId = permitContactService.GetById(id, null);
                if (byId == null)
                {
                  source1.Add("Permit Contact Information is missing");
                  dictionary1.Add(key1, (object) str22);
                  break;
                }
                if (byId.IsBusiness)
                {
                    if (byId.BusinessName == null)
                    {
                        source1.Add("Permit Contact is Business, but the Business Name is missing");
                        dictionary1.Add(key1, (object)str22);
                        break;
                    }
                    str22 = string.Format("{0}", (object)byId.BusinessName);
                }
                else
                {
                    string first = byId.FirstName ?? string.Empty;
                    string middleInit = byId.MiddleInitial ?? string.Empty;
                    string last = byId.LastName ?? string.Empty;
                    str22 = string.Format("{0} {1}", first, string.IsNullOrEmpty(middleInit) ? last : (middleInit + " " + last));
                }
              }
              dictionary1.Add(key1, (object) str22);
              break;
            case "CarbonCopy":
              string ccField = string.Empty;
              if (permit.PermitContacts == null)
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              var ccContacts = permit.PermitContacts.Where<PermitContact>(c => (c.ContactTypeId.GetValueOrDefault() == 6 && c.ContactTypeId.HasValue));
              var count = 0;
              foreach (PermitContact cc in ccContacts)
              {
                PermitContactInformation ccInfo = _permitContactService.GetById(cc.PermitContactInformationId.Value, null);
                if (ccInfo == null)
                {
                    source1.Add("Permit Carbon Copy Contact Information is missing");
                }
                else
                {
                    if (count > 0)
                        ccField += ",\r\n";
                    ccField = string.Format("{0} {1}", (object)ccInfo.FirstName, string.IsNullOrEmpty(ccInfo.MiddleInitial) ? (object)ccInfo.LastName : (object)(ccInfo.MiddleInitial + " " + ccInfo.LastName));
                    count++;
                }
              }
              dictionary1.Add(key1, ccField);
              break;
            case "PumpageContactName":
              string str23 = upper;
              if (permit.PermitContacts == null)
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              PermitContact permitContact3 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 22 && contactTypeId.HasValue;
              })) ?? permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c => c.IsPermittee));
              if (permitContact3 != null)
              {
                IService<PermitContactInformation> permitContactService = this._permitContactService;
                nullable = permitContact3.PermitContactInformationId;
                int id = nullable.Value;
                // ISSUE: variable of the null type
                PermitContactInformation byId = permitContactService.GetById(id, null);
                if (byId == null)
                {
                  source1.Add("Permit Contact Information is missing");
                  dictionary1.Add(key1, (object) str23);
                  break;
                }
                if (!string.IsNullOrEmpty(byId.BusinessName)) 
                {
                    str23 = string.Format("{0}", (object)byId.BusinessName);
                    if (!string.IsNullOrEmpty(byId.FirstName) || !string.IsNullOrEmpty(byId.LastName))
                        str23 += string.Format("\r\n{0} {1}", (object)byId.FirstName, string.IsNullOrEmpty(byId.MiddleInitial) ? (object)byId.LastName : (object)(byId.MiddleInitial + " " + byId.LastName));
                }
                else if (!string.IsNullOrEmpty(byId.FirstName) || !string.IsNullOrEmpty(byId.LastName))
                    str23 = string.Format("{0} {1}", (object)byId.FirstName, string.IsNullOrEmpty(byId.MiddleInitial) ? (object)byId.LastName : (object)(byId.MiddleInitial + " " + byId.LastName));
                else
                    source1.Add("Permit Contact Information is missing");
              }
              dictionary1.Add(key1, (object) str23);
              break;
            case "ApplicantAddress":
              string str25 = upper;
              if (permit.PermitContacts == null)
              {
                source1.Add("Contacts for the permit are not specified");
                dictionary1.Add(key1, (object) str25);
                break;
              }
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                dictionary1.Add(key1, (object) str25);
                break;
              }
              PermitContact permitContact4 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c => c.IsPermittee));
              if (permitContact4 != null)
              {
                IService<PermitContactInformation> permitContactService = this._permitContactService;
                nullable = permitContact4.PermitContactInformationId;
                int id = nullable.Value;
                PermitContactInformation byId = permitContactService.GetById(id, null);
                if (byId.Address1 != null)
                  str25 = byId.Address1 + "\r\n";
                if (byId.Address2 != null)
                  str25 = str25 + byId.Address2 + "\r\n";
                if (byId.City != null)
                  str25 = str25 + byId.City + " ";
                nullable = byId.StateId;
                if (nullable.HasValue)
                {
                  string key2 = this._stateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
                  {
                    new DynamicFilter("Id == " + (object) byId.StateId, new object[0])
                  }, (string) null).SingleOrDefault<LU_State>().Key;
                  str25 = str25 + key2 + " ";
                }
                if (byId.ZipCode != null)
                  str25 += byId.ZipCode;
              }
              dictionary1.Add(key1, (object) str25);
              break;
            case "PumpageContactAddress":
              string str26 = upper;
              if (permit.PermitContacts == null)
              {
                source1.Add("Contacts for the permit are not specified");
                dictionary1.Add(key1, (object) str26);
                break;
              }
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                dictionary1.Add(key1, (object) str26);
                break;
              }
              PermitContact permitContact5 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 22 && contactTypeId.HasValue;
              })) ?? permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c => c.IsPermittee));
              if (permitContact5 != null)
              {
                IService<PermitContactInformation> permitContactService = this._permitContactService;
                nullable = permitContact5.PermitContactInformationId;
                int id = nullable.Value;
                // ISSUE: variable of the null type
                PermitContactInformation byId = permitContactService.GetById(id, null);
                if (byId.Address1 != null)
                  str26 = byId.Address1 + "\r\n";
                if (byId.Address2 != null)
                  str26 = str26 + byId.Address2 + "\r\n";
                if (byId.City != null)
                  str26 = str26 + byId.City + " ";
                nullable = byId.StateId;
                if (nullable.HasValue)
                {
                  string key2 = this._stateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
                  {
                    new DynamicFilter("Id == " + (object) byId.StateId, new object[0])
                  }, (string) null).SingleOrDefault<LU_State>().Key;
                  str26 = str26 + key2 + " ";
                }
                if (byId.ZipCode != null)
                  str26 += byId.ZipCode;
              }
              dictionary1.Add(key1, (object) str26);
              break;
            case "ApplicantPhone":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              string str27 = upper;
              PermitContact permitContact6 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c => c.IsPermittee));
              if (permitContact6 != null)
              {
                ContactCommunicationMethod communicationMethod = this._communicationMethodService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
                {
                  new DynamicFilter("ContactId == " + (object) permitContact6.ContactId, new object[0]),
                  new DynamicFilter("IsPrimaryPhone == True", new object[0])
                }, (string) null).FirstOrDefault<ContactCommunicationMethod>();
                str27 = communicationMethod == null ? empty : communicationMethod.CommunicationValue;
              }
              dictionary1.Add(key1, (object) str27);
              break;
            case "ProjectManagerName":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              PermitContact permitContact7 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 20 && contactTypeId.HasValue;
              }));
              dictionary1.Add(key1, permitContact7 != null ? (object) string.Format("{0} {1}", (object) permitContact7.Contact.FirstName, (object) permitContact7.Contact.LastName) : (object) "Unknown");
              break;
            case "ProjectSupervisorName":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              PermitContact permitContact8 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 24 && contactTypeId.HasValue;
              }));
              dictionary1.Add(key1, permitContact8 != null ? (object) string.Format("{0} {1}", (object) permitContact8.Contact.FirstName, (object) permitContact8.Contact.LastName) : (object) "Unknown");
              break;
            case "EnvironmentalHealthDirector":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              PermitContact permitContact9 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 33 && contactTypeId.HasValue;
              }));
              dictionary1.Add(key1, permitContact9 != null ? (object) string.Format("{0} {1}", (object) permitContact9.Contact.FirstName, (object) permitContact9.Contact.LastName) : (object) "Unknown");
              break;
            case "ProjectManagerTitle":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              string str28 = upper;
              PermitContact permitContact10 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 20 && contactTypeId.HasValue;
              }));
              if (permitContact10 != null)
              {
                nullable = permitContact10.Contact.TitleId;
                if (nullable.HasValue)
                {
                  IService<LU_Title> titleService = this._titleService;
                  nullable = permitContact10.Contact.TitleId;
                  int id = nullable.Value;
                  // ISSUE: variable of the null type
                  str28 = titleService.GetById(id, null).Description;
                }
              }
              dictionary1.Add(key1, (object) str28);
              break;
            case "ProjectSupervisorTitle":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              string str29 = upper;
              PermitContact permitContact11 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 24 && contactTypeId.HasValue;
              }));
              if (permitContact11 != null)
              {
                nullable = permitContact11.Contact.TitleId;
                if (nullable.HasValue)
                {
                  IService<LU_Title> titleService = this._titleService;
                  nullable = permitContact11.Contact.TitleId;
                  int id = nullable.Value;
                  // ISSUE: variable of the null type
                  str29 = titleService.GetById(id, null).Description;
                }
              }
              dictionary1.Add(key1, (object) str29);
              break;
            case "SignerName":
              string appSetting = ConfigurationManager.AppSettings["PermitSignerName"];
              dictionary1.Add(key1, (object) appSetting);
              break;
            case "Conditions":
              List<string> list = Formatter.ReplaceVariables(this._permitConditionService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("PermitId == " + (object) permit.Id, new object[0])
              }, (string) null).ToList<PermitCondition>().Select<PermitCondition, CustomCondition>((Func<PermitCondition, CustomCondition>) (c => new CustomCondition()
              {
                ConditionText = c.ConditionText,
                DueDate = !permit.EffectiveDate.HasValue || !c.OneTimeReportDays.HasValue ? new DateTime?() : new DateTime?(permit.EffectiveDate.Value.AddDays((double) c.OneTimeReportDays.Value))
              })), permit, surface, ground).Select<CustomCondition, string>((Func<CustomCondition, string>) (c => c.ConditionText)).ToList<string>();
              dictionary1.Add(key1, (object) list);
              break;
            case "Enclosures":
              dictionary1.Add(key1, (object) docs);
              break;
            case "InterestedParties":
              if (!permit.PermitContacts.Any<PermitContact>())
              {
                source1.Add("Contacts for the permit are not specified");
                break;
              }
              IEnumerable<PermitContact> source2 = permit.PermitContacts.Where<PermitContact>((Func<PermitContact, bool>) (c =>
              {
                int? contactTypeId = c.ContactTypeId;
                return contactTypeId.GetValueOrDefault() == 15 && contactTypeId.HasValue;
              }));
              StringBuilder stringBuilder = new StringBuilder();
              foreach (Contact contact in source2.Select<PermitContact, Contact>((Func<PermitContact, Contact>) (item => item.Contact)))
              {
                stringBuilder.Append(string.Format("{0} {1} ", (object) contact.FirstName, (object) contact.LastName));
                if (contact.Address1 != null)
                  stringBuilder.Append(string.Format("{0} ", (object) contact.Address1));
                if (contact.Address2 != null)
                  stringBuilder.Append(string.Format("{0} ", (object) contact.Address2));
                if (contact.City != null)
                  stringBuilder.Append(contact.City + " ");
                nullable = contact.StateId;
                if (nullable.HasValue)
                {
                  string key2 = this._stateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
                  {
                    new DynamicFilter("Id == " + (object) contact.StateId, new object[0])
                  }, (string) null).SingleOrDefault<LU_State>().Key;
                  stringBuilder.Append(key2 + " ");
                }
                if (contact.ZipCode != null)
                  stringBuilder.Append(contact.ZipCode);
                stringBuilder.AppendLine().AppendLine();
              }
              dictionary1.Add(key1, (object) stringBuilder.ToString());
              break;
            case "SignatureImage":
              if (finalPermit)
              {
                if (!permit.PermitContacts.Any<PermitContact>())
                {
                  source1.Add("Contacts for the permit are not specified");
                  break;
                }
                PermitContact permitContact12 = permit.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (c =>
                {
                  int? contactTypeId = c.ContactTypeId;
                  return contactTypeId.GetValueOrDefault() == 24 && contactTypeId.HasValue;
                }));
                if (permitContact12 != null)
                {
                  if (!string.IsNullOrEmpty(permitContact12.Contact.SignatureFileName))
                  {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                      Image.FromFile(digitalSignatureFolder + permitContact12.Contact.SignatureFileName).Save((Stream) memoryStream, ImageFormat.Jpeg);
                      array = memoryStream.ToArray();
                    }
                    dictionary1.Add(key1, (object) array);
                  }
                  else
                    source1.Add("No Electronic Signature on file for the user logged in");
                  break;
                }
                break;
              }
              break;
            case "ChiefSignatureImd":
              if (finalPermit)
              {
                Contact byId = this._contactService.GetById(contactId, (string) null);
                if (byId != null && byId.SignatureFileName != null)
                {
                  using (MemoryStream memoryStream = new MemoryStream())
                  {
                    Image.FromFile(digitalSignatureFolder + byId.SignatureFileName).Save((Stream) memoryStream, ImageFormat.Jpeg);
                    array = memoryStream.ToArray();
                  }
                  dictionary1.Add(key1, (object) array);
                  break;
                }
                source1.Add("No Electronic Signature on file for the user logged in");
                break;
              }
              break;
            case "for":
              if (permit.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (pc =>
              {
                int num;
                if (pc.ContactId == contactId)
                {
                  int? contactTypeId = pc.ContactTypeId;
                  num = contactTypeId.GetValueOrDefault() != 32 ? 0 : (contactTypeId.HasValue ? 1 : 0);
                }
                else
                  num = 0;
                return num != 0;
              })))
              {
                dictionary1.Add(key1, (object) string.Empty);
                break;
              }
              break;
            case "CustomPackageOriginationDate":
            case "DraftPermitOriginationDate":
              Dictionary<string, object> dictionary8 = dictionary1;
              string key8 = key1;
              dateTime1 = DateTime.Today;
              string str30 = dateTime1.ToString("MMMM d, yyyy");
              dictionary8.Add(key8, (object) str30);
              break;
          }
        }
        if (source1.Any<string>())
          dictionary1.Add("Errors", (object) source1);
        return (IDictionary<string, object>) dictionary1;
      }
      catch (Exception ex)
      {
        dictionary1.Add("Errors", (object) ex.Message);
        return (IDictionary<string, object>) dictionary1;
      }
    }
  }
}
