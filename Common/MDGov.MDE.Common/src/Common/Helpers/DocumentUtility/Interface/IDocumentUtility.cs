﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Helpers.DocumentUtility.Interface.IDocumentUtility
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.Common.Helpers.DocumentUtility.Interface
{
  public interface IDocumentUtility
  {
    IDictionary<string, object> GetPermitTemplateVariables(IEnumerable<string> keys, Permit permit, List<string> docs, string digitalSignatureFolder, int contactId, bool finalPermit = false);
  }
}
