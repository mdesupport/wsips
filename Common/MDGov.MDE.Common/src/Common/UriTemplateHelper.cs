﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.UriTemplateHelper
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

namespace MDGov.MDE.Common
{
  public class UriTemplateHelper
  {
    public const string RangeTemplate = "GetRange?skip={0}&take={1}&orderby={2}";
    public const string IdTemplate = "GetById?Id={0}";
    public const string CountTemplate = "Count?filter={0}";
    public const string GetAllTemplate = "GetAll?includeProperties={0}";
    public const string DeleteTemplate = "Delete?Id={0}";
    public const string SaveTemplate = "Save";
  }
}
