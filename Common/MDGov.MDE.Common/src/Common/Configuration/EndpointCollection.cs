﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Configuration.EndpointCollection
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using System.Configuration;

namespace MDGov.MDE.Common.Configuration
{
  public class EndpointCollection : ConfigurationElementCollection
  {
    public Endpoint this[int index]
    {
      get
      {
        return this.BaseGet(index) as Endpoint;
      }
      set
      {
        if (this.BaseGet(index) != null)
          this.BaseRemoveAt(index);
        this.BaseAdd(index, (ConfigurationElement) value);
      }
    }

    public Endpoint this[string key]
    {
      get
      {
        return this.BaseGet((object) key) as Endpoint;
      }
      set
      {
        if (this.BaseGet((object) key) != null)
          this.BaseRemove((object) key);
        this.BaseAdd((ConfigurationElement) value);
      }
    }

    protected override ConfigurationElement CreateNewElement()
    {
      return (ConfigurationElement) new Endpoint();
    }

    protected override object GetElementKey(ConfigurationElement element)
    {
      return (object) ((Endpoint) element).Type;
    }
  }
}
