﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Configuration.ServiceConfig
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using System.Configuration;

namespace MDGov.MDE.Common.Configuration
{
  public class ServiceConfig : ConfigurationSection
  {
    [ConfigurationCollection(typeof (EndpointCollection))]
    [ConfigurationProperty("endpoints", IsDefaultCollection = true)]
    public EndpointCollection Endpoints
    {
      get
      {
        return this["endpoints"] as EndpointCollection;
      }
    }

    public static ServiceConfig GetConfig()
    {
      return (ServiceConfig) ConfigurationManager.GetSection("serviceConfig") ?? new ServiceConfig();
    }
  }
}
