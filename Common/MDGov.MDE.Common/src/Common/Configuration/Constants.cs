﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Configuration.Constants
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

namespace MDGov.MDE.Common.Configuration
{
  public static class Constants
  {
    public const string ADMINISTRATIVE_SPECIALIST = "Administrative Specialist";
    public const string APPLICANT_ROLE = "Applicant / Permittee";
    public const string COMPLIANCE_MANAGER = "Compliance / Enforcement Manager";
    public const string COMPLIANCE_STAFF = "Compliance / Enforcement Staff";
    public const string DEFAULT_ROLE = "MDE Staff";
    public const string DIVISION_CHIEF = "Division Chief";
    public const string ECOMMERCE_MEMBER = "WSIPS eCommerce Member";
    public const string EXTERNAL_AUTHORITY_ROLE = "External Authority";
    public const string IT_ADMIN = "IT Administrator";
    public const string OPERATIONAL_ADMIN = "Operational Administrator";
    public const string PERMIT_SUPERVISOR = "Permit Supervisor";
    public const string PROJECT_MANAGER = "Project Manager";
    public const string SECRETARY = "Secretary";
    public const string SYSTEM = "System";
    public const string WORD_MIME_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public const string ZIP_MIME_TYPE = "application/zip";
  }
}
