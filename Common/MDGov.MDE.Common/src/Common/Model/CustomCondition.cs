﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Model.CustomCondition
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using System;

namespace MDGov.MDE.Common.Model
{
  public class CustomCondition
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public string ConditionText { get; set; }

    public int? OneTimeReportDays { get; set; }

    public DateTime? DueDate { get; set; }

    public int? ConditionComplianceId { get; set; }

    public int StandardConditionTypeId { get; set; }

    public bool CanBeDeleted { get; set; }
  }
}
