﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Model.DynamicFilter
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

namespace MDGov.MDE.Common.Model
{
  public class DynamicFilter
  {
    public string Predicate { get; set; }

    public object[] Values { get; set; }

    public DynamicFilter()
    {
    }

    public DynamicFilter(string predicate, params object[] values)
    {
      this.Predicate = predicate;
      this.Values = values;
    }
  }
}
