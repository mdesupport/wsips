﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Model.ErrorLog
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using System;

namespace MDGov.MDE.Common.Model
{
  public class ErrorLog : IUpdatableEntity
  {
    public int Id { get; set; }

    public DateTime ErrorTime { get; set; }

    public string Message { get; set; }

    public string ExceptionMessage { get; set; }

    public string ExceptionType { get; set; }

    public string StackTrace { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public string Application { get; set; }

    public string MessageType { get; set; }
  }
}
