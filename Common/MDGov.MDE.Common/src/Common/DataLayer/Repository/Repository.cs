﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.DataLayer.Repository`1
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace MDGov.MDE.Common.DataLayer
{
  public class Repository<T> : IRepository<T> where T : class, IUpdatableEntity
  {
    protected readonly DbContext _context;
    protected readonly IErrorLogging _loggingContext;

    public Repository(DbContext context, IErrorLogging loggingContext)
    {
      this._context = context;
      this._loggingContext = loggingContext;
    }

    public Repository(LoggingContext context)
    {
      this._context = (DbContext) context;
    }

    public IQueryable<T> All(string includeProperties = null)
    {
      return (IQueryable<T>) this.EntitySet(includeProperties);
    }

    public IQueryable<T> GetRange(int skip, int take, string order, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
            IQueryable<T> source1 = (IQueryable<T>)this.EntitySet(includeProperties);
            if (filters != null)
            {
                foreach (DynamicFilter dynamicFilter in filters.Where<DynamicFilter>((Func<DynamicFilter, bool>)(f => !string.IsNullOrEmpty(f.Predicate))))
                    source1 = source1.Where<T>(dynamicFilter.Predicate, dynamicFilter.Values);
            }
            if (string.IsNullOrEmpty(order))
                order = "Id";
            IQueryable<T> source2 = source1.OrderBy<T>(order);
            if (skip > 0)
                source2 = source2.Skip<T>(skip);
            if (take < int.MaxValue)
                source2 = source2.Take<T>(take);
            return source2;
    }

    public T GetById(int id, string includeProperties = null)
    {
      return this.EntitySet(includeProperties).FirstOrDefault<T>((Expression<Func<T, bool>>) (entity => entity.Id == id));
    }

    public int Save(T model)
    {
      model.LastModifiedDate = DateTime.Now;
      if (model.Id == 0)
      {
        model.CreatedBy = model.LastModifiedBy;
        model.CreatedDate = model.LastModifiedDate;
        model = this._context.Set<T>().Add(model);
      }
      else if (this._context.Entry<T>(model).State == EntityState.Detached)
      {
        T entity = this._context.Set<T>().Find((object) model.Id);
        if ((object) entity != null)
        {
          DbEntityEntry<T> dbEntityEntry = this._context.Entry<T>(entity);
          model.CreatedBy = entity.CreatedBy;
          model.CreatedDate = entity.CreatedDate;
          dbEntityEntry.CurrentValues.SetValues((object) model);
        }
      }
      try
      {
        this._context.SaveChanges();
      }
      catch (DbEntityValidationException ex)
      {
        foreach (DbEntityValidationResult entityValidationError in ex.EntityValidationErrors)
        {
          this._loggingContext.Log(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", (object) entityValidationError.Entry.Entity.GetType().Name, (object) entityValidationError.Entry.State), LogMessageType.Error);
          foreach (DbValidationError validationError in (IEnumerable<DbValidationError>) entityValidationError.ValidationErrors)
            this._loggingContext.Log(string.Format("- Property: \"{0}\", Error: \"{1}\"", (object) validationError.PropertyName, (object) validationError.ErrorMessage), LogMessageType.Error);
        }
      }
      return model.Id;
    }

    public void Delete(int id)
    {
      T entity = this._context.Set<T>().Find((object) id);
      if ((object) entity == null)
        return;
      this._context.Set<T>().Remove(entity);
      this._context.SaveChanges();
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      IQueryable<T> source = (IQueryable<T>) this._context.Set<T>();
      if (filters != null)
      {
        foreach (DynamicFilter dynamicFilter in filters.Where<DynamicFilter>((Func<DynamicFilter, bool>) (f => !string.IsNullOrEmpty(f.Predicate))))
          source = source.Where<T>(dynamicFilter.Predicate, dynamicFilter.Values);
      }
      return source.Count<T>();
    }

    private DbQuery<T> EntitySet(string includeProperties)
    {
            DbQuery<T> dbQuery = (DbQuery<T>)this._context.Set<T>();
            if (string.IsNullOrEmpty(includeProperties))
                return dbQuery;
            string str = includeProperties;
            char[] chArray = new char[1] { ',' };
            foreach (string path in str.Split(chArray))
                dbQuery = dbQuery.Include(path);
            return dbQuery;
    }
  }
}
