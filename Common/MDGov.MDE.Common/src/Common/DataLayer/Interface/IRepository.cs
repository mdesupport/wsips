﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.DataLayer.IRepository`1
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Model;
using System.Collections.Generic;
using System.Linq;

namespace MDGov.MDE.Common.DataLayer
{
  public interface IRepository<T>
  {
    IQueryable<T> All(string includeProperties = null);

    IQueryable<T> GetRange(int skip, int take, string order, IEnumerable<DynamicFilter> filters, string includeProperties = null);

    T GetById(int id, string includeProperties = null);

    int Save(T entity);

    void Delete(int id);

    int Count(IEnumerable<DynamicFilter> filters);
  }
}
