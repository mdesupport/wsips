﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.ServiceLayer.UpdatableServiceBase`1
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using StructureMap;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace MDGov.MDE.Common.ServiceLayer
{
  public class UpdatableServiceBase<T> : ServiceBase<T>, IUpdatableService<T>, IService<T>
  {
    public virtual int Save(T entity)
    {
      HttpResponseMessage result = this.Client.PostAsync<T>("Save", entity, (MediaTypeFormatter) GlobalConfiguration.Configuration.Formatters.JsonFormatter).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
        this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    public virtual void Delete(int id)
    {
      HttpResponseMessage result = this.Client.DeleteAsync(string.Format("Delete?Id={0}", (object) id)).Result;
      if (result.IsSuccessStatusCode)
        return;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
    }

    private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      if (statusCode == HttpStatusCode.NotFound)
      {
        exception.ExceptionType = "NotFound";
        exception.ExceptionMessage = exception.Message;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }
  }
}
