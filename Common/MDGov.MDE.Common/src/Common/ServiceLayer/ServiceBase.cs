﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.ServiceLayer.ServiceBase`1
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using StructureMap;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MDGov.MDE.Common.ServiceLayer
{
  public class ServiceBase<T> : HttpClientBase<T>, IService<T>
  {
    public IEnumerable<T> GetAll(string includeProperties = null)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetAll?includeProperties={0}", (object) includeProperties)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<T>>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<T>) null;
    }

    public IEnumerable<T> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync<IEnumerable<DynamicFilter>>(string.Format("GetRange?skip={0}&take={1}&orderby={2}", (object) skip, (object) take, (object) orderBy) + string.Format("&includeProperties={0}", (object) includeProperties), filters).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<T>>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<T>) null;
    }

    public T GetById(int id, string includeProperties = null)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetById?Id={0}", (object) id) + string.Format("&includeProperties={0}", (object) includeProperties)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<T>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return default (T);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync<IEnumerable<DynamicFilter>>("Count?filter={0}", filters).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      if (statusCode == HttpStatusCode.NotFound)
      {
        exception.ExceptionType = "NotFound";
        exception.ExceptionMessage = exception.Message;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }
  }
}
