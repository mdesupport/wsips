﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.ServiceLayer.HttpClientBase
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Configuration;
using MDGov.MDE.Common.Logging.Interface;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace MDGov.MDE.Common.ServiceLayer
{
  public abstract class HttpClientBase
  {
    public HttpClient Client { get; set; }

    protected HttpClientBase(string endpointUri)
    {
      IErrorLogging instance = ObjectFactory.GetInstance<IErrorLogging>();
      try
      {
        ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback) ((s, certificate, chain, sslPolicyErrors) => true);
        WebRequestHandler webRequestHandler = new WebRequestHandler();
        string appSetting = ConfigurationManager.AppSettings["ExternalServiceClientCertificateDistinguishedName"];
        if (!string.IsNullOrEmpty(appSetting))
        {
          X509Store x509Store = new X509Store(StoreLocation.LocalMachine);
          x509Store.Open(OpenFlags.ReadOnly);
          X509Certificate2Collection certificate2Collection = x509Store.Certificates.Find(X509FindType.FindBySubjectDistinguishedName, (object) appSetting, true);
          webRequestHandler.ClientCertificates.AddRange((X509CertificateCollection) certificate2Collection);
        }
        this.Client = new HttpClient((HttpMessageHandler) webRequestHandler);
        if (string.IsNullOrEmpty(endpointUri))
        {
          string fullName = ((IEnumerable<Type>) this.GetType().BaseType.GenericTypeArguments).First<Type>().FullName;
          Endpoint endpoint = ServiceConfig.GetConfig().Endpoints[fullName];
          if (endpoint == null)
            throw new NullReferenceException("Endpoint element not found: " + fullName);
          this.Client.BaseAddress = new Uri(endpoint.Uri);
        }
        else
          this.Client.BaseAddress = new Uri(endpointUri);
        this.Client.DefaultRequestHeaders.Add("Accept", "application/json");
      }
      catch (Exception ex)
      {
        instance.Log(ex);
      }
    }
  }

  public abstract class HttpClientBase<T> : HttpClientBase
  {
    protected HttpClientBase()
      : base((string) null)
    {
    }
  }
}
