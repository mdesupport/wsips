﻿using MDGov.MDE.Common.Model;

namespace MDGov.MDE.Common.ServiceLayer
{
    public interface IUpdatableService<T> : IService<T>
    {
        /// <summary>
        /// Saves an entity
        /// </summary>
        /// <param name="entity">Data entity</param>
        /// <returns>Id (integer)</returns>
        int Save(T entity);

        /// <summary>
        /// Deletes an entity
        /// </summary>
        /// <param name="id">Entity Id</param>
        void Delete(int id);
    }
}