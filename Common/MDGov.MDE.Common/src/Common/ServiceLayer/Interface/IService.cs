﻿using System.Collections.Generic;
using MDGov.MDE.Common.Model;

namespace MDGov.MDE.Common.ServiceLayer
{
    public interface IService<T>
    {
        /// <summary>
        /// Get All 
        /// </summary>
        /// <returns>Collection of Entities</returns>
        IEnumerable<T> GetAll(string includeProperties = null);

        /// <summary>
        /// Gets Entities based on specified parameters
        /// </summary>
        /// <param name="skip">Skip (Integer)</param>
        /// <param name="take">Take (Integer)</param>
        /// <param name="orderBy">Order By (String)</param>
        /// <param name="filters">Collection of DynamicFilter objects</param>
        /// <param name="includeProperties">Optional string of navigation properties to include</param>
        /// <returns>Collection of Entities</returns>           
        IEnumerable<T> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null);

        /// <summary>
        /// Gets entity by Id
        /// </summary>
        /// <param name="id">Id (integer)</param>
        /// <param name="includeProperties">Optional string of navigation properties to include</param>
        /// <returns>Entity</returns>
        T GetById(int id, string includeProperties = null);

        /// <summary>
        /// Gets total number of entities based on the filters specified
        /// </summary>
        /// <param name="filters">Collection of DynamicFilter objects</param>
        /// <returns>Integer</returns>
        int Count(IEnumerable<DynamicFilter> filters);
    }
}