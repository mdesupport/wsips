﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Logging.Interface.IErrorLogging
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.Helpers.Common;
using System;

namespace MDGov.MDE.Common.Logging.Interface
{
  public interface IErrorLogging
  {
    void Log(ApiException exc);

    void Log(Exception exc);

    void Log(string message, LogMessageType logMessageType);
  }
}
