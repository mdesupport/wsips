﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Common.Logging.ErrorLogging
// Assembly: MDGov.MDE.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D5BCFEAA-0777-4EF0-8C8C-B38F11E0F551
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Communication\bin\MDGov.MDE.Common.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Helpers.Configuration;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using System;
using System.Diagnostics;
using System.IO;

namespace MDGov.MDE.Common.Logging
{
  public class ErrorLogging : IErrorLogging
  {
    private readonly IRepository<ErrorLog> _loggerRepository;
    private readonly string _userName;

    public ErrorLogging()
    {
      this._userName = Environment.UserName;
    }

    public ErrorLogging(IRepository<ErrorLog> loggerRepository)
    {
      this._loggerRepository = loggerRepository;
      this._userName = Environment.UserName;
    }

    public void Log(ApiException exc)
    {
      if (exc == null)
        return;
      this.SaveLogEntry(new ErrorLog()
      {
        LastModifiedBy = this._userName,
        LastModifiedDate = DateTime.Now,
        ErrorTime = DateTime.Now,
        Message = exc.Error,
        ExceptionType = exc.ExceptionType,
        ExceptionMessage = exc.ExceptionMessage,
        StackTrace = exc.StackTrace,
        MessageType = LogMessageType.Error.ToString(),
        Application = Config.AppName
      });
    }

    public void Log(Exception exc)
    {
      if (exc == null)
        return;
      this.SaveLogEntry(new ErrorLog()
      {
        LastModifiedBy = this._userName,
        LastModifiedDate = DateTime.Now,
        ErrorTime = DateTime.Now,
        Message = exc.Message,
        ExceptionType = exc.GetType().ToString(),
        StackTrace = exc.StackTrace,
        MessageType = LogMessageType.Error.ToString(),
        Application = Config.AppName
      });
    }

    public void Log(string message, LogMessageType logMessageType)
    {
      if (string.IsNullOrEmpty(message))
        return;
      this.SaveLogEntry(new ErrorLog()
      {
        LastModifiedBy = this._userName,
        LastModifiedDate = DateTime.Now,
        ErrorTime = DateTime.Now,
        Message = message,
        ExceptionType = string.Empty,
        StackTrace = string.Empty,
        MessageType = logMessageType.ToString(),
        Application = Config.AppName
      });
    }

    private void WriteToATextFile(ErrorLog logItem)
    {
      string logsPath = Config.LogsPath;
      string path2 = string.Format("{0}-{1}", (object) DateTime.Now.ToString("yyyyMMddhhmmss"), (object) Config.AppName);
      string str1 = Path.Combine(logsPath, "Errors");
      if (!Directory.Exists(str1))
        Directory.CreateDirectory(str1);
      using (StreamWriter streamWriter1 = new StreamWriter(Path.Combine(str1, path2) + ".txt"))
      {
        StreamWriter streamWriter2 = streamWriter1;
        string format = "Error Recorded: {0} at {1}";
        DateTime now = DateTime.Now;
        string shortDateString = now.ToShortDateString();
        now = DateTime.Now;
        string shortTimeString = now.ToShortTimeString();
        string str2 = string.Format(format, (object) shortDateString, (object) shortTimeString);
        streamWriter2.WriteLine(str2);
        if (!string.IsNullOrWhiteSpace(logItem.Message))
          streamWriter1.WriteLine(logItem.Message);
        if (!string.IsNullOrWhiteSpace(logItem.ExceptionMessage))
          streamWriter1.WriteLine(logItem.ExceptionMessage);
        if (!string.IsNullOrWhiteSpace(logItem.StackTrace))
          streamWriter1.WriteLine(logItem.StackTrace);
        streamWriter1.WriteLine("----------{0}{1}", (object) Environment.NewLine, (object) Environment.NewLine);
      }
    }

    private void WriteToAnEventLog(ErrorLog logItem)
    {
      string appName = Config.AppName;
      string message = (!string.IsNullOrWhiteSpace(logItem.Message) ? logItem.Message : logItem.ExceptionMessage) + Environment.NewLine + logItem.StackTrace;
      if (!EventLog.SourceExists(appName))
        EventLog.CreateEventSource(appName, "Application");
      EventLogEntryType type = EventLogEntryType.Error;
      switch (logItem.MessageType)
      {
        case "Information":
          type = EventLogEntryType.Information;
          break;
        case "Warning":
          type = EventLogEntryType.Warning;
          break;
      }
      EventLog.WriteEntry(appName, message, type, 1000);
    }

    private void SaveLogEntry(ErrorLog logItem)
    {
      if ((LogMessageType) Enum.Parse(typeof (LogMessageType), logItem.MessageType) < (LogMessageType) Config.LogMessageLevel)
        return;
      try
      {
        this._loggerRepository.Save(logItem);
      }
      catch (Exception ex1)
      {
        try
        {
          this.WriteToATextFile(logItem);
        }
        catch (Exception ex2)
        {
          try
          {
            this.WriteToAnEventLog(logItem);
          }
          catch (Exception ex3)
          {
          }
        }
      }
    }
  }
}
