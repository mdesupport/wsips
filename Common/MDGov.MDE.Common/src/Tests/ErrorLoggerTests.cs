﻿using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace Tests
{
    [TestClass]
    public class ErrorLoggerTests
    {

        [TestMethod]
        public void LogException_ValidException_MessageIsCorrect()
        {
            var exc = new ApiException{ Error = "Error Occurred" };
            var mockedLogger = new Mock<IErrorLogging>();
            
            mockedLogger.Setup(x => x.Log(exc));
            mockedLogger.Object.Log(exc);

            mockedLogger.Verify(x => x.Log(It.Is<ApiException>(s => s.Error.StartsWith("E"))), Times.Exactly(1));
        }

        [TestMethod]
        public void LogException_2CustomExceptionsLogged_ObjectsAreCorrect()
        {
            var exc = new CustomException(new ApiException { Error = "Custom Error Occurred", StackTrace = "Boo" });
            var mockedLogger = new Mock<IErrorLogging>();

            mockedLogger.Setup(x => x.Log(exc));

            mockedLogger.Object.Log(exc);
            mockedLogger.Object.Log(exc);

            mockedLogger.Verify(x => x.Log(It.Is<CustomException>(s => s.StackTrace.Equals("Boo"))), Times.Exactly(2));
        }

        [TestMethod]
        public void LogErrorMessage_ValidMessage_MessageIsCorrect()
        {
            const string error = "Some Error Occurred";
            var mockedLogger = new Mock<IErrorLogging>();

            mockedLogger.Setup(x => x.Log(error, LogMessageType.Error));
            mockedLogger.Object.Log(error, LogMessageType.Error);

            mockedLogger.Verify(x => x.Log("Some Error Occurred", LogMessageType.Error));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void LogException_ApiExceptionIsNull_ExceptionIsThrown()
        {
            ApiException exc = null;
            var mockedLogger = new Mock<IErrorLogging>();

            mockedLogger.Setup(x => x.Log(exc)).Throws<ArgumentNullException>();

            mockedLogger.Object.Log(exc);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void LogException_CustomExceptionIsNull_ExceptionIsThrown()
        {
            CustomException exc = null;
            var mockedLogger = new Mock<IErrorLogging>();

            mockedLogger.Setup(x => x.Log(exc)).Throws<ArgumentNullException>();

            mockedLogger.Object.Log(exc);
        }
    }
}
