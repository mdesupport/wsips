USE [MdeReports]
GO

/****** Object:  StoredProcedure [dbo].[OverAppropriation]    Script Date: 5/2/2017 3:15:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER  PROCEDURE [dbo].[OverAppropriation]
(@Year int,
 @AnnualAverageAllocation float,
 @AnnualAverageOverusePercentage float)
AS
BEGIN
 
    -- EXECUTE [dbo].[OverAppropriation] 2015, 0, 0

	if @Year = NULL set @Year = year(GETDATE()) - 1;
	if @AnnualAverageAllocation = NULL set @AnnualAverageAllocation = 0;
	if @AnnualAverageOverusePercentage = NULL set @AnnualAverageOverusePercentage = 0;
	  
	WITH SupplementPermitInfo AS (	SELECT PSG1.PermitId, PSG1.SUPPLEMENTALGROUPID,
										   CASE PSG1.SUPPLEMENTALTYPEID WHEN 2 THEN 'Combined' WHEN 3 THEN 'Special' ELSE '' END SupplementalType,
										   STUFF(( SELECT  ', '+ ISNULL(p.PermitName , '')  
												   FROM MdeWaterPermitting.dbo.PERMITSUPPLEMENTALGROUP a 
														INNER JOIN MdeWaterPermitting.dbo.PERMIT P ON a.PermitId = p.Id
														WHERE A.SUPPLEMENTALGROUPID = psg1.SUPPLEMENTALGROUPID  AND SUPPLEMENTALTYPEID IN (2, 3) 
															  AND p.Id <> PSG1.PERMITID FOR XML PATH('')),1 ,1, '' ) SupplementalPermitNames,
										   STUFF(( SELECT  ', '+  CAST(p.Id AS VARCHAR(20))
												   FROM MdeWaterPermitting.dbo.PERMITSUPPLEMENTALGROUP a 
														INNER JOIN MdeWaterPermitting.dbo.PERMIT P ON a.PermitId = p.Id
														WHERE A.SUPPLEMENTALGROUPID = psg1.SUPPLEMENTALGROUPID  AND SUPPLEMENTALTYPEID IN (2, 3) 
															  AND p.Id <> PSG1.PERMITID FOR XML PATH('')),1 ,1, '' ) SupplementalPermitIds
									FROM   MdeWaterPermitting.dbo.PERMITSUPPLEMENTALGROUP PSG1  
									WHERE  EXISTS (  SELECT SUPPLEMENTALGROUPID FROM  MdeWaterPermitting.dbo.PERMITSUPPLEMENTALGROUP PSG2
													 WHERE  PSG1.SUPPLEMENTALGROUPID = PSG2.SUPPLEMENTALGROUPID ) AND SUPPLEMENTALTYPEID IN (2, 3))

	-- Not calculating supplemental permits correctly
	--SELECT * FROM (
	--SELECT	PermitNumber, p.PermitName, dbo.GetPermitteeName(p.Id) PermitteeName,[dbo].[GetTopTypeOfUse](p.Id) TypeOfUse, 
	--		ISNULL(t.SupplementalPermitNames,'') SupplementalPermits, SupplementalType, t.AvgGalPerDay ListedPermitAllocation, 
	--		CASE WHEN t.SUPPLEMENTALGROUPID IS NULL THEN t.AvgGalPerDay 
	--		ELSE SUM(t.AvgGalPerDay) OVER (PARTITION BY t.SUPPLEMENTALGROUPID) END TotalPermitAllocation, 
	--		ROUND(t.PermitAnnualAverageUsage,0) PermitAnnualAverageUsage, ROUND(t.AnnualAverageUsage,0) AvgGalPerDayUsage, 
	--		CAST(ROUND(t.AnnualAverageOverusePercentage,0) AS VARCHAR(50)) + '%' AnnualAverageOverusePercentage
	--FROM	(SELECT p.Id PermitId, cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay, 
	--				cc.AnnualAverageOverusePercentage , cc.AnnualAverageUsage, spi.SUPPLEMENTALGROUPID , cc.PermitAnnualAverageUsage,
	--				ISNULL(spi.SupplementalPermitNames, '') SupplementalPermitNames, ISNULL(spi.SupplementalType, '') SupplementalType
	--		FROM	MdeWaterPermitting..Permit p   
	--			INNER JOIN MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1 
	--			INNER JOIN MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId 
	--			LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
	--			LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id  
	--			LEFT OUTER JOIN SupplementPermitInfo SPI ON p.Id = spi.PermitId
	--		WHERE year(cc.ComplianceReportingEndDate) = @Year 
	--		GROUP BY p.Id, pwg.AvgGalPerDay, psw.AvgGalPerDay,cc.AnnualAverageOverusePercentage,  cc.AnnualAverageUsage, 
	--				 cc.PermitAnnualAverageUsage, spi.SUPPLEMENTALGROUPID, spi.SupplementalPermitNames, SupplementalType ) T
	--	INNER JOIN MdeWaterPermitting..Permit p on p.Id = t.PermitId   
	--WHERE AnnualAverageOverusePercentage > @AnnualAverageOverusePercentage ) A
	--WHERE TotalPermitAllocation > @AnnualAverageAllocation
	--ORDER BY PermitNumber 

	SELECT PermitNumber, PermitName, PermitteeName, TypeOfUse, SupplementalPermits, SupplementalType, ListedPermitAllocation, 
				TotalPermitAllocation, PermitAnnualAverageUsage ListedPermitAnnualAvgUse, AvgGalPerDayUsage TotalAnnualAvgUse,
				CAST(AnnualAverageOverusePercentage AS VARCHAR(50)) + '%' AnnualAverageOverusePercentage
	FROM
		(
		 SELECT *,
				CASE WHEN (TotalPermitAllocation IS NULL OR TotalPermitAllocation = 0) THEN 0
				ELSE ROUND(((AvgGalPerDayUsage/TotalPermitAllocation)-1)*100,0) END AnnualAverageOverusePercentage
		 FROM
			(SELECT PermitNumber, p.PermitName, dbo.GetPermitteeName(p.Id) PermitteeName,[dbo].[GetTopTypeOfUse](p.Id) TypeOfUse, 
					ISNULL(t.SupplementalPermitNames,'') SupplementalPermits, SupplementalType, t.AvgGalPerDay ListedPermitAllocation, 
					CASE WHEN t.SUPPLEMENTALGROUPID IS NULL THEN t.AvgGalPerDay 
					ELSE SUM(t.AvgGalPerDay) OVER (PARTITION BY t.SUPPLEMENTALGROUPID) END TotalPermitAllocation, 
					ROUND(t.PermitAnnualAverageUsage,0) PermitAnnualAverageUsage, 
					CASE WHEN t.SUPPLEMENTALGROUPID IS NULL THEN ROUND(t.AnnualAverageUsage,0) 
					ELSE SUM(t.AnnualAverageUsage) OVER (PARTITION BY t.SUPPLEMENTALGROUPID) END AvgGalPerDayUsage,
					t.SUPPLEMENTALGROUPID
				FROM	
					(SELECT p.Id PermitId, cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay, 
							cc.AnnualAverageOverusePercentage , cc.AnnualAverageUsage, spi.SUPPLEMENTALGROUPID , cc.PermitAnnualAverageUsage,
							ISNULL(spi.SupplementalPermitNames, '') SupplementalPermitNames, ISNULL(spi.SupplementalType, '') SupplementalType
						FROM	MdeWaterPermitting..Permit p   
						INNER JOIN MdeWaterPermitting..PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1 
						INNER JOIN MdeWaterPermitting..ConditionCompliance cc on pc.id = cc.PermitConditionId 
						LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
						LEFT OUTER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id  
						LEFT OUTER JOIN SupplementPermitInfo SPI ON p.Id = spi.PermitId
					 WHERE year(cc.ComplianceReportingEndDate) = @Year
					 GROUP BY p.Id, pwg.AvgGalPerDay, psw.AvgGalPerDay,cc.AnnualAverageOverusePercentage,  cc.AnnualAverageUsage, 
								cc.PermitAnnualAverageUsage, spi.SUPPLEMENTALGROUPID, spi.SupplementalPermitNames, SupplementalType 
					) T
			 INNER JOIN MdeWaterPermitting..Permit p on p.Id = t.PermitId
		) A
	) B
	WHERE AnnualAverageOverusePercentage > @AnnualAverageOverusePercentage AND TotalPermitAllocation > @AnnualAverageAllocation
	ORDER BY PermitNumber 

END

GO


