USE [MdeReports]
GO

/****** Object:  StoredProcedure [dbo].[PumpageReportSummaryByPermit]    Script Date: 5/8/2017 10:37:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER PROCEDURE [dbo].[PumpageReportSummaryByPermit]
(@PermitName varchar(24),
@BeginYear int = null,
@EndYear int = null)
AS
BEGIN

	-- exec [PumpageReportSummaryByPermit] 'DO1992G057(01)'
	-- exec [PumpageReportSummaryByPermit] 'CO1990G024'
	-- exec [PumpageReportSummaryByPermit] 'CO1990G024', 1999, 2015
	-- exec [PumpageReportSummaryByPermit] 'AA2005G020(01)'
	-- exec [PumpageReportSummaryByPermit] 'CE1977S027'
	
DECLARE @revisionNumber varchar(20)
DECLARE @openParenIndex int = CHARINDEX('(', @PermitName)
DECLARE @closeParenIndex int = CHARINDEX(')', @PermitName)
DECLARE @slashIndex int = CHARINDEX('/', @PermitName)
DECLARE @permitNameLen int = LEN(@PermitName)
SET @revisionNumber = CASE 
						WHEN (@openParenIndex > 0) THEN SUBSTRING(@PermitName, @openParenIndex + 1, @closeParenIndex - @openParenIndex - 1)
						WHEN (@slashIndex > 0) THEN SUBSTRING(@PermitName, @slashIndex + 1, @permitNameLen)
						ELSE NULL
					  END

	SELECT   PermitNumber
		   , ReportYear
		   , @revisionNumber RevisionNumber
		   --, MAX(ISNULL(HighMonth,0)) OVER (PARTITION BY PERMITNUMBER,REPORTYEAR) HighMonth
		   , (SELECT TOP 1 HighMonth 
				  FROM MdeWaterPermitting..ConditionCompliance 
				  WHERE HighMonthAvgGalPerDay = MAX(cc.HighMonthAvgGalPerDay)) HighMonth
		   --, MAX(ISNULL(HighMonthAvgGalPerDay,0)) OVER (PARTITION BY PERMITNUMBER,REPORTYEAR) HighMonthAvgGalPerDay
		   , MAX(cc.HighMonthAvgGalPerDay) HighMonthAvgGalPerDay
		   , MAX(ISNULL(A.AnnualAverageOverusePercentage,0)) AnnualAverageOverusePercentage
		   --, MAX(ISNULL(AnnualAverageUsage,0)) OVER (PARTITION BY PERMITNUMBER,REPORTYEAR) AnnualAverageUsage
		   , CASE 
				 -- count == 1 but report only covers half of year
				 WHEN (COUNT(DISTINCT A.PermitConditionId) > 1 OR (MONTH(MIN(ComplianceReportingStartDate)) != 1 OR MONTH(MAX(ComplianceReportingEndDate)) != 12)) 
					THEN SUM(AnnualTotalUsage) / (DATEDIFF(DAY, DATEFROMPARTS(ReportYear, 1, 1), DATEFROMPARTS(ReportYear, 12, 31)) + 1)
		         ELSE MAX(AnnualAverageUsage)
			 END AnnualAverageUsage
		   , MAX(ISNULL(A.MonthlyMaximumOverusePercentage,0)) MonthlyMaximumOverusePercentage
		   --, MAX(ISNULL(AnnualTotalUsage,0)) OVER (PARTITION BY PERMITNUMBER,REPORTYEAR) AnnualTotalUsage									2015 calculated values are off for some reason
		   , CASE 
				 WHEN (COUNT(DISTINCT A.PermitConditionId) > 1) 
					THEN  SUM(AnnualTotalUsage) 
				 ELSE MAX(AnnualTotalUsage) 
			 END AnnualTotalUsage
		   , MAX(Month1) Month1
		   , MAX(Month2) Month2
		   , MAX(Month3) Month3
		   , MAX(Month4) Month4
		   , MAX(Month5) Month5
		   , MAX(Month6) Month6
		   , MAX(Month7) Month7
		   , MAX(Month8) Month8
		   , MAX(Month9) Month9
		   , MAX(Month10) Month10
		   , MAX(Month11) Month11
		   , MAX(Month12) Month12
	FROM (
		  SELECT   PermitNumber
			     , ReportYear
				 , ConditionReportingPeriodId
			     , PermitConditionId
			     --, HighMonth
			     --, HighMonthAvgGalPerDay2
			     , case 
					 when [1] is null and [7] is null then null 
					 else AnnualAverageOverusePercentage 
				   end AnnualAverageOverusePercentage
			     --, case when [1] is null and [7] is null 
				 --	  then null else AnnualAverageUsage end AnnualAverageUsage
			     , case 
					 when [1] is null and [7] is null 
					 then null else MonthlyMaximumOverusePercentage 
				   end MonthlyMaximumOverusePercentage
			     --, AnnualTotalUsage
			     , [1] Month1, [2] Month2, [3] Month3, [4] Month4, [5] Month5, [6] Month6, [7] Month7, [8] Month8, [9] Month9, [10] Month10, [11] Month11, [12] Month12
		  FROM (
				SELECT   p.PermitNumber
					   , pc.ConditionReportingPeriodId
					   , cc.PermitConditionId
					   , MonthId
					   , Gallons
					   , YEAR(cc.ComplianceReportingEndDate) ReportYear
					   , MAX(cc.AnnualAverageOverusePercentage) AnnualAverageOverusePercentage
					   --, max(cc.AnnualTotalUsage) AnnualTotalUsage								-- not calculating correctly when 2 revisions in same year
					   --, SUM(Gallons) OVER (PARTITION BY p.PermitNumber, YEAR(cc.ComplianceReportingEndDate)) AnnualTotalUsage
					   --, max(cc.AnnualAverageUsage) AnnualAverageUsage
					   , MAX(cc.MonthlyMaximumOverusePercentage) MonthlyMaximumOverusePercentage
					   --, max(cc.HighMonth) HighMonth												-- can't select the max month number, doesn't always match max avg
					   --, (SELECT TOP 1 HighMonth 
					   --	   FROM MdeWaterPermitting..ConditionCompliance 
					   --	   WHERE HighMonthAvgGalPerDay = MAX(cc.HighMonthAvgGalPerDay)) HighMonth
					   --, MAX(cc.HighMonthAvgGalPerDay) HighMonthAvgGalPerDay
			    FROM  MdeWaterPermitting..Permit p
					  INNER JOIN MdeWaterPermitting..PermitCondition pc ON pc.PermitId = p.Id AND StandardConditionTypeId = 14 AND pc.RequiresSelfReporting = 1
					  INNER JOIN MdeWaterPermitting..ConditionCompliance cc 
						  ON      pc.id = cc.PermitConditionId -- AND cc.PermitConditionComplianceStatusId = 5		do not only show submitted years
							  AND ISNULL(@BeginYear, YEAR(cc.ComplianceReportingEndDate)) <= YEAR(cc.ComplianceReportingEndDate)
							  AND ISNULL(@EndYear, YEAR(cc.ComplianceReportingEndDate)) >= YEAR(cc.ComplianceReportingEndDate)
							  AND ISNULL(@EndYear, YEAR(GETDATE()) - 1) >= YEAR(cc.ComplianceReportingDueDate) - 1
					  LEFT JOIN MdeWaterPermitting..PumpageReport pr ON pr.ConditionComplianceId = cc.Id AND pr.Active = 1
					  LEFT JOIN MdeWaterPermitting..PumpageReportDetail prd ON prd.PumpageReportId = pr.Id
			    WHERE	  p.PermitName = CASE WHEN CHARINDEX('(', @PermitName) > 0 OR CHARINDEX('/', @PermitName) > 0 THEN @PermitName ELSE p.PermitName END
					  AND p.PermitNumber = CASE WHEN CHARINDEX('(', @PermitName) = 0 AND CHARINDEX('/', @PermitName) = 0 THEN @PermitName ELSE p.PermitNumber END
			    GROUP BY p.Id, p.PermitNumber, pc.ConditionReportingPeriodId, cc.PermitConditionId, MonthId, Gallons, YEAR(cc.ComplianceReportingEndDate)
			   ) AS SourceTable
		       PIVOT
		       (
			       SUM(Gallons)
			       FOR MonthId IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
		       ) AS pvt 
		 ) A
	INNER JOIN MdeWaterPermitting..ConditionCompliance cc ON cc.PermitConditionId = A.PermitConditionId  --AND cc.PermitConditionComplianceStatusId = 5 
				AND YEAR(ComplianceReportingEndDate) = ReportYear
	GROUP BY PermitNumber, ReportYear, ConditionReportingPeriodId
	ORDER BY ReportYear

END


GO


