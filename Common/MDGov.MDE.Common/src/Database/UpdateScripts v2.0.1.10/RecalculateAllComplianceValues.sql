/****** Script for SelectTopNRows command from SSMS  ******/
DECLARE @currYear int = 1992 --(SELECT MIN(YEAR(ComplianceReportingEndDate)) FROM [MdeWaterPermitting].[dbo].CONDITIONCOMPLIANCE)
DECLARE @tablePermits TABLE (Id int IDENTITY(1,1), PermitId int)
DECLARE @maxId int
DECLARE @currId int
DECLARE @currPermitId int

WHILE @currYear < 1993
BEGIN

  print @currYear

  INSERT INTO @tablePermits
    SELECT distinct PermitId
	  FROM  [MdeWaterPermitting].[dbo].[Permit] p
	        INNER JOIN [MdeWaterPermitting].[dbo].PERMITCONDITION pc ON pc.PERMITID = p.ID AND STANDARDCONDITIONTYPEID = 14
	        INNER JOIN [MdeWaterPermitting].[dbo].CONDITIONCOMPLIANCE cc ON pc.ID = cc.PERMITCONDITIONID AND YEAR(COMPLIANCEREPORTINGENDDATE) = @currYear
			INNER JOIN [MdeWaterPermitting].[dbo].PumpageReport pr ON pr.ConditionComplianceId = cc.Id
	  WHERE pr.id is not null

  SET @maxId = (SELECT MAX(Id) FROM @tablePermits)

  SET @currId = 1
  WHILE @currId <= @maxId
  BEGIN
    SELECT @currPermitId = PermitId FROM @tablePermits WHERE Id = @currId
    EXEC [MdeWaterPermitting].[dbo].[sp_CalculateConditionComplianceValues] @permitId=@currPermitId, @year=@currYear

    SET @currId = @currId + 1
  END

  DELETE FROM @tablePermits
  SET @currYear = @currYear + 1
END
