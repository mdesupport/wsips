-- Move CC ids for duplicate records to correct report ids (only 1, so doing by hand)
UPDATE [MdeWaterPermitting].[dbo].[PumpageReport]
	SET ConditionComplianceId = 681068
	WHERE [Id] = 1104

DELETE FROM [MdeWaterPermitting].[dbo].[ConditionCompliance]
	WHERE [Id] = 619495

-- Delete duplicate CC records
DROP TABLE #temp

SELECT    row_number() 
			OVER (PARTITION BY PermitConditionId, ComplianceReportingStartDate, ComplianceReportingEndDate 
					ORDER BY PermitConditionId, ComplianceReportingStartDate, prId DESC) rowNumber
		, *
	INTO #temp
	FROM	(SELECT   pr.Id prId
					, cc.* 
				FROM [MdeWaterPermitting].[dbo].[ConditionCompliance] AS cc
					LEFT JOIN PumpageReport pr ON pr.ConditionComplianceId = cc.Id
				WHERE EXISTS	(SELECT [Id] 
									FROM [MdeWaterPermitting].[dbo].[ConditionCompliance] cc2 
									WHERE       cc2.[Id] != cc.[Id] 
											AND cc2.[PermitConditionId] = cc.[PermitConditionId]
											AND cc2.[ComplianceReportingStartDate] = cc.[ComplianceReportingStartDate]
											AND cc2.[ComplianceReportingEndDate] = cc.[ComplianceReportingEndDate]
								)
			) a

DELETE FROM [MdeWaterPermitting].[dbo].[ConditionCompliance]
	WHERE [Id] IN (SELECT [Id] FROM #temp WHERE [rowNumber] > 1)

-- Find all CC records where dates are incorrect
DROP TABLE #temp

SELECT    pr.Id PumpageReportId
		, cc.Id ConditionComplianceId 
		, cc.PermitConditionId
		, pc.ConditionReportingPeriodId
		, cc.ComplianceReportingStartDate
		, cc.ComplianceReportingEndDate
		, cc2.Id ValidConditionComplianceId
		, cc2.ComplianceReportingStartDate ValidStartDate
		, cc2.ComplianceReportingEndDate ValidEndDate
	INTO #temp
	FROM ConditionCompliance cc
	JOIN PermitCondition pc ON pc.Id = cc.PermitConditionId AND pc.ConditionReportingPeriodId != 5
	LEFT JOIN PumpageReport pr ON pr.ConditionComplianceId = cc.Id --and pr.Active = 1
	LEFT JOIN ConditionCompliance cc2 ON cc2.PermitConditionId = cc.PermitConditionId and cc2.Id != cc.Id AND cc2.ComplianceReportingEndDate = cc.ComplianceReportingEndDate
	WHERE       NOT (MONTH(cc.ComplianceReportingStartDate) = 1 AND DAY(cc.ComplianceReportingStartDate) = 1) 
			AND NOT (MONTH(cc.ComplianceReportingStartDate) = 7 AND DAY(cc.ComplianceReportingStartDate) = 1)
			OR cc.ComplianceReportingEndDate < cc.ComplianceReportingStartDate

-- Move Valid CC ids to reports
UPDATE pr
	SET ConditionComplianceId = (SELECT ValidConditionComplianceId FROM #temp WHERE PumpageReportId = pr.Id)
	FROM PumpageReport pr
	WHERE Id IN (SELECT PumpageReportId FROM #temp WHERE ValidConditionComplianceId IS NOT NULL)

-- Delete duplicate CC records
DELETE FROM ConditionCompliance
	WHERE Id IN (SELECT ConditionComplianceId FROM #temp WHERE ValidConditionComplianceId IS NOT NULL)

-- Correct remaining CC dates
UPDATE ConditionCompliance
	SET ComplianceReportingStartDate =  CASE
											WHEN MONTH(ComplianceReportingEndDate) = 6 THEN DATEFROMPARTS(YEAR(ComplianceReportingEndDate), 1,1)
											ELSE DATEFROMPARTS(YEAR(ComplianceReportingEndDate), 7, 1)
										END
	WHERE Id IN (SELECT ConditionComplianceId FROM #temp t
					WHERE ConditionReportingPeriodId = 3 AND ValidConditionComplianceId IS NULL)

UPDATE ConditionCompliance
	SET ComplianceReportingStartDate =  DATEFROMPARTS(YEAR(ComplianceReportingEndDate), 1,1)
	WHERE Id IN (SELECT ConditionComplianceId FROM #temp t
					WHERE ConditionReportingPeriodId = 1 AND ValidConditionComplianceId IS NULL)

/* what about periodid = 5 (one time) - none exist in this case */

-- Set all but the last report to inactive
WITH cte AS
(
	SELECT    row_number() OVER (PARTITION BY ConditionComplianceId ORDER BY ReceivedDate) rowNumber
			, *
		FROM PumpageReport
)
UPDATE cte
    SET Active = 0, LastModifiedBy = 'CantonGroup', LastModifiedDate = CURRENT_TIMESTAMP
	WHERE rowNumber > 1



