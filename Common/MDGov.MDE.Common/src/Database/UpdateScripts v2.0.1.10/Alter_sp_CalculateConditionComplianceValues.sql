USE [MdeWaterPermitting]
GO

/****** Object:  StoredProcedure [dbo].[sp_CalculateConditionComplianceValues]    Script Date: 3/24/2017 2:02:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[sp_CalculateConditionComplianceValues]
(@permitId int, @year int)
AS
BEGIN

       --exec dbo.sp_CalculateConditionComplianceValues 2628, 2012 -- no supplemental group
       --exec dbo.sp_CalculateConditionComplianceValues 6408, 2012 -- supplemental group 6408 
       --exec dbo.sp_CalculateConditionComplianceValues 52777, 2013 -- supplemental group 6408 
       --exec dbo.sp_CalculateConditionComplianceValues 60900, 2012 

       -- full year one permit
       --declare @permitId int, @year int
       --set @permitid = 789
       --set @year = 1997

       -- 1/2 year (beginning) one permit
       --declare @permitId int, @year int
       --set @permitid = 60879
       --set @year = 2014

        -- get all of the condition compliance records  
       SELECT	p.ID PERMITID , CC.ID ConditionComplianceId, PR.ID PumpageReportId , PRD.ID , prd.MonthId, 
				cast(isnull(prd.Gallons,0) as float) Gallons, cc.COMPLIANCEREPORTINGSTARTDATE, cc.COMPLIANCEREPORTINGENDDATE , 
				cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay,
				cast(isnull(pwg.MaxGalPerDay,0) + isnull(psw.MaxGalPerDay,0) as float) MaxGalPerDay
	   INTO #TEMP
	   FROM PERMIT P --INNER JOIN (	SELECT PSG1.PERMITID										-- Supplemental permits should not be included in Pumpage Report Totals
									--FROM   MdeWaterPermitting.dbo.PERMITSUPPLEMENTALGROUP PSG1
									--WHERE  EXISTS (  SELECT SUPPLEMENTALGROUPID FROM  MdeWaterPermitting.dbo.PERMITSUPPLEMENTALGROUP PSG2
									--				WHERE  PSG1.SUPPLEMENTALGROUPID = PSG2.SUPPLEMENTALGROUPID AND PERMITID = @PERMITID )
									--AND SUPPLEMENTALTYPEID IN (2,3) /* USE BOTH Supplemental and Combined, Supplemental Special */
									--UNION  
									--SELECT @PERMITID ) PERMITS ON P.ID = PERMITS.PERMITID
					 INNER JOIN PERMITCONDITION PC ON PC.PERMITID = P.ID AND STANDARDCONDITIONTYPEID = 14
                     INNER JOIN CONDITIONCOMPLIANCE CC ON PC.ID = CC.PERMITCONDITIONID AND YEAR(COMPLIANCEREPORTINGENDDATE) = @year 
					 LEFT OUTER JOIN PUMPAGEREPORT PR ON PR.CONDITIONCOMPLIANCEID = CC.ID AND PR.ACTIVE = 1  
					 LEFT OUTER JOIN PUMPAGEREPORTDETAIL PRD ON PRD.PUMPAGEREPORTID = PR.ID
					 LEFT OUTER JOIN PERMITWITHDRAWALGROUNDWATER PWG ON PWG.PERMITID = P.ID
					 LEFT OUTER JOIN PERMITWITHDRAWALSURFACEWATER PSW ON PSW.PERMITID = P.ID 
	   WHERE p.Id = @permitId 
	   ORDER BY p.ID , CC.ID , PR.ID , PRD.ID

	   -- SELECT * FROM #TEMP ORDER BY PERMITID , ConditionComplianceId ,PumpageReportId , ID ,  MonthId;
	    

                     -- first get the total number of gallons used for the specified permit
                     --DECLARE @totalUsageSingle float
                     --select @totalUsageSingle = isnull(sum(gallons),0) from #temp where permitid = @permitid

                     DECLARE @HighMonthSingle int
                     DECLARE @HighMonthAvgGalPerDaySingle float
                     select  @HighMonthAvgGalPerDaySingle = MonthAvgGalPerDay , @HighMonthSingle = MonthId
                     from   ( SELECT TOP 1 Gallons/DaysInMonth MonthAvgGalPerDay, MonthId
							  FROM	(     select MonthId, sum(gallons) Gallons, dbo.[DaysInMonth] (MonthId, @year) DaysInMonth
										  from   #temp
										  where  gallons > 0
										  and permitid = @permitId
										  group by MonthId  ) t1
							  ORDER BY MonthAvgGalPerDay DESC ) t2
				 
						-- SELECT '@HighMonthAvgGalPerDaySingle', @HighMonthAvgGalPerDaySingle, '@HighMonthSingle', @HighMonthSingle


						-- SELECT  t.ReportTotalGallons/ ( DATEDIFF(DAY, ReportStartDate, ReportEndDate) + 1 ) ReportAverageUsage,
						--		 ( DATEDIFF(DAY, ReportStartDate, ReportEndDate) + 1 ) ReportDuration,
                        --         t.ReportTotalGallons ReportTotalUsage,
                        --          t.AnnualTotalGallons AnnualTotalUsage,
                        --         t.AnnualTotalGallons/( DATEDIFF(DAY, AnnualStartDate, AnnualEndDate) + 1 ) AnnualAverageUsage,
						--		 ( DATEDIFF(DAY, AnnualStartDate, AnnualEndDate) + 1 ) AnnualDuration,
                        --          @HighMonthSingle HighMonth,
                        --        @HighMonthAvgGalPerDaySingle HighMonthAvgGalPerDay 
                        --  FROM   ConditionCompliance cc
                        --  INNER JOIN		   (SELECT  DISTINCT t.permitid, cc.Id ConditionComplianceId, pc.ConditionReportingPeriodId,
						--								 isnull(sum(gallons) OVER (PARTITION BY cc.Id) ,0) ReportTotalGallons,
						--								 isnull(sum(gallons) OVER () ,0) AnnualTotalGallons,
						--								 MIN(t.COMPLIANCEREPORTINGSTARTDATE) OVER (PARTITION BY cc.Id) ReportStartDate,
						--								 MAX(t.COMPLIANCEREPORTINGENDDATE) OVER (PARTITION BY cc.Id) ReportEndDate,   
						--								 MIN(t.COMPLIANCEREPORTINGSTARTDATE) OVER () AnnualStartDate,
						--								 MAX(t.COMPLIANCEREPORTINGENDDATE) OVER () AnnualEndDate  
                        --                        FROM   ConditionCompliance cc
                        --                                INNER JOIN PermitCondition pc on pc.Id = cc.PermitConditionId 
                        --                               INNER JOIN #temp t on t.ConditionComplianceId = cc.Id 
                        --                        WHERE  cc.Id in (select DISTINCT ConditionComplianceId FROM #temp )) t 
						--on t.ConditionComplianceId = cc.Id and t.permitId = @permitid;

                     -- update the compliance records
                     UPDATE     ConditionCompliance
                     SET		ReportAverageUsage =  t.ReportTotalGallons/ ( DATEDIFF(DAY, ReportStartDate, ReportEndDate) + 1 ) ,
                                ReportTotalUsage = t.ReportTotalGallons,
                                AnnualTotalUsage = t.AnnualTotalGallons,
                                AnnualAverageUsage = t.AnnualTotalGallons/( DATEDIFF(DAY, DATEFROMPARTS(YEAR(AnnualStartDate),1,1), DATEFROMPARTS(YEAR(AnnualEndDate),12,31)) + 1 ),
								PermitAnnualAverageUsage = t.PermitAnnualTotalGallons/( DATEDIFF(DAY, DATEFROMPARTS(YEAR(AnnualStartDate),1,1), DATEFROMPARTS(YEAR(AnnualEndDate),12,31)) + 1 ),
                                HighMonth = @HighMonthSingle,
                                HighMonthAvgGalPerDay = @HighMonthAvgGalPerDaySingle, 
                                LastModifiedBy = 'sp_CalculateConditionComplianceValues',
                                LastModifiedDate = GETDATE()
                        FROM   ConditionCompliance cc
                           INNER JOIN		   (SELECT  DISTINCT t.PermitId, cc.Id ConditionComplianceId, pc.ConditionReportingPeriodId,
														 isnull(sum(gallons) OVER (PARTITION BY cc.Id) ,0) ReportTotalGallons,
														 isnull(sum(gallons) OVER () ,0) AnnualTotalGallons,
														 isnull(sum(gallons) OVER (PARTITION BY t.PermitId) ,0) PermitAnnualTotalGallons,
														 MIN(t.COMPLIANCEREPORTINGSTARTDATE) OVER (PARTITION BY cc.Id) ReportStartDate,
														 MAX(t.COMPLIANCEREPORTINGENDDATE) OVER (PARTITION BY cc.Id) ReportEndDate,   
														 MIN(t.COMPLIANCEREPORTINGSTARTDATE) OVER (PARTITION BY t.PermitId) AnnualStartDate,
														 MAX(t.COMPLIANCEREPORTINGENDDATE) OVER (PARTITION BY t.PermitId) AnnualEndDate  
                                                FROM   ConditionCompliance cc
                                                       INNER JOIN PermitCondition pc on pc.Id = cc.PermitConditionId 
                                                       INNER JOIN #temp t on t.ConditionComplianceId = cc.Id 
                                                WHERE  cc.Id in (select DISTINCT ConditionComplianceId FROM #temp )) t 
							on t.ConditionComplianceId = cc.Id and t.PermitId = @permitId;

                     --select      cc.*
                     --from Permit p
                     --     inner join PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14
                     --     inner join ConditionCompliance cc on pc.id = cc.PermitConditionId and year(compliancereportingenddate) = 2012
                     --where       p.Id = 2628
					  
    /* *********************************************************** */
       -- check to see if every compliance condition for the time period has a pumpage report turned in
       if (select count(*) from #temp where PumpageReportId is null) = 0
       BEGIN 
                     -- now get the total number of gallons used across all permits
                     DECLARE @totalUsage float
                     select @totalUsage = sum(gallons) from #temp

                     -- the number of days needs to be 365/366 if there is one annual or 2 semi annual reports per permit
                     -- or 183/182 Days for Jan-June and 184 Days for Jul-Dec is only one semi-annual report period is reported
					 -- 3/24 - Annual avgs are annual avgs, always divide by 365/366
                     DECLARE @numberOfDays float
                     select @numberOfDays = DATEDIFF(DAY, AnnualStartDate, AnnualEndDate) + 1
					 from (select DATEFROMPARTS(YEAR(MIN(ComplianceReportingStartDate) OVER (PARTITION BY PERMITID)),1,1) AnnualStartDate,
								  DATEFROMPARTS(YEAR(MAX(ComplianceReportingEndDate) OVER (PARTITION BY PERMITID)),12,31) AnnualEndDate
							from #temp where permitid = @permitid) t;
              

                     -- get the average gallons per day allowed for all permits in the group
                     DECLARE @avgGallonsPerDayAllowed float
                     select @avgGallonsPerDayAllowed = sum(Total)
                     from   (select       permitid, max(AvgGalPerDay) Total
                                         from   #temp
                                         group by permitid) t

                     -- calculate annual Overuse
                     DECLARE @annualOveruse float
                     select @annualOveruse = ROUND(dbo.MathMax(0, CASE WHEN @avgGallonsPerDayAllowed = 0 THEN (@totalUsage/@numberOfDays) ELSE (((@totalUsage/@numberOfDays) / @avgGallonsPerDayAllowed) - 1) END) * 100, 3)
                     --select    @totalUsage, @numberOfDays, @avgGallonsPerDayAllowed, @annualOveruse  
							 
                     -- get the max gallons per day allowed
                     DECLARE @maxGallonsPerDayAllowed float
                     select @maxGallonsPerDayAllowed = sum(Total)
                     from   (select       permitid, max(MaxGalPerDay) Total
                                         from   #temp
                                         group by permitid) t

                     -- get the highest month and highest monthly average
                     DECLARE @HighMonth int
                     DECLARE @HighMonthAvgGalPerDay float 
                     select  @HighMonthAvgGalPerDay = MonthAvgGalPerDay , @HighMonth = MonthId
                     from   ( SELECT TOP 1 Gallons/DaysInMonth MonthAvgGalPerDay, MonthId
							  FROM	(     select MonthId, sum(gallons) Gallons, dbo.[DaysInMonth] (MonthId, @year) DaysInMonth
										  from   #temp
										  where  gallons > 0
										  and permitid = @permitid
										  group by MonthId  ) t1
							  ORDER BY MonthAvgGalPerDay DESC ) t2

                     -- calculate monthly overuse
                     DECLARE @monthlyOveruse float
                     select TOP 1 @monthlyOveruse = ROUND(dbo.MathMax(0, CASE WHEN @maxGallonsPerDayAllowed = 0 THEN @HighMonthAvgGalPerDay ELSE ((@HighMonthAvgGalPerDay / @maxGallonsPerDayAllowed) - 1) END) * 100, 3)

                     -- update all records with the compliance 
                     UPDATE ConditionCompliance
                     SET    AnnualAverageOverusePercentage = isnull(@annualOveruse,0),
                            MonthlyMaximumOverusePercentage = isnull(@monthlyOveruse,0), 
                            LastModifiedBy = 'sp_CalculateConditionComplianceValues',
                            LastModifiedDate = GETDATE()
                     WHERE  Id in (select DISTINCT ConditionComplianceId FROM #temp)
       
                     -- diagnostic
                     --select @totalUsage,@numberOfDays, @avgGallonsPerDayAllowed, @annualOveruse, @maxGallonsPerDayAllowed, @monthlyOveruse
                     -- select     @monthlyOveruse, @annualOveruse
                                  --select      *
                                  --from ConditionCompliance 
                                  --where       Id in (select ConditionComplianceId from #temp)
                     -- diagnostic
                                  --select      *
                                  --from #temp
                                  --where       gallons > 0
                                  --drop table #temp
       
       END
       ELSE
       BEGIN
                     -- update all records with the compliance 
                     UPDATE ConditionCompliance
                     SET    AnnualAverageOverusePercentage = 0,
                            MonthlyMaximumOverusePercentage = 0, 
                            LastModifiedBy = 'sp_CalculateConditionComplianceValues',
                            LastModifiedDate = GETDATE()
                     WHERE  Id in (select DISTINCT ConditionComplianceId FROM #temp)
       END 

END 


GO


