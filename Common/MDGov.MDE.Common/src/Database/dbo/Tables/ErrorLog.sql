﻿CREATE TABLE [dbo].[ErrorLog] (
    [Id] INT IDENTITY(1,1) NOT NULL,
	[Application] VARCHAR (255) NULL,
	[MessageType] VARCHAR (20) NULL,
    [ErrorTime] DATETIME2 DEFAULT (getdate()) NOT NULL,
	[Message] NVARCHAR (4000) NULL,
	[ExceptionMessage] NVARCHAR (4000) NULL,
	[ExceptionType] VARCHAR (100) NULL,
	[StackTrace] NVARCHAR (4000) NULL,
	[CreatedBy] VARCHAR (100) NOT NULL,
	[CreatedDate] DATETIME2 NOT NULL,
	[LastModifiedBy] VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2 NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);