﻿CREATE TABLE [dbo].[ActivityLog]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [UserName] VARCHAR(50) NOT NULL,
    [ContactId] INT NULL,
    [LoginDate] DATETIME2 NOT NULL,
    [LoginType] VARCHAR(10) NOT NULL,
	[CreatedDate] DATETIME2 NOT NULL,
    [CreatedBy] VARCHAR(50) NOT NULL,
    [LastModifiedDate] DATETIME2 NOT NULL,
    [LastModifiedBy] VARCHAR(50) NOT NULL
)
