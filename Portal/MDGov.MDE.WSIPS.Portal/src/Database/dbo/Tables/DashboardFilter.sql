﻿CREATE TABLE [dbo].[DashboardFilter]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserId] INT NOT NULL, 
    [FilterString] VARCHAR(MAX) NOT NULL,
	[CreatedDate] DATETIME2 NOT NULL,
    [CreatedBy] VARCHAR(50) NOT NULL,
    [LastModifiedDate] DATETIME2 NOT NULL,
    [LastModifiedBy] VARCHAR(50) NOT NULL
)
