﻿$.extend($.jgrid.defaults, {
    altRows: true,
    datatype: "json",
    jsonReader: {
        root: "Data",
        page: "CurrentPage",
        total: "PageTotal",
        records: "RecordCount",
        repeatitems: false
    },
    pager: $('#pager'),
    viewrecords: true
});