﻿/// <reference path="../jquery-1.10.1.js" />
$(function () {
    $(document).on('blur', 'input[id$=ZipCode]', function () {
        var self = this,
            zipCodeValue = $(self).val(),
            autoPopulateAction = $(self).attr('data-autopopulateurl'),
            autoPopulateFieldsContainer = $(self).closest('.autoPopulateFields'),
            cityInput = autoPopulateFieldsContainer.find('input[id$=City]'),
            stateInput = autoPopulateFieldsContainer.find('select[id$=StateId]');

        if (zipCodeValue.length) {
            $.ajax({
                url: autoPopulateAction,
                type: 'GET',
                cache: false,
                data: { zipCode: $(self).val() },
                success: function (result) {
                    if (result) {
                        cityInput.val(result.City);
                        stateInput.val(result.StateId).change();
                    }
                }
            });
        }
    });
});
