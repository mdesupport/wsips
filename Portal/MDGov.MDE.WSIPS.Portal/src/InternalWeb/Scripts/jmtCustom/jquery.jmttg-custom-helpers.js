﻿/// <reference path="jquery.jmttg-custom-bindings.js" />
/// <reference path="../jquery-1.10.1.js" />

/* JS for WSIPS Helpers */
(function ($) {

    /* BASE HELPER */
    var baseInputHelper = function () {
        var className = $(this).prop("class");
        switch (className) {
            case "yesNo":
                var obj = new yesNoHelper();
                obj.init($(this));
                break;
            default:
                //alert("missed case in baseInputHelper switch case : " + className);
                var obj = new checkboxHelper();
                obj.init($(this));
                break;
        }
    }

    /* YES/NO HELPER */
    var yesNoHelper = function () {
        var $input;     //orig checkbox
        var $yesSpan;   //Yes span & text
        var $noSpan;    //no psan & text
        var $yesButton; //div around yes span
        var $noButton;  //div around no span
        var $yesNoOuter;    //div around both yes & no buttons
        var $textSpan;  //text next to the on/off buttons
        var $activeButton;  //currently active on/off button

        this.init = function ($this) {
            if (!$this.data().isInitialized) {
                //make sure it's a checkbox
                if ($this.is("input[type=checkbox]")) {
                    //hide original and set global var value
                    $input = $this.hide();
                    //input elements
                    appendGUIElements();
                    //bind overs
                    yesNoOver();
                    //bind clicks
                    yesNoBindClick();
                    // Bind to the change event on the input.  This provides the ability to
                    // programmatically change the value and still affect the style of the yes/no button
                    $input.change(function () {
                        //set styles
                        setActiveButtonClass();
                        //set disabled status, if necessary
                        setDisabledStatus();
                    });
                    // Flip our initialized bit
                    $input.data().isInitialized = true;
                }
                else {
                    alert("You must call this with a checkbox target");
                }
            }
        }

        var appendGUIElements = function () {
            //create html
            $yesSpan = $("<span></span>").html("Yes");
            $noSpan = $("<span></span>").html("No");
            $yesButton = $("<div></div>").addClass("ynYesGreen ynBtnwidth helper-tl-corner helper-bl-corner").prop("title", "Yes?").append($yesSpan);
            $noButton = $("<div></div>").addClass("ynNoRed ynBtnwidth helper-tr-corner helper-br-corner").prop("title", "No?").append($noSpan);
            $yesNoOuter = $("<div></div>").addClass("ynButtonsOuter").append($yesButton, $noButton);

            //set button on/off state and disabled status
            setActiveButtonClass();
            setDisabledStatus();

            //put it all together
            $("<div></div>").addClass("controlOuterDiv").prop("id", $input.prop("id") + "_yesNo").append($yesNoOuter).insertAfter($input);
        }

        var setActiveButtonClass = function () {
            if ($input.is(':checked')) {
                $activeButton = $yesButton.removeClass("ynYesGreenOff");
                $noButton.addClass("ynNoRedOff");
            }
            else {
                $activeButton = $noButton.removeClass("ynNoRedOff");
                $yesButton.addClass("ynYesGreenOff");
            }
        }

        var setDisabledStatus = function () {
            if ($input.is(':disabled')) {
                $yesNoOuter.addClass('disabled');
            }
            else {
                $yesNoOuter.removeClass('disabled');
            }
        }

        var yesNoOver = function () {
            //set overs for yes/no buttons
            $.jmttgCustomBindings.bindOver($yesButton, "ynYesGreenOver");
            $.jmttgCustomBindings.bindOver($noButton, "ynNoRedOver");
        }

        var yesNoBindClick = function () {
            //remove any old bindings
            $yesButton.unbind("click");
            $noButton.unbind("click");
            //add new bindings
            $yesButton.bind("click", yesNoClick);
            $noButton.bind("click", yesNoClick);
        }

        var yesNoClick = function () {
            var $clickBtn = $(this);
            if ($input.is(':disabled') || $clickBtn.is($activeButton)) return;
            //set value on original input
            if ($clickBtn.is($yesButton)) {
                $input.prop("checked", true).change();
            }
            else {
                $input.prop("checked", false).change();
            }
        }
    }

    /* On click of home and search page, avoid showing warning message*/
    $('.mainNavLink').click(function () {
        window.onbeforeunload = null;
    });

    /* CHECKBOX HELPER */
    var checkboxHelper = function () {
        var $input;     //orig checkbox
        var $inputClass      //class for input, defines output format
        var $chk_outer;     //outer div around checkbox and text
        var $chk_checkbox;  //checkbox div with mbackground image
        var $disabled;      //div for disabled

        this.init = function ($this) {
            if ($('#' + $this.prop("id") + '_chkBx').length == 0) {
                //make sure it's a checkbox
                if ($this.is("input[type=checkbox]")) {
                    //hide original and set global var value
                    $input = $this.hide();
                    //input elements
                    appendGUIElements();
                    //bind clicks
                    bindClickEvents();
                    //set disabled status
                    setDisabledStatus();
                    // Bind to the change event on the input.  This provides the ability to
                    // programmatically change the value and still affect the style of the yes/no button
                    $input.change(function () {
                        //set styles
                        setCheckedStatus();
                        //set disabled status
                        setDisabledStatus();
                    });
                }
                else {
                    alert("You must call this with a checkbox target");
                }
            }
        }

        var bindClickEvents = function () {
            //remove old
            $chk_checkbox.unbind("click");
            //add click
            $chk_checkbox.bind("click", checkboxClick);
        }

        var checkboxClick = function () {
            //update original (hidden) checkbox
            if (!$input.is(':checked')) {
                //force change event!
                $input.prop("checked", true).change();
            }
            else {
                //force change event!
                $input.prop("checked", false).change();
            }
        }

        var appendGUIElements = function () {
            //internal elements
            $chk_checkbox = $("<div></div>").addClass("allCheckboxImage sprite checkbox");
            //set checked/un-checked state
            setCheckedStatus();
            //create outer div, insert after hidden input
            $chk_outer = $("<div></div>").addClass("checkboxParent").append($chk_checkbox).prop("id", $input.prop("id") + "_chkBx").prop("title", $input.prop("title")).insertAfter($input);
        }

        var setCheckedStatus = function () {
            if ($input.is(':checked')) {
                $chk_checkbox.addClass("checked");
            }
            else {
                $chk_checkbox.removeClass("checked");
            }
        }

        var setDisabledStatus = function () {
            if ($input.is(':disabled')) {
                //add disabled div
                $disabled = $("<div style='height:100%;width:100%;'></div>").addClass("sprite disabled");
                $chk_checkbox.html($disabled);
                //disable click events
                $chk_checkbox.unbind("click");
            }
            else {
                //remove disabled div
                $chk_checkbox.html("");
                //bind click events
                bindClickEvents();
            }
        }
    }

    $.fn.checkbox = function () {
        this.each(baseInputHelper);
    }

    $.requireScript = function (uri) {
        if (!$('script[src="' + uri + '"]').length) {
            $('<script></script>').attr('type', 'text/javascript').attr('src', uri).appendTo('body');
        }
    }
})(jQuery);