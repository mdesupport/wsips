﻿/// <reference path="jquery.jmttg-custom-browser.js" />
$.ajaxSetup({ cache: false });

//Main document ready for the application
$(document).ready(function () {
    onResizeEnd();

    var g_resizeTimer;

    //alert("main ready");
    $(window).unbind('resize');
    //bind resize event to window object
    $(window).bind('resize', function () {
        if (g_resizeTimer != null) { clearTimeout(g_resizeTimer) };
        g_resizeTimer = setTimeout(onResizeEnd, 100);
    });

    $('.scrollcontainer').scrollbars();

    BindUIControls();
    bindFormKeyPress();
});

function styleDialogInputs() {
    //style dialog controls
    $('.ui-dialog input[type=checkbox]').checkbox();
    $(".ui-dialog input[type=button]").button();
    $(".ui-dialog input[type=submit]").button();
    $(".ui-dialog button[type=button]").button();
    $(".ui-dialog button[type=submit]").button();
}

function bindFormKeyPress() {
    //remove any old bindings
    $("form").unbind("keypress");
    //add form keypress
    $("form").keypress(function (e) {
        var ButtonKeys = { EnterKey: 13 };
        if (e.which == ButtonKeys.EnterKey) {
            //get the default button id for the form attribute
            var defaultButtonId = $(this).attr("defaultbutton");
            //is there a default button attribute?
            if (defaultButtonId !== undefined) {
                //do we have that button in the page?
                if ($("#" + defaultButtonId) > 0) {
                    //click the button
                    $("#" + defaultButtonId).click();
                    //prevent any more bubbling
                    return false;
                }
            }
        }
    });
}

function BindUIControls() {
    //turn all chekboxes into custom ones
    $('input[type=checkbox]').checkbox();
    //handle buttons
    $("input[type=button]").button();
    $("input[type=submit]").button();
    $("button[type=button]").button();
    $("button[type=submit]").button();

    //handle selects
    $("select").not('[multiple="multiple"]').combobox();
    //handle radio buttons

    //change date inputs to jquery ui date pickers
    $('.date').datepicker({
        changeMonth: true,
        changeYear: true
    });

    $('input:not(.ui-combobox-input)').labelify({ labelledClass: "labelHighlight" });
    $('form').ajaxSend(function () {
        $(this).find("input:not(.ui-combobox-input)").labelify({ labelledClass: "labelHighlight" });
    });
}

//Resize, call jmt custom browser function
// could not call directly from function above
function onResizeEnd() {
    $.jmttgCustomBrowser.brwResized();
}