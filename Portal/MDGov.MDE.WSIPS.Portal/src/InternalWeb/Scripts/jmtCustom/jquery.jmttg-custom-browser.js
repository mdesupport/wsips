﻿/// <reference path="../jquery-1.8.2.js" />

jQuery.extend({
    jmttgCustomBrowser: {
        //what happens on resize
        brwResized: function () {
            //******************************************
            //set content div size
            //this function does not exist as of 8/15/12
            //setDivHeights();
            //******************************************
            if (window.localResize && typeof window.localResize == typeof Function) {
                // function exists, so we can now call it
                localResize();
            }
        },
        isIpad: function () {
            var rtnBool = false;
            if (navigator.userAgent.match(/iPad/i)) {
                rtnBool = true;
            }
            return rtnBool;
        },
        isAndroid: function () {
            var rtnBool = false;
            if (navigator.userAgent.match(/Android/)) {
                rtnBool = true;
            }
            return rtnBool;
        },
        //get browser type
        getBrowserName: function () {
            var browserName;
            if ($.browser.safari) {
                browserName = "sf";
            }
            if ($.browser.opera) {
                browserName = "op";
            }
            if ($.browser.msie) {
                browserName = "ie";
            }
            if ($.browser.mozilla) {
                browserName = "mz";
            }
            return browserName;
        },

        getBrowserVersion: function () {
            return jQuery.browser.version;
        },

        getBrowserVersionInteger: function () {
            return Math.floor(jQuery.browser.version);
        },


        isIE6: function () {
            if (browserName == "ie") {
                if (browserVersion == 6) {
                    return true;
                }
                return false;
            }
            return false;
        },

        //get the avaiable vertical space within browser window
        getAvailableHeight: function () {
            var avHeight;
            if (window.innerHeight) {
                avHeight = window.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                avHeight = document.documentElement.clientHeight;
            }
            else if (document.body) {
                avHeight = document.body.clientHeight;
            }
            return avHeight;
        },

        //get the avaiable horizontal space within browser window
        getAvailableWidth: function () {
            var avWidth;
            if (window.innerWidth) {
                avWidth = window.innerWidth;
            }
            else if (document.documentElement && document.documentElement.clientWidth) {
                avWidth = document.documentElement.clientWidth;
            }
            else if (document.body) {
                avWidth = document.body.clientWidth;
            }
            return avWidth;
        },

        //gets the page name + .aspx - UPPERCASE
        getPageName: function () {
            var URL = document.location.href;
            var lastSlash = eval(URL.lastIndexOf("/") + 1);
            var questPos = URL.indexOf("?")
            if (questPos == -1) {
                //no "?" in URL
                questPos = URL.length;
            }
            return URL.substring(lastSlash, questPos).toUpperCase();
        },
        //sets scrollwrap to 100% of available area, otherwise can be small compared to available parent width
        setRenderbodyRightScrollWrap100Percent: function () {
            //$('.renderbodyRight .scrollwrap').width(eval($('.renderbodyRight').width() - 50));
            $('.renderbodyRight .scrollwrap').width(eval($('.renderbodyRight .scrollcontent').width() - 50));

        },
        setRenderbodyLeftScrollWrap100Percent: function () {
            $('.renderbodyLeft .scrollwrap').width(eval($('.renderbodyLeft').width() - 35));

        },
        setScrollwrapForTabContent: function () {
            var $mc = $("#tabContent");
            var $swrap = $mc.find(".scrollwrap");
            //var $swrapParent = $swrap.parent();
            var $swrapParent = $mc.find(".scrollcontent");
			
            $swrap.width(eval($swrapParent.width() - 50));

        }

    }
});
