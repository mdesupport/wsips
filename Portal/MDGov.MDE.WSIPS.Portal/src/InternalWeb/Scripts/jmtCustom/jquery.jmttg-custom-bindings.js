﻿

//do we have an iPad?
// requires jmt customer browser inclusion
var gIPad = $.jmttgCustomBrowser.isIpad();

jQuery.extend({
    jmttgCustomBindings: {
            bindOver: function (jqTarget, className) {
                if (!gIPad) {
                    jqTarget.bind('mouseenter mouseleave', function () {
                        $(this).unbind("toggleClass");
                        $(this).toggleClass(className);
                    });
                }
            },
            //input prompts
            bindInputs: function (targetObj, standardText, promptClassName) {
                //FOCUS IN
                targetObj.bind('focusout', function () {
                    $.jmttgCustomBindings.inputFocusOut(targetObj, standardText, promptClassName);
                });
                //FOCUS OUT
                targetObj.bind('focusin', function () {
                    $.jmttgCustomBindings.inputFocusIn(targetObj, standardText, promptClassName)
                });
                targetObj.val(standardText);
            },
            //input helper
            inputFocusIn: function (targetObj, standardText, promptClassName) {
                if (targetObj.val() == standardText) {
                    targetObj.val("");
                    targetObj.removeClass(promptClassName);
                }
            },
            //input helper
            inputFocusOut: function (targetObj, standardText, promptClassName) {
                if (targetObj.val() == "") {
                    targetObj.addClass(promptClassName);
                    targetObj.val(standardText);
                }
            }

    }
});