﻿(function ($) {
    var reviewTab = function () {
        var self = this;

        var settings = {
            permitId: null,
            permitStatusId: null,
            urls: {}
        }

        var pageElements = {
            tab: $('.appDetailsNavContainer #Review'),
            contentContainer: $('#tabContent')
        }

        this._create = function (options, loadContentCompleteCallback) {
            $.extend(settings, options);

            settings.urls.loadTabContentAction = pageElements.tab.attr('data-contenturl');
            pageElements.tab.show();
        }

        this._attach = function () {
            if (!$('.permitStatusLink.wizardStepActive').length) {
                var selectedWizardStep = $('.reviewStepsLinks').first().addClass("wizardStepActive");
                $('#PermitStatusId').val(selectedWizardStep.attr('data-permitstatusid'));
            }

            loadRightSide();

            $('.permitStatusLink').unbind('click');
            $('.permitStatusLink').click(function () {
                //clear previous active link
                $('.permitStatusLink').removeClass("wizardStepActive");
                //set active link
                $(this).addClass("wizardStepActive");

                //save current permit status
                $('#PermitStatusId').val($(this).attr('data-permitstatusid'));

                loadRightSide();
            });

            $('.permitstatusbutton').unbind('click');
            $('.permitstatusbutton').click(function () {
                if ($(this).hasClass('disabled')) {
                    return false;
                }

                $.ajax({
                    url: $(this).attr('data-posturl'),
                    type: 'POST',
                    data: { permitId: $('#PermitId').val(), permitStatusId: $('#SelectedStatusId').val(), completed: $(this).prop('id') == 'MarkAsComplete' },
                    success: function (response) {
                        // Handle success
                        if (response.IsSuccess) {
                            if ($.isFunction(self.onPermitStatusChange)) {
                                self.onPermitStatusChange();
                            }
                        }
                    }
                });
            });

            //
            // Permit Status Options Events
            //
            $('.statusOptionContainer .changeStatusButton').click(function () {
                var newStatusId = $(this).attr('data-permitstatusid');

                if ($(this).hasClass("showconfirmationmodal")) {
                    $("#statusChangeDialog > #statusChangeMessage").html($(this).attr("msg"));
                    $("#statusChangeDialog").dialog({
                        modal: true,
                        position: "center",
                        close: function () {
                            $(this).dialog("destroy");
                        },
                        buttons: {
                            "Yes": function () {
                                self._changeStatus(newStatusId);
                                $(this).dialog("close");
                            },
                            "No": function () {
                                $(this).dialog("close");
                            },
                        }
                    })
                }
                else {
                    self._changeStatus(newStatusId);
                }
            });

            $('.statusOptionContainer .previousStatusButton').click(function () {
                $.blockUI();

                var permitId = $('#PermitId').val();
                var permitStatusId = $(this).attr('data-permitstatusid');

                $.ajax({
                    url: $('.statusOptionContainer').attr('data-setpreviousstatusaction'),
                    type: 'POST',
                    data: { permitId: permitId },
                    cache: false,
                    success: function (response) {
                        if (response.success) {
                            if ($.isFunction(self.onPermitStatusChange)) {
                                self.onPermitStatusChange();
                            }
                        }
                        else {
                            alert("An error occured trying to change the permit status.");
                        }
                    }
                });
            });

            $('#toolsLinks .reviewStepsLinks').click(function () {
                window.open($(this).attr('data-clickurl'));
            });

            $('.scrollcontainer').scrollbars();

            $.jmttgCustomBrowser.setRenderbodyLeftScrollWrap100Percent();
            $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
        }

        this.loadContent = function () {
            // Get our self content
            $.ajax({
                url: settings.urls.loadTabContentAction,
                type: 'get',
                data: { permitId: settings.permitId },
                cache: false,
                success: function (result) {
                    pageElements.contentContainer.html(result);
                    self._attach();
                    if ($.isFunction(self.onLoadContentComplete)) {
                        self.onLoadContentComplete();
                    }
                }
            });
        }

        this._changeStatus = function (permitStatusId) {
            $.blockUI();

            var permitId = $('#PermitId').val();

            $.ajax({
                url: $('.statusOptionContainer').attr('data-setstatusaction'),
                type: 'POST',
                data: { permitId: permitId, permitStatusId: permitStatusId },
                cache: false,
                success: function (response) {
                    if (response.success) {
                        if ($.isFunction(self.onPermitStatusChange)) {
                            self.onPermitStatusChange();
                        }
                    }
                    else {
                        alert("An error occured trying to change the permit status.");
                    }
                }
            });
        }

        this.tabResize = function () {
            $.jmttgCustomBrowser.setRenderbodyLeftScrollWrap100Percent();
            $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();

            $("#uploadDashboardGrid").jqGrid("fixGridWidthRBRLargePadding");
            $("#commentDashboardGrid").jqGrid("fixGridWidthRBRLargePadding");
            $("#permitHearingGrid").jqGrid("fixGridWidthRBRLargePadding");
        }

        /* Callbacks */
        this.onLoadContentComplete = null;

        this.onPermitStatusChange = null;
    }

    $.permitdetails.tabs.review = function (options) {
        var instance = new reviewTab();
        instance._create(options);

        return instance;
    }
})(jQuery)