﻿(function ($) {
    var ccListTab = function () {
        var self = this;

        var settings = {
            permitId: null,
            contactTypeId: 6,
            urls: {}
        };

        var pageElements = {
            tab: $('.appDetailsNavContainer #CCList'),
            contentContainer: $('#tabContent')
        };

        this._create = function (options) {
            $.extend(settings, options);

            settings.urls.loadTabContentAction = pageElements.tab.attr('data-contenturl');
            pageElements.tab.show();
        };

        this._attach = function () {
            $('.scrollcontainer').scrollbars();
            $.jmttgCustomBrowser.setScrollwrapForTabContent();

            self._createGrid();

            $("#CcContactGrid").jqGrid("fixGridWidthTabContent");

            $('#addCcContact').click(function () {
                ContactSearch.Initialize('Generic', self._getContact);
            });
        };

        this.loadContent = function () {
            // Get our tab content
            $.ajax({
                url: settings.urls.loadTabContentAction,
                type: 'get',
                data: { permitId: settings.permitId },
                cache: false,
                success: function (result) {
                    pageElements.contentContainer.html(result);
                    self._attach();

                    if ($.isFunction(self.onLoadContentComplete)) {
                        self.onLoadContentComplete();
                    }
                }
            });
        };

        this.onLoadContentComplete = null;

        this.tabResize = function () {
            $.jmttgCustomBrowser.setScrollwrapForTabContent();
            $("#CcContactGrid").jqGrid("fixGridWidthTabContent");
        };

        this._createGrid = function () {
            var editLinkFormatter = function (cellvalue, options, rowObject) {
                return '<span class="spanLink editLink" data-contactid="' + rowObject.ContactId + '">Edit</span>';
            };

            var removeLinkFormatter = function (cellvalue, options, rowObject) {
                return '<span class="spanLink removeLink" data-permitcontactid="' + rowObject.Id + '">Remove</span>';
            };

            $("#CcContactGrid").jqGrid({
                colModel: [
                    { name: 'LName', jsonmap: "LastName", width: "20%" },
                    { name: 'FName', jsonmap: "FirstName", width: "20%" },
                    { name: 'Address', jsonmap: "Address1", width: "30%" },
                    { name: 'Email', width: "20%" },
                    { name: 'edit', formatter: editLinkFormatter, width: "5%" },
                    { name: 'delete', formatter: removeLinkFormatter, width: "5%" }
                ],
                url: $('#CcContactGridContainer #GridDataAction').val(),
                postData: { id: settings.permitId, contactTypeId: settings.contactTypeId },
                colNames: ['Last Name', 'First Name', 'Address', 'Email', '', ''],
                gridComplete: function () {
                    if ($("#CcContactGrid").jqGrid('getGridParam', 'records') == 0) {
                        $("#CcContactGridContainer").hide();
                        $("#MsgNoContact").detach();
                        $("#addCcContact").parent().append('<p id="MsgNoContact">No contact is available</p>');
                    } else {
                        $("#MsgNoContact").detach();
                        $("#CcContactGridContainer").show();
                    }
                },
                autowidth: true,
                scrollOffset: 0,
                forceFit: true,
                height: "auto"
            });

            $('#CcContactGrid').on('click', '.editLink', function () {
                self._getContact($(this).attr('data-contactid'));
            });

            $('#CcContactGrid').on('click', '.removeLink', function () {
                var permitContactId = $(this).attr('data-permitcontactid');
                $('#contactDeleteDialog').dialog({
                    modal: true,
                    position: "center",
                    width: 500,
                    draggable: false,
                    resizable: false,
                    buttons: [{
                        text: "Yes",
                        click: function () {
                            $.ajax({
                                url: $('#CcContactGridContainer #RemoveContactAction').val(),
                                type: 'post',
                                data: { permitContactId: permitContactId },
                                success: function (response) {
                                    self.reloadGrid();
                                }
                            });

                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "No",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }]
                });
            });
        };

        this._getContact = function (contactId) {
            $.ajax({
                url: $('#CcContactGridContainer #GetContactAction').val(),
                type: 'Get',
                data: { contactId: contactId },
                success: function (response) {
                    $('#CcContactForm #DynamicContent').html(response);

                    $("#CcContactForm #ContactType").val('CarbonCopy');
                    $("#CcContactForm").removeData("validator");
                    $("#CcContactForm").removeData("unobtrusiveValidation");
                    $.validator.unobtrusive.parse("#CcContactForm");

                    $('#EditCcContact #TelephoneNumber_CommunicationValue').mask("999-999-9999");

                    $('#EditCcContact').dialog({
                        title: 'Contact',
                        modal: true,
                        height: 580,
                        width: 600,
                        draggable: false,
                        resizable: false,
                        open: function () { styleDialogInputs(); },
                        buttons: [{
                            text: "Save",
                            click: function () {
                                var $form = $('#CcContactForm');
                                if ($form.valid()) {
                                    $form.submit();
                                }
                            }
                        },
                        {
                            text: "Cancel",
                            click: function () {
                                $(this).dialog('close');
                            }
                        }]
                    });
                }
            });
        };

        this.reloadGrid = function () {
            $("#CcContactGrid").trigger("reloadGrid");
            $('#CcContactGridContainer').show();
        };
    };

    $.permitdetails.tabs.cclist = function (options) {
        var instance = new ccListTab();
        instance._create(options);

        return instance;
    };
})(jQuery)