﻿(function ($) {
    var detailsTab = function () {
        var self = this;

        var settings = {
            permitId: null,
            urls: {}
        };

        var pageElements = {
            tab: $('.appDetailsNavContainer #Details'),
            contentContainer: $('#tabContent')
        };

        this._create = function (options) {
            $.extend(settings, options);

            settings.urls.loadTabContentAction = pageElements.tab.attr('data-contenturl');
            pageElements.tab.show();
        };

        this._attach = function () {
            ApplicationWizard.Initialize(settings.urls);

            //
            // Permit Status Options Events
            //
            $('.statusOptionContainer .changeStatusButton').click(function () {
                var newStatusId = $(this).attr('data-permitstatusid');

                if ($(this).hasClass("showconfirmationmodal")) {
                    $("#statusChangeDialog > #statusChangeMessage").html($(this).attr("msg"));
                    $("#statusChangeDialog").dialog({
                        modal: true,
                        position: "center",
                        close: function () {
                            $(this).dialog("destroy");
                        },
                        buttons: {
                            "Yes": function () {
                                self._changeStatus(newStatusId);
                                $(this).dialog("close");
                            },
                            "No": function () {
                                $(this).dialog("close");
                            },
                        }
                    });
                } else {
                    self._changeStatus(newStatusId);
                }
            });

            $('.statusOptionContainer .previousStatusButton').click(function () {
                $.blockUI();

                var permitId = $('#PermitId').val();
                var permitStatusId = $(this).attr('data-permitstatusid');

                $.ajax({
                    url: $('.statusOptionContainer').attr('data-setpreviousstatusaction'),
                    type: 'POST',
                    data: { permitId: permitId },
                    cache: false,
                    success: function (response) {
                        if (response.success) {
                            window.location.reload();
                        } else {
                            alert("An error occured trying to change the permit status.");
                        }
                    }
                });
            });

            $('.scrollcontainer').scrollbars();

            $.jmttgCustomBrowser.setRenderbodyLeftScrollWrap100Percent();
            $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
        };

        this.loadContent = function () {
            // Get our tab content
            $.ajax({
                url: settings.urls.loadTabContentAction,
                type: 'get',
                data: { permitId: settings.permitId },
                cache: false,
                success: function (result) {
                    pageElements.contentContainer.html(result);
                    self._attach();

                    if ($.isFunction(self.onLoadContentComplete)) {
                        self.onLoadContentComplete();
                    }
                }
            });
        };

        this.onLoadContentComplete = null;

        this._changeStatus = function (permitStatusId) {
            $.blockUI();

            var permitId = $('#PermitId').val();

            $.ajax({
                url: $('.statusOptionContainer').attr('data-setstatusaction'),
                type: 'POST',
                data: { permitId: permitId, permitStatusId: permitStatusId },
                cache: false,
                success: function (response) {
                    if (response.success) {
                        window.location.href = response.Url;
                        //window.location.reload();
                    } else {
                        alert("An error occured trying to change the permit status.");
                    }
                }
            });
        };
        
        this.tabResize = function () {

        };
    };

    $.permitdetails.tabs.details = function (options) {
        var instance = new detailsTab();
        instance._create(options);

        return instance;
    };
})(jQuery)