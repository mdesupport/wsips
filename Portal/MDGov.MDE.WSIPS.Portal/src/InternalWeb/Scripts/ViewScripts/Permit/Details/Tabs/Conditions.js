﻿(function ($) {
    var conditionsTab = function () {
        var self = this;

        var settings = {
            permitId: null,
            urls: {}
        }

        var pageElements = {
            tab: $('.appDetailsNavContainer #Conditions'),
            contentContainer: $('#tabContent')
        }

        this._create = function (options) {
            $.extend(settings, options);

            settings.urls.loadTabContentAction = pageElements.tab.attr('data-contenturl');
            pageElements.tab.show();
        }

        this._attach = function () {
            if ($('#PermitCategoryId').val() != 6 && $('#PermitCategoryId').val() != 7) {
                $("#addConditionsButton").click(function () {
                    if ($("#SelectedConditionsTemplateId").val()) {
                        $("#SelectedConditionsTemplateId").prop('disabled', true);
                        $(this).prop('disabled', true);
                        self.copyConditions($(this).attr('data-actionurl'));
                    }
                });
            } else {
                $("#addConditionsButton").addClass('disabled ui-button-disabled ui-state-disabled')
            }

            $('.scrollcontainer').scrollbars();

            $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
            $("#conditionsResults").jqGrid("fixGridWidth");
        }

        this.loadContent = function () {
            // Get our tab content
            $.ajax({
                url: settings.urls.loadTabContentAction,
                type: 'get',
                data: { permitId: settings.permitId },
                cache: false,
                success: function (result) {
                    pageElements.contentContainer.html(result);
                    self._attach();

                    if ($.isFunction(self.onLoadContentComplete)) {
                        self.onLoadContentComplete();
                    }
                }
            });
        }

        this.onLoadContentComplete = null;

        this.copyConditions = function (url) {
            $.ajax({
                url: url,
                type: 'Get',
                data: { permitId: settings.permitId, permitTemplateId: $("#SelectedConditionsTemplateId").val() },
                success: function (response) {
                    if (response.IsSuccess) self.loadContent();
                }
            });
        }

        this.tabResize = function () {
            $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
            $("#conditionsResults").jqGrid("fixGridWidth");
        }
    }

    $.permitdetails.tabs.conditions = function (options) {
        var instance = new conditionsTab();
        instance._create(options);

        return instance;
    }
})(jQuery)