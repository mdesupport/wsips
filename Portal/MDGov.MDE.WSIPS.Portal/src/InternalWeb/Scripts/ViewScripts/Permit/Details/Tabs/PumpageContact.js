﻿(function ($) {
    var pumpageContactTab = function () {
        var self = this;

        var settings = {
            permitId: null,
            urls: {}
        }

        var pageElements = {
            tab: $('.appDetailsNavContainer #PumpageContact'),
            contentContainer: $('#tabContent')
        }

        this._create = function (options) {
            $.extend(settings, options);

            settings.urls.loadTabContentAction = pageElements.tab.attr('data-contenturl');
            pageElements.tab.show();
        }

        this._attach = function () {
            $('.scrollcontainer').scrollbars();
            $.jmttgCustomBrowser.setScrollwrapForTabContent();

            $("#pumpageContactForm").removeData("validator");
            $("#pumpageContactForm").removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse("#pumpageContactForm");

            //$("#pumpageContactForm").append('<input type="hidden" id="PermitId" name="PermitId" value=' + $("#PermitId").val() + '>');
            $("#pumpageContactForm input[id$=TelephoneNumber_CommunicationValue]").mask("999-999-9999");

            $('#AddPumpageContactButton').click(function () { 
                 ContactSearch.Initialize('Generic', self.populatePumpageContactForm);
            });

            $('#SavePumpageContactButton').click(function () {
                if ($("#pumpageContactForm").valid()) {
                    $("#pumpageContactForm").submit();
                }
            });
        }

        this.loadContent = function () {
            // Get our tab content
            $.ajax({
                url: settings.urls.loadTabContentAction,
                type: 'get',
                data: { permitId: settings.permitId },
                cache: false,
                success: function (result) {
                    pageElements.contentContainer.html(result);
                    self._attach();

                    if ($.isFunction(self.onLoadContentComplete)) {
                        self.onLoadContentComplete();
                    }
                }
            });
        }

        this.onLoadContentComplete = null;

        this.populatePumpageContactForm = function () { 
            if (global.GenericUserContactFound) {
                var container = $('#pumpageContactForm .contact');
                if (ContactSearch.SelectedContact != null) { 
                    container.find('input[id$=Id]').val(ContactSearch.SelectedContact.Id);
                    container.find('span[id$=IdLabel]').text(ContactSearch.SelectedContact.Id);
                    container.find('input[id$=UserId]').val(ContactSearch.SelectedContact.userId);
                    container.find('input[id$=FirstName]').val(ContactSearch.SelectedContact.FirstName);
                    container.find('input[id$=MiddleInitial]').val(ContactSearch.SelectedContact.MiddleInitial);
                    container.find('input[id$=LastName]').val(ContactSearch.SelectedContact.LastName);
                    container.find('input[id$=BusinessName]').val(ContactSearch.SelectedContact.BusinessName);
                    /*container.find('input[id$=SecondaryName]').val(ContactSearch.SelectedContact.SecondaryName);*/
                    container.find('input[id$=Address1]').val(ContactSearch.SelectedContact.Address1);
                    container.find('input[id$=Address2]').val(ContactSearch.SelectedContact.Address2);
                    container.find('input[id$=City]').val(ContactSearch.SelectedContact.City);
                    container.find('select[id$=StateId]').val(ContactSearch.SelectedContact.StateId).change();
                    container.find('input[id$=ZipCode]').val(ContactSearch.SelectedContact.ZipCode);
                    container.find('input[id$=PrimaryTelephoneNumber_CommunicationValue]').val(ContactSearch.SelectedContact.PrimaryTelephoneNumber.CommunicationValue);
                    container.find('select[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val(ContactSearch.SelectedContact.PrimaryTelephoneNumber.SelectedCommunicationMethodId).change();
                    container.find('input[id$=AltTelephoneNumber_CommunicationValue]').val(ContactSearch.SelectedContact.AltTelephoneNumber.CommunicationValue);
                    container.find('select[id$=AltTelephoneNumber_SelectedCommunicationMethodId]').val(ContactSearch.SelectedContact.AltTelephoneNumber.SelectedCommunicationMethodId).change();
                    container.find('input[id$=EmailAddress_CommunicationValue]').val(ContactSearch.SelectedContact.EmailAddress.CommunicationValue);
                }
                else { 
                    $("label[for='Contact_Id']", container).hide();
                    container.find('input[id$=Id]').val('');
                    container.find('span[id$=IdLabel]').text('');
                    container.find('input[id$=UserId]').val('');
                    container.find('input[id$=FirstName]').val($("#SearchFirstName").val());
                    container.find('input[id$=MiddleInitial]').val('');
                    container.find('input[id$=LastName]').val($("#SearchLastName").val());
                    container.find('input[id$=BusinessName]').val($("#SearchBusinessName").val());
                    /*container.find('input[id$=SecondaryName]').val('');*/
                    container.find('input[id$=Address1]').val('');
                    container.find('input[id$=Address2]').val('');
                    container.find('input[id$=City]').val('');
                    container.find('select[id$=StateId]').val('').change();
                    container.find('input[id$=ZipCode]').val('');  
                    container.find('input[id$=PrimaryTelephoneNumber_CommunicationValue]').val('').prop('readonly', false).mask('999-999-9999');
                    container.find('select[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val('').prop('disabled', false).change();
                    container.find('input[id$=AltTelephoneNumber_CommunicationValue]').val('').prop('readonly', false).mask('999-999-9999');
                    container.find('select[id$=AltTelephoneNumber_SelectedCommunicationMethodId]').val('').prop('disabled', false).change();
                    container.find('input[id$=EmailAddress_CommunicationValue]').val($("#SearchEmailAddress").val()).prop('readonly', false);  
                } 
            }
        }

        this.tabResize = function () {
            $.jmttgCustomBrowser.setScrollwrapForTabContent();
        }
    }

    $.permitdetails.tabs.pumpagecontact = function (options) {
        var instance = new pumpageContactTab();
        instance._create(options);

        return instance;
    }
})(jQuery)