﻿/// <reference path="../../../jquery-1.10.1.intellisense.js" />
/// <reference path="Tabs/Comments.js" />
/// <reference path="Tabs/Details.js" />
/// <reference path="Tabs/Documents.js" />
/// <reference path="Tabs/Compliance.js" />
/// <reference path="Tabs/Conditions.js" />
/// <reference path="Tabs/Enforcement.js" />
/// <reference path="Tabs/IPL.js" />
/// <reference path="Tabs/Review.js" />
/// <reference path="Tabs/Contacts.js" />
/// <reference path="Controls/Files.js" />
/// <reference path="Controls/Checklist.js" />
/// <reference path="Controls/Comments.js" />

(function ($) {
    var permitDetailsForm = function () {
        var self = this,
            permit = null,
            navControl = new permitNavigationControl();

        var tabs = {
            Details: null,
            Review: null,
            Conditions: null,
            IPL: null,
            CCList: null,
            PumpageContact: null,
            Documents: null,
            Comments: null,
            Compliance: null,
            Enforcement: null,
            Contacts: null
        };

        var settings = {
            permitId: null,
            defaultTab: 'Details',
            urls: {
                getPermitAction: '',
                dashboardAction: ''
            }
        }

        this._attach = function (options) {
            // Extend our default settings
            $.extend(settings, options);

            var queryParams = $.parseQueryString(window.location.search);
            if (queryParams.id) {
                settings.permitId = queryParams.id;
            }

            // Get our Permit object
            self._getPermit(settings.permitId);

            self._buildNavControl();
            self._buildTabs();
            self._bindEvents();

            window.History.Adapter.bind(window, 'statechange', function () { // Note: We are using statechange instead of popstate
                var state = History.getState(); // Note: We are using History.getState() instead of event.state

                var queryParams = $.parseQueryString(state.url);
                if (queryParams.activeTab) {
                    self.setActiveTab(queryParams.activeTab);
                }
                else {
                    self.setActiveTab(settings.defaultTab);
                }
            });

            if (queryParams.activeTab) {
                self.setActiveTab(queryParams.activeTab);
            }
            else {
                self.setActiveTab(settings.defaultTab);
            }
        }

        this._buildNavControl = function () {
            // Build our Permit Navigation Controls
            navControl.onUpdate = self.reload;
            navControl._attach(permit);
        }

        this.getNavControl = function () {
            return navControl;
        }

        /* Loading of Tabs Section */

        this._buildTabs = function () {
            var availableTabs = {}
            var tabSettings = {
                permitId: settings.permitId
            }
            var categoryId = permit.CategoryId;

            // Default tabs for any category
            $.extend(availableTabs, {
                Details: self._getDetailsTabInstance(tabSettings),
                Documents: self._getDocumentsTabInstance(tabSettings),
                Contacts: self._getContactsTabInstance(tabSettings)
            });

            // Category specific tabs
            if (categoryId > 1) {
                if (categoryId == 8) { // Enforcement Only
                    $.extend(availableTabs, {
                        Enforcement: self._getEnforcementTabInstance(tabSettings)
                    });
                }
                else {
                    $.extend(availableTabs, {
                        Review: self._getReviewTabInstance(tabSettings),
                        Comments: self._getCommentsTabInstance(tabSettings)
                    });

                    if (categoryId == 2 || categoryId == 3 || categoryId == 4) {
                        $.extend(availableTabs, {
                            Conditions: self._getConditionsTabInstance(tabSettings),
                            IPL: self._getIPLTabInstance(tabSettings),
                            CCList: self._getCCListTabInstance(tabSettings),
                            PumpageContact: self._getPumpageContactTabInstance(tabSettings)
                        });
                    } else if (categoryId == 6) {
                        $.extend(availableTabs, {
                            Conditions: self._getConditionsTabInstance(tabSettings),
                            IPL: self._getIPLTabInstance(tabSettings),
                            CCList: self._getCCListTabInstance(tabSettings),
                            PumpageContact: self._getPumpageContactTabInstance(tabSettings),
                            Compliance: self._getComplianceTabInstance(tabSettings),
                            Enforcement: self._getEnforcementTabInstance(tabSettings)
                        });
                    } else if (categoryId == 7) {
                        $.extend(availableTabs, {
                            Enforcement: self._getEnforcementTabInstance(tabSettings)
                        });
                    }
                }
            }

            tabs = availableTabs;
        }

        this._getDetailsTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.details(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getReviewTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.review(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;
            tab.onPermitStatusChange = function () {
                self.reload(true);
            }

            return tab;
        }

        this._getConditionsTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.conditions(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getIPLTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.ipl(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getCCListTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.cclist(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getPumpageContactTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.pumpagecontact(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getDocumentsTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.documents(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getCommentsTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.comments(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getComplianceTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.compliance(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getEnforcementTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.enforcement(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        this._getContactsTabInstance = function (tabSettings) {
            var tab = $.permitdetails.tabs.contacts(tabSettings);
            tab.onLoadContentComplete = self.onTabLoadContentComplete;

            return tab;
        }

        /*End Loading of Tabs Section*/

        this._bindEvents = function () {
            ///
            // Analyze Button
            ///
            $('.analyzeInMapIcon').click(function () {
                window.location.href = settings.urls.dashboardAction + '?' + $.param({
                    mode: 'Analysis',
                    permitId: settings.permitId
                });
            });

            //
            // Tab Click
            //
            $('.appDetailsNavContainer .grayTabLink').click(self._handleTabClick);

            //
            // Attach Separator Clicks
            //
            $('#tabContent').on('click', '.filterheader', function () {
                //get arrow
                var $arrowSpan = $(this).find(".renderBLArrow");
                //get content
                var $contentDiv = $(this).next("div");
                //handle arrow & title
                if ($contentDiv.is(":visible")) {
                    $arrowSpan.html("&#x25BC;");
                    $(this).attr("title", "Open");
                } else {
                    $arrowSpan.html("&#x25B2;");
                    $(this).attr("title", "Close");
                }

                $contentDiv.slideToggle();
            });
        }

        this._handleTabClick = function (tabElement) {
            if (!$(this).is('.active')) {
                var id = $(this).prop('id');
                window.History.pushState({ tabId: id }, "Permit Details - Water Supply Information & Permitting", "?id=" + settings.permitId + "&activeTab=" + id);
            }
        }

        this._getPermit = function (permitId) {
            $.ajax({
                url: settings.urls.getPermitAction,
                type: 'get',
                data: {
                    id: permitId
                },
                async: false,
                cache: false,
                success: function (result) {
                    permit = result;
                }
            });
        }

        this.reload = function (reloadTab) {
            self._getPermit(settings.permitId);

            navControl.reload(permit);

            if (reloadTab) {
                self.setActiveTab(settings.activeTab);
            }
        }

        this.setActiveTab = function (tabId) {
            if (tabs[tabId] != null) {
                $.blockUI({ message: '<h4>Loading...</h4>' });

                $('#tabContent').empty();

                settings.activeTab = tabId;
                $('.appDetailsNavContainer .grayTabLink').removeClass('active');
                $('.appDetailsNavContainer #' + tabId).addClass('active');

                tabs[tabId].loadContent();
            }
        }

        this.onTabLoadContentComplete = function () {
            $.unblockUI();
        }

        this.localResize = function () {
            if (tabs[settings.activeTab]) {
                var activeTab = tabs[settings.activeTab];
                if (activeTab != null && activeTab.tabResize && typeof (activeTab.tabResize) == typeof (Function)) {
                    activeTab.tabResize();
                }
            }
        }
    }

    var permitNavigationControl = function () {
        var self = this,
            permit = null,
            supplGroupControl = new supplementalGroupControl();

        var settings = {
            urls: {
                getPermitHistoryAction: $('#permitHistoryPulldown').attr('data-geturl'),
                getPermitTypesAction: $('#permitTypePulldown').attr('data-geturl'),
                savePermitTypeAction: $('#permitTypePulldown').attr('data-saveurl')
            },
            permitTypeId: null
        }

        this._attach = function (permit) {
            self.reload(permit);

            self._bindEvents();
        }

        this._populatePermitHistoryDropdown = function () {
            $.ajax({
                url: settings.urls.getPermitHistoryAction,
                type: 'get',
                data: {
                    id: permit.Id
                },
                async: true,
                cache: false,
                success: function (result) {
                    var a = [],
                        b = [];

                    if (result.PermitHistory && result.PermitHistory.length) {
                        for (i in result.PermitHistory) {
                            var item = result.PermitHistory[i];
                            if (item.Id == permit.Id) {
                                if (permit.CategoryId == 2 || permit.CategoryId == 3 || permit.CategoryId == 4) {
                                    a[i] = '<div class="LNPulldownClickableItem LNPulldownClickableItemSelected"><span class="permitName">' + item.PermitName + '</span> <span class="spanLink editLink">(Edit)</span></div>';
                                }
                                else {
                                    a[i] = '<div class="LNPulldownClickableItem LNPulldownClickableItemSelected"><span class="permitName">' + item.PermitName + '</span></div>';
                                }
                            } else {
                                a[i] = '<div class="LNPulldownClickableItem" data-permitid="' + item.Id + '"><span class="permitName">' + item.PermitName + '</span></div>';
                            }
                        }
                    } else {
                        a[0] = 'No Permit History To Display';
                    }

                    if (result.SupplementalGroupId > 0) {
                        b[0] = '<span id="supplementalGroupViewSpan" class="LNPulldownClickableItem" data-groupid="' + result.SupplementalGroupId + '">' + result.SupplementalGroupName + '</div>';
                    } else {
                        b[0] = '<span id="supplementalGroupAddSpan" class="LNPulldownClickableItem">Add to Group</span>'
                    }

                    // Fill in History
                    $('#permitHistoryPulldown #history .LNPulldownScrollContent').html(a.join(''));

                    // Handle item clicks
                    $('#permitHistoryPulldown #history .LNPulldownClickableItem').not('.LNPulldownClickableItemSelected').click(function () {
                        window.location.search = $.param({
                            id: $(this).attr('data-permitid')
                        });
                    });
                    $('#permitHistoryPulldown .editLink').click(function () {
                        $(this).closest('.LNPulldown').slideUp(400, "easeOutQuad");
                        $('#permitHistoryPulldown #EditPermitNumberDialog').clone().dialog({
                            title: 'Edit Permit Number',
                            modal: true,
                            width: 500,
                            draggable: false,
                            resizable: false,
                            open: function () {
                                $(this).find('#PermitNumber').mask('aa9999a999');
                            },
                            close: function () {
                                $(this).dialog('destroy');
                            },
                            buttons: [{
                                text: "Save",
                                click: function () {
                                    var dialog = $(this),
                                        permitNumber = dialog.find('#PermitNumber').val(),
                                        isUniqueAction = dialog.find('#IsUniquePermitNumberAction').val(),
                                        saveAction = dialog.find('#SavePermitNumberAction').val();

                                    if (permitNumber.length) {
                                        $.ajax({
                                            url: isUniqueAction,
                                            type: 'get',
                                            data: { permitNumber: permitNumber },
                                            success: function (response) {
                                                if (response.isUnique) {
                                                    // We're good to save the new permit number
                                                    $.ajax({
                                                        url: saveAction,
                                                        type: 'post',
                                                        data: { permitId: permit.Id, permitNumber: permitNumber },
                                                        success: function (response) {
                                                            dialog.dialog('close');
                                                            self.onUpdate(permit);
                                                        }
                                                    });
                                                }
                                                else {
                                                    alert('Permit number is not unique. Please enter a unique value.');
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        alert('Please enter a permit number');
                                    }
                                }
                            },
                            {
                                text: "Cancel",
                                click: function () {
                                    $(this).dialog('close');
                                }
                            }]
                        });
                    });

                    if (permit.CategoryId == 1) {
                        $('#relatedPermits').hide();
                    }
                    else {
                        // Fill in Supplemental Groups
                        $('#permitHistoryPulldown #related .LNPulldownScrollContent').html(
                            '<div style="text-align: center; line-height: 40px;">' +
                            b.join('') +
                            '</div>');

                        // Attach our supplemental group control
                        supplGroupControl._attach();

                        // Handle item clicks
                        $('#permitHistoryPulldown #related #supplementalGroupAddSpan').click(function () {
                            // Adding new group
                            supplGroupControl.addToSupplementalGroup();
                            self.togglePermitHistoryPulldown();
                        });
                        $('#permitHistoryPulldown #related #supplementalGroupViewSpan').click(function () {
                            // Viewing existing group
                            supplGroupControl.viewSupplementalGroup($(this).attr('data-groupid'));
                            self.togglePermitHistoryPulldown();
                        });
                    }

                    // Scrollbars
                    $('#permitHistoryPulldown .LNPulldownScrollContent').scrollbars();
                    // Set to width of container
                    $('#permitHistoryPulldown .scrollwrap').width($('#permitHistoryPulldown').width() - 35);
                }
            });
        }

        this._populatePermitTypeDropdown = function () {
            $.ajax({
                url: settings.urls.getPermitTypesAction,
                type: 'get',
                async: true,
                cache: false,
                success: function (result) {
                    var a = new Array();
                    for (i in result) {
                        if (result[i].Key.toLowerCase() == 'exemption') {
                            continue;
                        }

                        // Append to Array
                        a[i] = '<div class="LNPulldownClickableItem" data-permittypeid="' + result[i].Id + '">' + result[i].Key + '</div>';
                    }

                    // Scrollbars
                    $('#permitTypePulldown .LNPulldownScrollContent').html(a.join('')).scrollbars();
                    // Set to width of container
                    $('#permitTypePulldown .scrollwrap').width($('#permitTypePulldown').width() - 35);

                    $('#permitTypePulldown .LNPulldownClickableItem').click(function () {
                        settings.permitTypeId = $(this).attr('data-permittypeid');
                        $(this).addClass('LNPulldownClickableItemSelected').siblings().removeClass('LNPulldownClickableItemSelected');
                    });
                }
            });
        }

        this._bindEvents = function () {
            $(document).mousedown(function (event) {
                // Close any open pulldowns.
                // If click occurs within Local Nav Pulldown Container, do nothing.
                $('.LNPulldown').not($(event.target).closest('.LNPulldown')).slideUp(400, "easeOutQuad");
            });

            $('.localNavContainer #permitNumberArrowSpan').click(function () {
                self.togglePermitHistoryPulldown();
            });
            $('.localNavContainer #permitTypeEditSpan').click(function () {
                if ($('#permitTypePulldown').is(':hidden')) {
                    $('#permitTypePulldown .LNPulldownClickableItem').removeClass('LNPulldownClickableItemSelected');
                    $('#permitTypePulldown .LNPulldownClickableItem[data-permittypeid="' + permit.PermitTypeId + '"]').addClass('LNPulldownClickableItemSelected');
                }

                self.togglePermitTypePulldown();
            });

            $('.LNPulldownBottomLink.close').click(function () {
                $(this).closest('.LNPulldown').slideUp(400, "easeOutQuad");;
            });

            $('#permitTypePulldown .LNPulldownBottomLink.save').click(function () {
                var newPermitTypeId = $('#permitTypePulldown .LNPulldownClickableItem.LNPulldownClickableItemSelected').attr('data-permittypeid'),
                    newPermitTypeKey = $('#permitTypePulldown .LNPulldownClickableItem.LNPulldownClickableItemSelected').text();

                $('#permitTypeNavControl #permitTypeSpan').text(newPermitTypeKey);
                $('#permitTypePulldown').slideUp(400, "easeOutQuad");

                $.ajax({
                    url: settings.urls.savePermitTypeAction,
                    type: 'post',
                    async: true,
                    cache: false,
                    data: {
                        id: permit.Id,
                        permitTypeId: settings.permitTypeId
                    },
                    success: function () {
                        self.onUpdate(true);
                    }
                });
            });
        }

        this.togglePermitHistoryPulldown = function ($target) {
            var $target = $('#permitHistoryPulldown');

            //hide other menus if open
            $('.LNPulldown').not($target).slideUp(400, "easeOutQuad");

            //show/Hide menu
            if ($target.is(':visible')) {
                //need to hide menu
                $target.slideUp(400, "easeOutQuad");
            } else {
                //need to show menu
                //first, position the menu accordingly
                var left = $('#permitHistoryNavControl').position().left;
                var width = $('#permitHistoryNavControl').outerWidth();

                var right = left + width;

                // Pulldown container is 225px + 1px left/right border
                $('#permitHistoryPulldown').css('left', right - 227);

                //now open it
                $target.slideDown(600, "easeOutQuad");
            }
        }

        this.togglePermitTypePulldown = function () {
            var $target = $('#permitTypePulldown');

            //hide other menus if open
            $('.LNPulldown').not($target).slideUp(400, "easeOutQuad");

            //show/Hide menu
            if ($target.is(':visible')) {
                //need to hide menu
                $target.slideUp(400, "easeOutQuad");
            } else {
                //need to show menu
                //first, position the menu accordingly
                var left = $('#permitTypeNavControl').position().left;
                var width = $('#permitTypeNavControl').outerWidth();

                var right = left + width;

                // Pulldown container is 225px + 1px left/right border
                $('#permitTypePulldown').css('left', right - 227);

                //now open it
                $target.slideDown(600, "easeOutQuad");
            }
        }

        this.reload = function (param) {
            if (param) {
                permit = param;
            }

            // Permittee Nav Control
            $('#permitteeNavControl #permitteeSpan').text(permit.Permittee);

            // Project Name Nav Control
            $('#permitteeNavControl #projectNameSpan').text(permit.ProjectName);

            // Permit History Nav Control
            $('#permitHistoryNavControl #permitNumberSpan').text(permit.PermitName);
            self._populatePermitHistoryDropdown();

            // Permit Type Nav Control
            if (permit.CategoryId == 6) {
                $('#permitTypeNavControl').hide();
            } else {
                if (permit.CategoryId == 1) {
                    $('#permitTypeNavControl #permitTypeSpan').text(permit.ApplicationTypeDescription);
                    $('#permitTypeNavControl #permitTypeEditSpan').remove();                    
                }
                else {
                    $('#permitTypeNavControl #permitTypeSpan').text(permit.PermitTypeKey);
                    self._populatePermitTypeDropdown();
                }

                $('#permitTypeNavControl').show();
            }

            // Permit Status Nav Control
            $('#permitStatusNavControl #permitStatusSpan').text(permit.PermitStatus);
        }

        this.onUpdate = null;
    }

    var supplementalGroupControl = function () {
        var self = this,
            selectedSupplementalGroupId = null,
            supplementalTypeDropdown = null;

        var gridFormatters = {
            addToGroupLinkFormatter: function (cellvalue, options, rowObject) {
                return '<span class="spanLink addToGroupLink" data-groupid="' + rowObject.Id + '">Add to Group</span>';
            },

            viewGroupLinkFormatter: function (cellvalue, options, rowObject) {
                return '<span class="spanLink viewGroupLink" data-groupid="' + rowObject.Id + '">View Group</span>';
            },

            removeFromGroupLinkFormatter: function (cellvalue, options, rowObject) {
                return '<span class="spanLink removeFromGroupLink" data-permitid="' + rowObject.PermitId + '">Remove</span>';
            },

            supplementalTypeDropdownFormatter: function (cellvalue, options, rowObject) {
                var list = supplementalTypeDropdown.clone().attr('data-permitid', rowObject.PermitId);
                list.children('option[value="' + rowObject.SupplementalTypeId + '"]').attr('selected', 'selected');
                return list[0].outerHTML;
            }
        }

        this._attach = function () {
            self._bindEvents();

            self._populateSupplementalTypes();

            $("#SupplementalGroupDialog").dialog({
                modal: true,
                autoOpen: false,
                position: "center",
                resizable: false,
                draggable: false,
                width: 700
            });

            $("#SupplementalGroupSearchGrid").jqGrid({
                colModel: [
                    { name: 'Name', width: "25" },
                    { name: 'addToGroup', width: "12", formatter: gridFormatters.addToGroupLinkFormatter },
                    { name: 'viewGroup', width: "12", formatter: gridFormatters.viewGroupLinkFormatter }
                ],
                colNames: ['Group Name', '', ''],
                shrinkToFit: true,
                scrollOffset: 0,
                width: 670,
                gridComplete: function () {
                    if ($(this).jqGrid('getGridParam', 'records') == 0) {
                        $("#NoResultsFoundMsg").show();
                    } else {
                        $("#NoResultsFoundMsg").hide();
                    }
                }
            });

            $("#ViewSupplementalGroupGrid").jqGrid({
                colModel: [
                    { name: 'PermitName', width: "25" },
                    { name: 'SupplementalType', width: "25", align: 'center', formatter: gridFormatters.supplementalTypeDropdownFormatter },
                    { name: 'removeFromGroup', width: "25", align: 'center', formatter: gridFormatters.removeFromGroupLinkFormatter }
                ],
                colNames: ['Permit Name', 'Supplemental Type', ''],
                shrinkToFit: true,
                scrollOffset: 0,
                width: 670
            });
        }

        this._bindEvents = function () {
            $('#SupplementalGroupSearchButton').click(function () {
                $('#SupplementalGroupSearchGridContainer').show();
                $('#SupplementalGroupSearchGrid')
                    .jqGrid('setGridParam', {
                        url: $(this).attr('data-url'),
                        postData: {
                            permitNames: $('#SupplementalGroupSearchTextBox').val()
                        }
                    }).trigger("reloadGrid");
            });

            $('#SupplementalGroupSearchGrid').on('click', '.addToGroupLink', function () {
                var groupId = $(this).attr('data-groupid')
                $.ajax({
                    url: $('#ViewSupplementalGroupSection #AddToSupplementalGroupAction').val(),
                    type: 'POST',
                    data: {
                        permitId: $('#PermitId').val(),
                        groupId: groupId
                    },
                    success: function (response) {
                        if (response.Success) {
                            // Toggle the modal and refresh the nav header
                            self.viewSupplementalGroup(groupId);

                            $.permitdetails.index('getNavControl').reload();
                        }
                    }
                });
            });

            $('#SupplementalGroupSearchGrid').on('click', '.viewGroupLink', function () {
                self.viewSupplementalGroup($(this).attr('data-groupid'));
            });

            $('#CreateSupplementalGroupLink').click(function () {
                $.ajax({
                    url: $(this).attr('data-posturl'),
                    type: 'POST',
                    data: {
                        activePermitId: $('#PermitId').val(),
                        additionalPermitNames: $('#SupplementalGroupSearchTextBox').val()
                    },
                    success: function (response) {
                        selectedSupplementalGroupId = response.GroupId;
                        $('#SearchPermitSection').hide();
                        $('#ViewSupplementalGroupSection').show();

                        $('#ViewSupplementalGroupGrid')
                            .jqGrid('setGridParam', {
                                url: $('#ViewSupplementalGroupSection #GridDataAction').val(),
                                postData: {
                                    id: selectedSupplementalGroupId
                                }
                            })
                            .trigger("reloadGrid");

                        // Refresh the nav header
                        $.permitdetails.index('getNavControl').reload();
                    }
                });
            });

            $('#ViewSupplementalGroupSection #AddButton').click(function () {
                $('#ViewSupplementalGroupSection #AddPermitValidation').hide();
                if ($('#ViewSupplementalGroupSection #PermitName').val().length) {
                    $.ajax({
                        url: $('#ViewSupplementalGroupSection #GetPermitIdAction').val(),
                        type: 'GET',
                        data: {
                            permitName: $('#ViewSupplementalGroupSection #PermitName').val()
                        },
                        success: function (permitId) {
                            if (permitId > 0) {
                                $.ajax({
                                    url: $('#ViewSupplementalGroupSection #AddToSupplementalGroupAction').val(),
                                    type: 'POST',
                                    data: {
                                        permitId: permitId,
                                        groupId: selectedSupplementalGroupId
                                    },
                                    success: function (response) {
                                        if (response.Success) {
                                            $('#ViewSupplementalGroupGrid').trigger("reloadGrid");
                                        }
                                        else {
                                            $('#ViewSupplementalGroupSection #AddPermitValidation').show();
                                        }
                                    }
                                });
                            }
                            else {
                                $('#ViewSupplementalGroupSection #AddPermitValidation').show();
                            }
                        }
                    });


                }
            });

            $('#ViewSupplementalGroupGrid').on('change', '.supplementalTypeSelect', function () {
                $.ajax({
                    url: $('#ViewSupplementalGroupSection #SetSupplementalTypeAction').val(),
                    type: 'POST',
                    data: { permitId: $(this).attr('data-permitid'), newSupplementalTypeId: $(this).val() },
                    success: function (response) {
                        $('#ViewSupplementalGroupGrid').trigger("reloadGrid");
                    }
                });
            });

            $('#ViewSupplementalGroupGrid').on('click', '.removeFromGroupLink', function () {
                $.ajax({
                    url: $('#ViewSupplementalGroupSection #RemoveFromSupplementalGroupAction').val(),
                    type: 'POST',
                    data: { permitId: $(this).attr('data-permitid') },
                    success: function (response) {
                        if (response.Success) {
                            if (response.GroupDeleted) {
                                // Close the modal and refresh the nav header
                                self._close();
                                $.permitdetails.index('getNavControl').reload();
                            }
                            else {
                                // Simply reload the grid
                                $('#ViewSupplementalGroupGrid').trigger("reloadGrid");
                            }
                        }
                    }
                });
            });
        }

        this._populateSupplementalTypes = function () {
            $.ajax({
                url: $('#ViewSupplementalGroupSection #SupplementalTypesAction').val(),
                type: 'GET',
                async: false,
                cache: false,
                success: function (response) {
                    supplementalTypeDropdown = $('<select class="supplementalTypeSelect"></select>');
                    for (i in response) {
                        var item = response[i];
                        supplementalTypeDropdown.append($('<option></option>').prop('value', item.Id).html(item.Description));
                    }
                }
            });
        }

        this.addToSupplementalGroup = function () {
            self._resetDialog();
            $("#SupplementalGroupDialog").dialog('open');
        }

        this.viewSupplementalGroup = function (id) {
            selectedSupplementalGroupId = id;
            self._resetDialog();
            $('#SearchPermitSection').hide();
            $('#ViewSupplementalGroupSection').show();
            $('#ViewSupplementalGroupGrid')
                .jqGrid('setGridParam', {
                    url: $('#ViewSupplementalGroupSection #GridDataAction').val(),
                    postData: {
                        id: selectedSupplementalGroupId
                    }
                })
                .trigger("reloadGrid");

            $("#SupplementalGroupDialog").dialog('open');
        }

        this._resetDialog = function () {
            $('#SearchPermitSection').show();
            $('#SupplementalGroupSearchGridContainer').hide();
            $('#CreateSupplementalGroupSection').hide();
            $('#ViewSupplementalGroupSection').hide();
            $("#NoResultsFoundMsg").hide();
        }

        this._close = function () {
            selectedSupplementalGroupId = null;
            $("#SupplementalGroupDialog").dialog('close');
        }
    }

    $.permitdetails = {
        index: function (options) {
            var namespace = "mdgov.mde.wsips.portal.permitdetails";
            var fullName = namespace + '.index';

            var isMethodCall = typeof options === "string",
                args = Array.prototype.slice.call(arguments, 1);

            if (isMethodCall) {
                var instance = $.data(document, fullName);
                if (!instance) {
                    return $.error("cannot call methods on " + fullName + " prior to initialization; " +
                        "attempted to call method '" + options + "'");
                }
                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                    return $.error("no such method '" + options + "' for " + fullName + " self instance");
                }

                return instance[options].apply(instance, args);
            } else {
                var instance = $.data(document, fullName);
                if (!instance) {
                    instance = new permitDetailsForm();
                    instance._attach(options);

                    $.data(document, fullName, instance);
                }
            }
        },
        tabs: {}
    }

    $.parseQueryString = function (url) {
        if (!url) return {};

        var retVal = {},
            urlParts = url.split("?"),
            queryString = urlParts[urlParts.length - 1],
            queryParts = queryString.split("&");

        for (var i = 0; i < queryParts.length; ++i) {
            var q = queryParts[i].split('=');
            if (q.length != 2) continue;
            retVal[q[0]] = decodeURIComponent(q[1].replace(/\+/g, " "));
        }

        return retVal;
    }
})(jQuery);