﻿(function ($) {
    var contactsTab = function () {
        var self = this;

        var settings = {
            permitId: null,
            urls: {}
        }

        var pageElements = {
            tab: $('.appDetailsNavContainer #Contacts'),
            contentContainer: $('#tabContent')
        }

        this._create = function (options) {
            $.extend(settings, options);

            settings.urls.loadTabContentAction = pageElements.tab.attr('data-contenturl');
            pageElements.tab.show();
        }

        this._attach = function () {
            $('.scrollcontainer').scrollbars();
            $.jmttgCustomBrowser.setScrollwrapForTabContent();
        }

        this.loadContent = function () {
            // Get our tab content
            $.ajax({
                url: settings.urls.loadTabContentAction,
                type: 'get',
                data: { permitId: settings.permitId },
                cache: false,
                success: function (result) {
                    pageElements.contentContainer.html(result);
                    self._attach();

                    if ($.isFunction(self.onLoadContentComplete)) {
                        self.onLoadContentComplete();
                    }
                }
            });
        }

        this.onLoadContentComplete = null;

        this.tabResize = function () {
            $.jmttgCustomBrowser.setScrollwrapForTabContent();
        }
    }

    $.permitdetails.tabs.contacts = function (options) {
        var instance = new contactsTab();
        instance._create(options);

        return instance;
    }
})(jQuery)