﻿(function ($) {
    var enforcementTab = function () {
        var self = this;

        var settings = {
            permitId: null,
            urls: {}
        }

        var pageElements = {
            tab: $('.appDetailsNavContainer #Enforcement'),
            contentContainer: $('#tabContent')
        }

        this._create = function (options) {
            $.extend(settings, options);

            settings.urls.loadTabContentAction = pageElements.tab.attr('data-contenturl');
            pageElements.tab.show();
        }

        this._attach = function () {
            $('.scrollcontainer').scrollbars();

            $.jmttgCustomBrowser.setScrollwrapForTabContent();

            $('.editLink, .addLink').modal();
            $("#violationResults").jqGrid('fixGridWidthTabContent');
            $("#enforcementActionsResults").jqGrid('fixGridWidthTabContent');
            $("#penaltyResults").jqGrid('fixGridWidthTabContent');
        }

        this.loadContent = function () {
            // Get our tab content
            $.ajax({
                url: settings.urls.loadTabContentAction,
                type: 'get',
                data: { permitId: settings.permitId },
                cache: false,
                success: function (result) {
                    pageElements.contentContainer.html(result);
                    self._attach();

                    if ($.isFunction(self.onLoadContentComplete)) {
                        self.onLoadContentComplete();
                    }
                }
            });
        }

        this.onLoadContentComplete = null;

        this.tabResize = function () {
            $.jmttgCustomBrowser.setScrollwrapForTabContent();

            $("#violationResults").jqGrid('fixGridWidthTabContent');
            $("#enforcementActionsResults").jqGrid('fixGridWidthTabContent');
            $("#penaltyResults").jqGrid('fixGridWidthTabContent');
        }
    }

    $.permitdetails.tabs.enforcement = function (options) {
        var instance = new enforcementTab();
        instance._create(options);

        return instance;
    }
})(jQuery)