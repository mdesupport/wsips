﻿var PermitHearing = {
    Initialize: function (PermitId) {
        PermitHearing.SetGrid(PermitId);
        PermitHearing.HearingButtongClick();
    },

    Edit:false,

    SetGrid: function (PermitId) {
        $("#permitHearingGrid").jqGrid({
            colModel: [
                { name: 'Date' },
                { name: 'Time' },
                { name: 'Location' },
                { name: 'Officer' },
                { name: 'editColumn', formatter: PermitHearing.EditFormatter },
                { name: 'delColumn', formatter: PermitHearing.DeleteFormatter }
            ],
            colNames: ['Date', 'Time', 'Location', 'Officer', '', ''],
            url: Helper.GetBaseURL("Permitting") + "Permit/GetPermitHearingByPermitId?PermitId=" + PermitId,
            datatype: "json",
            gridComplete: function () {
                if (jQuery("#permitHearingGrid").jqGrid('getGridParam', 'records') == 0) {
                    $("#gbox_permitHearingGrid").hide()
                    $("#gbox_permitHearingGrid #pager").hide()
                    $("#MsgNoHearing").detach()
                    $("#scheduleHearingButton").parent().append('<p id="MsgNoHearing">No Hearings Have Been Scheduled</p>')
                } else {
                    $("#MsgNoHearing").detach()
                    $("#gbox_permitHearingGrid").show()
                    $("#gbox_permitHearingGrid #pager").show()
                }
            },
            autowidth: true,
            scrollOffset: 0,
            forceFit: true,
            height: "auto"
        });
    },

    EditFormatter: function (cellvalue, options, rowelem) {
        return '<span class="spanLink" onclick="PermitHearing.EditHearing(' + rowelem.Id + ')">Edit</span>';
    },

    DeleteFormatter: function (cellvalue, options, rowelem) {
        return '<span class="spanLink" onclick="PermitHearing.DeleteHearing(' + rowelem.Id + ')">Delete</span>';
    },

    DeleteHearing: function (id) {
        var deleteDialog = $("#permitHearingDeleteDialog")
        $(deleteDialog).dialog({
            modal: true,
            position: "center",
            width: 500,
            close: function () {
                $(this).dialog("destroy");
            },
            buttons: {
                "Yes": function () {
                    $.ajax({
                        url: Helper.GetBaseURL("Permitting") + "Permit/DeletePermitHearing",
                        data: { id: id },
                        success: function (response) {
                            $("#permitHearingGrid").trigger("reloadGrid");
                        }
                    });
                    $(this).dialog("close");
                },
                "No": function () {
                    $(this).dialog("close");
                },
            }
        })
    },

    SaveCallBack:function(){
    },

    EditHearing: function (id) {
        PermitHearing.Edit = true;
        $.ajax({
            url: Helper.GetBaseURL("Permit") + "Permit/GetPermitHearing",
            data: { id: id },
            cache: false,
            success: function (response) {
                PermitHearing.PopulateForm(response)
            }
        });
    },

    PopulateForm: function (data) {
        if (data != null) {
            var newForm = $("#permitHearingFormContainer")
            $(newForm).find("#Id").val(data.Id)
            $(newForm).find("#Address1").val(data.Address1)
            $(newForm).find("#Address2").val(data.Address2)
            $(newForm).find("#City").val(data.City)
            $(newForm).find("#StateId").val(data.StateId).change()
            $(newForm).find("#ZipCode").val(data.ZipCode)
            $(newForm).find("#DateOfHearing").val(data.DateOfHearing)
            $(newForm).find("#TimeOfHearing").val(data.TimeOfHearing)
            $(newForm).find("#HearingOfficer").val(data.HearingOfficer)
        }
        $("#scheduleHearingButton").click()
    },

    ClearValidationError: function () {
        //Removes validation summary 
        $('.validation-summary-errors').addClass('validation-summary-valid');
        $('.validation-summary-errors').removeClass('validation-summary-errors');

        //Removes validation from input-fields
        $('.input-validation-error').addClass('input-validation-valid');
        $('.input-validation-error').removeClass('input-validation-error');

        //Removes validation message after input-fields
        $('.field-validation-error').addClass('field-validation-valid');
        $('.field-validation-error').removeClass('field-validation-error');
    },

    HearingButtongClick: function () {
        $("#scheduleHearingButton").click(function () {
            var newForm = $("#permitHearingFormContainer")
            newForm.find("#DateOfHearing").datepicker({

                close: function () {
                    $(this).datepicker("destroy")
                }, changeYear: true, changeMonth: true

            });
            if (PermitHearing.Edit) {
                newForm.find("#permithearingdiv").show()
            }
            newForm.find("#TimeOfHearing").timepicker({
                timeFormat: "hh:mm tt",
                defaultValue: '12:00 AM'
            });

            newForm.removeData("validator");
            newForm.removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse(newForm);

            var states = newForm.find('#StateId').clone()

            newForm.dialog({
                modal: true,
                position: "center",
                width: 420,
                resizable: false,
                title: "Schedule a Hearing",
                close: function () {
                    newForm.find('input:text').val('')             // reset all textboxes
                    newForm.find('#StateId').replaceWith(states)   // reset states dro
                    $(this).dialog("destroy");
                    PermitHearing.Edit = false;
                    newForm.find("#permithearingdiv").hide()
                    PermitHearing.ClearValidationError()
                },
                buttons: {
                    "Schedule": function () {
                        if ($(this).find('form').valid()) {
                            $(this).find('form').submit()
                            $(this).dialog("close");
                            $("#permitHearingGrid").trigger("reloadGrid");

                        }
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    },
                }
            });
        });
    }
}