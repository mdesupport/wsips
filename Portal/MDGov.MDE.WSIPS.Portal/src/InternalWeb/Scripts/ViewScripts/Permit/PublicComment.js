﻿var PublicComment = {

    PermitId: "",
    IncludeGrid: false,
    MakeReadOnly: false,

    makeReadOnly: function () {
        PublicComment.MakeReadOnly = true;
    },

    Initialize: function (PermitId, IncludeGrid) {
        PublicComment.PermitId = PermitId;
        PublicComment.IncludeGrid = IncludeGrid;

        $("#addPublicCommentButton").click(function (e) {
            PublicComment.AddNewComment();
        });

        if (PublicComment.IncludeGrid) {
            PublicComment.SetGrid(PermitId);
        }
    },

    SetGrid: function (id) {
        $("#publicCommentGrid").jqGrid({
            colModel: [
                { name: 'Comment', width: 350 },
                { name: 'LastModifiedDate' },
                { name: 'Name' },
                { name: 'view', formatter: PublicComment.ViewFormatter },
                { name: 'delete', formatter: PublicComment.DeleteFormatter }
            ],
            colNames: ['Comment', 'Comment Date', 'Comment By', '', ''],
            url: Helper.GetBaseURL("Permitting") + "PublicComment/Data?id=" + id,
            gridComplete: function () {
                if ($("#publicCommentGrid").jqGrid('getGridParam', 'records') == 0) {
                    $("#gbox_publicCommentGrid").hide();
                    $("#gbox_publicCommentGrid #pager").hide();
                    $("#MsgNoPublicComment").show();
                } else {
                    $("#MsgNoPublicComment").hide();
                    $("#gbox_publicCommentGrid").show();
                    $("#gbox_publicCommentGrid #pager").show();
                    $("#publicCommentGrid").jqGrid("fixGridWidth")
                }
            },
            datatype: "json",
            height: "auto"
        });
    },

    ViewFormatter: function (cellvalue, options, rowelem) {
        return '<div style="text-align:center"><span class="spanLink" onclick="PublicComment.ViewComment(' + rowelem.Id + ')">View</span></div>';
    },

    ViewComment: function (commentId) {
        $.ajax({
            url: Helper.GetBaseURL("Permitting") + "PublicComment/GetComment",
            data: { id: commentId },
            success: function (response) {
                PublicComment.ViewExistingComment(response.comment);
            }
        });
    },

    DeleteFormatter: function (cellvalue, options, rowelem) {
        if (PublicComment.MakeReadOnly) { // disable all deleting
            return '<div style="text-align:center"><span class="spanLink disabled ui-button-disabled ui-state-disabled">Delete</span></div>';
        } else {
            return '<div style="text-align:center"><span class="spanLink" onclick="PublicComment.DeleteComment(' + rowelem.Id + ')">Delete</span></div>';
        }
    },

    DeleteComment: function (commentId) {
        $("#publicCommentDeleteDialog").dialog({
            modal: true,
            position: "center",
            width: 330,
            resizable: false,
            draggable: false,
            open: function (event, ui) {
                //style dialog buttons
                styleDialogInputs();
            },
            close: function () {
                $(this).dialog("destroy")
            },
            buttons: {
                "Yes": function () {
                    $.ajax({
                        url: Helper.GetBaseURL("Permitting") + "PublicComment/Delete",
                        data: { id: commentId },
                        success: function (response) {
                            $("#publicCommentGrid").trigger("reloadGrid");
                        }
                    });

                    $(this).dialog("close");
                },
                "No": function () {
                    $(this).dialog("close");
                },
            }
        });
    },

    AddNewComment: function () {
        var commentContainer = $("#publicCommentContainer").clone().prop('title', 'Add New Public Comment');
        PublicComment.CreateDialog(commentContainer);
    },

    ViewExistingComment: function (comment) {
        var commentContainer = $("#publicCommentContainer").clone().prop('title', 'View Public Comment');

        commentContainer.find('#thePublicComment').val(comment.Comment);
        commentContainer.find('#PublicCommentContactInfo').val(comment.ContactInfo);
        commentContainer.find('#PublicCommentName').val(comment.Name);
        commentContainer.find('#publicComentDbId').val(comment.Id);

        PublicComment.CreateDialog(commentContainer);
    },

    CreateDialog: function (container) {
        $.validator.unobtrusive.parse(container.find("form"));

        container.dialog({
            modal: true,
            title: container.prop('title'),
            position: "center",
            width: 450,
            resizable: false,
            draggable: false,
            open: function () {
                styleDialogInputs();

                container.find('#SavePublicCommentButton').click(function () {
                    if (container.find("form").valid()) {
                        $.ajax({
                            url: Helper.GetBaseURL("Permitting") + "PublicComment/SaveComment",
                            data: { Comment: container.find('#thePublicComment').val(), Name: container.find('#PublicCommentName').val(), ContactInfo: container.find('#PublicCommentContactInfo').val(), PermitId: PublicComment.PermitId, Id: container.find('#publicComentDbId').val() },
                            success: function (response) {
                                if (PublicComment.IncludeGrid) {
                                    container.dialog("close");
                                    $('#publicCommentGrid').trigger("reloadGrid");
                                } else {
                                    container.find('#addPublicCommentForm').html("Comment Added. " +
                                        "Public Comments can be viewed within the Comments tab");
                                }
                            }
                        });
                    }
                });

                container.find('#CancelPublicCommentButton').click(function () {
                    container.dialog("close");
                });
            },
            close: function () {
                container.dialog("destroy");
            }
        });
    }
}