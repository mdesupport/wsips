﻿/// <reference path="ApplicationWizard.js" />
/// <reference path="../../underscore.js" />

//Bind on change of the radio button
$('#SearchedContacts').on('change', 'input:radio[id$=IsBusiness]', function () {
    var isBusiness = $(this).val();

    ContactSearchResult.ClearValidationError();
    ContactSearchResult.AdjustContactInformationLayout(isBusiness);
});

// Extends BaseContactSearchResult 
$.extend(ContactSearchResult, {
    Next: function () {

        if (global.CurrentlySearching == "WaterUser") {
            WaterUser.Next();
        }
        else if (global.CurrentlySearching == "LandOwner") {
            LandOwner.Next();
        }
        else if (global.CurrentlySearching = "Consultant") {
            Consultant.Next();
        }
    },

    Back: function () {

        if (global.CurrentlySearching == "WaterUser") {
            WaterUser.Back();
        }
        else if (global.CurrentlySearching == "LandOwner") {
            LandOwner.Back();
        }
        else if (global.CurrentlySearching = "Consultant") {
            Consultant.Back();
        }
    },

    Cancel: function () {
    }
});

$.extend(WaterUser, {
    Next: function () {
        $('#WaterUser .stateValidationMessage').hide();
        $('#WaterUser .primaryPhoneNumberValidationMessage').hide();

        var selectedState = $('#WaterUser').find('[id$=StateId]').val();
        var primaryPhoneNumber = $('#WaterUser').find('[id$=PrimaryTelephoneNumber_CommunicationValue]').val();;
        var primaryPhoneType = $('#WaterUser').find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val();;

        if (ContactSearchResult.ValidateData()) {
            if (selectedState == '') {
                ContactSearchResult.ShowStateValidationMessage('#WaterUser');
                ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
            }
            else if (primaryPhoneNumber == '' || primaryPhoneType == '') {
                ContactSearchResult.ShowPrimaryPhoneNumberValidationMessage('#WaterUser');
                ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
            }
            else {
                LandOwner.Initialize();
            }
        }
        else {
            ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
        }
    },
    Back: function () {
        // until they confirm, Stay in the same step
        ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));

        if (confirm("All the contacts will be deleted. Are you sure you want to start over the application?")) {
            $('#SearchedContacts').hide();
            ApplicationWizard.ClearSelectedWizardStep();
            ApplicantIdentification.Initialize();
        }
        else {
            // Stay in the same step
            ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
        }
    }
});

$.extend(LandOwner, {
    Next: function () {
        $('#LandOwner .stateValidationMessage').hide();
        $('#LandOwner .primaryPhoneNumberValidationMessage').hide();

        var selectedState = $('#LandOwner').find('[id$=StateId]').val();
        var primaryPhoneNumber = $('#LandOwner').find('[id$=PrimaryTelephoneNumber_CommunicationValue]').val();;
        var primaryPhoneType = $('#LandOwner').find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val();;

        if (ContactSearchResult.ValidateData()) {
            if (selectedState == '') {
                ContactSearchResult.ShowStateValidationMessage('#LandOwner');
                ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
            }
            else if (primaryPhoneNumber == '' || primaryPhoneType == '') {
                ContactSearchResult.ShowPrimaryPhoneNumberValidationMessage('#LandOwner');
                ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
            }
            else {
                Consultant.Initialize();
            }
        }
        else {
            ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
        }
    },
    Back: function () {
        WaterUser.Initialize();
    }
});

$.extend(Consultant, {
    Next: function () {
        $('#Consultant .stateValidationMessage').hide();
        $('#Consultant .primaryPhoneNumberValidationMessage').hide();

        var selectedState = $('#Consultant').find('[id$=StateId]').val();
        var primaryPhoneNumber = $('#Consultant').find('[id$=PrimaryTelephoneNumber_CommunicationValue]').val();;
        var primaryPhoneType = $('#Consultant').find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val();;

        if (ContactSearchResult.ValidateData()) {
            if (selectedState == '') {
                ContactSearchResult.ShowStateValidationMessage('#Consultant');
                ApplicationWizard.HighlightCurrentWizardStep($('.Consultant'));
            }
            else if (primaryPhoneNumber == '' || primaryPhoneType == '') {
                ContactSearchResult.ShowPrimaryPhoneNumberValidationMessage('#Consultant');
                ApplicationWizard.HighlightCurrentWizardStep($('.Consultant'));
            }
            else {
                WaterUseDescription.Initialize();
            }
        }
        else {
            ApplicationWizard.HighlightCurrentWizardStep($('.Consultant'));
        }
    },
    Back: function () {
        LandOwner.Initialize("back");
    }
});