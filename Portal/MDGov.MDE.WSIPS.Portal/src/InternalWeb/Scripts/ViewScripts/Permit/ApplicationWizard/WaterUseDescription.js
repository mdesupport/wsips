﻿/// <reference path="_permitReference.js" />

$.extend(WaterUseDescription , {
    Next: function () {
        if (this.ValidateData()) {

            //save and continue
            if ($('#PermitId').val() == "0") {
                ApplicationWizard.SaveData();
            }

            $('#waterUseDescription').hide();
            WaterUseCategoryAndType.Initialize();
        }
        else {
            $('.waterUseDescription').addClass('wizardStepActive');
        }
    },

    Back: function () {
        $('#waterUseDescription').hide();

        if (!global.isLandOwner && !global.isWaterUser) {
            Consultant.Initialize();
        }
        else if (!global.isLandOwner && global.isWaterUser) {
            LandOwner.Initialize();
        }
        else if (global.isLandOwner && !global.isWaterUser) {
            LandOwner.Initialize();
        }
        else if (global.isLandOwner && global.isWaterUser) {
            WaterUser.Initialize();
        }
    },

    Cancel: function () {
    }
});