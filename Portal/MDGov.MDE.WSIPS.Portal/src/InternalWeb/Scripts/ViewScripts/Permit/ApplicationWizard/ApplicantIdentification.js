﻿/// <reference path="ApplicationWizard.js" />
/// <reference path="InformationVerification.js" />

$('#ApplicantIdentification input:checkbox').change(function (e) {
    //Reset the value of the permittee when there is a change
    $('#PermitIssuedto').val('0');
    ApplicantIdentification.SetupOptions();
});

$("#PermitIssuedto").change(function () {
    ApplicantIdentification.DecideWhatToSearch();
});

$.extend(ApplicantIdentification, {
    Initialize: function () {
        ApplicationWizard.ClearSelectedWizardStep();
        //hide the back button for the current view
        $('#BackButton').hide();
        $('#NextButton').show();
        $('#SaveButton').hide();
        $('#ApplicantIdentification').show();
        global.currentStep = Steps.ApplicantIdentification;
        $('.ApplicantIdentification').addClass('wizardStepActive');

        this.SetupOptions();
    },

    Next: function () {
        //Validate the input before going the next screen
        var errorMessage = this.ValidateData();

        if (errorMessage.length == 0) {
            $('#ApplicantIdentification').hide();
            //Clear error message
            $('#MessageSummary').html('');
            // Go to next page. show the contact of the water user.
            WaterUser.Initialize();
        }
        else {
            $('.ApplicantIdentification').addClass('wizardStepActive');
            ApplicationWizard.DisplayErrorMessage(errorMessage);
        }
    },

    Cancel: function () {
    }
});