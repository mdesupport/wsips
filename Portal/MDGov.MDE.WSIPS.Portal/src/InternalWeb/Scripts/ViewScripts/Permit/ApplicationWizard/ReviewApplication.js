﻿/// <reference path="_permitReference.js" />

var ReviewApplication = {
    Initialize: function () {
        global.currentStep = Steps.ReviewApplication;
        $('#CurrentStep').val('10');

        //if they are editing submitted application, don't show the affirmation page -- hide next button.
        var permitStatusId = $('#PermitStatusId').val();

        if (permitStatusId == "0" || permitStatusId == "") {
            $('#NextButton').show();
        }
        else if (permitStatusId != "1") {
            $('#NextButton').hide();
        }

        $('.ReviewApplication').addClass('wizardStepActive');

        ReviewApplication.LoadSummary();
        ReviewApplication.LoadDocumentUploadPartialView("Permit", $("#PermitId").val(), $('#uploadDocument'), "Permit");
        $("#pdfMessage").html("");
        $('#reviewApplication').show();
        $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
    },

    Cancel: function () {
    },

    Next: function () {
        $('#reviewApplication').hide();
        Affirmation.Initialize();
    },

    Back: function () {
        $('#NextButton').show();
        $('#reviewApplication').hide();
        ApplicationWizard.IdentifyLocation.Initialize();
    },

    Print: function () {
        // Disable the message 
        window.onbeforeunload = null;

        var _url = Helper.GetBaseURL("Permit") + "Permit/CreatePdf/";
        var permitID = $("#PermitId").val();
        var _ids = global.SelectedWaterUseCategoryTypeList.toString();

        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", _url);

        // setting form target to a window named 'formresult'
        form.setAttribute("target", "formresult");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("name", "waterDetailsSelected");
        hiddenField.setAttribute("value", _ids);
        form.appendChild(hiddenField);

        var hiddenField2 = document.createElement("input");
        hiddenField2.setAttribute("name", "id");
        hiddenField2.setAttribute("value", $("#PermitId").val());
        form.appendChild(hiddenField2);

        document.body.appendChild(form);

        // creating the 'formresult' window with custom features prior to submitting the form
        window.open("", 'formresult', '');

        form.submit();
    },

    LoadDocumentUploadPartialView: function (refTable, refId, $container, controller) {
        var _url = Helper.GetBaseURL(controller);

        _url += "Upload/Document";

        $.ajax({
            url: _url,
            type: 'Get',
            async: false,
            cache: false,
            data: { refTable: refTable, refId: refId, permitStatusId: 1 },
            success: function (response) {
                $container.html(response);
                UploadDocument.InitializeVariables(controller);
                $("#uploadDashboardGrid").hideCol("permitStatus");
            }
        });
    },

    LoadSummary: function () {
        var _url = Helper.GetBaseURL("Permit") + "Permit/summary/";
        var permitId = $("#PermitId").val();
        var _ids = global.SelectedWaterUseCategoryTypeList.toString();

        $.ajax({
            url: _url,
            type: 'Get',
            async: false,
            data: { id: permitId, waterDetailsSelected: _ids },
            success: function (response) {
                $("#applicationSummary").html(response);
            }
        });
    }
};