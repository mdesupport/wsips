﻿/// <reference path="../../_references.js" />
/// <reference path="_permitReference.js" />

$(document).ready(function () {
    $.blockUI({ message: '<h4>Please wait, page loading...</h4>' });
    //pass in the current page sent from the server.
    ApplicationWizard.Initialize();

    //Load the water Use Catagory And Type page
    ApplicationWizard.LoadwaterUseCatagoryAndTypeView();

    // Load the water use detail page
    ApplicationWizard.LoadwaterUseDetailView();

    // Show a warning when the user presses the browser's back button or closes the browser
    window.onbeforeunload = function () { return "Warning - This will exit the application process and any unsaved data will be lost."; };

    return false;
});

var ContactType = {
    WaterUser: 1,
    LandOwner: 2,
    Consultant: 3
}

var Steps = {
    ApplicantIdentification: "ApplicantIdentification",
    ContactSearch: "ContactSearch",
    ContactSearchResult: "ContactSearchResult",
    WaterUseDescription: "WaterUseDescription",
    WaterUseCategoryAndType: "WaterUseCategoryAndType",
    WaterUseDetail: "WaterUseDetail",
    IdentifyLocation: "IdentifyLocation",
    WastewaterTreatmentAndDisposal: "WastewaterTreatmentAndDisposal",
    ReviewApplication: "ReviewApplication",
    Affirmation: "Affirmation"
}

var global = {
    currentStep: "",
    isWaterUser: 0,
    isLandOwner: 0,
    isConsultant: 0,
    SearchResult: null,
    waterUserContactFound: false,
    LandOwnerContactFound: false,
    ConsultantContactFound: false,
    CurrentlySearching: null,
    SelectedWaterUseCategoryTypeList: new Array(),
    isSaveAndResume: false,
    dataSaved: false,
    businessContactId: "",
    showWaterUseDetail: false,
    permittee: 0,
    ReloadPrivateWaterSupplierView: false,
    ReloadWasteWaterTreatmentAndDisposalPage: false,
    WaterUseCategoryAndTypeChanged: false
};

var ApplicationWizard = {

    Initialize: function () {
        ApplicationWizard.ClearSelectedWizardStep();

        ApplicationWizard.AttachDocumentsClick();

        $("[id$=TelephoneNumber_CommunicationValue]").each(function () {
            $(this).mask("999-999-9999");
            // if it has value, disable editing 
            if ($(this).val().length > 0) {
                $(this).attr('readonly', true);
            }
        });

        $("[id$=EmailAddress_CommunicationValue]").each(function () {
            // if it has value, disable editing 
            if ($(this).val().length > 0) {
                $(this).attr('readonly', true);
            }
        });

        //Make the first step visible
        if ($('#PermitId').val() == "0") {
            ApplicantIdentification.Initialize();
            $("#attachDocumentLink").hide();
        }
        else {
            //if they are editing submitted application, don't show the affirmation page -- hide next button.
            var permitStatusId = $('#PermitStatusId').val();

            if (permitStatusId != "1") {
                $('.submitApplication').hide();
            }

            global.dataSaved == true;
            ApplicantIdentification.AssignData();
            ApplicantIdentification.SetupOptions();
            ApplicationWizard.InitializeGlobalVariable();
            var currentStep = $('#CurrentStep').val();
            $("#attachDocumentLink").show();
            switch (currentStep) {
                case '5':
                    WaterUseDescription.Initialize();
                    break;
                case '6':
                    WaterUseCategoryAndType.Initialize();
                    break;
                case '7':
                    WaterUseDetail.Initialize();
                    break;
                case '8':
                    WastewaterTreatmentAndDisposal.Initialize();
                    break;
                case '9':
                    ApplicationWizard.IdentifyLocation.Initialize();
                    break;
                case '10':
                    ReviewApplication.Initialize();
                    break;
                case '11':
                    Affirmation.Initialize();
                    break;
                default:
                    WaterUseDescription.Initialize();
            };
        }
    },

    Next: function () {

        ApplicationWizard.ClearSelectedWizardStep();

        if (global.currentStep == Steps.ApplicantIdentification) {
            ApplicantIdentification.Next();
        }
        else if (global.currentStep == Steps.ContactSearchResult) {
            ContactSearchResult.Next();
        }
        else if (global.currentStep == Steps.WaterUseDescription) {
            WaterUseDescription.Next();
        }
        else if (global.currentStep == Steps.WaterUseCategoryAndType) {
            WaterUseCategoryAndType.Next();
        }
        else if (global.currentStep == Steps.WaterUseDetail) {
            WaterUseDetail.Next();
        }
        else if (global.currentStep == Steps.WastewaterTreatmentAndDisposal) {
            WastewaterTreatmentAndDisposal.Next();
        }
        else if (global.currentStep == Steps.IdentifyLocation) {
            this.IdentifyLocation.Next();
        }
        else if (global.currentStep == Steps.ReviewApplication) {
            ReviewApplication.Next();
        }

    },

    Back: function () {

        ApplicationWizard.ClearSelectedWizardStep();

        if (global.currentStep == Steps.ContactSearch) {
            ContactSearch.Back();
        }
        else if (global.currentStep == Steps.ContactSearchResult) {
            ContactSearchResult.Back();
        }
        else if (global.currentStep == Steps.WaterUseDescription) {
            WaterUseDescription.Back();
        }
        else if (global.currentStep == Steps.WaterUseCategoryAndType) {
            WaterUseCategoryAndType.Back();
        }
        else if (global.currentStep == Steps.WaterUseDetail) {
            WaterUseDetail.Back();
        }
        else if (global.currentStep == Steps.WastewaterTreatmentAndDisposal) {
            WastewaterTreatmentAndDisposal.Back();
        }
        else if (global.currentStep == Steps.IdentifyLocation) {
            this.IdentifyLocation.Back();
        }
        else if (global.currentStep == Steps.ReviewApplication) {
            ReviewApplication.Back();
        }
        else if (global.currentStep == Steps.Affirmation) {
            Affirmation.Back();
        }
    },

    Cancel: function () {
        if (confirm("Warning: Canceling this application will delete the application in its entirety and your changes will be lost. \r \n Are you sure you want to cancel this application? ")) {
            // Cancel the application and remove any associated data
            if (global.currentStep == Steps.ApplicantIdentification) {
                ApplicantIdentification.Cancel();
            }
            else if (global.currentStep == Steps.ContactSearch) {
                ContactSearch.Cancel();
            }
            else if (global.currentStep == Steps.ContactSearchResult) {
                ContactSearchResult.Cancel();
            }
            else if (global.currentStep == Steps.WaterUseDescription) {
                WaterUseDescription.Cancel();
            }
            else if (global.currentStep == Steps.WaterUseCategoryAndType) {
                WaterUseCategoryAndType.Cancel();
            }
            else if (global.currentStep == Steps.WaterUseDetail) {
                WaterUseDetail.Cancel();
            }
            else if (global.currentStep == Steps.WastewaterTreatmentAndDisposal) {
                WastewaterTreatmentAndDisposal.Cancel();
            }
            else if (global.currentStep == Steps.IdentifyLocation) {
                this.IdentifyLocation.Cancel();
            }
            else if (global.currentStep == Steps.ReviewApplication) {
                ReviewApplication.Cancel();
            }
            else if (global.currentStep == Steps.Affirmation) {
                Affirmation.Cancel();
            }

            // Delete any records that were created
            $.ajax({
                url: urls.cancelUrl,
                type: 'POST',
                data: { permitId: $('#PermitId').val() },
                success: function () {
                    // Remove the warning for page unload
                    window.onbeforeunload = null;

                    // Redirect
                    window.location.href = Helper.GetBaseURL("Permitting");
                }
            });
        }
    },

    CollectSelectedWaterUseCategoryType: function () {
        // Get all selected values for water use category and type
        var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
        var commaSeparatedValues = _.reduce(selectedValues, function (memo, num) { return memo + num + ','; }, '');
        $('#SelectedWaterUsePurposeIds').val(commaSeparatedValues);
    },

    SaveAndResumeLater: function () {
        ApplicationWizard.CollectSelectedWaterUseCategoryType();
        //Save data -- submit form
        global.isSaveAndResume = true;
        ApplicationWizard.SaveData();
    },

    SaveData: function () {
        ApplicationWizard.CollectSelectedWaterUseCategoryType();
        if ($('#wizardForm').valid()) {
            $.blockUI({ message: '<h4>Please wait, saving changes...</h4>' });
            $('#wizardForm').submit();
        }
    },

    SaveCallBack: function (response) {
        if (response.success) {
            if (global.isSaveAndResume) {
                $.unblockUI();
                $("#saveMessage").html('This application has been saved.').dialog({
                    modal: true,
                    position: "center",
                    width: 350,
                    buttons: {
                        "OK": function () {
                            // Remove the warning for page unload
                            window.onbeforeunload = null;

                            // Redirect
                            window.location.href = Helper.GetBaseURL("Permitting");
                        }
                    }
                });
            }
            else {
                //continue permit application
                $.unblockUI();
                global.dataSaved = true;
                $('#PermitId').val(response.permitId);
                $('#PermitStatusId').val(response.permitStatusId)
                $('#Contacts_0__Id').val(response.waterUserContactId);
                $('#Contacts_1__Id').val(response.landOwnerContactId);
                $('#Contacts_2__Id').val(response.consultantContactId);
                $("#attachDocumentLink").show();
            }
        }
        else {
            $("#saveMessage").html('An error occured while attempting to save your application. Please contact the WSIPS system administrator.').dialog({
                modal: true,
                position: "center",
                width: 400
            });
        }
    },

    DisplayErrorMessage: function (errorMessage) {
        $('#MessageSummary').html(errorMessage).show();
    },

    LoadwaterUseCatagoryAndTypeView: function () {
        WaterUseCategoryAndType.LoadwaterUseCatagoryAndTypeView();

        return false;
    },

    LoadwaterUseDetailView: function () {
        var permitId = $('#PermitId').val();
        WaterUseDetail.LoadwaterUseDetailView(permitId);

        return false;
    },

    HighlightCurrentWizardStep: function ($div) {
        $div.show();
        $div.addClass('wizardStepActive');
    },

    ClearSelectedWizardStep: function () {
        $("#wizardLeft").children().removeClass("wizardStepActive");
    },

    InitializeGlobalVariable: function () {
        // When a permit is resumed, we assume that all contact types are found. 
        global.ConsultantContactFound = true;
        global.waterUserContactFound = true;
        global.LandOwnerContactFound = true;
    },

    AttachDocumentsClick: function () {
        $("#attachDocumentLink").click(function () {
            var _url = Helper.GetBaseURL("Permitting");

            _url += "Upload/Document";
            $.ajax({
                url: _url,
                type: 'Get',
                async: false,
                cache: false,
                data: { refTable: "Permit", refId: $('#PermitId').val(), permitStatusId: 1 },
                success: function (response) {
                    $("#attachedDocument").html(response);
                    $("#applicationUploadMsg").show();
                    UploadDocument.InitializeVariables("Permitting");
                }
            });

            $("#attachedDocument").dialog({
                modal: true,
                position: "center",
                height: 390,
                width: 800,
                resizable: false,
                draggable: false,
                open: function (event, ui) {
                    //style upload new files button
                    $("input[type=button]").button();
                    //center the upload button in pop-up
                    $("#uploadButtonParent").addClass("centerText");
                    //force inner (dialog) scrolling on jqgrid
                    $("#uploadDashboardGridContainer").addClass("dialogUploadGridContainer");
                    $("#uploadDashboardGrid").jqGrid("fixGridWidthWithoutParentScroll");

                    //display exmaples text
                    $("#uploadExamples").show();
                }
            });

            //if upload button gets clicked, hide its dialog
            //when uploadDialog is closed, show the hidden dialog again
            $('#uploadButton').click(function () { $('#attachedDocument').dialog('close'); })
            $('#uploadDialog').bind('dialogclose', function () {
                $('#uploadConfirmation').dialog({
                    modal: true,
                    resizable: false,
                    draggable: false,
                    close: function () {
                        $(this).dialog('destroy');
                    },
                    buttons: [
                        {
                            text: 'Yes',
                            click: function () {
                                $('#uploadButton').click();
                                $(this).dialog('close');
                            }
                        },
                        {
                            text: 'No',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    ]
                });
            });
        })
    }
};


function localResize() {
    //alert("local resize")
    //reset scrolwrap width to full width
    //$.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();

    //search results does not have custom scrollbars
    if (global.currentStep == Steps.ContactSearchResult) {
        $("#contactGrid").jqGrid("fixGridWidthWithoutParentScroll");
    }

    if ($("#uploadDashboardGrid").jqGrid('getGridParam', 'records') != 0) {
        //$("#uploadDashboardGrid").jqGrid("fixGridWidth");
        $("#uploadDashboardGrid").jqGrid("fixGridWidthWithoutParentScroll");
        $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
    }
}