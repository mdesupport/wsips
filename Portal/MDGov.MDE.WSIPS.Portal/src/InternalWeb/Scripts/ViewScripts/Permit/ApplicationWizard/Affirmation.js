﻿/// <reference path="_permitReference.js" />

var Affirmation = {
    Initialize: function () {
        global.currentStep = Steps.Affirmation;
        $('#CurrentStep').val('11');
        $('.submitApplication').addClass('wizardStepActive');
        $('#affirmation').show();
        $("#affirmationDate").text("").text(Affirmation.SubmitTime())
        $('#NextButton').hide();
        $('#SubmitButton').show();
    },

    Back: function () {
        $('#affirmation').hide();
        ReviewApplication.Initialize();
        $('#NextButton').show();
        $('#SubmitButton').hide();
    },

    Cancel: function () {
    },

    SubmitTime: function () {
        var currentTime = new Date()
        var theDate = (currentTime.getMonth() + 1) + "/" + currentTime.getDate() + "/" + currentTime.getFullYear();
        var hours = currentTime.getHours()
        var minutes = currentTime.getMinutes()
        var secs = currentTime.getSeconds()

        var suffix = "AM";
        if (hours >= 12) {
            suffix = "PM";
            hours = hours - 12;
        }
        if (hours == 0) {
            hours = 12;
        }

        if (minutes < 10)
            minutes = "0" + minutes

        if (secs < 10)
            secs = "0" + secs

        return theDate + " " + hours + ":" + minutes + ":" + secs + " " + suffix;
    },
    
    Submit: function () {
        var _url = Helper.GetBaseURL("Permit") + "Permit/Affirmation/";
        var permitID = $("#PermitId").val();
        var submittedBy = $("#affirmationName").val();
        var submittedDate = $("#affirmationDate").text();

        if ($("#affirmationName").val().length == 0) {
            $("#affirmationMsg").html("Please input applicant's name").css("color", "red");
        }
        else {
            $.ajax({
                url: _url,
                type: 'POST',
                async: false,
                data: { id: permitID, submittedBy: submittedBy, submittedDate: submittedDate },
                success: function (response) {
                    //$("#affirmationMsg").html("").css("color", "inherit").html("Successfuly submitted by " + $("#affirmationName").val() + " on " + $("#affirmationDate").text())
                    if (!response.Success) {
                        $("#affirmationDialog").html('These was a problem submitting the application. Please contact the Administrator.').dialog({
                            modal: true,
                            open: function () {
                            },
                            position: "center",
                            width: 500,
                            buttons: {
                                "OK": function () {
                                    $("#affirmationDialog").dialog('close');
                                }
                            }
                        })
                    }
                    else {
                        $("#affirmationDialog").html('<p>Thank You! This application has been successfully submitted.</p><p>' + response.Message + '</p>').dialog({
                            modal: true,
                            open: function () {
                                $(this).prev("div").find("a").hide()
                            },
                            position: "center",
                            width: 500,
                            buttons: {
                                "OK": function () {
                                    // Disable the message 
                                    window.onbeforeunload = null;
                                    window.location.href = Helper.GetBaseURL("Permitting");
                                }
                            }
                        })
                    }

                    $("#affirmationButton").attr("disabled", "true");
                    $("#SubmitButton").attr("disabled", "true");
                }
            });
        }
    }
};