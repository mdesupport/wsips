﻿var Checklist = {
    Initialize: function () {
        if ($('#CategoryId').val() == 6 || $('#CategoryId').val() == 7) {
            $('input[type=checkbox]').prop('disabled', true);
            $('.permitstatusbutton,.finalizebutton').prop('disabled', true).addClass('disabled ui-button-disabled ui-state-disabled')
        }
        $('input[type=checkbox]').checkbox();
        $('#checklistDiv input[type=checkbox]').unbind('change');
        $('#checklistDiv input[type=checkbox]').change(function () {
            var id = $(this).prop('id'),
                className = $(this).prop('class'),
                attr = 'checked',
                values,
                data;


            $('.' + className + '[id!="' + id + '"]').each(function () {
                var id = $(this).prop("id");
                $(this).prop(attr, false);
                $('#' + id + '_chkBx div').removeClass(attr);
            });

            values = id.split('_');

            if ($(this).is(':checked')) {
                $('#' + id + '_chkBx div').addClass(attr);

                data = {
                    Id: values[1],
                    CheckListQuestionId: values[2],
                    PermitId: values[3],
                    AnswerId: values[4]
                };
            } else {
                $('#' + id + '_chkBx div').removeClass(attr);

                data = {
                    Id: values[1],
                    CheckListQuestionId: values[2],
                    PermitId: values[3],
                    AnswerId: 0
                };
            }

            $.ajax({
                url: ChecklistSettings.UpdateAnswerUrl,
                type: 'POST',
                data: data,
                success: function () {
                    var selector = $('div.checklistquestion'),
                        len = selector.length,
                        i = 0,
                        bAnswered;

                    selector.each(function () {
                        bAnswered = false;
                        $(this).children('input[type=checkbox]').each(function () {
                            if ($(this).is(':checked')) {
                                bAnswered = true;
                                i++;
                                return false;
                            }
                        });
                    });

                    if (i == len) {
                        if ($("#SelectedStatusId").val() == 59 || $("#SelectedStatusId").val() == 62 || $("#SelectedStatusId").val() == 65 || $("#SelectedStatusId").val() == 45) {
                            //check if it is pendding exemption
                            if ($("#SelectedStatusId").val() == 45) {
                                _FinalizePermit.EnableDisableFinalizeButtons(true, true);
                            }
                            else {
                                _FinalizePermit.EnableDisableFinalizeButtons(true, false);
                            }

                            _FinalizePermit.ToggleApproveDenyButtons(true);

                        } else {
                            //Enable the Mark as complete button
                            $('#MarkAsCompleteSpan').show();
                            $('#MarkAsIncompleteSpan').hide();
                            $('#MarkAsComplete').removeClass('disabled');
                        }
                    }
                    else {
                        $.ajax({
                            url: ChecklistSettings.UpdateStatusUrl,
                            type: 'POST',
                            data: { permitId: $('#PermitId').val(), permitStatusId: $('#SelectedStatusId').val(), completed: false },
                        });


                        if ($("#SelectedStatusId").val() == 59 || $("#SelectedStatusId").val() == 62 || $("#SelectedStatusId").val() == 65 || $("#SelectedStatusId").val() == 45) {
                            //check if it is pendding exemption
                            if ($("#SelectedStatusId").val() == 45) {
                                _FinalizePermit.EnableDisableFinalizeButtons(false, true);
                            }
                            else {
                                _FinalizePermit.EnableDisableFinalizeButtons(false, false);
                            }

                            _FinalizePermit.ToggleApproveDenyButtons(true);

                        } else {
                            //Disable the Mark as complete button
                            $('#CompletedByAndDate').html('');
                            $('#MarkAsCompleteSpan').show();
                            $('#MarkAsIncompleteSpan').hide();
                            $('#MarkAsComplete').addClass('disabled');
                        }
                    }
                }
            });
        });
    }
}