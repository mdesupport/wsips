﻿var WastewaterTreatmentAndDisposal = {

    Initialize: function () {

        var selectedWaterUseCategoryType = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });

        if (_.any(selectedWaterUseCategoryType, function (value) { return parseInt(value) > 11; })) {

            global.currentStep = Steps.WastewaterTreatmentAndDisposal;
            $('#CurrentStep').val('8');
            $('.wastewaterTreatmentDisposal').addClass('wizardStepActive');
            $('#SaveButton').show();

            //WastewaterTreatmentAndDisposal.SetupView();

            // Remove the auto-complete feature from the dropdown list
            WastewaterTreatmentAndDisposal.AdjustDropdown();

            $('.wastewaterTreatmentDisposal').show();
            $('#WastewaterTreatmentAndDisposal').show();
        }
        else {
            $('#WastewaterTreatmentAndDisposal').hide();
            // Save and Skip to next step
            ApplicationWizard.SaveData();
            ApplicationWizard.IdentifyLocation.Initialize();
        }
    },

    ValidateData: function () {
        return $('#wizardForm').valid();
    },

    SetupView: function ($this) {
        if ($this.is(':checked')) {
            $this.parent().parent().find('.wasteWaterTreatmentInfoContainer').show();
            //$('#wasteWaterTreatmentInfoContainer').show();
        }
        else {
            $this.parent().parent().find('.wasteWaterTreatmentInfoContainer').hide();
        }

        //if ($('#WastewaterTreatmentDisposalForm_WaterDispersementTypeId option:selected').text() == 'Groundwater - Other') {
        //    $(".natureOfGroundwaterDischarge").show();
        //}
        //else {
        //    $(".natureOfGroundwaterDischarge").hide();
        //}
    },

    AdjustDropdown: function () {
        $(".WastewaterTreatmentDisposalFormDropDown").show()//$('#WastewaterTreatmentDisposalForm_WaterDispersementTypeId').show();
        $('.wasteWaterTreatmentInfoContainer .ui-combobox').detach();
    }
};