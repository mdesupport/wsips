﻿/// <reference path="ApplicationWizard.js" />

var ContactSearch = {
    GridDataUrl: null,
    IsContactSearchGeneric: false,
    SelectedContact: null,
    CallBack: null,
    Initialize: function (currentlySearching, callback) {
        ContactSearch.CallBack = callback;
     
        $('#BackButton').show();

        if (currentlySearching != null) {
            if (currentlySearching == "Generic") {
                ContactSearch.SearchGeneric();
            }
            else if (global.CurrentlySearching == "WaterUser") {
                ContactSearch.SearchWaterUser();
            }
            else if (global.CurrentlySearching == "LandOwner") {
                ContactSearch.SearchLandowner();
            }
            else if (global.CurrentlySearching == "Consultant") {
                ContactSearch.SearchConsultant();
            }
        }
        else {
            if (!global.waterUserContactFound) {
                ContactSearch.SearchWaterUser();
            }
            else if (!global.LandOwnerContactFound) {
                ContactSearch.SearchLandowner();
            }
            else if (!global.ConsultantContactFound) {
                ContactSearch.SearchConsultant();
            }
        }
    },

    Search: function () {
        var _firstName = $('#SearchFirstName').val();
        var _lastName = $('#SearchLastName').val();
        var _businessName = $('#SearchBusinessName').val();
        var _emailAddress = $('#SearchEmailAddress').val();
        var _permitNumber = $('#SearchPermitName').val();

        var _grid = $("#contactGrid");

        if (_firstName == "" && _lastName == "" && _businessName == "" && _emailAddress == "" && _permitNumber == "") {
            alert('Please enter search criteria');
        }
        else {
            if (_grid == null) {
                _grid = $("#contactGrid");
            }
            else {
                _grid.GridUnload();
                _grid = $("#contactGrid");
            }

            _grid.jqGrid({
                colNames: ['Contact Id', 'First Name', 'Middle Name', 'Last Name', 'Business Name', 'Address', 'Email Address', ''],
                colModel: [
                    { name: 'ContactId', index: 'ContactId', width: 12.5 },
                    { name: 'FirstName', index: 'FirstName', width: 12.5 },
                    { name: 'MiddleName', index: 'MiddleName', width: 10 },
                    { name: 'LastName', index: 'LastName', width: 12.5 },
                    { name: 'BusinessName', index: 'BusinessName', width: 20 },
                    { name: 'Address1', width: 25 },
                    { name: 'EmailAddress', width: 15, sortable: false },
                    { name: 'ContactId', formatter: ContactSearch.SelectLinkFormatter, width: 5, sortable: false },
                ],
                loadComplete: function (response) {
                    if (!_.isNull(response)) {
                        global.SearchResult = response.ActualResult;
                        //_grid.jqGrid("fixGridWidth");
                        _grid.jqGrid("fixGridWidthWithoutParentScroll");
                    }
                },
                autowidth: true,
                shrinkToFit: true,
                gridComplete: function (data) {
                    $(".selectContact").click(function () {   
                        var selectedContactId = $(this).attr('data-contactid');
                        $('#contactSearchResult').dialog('close');

                        if (ContactSearch.IsContactSearchGeneric) {
                            global.GenericUserContactFound = true;
                            ContactSearch.SelectedContact = _.find(global.SearchResult, function (contact) { return contact.Id == selectedContactId; });

                            if ($.isFunction(ContactSearch.CallBack)) {
                                ContactSearch.CallBack(selectedContactId);
                            }
                        }
                        else {
                            ContactSearchResult.DisplayContact(selectedContactId);
                        }
                    });

                    //Show the result
                    var recs = parseInt($("#contactGrid").getGridParam("records"), 10);
                    if (isNaN(recs) || recs == 0) {
                        ContactSearchResult.Initialize(0);
                    }
                    else {
                        ContactSearchResult.Initialize(recs);
                    }
                },
                pager: $('#pager'),
                rowNum: 20,
                forceFit: true,
                height: 300,
                url: ContactSearch.GridDataUrl + '?SearchFirstName=' + _firstName + '&SearchLastName=' + _lastName + '&SearchBusinessName=' + _businessName + '&SearchEmailAddress=' + _emailAddress + '&SearchPermitName=' + _permitNumber,
                page: 1
            });
        }
    },

    Modify: function () {
        $('#contactSearchResult').dialog('close');

        if (global.CurrentlySearching == "WaterUser") {
            ContactSearch.SearchWaterUser();
        }
        else if (global.CurrentlySearching == "LandOwner") {
            ContactSearch.SearchLandowner();
        }
        else if (global.CurrentlySearching == "Consultant") {
            ContactSearch.SearchConsultant();
        }
        else {
            ContactSearch.SearchGeneric();
        }
    },

    SelectLinkFormatter: function (cellvalue, options, rowObject) {
        return '<span class="spanLink selectContact" data-contactid="' + cellvalue + '">Select</span>';
    },

    SearchWaterUser: function () {
        global.CurrentlySearching = "WaterUser";
        ContactSearch.IsContactSearchGeneric = false;
        $('.searchTitle').hide();
        $('.searchTitle.waterUser').show();
        ContactSearchDialogBox.Show($('#contactSearch'), 350, 700, "Search for Water User");
    },

    SearchLandowner: function () {
        global.CurrentlySearching = "LandOwner";
        ContactSearch.IsContactSearchGeneric = false;
        $('.searchTitle').hide();
        $('.searchTitle.landOwner').show();
        ContactSearchDialogBox.Show($('#contactSearch'), 350, 700, "Search for Land Owner");
    },

    SearchConsultant: function () {
        global.CurrentlySearching = "Consultant";
        ContactSearch.IsContactSearchGeneric = false;
        $('.searchTitle').hide();
        $('.searchTitle.consultant').show();
        ContactSearchDialogBox.Show($('#contactSearch'), 350, 700, "Search for Consultant");
    },

    SearchGeneric: function () {
        ContactSearch.IsContactSearchGeneric = true;
        ContactSearchDialogBox.Show($('#contactSearch'), 350, 700, "Search for Contact");
    }
};

var ContactSearchDialogBox = {
    Show: function ($this, $height, $width, $title) {
        //Clear search criteria 
        this.ClearSearchCriteria();

        $this.dialog({
            title: $title,
            modal: true,
            height: $height,
            width: $width,
            draggable: false,
            resizable: false,
            open: function () {
                window.styleDialogInputs();

                //attach ENTER key press event for dialog
                $(this).unbind('keydown');
                $(this).keydown(function (e) {
                    if (e.which == 13) {
                        $(this).focus();
                        ContactSearch.Search();
                    }
                });
            },
            close: function () {
                $(this).dialog('destroy');
            },
            buttons: [
                {
                    text: "Search", click: function () {
                        ContactSearch.Search();
                    }
                },
                {
                    text: "Cancel", click: function () {
                        $this.dialog('close');
                    }
                }
            ]
        });
    },

    ClearSearchCriteria: function () {
        $('#SearchFirstName').val('');
        $('#SearchLastName').val('');
        $('#SearchBusinessName').val('');
        $('#SearchEmailAddress').val('');
        $('#SearchPermitName').val('');
    }
};