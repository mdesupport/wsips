﻿var WaterUseDescription = {
    PreviousView: "",

    Initialize: function () {
        ApplicationWizard.ClearSelectedWizardStep();
        this.PreviousView = global.currentStep;
        global.currentStep = Steps.WaterUseDescription;
        $('#CurrentStep').val('5');
        $('.waterUseDescription').addClass('wizardStepActive');
        $('#SearchedContacts').hide();
        $('#SaveButton').show();
        $('#waterUseDescription').show();
    },

    ValidateData: function () {
        //validate the form and if there is validation error don't pass to next step
        return $('#wizardForm').valid();
    }
};