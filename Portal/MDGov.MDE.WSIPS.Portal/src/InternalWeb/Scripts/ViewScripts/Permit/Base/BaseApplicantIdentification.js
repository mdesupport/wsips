﻿var ApplicantIdentification = {
    SetupOptions: function () {
        ApplicantIdentification.AssignData();

        var _permittee = $('#PermitIssuedto').val();

        if (_.isNull(_permittee)) {
            _permittee = $('#permitIssuedToFromDB').val();
        }

        if (global.isWaterUser == 1 && global.isLandOwner == 1) {
            global.waterUserContactFound = false;
            global.LandOwnerContactFound = true;
            global.ConsultantContactFound = true;

            $('.WaterUser').show();
            $('.LandOwner').hide();
            $('.Consultant').hide();

            $('#consultant').hide();
            $("#permitOwner").hide();
        }
        else if (global.isWaterUser == 0 && global.isLandOwner == 1) {
            // The applicant is the land owner, hence the land owner contact is known
            global.LandOwnerContactFound = false;
            global.waterUserContactFound = false;
            global.ConsultantContactFound = true; // not needed

            $('.LandOwner').show();
            $('.WaterUser').show();
            $('.Consultant').hide();

            $('#consultant').hide();
            $("#permitOwner").show();
            ApplicantIdentification.PopulateSelectList(true, false, false, _permittee);
        }
        else if (global.isWaterUser == 1 && global.isLandOwner == 0) {
            // The applicant is the water user, hence the water user contact is known
            global.waterUserContactFound = false;
            global.LandOwnerContactFound = false;
            global.ConsultantContactFound = true; // not needed

            $('.LandOwner').show();
            $('.WaterUser').show();
            $('.Consultant').hide();

            $('#consultant').hide();
            $("#permitOwner").show();
            ApplicantIdentification.PopulateSelectList(false, true, false, _permittee);
        }
        else if (global.isWaterUser == 0 && global.isLandOwner == 0) {
            // The applicant is the consultant, hence the contact of the water user and land owner needs to be searched.
            global.waterUserContactFound = false;
            global.LandOwnerContactFound = false;
            global.ConsultantContactFound = false;

            $('.LandOwner').show();
            $('.WaterUser').show();
            $('.Consultant').show();

            $('#consultant').show();
            $("#permitOwner").show();
            ApplicantIdentification.PopulateSelectList(false, false, true, _permittee);
        }
    },

    ValidateData: function () {
        var licenseIssuedTo = $("#PermitIssuedto").val();

        if (global.isWaterUser == 0 && global.isLandOwner == 0 && global.isConsultant == 0) {
            return "Please identify yourself";
        }
        else if (!(global.isWaterUser == 1 && global.isLandOwner == 1) && (_.isUndefined(licenseIssuedTo) || licenseIssuedTo == "0")) {
            return "Please identify to whom the permit will be issued";
        }
        else {
            return "";
        }
    },

    AssignData: function () {
        global.isWaterUser = $('#IsWaterUser').is(':checked') == true ? 1 : 0;
        global.isLandOwner = $('#IsLandOwner').is(':checked') == true ? 1 : 0;
        global.isConsultant = $('#IsConsultant').is(':checked') == true ? 1 : 0;
    },

    DecideWhatToSearch: function () {
        global.permittee = $("#PermitIssuedto").val();
        if (global.permittee == "4" || (global.isWaterUser == 1 && global.isLandOwner == 1)) {
            $('.LandOwner').hide();
            global.LandOwnerContactFound = true;
        }
        else {
            $('.LandOwner').show();
            global.LandOwnerContactFound = false;
        }
    },

    PopulateSelectList: function (optionOne, optionTwo, optionThree, selectedValue) {
        var html = "";
        $('#PermitIssuedto').html('');

        if (optionOne) {
            html = '<option value="0">Select</option>' +
            '<option value="2">The water user</option> ' +
            '<option value="3">The land owner</option>';

        }
        else if (optionTwo) {
            html = '<option value="0">Select</option>' +
             '<option value="2">The water user</option> ' +
             '<option value="3">The land owner</option>';
        }
        else if (optionThree) {
            html = '<option value="0">Select</option>' +
             '<option value="2">The water user</option> ' +
             '<option value="3">The land owner</option>' +
             '<option value="4">Both, as the water user and land owner are one and same</option>';
        }

        if (!_.isUndefined(selectedValue) && !_.isNull(selectedValue)) {
            $('#PermitIssuedto').html(html).val(selectedValue).change();
        }
        else {
            $('#PermitIssuedto').html(html).val('0').change();
        }
    }
};