﻿///<reference path="../../esri.arcgis.jsapi-3.4.min.js" />
///<reference path="~/Scripts/Helper.js"/>

var gisServicesHost = $('#GISServicesHost').val(),
    gisServicesDirectory = $('#GISServicesDirectory').val();

var map,
    basemap,
    basemapTopo,
    basemapImagery,
    overlaysLayer,
    currentPermitPoly,
    permitLocationsLayer,
    groundwaterWithdrawalPointsLayer,
    surfaceWaterWithdrawalPointsLayer,
    activePermitLocationGraphicsLayer,
    activeGroundwaterWithdrawalPointsGraphicsLayer,
    activeSurfaceWaterWithdrawalPointsGraphicsLayer,
    spatialFilterOverlayGraphicsLayer,
    locatorService,
    drawToolbar,
    geometrySvc,
    selectedPermitIds;

dojo.require("esri.map");
dojo.require("esri.layers.FeatureLayer");
dojo.require("esri.layers.graphics");
dojo.require("esri.tasks.geometry");
dojo.require("esri.tasks.locator");
dojo.require("esri.tasks.PrintTask");
dojo.require("esri.tasks.query");
dojo.require("esri.toolbars.draw");

var PrintMap = function (templateString) {
    var printUrl = gisServicesHost + "/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";
    var printTask = new esri.tasks.PrintTask(printUrl);
    var params = new esri.tasks.PrintParameters();
    var template = new esri.tasks.PrintTemplate();
    params.map = map;
    template.layout = templateString;
    template.preserveScale = true;
    params.template = template;
    printTask.execute(params, printResult, printError);
};

var printResult = function (result) {
    $('#printtemplates').prop('value', 'Layout');
    $('#printtemplates').prop('disabled', false);
    window.open(result.url);
};

var printError = function (result) {
    alert(result);
    $('#printtemplates').prop('value', 'Layout');
    $('#printtemplates').prop('disabled', false);
};

var DrawType = {
    PermitDistance: 1,
    PermitFeature: 2
};

var Dashboard = {
    ExportToExcel: function () {
        var url = exportToExcelAction;

        $.ajax({
            url: url,
            type: 'Get',
            data: {},
            cache: false,
            success: function (response) {
                if (response.ReportParameterId != null) {
                    window.open(response.Url);
                }
            }
        });
    }
};

function FilterPermitStatuses() {
    var categoryIds = $('#PermitCategoryFilter_SelectedPermitCategoryIds').val();

    if (categoryIds && categoryIds.length == 1 && categoryIds[0] > 0) {
        var categoryId = categoryIds[0];
        if (!selectedPermitStatusId || !selectedPermitStatusId.length) {
            selectedPermitStatusId = -1;
        }

        $.ajax({
            url: getPermitStatusesAction,
            type: 'get',
            data: { categoryId: categoryId },
            success: function (permitStatuses) {
                var a = [];
                var exists = false;
                for (i in permitStatuses) {
                    var permitStatus = permitStatuses[i];
                    if (permitStatus.PermitCategoryId == categoryId) {
                        if (permitStatus.Id == selectedPermitStatusId) {
                            exists = true;
                        }

                        a[i] = '<option value="' + permitStatus.Id + '">' + permitStatus.Description + '</option>';
                    }
                }

                if (exists) {
                    $('#PermitStatusFilter_SelectedPermitStatusId').html('<option value="0">Display All</option>' + a.join('')).val(selectedPermitStatusId).change();
                    selectedPermitStatusId = null;
                }
                else {
                    $('#PermitStatusFilter_SelectedPermitStatusId').html('<option value="0">Display All</option>' + a.join('')).val(0).change();
                }
            }
        });
    }
    else {
        $('#PermitStatusFilter_SelectedPermitStatusId').html('<option value="0">Display All</option>').val(0).change();
    }
}

$(function () {
    //enable editing the dates
    $('#PermitIssuanceDateFilter_StartDate').mask("99/99/9999");
    $('#PermitIssuanceDateFilter_EndDate').mask("99/99/9999");
    $('#PermitExpirationDateFilter_StartDate').mask("99/99/9999");
    $('#PermitExpirationDateFilter_EndDate').mask("99/99/9999");

    FilterPermitStatuses();
    ToggleDashboardLettersLink();
    $("#dashboardGrid").jqGrid({
        ajaxGridOptions:
        {
            dataFilter: function (data, dataType) { // Preprocess the data
                if (data.length && dataType == "json") {
                    var parsedData;

                    try {
                        parsedData = JSON.parse(data);
                    } catch (err) {
                        location.reload();
                    }

                    //if (parsedData.PermitIds) {
                    PopulateMap(parsedData.PermitIds, parsedData.SpatialFilterFeature);
                    //}

                    return JSON.stringify(parsedData.GridModel);
                }
            }
        },
        colModel: [
            { name: 'PermitName', index: 'permitname', align: 'center', width: 105, fixed: true, formatter: permitNumberFormatter },
            { name: 'CountyCode', index: 'county', align: 'center', width: 50 },
            { name: 'PermitteeName', index: 'permittee', width: 150 },
            { name: 'PermitCategory', index: 'permitcategory', width: 120 },
            { name: 'PermitStatus', index: 'permitstatus', align: 'center', width: 150 },
            { name: 'IsLargePermit', index: 'islarge', align: 'center', width: 40, formatter: booleanYesNoFormatter },
            { name: 'RequiresAdvertising', index: 'reqadvertising', align: 'center', width: 30, formatter: booleanYesNoFormatter },
            { name: 'DaysPending', index: 'dayspending', align: 'center', width: 50 },
            { name: 'ProjectManagerName', index: 'projectmanager', width: 100 },
            { name: 'Date', index: 'date', align: 'center', width: 65, formatter: 'date' },
            { name: 'ShowMap', width: 40, align: 'center', sortable: false, formatter: showMapLinkFormatter }
        ],
        colNames: ['Permit No.', 'County', 'Permittee', 'Category', 'Status', 'Large', 'Adv', 'Pending', 'Project Manager', 'Date', ''],
        afterInsertRow: function (id, rowdata, rowelem) {
            if (rowelem.HasRelatedPermits) {
                $('#dashboardGrid tr#' + id).addClass('greenHighlight');
            }
        },
        gridComplete: onGridComplete,
        rowNum: 50,
        sortname: 'permitname',
        sortorder: 'asc',
        url: gridDataAction
    });

    // enable modal popup for letters link
    $('.dashbordLetters').modal();
});

function ToggleDashboardLettersLink() {
    if ($('#SelectedFilterTemplate').val() == 'Compliance') {
        $('#dashboardLettersSpan').show();
    }
    else {
        var val = $('#PermitCategoryFilter_SelectedPermitCategoryIds').val();
        if (val && val.length == 1 && val[0] == 6) { // Existing Permits
            $('#dashboardLettersSpan').show();
        } else {
            $('#dashboardLettersSpan').hide();
        }
    }
}

function permitNumberFormatter(cellvalue, options, rowObject) {
    var linkText;

    if (cellvalue) {
        linkText = cellvalue;
    } else {
        linkText = "Details";
    }

    return '<a href="' + detailsAction + '?' + $.param({ id: rowObject.Id }) + '">' + linkText + '</a>';
}

function booleanYesNoFormatter(cellvalue, options, rowObject) {
    if (cellvalue) {
        return 'Yes';
    }
    else {
        return 'No';
    }
}

function showMapLinkFormatter(cellvalue, options, rowObject) {
    if ($('#dashboardGrid').jqGrid('getGridParam', 'records') > 500) {
        return '<span class="spanLink showMapLink disabled" data-permitid="' + rowObject.Id + '">Map</span>';
    }
    else {
        return '<span class="spanLink showMapLink" data-permitid="' + rowObject.Id + '">Map</span>';
    }
}

function onGridComplete() {
    $('#recordCount').text($(this).jqGrid('getGridParam', 'records').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

function localResize() {
    $("#dashboardGrid").jqGrid("fixGridWidthWithoutParentScroll");
    $("#dashboardGrid").jqGrid("fixGridHeightWithoutParentScroll");
}

$("#dashboardSearchButton").click(function () {
    $('#SearchForm').submit();
});

$('#resetButton').click(function () {
    $('#SelectedFilterTemplate').change();
});

function onSearchSuccess(result) {
    $('#dashboardGrid').trigger("reloadGrid", [{ page: 1 }]);
}

$('#clearSearch').click(function () {
    $(this).next().focus().val('').blur().next().click();
});

function onFilterSuccess(result) {
    if (result.IsSaveFilterResult && result.IsSuccess) {
        alert('Filter Saved Successfully.');
    }
    else {
        $('#dashboardGrid').trigger("reloadGrid", [{ page: 1 }]);
    }
}

$('#FilterForm').on('change', 'input', function () {
    $('#FilterForm').submit();
});

$('#FilterForm').on('change', 'select', function () {
    $('#FilterForm').submit();
});

$('#saveFilterButton').click(function () {
    $('#SelectedFilterTemplate').val('MySavedFilter');
    $('#FilterForm').attr('action', saveFilterAction).submit().attr('action', filterAction);
});

$('#FilterForm').on('click', '.filterheader', function () {
    $(this).next().slideToggle();
});

$('#FilterForm').on('mouseover', '.filterheader', function () {
    $(this).addClass('ui-state-hover');
});

$('#FilterForm').on('mouseout', '.filterheader', function () {
    $(this).removeClass('ui-state-hover');
});

$('#SelectedFilterTemplate').change(function () {
    $.ajax({
        url: getFilterControlAction,
        data: { filterTemplate: $('#SelectedFilterTemplate').val() },
        success: function (data) {
            if (data != null && data.length > 0) {
                $('#FilterControlContainer').empty().html(data);
                BindUIControls();
                ToggleDashboardLettersLink();
                if ($('#PermitStatusFilter_SelectedPermitStatusId').length) {
                    FilterPermitStatuses();
                }
                else {
                    $('#FilterForm').submit();
                }
            }
        }
    });
});

$('#FilterForm').on('change', '#PermitCategoryFilter_SelectedPermitCategoryIds', function () {
    // Hide Unaffected Statuses in Filter Control
    FilterPermitStatuses();
    ToggleDashboardLettersLink();
});

$("#exportLink").click(function () {
    if ($("#recordCount").text() == 0)
        return false;
});

$('.tabLink').click(function () {
    if (!$(this).hasClass('active')) {
        $('.tabLink').removeClass('active');
        $(this).addClass('active');

        if ($(this).is('#tableLink')) {
            $('#tabular').show();
            $('#spatial').css('z-index', -9999);
        }
        else if ($(this).is('#mapLink')) {
            $('#tabular').hide();
            $('#spatial').css('z-index', '');

            SpatialFilter.AddMapExtentFilter();

            if (permitLocationsLayer && permitLocationsLayer.graphics.length > 0 && $('#MapExtentFilter_IsApplied').val().toLowerCase() != 'true') {
                var permitExtent = esri.graphicsExtent(permitLocationsLayer.graphics);
                if (permitExtent) {
                    map.setExtent(permitExtent, true);
                }
            }
        }
    }
});

$('#dashboardGrid').on('click', '.showMapLink', function () {
    if ($(this).hasClass('disabled')) {
        alert('Show map functionality only works when there are less than 500 records');
    }
    else {
        $('#tabular').hide();
        $('#spatial').css('z-index', '');
        $('.tabLink').removeClass('active');
        $('#mapLink').addClass('active');

        if (permitLocationsLayer.graphics.length > 0) {
            for (i in permitLocationsLayer.graphics) {
                var currentGraphic = permitLocationsLayer.graphics[i];
                if (currentGraphic.attributes["PermitId"] == $(this).attr('data-permitid')) {
                    var permitExtent = esri.graphicsExtent([currentGraphic]);
                    if (permitExtent) {
                        map.setExtent(permitExtent, true);
                    }

                    break;
                }
            }
        }
    }
});


///////////
// Map
///////////

function buildKmlFile() {
    var outSR = new esri.SpatialReference({ wkid: 4326 });
    var params = new esri.tasks.ProjectParameters();

    params.geometries = [currentPermitPoly];
    params.outSR = outSR;

    try {
        geometrySvc.project(params,
        function (features) {
            $.ajax({
                url: buildKmlFileAction,
                type: 'POST',
                data: JSON.stringify(features[0].rings),
                traditional: true,
                dataType: "json",
                contentType: 'application/json',
                success: function (response) {
                    window.open(response.Url);
                },
                error: function (e) {
                    alert(e.message);
                }
            });
        });
    } catch (e) { alert(e.message); }
}

$('#search').keypress(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { //Enter keycode
        $('#searchbutton').click();
    }
});

$('#searchbutton').click(function (e) {
    if ($('#search').val() != '' && $('#search').val() != $('#search').attr('title')) {
        if ($("#searchtype").val() == "1") {
            //Address search
            LocateAddress();
        }
        else if ($("#searchtype").val() == "2") {
            //Lat/Long search
            ZoomToLatLong();
        }
        else if ($("#searchtype").val() == "3") {
            //Parcel search
            ZoomToParcel();
        }
    }
});

$('#printtemplates').click(function (e) {
    $('.selectBox').slideToggle(200);
    $('.selectBox li').click(function () {
        $('.mehidden').val($(this).text());
        $('.default').text($(this).text());
        $('.selectBox').slideUp(200);
    });
});

$('#printbutton').click(function (e) {
    var template = $('#printtemplates').attr('value');
    if (template == "Layout") {
        alert("Please select a print layout.");
    } else {
        $('#printtemplates').attr('value', 'Printing...');
        $('#printtemplates').attr('disabled', true);
        if (template == "Map Only") {
            PrintMap("MAP_ONLY");
        } else {
            PrintMap("A4 " + template);
        }
    }
});

$('.selectBox li').click(function (e) {
    switch ($(this).attr('title')) {
        case "Portrait":
            $('#printtemplates').attr('value', 'Portrait');
            break;

        case "Landscape":
            $('#printtemplates').attr('value', 'Landscape');
            break;

        case "Map Only":
            $('#printtemplates').attr('value', 'Map Only');
            break;
    }
});

$('#basemapswitcher').change(function () {
    if ($('#basemaptype').val() == "1") {
        basemapImagery.hide();
        basemapTopo.hide();
        basemap.show();
    }
    else if (($('#basemaptype').val() == "2")) {
        basemapImagery.show();
        basemapTopo.hide();
        basemap.hide();
    }
    else if (($('#basemaptype').val() == "3")) {
        basemapImagery.hide();
        basemapTopo.show();
        basemap.hide();
    }
});

$('#searchtype').change(function () {
    if ($('#search').val() == $('#search').attr('title')) {
        $('#search').val('');
    }

    if ($(this).val() == "1") {
        $('#search').attr('title', 'e.g. 2345 Glen Arm Rd. 21234').labelify({ labelledClass: "labelHighlight" });
    }
    else if ($(this).val() == "2") {
        $('#search').attr('title', 'e.g. 39.27138, -76.131995').labelify({ labelledClass: "labelHighlight" });
    }
    else if ($(this).val() == "3") {
        $('#search').attr('title', 'County Code,Tax Map,Grid,Parcel,Lot').labelify({ labelledClass: "labelHighlight" });
    }
});

$('#overlay').change(function () {
    // Turn on specific layer visibility
    overlaysLayer.setVisibleLayers([$(this).val()]);
});

$('#zoomToPermit').click(function () {
    // Get the extent of the active permit geometry and zoom
    if (activePermitLocationGraphicsLayer != null && activePermitLocationGraphicsLayer.graphics.length > 0) {
        for (i in activePermitLocationGraphicsLayer.graphics) {
            var currentGraphic = activePermitLocationGraphicsLayer.graphics[i];
            if (currentGraphic.attributes["PermitId"] == $(this).attr('data-permitid')) {
                var permitExtent = esri.graphicsExtent([currentGraphic]);
                if (permitExtent) {
                    map.setExtent(permitExtent, true);
                }

                break;
            }
        }
    }
});

dojo.addOnLoad(function () {
    var initialExtent = new esri.geometry.Extent(-8938240.339553142, 4517704.527939503, -8242357.634044933, 4867480.369372453,
        new esri.SpatialReference({ wkid: 3857 }));

    map = new esri.Map("map", {
        logo: false,
        extent: initialExtent
    });

    $('#map').resize(function () {
        map.resize();
    });

    if ($('#MapExtentFilter_IsApplied').val().toLowerCase() == 'true') {
        var xmin = parseFloat($('#MapExtentFilter_XMin').val());
        var ymin = parseFloat($('#MapExtentFilter_YMin').val());
        var xmax = parseFloat($('#MapExtentFilter_XMax').val());
        var ymax = parseFloat($('#MapExtentFilter_YMax').val());
        var srid = parseInt($('#MapExtentFilter_SRID').val());

        var extent = new esri.geometry.Extent(xmin, ymin, xmax, ymax, new esri.SpatialReference({ wkid: srid }));
        map.setExtent(extent, true);
    }

    dojo.connect(map, "onExtentChange", function (extent) {
        $('#MapExtentFilter_XMin').val(extent.xmin);
        $('#MapExtentFilter_YMin').val(extent.ymin);
        $('#MapExtentFilter_XMax').val(extent.xmax);
        $('#MapExtentFilter_YMax').val(extent.ymax);
        $('#MapExtentFilter_SRID').val(extent.spatialReference.latestWkid || extent.spatialReference.wkid);

        if ($('#MapExtentFilter_IsApplied').val().toLowerCase() == 'true') {
            $('#DashboardSpatialFilterForm').submit();
        }

        if (map.__LOD.level > 13) {
            parcelLayer.show();
            //taxMapLayer.show();
        }
        else {
            parcelLayer.hide();
            //taxMapLayer.hide();
        }
    });

    map.infoWindow.resize(385, 300);

    drawToolbar = new esri.toolbars.Draw(map);
    dojo.connect(drawToolbar, "onDrawEnd", onIdentifyLocationEnd);
    esri.bundle.toolbars.draw.addPoint = "Click to Identify Location";

    geometrySvc = new esri.tasks.GeometryService(gisServicesHost + "/arcgis/rest/services/Utilities/Geometry/GeometryServer");

    //var basemapURL = gisServicesHost + "/mdimap/rest/services/ImageryBaseMapsEarthCover/MD.State.MDiMap_Gazetteer83M/MapServer";
    var basemapURL = "https://server.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer";
    basemap = new esri.layers.ArcGISTiledMapServiceLayer(basemapURL);
    map.addLayer(basemap);

    var basemapImageryUrl = "https://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer";
    basemapImagery = new esri.layers.ArcGISTiledMapServiceLayer(basemapImageryUrl);
    map.addLayer(basemapImagery);
    basemapImagery.hide();

    var basemapTopoURL = "https://server.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer";
    basemapTopo = new esri.layers.ArcGISTiledMapServiceLayer(basemapTopoURL);
    map.addLayer(basemapTopo);
    basemapTopo.hide();

    var parcelLayerURL = "https://geodata.md.gov/imap/rest/services/PlanningCadastre/MD_ParcelBoundaries/MapServer";
    var parcelLayer = new esri.layers.ArcGISTiledMapServiceLayer(parcelLayerURL);
    map.addLayer(parcelLayer);
    parcelLayer.hide();

    var taxMapLayerURL = "https://geodata.md.gov/imap/rest/services/PlanningCadastre/MD_PropertyData/MapServer";
    var taxMapLayer = new esri.layers.ArcGISDynamicMapServiceLayer(taxMapLayerURL);
    taxMapLayer.setVisibleLayers([1]);
    map.addLayer(taxMapLayer);
    //taxMapLayer.hide();

    var overlaysLayerURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsOverlays/MapServer";
    overlaysLayer = new esri.layers.ArcGISDynamicMapServiceLayer(overlaysLayerURL, { "opacity": 0.7 });
    overlaysLayer.setVisibleLayers([-1]);
    map.addLayer(overlaysLayer);

    spatialFilterOverlayGraphicsLayer = new esri.layers.GraphicsLayer();
    map.addLayer(spatialFilterOverlayGraphicsLayer);

    var permitLocationInfoWindowTemplate = new esri.InfoTemplate(getPermitLocationInfoWindowTitle, getPermitLocationInfoWindowContent);

    var permitLocationsURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsPermitLocation/FeatureServer/0";
    permitLocationsLayer = new esri.layers.FeatureLayer(permitLocationsURL, {
        infoTemplate: permitLocationInfoWindowTemplate,
        mode: esri.layers.FeatureLayer.MODE_SELECTION,
        outFields: ['*']
    });
    map.addLayer(permitLocationsLayer);

    dojo.connect(permitLocationsLayer, "onClick", function (event) {
        currentPermitPoly = event.graphic.geometry;
    });

    activePermitLocationGraphicsLayer = new esri.layers.GraphicsLayer();
    activePermitLocationGraphicsLayer.setInfoTemplate(permitLocationInfoWindowTemplate);
    map.addLayer(activePermitLocationGraphicsLayer);

    var groundwaterInfoWindowTemplate = new esri.InfoTemplate("Groundwater Withdrawal", getGroundwaterInfoWindowContent);
    var surfaceWaterInfoWindowTemplate = new esri.InfoTemplate("Surface Water Withdrawal", getSurfaceWaterInfoWindowContent);

    var groundwaterWithdrawalPointsURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsWaterWithdrawalAndPermitLocation/FeatureServer/1";
    groundwaterWithdrawalPointsLayer = new esri.layers.FeatureLayer(groundwaterWithdrawalPointsURL, {
        infoTemplate: groundwaterInfoWindowTemplate,
        mode: esri.layers.FeatureLayer.MODE_SELECTION,
        outFields: ['*']
    });
    map.addLayer(groundwaterWithdrawalPointsLayer);

    var surfaceWaterWithdrawalPointsURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsWaterWithdrawalAndPermitLocation/FeatureServer/0";
    surfaceWaterWithdrawalPointsLayer = new esri.layers.FeatureLayer(surfaceWaterWithdrawalPointsURL, {
        infoTemplate: surfaceWaterInfoWindowTemplate,
        mode: esri.layers.FeatureLayer.MODE_SELECTION,
        outFields: ['*']
    });
    map.addLayer(surfaceWaterWithdrawalPointsLayer);

    activeGroundwaterWithdrawalPointsGraphicsLayer = new esri.layers.GraphicsLayer();
    activeSurfaceWaterWithdrawalPointsGraphicsLayer = new esri.layers.GraphicsLayer();
    activeGroundwaterWithdrawalPointsGraphicsLayer.setInfoTemplate(groundwaterInfoWindowTemplate);
    activeSurfaceWaterWithdrawalPointsGraphicsLayer.setInfoTemplate(surfaceWaterInfoWindowTemplate);
    map.addLayer(activeGroundwaterWithdrawalPointsGraphicsLayer);
    map.addLayer(activeSurfaceWaterWithdrawalPointsGraphicsLayer);

    locatorService = new esri.tasks.Locator("https://geodata.md.gov/imap/rest/services/GeocodeServices/MD_CompositeLocatorWithZIPCodeCentroids/GeocodeServer");
    dojo.connect(locatorService, "onAddressToLocationsComplete", showResults);

    if ($('#DisplayMode').val() == "Analysis") {
        var query = new esri.tasks.Query();
        query.where = "PermitId = " + $('#zoomToPermit').attr('data-permitid');

        // Query for the permit locations with the given permit Id
        permitLocationsLayer.queryFeatures(query, function (featureSet) {
            if (featureSet.features.length > 0) {
                var symbol = new esri.symbol.SimpleFillSymbol(
                    esri.symbol.SimpleFillSymbol.STYLE_SOLID,
                    new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 255]), 3),
                    new dojo.Color([255, 255, 255, 0]));

                var permitGeometries = [];

                for (i in featureSet.features) {
                    var permitFeature = featureSet.features[i];
                    permitGeometries[i] = permitFeature.geometry;
                    permitFeature.setSymbol(symbol);
                    activePermitLocationGraphicsLayer.add(permitFeature);
                }

                var permitExtent = esri.graphicsExtent(featureSet.features);
                if (permitExtent) {
                    map.setExtent(permitExtent, true);
                }

                geometrySvc.union(permitGeometries, function (geometry) {
                    currentPermitPoly = geometry;
                });
            }
        });

        // Query for the ground water withdrawal locations with the given permit Id
        groundwaterWithdrawalPointsLayer.queryFeatures(query, function (featureSet) {
            if (featureSet.features.length > 0) {
                var symbol = new esri.symbol.SimpleMarkerSymbol(
                    esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE,
                    8,
                    null,
                    new dojo.Color([0, 36, 116]));

                for (i in featureSet.features) {
                    var pointFeature = featureSet.features[i];
                    pointFeature.setSymbol(symbol);
                    activeGroundwaterWithdrawalPointsGraphicsLayer.add(pointFeature);
                }
            }
        });

        // Query for the surface water withdrawal locations with the given permit Id
        surfaceWaterWithdrawalPointsLayer.queryFeatures(query, function (featureSet) {
            if (featureSet.features.length > 0) {
                var symbol = new esri.symbol.SimpleMarkerSymbol(
                    esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE,
                    8,
                    null,
                    new dojo.Color([36, 116, 0]));

                for (i in featureSet.features) {
                    var pointFeature = featureSet.features[i];
                    pointFeature.setSymbol(symbol);
                    activeSurfaceWaterWithdrawalPointsGraphicsLayer.add(pointFeature);
                }
            }
        });
    }
});

var getPermitLocationInfoWindowTitle = function (graphic) {
    if (graphic.attributes.PermitName == null) {
        return "No Permit Number Assigned";
    }
    else {
        return graphic.attributes.PermitName;
    }
}

var getPermitLocationInfoWindowContent = function (graphic) {
    var $container = $('<div></div>').css('height', 275).css('overflow', 'auto').css('margin-bottom', 15);

    var $table = $('<table></table>').css('width', 350)
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Permittee:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Permittee))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Permit Category:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.PermitCategory))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Permit Type:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.PermitType))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Permit Status:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.PermitStatus))))


    var $link = $('<div></div>').css('text-align', 'center').css('position', 'absolute').css('bottom', 0).width(360)
        .append($('<a></a>')
            .attr('href', detailsAction + '?' + $.param({ id: graphic.attributes.PermitId }))
            .text('Details'))
        .append($('<a></a>')
            .attr('href', 'javascript:buildKmlFile()')
            .css('color', 'blue')
            .text('Google Earth'));

    if ($('#DisplayMode').val() != "Analysis") {
        $link.append($('<span></span>')
            .addClass('spanLink')
            .attr('onclick', "window.location.search = 'mode=Analysis&permitId=" + graphic.attributes.PermitId + "'")
            .css('color', 'blue')
            .text('Analyze'));
    }

    return $('<div></div>').append($container.append($table)).append($link).html();
}

var getGroundwaterInfoWindowContent = function (graphic) {
    var $container = $('<div></div>').css('height', 275).css('overflow', 'auto').css('margin-bottom', 15);

    var $table = $('<table></table>').css('width', 350)
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Type:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.GroundwaterType))));

    if (graphic.attributes.GroundwaterType == "Well") {
        $table
            .append($('<tr></tr>')
                .append($('<td class="infoWindowLeftLabel"></td>').text('Well Status:'))
                .append($('<td></td>').text(nullOrEmpty(graphic.attributes.IsExisting))))
            .append($('<tr></tr>')
                .append($('<td class="infoWindowLeftLabel"></td>').text('Well Depth:'))
                .append($('<td></td>').text(nullOrEmpty(graphic.attributes.WellDepthFeet + ' ft'))))
            .append($('<tr></tr>')
                .append($('<td class="infoWindowLeftLabel"></td>').text('Well Diameter:'))
                .append($('<td></td>').text(nullOrEmpty(graphic.attributes.WellDiameterInches + ' in'))));
    }

    $table
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('County:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.County))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Street:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Street))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('City:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.City))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Zip:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Zip))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Tax Map:'))
            .append($('<td></td>').html(getTaxMapCellText(graphic.attributes.TaxMap, graphic.attributes.ParcelId))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Watershed:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Watershed))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Comments:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Comment))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Water Use(s):'))
            .append($('<td></td>').html(nullOrEmpty(graphic.attributes.WaterUse).replace('|', '<br/>'))));

    return $('<div></div>').append($container.append($table)).html();
}

var getSurfaceWaterInfoWindowContent = function (graphic) {
    var $container = $('<div></div>').css('height', 275).css('overflow', 'auto').css('margin-bottom', 15);

    var $table = $('<table></table>').css('width', 350)
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Type:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.SurfacewaterType))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Water Body Name:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.WaterSourceName))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Location of Intake:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.ExactLocationOfIntake))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('County:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.County))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Street:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Street))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('City:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.City))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Zip:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Zip))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Tax Map:'))
            .append($('<td></td>').html(getTaxMapCellText(graphic.attributes.TaxMap, graphic.attributes.ParcelId))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Watershed:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Watershed))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Comments:'))
            .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Comment))))
        .append($('<tr></tr>')
            .append($('<td class="infoWindowLeftLabel"></td>').text('Water Use(s):'))
            .append($('<td></td>').html(nullOrEmpty(graphic.attributes.WaterUse).replace('|', '<br/>'))));

    return $('<div></div>').append($container.append($table)).html();
}

var nullOrEmpty = function (string) {
    return string && string != '' ? string : 'Not defined';
}

var getTaxMapCellText = function (taxMap, parcel) {
    if (taxMap && taxMap != '') {
        if (parcel && parcel != '') {
            var baseUrl = 'http://sdatcert3.resiusa.org/rp_rewrite/details.aspx';

            if (parcel.substring(0, 2) == '03') {
                // Baltimore City has a different format for their ACCTID
                var countyId = parcel.substring(0, 2);
                var ward = parcel.substring(2, 4);
                var section = parcel.substring(4, 6);
                var block = parcel.substring(6, 11).replace(/\s+$/, ''); //Some blocks have an alpha character at the end, while others have a whitespace
                var lot = parcel.substring(11, 14);

                var queryString = '?County=' + countyId + '&SearchType=ACCT&Ward=' + ward + '&Section=' + section + '&Block=' + block + '&Lot=' + lot;

                return taxMap + '<a href="' + baseUrl + queryString + '" target="_blank">SDAT</a>';
            }
            else {
                var countyId = parcel.substring(0, 2);
                var districtId = parcel.substring(2, 4);
                var accountNumber = parcel.substring(4);

                var queryString = '?County=' + countyId + '&SearchType=ACCT&District=' + districtId + '&AccountNumber=' + accountNumber;

                return taxMap + '<a href="' + baseUrl + queryString + '" target="_blank">SDAT</a>';
            }
        }
        else {
            return taxMap;
        }
    }
    else {
        return 'Not Defined';
    }
}

var PopulateMap = function (permitIds, feature) {
    if (permitLocationsLayer && permitLocationsLayer.selectFeatures && typeof (permitLocationsLayer.selectFeatures === 'function') && spatialFilterOverlayGraphicsLayer != null) {
        spatialFilterOverlayGraphicsLayer.clear();
        if (feature != null) {
            var poly = new esri.geometry.Polygon(feature);

            var simpleFillSymbol = new esri.symbol.SimpleFillSymbol(
                esri.symbol.SimpleFillSymbol.STYLE_SOLID,
                null,
                new dojo.Color([118, 154, 60, 0.5]));

            var graphic = new esri.Graphic(poly, simpleFillSymbol, null, null);

            spatialFilterOverlayGraphicsLayer.add(graphic);
        }

        selectedPermitIds = permitIds;
        var query = new esri.tasks.Query();

        if (permitIds && permitIds.length) {
            if (permitIds.length <= 500) {
                query.where = "PermitId IN (" + permitIds + ")";
                $('#mapMessages').text('').hide();
            }
            else {
                query.where = "1 = 2";
                $('#mapMessages').text('Too many records to map. Please zoom in or alter your filters.').show();
            }
        }
        else {
            query.where = "1 = 2";
            $('#mapMessages').text('').hide();
        }

        permitLocationsLayer.selectFeatures(query, esri.layers.FeatureLayer.SELECTION_NEW);
        groundwaterWithdrawalPointsLayer.selectFeatures(query, esri.layers.FeatureLayer.SELECTION_NEW);
        surfaceWaterWithdrawalPointsLayer.selectFeatures(query, esri.layers.FeatureLayer.SELECTION_NEW);
    }
    else {
        setTimeout(PopulateMap, 100, permitIds, feature);
    }
}

function ExportToShapeFile ()
{
    alert("Exporting " + selectedPermitIds.length.toString() + " Permits...");
    alert("Permit Url = " + permitLocationsLayer.url);
    alert("Groundwater Points Url = " + groundwaterWithdrawalPointsLayer.url);
    alert("Surface Water Points Url = " + surfaceWaterWithdrawalPointsLayer.url);
}

var LocateAddress = function () {
    map.graphics.clear();

    var address = { "SingleLine": $('#search').val() };
    locatorService.outSpatialReference = map.spatialReference;
    var options = {
        address: address,
        outFields: ["State"]
    }

    locatorService.addressToLocations(options);
}

var ZoomToLatLong = function () {
    map.graphics.clear();

    var latLong = $('#search').val().split(',');
    if (latLong.length != 2) {
        return;
    }

    var lat = parseFloat(latLong[0]);
    var long = parseFloat(latLong[1]);

    var point = new esri.geometry.Point(long, lat);

    var params = new esri.tasks.ProjectParameters();
    params.geometries = [point];
    params.outSR = map.spatialReference;
    geometrySvc.project(params, onGeometryServiceProjectComplete);
}

var ZoomToParcel = function () {
    map.graphics.clear();

    var params = $('#search').val().split(',');
    if (params.length < 4 || params.length > 5) {
        // Lot is optional, hence length < 4
        return;
    }

    var countyName = params[0].trim();

    var queryUrl = GetQueryTaskUrlByCountyName(countyName);
    if (queryUrl) {
        var queryTask = new esri.tasks.QueryTask(queryUrl);

        // build query filter
        var query = new esri.tasks.Query();
        query.returnGeometry = true;
        query.outFields = ["MAP", "GRID", "PARCEL", "LOT"];

        /* Parse out our query values */
        var parcelMap = ("0000" + params[1].trim()).slice(-4), // returns 00123params[1].replace(/^\s+|^[0]+|\s+$/g, ''),
            grid = ("0000" + params[2].trim()).slice(-4), // returns 00123params[2].replace(/^\s+|^[0]+|\s+$/g, ''),
            parcel = ("0000" + params[3].trim()).slice(-4); // returns 00123params[3].replace(/^\s+|^[0]+|\s+$/g, ''),

        query.where = "MAP = '" + parcelMap + "' AND GRID = '" + grid + "' AND PARCEL = '" + parcel + "'";

        if (params.length == 5) {
            // Lot Included
            var lot = params[4].trim();
            query.where = query.where + " AND LOT = '" + lot + "'";
        }

        dojo.connect(queryTask, "onComplete", function (featureSet) {
            if (featureSet.features.length == 0) {
                alert("No results found");
                return;
            }

            var feature = featureSet.features[0];

            var symbol = new esri.symbol.SimpleMarkerSymbol();
            var infoTemplate = new esri.InfoTemplate("Parcel", "Map: ${MAP}<br/>Grid: ${GRID}<br/>Parcel: ${PARCEL}<br/>Lot: ${LOT}");

            symbol.setStyle(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE);
            symbol.setColor(new dojo.Color([153, 0, 51, 0.75]));

            //add a graphic to the map at the parcel location
            var graphic = new esri.Graphic(feature.geometry, symbol, feature.attributes, infoTemplate);
            map.graphics.add(graphic);

            map.centerAndZoom(feature.geometry, 16);
        });

        queryTask.execute(query);
    }
}

var GetQueryTaskUrlByCountyName = function (countyName) {
    var baseUrl = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsParcelPoints/MapServer/";

    // Append the Layer Id based upon the supplied county name
    switch (countyName.toLowerCase()) {
        case "allegany":
        case "al":
            return baseUrl + "0";
        case "anne arundel":
        case "aa":
            return baseUrl + "1";
        case "baltimore city":
        case "bc":
            return baseUrl + "2";
        case "baltimore":
        case "ba":
            return baseUrl + "3";
        case "calvert":
        case "ca":
            return baseUrl + "4";
        case "caroline":
        case "co":
            return baseUrl + "5";
        case "carroll":
        case "cl":
            return baseUrl + "6";
        case "cecil":
        case "ce":
            return baseUrl + "7";
        case "charles":
        case "ch":
            return baseUrl + "8";
        case "dorchester":
        case "do":
            return baseUrl + "9";
        case "frederick":
        case "fr":
            return baseUrl + "10";
        case "garrett":
        case "ga":
            return baseUrl + "11";
        case "harford":
        case "ha":
            return baseUrl + "12";
        case "howard":
        case "ho":
            return baseUrl + "13";
        case "kent":
        case "ke":
            return baseUrl + "14";
        case "montgomery":
        case "mo":
            return baseUrl + "15";
        case "prince george's":
        case "prince georges":
        case "pg":
            return baseUrl + "16";
        case "queen anne's":
        case "queen annes":
        case "qa":
            return baseUrl + "17";
        case "somerset":
        case "so":
            return baseUrl + "18";
        case "st. mary's":
        case "st. marys":
        case "st mary's":
        case "st marys":
        case "sm":
            return baseUrl + "19";
        case "talbot":
        case "ta":
            return baseUrl + "20";
        case "washington":
        case "wa":
            return baseUrl + "21";
        case "wicomico":
        case "wi":
            return baseUrl + "22";
        case "worcester":
        case "wo":
            return baseUrl + "23";
        default:
            alert("Invalid County Name");
            return;
    }
}

var showResults = function (candidates) {
    var candidate;
    var symbol = new esri.symbol.SimpleMarkerSymbol();
    var infoTemplate = new esri.InfoTemplate("Location", "Address: ${address}");

    symbol.setStyle(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE);
    symbol.setColor(new dojo.Color([153, 0, 51, 0.75]));

    var geom;

    $.each(candidates, function (idx, candidate) {
        if (candidate.score > 80) {
            var attributes = { address: candidate.address, score: candidate.score, locatorName: candidate.attributes.Loc_name };
            geom = candidate.location;

            //add a graphic to the map at the geocoded location
            var graphic = new esri.Graphic(geom, symbol, attributes, infoTemplate);
            map.graphics.add(graphic);

            //Add a text symbol to the map listing the location of the matched address.
            var displayText = candidate.address;
            var font = new esri.symbol.Font("16pt", esri.symbol.Font.STYLE_NORMAL, esri.symbol.Font.VARIANT_NORMAL, esri.symbol.Font.WEIGHT_BOLD, "Helvetica");

            var textSymbol = new esri.symbol.TextSymbol(displayText, font, new dojo.Color("#666633"));
            textSymbol.setOffset(0, 8);
            map.graphics.add(new esri.Graphic(geom, textSymbol));

            //break out of loop after one candidate with score greater than 80 is found.
            return false;
        }
    });

    if (geom == undefined) {
        alert("No results found");
    }
    else {
        map.centerAndZoom(geom, 16);
    }
}

var onGeometryServiceProjectComplete = function (geometries) {
    var geom = geometries[0];

    if (geom == null || isNaN(geom.x) || isNaN(geom.y)) {
        alert("No results found");
        return;
    }

    var symbol = new esri.symbol.SimpleMarkerSymbol();
    symbol.setStyle(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE);
    symbol.setColor(new dojo.Color([153, 0, 51, 0.75]));

    //Add a graphic to the map at the x,y location
    var graphic = new esri.Graphic(geom, symbol);
    map.graphics.add(graphic);

    //Add a text symbol to the map listing the location of the matched address.
    //var displayText = candidate.address;
    //var font = new esri.symbol.Font("16pt", esri.symbol.Font.STYLE_NORMAL, esri.symbol.Font.VARIANT_NORMAL, esri.symbol.Font.WEIGHT_BOLD, "Helvetica");

    //var textSymbol = new esri.symbol.TextSymbol(displayText, font, new dojo.Color("#666633"));
    //textSymbol.setOffset(0, 8);
    //map.graphics.add(new esri.Graphic(geom, textSymbol));

    map.centerAndZoom(geom, 16);
}

$('.spatialFilterIconButton').click(function () {
    var $input = $(this).next('input[id$="IsApplied"]');
    if ($input.val().toLowerCase() == 'true') {  // We already have a filter, need to remove
        $input.val(false);

        $(this).siblings('.spatialFilterButtonText').removeClass('active');
        $(this).attr('title', 'Add').children('.spatialFilterIcons').removeClass('spatialFilterX').addClass('spatialFilterPlus');

        $('#DashboardSpatialFilterForm').submit();
    }
    else {  // Adding a new filter
        if (!$('#mapLink').is('.active')) {
            $('#mapLink').click();
        }

        switch ($(this).parent().attr('id')) {
            case 'distanceFilterButton':
                SpatialFilter.AddDistanceFilter();
                break;
            case 'featureFilterButton':
                SpatialFilter.AddFeatureFilter();
                break;
            case 'mapExtentFilterButton':
                SpatialFilter.AddMapExtentFilter();
                break;
            default:
                $.error('Invalid case specified for spatial filter icon button click event');
                break;
        }
    }
});

var SpatialFilter = {
    ActivateFilterButton: function ($button) {
        var $input = $button.next('input[id$="IsApplied"]');
        $input.val(true);

        $button.siblings('.spatialFilterButtonText').addClass('active');
        $button.attr('title', 'Remove').children('.spatialFilterIcons').removeClass('spatialFilterPlus').addClass('spatialFilterX');
    },

    AddDistanceFilter: function () {
        // Pop up Modal Dialog
        $('#mapModal')
            .append($('<div></div>')
                .append($('<p></p>').text('Use the field below to enter the radius of the search.  Then click a location on the map to begin the search.'))
                .append($('<span></span>').text('Enter the distance in miles:').css('margin-right', '10px'))
                .append($('<span></span>')
                    .append($('<input></input>')
                        .attr('type', 'textbox')
                        .attr('id', 'distanceTextBox')
                        .attr('maxlength', '4')
                        .css('width', '40px')
                    )
                )
            )
            .dialog({
                buttons: [
                    {
                        text: 'Go',
                        click: function () {
                            var currentDistanceVal = $('#distanceTextBox').val();
                            if (currentDistanceVal && /^((\d+)|(\d*\.\d*))$/.test(currentDistanceVal)) {
                                $('#distanceTextBox').css('border-color', '')

                                $('#PermitDistanceFilter_Distance').val(currentDistanceVal);

                                ActivateDraw(DrawType.PermitDistance);

                                $(this).dialog("close");
                            }
                            else {
                                $('#distanceTextBox').css('border-color', '#FF0000').focus();
                            }
                        }
                    },
                    {
                        text: 'Cancel',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                close: function () {
                    $(this).empty();
                },
                draggable: false,
                height: 240,
                modal: true,
                resizable: false,
                title: 'Search By Distance',
                width: 350
            });
    },

    AddFeatureFilter: function () {
        // Pop up Modal Dialog
        $('#mapModal')
            .append($('<div></div>')
                .append($('<p></p>').text('Use the dropdown below to select the type of feature to search.  Then click a location on the map to begin the search.'))
                .append($('<span></span>').text('Select the feature type:').css('margin-right', '10px'))
                .append($('<span></span>')
                    .append($('<select></select>')
                        .attr('id', 'featureTypeDropDown')
                        .append($('<option></option>')
                            .val('RechargeEasement')
                            .text('Recharge Easement'))
                        .append($('<option></option>')
                            .val('WmStrategyArea')
                            .text('Water Management Strategy Area'))
                        .append($('<option></option>')
                            .val('Watershed')
                            .text('Watershed'))
                    )
                )
            )
            .dialog({
                buttons: [
                    {
                        text: 'Go',
                        click: function () {
                            var currentFeatureType = $('#featureTypeDropDown').val();
                            $('#PermitFeatureFilter_FeatureType').val(currentFeatureType);

                            ActivateDraw(DrawType.PermitFeature);

                            $(this).dialog("close");
                        }
                    },
                    {
                        text: 'Cancel',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                close: function () {
                    $(this).empty();
                },
                draggable: false,
                height: 240,
                modal: true,
                resizable: false,
                title: 'Search By Feature',
                width: 350
            });
    },

    AddMapExtentFilter: function () {
        SpatialFilter.ActivateFilterButton($('#mapExtentFilterButton > .spatialFilterIconButton'));
        $('#DashboardSpatialFilterForm').submit();
    }
}

var ActivateDraw = function (drawType) {
    switch (drawType) {
        case DrawType.PermitDistance:
        case DrawType.PermitFeature:
            drawToolbar.drawType = drawType;
            break;
        default:
            alert("Invalid Draw Type");
            return;
    }

    permitLocationsLayer.disableMouseEvents();
    drawToolbar.activate(esri.toolbars.Draw.POINT);
}

var DeactivateDraw = function () {
    permitLocationsLayer.enableMouseEvents();
    drawToolbar.deactivate();
}

var onIdentifyLocationEnd = function (point) {
    switch (drawToolbar.drawType) {
        case DrawType.PermitDistance:
            $('#PermitDistanceFilter_PointX').val(point.x);
            $('#PermitDistanceFilter_PointY').val(point.y);
            $('#PermitDistanceFilter_SRID').val(point.spatialReference.latestWkid || point.spatialReference.wkid);

            SpatialFilter.ActivateFilterButton($('#distanceFilterButton > .spatialFilterIconButton'));

            break;
        case DrawType.PermitFeature:
            $('#PermitFeatureFilter_PointX').val(point.x);
            $('#PermitFeatureFilter_PointY').val(point.y);
            $('#PermitFeatureFilter_SRID').val(point.spatialReference.latestWkid || point.spatialReference.wkid);

            SpatialFilter.ActivateFilterButton($('#featureFilterButton > .spatialFilterIconButton'));

            break;
        default:
            break;
    }

    $('#DashboardSpatialFilterForm').submit();

    DeactivateDraw();
}