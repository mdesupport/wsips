﻿var AdvertisementFees = {

    Initialize: function (PermitId) {
        AdvertisementFees.SetGrid(PermitId)
        AdvertisementFees.AddFeeButtonClick()
        AdvertisementFees.DropDownSelectEvent()

        //format Amount so that it only has two decimal places
        $("#ProductsDropDown").find("option").each(function () {
            $(this).attr('Amount', ((parseFloat(Math.round($(this).attr('Amount') * 100) / 100).toFixed(2))))
        })
    },


    SetGrid: function (PermitId) {

        $("#advertisementFeesGrid").jqGrid({
            colModel: [

                { name: 'FeeAmount' },
                { name: 'FeeDate' },
                { name: 'CreatedDate' },
                { name: 'CreatedBy' },
                { name: 'delColumn', formatter: AdvertisementFees.DeleteFormatter }
            ],
            colNames: ['Fee Amount', 'Due Date', 'Created Date', 'Created By',''],
            url: Helper.GetBaseURL("Permitting") + "Enforcement/GetAdvertisementFeesByPermitId?PermitId=" + PermitId,
            datatype: "json",
            gridComplete: function () {

            },
            autowidth: true,
            scrollOffset: 0,
            forceFit: true,
            height: "auto"
        });

    },

    DeleteFormatter: function (cellvalue, options, rowelem) {
        return '<a href="javascript:void(0)" onclick="AdvertisementFees.DeleteHearing(' + rowelem.Id + ')">Delete</a>';
    },

    DeleteHearing: function (id) {
        var deleteDialog = $("#advertisementFeesDeleteDialog")
        $(deleteDialog).dialog({
            modal: true,
            position: "center",
            width: 500,
            close: function () {
                $(this).dialog("destroy");
            },
            buttons: {
                "Yes": function () {
                    $.ajax({
                        url: Helper.GetBaseURL("Permitting") + "Enforcement/DeleteAdvertisementFee",
                        data: { id: id },
                        success: function (response) {
                            $("#advertisementFeesGrid").trigger("reloadGrid");
                        }
                    });
                    $(this).dialog("close");
                },
                "No": function () {
                    $(this).dialog("close");
                },
            }
        })
    },
    DropDownSelectEvent: function () {
        $("#ProductsDropDown").unbind('change').change(
            function () {
                $('#overrideAmount').val($(this).find("option[value='" + $(this).val() + "']").attr('Amount')).maskMoney({ precision: 2 }).maskMoney('mask')
        })
    },

    AddFeeButtonClick: function () {
        $("#advertisementFeesButton").click(function(){
            $("#advertisementFeesFormContainer").dialog({
                modal: true,
                position: "center",
                width: 500,
                resizable: false,
                close: function () {
                    $(this).dialog("destroy");
                },
                buttons: {
                    "Create": function () {
                        if ($("#ProductsDropDown").val() != "") {
                            $.ajax({
                                url: Helper.GetBaseURL("Permitting") + "Enforcement/CreateContactSale",
                                data: { PermitId: $("#PermitId").val(), ProductId: $("#ProductsDropDown").val(), Amount: $("#overrideAmount").val() },
                                type: 'post',
                                success: function (response) {
                                    $("#advertisementFeesGrid").trigger("reloadGrid");
                                }
                            });
                            $(this).dialog("close");
                        }
                        
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    },
               
                }
            })
        })
    }
    
}