﻿/// <reference path="../../../jquery-1.8.2.intellisense.js" />

var ApprovedAllocation = {
    Initialize: function () {
        $('.approvedAllocation').addClass('wizardStepActive');

        $.ajax({
            url: $('#ApprovedAllocation').attr('data-contenturl'),
            type: 'GET',
            data: { id: $('#PermitId').val() },
            success: function (response) {
                $('#ApprovedAllocation').html(response).show();
                $(".commaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');
                $(".decimalInput").maskMoney({ precision: 2, defaultZero: false, allowZero: true }).maskMoney('mask');
            }
        });
    }
}