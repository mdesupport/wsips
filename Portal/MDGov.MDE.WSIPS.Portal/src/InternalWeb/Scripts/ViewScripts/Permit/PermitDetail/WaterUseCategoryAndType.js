﻿/// <reference path="_permitReference.js" />

$.extend(WaterUseCategoryAndType, {

    Initialize: function () {
        global.currentStep = Steps.WaterUseCategoryAndType;
        // For Detail view hide the title 
        $('#waterUseCategoryTypeInfoTitle').hide();
        $('#CurrentStep').val('6');
        $('.waterUseCategoryType').addClass('wizardStepActive');
        $('#waterUseCatagoryAndType').show();
        $('#SaveButton').hide();
    }
});