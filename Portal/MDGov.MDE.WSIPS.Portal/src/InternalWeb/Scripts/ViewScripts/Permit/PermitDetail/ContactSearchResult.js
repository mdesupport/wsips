﻿/// <reference path="ApplicationWizard.js" />
/// <reference path="../../underscore.js" />

//Bind on change of the radio button
$('#SearchedContacts').on('change', 'input:radio[id$=IsBusiness]', function () {
    var isBusiness = $(this).val();

    ContactSearchResult.ClearValidationError();                     
    ContactSearchResult.AdjustContactInformationLayout(isBusiness);
});