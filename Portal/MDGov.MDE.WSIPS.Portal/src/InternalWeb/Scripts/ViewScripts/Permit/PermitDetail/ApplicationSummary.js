﻿var ContactType = {
    WaterUser: 1,
    LandOwner: 2,
    Consultant: 3
};

var Steps = {
    ApplicantIdentification: "ApplicantIdentification",
    ContactSearch: "ContactSearch",
    ContactSearchResult: "ContactSearchResult",
    WaterUseDescription: "WaterUseDescription",
    WaterUseCategoryAndType: "WaterUseCategoryAndType",
    WaterUseDetail: "WaterUseDetail",
    IdentifyLocation: "IdentifyLocation",
    WastewaterTreatmentAndDisposal: "WastewaterTreatmentAndDisposal",
    ReviewApplication: "ReviewApplication",
    Affirmation: "Affirmation"
};

var global = {
    currentStep: "",
    isWaterUser: 0,
    isLandOwner: 0,
    isConsultant: 0,
    SearchResult: null,
    GenericUserContactFound:false,
    waterUserContactFound: false,
    LandOwnerContactFound: false,
    ConsultantContactFound: false,
    CurrentlySearching: null,
    SelectedWaterUseCategoryTypeList: new Array(),
    isSaveAndResume: false,
    dataSaved: false,
    businessContactId: "",
    showWaterUseDetail: false,
    permittee: 0,
    ApplicantInformationChanged: false,
    ReloadPrivateWaterSupplierView: false,
    ReloadWasteWaterTreatmentAndDisposalPage: false
};

var ApplicationWizard = {

    Initialize: function () {
        $("[id$=TelephoneNumber_CommunicationValue]").each(function () {
            $(this).mask("999-999-9999");
            // if it has value, disable editing 
            if ($(this).val().length > 0) {
                $(this).attr('readonly', true);
            }
        });

        $("[id$=EmailAddress_CommunicationValue]").each(function () {
            // if it has value, disable editing 
            if ($(this).val().length > 0) {
                $(this).attr('readonly', true);
            }
        });

        ApplicationWizard.ClearSelectedWizardStep();

        global.dataSaved == true;
        ApplicantIdentification.AssignData();
        ApplicantIdentification.SetupOptions();
        $('.ApplicationSum').addClass('wizardStepActive');
        $("#reviewApplication").show();
        ApplicationWizard.LoadSummary();
        $("#SearchedContacts").append("<div id=newLandOwnersContainer></div>");
        ApplicationWizard.LoadAdditionalLandOwners();
        ApplicationWizard.InitializeGlobalVariable();
        ApplicationWizard.LoadwaterUseCatagoryAndTypeView();
        ApplicationWizard.LoadwaterUseDetailView();
    },

    HideEverything: function () {
        $('#MessageSummary').html('');
        $('#CreatePendingPermit').hide();
        $('#waterUseDescription').hide();
        $('#waterUseCatagoryAndType').hide();
        $('#waterUseDetails').hide();
        $("#reviewApplication").hide();
        $('#WastewaterTreatmentAndDisposal').hide();
        $('#ApplicantIdentification').hide();
        $('#SearchedContacts #WaterUser').hide();
        $('#SearchedContacts #LandOwner').hide();
        $('#SearchedContacts #Consultant').hide();
        $('#AddLandOwner').hide();
        $("#newLandOwnersContainer").hide();
        $("#WithdrawalSourceInformation").hide();
        $("#PermitInformation").hide();
        $("#ApprovedAllocation").hide();
        $("#identifyLocation").css('z-index', '-9999');
        $(".detailsTabButtons").hide();
    },

    LoadAdditionalLandOwners: function () {
        $.ajax({
            url: urls.additionalLandOwnersUrl,
            type: 'Get',
            data: { PermitId: $("#PermitId").val() },
            async: false,
            success: function (response) {
                var items = $(response).children("div").length;
                if (items > 0) {
                    $("#newLandOwnersContainer").append(response);
                    for (i = 0; i < items; i++) {
                        //$("#newLandOwnersContainer").append("<hr />" );
                        $("#newLandOwnersContainer").children("div").eq(i).find("input")
                            .attr('id', function () {
                                return "ContactForm_" + i + "__" + $(this).attr('id');
                            })
                            .attr('name', function () {
                                return "ContactForm[" + i + "]." + $(this).attr('name');
                            });

                        $("#newLandOwnersContainer").children("div").eq(i).find("select")
                            .attr('id', function () {
                                return "ContactForm_" + i + "__" + $(this).attr('id');
                            })
                            .attr('name', function () {
                                return "ContactForm[" + i + "]." + $(this).attr('name');
                            });

                        $("<hr>").insertBefore($("#newLandOwnersContainer").children("div").eq(i));
                    }

                    $("#newLandOwnersContainer").attr("style", "display:none");
                    $("#newLandOwnersContainer").children("div").attr("style", "");
                }
            }
        });
    },

    AddLandOwner: function () {
        var _url = Helper.GetBaseURL("Permit") + "Permit/AddLandOwner";

        $.ajax({
            url: _url,
            type: 'Get',
            data: { 'PermitId': $("#PermitId").val() },
            async: false,
            success: function (response) {
                $("#newLandOwnersContainer").append("<hr />" + response);
                var index = $("#newLandOwnersContainer").children("div").length - 1;

                $("#newLandOwnersContainer").children("div:last-child").find("input")
                    .attr('id', function () {
                        return "ContactForm_" + index + "__" + $(this).attr('id')
                    })
                    .attr('name', function () {
                        return "ContactForm[" + index + "]." + $(this).attr('name')
                    });

                $("#newLandOwnersContainer").children("div:last-child").find("select")
                    .attr('id', function () {
                        return "ContactForm_" + index + "__" + $(this).attr('id')
                    })
                    .attr('name', function () {
                        return "ContactForm[" + index + "]." + $(this).attr('name')
                    })
                    .change(function () {
                        $(this).val($(this).val());
                    });

                $("#newLandOwnersContainer").children("div:last-child").show();
            }
        });

        ContactSearch.Initialize('Generic', ApplicationWizard.PopulateLandOwner);
    },

    PopulateLandOwner: function () {
        var index = $("#newLandOwnersContainer").children("div").length - 1;

        $('#ContactForm_' + index + '__Id').val(ContactSearch.SelectedContact.Id);
        $('#ContactForm_' + index + '__UserId').val(ContactSearch.SelectedContact.userId);
        $('#ContactForm_' + index + '__FirstName').val(ContactSearch.SelectedContact.FirstName);
        $('#ContactForm_' + index + '__MiddleInitial').val(ContactSearch.SelectedContact.MiddleInitial);
        $('#ContactForm_' + index + '__LastName').val(ContactSearch.SelectedContact.LastName);
        $('#ContactForm_' + index + '__BusinessName').val(ContactSearch.SelectedContact.BusinessName);
        /*$('#ContactForm_' + index + '__SecondaryName').val(ContactSearch.SelectedContact.SecondaryName);*/
        $('#ContactForm_' + index + '__Address1').val(ContactSearch.SelectedContact.Address1);
        $('#ContactForm_' + index + '__Address2').val(ContactSearch.SelectedContact.Address2);
        $('#ContactForm_' + index + '__City').val(ContactSearch.SelectedContact.City);
        $('#ContactForm_' + index + '__StateId').val(ContactSearch.SelectedContact.StateId).change();
        $('#ContactForm_' + index + '__ZipCode').val(ContactSearch.SelectedContact.ZipCode);
        $('#ContactForm_' + index + '__PrimaryTelephoneNumber_CommunicationValue').val(ContactSearch.SelectedContact.PrimaryTelephoneNumber.CommunicationValue);
        $('#ContactForm_' + index + '__PrimaryTelephoneNumber_SelectedCommunicationMethodId').val(ContactSearch.SelectedContact.PrimaryTelephoneNumber.SelectedCommunicationMethodId).change();
        $('#ContactForm_' + index + '__AltTelephoneNumber_CommunicationValue').val(ContactSearch.SelectedContact.AltTelephoneNumber.CommunicationValue);
        $('#ContactForm_' + index + '__AltTelephoneNumber_SelectedCommunicationMethodId').val(ContactSearch.SelectedContact.AltTelephoneNumber.SelectedCommunicationMethodId).change();
        $('#ContactForm_' + index + '__EmailAddress_CommunicationValue').val(ContactSearch.SelectedContact.EmailAddress);
    },

    CreatePendingPermit: function () {
        var _url = Helper.GetBaseURL("Permit");
        _url += "Permit/CreatePendingPermit";

        $.ajax({
            url: _url,
            type: 'Get',
            async: false,
            data: { PermitId: $('#PermitId').val() },
            success: function (response) {
                if (!_.isNull(response)) {
                    $('#CreatePendingPermit').html(response);

                    //Bind the change event
                    $('#PermitTypeId').change(function () {
                        var selectedValue = $(this).val();
                        if (selectedValue == "") {
                            $('#PermitNumberContainer').hide();
                            $('#PermitNumberButtonContainer').hide();
                        }
                        else if (selectedValue == "5" || $("#PermitStatusId").val() == "46" || ($("#PermitStatusId").val() == 3 && ($("#ApplicationTypeId").val() == 3 || $("#ApplicationTypeId").val() == 2))) {
                            $('#PermitNumberContainer').hide();
                            $('#PermitNumberButtonContainer').show();
                        }
                        else {
                            var _url = Helper.GetBaseURL("Permit");
                            _url += "Permit/GeneratePermitNumber";

                            $.ajax({
                                url: _url,
                                type: 'Get',
                                async: false,
                                data: { PermitId: $('#PermitId').val() },
                                success: function (response) {
                                    if (!_.isNull(response)) {
                                        $('#PermitNumberContainer #PermitNumber').val(response.PermitNumber);
                                        $('#PermitNumberContainer #PermitNumber').mask("aa9999a999");
                                        $('#PermitNumberContainer').show();
                                        $('#PermitNumberButtonContainer').show();
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    },

    GetPermitDocument: function () {
        $.ajax({
            url: Helper.GetBaseURL("Permitting") + "Upload/GetPermitDocument",
            async: false,
            data: { PermitId: $('#PermitId').val() },
            success: function (response) {
                if (response.length > 0)
                    window.open(response);
            }
        });
    },

    ClonePermitVerify: function () {
        if ($("#PermitStatusId").val() == 46) {
            if ($('#PendingPermitTypeId').val() == "") {
                alert("Please select Pending Permit Type");
            }

            $.ajax({
                url: Helper.GetBaseURL("Permit") + "Permit/GetPendingPermitCount",
                data: { permitId: $('#PermitId').val() },
                type: 'get',
                success: function (response) {
                    if (response.count) {
                        alert("A pending modification, renewal, or exemption has already been created from this permit.");
                    }
                    else {
                        $("#confirmCreatePendingPermit").dialog({
                            modal: true,
                            position: 'center',
                            draggable: false,
                            resizable: false,
                            close: function () {
                                $(this).dialog("destroy");
                            },
                            buttons: {
                                "Yes": function () {
                                    ApplicationWizard.ClonePermit();
                                    $(this).dialog("close");
                                },
                                "No": function () {
                                    $(this).dialog("close");
                                },
                            }
                        });
                    }
                }
            });
        }
        else if ($("#PermitStatusId").val() == 3) {
            if ($("#ApplicationTypeId").val() == 1 || $("#ApplicationTypeId").val() == 4) {
                $("#confirmCreatePendingPermit").dialog({
                    modal: true,
                    position: 'center',
                    draggable: false,
                    resizable: false,
                    close: function () {
                        $(this).dialog("destroy");
                    },
                    buttons: {
                        "Yes": function () {
                            ApplicationWizard.ClonePermit();
                            $(this).dialog("close");
                        },
                        "No": function () {
                            $(this).dialog("close");
                        },
                    }
                });
            }
            else {
                $.ajax({
                    url: Helper.GetBaseURL("Permit") + "Permit/GetPendingPermitCount",
                    data: { permitId: $('#PermitId').val() },
                    type: 'get',
                    success: function (response) {
                        if (response.count) {
                            alert("A pending modification, renewal, or exemption has already been created from this permit.");
                        }
                        else {
                            $("#confirmCreatePendingPermit").dialog({
                                modal: true,
                                position: 'center',
                                draggable: false,
                                resizable: false,
                                close: function () {
                                    $(this).dialog("destroy");
                                },
                                buttons: {
                                    "Yes": function () {
                                        ApplicationWizard.ClonePermit();
                                        $(this).dialog("close");
                                    },
                                    "No": function () {
                                        $(this).dialog("close");
                                    },
                                }
                            });
                        }
                    }
                });
            }
        }
    },

    ClonePermit: function () {
        var baseUrl = Helper.GetBaseURL("Permit"),
            permitNumber = null,
            temp = null;

        if ($("#PermitStatusId").val() == 46) {
            temp = $("#PermitId").val();
        }
        else if ($("#PermitStatusId").val() == 3) {
            if ($("#ApplicationTypeId").val() == 2 || $("#ApplicationTypeId").val() == 3) {
                temp = $("#RefId").val();
            }
        }

        if (temp != null) {
            $.ajax({
                url: baseUrl + "Permit/GetPermitNumber",
                type: 'Get',
                async: false,
                data: { permitId: temp },
                success: function (response) {
                    if (!_.isNull(response)) {
                        permitNumber = response.permitNumber;
                    }
                }
            });
        }
        else {
            permitNumber = $('#PermitNumberContainer #PermitNumber').val();
        }

        if (permitNumber && ApplicationWizard.IsValidPermitFormat(permitNumber)) {
            if ($("#PermitStatusId").val() == "46" || ($("#PermitStatusId").val() == 3 && ($("#ApplicationTypeId").val() == 3 || $("#ApplicationTypeId").val() == 2))) {
                ApplicationWizard.DoCloning(permitNumber);
            }
            else {
                //Check permit number uniqueness and create pending permit
                $.ajax({
                    url: baseUrl + "Permit/IsUniquePermitNumber",
                    type: 'Get',
                    async: false,
                    data: { permitNumber: permitNumber },
                    success: function (response) {
                        if (!_.isNull(response)) {
                            if (response.isUnique == true) {
                                // Create Pending permit if the permit number is unique.
                                ApplicationWizard.DoCloning(permitNumber);
                            }
                            else {
                                alert("Permit Number is not unique. Please enter a unique value.");
                            }
                        }
                    }
                });
            }
        } else if ($("#ApplicationTypeId").val() == 1 || $("#ApplicationTypeId").val() == 4 || $("#ApplicationTypeId").val() == 5) {
            ApplicationWizard.DoCloning(permitNumber);
        }
        else {
            alert("Please enter permit number.");
        }
    },

    DoCloning: function (_permitNumber) {
        var _permitId = $('#PermitId').val();
        var _url = Helper.GetBaseURL("Permit");
        _url += "Permit/ClonePermit";
        $.ajax({
            url: _url,
            type: 'Post',
            async: false,
            data: { PermitId: _permitId, PermitTypeId: $('#PermitTypeId').val(), PermitNumber: _permitNumber.toUpperCase(), PendingPermitTypeId: $('#PendingPermitTypeId').val() },
            success: function (response) {
                if (response.clonedPermitId > 0) {
                    //  popup 
                    //  response.permitNumber
                    _url = Helper.GetBaseURL("Permit");

                    //Refresh the permit history user interface 
                    $.permitdetails.index('getNavControl').reload();

                    $("#PendingPermitCreatedMessage").html('New Pending Permit created Successfully').dialog({
                        modal: true,
                        position: "center",
                        width: 500,
                        buttons: {
                            "View/Edit Pending Permit": function () {
                                window.location.href = _url + "Permit?id=" + response.clonedPermitId;
                            },
                            "Create Another Pending Permit": function () {
                                ApplicationWizard.CreatePendingPermit();
                                $("#PendingPermitCreatedMessage").dialog('close');
                            }
                        }
                    });
                }
                else {
                    alert("There was an error while creating pending permit");
                }
            }
        });
    },

    IsValidPermitFormat: function (permitNumber) {
        return true;
    },

    Navigate: function (currentStep) {
        if (!global.ApplicantInformationChanged) {
            ApplicationWizard.InitializeGlobalVariable();
        }

        switch (currentStep) {
            case 1:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                ApplicantIdentification.Initialize();
                break;
            case 2:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUser.Initialize();
                $('#SaveButton').show();
                break;
            case 3:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                LandOwner.Initialize();
                $('#AddLandOwner').show();
                $('#SaveButton').show();
                $("#newLandOwnersContainer").show();
                break;
            case 4:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                Consultant.Initialize();
                $('#SaveButton').show();
                break;
            case 5:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUseDescription.Initialize();
                break;
            case 6:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUseCategoryAndType.Initialize();
                $('#SaveButton').show();
                break;
            case 7:
                var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
                if (selectedValues.length > 0) {
                    ApplicationWizard.ClearSelectedWizardStep();
                    ApplicationWizard.HideEverything();
                    WaterUseDetail.Initialize();
                }
                break;
            case 8:
                var selectedWaterUseCategoryType = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
                if (_.any(selectedWaterUseCategoryType, function (value) { return parseInt(value) > 11; })) {
                    ApplicationWizard.ClearSelectedWizardStep();
                    ApplicationWizard.HideEverything();
                    WastewaterTreatmentAndDisposal.Initialize();
                }
                break;
            case 9:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                $('.ApplicationSum').addClass('wizardStepActive');
                $("#reviewApplication").show();
                ApplicationWizard.LoadSummary();
                $('#SaveButton').hide();
                break;
            case 10:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                $('.CreatePendingPermit').addClass('wizardStepActive');
                $("#CreatePendingPermit").show();
                ApplicationWizard.CreatePendingPermit();
                break;
            case 11:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                $('.waterWithdrawalLocation').addClass('wizardStepActive');
                $("#identifyLocation").show();
                ApplicationWizard.IdentifyLocation.Initialize();
                break;
            case 12:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                PermitInformation.Initialize();
                $('#SaveButton').show();
                break;
            case 13:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                ApprovedAllocation.Initialize();
                $('#SaveButton').show();
                break;

            default:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUseDescription.Initialize();
                break;
        }
    },

    CollectSelectedWaterUseCategoryType: function () {
        // Get all selected values for water use category and type
        var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
        var commaSeparatedValues = _.reduce(selectedValues, function (memo, num) { return memo + num + ','; }, '');
        $('#SelectedWaterUsePurposeIds').val(commaSeparatedValues);
    },

    SaveAndResumeLater: function () {
        ApplicationWizard.CollectSelectedWaterUseCategoryType();
        //Save data -- submit form
        global.isSaveAndResume = true;
        ApplicationWizard.SaveData();
    },

    SaveData: function () {
        ApplicationWizard.CollectSelectedWaterUseCategoryType();
        var Message = "";

        if (global.ApplicantInformationChanged) {
            if (!global.waterUserContactFound) {
                Message = "Please add the water user contact";

                if (!global.LandOwnerContactFound) {
                    Message = "Please add the water user, and the land owner contact";
                }

                if (!global.ConsultantContactFound) {
                    Message = "Please add the water user, the land owner and the consultant contact";
                }
            }
            else if (!global.LandOwnerContactFound) {
                Message = "Please add the land owner contact";
                if (!global.ConsultantContactFound) {
                    Message = "Please add the land owner and consultant contact";
                }
            }
            else if (!global.ConsultantContactFound) {
                Message = "Please add the consultant contact";
            }

            if (Message.length > 0) {
                $('#WarningMessage').html(Message).dialog({
                    modal: true,
                    position: "center",
                    width: 500,
                    buttons: {
                        "OK": function () {
                            $("#WarningMessage").dialog('close');
                        }
                    }
                });
            }
        }
        else {
            $('#wizardForm').submit();
        }
    },

    SaveCallBack: function (response) {
        if (response.success) {
            if ($('#permitInformationForm').length) {
                $('#permitInformationForm').submit();
            }
            else if ($('#approvedAllocationForm').length) {
                $('#approvedAllocationForm').submit();
            }
            else {
                alert('Saved successfully');
                $.permitdetails.index('reload');
            }

            ////prepare the add additional land owners form
            //$("#addLandOwnerForm").empty();
            //$("#newLandOwnersContainer").clone().appendTo("#addLandOwnerForm");
            //$("#addLandOwnerForm").append('<input type="hidden" id="PermitId" name="PermitId" value=' + $("#PermitId").val() + '>');

            ////appending state ids (without doing this, stateIds will not be sent via form submit)
            //var items = $("#newLandOwnersContainer").find("select").length;
            //for (i = 0; i < items; i++) {
            //    $("select#ContactForm_" + i + "__StateId").val($("select#ContactForm_" + i + "__StateId").val());
            //}

            //$('#addLandOwnerForm').submit();
        }
        else {
            alert('There is a data quality issue that is preventing this permit from saving. Please verify that all required data has been provided for each permit detail section and then save.');
        }
    },

    //SaveCallBackAdditionalLandOwners: function (response) {
    //    if (response.success) {
    //        for (var i = 0; i < response.Data.length; i++) {
    //            $("#newLandOwnersContainer").find('#ContactForm_' + i + '__Id').val(response.Data[i])
    //        }
    //        ContactSearchResult.ClearValidationError();

    //    }
    //    else {
    //        alert('There is a data quality issue that is preventing this permit from saving. Please verify that all required data has been provided for each permit detail section and then save.');
    //    }
    //},

    SaveCallBackPermitInformation: function (response) {
        if (response.success) {
            if ($('#approvedAllocationForm').length) {
                $('#approvedAllocationForm').submit();
            }
            else {
                alert('Saved successfully');
                $.permitdetails.index('reload');
            }
        }
        else {
            alert('There is a data quality issue that is preventing this permit from saving. Please verify that all required data has been provided for each permit detail section and then save.');
        }
    },

    SaveCallBackApprovedAllocation: function (response) {
        if (response.success) {
            alert('Saved successfully');
            $.permitdetails.index('reload');
        }
        else {
            alert('There is a data quality issue that is preventing this permit from saving. Please verify that all required data has been provided for each permit detail section and then save.');
        }
    },

    DisplayErrorMessage: function (errorMessage) {
        $('#MessageSummary').html(errorMessage).show();
    },

    LoadwaterUseCatagoryAndTypeView: function () {
        WaterUseCategoryAndType.LoadwaterUseCatagoryAndTypeView();
    },

    LoadwaterUseDetailView: function () {
        var permitId = $('#PermitId').val();
        WaterUseDetail.LoadwaterUseDetailView(permitId);
    },

    RenderbodyRightWidth: function () {
        return $('.renderbodyRight').width() - 50;
    },

    HighlightCurrentWizardStep: function ($div) {
        $div.show();
        $div.addClass('wizardStepActive');
    },

    ClearSelectedWizardStep: function () {
        $("#leftSide .wizardStepActive").removeClass("wizardStepActive");
    },

    InitializeGlobalVariable: function () {
        // When a permit is resumed, we assume that all contact types are found. 
        global.ConsultantContactFound = true;
        global.waterUserContactFound = true;
        global.LandOwnerContactFound = true;
    },

    LoadSummary: function () {
        var ids = global.SelectedWaterUseCategoryTypeList.toString();

        $.ajax({
            url: urls.summaryUrl,
            type: 'Get',
            async: false,
            data: { id: $("#PermitId").val(), waterDetailsSelected: ids },
            success: function (response) {
                $("#applicationSummary").html(response);
                $("#applicationSummary").append('<div><span class="spanLink" onclick="ApplicationWizard.Print();">Print Permit Summary</span></div>');
            }
        });
    },

    Print: function () {
        var url = Helper.GetBaseURL("Permit") + "Permit/CreatePdf/";
        var ids = global.SelectedWaterUseCategoryTypeList.toString();

        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", url);

        // setting form target to a window named 'formresult'
        form.setAttribute("target", "formresult");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("name", "waterDetailsSelected");
        hiddenField.setAttribute("value", ids);
        form.appendChild(hiddenField);

        var hiddenField2 = document.createElement("input");
        hiddenField2.setAttribute("name", "id");
        hiddenField2.setAttribute("value", $("#PermitId").val());
        form.appendChild(hiddenField2);

        document.body.appendChild(form);

        // creating the 'formresult' window with custom features prior to submitting the form
        window.open("", 'formresult', '');

        form.submit();
    }
};