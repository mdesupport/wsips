﻿/// <reference path="_permitReference.js" />

$("input[id$='IsWastewaterDischarge']").change(function (e) {
    WastewaterTreatmentAndDisposal.SetupView($(this));
});

$("select[id$='WaterDispersementTypeId']").change(function () {
    if ($(this).val() == '4') {
        $(this).parent().parent().find('.natureOfGroundwaterDischarge').show();
    }
    else {
        $(this).parent().parent().find('.natureOfGroundwaterDischarge').hide();
    }
});


//$.extend(WastewaterTreatmentAndDisposal, {});