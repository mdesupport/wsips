﻿/// <reference path="../../../jquery-1.8.2.intellisense.js" />

var PermitInformation = {
    Initialize: function () {
        $('.permitInformation').addClass('wizardStepActive');

        $.ajax({
            url: $('#PermitInformation').attr('data-contenturl'),
            type: 'GET',
            data: { id: $('#PermitId').val() },
            success: function (response) {
                $('#PermitInformation').html(response).show();
                $('#PermitInformation input[type=checkbox]').checkbox();
                $('#PermitInformation input[id$=PWSID]').mask('9999999');
            }
        });
    }
}