﻿(function () {
    window.PumpageReportingPage = function () {
        var janJun = $('#month1, #month2, #month3, #month4, #month5, #month6'),
            julDec = $('#month7, #month8, #month9, #month10, #month11, #month12'),
            multiplyBy = 27154;

        if ($('#CropTypeId').val() === '12') $('#OtherWaterWithdrawalMethodContainer').show();

        janJun.keyup(function (e) {
            var subTotal = 0,
                monthly = 0;
            janJun.each(function () {
                if ($(this).val() === '') return;
                monthly = parseFloat($(this).val().replace(/,/g, ''));
                subTotal = subTotal + monthly;
            });
            $('#janJunTotal').val(subTotal == 0 ? '' : subTotal);
            maskDigits();
        });

        julDec.keyup(function (e) {
            var subTotal = 0,
                monthly = 0;
            julDec.each(function () {
                if ($(this).val() === '') return;
                monthly = parseFloat($(this).val().replace(/,/g, ''));
                subTotal = subTotal + monthly;
            });
            $('#julDecTotal').val(subTotal == 0 ? '' : subTotal);
            maskDigits();
        });
        
        maskDigits();
        $("#Phone").mask("999-999-9999");
        $('.date').attr("readonly", false).mask("99/99/9999");

        $('.witdrawalEstimate').checkbox();
        if ($('#CropTypeId').val() != undefined) $('#9_chkBx').hide();

        $('.witdrawalEstimate').change(function () {
            var id = $(this).attr('id');
            $(".witdrawalEstimate").each(function () {
                var item = $(this),
                    itemId = item.prop('id');

                if (itemId != id) {
                    item.prop('checked', false);
                    $('#' + itemId + '_chkBx .allCheckboxImage').removeClass('checked');
                }
            });
        });

        $('#CropTypeId').change(function () {
            var val = $(this).val();
            if (val === '12') {
                $('#OtherWaterWithdrawalMethodContainer').show();
            } else {
                $('#OtherWaterWithdrawalMethodContainer').hide();
            }
        });

        $('#inches1, #acres1').keyup(function () { updateTotal(1, janJun, multiplyBy); });
        $('#inches2, #acres2').keyup(function () { updateTotal(2, janJun, multiplyBy); });
        $('#inches3, #acres3').keyup(function () { updateTotal(3, janJun, multiplyBy); });
        $('#inches4, #acres4').keyup(function () { updateTotal(4, janJun, multiplyBy); });
        $('#inches5, #acres5').keyup(function () { updateTotal(5, janJun, multiplyBy); });
        $('#inches6, #acres6').keyup(function () { updateTotal(6, janJun, multiplyBy); });

        $('#inches7, #acres7').keyup(function () { updateTotal(7, julDec, multiplyBy); });
        $('#inches8, #acres8').keyup(function () { updateTotal(8, julDec, multiplyBy); });
        $('#inches9, #acres9').keyup(function () { updateTotal(9, julDec, multiplyBy); });
        $('#inches10, #acres10').keyup(function () { updateTotal(10, julDec, multiplyBy); });
        $('#inches11, #acres11').keyup(function () { updateTotal(11, julDec, multiplyBy); });
        $('#inches12, #acres12').keyup(function () { updateTotal(12, julDec, multiplyBy); });

        $('.date').datepicker({ changeMonth: true, changeYear: true });
    };
})();

function maskDigits() {
    $(".commaFormatted.twoDecimals").maskMoney({ precision: 2, defaultZero: false, allowZero: true }).maskMoney('mask');
    $(".commaFormatted.noDecimal").maskMoney({ precision: 0, defaultZero: true, allowZero: true }).maskMoney('mask');
}

function updateTotal(index, selector, coefficient) {
    var i = $('#inches' + index).val(),
        a = $('#acres' + index).val(),
        m = $('#month' + index);

    if (!i.length || isNaN(i) || !a.length || isNaN(a)) return;

    m.val((parseFloat(i) * parseFloat(a) * coefficient).toFixed(2));

    selector.keyup();
}

// Load model data from the form
function loadData() {
    var data = {
        Id: $('#PumpageReportId').val(),
        PermitConditionId: $('#PermitConditionId').val(),
        ConditionComplianceId: $('#ConditionComplianceId').val(),
        Name: $('#Name').val(),
        WaterWithdrawalEstimateId: $('.witdrawalEstimate:checked').attr('id'),
        CropTypeId: $('#CropTypeId').val(),
        OtherCropDescription: $('#OtherCropDescription').val(),
        OtherWaterWithdrawalMethod: $('#OtherWaterWithdrawalMethod').val(),
        ReportYear: $('#ReportYear').val(),
        SubmittedBy: $('#SubmittedBy').val(),
        ReceivedDate: $('#ReceivedDate').val(),
        Phone: $('#Phone').val(),
        SubmittedDate: $('#SubmittedDate').val(),
        IsResumbission: $('#IsResumbission').val(),
        PumpageReportDetails: []
    };
    for (var i = 1; i <= 12; i++) {

        if ($('#month' + i).length != 0) {
            var item = {
                Id: $('#month' + i).attr('alt'),
                MonthId: i,
                PumpageReportId: $('#PumpageReportId').val(),
                Gallons: parseFloat($('#month' + i).val().replace(/,/g, '')).toFixed(),
                InchesApplied: $('#inches' + i).val(),
                Acres: $('#acres' + i).val(),
            };
            data.PumpageReportDetails.push(item);
        }
    }
    return data;
}

// Handle server error
function HandleError(err) {
    var container = $("div[data-valmsg-summary=true]"),
        list = container.find("ul");

    if (list && list.length) {
        container.addClass("validation-summary-errors").removeClass("validation-summary-valid");
        $("<li />").html(err).appendTo(list);
    }
}