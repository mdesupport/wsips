﻿$(function () {
    $('#additionalRoles input[type=checkbox]').change(function (e) {
        if ($('#additionalRoles input[type=checkbox]:checked').length) {
            $('#defaultRole input[type=checkbox]').prop('checked', false).change();
        }
        else {
            $('#defaultRole input[type=checkbox]').prop('checked', true).change();
        }
    });

    $("#MDE_Staff").change(function (e) {
        if ($(this).is(':checked') && $('#additionalRoles input[type=checkbox]:checked').length) {
            $('#additionalRoles input[type=checkbox]').prop('checked', false).change();
        }
    });
});