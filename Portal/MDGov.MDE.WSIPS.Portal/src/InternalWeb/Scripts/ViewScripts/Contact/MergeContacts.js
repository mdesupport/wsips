﻿$.extend(ContactSearchResult, {
    DivId: "",

    EditContact: function (id) {
        $.ajax({
            url: urls.getContactAction,
            data: { id: id },
            cache: false,
            success: function (response) {
                ContactSearchResult.PopulateContactValues(0, response);
            }
        });
    },
    
    UserNotFound: function () {
        ContactSearchResult.PopulateContactValues(0, null);
    },

    DoMerge: function ($jqObj) {
        var toContact = $jqObj.attr("contactid");
        var fromContact;

        if ($('.keepbutton').first().attr('contactid') == toContact) {
            fromContact = $('.keepbutton').last().attr('contactid');
        } else {
            fromContact = $('.keepbutton').first().attr('contactid');
        }

        $.ajax({
            url: urls.mergeContactsAction,
            data: { fromContactId: fromContact, toContactid: toContact },
            type: 'POST',
            success: function (response) {
                $("#MergedSuccessfullyDialog").dialog({
                    modal: true,
                    position: "center",
                    width: 700,
                    close: function() {
                        window.location.reload();
                    }
                });
            }
        });
    },

    PopulateContactValues: function (contact) {
        $(".ui-dialog-content").dialog("close");

        $('#' + ContactSearchResult.DivId).find(".contactContainer").show();
        $('#' + ContactSearchResult.DivId).find(".keepbutton").show();
        $('#' + ContactSearchResult.DivId).find(".keepbutton").attr('userid', contact.UserId);
        $('#' + ContactSearchResult.DivId).find(".keepbutton").attr('contactid', contact.Id);

        $('#' + ContactSearchResult.DivId).find('.firstname').html(contact.FirstName);
        $('#' + ContactSearchResult.DivId).find('.middleInitial').html(contact.MiddleInitial);
        $('#' + ContactSearchResult.DivId).find('.lastName').html(contact.LastName);
        $('#' + ContactSearchResult.DivId).find('.businessName').html(contact.BusinessName);
        $('#' + ContactSearchResult.DivId).find('.address1').html(contact.Address1);
        $('#' + ContactSearchResult.DivId).find('.address2').html(contact.Address2);
        $('#' + ContactSearchResult.DivId).find('.city').html(contact.City);
        /*$('#' + ContactSearchResult.DivId).find('.secondaryName').html(contact.SecondaryName);*/
        $('#' + ContactSearchResult.DivId).find('.stateId').html(contact.StateId).change();
        $('#' + ContactSearchResult.DivId).find('.zipCode').html(contact.ZipCode);
        $('#' + ContactSearchResult.DivId).find('.telephoneNumber').html(contact.PrimaryTelephoneNumber.CommunicationValue);
        $('#' + ContactSearchResult.DivId).find('.emailAddress').html(contact.EmailAddress.CommunicationValue);

        if ($(".keepbutton:visible").length == 2) {
            if ($("body").find('[userid="0"]').length == 2) { // if both selected contacts don't have a userid, enable buttons
                $('.keepbutton').removeClass('disabled ui-button-disabled ui-state-disabled').prop('disabled', false);
            } else if ($("body").find('[userid="0"]').length == 0) { // if both selected contacts have a userid, enable buttons
                $('.keepbutton').removeClass('disabled ui-button-disabled ui-state-disabled').prop('disabled', false);
            } else if ($("body").find('[userid="0"]').length == 1) {
                if ($('.keepbutton').first().attr('userid') == 0) {
                    $('.keepbutton').last().removeClass('disabled ui-button-disabled ui-state-disabled').prop('disabled', false);
                    $('.keepbutton').first().addClass('disabled ui-button-disabled ui-state-disabled').prop('disabled', true);
                } else {
                    $('.keepbutton').first().removeClass('disabled ui-button-disabled ui-state-disabled').prop('disabled', false);
                    $('.keepbutton').last().addClass('disabled ui-button-disabled ui-state-disabled').prop('disabled', true);
                }
            }
        }
    },

    BeginSearch: function (divId) {
        ContactSearchResult.DivId = divId;

        ContactSearch.SearchGeneric();
    },

    SaveCallBack: function (response) {
        $(".ui-dialog-content").dialog("close");
    }
});