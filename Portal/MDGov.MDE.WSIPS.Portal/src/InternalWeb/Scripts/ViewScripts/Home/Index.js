﻿
function setNotificationCount(count) {
    var plural = "";
    if (count != null) {
        if (count < 2) {
            plural = "s"
        }
        $('#notifications-link').text(count + ' New Notification' + plural);
    }
}
function getNotificationCount() {
    var rtnCount = 0;
    $(".notificationContainer").each(function () {
        if ($(this).is(":visible")) {
            rtnCount += 1;
        }
    });
    return rtnCount;
}

function checkLastNotificationUnderline() {
    //get last notification
    var $lastP = $(".notificationContainer:visible").last().find("p");
    //remove underline if it's there
    if ($lastP.hasClass("link")) { $lastP.removeClass("link") }
}

function getNotificationContainerFromHideNotificationSpan($span) {
    if ($span != null) {
        return $span.closest(".notificationContainer");
    }
    else {
        alert("you must provide a valid span for this function");
    }
}

function closeAllMenus(close) {
    $("#notifications-container").slideUp();
    $("#myaccount-container").slideUp();
}

function makeScrollbarsGreen() {
    //use green color class for scrollbar display
    $(".slideContainerScrollerContainer").find(".scrollblock").addClass("LNPulldownScrollBarColor");
    $(".slideContainerScrollerContainer").find(".scrollbtn").addClass("LNPulldownScrollBtnColor");
}