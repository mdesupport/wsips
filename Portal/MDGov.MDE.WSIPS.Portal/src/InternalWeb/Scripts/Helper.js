﻿var Helper = {
    GetBaseURL: function (urlPart) {
        var currentUrl = document.location;
        var splitStringArray = currentUrl.href.split('?')[0].split('/');
        var baseUrl = '';

        for (var i = 0; i < splitStringArray.length; i++) {
            if (splitStringArray[i] == urlPart) {
                break;
            }
            else {
                baseUrl += splitStringArray[i] + "/";
            }
        }

        return baseUrl;
    },

    GetParameterValueByName: function (name, content) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(content);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    GetHashValueByName: function (name, content) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\#&]" + name + "=([^&#]*)"),
            results = regex.exec(content);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
};