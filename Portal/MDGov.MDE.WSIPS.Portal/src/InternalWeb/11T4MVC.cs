﻿// Decompiled with JetBrains decompiler
// Type: T4MVC.ReportingClass
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Reporting.Controllers;
using System.CodeDom.Compiler;
using System.Diagnostics;

namespace T4MVC
{
  [GeneratedCode("T4MVC", "2.0")]
  [DebuggerNonUserCode]
  public class ReportingClass
  {
    public readonly string Name = "Reporting";
    public ReportsController Reports = (ReportsController) new T4MVC_ReportsController();
    public T4MVC.Reporting.SharedController Shared = new T4MVC.Reporting.SharedController();
  }
}
