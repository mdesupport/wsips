﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.T4MVC_PermitController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [GeneratedCode("T4MVC", "2.0")]
  [DebuggerNonUserCode]
  public class T4MVC_PermitController : PermitController
  {
    public T4MVC_PermitController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult Index(int id, PermitDetailsTab? activeTab)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Index, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "activeTab", (object) activeTab);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult GetPermit(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPermitId(string permitName)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitId, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitName", (object) permitName);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult LoadDetailsTab(int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadDetailsTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override PartialViewResult LoadReviewTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadReviewTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadConditionsTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadConditionsTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadIPLTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadIPLTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult GetIplContact(int permitContactId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.GetIplContact, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitContactId", (object) permitContactId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadCCListTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadCCListTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadPumpageContactTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadPumpageContactTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadDocumentsTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadDocumentsTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadCommentsTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadCommentsTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadComplianceTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadComplianceTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadEnforcementTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadEnforcementTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override PartialViewResult LoadContactsTab(int permitId)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadContactsTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult GetPermitHistory(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitHistory, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPermitTypes()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitTypes, (string) null);
    }

    [NonAction]
    public override JsonResult SavePermitType(int id, int permitTypeId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SavePermitType, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitTypeId", (object) permitTypeId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetSupplementalTypeList()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetSupplementalTypeList, (string) null);
    }

    [NonAction]
    public override JsonResult SearchSupplementalGroupGridData(GridCommand command, string permitNames)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchSupplementalGroupGridData, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitNames", (object) permitNames);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult ViewSupplementalGroupGridData(GridCommand command, int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ViewSupplementalGroupGridData, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SetSupplementalGroupType(int permitId, int newSupplementalTypeId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetSupplementalGroupType, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "newSupplementalTypeId", (object) newSupplementalTypeId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult AddToSupplementalGroup(int permitId, int groupId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddToSupplementalGroup, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "groupId", (object) groupId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult RemoveFromSupplementalGroup(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemoveFromSupplementalGroup, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult CreateNewSupplementalGroup(int activePermitId, string additionalPermitNames)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CreateNewSupplementalGroup, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "activePermitId", (object) activePermitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "additionalPermitNames", (object) additionalPermitNames);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult DenyPermit(int PermitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DenyPermit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "PermitId", (object) PermitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult ApprovePermit(int PermitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ApprovePermit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "PermitId", (object) PermitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SetPermitDates(int permitId, string effectiveDate, string expirationDate, string appropriationDate)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPermitDates, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "effectiveDate", (object) effectiveDate);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "expirationDate", (object) expirationDate);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "appropriationDate", (object) appropriationDate);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPermitDates(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitDates, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult DateElevenMonthsElevenYears(DateTime date)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DateElevenMonthsElevenYears, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "date", (object) date);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult LoadAdditionalLandOwners(int PermitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadAdditionalLandOwners, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "PermitId", (object) PermitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult LoadPermitHearing(int Permitid, int PermitStatusId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadPermitHearing, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "Permitid", (object) Permitid);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "PermitStatusId", (object) PermitStatusId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult SavePermitHearing(PermitHearingForm permitHearing)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SavePermitHearing, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitHearing", (object) permitHearing);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult DeletePermitHearing(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeletePermitHearing, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPermitHearing(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitHearing, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPermitHearingByPermitId(int PermitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitHearingByPermitId, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "PermitId", (object) PermitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult AddPumpageContact(PumpageContactForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddPumpageContact, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult RemovePermitContact(int permitContactId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemovePermitContact, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitContactId", (object) permitContactId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult LoadChecklist(int id, int permitStatus)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadChecklist, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitStatus", (object) permitStatus);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult SaveContactsInDetailsContactsTab(DetailsPermitContactForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SaveContactsInDetailsContactsTab, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult LoadPermitInformation(int id)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadPermitInformation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "id", (object) id);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult SavePermitInformation(PermitInformationForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SavePermitInformation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult LoadApprovedAllocation(int id)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadApprovedAllocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "id", (object) id);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult SaveApprovedAllocation(AllocationInformationForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SaveApprovedAllocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult GenerateAveryLabelsForIPL(int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.GenerateAveryLabelsForIPL, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult CopyConditions(int permitId, int permitTemplateId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CopyConditions, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitTemplateId", (object) permitTemplateId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult ResumeApplication()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.ResumeApplication, (string) null);
    }

    [NonAction]
    public override JsonResult LoadApplicationsInProgress(GridCommand command)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.LoadApplicationsInProgress, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult ApplicationWizard(int? id, int? applicationType)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.ApplicationWizard, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "applicationType", (object) applicationType);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult CancelPermitApplication(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CancelPermitApplication, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult LinksModal(string permitNumber, string linkType)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.LinksModal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitNumber", (object) permitNumber);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "linkType", (object) linkType);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult ApplicationWizard(ApplicationWizard form, WaterUseDetailForm waterUserDetailForm)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ApplicationWizard, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "waterUserDetailForm", (object) waterUserDetailForm);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult CreatePendingPermit(int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreatePendingPermit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult GetPendingPermitCount(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPendingPermitCount, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult CreatePendingPermit(PendingPermitForm form)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreatePendingPermit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "form", (object) form);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult GeneratePermitNumber(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GeneratePermitNumber, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult IsUniquePermitNumber(string permitNumber)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.IsUniquePermitNumber, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitNumber", (object) permitNumber);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult ClonePermit(PendingPermitForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ClonePermit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPermitNumber(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitNumber, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SetPermitNumber(int permitId, string permitNumber)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPermitNumber, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitNumber", (object) permitNumber);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPermitStatusesByCategory(int categoryId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitStatusesByCategory, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "categoryId", (object) categoryId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SetPermitStatus(int permitId, int permitStatusId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPermitStatus, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitStatusId", (object) permitStatusId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SetPreviousPermitStatus(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPreviousPermitStatus, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SearchContact(GridCommand command, ContactSearchForm searchForm)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchContact, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "searchForm", (object) searchForm);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult AddPermitLocation(int permitId, double x, double y, int srid)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddPermitLocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "x", (object) x);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "y", (object) y);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "srid", (object) srid);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult RemovePermitLocation(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemovePermitLocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult AddWaterWithdrawalLocation(int permitId, double x, double y, int srid)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.AddWaterWithdrawalLocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "x", (object) x);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "y", (object) y);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "srid", (object) srid);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult AddWaterWithdrawalLocation(WithdrawalLocationForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddWaterWithdrawalLocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetConservationEasement(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetConservationEasement, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SetConservationEasement(int permitId, bool? isSubjectToEasement, bool? haveNotifiedEasementHolder)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetConservationEasement, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "isSubjectToEasement", (object) isSubjectToEasement);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "haveNotifiedEasementHolder", (object) haveNotifiedEasementHolder);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult EditWaterWithdrawalLocation(int id)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.EditWaterWithdrawalLocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "id", (object) id);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult EditWaterWithdrawalLocation(WithdrawalLocationForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.EditWaterWithdrawalLocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult RemoveWaterWithdrawalLocation(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemoveWaterWithdrawalLocation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult Summary(int id, string waterDetailsSelected)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Summary, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "waterDetailsSelected", (object) waterDetailsSelected);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult Affirmation(int id, string submittedBy, string submittedDate)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Affirmation, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "submittedBy", (object) submittedBy);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "submittedDate", (object) submittedDate);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult UpdateStatus(int permitId, int permitStatusId, bool completed)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateStatus, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitStatusId", (object) permitStatusId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "completed", (object) completed);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GenerateComplianceReports(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateComplianceReports, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult DeleteComplianceReports(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeleteComplianceReports, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override FileResult CreatePdf(int id, string waterDetailsSelected)
    {
      T4MVC_System_Web_Mvc_FileResult webMvcFileResult = new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.CreatePdf, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcFileResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcFileResult.RouteValueDictionary, "waterDetailsSelected", (object) waterDetailsSelected);
      return (FileResult) webMvcFileResult;
    }
  }
}
