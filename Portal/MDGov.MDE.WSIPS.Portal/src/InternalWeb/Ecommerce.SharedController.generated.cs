﻿// Decompiled with JetBrains decompiler
// Type: T4MVC.Ecommerce.SharedController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;

namespace T4MVC.Ecommerce
{
  public class SharedController
  {
    private static readonly SharedController.ViewsClass s_views = new SharedController.ViewsClass();

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public SharedController.ViewsClass Views
    {
      get
      {
        return SharedController.s_views;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly SharedController.ViewsClass._ViewNamesClass s_ViewNames = new SharedController.ViewsClass._ViewNamesClass();
      private static readonly SharedController.ViewsClass._EditorTemplatesClass s_EditorTemplates = new SharedController.ViewsClass._EditorTemplatesClass();

      public SharedController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return SharedController.ViewsClass.s_ViewNames;
        }
      }

      public SharedController.ViewsClass._EditorTemplatesClass EditorTemplates
      {
        get
        {
          return SharedController.ViewsClass.s_EditorTemplates;
        }
      }

      public class _ViewNamesClass
      {
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public class _EditorTemplatesClass
      {
        public readonly string FeeForm = "FeeForm";
        public readonly string FeeRuleForm = "FeeRuleForm";
        public readonly string GraduatedFeeForm = "GraduatedFeeForm";
      }
    }
  }
}
