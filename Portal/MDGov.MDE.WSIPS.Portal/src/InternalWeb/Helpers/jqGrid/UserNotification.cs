﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Models.UserNotification
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Models
{
  public class UserNotification
  {
    public int Id { get; set; }

    public int? NotificationTypeId { get; set; }

    public string NotificationText { get; set; }

    public string PermitId { get; set; }

    public string PermitName { get; set; }

    public string CreatedDate { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public bool? Active { get; set; }
  }
}
