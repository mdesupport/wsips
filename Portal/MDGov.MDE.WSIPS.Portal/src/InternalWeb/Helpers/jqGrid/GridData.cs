﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Models.GridData
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Collections;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Models
{
  public class GridData
  {
    public int PageTotal { get; set; }

    public int CurrentPage { get; set; }

    public int RecordCount { get; set; }

    public IEnumerable Data { get; set; }

    public GridData()
    {
    }

    public GridData(IEnumerable data)
    {
      this.Data = data;
    }
  }
}
