﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Models.UserListing
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Models
{
  public class UserListing
  {
    public int UserId { get; set; }

    public string UserName { get; set; }

    public string LastName { get; set; }

    public int UserTypeId { get; set; }

    public string UserType { get; set; }

    public virtual IEnumerable<string> Roles { get; set; }
  }
}
