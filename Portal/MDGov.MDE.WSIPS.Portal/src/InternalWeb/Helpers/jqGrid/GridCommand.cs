﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Models.GridCommand
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Models
{
  [ModelBinder(typeof (GridCommandModelBinder))]
  public class GridCommand
  {
    public int Page { get; set; }

    [Binding(Name = "rows")]
    public int PageSize { get; set; }

    [Binding(Name = "sidx")]
    public string SortField { get; set; }

    [Binding(Name = "sord")]
    public string SortOrder { get; set; }

    public int Skip()
    {
      return (this.Page - 1) * this.PageSize;
    }

    public string BuildSort(IDictionary<string, string> propertyMap)
    {
      if (string.IsNullOrEmpty(this.SortField))
        return (string) null;
      return propertyMap[this.SortField] + " " + this.SortOrder;
    }
  }
}
