﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Comparer.StandardConditionComparer
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Comparer
{
  public class StandardConditionComparer : IEqualityComparer<LU_StandardCondition>
  {
    public bool Equals(LU_StandardCondition x, LU_StandardCondition y)
    {
      return x.Description.Equals(y.Description);
    }

    public int GetHashCode(LU_StandardCondition obj)
    {
      return obj.Description.GetHashCode();
    }
  }
}
