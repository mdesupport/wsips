﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ActiveDirectory.DirectoryHelper
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Security.Principal;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ActiveDirectory
{
  public class DirectoryHelper
  {
    private readonly DirectoryEntry _root = new DirectoryEntry(ConfigurationManager.AppSettings["ActiveDirectory"]);

    public IEnumerable<DirectoryUser> FindUsers(string filter, int pageSize)
    {
      List<DirectoryUser> directoryUserList = new List<DirectoryUser>();
      DirectorySearcher directorySearcher = new DirectorySearcher() { SearchRoot = this._root, SearchScope = SearchScope.Subtree, Filter = string.Format("(&(objectCategory=person)(objectClass=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(|(sAMAccountName=*{0}*)(givenName=*{0}*)(sn=*{0}*)))", (object) filter) };
      try
      {
        foreach (SearchResult searchResult in directorySearcher.FindAll())
        {
          string str = ((NTAccount) new SecurityIdentifier((byte[]) searchResult.Properties["objectsid"][0], 0).Translate(typeof (NTAccount))).ToString();
          object obj1 = searchResult.GetDirectoryEntry().Properties["givenName"].Value;
          object obj2 = searchResult.GetDirectoryEntry().Properties["sn"].Value;
          if (obj2 != null)
            directoryUserList.Add(new DirectoryUser()
            {
              UserName = str == null ? string.Empty : str.ToString(),
              FirstName = obj1 == null ? string.Empty : obj1.ToString(),
              LastName = obj2.ToString()
            });
        }
      }
      catch (Exception ex)
      {
        ObjectFactory.GetInstance<IErrorLogging>().Log(ex);
      }
      return (IEnumerable<DirectoryUser>) directoryUserList;
    }

    public IEnumerable<DirectoryUser> GetAllUsers(int pageSize)
    {
      List<DirectoryUser> directoryUserList = new List<DirectoryUser>();
      DirectorySearcher directorySearcher = new DirectorySearcher() { PageSize = pageSize, SizeLimit = 0, SearchRoot = this._root, SearchScope = SearchScope.Subtree, Filter = string.Format("(&(objectCategory=person)(objectClass=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))") };
      try
      {
        foreach (SearchResult searchResult in directorySearcher.FindAll())
        {
          object obj1 = searchResult.GetDirectoryEntry().Properties["sAMAccountName"].Value;
          object obj2 = searchResult.GetDirectoryEntry().Properties["givenName"].Value;
          object obj3 = searchResult.GetDirectoryEntry().Properties["sn"].Value;
          if (obj3 != null)
            directoryUserList.Add(new DirectoryUser()
            {
              UserName = obj1 == null ? string.Empty : obj1.ToString(),
              FirstName = obj2 == null ? string.Empty : obj2.ToString(),
              LastName = obj3.ToString()
            });
        }
      }
      catch (Exception ex)
      {
        ObjectFactory.GetInstance<IErrorLogging>().Log(ex);
      }
      return (IEnumerable<DirectoryUser>) directoryUserList;
    }
  }
}
