﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Geometry.EsriJsonWktParser
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Geometry
{
  public class EsriJsonWktParser
  {
    public string WellKnownText { get; set; }

    public int SpatialReference { get; set; }

    public EsriJsonWktParser(string wellKnownText, int spatialReference)
    {
      this.WellKnownText = wellKnownText;
      this.SpatialReference = spatialReference;
    }

    public object Parse()
    {
      switch (this.WellKnownText.Substring(0, this.WellKnownText.IndexOf('(')).Trim())
      {
        case "POINT":
          throw new NotImplementedException();
        case "POLYGON":
        case "MULTIPOLYGON":
          return this.ParsePolygon(this.WellKnownText.Substring(this.WellKnownText.IndexOf('(')));
        case "MULTIPOINT":
          throw new NotImplementedException();
        default:
          throw new Exception("Invalid Well Known Text");
      }
    }

    private object ParsePolygon(string rings)
    {
      string[] strArray1 = rings.Split(new string[3]{ "(", "), (", ")" }, StringSplitOptions.RemoveEmptyEntries);
      string[][][] strArray2 = new string[strArray1.Length][][];
      for (int index1 = 0; index1 < strArray1.Length; ++index1)
      {
        string[] strArray3 = strArray1[index1].Split(new char[1]{ ',' }, StringSplitOptions.RemoveEmptyEntries);
        strArray2[index1] = new string[strArray3.Length][];
        for (int index2 = 0; index2 < strArray3.Length; ++index2)
        {
          string[] strArray4 = strArray3[index2].Trim().Split(' ');
          strArray2[index1][index2] = strArray4;
        }
      }
      return (object) new{ rings = strArray2, spatialReference = new{ wkid = this.SpatialReference } };
    }
  }
}
