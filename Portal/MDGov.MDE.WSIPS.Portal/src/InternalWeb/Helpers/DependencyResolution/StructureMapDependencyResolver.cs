﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.DependencyResolution.StructureMapDependencyResolver
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using StructureMap;
using System;
using System.Web.Http.Dependencies;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.DependencyResolution
{
  public class StructureMapDependencyResolver : StructureMapDependencyScope, IDependencyResolver, IDependencyScope, IDisposable
  {
    public StructureMapDependencyResolver(IContainer container)
      : base(container)
    {
    }

    public IDependencyScope BeginScope()
    {
      return (IDependencyScope) new StructureMapDependencyResolver(this.Container.GetNestedContainer());
    }
  }
}
