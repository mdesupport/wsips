﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.DependencyResolution.IoC
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Helpers.DocumentUtility.Interface;
using MDGov.MDE.Common.Helpers.OpenXml;
using MDGov.MDE.Common.Helpers.OpenXml.Interface;
using MDGov.MDE.Common.Logging;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.Model;
using StructureMap;
using StructureMap.Graph;
using System;
using System.Data.Entity;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.DependencyResolution
{
  public static class IoC
  {
    public static IContainer Initialize()
    {
      ObjectFactory.Initialize((Action<IInitializationExpression>) (x =>
      {
        x.Scan((Action<IAssemblyScanner>) (scan =>
        {
          scan.TheCallingAssembly();
          scan.WithDefaultConventions();
        }));
        x.For<DbContext>().Use<PortalContext>();
        x.For<IRepository<ErrorLog>>().Use((Func<IContext, IRepository<ErrorLog>>) (ctx => (IRepository<ErrorLog>) new Repository<ErrorLog>(ctx.GetInstance<LoggingContext>())));
        x.For<IErrorLogging>().Use<ErrorLogging>();
        x.For(typeof (IRepository<>)).Use(typeof (Repository<>));
        x.For(typeof (IService<>)).Use(typeof (ServiceBase<>));
        x.For(typeof (IUpdatableService<>)).Use(typeof (UpdatableService<>));
        x.For<IFormsAuthenticationHelper>().Use<FormsAuthenticationHelper>();
        x.For<IMembershipAuthenticationService>().Use<MembershipAuthenticationService>();
        x.For<IOpenXmlWordHelper>().Use<OpenXmlHelper.WordHelper>();
        x.For<IDocumentUtility>().Use<MDGov.MDE.Common.Helpers.DocumentUtility.DocumentUtility>();
      }));
      return ObjectFactory.Container;
    }
  }
}
