﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common.SessionHandler
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.Interface;
using StructureMap;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common
{
  public class SessionHandler
  {
    public static object GetSessionVar(SessionVariables sessionVariable)
    {
      return HttpContext.Current.Session[sessionVariable.ToString()];
    }

    public static T GetSessionVar<T>(SessionVariables sessionVariable)
    {
      T obj;
      try
      {
        obj = (T) HttpContext.Current.Session[sessionVariable.ToString()];
      }
      catch
      {
        obj = default (T);
      }
      return obj;
    }

    public static void SetSessionVar(SessionVariables sessionVariable, object value)
    {
      HttpContext.Current.Session[sessionVariable.ToString()] = value;
    }

    public static void ClearAllSessionVars()
    {
      HttpContext.Current.Session.RemoveAll();
    }

    internal static bool BuildNewSession()
    {
      IMembershipAuthenticationService instance1 = ObjectFactory.GetInstance<IMembershipAuthenticationService>();
      IService<Contact> instance2 = ObjectFactory.GetInstance<IService<Contact>>();
      string name = HttpContext.Current.User.Identity.Name;
      int userIdByName = instance1.GetUserIdByName(name);
      if (userIdByName == 0)
        return false;
      var contactList = instance2.GetRange(0, 1, null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("UserId == " + (object) userIdByName, new object[0]) });
      var contact = contactList.FirstOrDefault<Contact>();
      if (contact == null)
        return false;
      IEnumerable<string> userRoles = instance1.GetUserRoles(name);
      SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserUsername, (object) name);
      SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserUserId, (object) userIdByName);
      SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserIsActive, (object) contact.Active);
      SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserRoles, (object) userRoles);
      SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserContactId, (object) contact.Id);
      SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserFullName, (object) string.Format("{0} {1}", (object) contact.FirstName, (object) contact.LastName));
      return true;
    }
  }
}
