﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders.GridCommandModelBinder
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders
{
  public class GridCommandModelBinder : DefaultModelBinder
  {
    protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
    {
      BindingAttribute bindingAttribute = propertyDescriptor.Attributes.OfType<BindingAttribute>().SingleOrDefault<BindingAttribute>();
      if (bindingAttribute == null)
      {
        base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
      }
      else
      {
        object obj = bindingContext.ValueProvider.GetValue(bindingAttribute.Name).ConvertTo(propertyDescriptor.PropertyType);
        this.SetProperty(controllerContext, bindingContext, propertyDescriptor, obj);
      }
    }
  }
}
