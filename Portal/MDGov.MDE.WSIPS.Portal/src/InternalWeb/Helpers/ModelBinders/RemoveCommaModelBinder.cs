﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders.RemoveCommaModelBinder
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;
using System.ComponentModel;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders
{
  public class RemoveCommaModelBinder : DefaultModelBinder
  {
    protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
    {
      if (propertyDescriptor.PropertyType.FullName.Contains("System.Int32") || propertyDescriptor.PropertyType == typeof (long?))
      {
        string name = propertyDescriptor.Name;
        if (string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + "." + propertyDescriptor.Name]))
          return;
        if (propertyDescriptor.PropertyType == typeof (long?))
          propertyDescriptor.SetValue(bindingContext.Model, (object) long.Parse(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + "." + propertyDescriptor.Name].Replace(",", "")));
        else
          propertyDescriptor.SetValue(bindingContext.Model, (object) Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + "." + propertyDescriptor.Name].Replace(",", "")));
      }
      else
        base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
    }
  }
}
