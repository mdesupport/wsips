﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Reporting.ReportingHelper
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Reporting
{
  public static class ReportingHelper
  {
    public static Dictionary<ReportingHelper.OutputTypes, string> MimeTypes = new Dictionary<ReportingHelper.OutputTypes, string>() { { ReportingHelper.OutputTypes.PDF, "application/pdf" }, { ReportingHelper.OutputTypes.CSV, "text/csv" }, { ReportingHelper.OutputTypes.Excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }, { ReportingHelper.OutputTypes.Word, "application/vnd.openxmlformats-officedocument.wordprocessingml.document" } };
    public static Dictionary<ReportingHelper.OutputTypes, string> FileExtensions = new Dictionary<ReportingHelper.OutputTypes, string>() { { ReportingHelper.OutputTypes.PDF, ".pdf" }, { ReportingHelper.OutputTypes.CSV, ".csv" }, { ReportingHelper.OutputTypes.Excel, ".xlsx" }, { ReportingHelper.OutputTypes.Word, ".docx" } };

    public enum Reports
    {
      Invoice,
    }

    public enum OutputTypes
    {
      PDF,
      CSV,
      Excel,
      Word,
    }
  }
}
