﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.RootAutoMapperProfile
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.MapResolvers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.TypeConverters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration
{
  public class RootAutoMapperProfile : Profile
  {
    public override string ProfileName
    {
      get
      {
        return "Root";
      }
    }

    protected override void Configure()
    {
      Mapper.CreateMap<IEnumerable<ILookup>, SelectList>().ConvertUsing<LookupSelectListTypeConverter>();
      Mapper.CreateMap<IEnumerable<LU_StateStream>, SelectList>().ConvertUsing<StateStreamSelectListTypeConverter>();
      Mapper.CreateMap<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>().ConvertUsing<WaterWithdrawalTypeTypeConverter>();
      Mapper.CreateMap<Notification, UserNotification>().ForMember((Expression<Func<UserNotification, object>>) (dest => dest.CreatedDate), (Action<IMemberConfigurationExpression<Notification>>) (opt => opt.MapFrom<string>((Expression<Func<Notification, string>>) (src => src.CreatedDate.ToString("g"))))).ForMember((Expression<Func<UserNotification, object>>) (dest => dest.PermitName), (Action<IMemberConfigurationExpression<Notification>>) (opt => opt.ResolveUsing<NotificationPermitNameResolver>().ConstructedBy(new Func<NotificationPermitNameResolver>(ObjectFactory.GetInstance<NotificationPermitNameResolver>))));
      Mapper.AssertConfigurationIsValid();
    }
  }
}
