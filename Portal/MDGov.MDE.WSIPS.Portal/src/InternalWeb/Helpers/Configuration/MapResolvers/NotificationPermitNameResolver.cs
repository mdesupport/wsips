﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.MapResolvers.NotificationPermitNameResolver
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.MapResolvers
{
  public class NotificationPermitNameResolver : ValueResolver<Notification, string>
  {
    private readonly IService<Permit> _permitService;

    public NotificationPermitNameResolver(IService<Permit> permitService)
    {
      this._permitService = permitService;
    }

    protected override string ResolveCore(Notification source)
    {
      if (source == null || !source.PermitId.HasValue)
        return (string) null;
      Permit byId = this._permitService.GetById(source.PermitId.Value, (string) null);
      if (byId == null)
        return (string) null;
      return byId.PermitName;
    }
  }
}
