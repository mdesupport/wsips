﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.MapResolvers.StateDropdownListResolver`1
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.MapResolvers
{
  public class StateDropdownListResolver<S> : ValueResolver<S, IEnumerable<SelectListItem>> where S : class
  {
    private readonly IService<LU_State> _stateService;

    public StateDropdownListResolver(IService<LU_State> stateService)
    {
      this._stateService = stateService;
    }

    protected override IEnumerable<SelectListItem> ResolveCore(S source)
    {
      return this.CreateSelectList(this._stateService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active == @0", new object[1]{ (object) true }) }, (string) null));
    }

    private IEnumerable<SelectListItem> CreateSelectList(IEnumerable<LU_State> source)
    {
      List<SelectListItem> selectListItemList = new List<SelectListItem>();
      selectListItemList.Add(new SelectListItem()
      {
        Text = "Select",
        Value = ""
      });
      selectListItemList.AddRange(source.Select<LU_State, SelectListItem>((Func<LU_State, SelectListItem>) (x => new SelectListItem()
      {
        Text = x.Description,
        Value = x.Id.ToString()
      })));
      return (IEnumerable<SelectListItem>) selectListItemList;
    }
  }
}
