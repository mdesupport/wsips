﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.MapResolvers.LookupDropdownListResolver`2
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.ServiceLayer;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.MapResolvers
{
  public class LookupDropdownListResolver<T, S> : ValueResolver<S, IEnumerable<SelectListItem>> where T : class where S : class
  {
    private readonly IService<T> _lookupService;

    public LookupDropdownListResolver(IService<T> lookupService)
    {
      this._lookupService = lookupService;
    }

    protected override IEnumerable<SelectListItem> ResolveCore(S source)
    {
      return this.CreateSelectList(this._lookupService.GetAll((string) null));
    }

    private IEnumerable<SelectListItem> CreateSelectList(IEnumerable<T> source)
    {
      List<SelectListItem> list = new SelectList((IEnumerable) source, "Id", "Description").ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = ""
      });
      return (IEnumerable<SelectListItem>) list;
    }
  }
}
