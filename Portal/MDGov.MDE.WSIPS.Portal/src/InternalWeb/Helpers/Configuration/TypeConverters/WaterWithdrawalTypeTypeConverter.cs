﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.TypeConverters.WaterWithdrawalTypeTypeConverter
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.TypeConverters
{
  public class WaterWithdrawalTypeTypeConverter : ITypeConverter<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>
  {
    public AttributedSelectList Convert(ResolutionContext context)
    {
      if (context.SourceValue == null)
        return (AttributedSelectList) null;
      List<AttributedSelectListItem> list = ((IEnumerable<LU_WaterWithdrawalType>) context.SourceValue).Select<LU_WaterWithdrawalType, AttributedSelectListItem>((Func<LU_WaterWithdrawalType, AttributedSelectListItem>) (i => new AttributedSelectListItem() { Text = i.Description, Value = i.Id.ToString(), HtmlAttributes = (object) new{ WaterWithdrawalSourceId = i.WaterWithdrawalSourceId } })).ToList<AttributedSelectListItem>();
      list.Insert(0, new AttributedSelectListItem()
      {
        Text = "Select"
      });
      return new AttributedSelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
