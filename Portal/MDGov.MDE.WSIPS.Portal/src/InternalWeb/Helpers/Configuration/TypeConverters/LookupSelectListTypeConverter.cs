﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.TypeConverters.LookupSelectListTypeConverter
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration.TypeConverters
{
  public class LookupSelectListTypeConverter : ITypeConverter<IEnumerable<ILookup>, SelectList>
  {
    public SelectList Convert(ResolutionContext context)
    {
      if (context.SourceValue == null)
        return (SelectList) null;
      List<SelectListItem> list = ((IEnumerable<ILookup>) context.SourceValue).Select<ILookup, SelectListItem>((Func<ILookup, SelectListItem>) (t => new SelectListItem() { Text = t.Description, Value = t.Id.ToString() })).OrderBy<SelectListItem, string>((Func<SelectListItem, string>) (t => t.Text)).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Select"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
