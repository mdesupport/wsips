﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Extensions.HtmlHelperExtensions
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Extensions
{
  public static class HtmlHelperExtensions
  {
    public static MvcHtmlString DropDownListCustom(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, IEnumerable items, string itemKey)
    {
      List<object> list = items.Cast<object>().ToList<object>();
      TagBuilder tagBuilder1 = new TagBuilder("select");
      string str1 = "";
      TagBuilder tagBuilder2 = new TagBuilder("option");
      tagBuilder2.MergeAttribute("value", "");
      tagBuilder2.SetInnerText("Select");
      string str2 = str1 + tagBuilder2.ToString(TagRenderMode.Normal) + "\n";
      foreach (SelectListItem select in selectList)
      {
        SelectListItem item = select;
        TagBuilder tagBuilder3 = new TagBuilder("option");
        tagBuilder3.MergeAttribute("value", item.Value.ToString());
        object obj = list.Where<object>((Func<object, bool>) (x => x.GetType().GetProperty("Id").GetValue(x).ToString().Contains(item.Value.ToString()))).FirstOrDefault<object>();
        tagBuilder3.MergeAttribute(itemKey, obj.GetType().GetProperty(itemKey).GetValue(obj, (object[]) null) == null ? "" : obj.GetType().GetProperty(itemKey).GetValue(obj, (object[]) null).ToString());
        tagBuilder3.SetInnerText(item.Text);
        str2 = str2 + tagBuilder3.ToString(TagRenderMode.Normal) + "\n";
      }
      tagBuilder1.MergeAttribute("data-val", "true");
      tagBuilder1.MergeAttribute("data-val-required", "The field is required.");
      tagBuilder1.MergeAttribute("id", name);
      tagBuilder1.InnerHtml = str2;
      return new MvcHtmlString(tagBuilder1.ToString(TagRenderMode.Normal));
    }

    public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList)
    {
      return HtmlHelperExtensions.DropDownList(htmlHelper, name, selectList, (string) null, (IDictionary<string, object>) null);
    }

    public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList, object htmlAttributes)
    {
      return HtmlHelperExtensions.DropDownList(htmlHelper, name, selectList, (string) null, (IDictionary<string, object>) HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
    }

    public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList, IDictionary<string, object> htmlAttributes)
    {
      return HtmlHelperExtensions.DropDownList(htmlHelper, name, selectList, (string) null, htmlAttributes);
    }

    public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList, string optionLabel)
    {
      return HtmlHelperExtensions.DropDownList(htmlHelper, name, selectList, optionLabel, (IDictionary<string, object>) null);
    }

    public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList, string optionLabel, object htmlAttributes)
    {
      return HtmlHelperExtensions.DropDownList(htmlHelper, name, selectList, optionLabel, (IDictionary<string, object>) HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
    }

    public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList, string optionLabel, IDictionary<string, object> htmlAttributes)
    {
      return HtmlHelperExtensions.DropDownListHelper(htmlHelper, (ModelMetadata) null, name, selectList, optionLabel, htmlAttributes);
    }

    public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList)
    {
      return HtmlHelperExtensions.DropDownListFor<TModel, TProperty>(htmlHelper, expression, selectList, (string) null, (IDictionary<string, object>) null);
    }

    public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList, object htmlAttributes)
    {
      return HtmlHelperExtensions.DropDownListFor<TModel, TProperty>(htmlHelper, expression, selectList, (string) null, (IDictionary<string, object>) HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
    }

    public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList, IDictionary<string, object> htmlAttributes)
    {
      return HtmlHelperExtensions.DropDownListFor<TModel, TProperty>(htmlHelper, expression, selectList, (string) null, htmlAttributes);
    }

    public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList, string optionLabel)
    {
      return HtmlHelperExtensions.DropDownListFor<TModel, TProperty>(htmlHelper, expression, selectList, optionLabel, (IDictionary<string, object>) null);
    }

    public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList, string optionLabel, object htmlAttributes)
    {
      return HtmlHelperExtensions.DropDownListFor<TModel, TProperty>(htmlHelper, expression, selectList, optionLabel, (IDictionary<string, object>) HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
    }

    public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList, string optionLabel, IDictionary<string, object> htmlAttributes)
    {
      if (expression == null)
        throw new ArgumentNullException("expression");
      ModelMetadata metadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
      return HtmlHelperExtensions.DropDownListHelper((HtmlHelper) htmlHelper, metadata, ExpressionHelper.GetExpressionText((LambdaExpression) expression), selectList, optionLabel, htmlAttributes);
    }

    private static MvcHtmlString DropDownListHelper(HtmlHelper htmlHelper, ModelMetadata metadata, string expression, IEnumerable<AttributedSelectListItem> selectList, string optionLabel, IDictionary<string, object> htmlAttributes)
    {
      return htmlHelper.SelectInternal(metadata, optionLabel, expression, selectList, false, htmlAttributes);
    }

    public static MvcHtmlString ListBox(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList)
    {
      return HtmlHelperExtensions.ListBox(htmlHelper, name, selectList, (IDictionary<string, object>) null);
    }

    public static MvcHtmlString ListBox(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList, object htmlAttributes)
    {
      return HtmlHelperExtensions.ListBox(htmlHelper, name, selectList, (IDictionary<string, object>) HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
    }

    public static MvcHtmlString ListBox(this HtmlHelper htmlHelper, string name, IEnumerable<AttributedSelectListItem> selectList, IDictionary<string, object> htmlAttributes)
    {
      return HtmlHelperExtensions.ListBoxHelper(htmlHelper, (ModelMetadata) null, name, selectList, htmlAttributes);
    }

    public static MvcHtmlString ListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList)
    {
      return HtmlHelperExtensions.ListBoxFor<TModel, TProperty>(htmlHelper, expression, selectList, (IDictionary<string, object>) null);
    }

    public static MvcHtmlString ListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList, object htmlAttributes)
    {
      return HtmlHelperExtensions.ListBoxFor<TModel, TProperty>(htmlHelper, expression, selectList, (IDictionary<string, object>) HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
    }

    public static MvcHtmlString ListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributedSelectListItem> selectList, IDictionary<string, object> htmlAttributes)
    {
      if (expression == null)
        throw new ArgumentNullException("expression");
      ModelMetadata metadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
      return HtmlHelperExtensions.ListBoxHelper((HtmlHelper) htmlHelper, metadata, ExpressionHelper.GetExpressionText((LambdaExpression) expression), selectList, htmlAttributes);
    }

    private static MvcHtmlString ListBoxHelper(HtmlHelper htmlHelper, ModelMetadata metadata, string name, IEnumerable<AttributedSelectListItem> selectList, IDictionary<string, object> htmlAttributes)
    {
      return htmlHelper.SelectInternal(metadata, (string) null, name, selectList, true, htmlAttributes);
    }

    private static IEnumerable<AttributedSelectListItem> GetSelectData(this HtmlHelper htmlHelper, string name)
    {
      object obj = (object) null;
      if (htmlHelper.ViewData != null)
        obj = htmlHelper.ViewData.Eval(name);
      if (obj == null)
        throw new InvalidOperationException(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "There is no ViewData item of type '{1}' that has the key '{0}'.", new object[2]{ (object) name, (object) "IEnumerable<SelectListItem>" }));
      IEnumerable<AttributedSelectListItem> attributedSelectListItems = obj as IEnumerable<AttributedSelectListItem>;
      if (attributedSelectListItems == null)
        throw new InvalidOperationException(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "The ViewData item that has the key '{0}' is of type '{1}' but must be of type '{2}'.", (object) name, (object) obj.GetType().FullName, (object) "IEnumerable<SelectListItem>"));
      return attributedSelectListItems;
    }

    internal static string ListItemToOption(AttributedSelectListItem item)
    {
      TagBuilder tagBuilder = new TagBuilder("option") { InnerHtml = HttpUtility.HtmlEncode(item.Text) };
      if (item.Value != null)
        tagBuilder.Attributes["value"] = item.Value;
      if (item.Selected)
        tagBuilder.Attributes["selected"] = "selected";
      if (item.HtmlAttributes != null)
        tagBuilder.MergeAttributes<string, object>((IDictionary<string, object>) HtmlHelper.AnonymousObjectToHtmlAttributes(item.HtmlAttributes));
      return tagBuilder.ToString(TagRenderMode.Normal);
    }

    private static IEnumerable<AttributedSelectListItem> GetSelectListWithDefaultValue(IEnumerable<AttributedSelectListItem> selectList, object defaultValue, bool allowMultiple)
    {
      IEnumerable source;
      if (allowMultiple)
      {
        source = defaultValue as IEnumerable;
        if (source == null || source is string)
          throw new InvalidOperationException(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "The parameter '{0}' must evaluate to an IEnumerable when multiple selection is allowed.", new object[1]{ (object) "expression" }));
      }
      else
        source = (IEnumerable) new object[1]
        {
          defaultValue
        };
      HashSet<string> stringSet = new HashSet<string>(source.Cast<object>().Select<object, string>((Func<object, string>) (value => Convert.ToString(value, (IFormatProvider) CultureInfo.CurrentCulture))), (IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
      List<AttributedSelectListItem> attributedSelectListItemList = new List<AttributedSelectListItem>();
      foreach (AttributedSelectListItem select in selectList)
      {
        select.Selected = select.Value != null ? stringSet.Contains(select.Value) : stringSet.Contains(select.Text);
        attributedSelectListItemList.Add(select);
      }
      return (IEnumerable<AttributedSelectListItem>) attributedSelectListItemList;
    }

    private static MvcHtmlString SelectInternal(this HtmlHelper htmlHelper, ModelMetadata metadata, string optionLabel, string name, IEnumerable<AttributedSelectListItem> selectList, bool allowMultiple, IDictionary<string, object> htmlAttributes)
    {
      string fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
      if (string.IsNullOrEmpty(fullHtmlFieldName))
        throw new ArgumentNullException("Argument cannot be null", "name");
      bool flag = false;
      if (selectList == null)
      {
        selectList = htmlHelper.GetSelectData(name);
        flag = true;
      }
      object defaultValue = allowMultiple ? htmlHelper.GetModelStateValue(fullHtmlFieldName, typeof (string[])) : htmlHelper.GetModelStateValue(fullHtmlFieldName, typeof (string));
      if (!flag && defaultValue == null && !string.IsNullOrEmpty(name))
        defaultValue = htmlHelper.ViewData.Eval(name);
      if (defaultValue != null)
        selectList = HtmlHelperExtensions.GetSelectListWithDefaultValue(selectList, defaultValue, allowMultiple);
      StringBuilder stringBuilder = new StringBuilder();
      if (optionLabel != null)
        stringBuilder.AppendLine(HtmlHelperExtensions.ListItemToOption(new AttributedSelectListItem()
        {
          Text = optionLabel,
          Value = string.Empty,
          Selected = false
        }));
      foreach (AttributedSelectListItem select in selectList)
        stringBuilder.AppendLine(HtmlHelperExtensions.ListItemToOption(select));
      TagBuilder tagBuilder = new TagBuilder("select") { InnerHtml = stringBuilder.ToString() };
      tagBuilder.MergeAttributes<string, object>(htmlAttributes);
      tagBuilder.MergeAttribute("name", fullHtmlFieldName, true);
      tagBuilder.GenerateId(fullHtmlFieldName);
      if (allowMultiple)
        tagBuilder.MergeAttribute("multiple", "multiple");
      ModelState modelState;
      if (htmlHelper.ViewData.ModelState.TryGetValue(fullHtmlFieldName, out modelState) && modelState.Errors.Count > 0)
        tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);
      tagBuilder.MergeAttributes<string, object>(htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata));
      return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
    }

    public static MvcHtmlString DatePicker(this HtmlHelper html, string name, DateTime? value = null)
    {
      return MvcHtmlString.Create(html.TextBox(name, value.HasValue ? (object) value.Value.ToString("MM/dd/yyyy") : (object) (string) null, (object) new{ @class = "date" }).ToString());
    }

    public static MvcHtmlString DatePickerFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
    {
      ModelMetadata modelMetadata = ModelMetadata.FromLambdaExpression<TModel, TValue>(expression, html.ViewData);
      string expressionText = ExpressionHelper.GetExpressionText((LambdaExpression) expression);
      DateTime? model = (DateTime?) modelMetadata.Model;
      return MvcHtmlString.Create(html.TextBox(expressionText, model.HasValue ? (object) model.Value.ToString("MM/dd/yyyy") : (object) (string) null, (object) new{ @class = "date" }).ToString());
    }

    public static MvcHtmlString DatePickerFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
    {
      ModelMetadata modelMetadata = ModelMetadata.FromLambdaExpression<TModel, TValue>(expression, html.ViewData);
      string expressionText = ExpressionHelper.GetExpressionText((LambdaExpression) expression);
      DateTime? model = (DateTime?) modelMetadata.Model;
      return MvcHtmlString.Create(html.TextBox(expressionText, model.HasValue ? (object) model.Value.ToString("MM/dd/yyyy") : (object) (string) null, (object) new{ @class = "date" }).ToString());
    }

    public static object GetModelStateValue(this HtmlHelper helper, string key, Type destinationType)
    {
      ModelState modelState;
      if (helper.ViewData.ModelState.TryGetValue(key, out modelState) && modelState.Value != null)
        return modelState.Value.ConvertTo(destinationType, (CultureInfo) null);
      return (object) null;
    }
  }
}
