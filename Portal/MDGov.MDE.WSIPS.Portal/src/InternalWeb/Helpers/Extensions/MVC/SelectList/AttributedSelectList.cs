﻿// Decompiled with JetBrains decompiler
// Type: System.Web.Mvc.AttributedSelectList
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Collections;

namespace System.Web.Mvc
{
  public class AttributedSelectList : AttributedMultiSelectList
  {
    public object SelectedValue { get; private set; }

    public AttributedSelectList(IEnumerable items)
      : this(items, (object) null)
    {
    }

    public AttributedSelectList(IEnumerable items, object selectedValue)
      : this(items, (string) null, (string) null, selectedValue)
    {
    }

    public AttributedSelectList(IEnumerable items, string dataValueField, string dataTextField)
      : this(items, dataValueField, dataTextField, (object) null)
    {
    }

    public AttributedSelectList(IEnumerable items, string dataValueField, string dataTextField, object selectedValue)
      : base(items, dataValueField, dataTextField, AttributedSelectList.ToEnumerable(selectedValue))
    {
      this.SelectedValue = selectedValue;
    }

    private static IEnumerable ToEnumerable(object selectedValue)
    {
      if (selectedValue == null)
        return (IEnumerable) null;
      return (IEnumerable) new object[1]
      {
        selectedValue
      };
    }
  }
}
