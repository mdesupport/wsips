﻿// Decompiled with JetBrains decompiler
// Type: System.Web.Mvc.AttributedMultiSelectList
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;

namespace System.Web.Mvc
{
  public class AttributedMultiSelectList : IEnumerable<AttributedSelectListItem>, IEnumerable
  {
    public string DataTextField { get; private set; }

    public string DataValueField { get; private set; }

    public IEnumerable Items { get; private set; }

    public IEnumerable SelectedValues { get; private set; }

    public AttributedMultiSelectList(IEnumerable items)
      : this(items, (IEnumerable) null)
    {
    }

    public AttributedMultiSelectList(IEnumerable items, IEnumerable selectedValues)
      : this(items, (string) null, (string) null, selectedValues)
    {
    }

    public AttributedMultiSelectList(IEnumerable items, string dataValueField, string dataTextField)
      : this(items, dataValueField, dataTextField, (IEnumerable) null)
    {
    }

    public AttributedMultiSelectList(IEnumerable items, string dataValueField, string dataTextField, IEnumerable selectedValues)
    {
      if (items == null)
        throw new ArgumentNullException("items");
      this.Items = items;
      this.DataValueField = dataValueField;
      this.DataTextField = dataTextField;
      this.SelectedValues = selectedValues;
    }

    public virtual IEnumerator<AttributedSelectListItem> GetEnumerator()
    {
      return this.GetListItems().GetEnumerator();
    }

    internal IList<AttributedSelectListItem> GetListItems()
    {
      if (string.IsNullOrEmpty(this.DataValueField))
        return this.GetListItemsWithoutValueField();
      return this.GetListItemsWithValueField();
    }

    private IList<AttributedSelectListItem> GetListItemsWithValueField()
    {
      HashSet<string> selectedValues = new HashSet<string>((IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
      if (this.SelectedValues != null)
        selectedValues.UnionWith(this.SelectedValues.Cast<object>().Select<object, string>((Func<object, string>) (value => Convert.ToString(value, (IFormatProvider) CultureInfo.CurrentCulture))));
      return (IList<AttributedSelectListItem>) this.Items.Cast<object>().Select(item => new
      {
        item = item,
        value = AttributedMultiSelectList.Eval(item, this.DataValueField)
      }).Select(param0 => new AttributedSelectListItem()
      {
        Value = param0.value,
        Text = AttributedMultiSelectList.Eval(param0.item, this.DataTextField),
        Selected = selectedValues.Contains(param0.value),
        HtmlAttributes = DataBinder.Eval(param0.item, "HtmlAttributes")
      }).ToList<AttributedSelectListItem>();
    }

    private IList<AttributedSelectListItem> GetListItemsWithoutValueField()
    {
      HashSet<object> selectedValues = new HashSet<object>();
      if (this.SelectedValues != null)
        selectedValues.UnionWith(this.SelectedValues.Cast<object>());
      return (IList<AttributedSelectListItem>) this.Items.Cast<object>().Select<object, AttributedSelectListItem>((Func<object, AttributedSelectListItem>) (item => new AttributedSelectListItem()
      {
        Text = AttributedMultiSelectList.Eval(item, this.DataTextField),
        Selected = selectedValues.Contains(item),
        HtmlAttributes = DataBinder.Eval(item, "HtmlAttributes")
      })).ToList<AttributedSelectListItem>();
    }

    private static string Eval(object container, string expression)
    {
      object obj = container;
      if (!string.IsNullOrEmpty(expression))
        obj = DataBinder.Eval(container, expression);
      return Convert.ToString(obj, (IFormatProvider) CultureInfo.CurrentCulture);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }
  }
}
