﻿// Decompiled with JetBrains decompiler
// Type: System.Web.Mvc.AttributedSelectListItem
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

namespace System.Web.Mvc
{
  public class AttributedSelectListItem
  {
    public bool Selected { get; set; }

    public string Text { get; set; }

    public string Value { get; set; }

    public object HtmlAttributes { get; set; }
  }
}
