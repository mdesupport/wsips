﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum.IrrigationType
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum
{
  public enum IrrigationType
  {
    IrrigationUndefined = 1,
    GolfCourseIrrigation = 2,
    LawnAndParkIrrigation = 3,
    SmallIntermitentIrrigation = 4,
    NurseryIrrigation = 5,
    SodIrrigation = 6,
  }
}
