﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum.ContactType
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum
{
  public enum ContactType
  {
    CarbonCopy = 6,
    Consultant = 8,
    InterestedParty = 15,
    LandOwner = 16,
    WaterUser = 27,
  }
}
