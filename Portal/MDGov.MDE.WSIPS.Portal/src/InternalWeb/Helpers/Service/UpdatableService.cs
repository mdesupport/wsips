﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.UpdatableService`1
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service
{
  public class UpdatableService<T> : UpdatableServiceBase<T> where T : IUpdatableEntity
  {
    public override int Save(T entity)
    {
      entity.LastModifiedBy = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "System" : HttpContext.Current.User.Identity.Name;
      return base.Save(entity);
    }
  }
}
