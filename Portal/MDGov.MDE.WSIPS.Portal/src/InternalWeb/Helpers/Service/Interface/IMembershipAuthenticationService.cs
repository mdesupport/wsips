﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.Interface.IMembershipAuthenticationService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.Interface
{
  public interface IMembershipAuthenticationService
  {
    bool Validate(string userName, string password);

    int GetUserIdByName(string userName);

    string CreateAccount(string userName, string email, string userType, string principalUser);

    IEnumerable<string> GetRoles();

    IEnumerable<string> GetUserRoles(string userName);

    IEnumerable<int> GetUsersInRole(string role);

    UserListing GetUserById(int id);

    bool UpdateUserName(int id, string newUserName);

    bool UpdateUserInfo(int id, string newUserName, string newLastName);

    bool AssignRoles(string userName, string[] roles);

    int GetUserTypeId(string userType);

    IEnumerable<UserListing> GetRange(int[] ids, string name, int typeId, string role);

    IEnumerable<UserTypeListing> GetUserTypes();

    bool IsInRole(string userName, string[] roles);

    bool DeleteUser(int userId);
  }
}
