﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.MembershipAuthenticationService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using StructureMap;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service
{
  public class MembershipAuthenticationService : HttpClientBase, IMembershipAuthenticationService
  {
    public MembershipAuthenticationService()
      : base(ConfigurationManager.AppSettings["MembershipUri"])
    {
    }

    public bool Validate(string userName, string password)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync("Validate", new{ UserName = userName, Password = password }).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<bool>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    public int GetUserIdByName(string userName)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetUserIdByName?userName={0}", (object) userName)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    public IEnumerable<string> GetRoles()
    {
      HttpResponseMessage result = this.Client.GetAsync("GetRoles").Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<string>>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<string>) null;
    }

    public IEnumerable<string> GetUserRoles(string userName)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetUserRoles?userName={0}", (object) userName)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<string>>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<string>) null;
    }

    public IEnumerable<int> GetUsersInRole(string role)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetUsersInRole?role={0}", (object) role)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<int>>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<int>) null;
    }

    public UserListing GetUserById(int id)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetUserById/{0}", (object) id)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<UserListing>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (UserListing) null;
    }

    public bool UpdateUserName(int id, string newUserName)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync("UpdateUserName", new{ UserId = id, Username = newUserName }).Result;
      if (result.IsSuccessStatusCode)
        return true;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    public bool UpdateUserInfo(int id, string newUserName, string newLastName)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync("UpdateUserInfo", new{ UserId = id, Username = newUserName, LastName = newLastName }).Result;
      if (result.IsSuccessStatusCode)
        return true;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    public string CreateAccount(string userName, string email, string userType, string principalUser)
    {
      bool flag = userType == "Internal";
      HttpResponseMessage result = this.Client.PostAsJsonAsync("CreateAccount", new{ UserName = userName, UserType = userType, Email = email, Approved = flag, PrincipalUser = principalUser }).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<string>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return string.Empty;
    }

    public bool AssignRoles(string userName, string[] roles)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync("AssignRoles", new{ UserName = userName, Roles = roles }).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<bool>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    public int GetUserTypeId(string userType)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetUserTypeId?userType={0}", (object) userType)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    public IEnumerable<UserTypeListing> GetUserTypes()
    {
      HttpResponseMessage result = this.Client.GetAsync("GetUserTypes").Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<UserTypeListing>>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<UserTypeListing>) new List<UserTypeListing>();
    }

    public IEnumerable<UserListing> GetRange(int[] ids, string name, int typeId, string role)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync("GetUserRange", new{ Ids = ids, Name = name, TypeId = typeId, Role = role }).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<UserListing>>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<UserListing>) new List<UserListing>();
    }

    public bool IsInRole(string userName, string[] roles)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync("IsInRole", new{ UserName = userName, Roles = roles }).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<bool>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    public bool DeleteUser(int userId)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("Delete?id={0}", (object) userId)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<bool>().Result;
      MembershipAuthenticationService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    private static void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      switch (statusCode)
      {
        case HttpStatusCode.NotFound:
          exception.ExceptionType = "NotFound";
          exception.ExceptionMessage = exception.Message;
          break;
        case HttpStatusCode.MethodNotAllowed:
          exception.ExceptionType = "MethodNotAllowed";
          exception.ExceptionMessage = exception.Message;
          break;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }
  }
}
