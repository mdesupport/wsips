﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters.RequiredIfAttribute
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters
{
  [AttributeUsage(AttributeTargets.Property)]
  public class RequiredIfAttribute : RequiredAttribute, IClientValidatable
  {
    public string DependentProperty { get; set; }

    public object[] TargetValues { get; set; }

    public ComparisonType ComparisonType { get; set; }

    public RequiredIfAttribute(string dependentProperty, params object[] targetValues)
    {
      this.DependentProperty = dependentProperty;
      this.TargetValues = targetValues;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
      PropertyInfo property = validationContext.ObjectInstance.GetType().GetProperty(this.DependentProperty);
      ValidationResult validationResult = ValidationResult.Success;
      if (property != (PropertyInfo) null)
      {
        object obj = property.GetValue(validationContext.ObjectInstance, (object[]) null);
        if (this.ComparisonType == ComparisonType.NotEqual)
        {
          if (obj == null)
          {
            if (this.TargetValues != null && !this.IsValid(value))
              validationResult = new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName), (IEnumerable<string>) new string[1]
              {
                validationContext.MemberName
              });
          }
          else if (this.TargetValues == null)
          {
            if (!this.IsValid(value))
              validationResult = new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName), (IEnumerable<string>) new string[1]
              {
                validationContext.MemberName
              });
          }
          else if (!((IEnumerable<object>) this.TargetValues).Any<object>(new Func<object, bool>(obj.Equals)) && !this.IsValid(value))
            validationResult = new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName), (IEnumerable<string>) new string[1]
            {
              validationContext.MemberName
            });
        }
        else if (obj == null)
        {
          if (this.TargetValues == null && !this.IsValid(value))
            validationResult = new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName), (IEnumerable<string>) new string[1]
            {
              validationContext.MemberName
            });
        }
        else if (this.TargetValues != null && ((IEnumerable<object>) this.TargetValues).Any<object>(new Func<object, bool>(obj.Equals)) && !this.IsValid(value))
          validationResult = new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName), (IEnumerable<string>) new string[1]
          {
            validationContext.MemberName
          });
      }
      return validationResult;
    }

    public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
    {
      ModelClientValidationRule rule = new ModelClientValidationRule() { ErrorMessage = this.FormatErrorMessage(metadata.GetDisplayName()), ValidationType = "requiredif" };
      string depProp = this.BuildDependentPropertyId(metadata, context as ViewContext);
      if (this.TargetValues == null)
      {
        rule.ValidationParameters.Add("targetvalues", (object) string.Empty);
      }
      else
      {
        List<string> stringList = new List<string>();
        foreach (object targetValue in this.TargetValues)
        {
          if (targetValue is bool)
            stringList.Add(targetValue.ToString().ToLower());
          else
            stringList.Add(targetValue.ToString());
        }
        rule.ValidationParameters.Add("targetvalues", (object) string.Join(",", (IEnumerable<string>) stringList));
      }
      rule.ValidationParameters.Add("dependentproperty", (object) depProp);
      rule.ValidationParameters.Add("comparisontype", (object) this.ComparisonType.ToString());
      yield return rule;
    }

    private string BuildDependentPropertyId(ModelMetadata metadata, ViewContext viewContext)
    {
      string str1 = viewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(this.DependentProperty);
      string str2 = metadata.PropertyName + "_";
      if (str1.StartsWith(str2))
        str1 = str1.Substring(str2.Length);
      return str1;
    }
  }
}
