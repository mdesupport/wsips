﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters.AuthorizationAttribute
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters
{
  public class AuthorizationAttribute : AuthorizeAttribute
  {
    private string[] _authorizedRoles;

    public string[] AuthorizedRoles
    {
      get
      {
        return this._authorizedRoles ?? new string[0];
      }
      set
      {
        this._authorizedRoles = value;
      }
    }

    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {
      if (httpContext == null)
        throw new ArgumentNullException("httpContext");
      if (!httpContext.User.Identity.IsAuthenticated || !SessionHandler.GetSessionVar<bool>(SessionVariables.AuthenticatedUserIsActive))
        return false;
      if (this.AuthorizedRoles.Length == 0)
        return true;
      return ((IEnumerable<string>) this.AuthorizedRoles).Any<string>(new Func<string, bool>((SessionHandler.GetSessionVar<IEnumerable<string>>(SessionVariables.AuthenticatedUserRoles)).Contains<string>));
    }
  }
}
