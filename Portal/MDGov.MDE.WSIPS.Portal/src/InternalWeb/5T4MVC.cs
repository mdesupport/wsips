﻿// Decompiled with JetBrains decompiler
// Type: MVC
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using System.CodeDom.Compiler;
using System.Diagnostics;
using T4MVC;

[GeneratedCode("T4MVC", "2.0")]
[DebuggerNonUserCode]
public static class MVC
{
  private static readonly AdministrationClass s_Administration = new AdministrationClass();
  private static readonly EcommerceClass s_Ecommerce = new EcommerceClass();
  private static readonly PermittingClass s_Permitting = new PermittingClass();
  private static readonly ReportingClass s_Reporting = new ReportingClass();
  public static BaseController Base = (BaseController) new T4MVC_BaseController();
  public static CommentController Comment = (CommentController) new T4MVC_CommentController();
  public static ContactController Contact = (ContactController) new T4MVC_ContactController();
  public static HomeController Home = (HomeController) new T4MVC_HomeController();
  public static NotificationController Notification = (NotificationController) new T4MVC_NotificationController();
  public static PublicCommentController PublicComment = (PublicCommentController) new T4MVC_PublicCommentController();
  public static UploadController Upload = (UploadController) new T4MVC_UploadController();
  public static SharedController Shared = new SharedController();

  public static AdministrationClass Administration
  {
    get
    {
      return MVC.s_Administration;
    }
  }

  public static EcommerceClass Ecommerce
  {
    get
    {
      return MVC.s_Ecommerce;
    }
  }

  public static PermittingClass Permitting
  {
    get
    {
      return MVC.s_Permitting;
    }
  }

  public static ReportingClass Reporting
  {
    get
    {
      return MVC.s_Reporting;
    }
  }
}
