﻿// Decompiled with JetBrains decompiler
// Type: Links.Content
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;

namespace Links
{
  [GeneratedCode("T4MVC", "2.0")]
  [DebuggerNonUserCode]
  public static class Content
  {
    public static readonly string Dashboard_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/Dashboard.min.css") ? Content.Url("Dashboard.css") : Content.Url("Dashboard.min.css");
    public static readonly string ie_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/ie.min.css") ? Content.Url("ie.css") : Content.Url("ie.min.css");
    public static readonly string ie8_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/ie8.min.css") ? Content.Url("ie8.css") : Content.Url("ie8.min.css");
    public static readonly string Map_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/Map.min.css") ? Content.Url("Map.css") : Content.Url("Map.min.css");
    public static readonly string Site_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/Site.min.css") ? Content.Url("Site.css") : Content.Url("Site.min.css");
    private const string URLPATH = "~/Content";

    public static string Url()
    {
      return T4MVCHelpers.ProcessVirtualPath("~/Content");
    }

    public static string Url(string fileName)
    {
      return T4MVCHelpers.ProcessVirtualPath("~/Content/" + fileName);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public static class ArcGIS_JS_API
    {
      private const string URLPATH = "~/Content/ArcGIS JS API";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API/" + fileName);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class css
      {
        public static readonly string esri_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/ArcGIS JS API/css/esri.min.css") ? Content.ArcGIS_JS_API.css.Url("esri.css") : Content.ArcGIS_JS_API.css.Url("esri.min.css");
        public static readonly string infowindow_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/ArcGIS JS API/css/infowindow.min.css") ? Content.ArcGIS_JS_API.css.Url("infowindow.css") : Content.ArcGIS_JS_API.css.Url("infowindow.min.css");
        private const string URLPATH = "~/Content/ArcGIS JS API/css";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API/css");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API/css/" + fileName);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class dijit
      {
        private const string URLPATH = "~/Content/ArcGIS JS API/dijit";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API/dijit");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API/dijit/" + fileName);
        }

        [DebuggerNonUserCode]
        [GeneratedCode("T4MVC", "2.0")]
        public static class images
        {
          public static readonly string infowindowGray_png = Content.ArcGIS_JS_API.dijit.images.Url("infowindowGray.png");
          public static readonly string popup_png = Content.ArcGIS_JS_API.dijit.images.Url("popup.png");
          private const string URLPATH = "~/Content/ArcGIS JS API/dijit/images";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API/dijit/images");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/ArcGIS JS API/dijit/images/" + fileName);
          }
        }
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public static class DigitalSignatures
    {
      public static readonly string _29385D97_C70E_4BCA_8311_24CF109723F4_jpg = Content.DigitalSignatures.Url("29385D97-C70E-4BCA-8311-24CF109723F4.jpg");
      public static readonly string B9A5AAF0_3C5D_4099_BBC5_E1134B568E7B_jpg = Content.DigitalSignatures.Url("B9A5AAF0-3C5D-4099-BBC5-E1134B568E7B.jpg");
      public static readonly string cynthialatham_jpg = Content.DigitalSignatures.Url("cynthialatham.jpg");
      public static readonly string johngrace_jpg = Content.DigitalSignatures.Url("johngrace.jpg");
      public static readonly string lynpoorman_jpg = Content.DigitalSignatures.Url("lynpoorman.jpg");
      public static readonly string marianna_jpg = Content.DigitalSignatures.Url("marianna.jpg");
      public static readonly string mariannaeberle_jpg = Content.DigitalSignatures.Url("mariannaeberle.jpg");
      public static readonly string normanlazarus_jpg = Content.DigitalSignatures.Url("normanlazarus.jpg");
      public static readonly string robertpeoples_jpg = Content.DigitalSignatures.Url("robertpeoples.jpg");
      public static readonly string samuelglover_jpg = Content.DigitalSignatures.Url("samuelglover.jpg");
      private const string URLPATH = "~/Content/DigitalSignatures";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/DigitalSignatures");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/DigitalSignatures/" + fileName);
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public static class Images
    {
      public static readonly string back_jpg = Content.Images.Url("back.jpg");
      public static readonly string background_jpg = Content.Images.Url("background.jpg");
      public static readonly string printer_png = Content.Images.Url("printer.png");
      private const string URLPATH = "~/Content/Images";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/Images");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/Images/" + fileName);
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public static class Sprite
      {
        public static readonly string Sprite_png = Content.Images.Sprite.Url("Sprite.png");
        private const string URLPATH = "~/Content/Images/Sprite";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/Images/Sprite");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/Images/Sprite/" + fileName);
        }
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public static class jquery_jqGrid
    {
      public static readonly string ellipsis_xbl_xml = Content.jquery_jqGrid.Url("ellipsis-xbl.xml");
      public static readonly string ui_jqgrid_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/jquery.jqGrid/ui.jqgrid.min.css") ? Content.jquery_jqGrid.Url("ui.jqgrid.css") : Content.jquery_jqGrid.Url("ui.jqgrid.min.css");
      private const string URLPATH = "~/Content/jquery.jqGrid";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/jquery.jqGrid");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/jquery.jqGrid/" + fileName);
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public static class templates
    {
      public static readonly string _6MonthLetterCWS_dotx = Content.templates.Url("6MonthLetterCWS.dotx");
      public static readonly string _6MonthLetterHarsh_dotx = Content.templates.Url("6MonthLetterHarsh.dotx");
      public static readonly string _6MonthLetterStandard_dotx = Content.templates.Url("6MonthLetterStandard.dotx");
      public static readonly string AcknowledgementFormCulturalBiological_dotx = Content.templates.Url("AcknowledgementFormCulturalBiological.dotx");
      public static readonly string ApplicationProcessOutlineAg_dotx = Content.templates.Url("ApplicationProcessOutlineAg.dotx");
      public static readonly string ApplicationProcessOutlineCD_dotx = Content.templates.Url("ApplicationProcessOutlineCD.dotx");
      public static readonly string ApplicationProcessOutlineNonAg_dotx = Content.templates.Url("ApplicationProcessOutlineNonAg.dotx");
      public static readonly string AquiferTestProcedure_dotx = Content.templates.Url("AquiferTestProcedure.dotx");
      public static readonly string BestManagementPractices_dotx = Content.templates.Url("BestManagementPractices.dotx");
      public static readonly string blank_docx = Content.templates.Url("blank.docx");
      public static readonly string blankCustomPackage_docx = Content.templates.Url("blankCustomPackage.docx");
      public static readonly string blankPermit_docx = Content.templates.Url("blankPermit.docx");
      public static readonly string CapacityManagementPlanGuidance_dotx = Content.templates.Url("CapacityManagementPlanGuidance.dotx");
      public static readonly string CapacityManagementPlanGuidance2_dotx = Content.templates.Url("CapacityManagementPlanGuidance2.dotx");
      public static readonly string CertificationNotificationForm_dotx = Content.templates.Url("CertificationNotificationForm.dotx");
      public static readonly string CoastalPlainConfined_dotx = Content.templates.Url("CoastalPlainConfined.dotx");
      public static readonly string CoastalPlainUnconfinedHydroProc_dotx = Content.templates.Url("CoastalPlainUnconfinedHydroProc.dotx");
      public static readonly string CountyTaxMapNumbers_dotx = Content.templates.Url("CountyTaxMapNumbers.dotx");
      public static readonly string CustomPackageCoverLetterAg_dotx = Content.templates.Url("CustomPackageCoverLetterAg.dotx");
      public static readonly string CustomPackageCoverLetterCD_dotx = Content.templates.Url("CustomPackageCoverLetterCD.dotx");
      public static readonly string CustomPackageCoverLetterNonAg_dotx = Content.templates.Url("CustomPackageCoverLetterNonAg.dotx");
      public static readonly string DecisionLetter_dotx = Content.templates.Url("DecisionLetter.dotx");
      public static readonly string DecisionLetterGroundwater_dotx = Content.templates.Url("DecisionLetterGroundwater.dotx");
      public static readonly string DecisionLetterSurfacewater_dotx = Content.templates.Url("DecisionLetterSurfacewater.dotx");
      public static readonly string DemandAnalysis_dotx = Content.templates.Url("DemandAnalysis.dotx");
      public static readonly string DraftPermitAgBody_dotx = Content.templates.Url("DraftPermitAgBody.dotx");
      public static readonly string DraftPermitAgCover_dotx = Content.templates.Url("DraftPermitAgCover.dotx");
      public static readonly string DraftPermitNonAgBody_dotx = Content.templates.Url("DraftPermitNonAgBody.dotx");
      public static readonly string DraftPermitNonAgCover_dotx = Content.templates.Url("DraftPermitNonAgCover.dotx");
      public static readonly string ExampleNotificationLetter_dotx = Content.templates.Url("ExampleNotificationLetter.dotx");
      public static readonly string ExemptionCurrentOwner_dotx = Content.templates.Url("ExemptionCurrentOwner.dotx");
      public static readonly string ExemptionCurrentOwnerPWS_dotx = Content.templates.Url("ExemptionCurrentOwnerPWS.dotx");
      public static readonly string ExpiredCurrentOwner_dotx = Content.templates.Url("ExpiredCurrentOwner.dotx");
      public static readonly string ExpiredNewOwner_dotx = Content.templates.Url("ExpiredNewOwner.dotx");
      public static readonly string FailureToReportNOVAg_dotx = Content.templates.Url("FailureToReportNOVAg.dotx");
      public static readonly string FailureToReportNOVNonAg_dotx = Content.templates.Url("FailureToReportNOVNonAg.dotx");
      public static readonly string FracRock_ConsolSed_HydroProc_dotx = Content.templates.Url("FracRock(ConsolSed)HydroProc.dotx");
      public static readonly string Fracrock_Crystalline_HydroProc_dotx = Content.templates.Url("Fracrock(Crystalline)HydroProc.dotx");
      public static readonly string Fracrock_Springs_HydroProc_dotx = Content.templates.Url("Fracrock(Springs)HydroProc.dotx");
      public static readonly string Fracrock_SubdIndWell_HydroProc_dotx = Content.templates.Url("Fracrock(SubdIndWell)HydroProc.dotx");
      public static readonly string InvestigationProcedures_dotx = Content.templates.Url("InvestigationProcedures.dotx");
      public static readonly string Invoice_odt = Content.templates.Url("Invoice.odt");
      public static readonly string Invoice_pdf = Content.templates.Url("Invoice.pdf");
      public static readonly string OverAppropriationNOV_dotx = Content.templates.Url("OverAppropriationNOV.dotx");
      public static readonly string PlumbingFixtureComplianceForm_dotx = Content.templates.Url("PlumbingFixtureComplianceForm.dotx");
      public static readonly string ProcessingProceduresAg_dotx = Content.templates.Url("ProcessingProceduresAg.dotx");
      public static readonly string ProcessingProceduresCD_dotx = Content.templates.Url("ProcessingProceduresCD.dotx");
      public static readonly string ProcessingProceduresNonAg_dotx = Content.templates.Url("ProcessingProceduresNonAg.dotx");
      public static readonly string PublicNoticeBillingForm_dotx = Content.templates.Url("PublicNoticeBillingForm.dotx");
      public static readonly string PumpageLetterAg_dotx = Content.templates.Url("PumpageLetterAg.dotx");
      public static readonly string PumpageLetterAgAcresInches_dotx = Content.templates.Url("PumpageLetterAgAcresInches.dotx");
      public static readonly string PumpageLetterNonAgPeriod1_dotx = Content.templates.Url("PumpageLetterNonAgPeriod1.dotx");
      public static readonly string PumpageLetterNonAgPeriod1_reminder_dotx = Content.templates.Url("PumpageLetterNonAgPeriod1_reminder.dotx");
      public static readonly string PumpageLetterNonAgPeriod2_dotx = Content.templates.Url("PumpageLetterNonAgPeriod2.dotx");
      public static readonly string PumpageLetterNonAgPeriod2_reminder_dotx = Content.templates.Url("PumpageLetterNonAgPeriod2_reminder.dotx");
      public static readonly string RenewalAGVoluntary_dotx = Content.templates.Url("RenewalAGVoluntary.dotx");
      public static readonly string SurfWtrHydroProc_dotx = Content.templates.Url("SurfWtrHydroProc.dotx");
      public static readonly string TierIIEvaluation_dotx = Content.templates.Url("TierIIEvaluation.dotx");
      public static readonly string WaterAuditReminder_dotx = Content.templates.Url("WaterAuditReminder.dotx");
      public static readonly string WaterAuditThankYou_dotx = Content.templates.Url("WaterAuditThankYou.dotx");
      public static readonly string WithdrawalLetterIncrease_dotx = Content.templates.Url("WithdrawalLetterIncrease.dotx");
      public static readonly string WithdrawalLetterNew_dotx = Content.templates.Url("WithdrawalLetterNew.dotx");
      private const string URLPATH = "~/Content/templates";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/templates");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/templates/" + fileName);
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public static class themes
    {
      private const string URLPATH = "~/Content/themes";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/themes");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/" + fileName);
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public static class @base
      {
        public static readonly string jquery_ui_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery-ui.min.css") ? Content.themes.@base.Url("jquery-ui.css") : Content.themes.@base.Url("jquery-ui.min.css");
        public static readonly string jquery_ui_accordion_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.accordion.min.css") ? Content.themes.@base.Url("jquery.ui.accordion.css") : Content.themes.@base.Url("jquery.ui.accordion.min.css");
        public static readonly string jquery_ui_all_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.all.min.css") ? Content.themes.@base.Url("jquery.ui.all.css") : Content.themes.@base.Url("jquery.ui.all.min.css");
        public static readonly string jquery_ui_autocomplete_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.autocomplete.min.css") ? Content.themes.@base.Url("jquery.ui.autocomplete.css") : Content.themes.@base.Url("jquery.ui.autocomplete.min.css");
        public static readonly string jquery_ui_base_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.base.min.css") ? Content.themes.@base.Url("jquery.ui.base.css") : Content.themes.@base.Url("jquery.ui.base.min.css");
        public static readonly string jquery_ui_button_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.button.min.css") ? Content.themes.@base.Url("jquery.ui.button.css") : Content.themes.@base.Url("jquery.ui.button.min.css");
        public static readonly string jquery_ui_core_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.core.min.css") ? Content.themes.@base.Url("jquery.ui.core.css") : Content.themes.@base.Url("jquery.ui.core.min.css");
        public static readonly string jquery_ui_datepicker_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.datepicker.min.css") ? Content.themes.@base.Url("jquery.ui.datepicker.css") : Content.themes.@base.Url("jquery.ui.datepicker.min.css");
        public static readonly string jquery_ui_dialog_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.dialog.min.css") ? Content.themes.@base.Url("jquery.ui.dialog.css") : Content.themes.@base.Url("jquery.ui.dialog.min.css");
        public static readonly string jquery_ui_menu_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.menu.min.css") ? Content.themes.@base.Url("jquery.ui.menu.css") : Content.themes.@base.Url("jquery.ui.menu.min.css");
        public static readonly string jquery_ui_progressbar_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.progressbar.min.css") ? Content.themes.@base.Url("jquery.ui.progressbar.css") : Content.themes.@base.Url("jquery.ui.progressbar.min.css");
        public static readonly string jquery_ui_resizable_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.resizable.min.css") ? Content.themes.@base.Url("jquery.ui.resizable.css") : Content.themes.@base.Url("jquery.ui.resizable.min.css");
        public static readonly string jquery_ui_selectable_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.selectable.min.css") ? Content.themes.@base.Url("jquery.ui.selectable.css") : Content.themes.@base.Url("jquery.ui.selectable.min.css");
        public static readonly string jquery_ui_slider_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.slider.min.css") ? Content.themes.@base.Url("jquery.ui.slider.css") : Content.themes.@base.Url("jquery.ui.slider.min.css");
        public static readonly string jquery_ui_spinner_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.spinner.min.css") ? Content.themes.@base.Url("jquery.ui.spinner.css") : Content.themes.@base.Url("jquery.ui.spinner.min.css");
        public static readonly string jquery_ui_tabs_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.tabs.min.css") ? Content.themes.@base.Url("jquery.ui.tabs.css") : Content.themes.@base.Url("jquery.ui.tabs.min.css");
        public static readonly string jquery_ui_theme_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.theme.min.css") ? Content.themes.@base.Url("jquery.ui.theme.css") : Content.themes.@base.Url("jquery.ui.theme.min.css");
        public static readonly string jquery_ui_tooltip_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/base/jquery.ui.tooltip.min.css") ? Content.themes.@base.Url("jquery.ui.tooltip.css") : Content.themes.@base.Url("jquery.ui.tooltip.min.css");
        private const string URLPATH = "~/Content/themes/base";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base/" + fileName);
        }

        [GeneratedCode("T4MVC", "2.0")]
        [DebuggerNonUserCode]
        public static class images
        {
          public static readonly string animated_overlay_gif = Content.themes.@base.images.Url("animated-overlay.gif");
          public static readonly string ui_bg_flat_0_aaaaaa_40x100_png = Content.themes.@base.images.Url("ui-bg_flat_0_aaaaaa_40x100.png");
          public static readonly string ui_bg_flat_75_ffffff_40x100_png = Content.themes.@base.images.Url("ui-bg_flat_75_ffffff_40x100.png");
          public static readonly string ui_bg_glass_55_fbf9ee_1x400_png = Content.themes.@base.images.Url("ui-bg_glass_55_fbf9ee_1x400.png");
          public static readonly string ui_bg_glass_65_ffffff_1x400_png = Content.themes.@base.images.Url("ui-bg_glass_65_ffffff_1x400.png");
          public static readonly string ui_bg_glass_75_dadada_1x400_png = Content.themes.@base.images.Url("ui-bg_glass_75_dadada_1x400.png");
          public static readonly string ui_bg_glass_75_e6e6e6_1x400_png = Content.themes.@base.images.Url("ui-bg_glass_75_e6e6e6_1x400.png");
          public static readonly string ui_bg_glass_95_fef1ec_1x400_png = Content.themes.@base.images.Url("ui-bg_glass_95_fef1ec_1x400.png");
          public static readonly string ui_bg_highlight_soft_75_cccccc_1x100_png = Content.themes.@base.images.Url("ui-bg_highlight-soft_75_cccccc_1x100.png");
          public static readonly string ui_icons_222222_256x240_png = Content.themes.@base.images.Url("ui-icons_222222_256x240.png");
          public static readonly string ui_icons_2e83ff_256x240_png = Content.themes.@base.images.Url("ui-icons_2e83ff_256x240.png");
          public static readonly string ui_icons_454545_256x240_png = Content.themes.@base.images.Url("ui-icons_454545_256x240.png");
          public static readonly string ui_icons_888888_256x240_png = Content.themes.@base.images.Url("ui-icons_888888_256x240.png");
          public static readonly string ui_icons_cd0a0a_256x240_png = Content.themes.@base.images.Url("ui-icons_cd0a0a_256x240.png");
          private const string URLPATH = "~/Content/themes/base/images";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base/images");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base/images/" + fileName);
          }
        }

        [GeneratedCode("T4MVC", "2.0")]
        [DebuggerNonUserCode]
        public static class minified
        {
          public static readonly string jquery_ui_min_css = Content.themes.@base.minified.Url("jquery-ui.min.css");
          public static readonly string jquery_ui_accordion_min_css = Content.themes.@base.minified.Url("jquery.ui.accordion.min.css");
          public static readonly string jquery_ui_autocomplete_min_css = Content.themes.@base.minified.Url("jquery.ui.autocomplete.min.css");
          public static readonly string jquery_ui_button_min_css = Content.themes.@base.minified.Url("jquery.ui.button.min.css");
          public static readonly string jquery_ui_core_min_css = Content.themes.@base.minified.Url("jquery.ui.core.min.css");
          public static readonly string jquery_ui_datepicker_min_css = Content.themes.@base.minified.Url("jquery.ui.datepicker.min.css");
          public static readonly string jquery_ui_dialog_min_css = Content.themes.@base.minified.Url("jquery.ui.dialog.min.css");
          public static readonly string jquery_ui_menu_min_css = Content.themes.@base.minified.Url("jquery.ui.menu.min.css");
          public static readonly string jquery_ui_progressbar_min_css = Content.themes.@base.minified.Url("jquery.ui.progressbar.min.css");
          public static readonly string jquery_ui_resizable_min_css = Content.themes.@base.minified.Url("jquery.ui.resizable.min.css");
          public static readonly string jquery_ui_selectable_min_css = Content.themes.@base.minified.Url("jquery.ui.selectable.min.css");
          public static readonly string jquery_ui_slider_min_css = Content.themes.@base.minified.Url("jquery.ui.slider.min.css");
          public static readonly string jquery_ui_spinner_min_css = Content.themes.@base.minified.Url("jquery.ui.spinner.min.css");
          public static readonly string jquery_ui_tabs_min_css = Content.themes.@base.minified.Url("jquery.ui.tabs.min.css");
          public static readonly string jquery_ui_theme_min_css = Content.themes.@base.minified.Url("jquery.ui.theme.min.css");
          public static readonly string jquery_ui_tooltip_min_css = Content.themes.@base.minified.Url("jquery.ui.tooltip.min.css");
          private const string URLPATH = "~/Content/themes/base/minified";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base/minified");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base/minified/" + fileName);
          }

          [GeneratedCode("T4MVC", "2.0")]
          [DebuggerNonUserCode]
          public static class images
          {
            public static readonly string animated_overlay_gif = Content.themes.@base.minified.images.Url("animated-overlay.gif");
            public static readonly string ui_bg_flat_0_aaaaaa_40x100_png = Content.themes.@base.minified.images.Url("ui-bg_flat_0_aaaaaa_40x100.png");
            public static readonly string ui_bg_flat_75_ffffff_40x100_png = Content.themes.@base.minified.images.Url("ui-bg_flat_75_ffffff_40x100.png");
            public static readonly string ui_bg_glass_55_fbf9ee_1x400_png = Content.themes.@base.minified.images.Url("ui-bg_glass_55_fbf9ee_1x400.png");
            public static readonly string ui_bg_glass_65_ffffff_1x400_png = Content.themes.@base.minified.images.Url("ui-bg_glass_65_ffffff_1x400.png");
            public static readonly string ui_bg_glass_75_dadada_1x400_png = Content.themes.@base.minified.images.Url("ui-bg_glass_75_dadada_1x400.png");
            public static readonly string ui_bg_glass_75_e6e6e6_1x400_png = Content.themes.@base.minified.images.Url("ui-bg_glass_75_e6e6e6_1x400.png");
            public static readonly string ui_bg_glass_95_fef1ec_1x400_png = Content.themes.@base.minified.images.Url("ui-bg_glass_95_fef1ec_1x400.png");
            public static readonly string ui_bg_highlight_soft_75_cccccc_1x100_png = Content.themes.@base.minified.images.Url("ui-bg_highlight-soft_75_cccccc_1x100.png");
            public static readonly string ui_icons_222222_256x240_png = Content.themes.@base.minified.images.Url("ui-icons_222222_256x240.png");
            public static readonly string ui_icons_2e83ff_256x240_png = Content.themes.@base.minified.images.Url("ui-icons_2e83ff_256x240.png");
            public static readonly string ui_icons_454545_256x240_png = Content.themes.@base.minified.images.Url("ui-icons_454545_256x240.png");
            public static readonly string ui_icons_888888_256x240_png = Content.themes.@base.minified.images.Url("ui-icons_888888_256x240.png");
            public static readonly string ui_icons_cd0a0a_256x240_png = Content.themes.@base.minified.images.Url("ui-icons_cd0a0a_256x240.png");
            private const string URLPATH = "~/Content/themes/base/minified/images";

            public static string Url()
            {
              return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base/minified/images");
            }

            public static string Url(string fileName)
            {
              return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/base/minified/images/" + fileName);
            }
          }
        }
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public static class gray
      {
        public static readonly string jquery_ui_1_9_1_custom_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/gray/jquery-ui-1.9.1.custom.min.css") ? Content.themes.gray.Url("jquery-ui-1.9.1.custom.css") : Content.themes.gray.Url("jquery-ui-1.9.1.custom.min.css");
        private const string URLPATH = "~/Content/themes/gray";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/gray");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/gray/" + fileName);
        }

        [GeneratedCode("T4MVC", "2.0")]
        [DebuggerNonUserCode]
        public static class images
        {
          public static readonly string ui_bg_flat_0_aaaaaa_40x100_png = Content.themes.gray.images.Url("ui-bg_flat_0_aaaaaa_40x100.png");
          public static readonly string ui_bg_flat_75_ffffff_40x100_png = Content.themes.gray.images.Url("ui-bg_flat_75_ffffff_40x100.png");
          public static readonly string ui_bg_glass_55_fbf9ee_1x400_png = Content.themes.gray.images.Url("ui-bg_glass_55_fbf9ee_1x400.png");
          public static readonly string ui_bg_glass_65_ffffff_1x400_png = Content.themes.gray.images.Url("ui-bg_glass_65_ffffff_1x400.png");
          public static readonly string ui_bg_glass_75_dadada_1x400_png = Content.themes.gray.images.Url("ui-bg_glass_75_dadada_1x400.png");
          public static readonly string ui_bg_glass_75_e6e6e6_1x400_png = Content.themes.gray.images.Url("ui-bg_glass_75_e6e6e6_1x400.png");
          public static readonly string ui_bg_glass_95_fef1ec_1x400_png = Content.themes.gray.images.Url("ui-bg_glass_95_fef1ec_1x400.png");
          public static readonly string ui_bg_highlight_soft_75_cccccc_1x100_png = Content.themes.gray.images.Url("ui-bg_highlight-soft_75_cccccc_1x100.png");
          public static readonly string ui_icons_222222_256x240_png = Content.themes.gray.images.Url("ui-icons_222222_256x240.png");
          public static readonly string ui_icons_2e83ff_256x240_png = Content.themes.gray.images.Url("ui-icons_2e83ff_256x240.png");
          public static readonly string ui_icons_454545_256x240_png = Content.themes.gray.images.Url("ui-icons_454545_256x240.png");
          public static readonly string ui_icons_888888_256x240_png = Content.themes.gray.images.Url("ui-icons_888888_256x240.png");
          public static readonly string ui_icons_cd0a0a_256x240_png = Content.themes.gray.images.Url("ui-icons_cd0a0a_256x240.png");
          private const string URLPATH = "~/Content/themes/gray/images";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/gray/images");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/gray/images/" + fileName);
          }
        }
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public static class renderbodyLeft
      {
        public static readonly string jquery_ui_1_9_2_custom_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/renderbodyLeft/jquery-ui-1.9.2.custom.min.css") ? Content.themes.renderbodyLeft.Url("jquery-ui-1.9.2.custom.css") : Content.themes.renderbodyLeft.Url("jquery-ui-1.9.2.custom.min.css");
        private const string URLPATH = "~/Content/themes/renderbodyLeft";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyLeft");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyLeft/" + fileName);
        }

        [GeneratedCode("T4MVC", "2.0")]
        [DebuggerNonUserCode]
        public static class images
        {
          public static readonly string ui_bg_flat_0_aaaaaa_40x100_png = Content.themes.renderbodyLeft.images.Url("ui-bg_flat_0_aaaaaa_40x100.png");
          public static readonly string ui_bg_flat_75_ffffff_40x100_png = Content.themes.renderbodyLeft.images.Url("ui-bg_flat_75_ffffff_40x100.png");
          public static readonly string ui_bg_glass_55_fbf9ee_1x400_png = Content.themes.renderbodyLeft.images.Url("ui-bg_glass_55_fbf9ee_1x400.png");
          public static readonly string ui_bg_glass_65_ffffff_1x400_png = Content.themes.renderbodyLeft.images.Url("ui-bg_glass_65_ffffff_1x400.png");
          public static readonly string ui_bg_glass_75_dadada_1x400_png = Content.themes.renderbodyLeft.images.Url("ui-bg_glass_75_dadada_1x400.png");
          public static readonly string ui_bg_glass_75_e6e6e6_1x400_png = Content.themes.renderbodyLeft.images.Url("ui-bg_glass_75_e6e6e6_1x400.png");
          public static readonly string ui_bg_glass_95_fef1ec_1x400_png = Content.themes.renderbodyLeft.images.Url("ui-bg_glass_95_fef1ec_1x400.png");
          public static readonly string ui_bg_highlight_soft_75_cccccc_1x100_png = Content.themes.renderbodyLeft.images.Url("ui-bg_highlight-soft_75_cccccc_1x100.png");
          public static readonly string ui_icons_222222_256x240_png = Content.themes.renderbodyLeft.images.Url("ui-icons_222222_256x240.png");
          public static readonly string ui_icons_2e83ff_256x240_png = Content.themes.renderbodyLeft.images.Url("ui-icons_2e83ff_256x240.png");
          public static readonly string ui_icons_454545_256x240_png = Content.themes.renderbodyLeft.images.Url("ui-icons_454545_256x240.png");
          public static readonly string ui_icons_888888_256x240_png = Content.themes.renderbodyLeft.images.Url("ui-icons_888888_256x240.png");
          public static readonly string ui_icons_cd0a0a_256x240_png = Content.themes.renderbodyLeft.images.Url("ui-icons_cd0a0a_256x240.png");
          private const string URLPATH = "~/Content/themes/renderbodyLeft/images";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyLeft/images");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyLeft/images/" + fileName);
          }
        }
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public static class renderbodyRight
      {
        public static readonly string jquery_ui_1_9_2_custom_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Content/themes/renderbodyRight/jquery-ui-1.9.2.custom.min.css") ? Content.themes.renderbodyRight.Url("jquery-ui-1.9.2.custom.css") : Content.themes.renderbodyRight.Url("jquery-ui-1.9.2.custom.min.css");
        private const string URLPATH = "~/Content/themes/renderbodyRight";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyRight");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyRight/" + fileName);
        }

        [GeneratedCode("T4MVC", "2.0")]
        [DebuggerNonUserCode]
        public static class images
        {
          public static readonly string ui_bg_flat_0_aaaaaa_40x100_png = Content.themes.renderbodyRight.images.Url("ui-bg_flat_0_aaaaaa_40x100.png");
          public static readonly string ui_bg_flat_75_ffffff_40x100_png = Content.themes.renderbodyRight.images.Url("ui-bg_flat_75_ffffff_40x100.png");
          public static readonly string ui_bg_glass_55_fbf9ee_1x400_png = Content.themes.renderbodyRight.images.Url("ui-bg_glass_55_fbf9ee_1x400.png");
          public static readonly string ui_bg_glass_65_ffffff_1x400_png = Content.themes.renderbodyRight.images.Url("ui-bg_glass_65_ffffff_1x400.png");
          public static readonly string ui_bg_glass_75_dadada_1x400_png = Content.themes.renderbodyRight.images.Url("ui-bg_glass_75_dadada_1x400.png");
          public static readonly string ui_bg_glass_75_e6e6e6_1x400_png = Content.themes.renderbodyRight.images.Url("ui-bg_glass_75_e6e6e6_1x400.png");
          public static readonly string ui_bg_glass_95_fef1ec_1x400_png = Content.themes.renderbodyRight.images.Url("ui-bg_glass_95_fef1ec_1x400.png");
          public static readonly string ui_bg_highlight_soft_75_cccccc_1x100_png = Content.themes.renderbodyRight.images.Url("ui-bg_highlight-soft_75_cccccc_1x100.png");
          public static readonly string ui_icons_222222_256x240_png = Content.themes.renderbodyRight.images.Url("ui-icons_222222_256x240.png");
          public static readonly string ui_icons_2e83ff_256x240_png = Content.themes.renderbodyRight.images.Url("ui-icons_2e83ff_256x240.png");
          public static readonly string ui_icons_454545_256x240_png = Content.themes.renderbodyRight.images.Url("ui-icons_454545_256x240.png");
          public static readonly string ui_icons_888888_256x240_png = Content.themes.renderbodyRight.images.Url("ui-icons_888888_256x240.png");
          public static readonly string ui_icons_cd0a0a_256x240_png = Content.themes.renderbodyRight.images.Url("ui-icons_cd0a0a_256x240.png");
          private const string URLPATH = "~/Content/themes/renderbodyRight/images";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyRight/images");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Content/themes/renderbodyRight/images/" + fileName);
          }
        }
      }
    }
  }
}
