﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers.T4MVC_FeeRuleController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers
{
  [DebuggerNonUserCode]
  [GeneratedCode("T4MVC", "2.0")]
  public class T4MVC_FeeRuleController : FeeRuleController
  {
    public T4MVC_FeeRuleController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override JsonResult Data(GridCommand command, int graduatedProductId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "graduatedProductId", (object) graduatedProductId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult Create()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.Create, (string) null);
    }

    [NonAction]
    public override JsonResult Create(FeeRuleForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Create, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult Edit(int id)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "id", (object) id);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult Edit(FeeRuleForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Delete(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }
  }
}
