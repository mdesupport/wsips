﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models.PublicContact
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models
{
  public class PublicContact
  {
    public int ContactId { get; set; }

    public int UserId { get; set; }

    [StringLength(50)]
    [Required(ErrorMessage = "Email Address is required")]
    [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", ErrorMessage = "Enter a valid email address.")]
    [DisplayName("Email Address")]
    public string EmailAddress { get; set; }

    [DisplayName("First Name")]
    [Required(ErrorMessage = "First Name is required")]
    [StringLength(40)]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Last Name is required")]
    [DisplayName("Last Name")]
    [StringLength(40)]
    public string LastName { get; set; }

    [Required(ErrorMessage = "Daytime Phone is required")]
    [DisplayName("Daytime Phone")]
    [StringLength(50)]
    public string DaytimePhone { get; set; }

    [DisplayName("Business Name")]
    [StringLength(120)]
    public string BusinessName { get; set; }

    [StringLength(100)]
    [Required(ErrorMessage = "Street Address is required")]
    [DisplayName("Street")]
    public string Street { get; set; }

    [DisplayName("City")]
    [StringLength(50)]
    [Required(ErrorMessage = "City is required")]
    public string City { get; set; }

    [DisplayName("State")]
    [Required(ErrorMessage = "State is required")]
    public string StateId { get; set; }

    [StringLength(10)]
    [DisplayName("Zip Code")]
    [Required(ErrorMessage = "Zip Code is required")]
    public string ZipCode { get; set; }

    public bool Active { get; set; }

    public int? UserNameHashCode { get; set; }

    public int? LastNameHashCode { get; set; }

    public bool IsEmailAddressDirty
    {
      get
      {
        return !this.EmailAddress.GetHashCode().Equals((object) this.UserNameHashCode);
      }
    }

    public bool IsLastNameDirty
    {
      get
      {
        return !this.LastName.GetHashCode().Equals((object) this.LastNameHashCode);
      }
    }
  }
}
