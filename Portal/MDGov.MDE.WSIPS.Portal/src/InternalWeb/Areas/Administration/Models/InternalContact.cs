﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models.InternalContact
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models
{
  public class InternalContact
  {
    public int ContactId { get; set; }

    public int UserId { get; set; }

    public string UserName { get; set; }

    [Required(ErrorMessage = "Email Address is required")]
    [DisplayName("Email Address")]
    [StringLength(50)]
    [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", ErrorMessage = "Enter a valid email address.")]
    public string EmailAddress { get; set; }

    [StringLength(40)]
    [Required(ErrorMessage = "First Name is required")]
    [DisplayName("First Name")]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Last Name is required")]
    [StringLength(40)]
    [DisplayName("Last Name")]
    public string LastName { get; set; }

    public bool Active { get; set; }

    public int? LastNameHashCode { get; set; }

    public bool IsLastNameDirty
    {
      get
      {
        return !this.LastName.GetHashCode().Equals((object) this.LastNameHashCode);
      }
    }

    public bool AdministrativeSpecialist { get; set; }

    public bool ComplianceManager { get; set; }

    public bool ComplianceStaff { get; set; }

    public bool DivisionChief { get; set; }

    public bool ITAdministrator { get; set; }

    public bool OperationalAdministrator { get; set; }

    public bool PermitSupervisor { get; set; }

    public bool ProjectManager { get; set; }

    public bool Secretary { get; set; }

    public bool ECommerceMember { get; set; }
  }
}
