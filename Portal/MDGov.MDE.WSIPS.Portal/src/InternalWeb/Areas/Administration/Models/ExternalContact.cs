﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models.ExternalContact
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models
{
  public class ExternalContact
  {
    public int ContactId { get; set; }

    public int UserId { get; set; }

    [Required(ErrorMessage = "User Type is required")]
    [Display(Name = "User Type")]
    public int UserTypeId { get; set; }

    [Display(Name = "County")]
    [RequiredIf("UserTypeId", new object[] {9}, ErrorMessage = "County is required")]
    public int? CountyId { get; set; }

    [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", ErrorMessage = "Enter a valid email address.")]
    [Display(Name = "Email Address")]
    [StringLength(50)]
    [Required(ErrorMessage = "Email Address is required")]
    public string EmailAddress { get; set; }

    [StringLength(40)]
    [Display(Name = "First Name")]
    [Required(ErrorMessage = "First Name is required")]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Last Name is required")]
    [StringLength(40)]
    [DisplayName("Last Name")]
    public string LastName { get; set; }

    [Display(Name = "Daytime Phone")]
    [StringLength(50)]
    [Required(ErrorMessage = "Daytime Phone is required")]
    public string DaytimePhone { get; set; }

    [StringLength(120)]
    [Required(ErrorMessage = "Organization is required")]
    [Display(Name = "Organization")]
    public string Organization { get; set; }

    [Required(ErrorMessage = "Title is required")]
    [Display(Name = "Title")]
    public string TitleId { get; set; }

    [Display(Name = "Active")]
    public bool Active { get; set; }

    public int? UserNameHashCode { get; set; }

    public int? LastNameHashCode { get; set; }

    public bool IsEmailAddressDirty
    {
      get
      {
        return !this.EmailAddress.GetHashCode().Equals((object) this.UserNameHashCode);
      }
    }

    public bool IsLastNameDirty
    {
      get
      {
        return !this.LastName.GetHashCode().Equals((object) this.LastNameHashCode);
      }
    }
  }
}
