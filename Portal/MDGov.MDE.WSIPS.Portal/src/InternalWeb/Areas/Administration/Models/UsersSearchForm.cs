﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models.UsersSearchForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models
{
  public class UsersSearchForm
  {
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserName { get; set; }

    public int UserTypeId { get; set; }

    public string UserRole { get; set; }

    public bool Active { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>()
      {
        new DynamicFilter("Active == @0 && UserId != null", new object[1]
        {
          (object) this.Active
        })
      };
      if (this.FirstName != null)
        dynamicFilterList.Add(new DynamicFilter("FirstName.Contains(@0)", new object[1]
        {
          (object) this.FirstName
        }));
      if (this.LastName != null)
        dynamicFilterList.Add(new DynamicFilter("LastName.Contains(@0)", new object[1]
        {
          (object) this.LastName
        }));
      dynamicFilterList.Add(new DynamicFilter("UserId != null", new object[0]));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }
  }
}
