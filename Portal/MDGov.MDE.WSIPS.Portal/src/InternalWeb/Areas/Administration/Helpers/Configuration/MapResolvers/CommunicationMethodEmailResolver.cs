﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration.MapResolvers.CommunicationMethodEmailResolver
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Permitting.Model;
using System;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration.MapResolvers
{
  public class CommunicationMethodEmailResolver : ValueResolver<Contact, string>
  {
    protected override string ResolveCore(Contact source)
    {
      ContactCommunicationMethod communicationMethod = source.ContactCommunicationMethods.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (c =>
      {
        if (c.IsPrimaryEmail.HasValue)
          return c.IsPrimaryEmail.Value;
        return false;
      }));
      if (communicationMethod != null)
        return communicationMethod.CommunicationValue;
      return string.Empty;
    }
  }
}
