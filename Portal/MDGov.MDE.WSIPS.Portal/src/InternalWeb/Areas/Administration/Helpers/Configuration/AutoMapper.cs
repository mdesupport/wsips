﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration.AdministrationAreaAutoMapperProfile
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration.MapResolvers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration
{
  public class AdministrationAreaAutoMapperProfile : Profile
  {
    public override string ProfileName
    {
      get
      {
        return "Administration";
      }
    }

    protected override void Configure()
    {
      Mapper.CreateMap<Contact, InternalContact>().ForMember((Expression<Func<InternalContact, object>>) (dest => (object) dest.ContactId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<int>((Expression<Func<Contact, int>>) (src => src.Id)))).ForMember((Expression<Func<InternalContact, object>>) (dest => dest.EmailAddress), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<CommunicationMethodEmailResolver>()));
      Mapper.CreateMap<Contact, ExternalContact>().ForMember((Expression<Func<ExternalContact, object>>) (dest => (object) dest.ContactId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<int>((Expression<Func<Contact, int>>) (src => src.Id)))).ForMember((Expression<Func<ExternalContact, object>>) (dest => dest.Organization), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<string>((Expression<Func<Contact, string>>) (src => src.BusinessName)))).ForMember((Expression<Func<ExternalContact, object>>) (dest => (object) dest.UserTypeId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<int>((Expression<Func<Contact, int>>) (src => src.ContactTypeId)))).ForMember((Expression<Func<ExternalContact, object>>) (dest => dest.EmailAddress), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<CommunicationMethodEmailResolver>())).ForMember((Expression<Func<ExternalContact, object>>) (dest => dest.DaytimePhone), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<CommunicationMethodPhoneResolver>()));
      Mapper.CreateMap<Contact, PublicContact>().ForMember((Expression<Func<PublicContact, object>>) (dest => (object) dest.ContactId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<int>((Expression<Func<Contact, int>>) (src => src.Id)))).ForMember((Expression<Func<PublicContact, object>>) (dest => dest.Street), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<string>((Expression<Func<Contact, string>>) (src => src.Address1)))).ForMember((Expression<Func<PublicContact, object>>) (dest => dest.EmailAddress), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<CommunicationMethodEmailResolver>())).ForMember((Expression<Func<PublicContact, object>>) (dest => dest.DaytimePhone), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<CommunicationMethodPhoneResolver>()));
      Mapper.CreateMap<ExternalContact, Contact>().ForMember((Expression<Func<Contact, object>>) (dest => dest.BusinessName), (Action<IMemberConfigurationExpression<ExternalContact>>) (opt => opt.MapFrom<string>((Expression<Func<ExternalContact, string>>) (src => src.Organization)))).ForMember((Expression<Func<Contact, object>>) (dest => (object) dest.ContactTypeId), (Action<IMemberConfigurationExpression<ExternalContact>>) (opt => opt.MapFrom<int>((Expression<Func<ExternalContact, int>>) (src => src.UserTypeId)))).ForMember((Expression<Func<Contact, object>>) (dest => (object) dest.IsBusiness), (Action<IMemberConfigurationExpression<ExternalContact>>) (opt => opt.UseValue<bool>(true)));
      Mapper.CreateMap<PublicContact, Contact>().ForMember((Expression<Func<Contact, object>>) (dest => dest.Address1), (Action<IMemberConfigurationExpression<PublicContact>>) (opt => opt.MapFrom<string>((Expression<Func<PublicContact, string>>) (src => src.Street)))).ForMember((Expression<Func<Contact, object>>) (dest => (object) dest.ContactTypeId), (Action<IMemberConfigurationExpression<PublicContact>>) (opt => opt.UseValue<int>(2)));
      Mapper.CreateMap<InternalContact, Contact>().ForMember((Expression<Func<Contact, object>>) (dest => (object) dest.ContactTypeId), (Action<IMemberConfigurationExpression<InternalContact>>) (opt => opt.UseValue<int>(30)));
      Mapper.CreateMap<Contact, ListContact>().ForMember((Expression<Func<ListContact, object>>) (dest => (object) dest.ContactId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<int>((Expression<Func<Contact, int>>) (src => src.Id)))).ForMember((Expression<Func<ListContact, object>>) (dest => dest.EmailAddress), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<CommunicationMethodEmailResolver>()));
      Mapper.CreateMap<ListProjectManagerCounty, ProjectManagerCounty>();
      Mapper.CreateMap<Contact, ListStaffMember>().ForMember((Expression<Func<ListStaffMember, object>>) (dest => (object) dest.ContactId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<int>((Expression<Func<Contact, int>>) (src => src.Id)))).ForMember((Expression<Func<ListStaffMember, object>>) (dest => (object) dest.SupervisorId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<SupervisorResolver>())).ForMember((Expression<Func<ListStaffMember, object>>) (dest => (object) dest.DivisionChiefId), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.ResolveUsing<DivisionChiefResolver>())).ForMember((Expression<Func<ListStaffMember, object>>) (dest => dest.Name), (Action<IMemberConfigurationExpression<Contact>>) (opt => opt.MapFrom<string>((Expression<Func<Contact, string>>) (src => string.Format("{0} {1}", src.FirstName, src.LastName)))));
    }
  }
}
