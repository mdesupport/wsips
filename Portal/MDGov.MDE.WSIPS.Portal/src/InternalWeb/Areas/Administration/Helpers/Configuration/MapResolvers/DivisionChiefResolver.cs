﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration.MapResolvers.DivisionChiefResolver
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Permitting.Model;
using System;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration.MapResolvers
{
  public class DivisionChiefResolver : ValueResolver<Contact, int?>
  {
    protected override int? ResolveCore(Contact source)
    {
      if (source.Supervisors.Select<Supervisor, int?>((Func<Supervisor, int?>) (s => s.DivisionChiefId)).FirstOrDefault<int?>().HasValue)
        return source.Supervisors.First<Supervisor>().DivisionChiefId;
      return new int?();
    }
  }
}
