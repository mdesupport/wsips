﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.ApplicationRoles
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers
{
  public static class ApplicationRoles
  {
    public const string Admin = "IT Administrator";
    public const string AdministrativeSpecialist = "Administrative Specialist";
    public const string ComplianceEnforcementManager = "Compliance / Enforcement Manager";
    public const string ComplianceEnforcementStaff = "Compliance / Enforcement Staff";
    public const string DivisionChief = "Division Chief";
    public const string eCommerceMember = "WSIPS eCommerce Member";
    public const string MdeStaff = "MDE Staff";
    public const string OperationalAdmin = "Operational Administrator";
    public const string PermitSupervisor = "Permit Supervisor";
    public const string ProjectManager = "Project Manager";
    public const string Secretary = "Secretary";
  }
}
