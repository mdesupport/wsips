﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Controllers.UsersController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ActiveDirectory;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Controllers
{
  public class UsersController : BaseController
  {
    private static readonly UsersController.ActionNamesClass s_actions = new UsersController.ActionNamesClass();
    private static readonly UsersController.ActionParamsClass_CreateInternal s_params_CreateInternal = new UsersController.ActionParamsClass_CreateInternal();
    private static readonly UsersController.ActionParamsClass_CreateExternal s_params_CreateExternal = new UsersController.ActionParamsClass_CreateExternal();
    private static readonly UsersController.ActionParamsClass_CreatePublic s_params_CreatePublic = new UsersController.ActionParamsClass_CreatePublic();
    private static readonly UsersController.ActionParamsClass_EditInternal s_params_EditInternal = new UsersController.ActionParamsClass_EditInternal();
    private static readonly UsersController.ActionParamsClass_EditExternal s_params_EditExternal = new UsersController.ActionParamsClass_EditExternal();
    private static readonly UsersController.ActionParamsClass_EditPublic s_params_EditPublic = new UsersController.ActionParamsClass_EditPublic();
    private static readonly UsersController.ActionParamsClass_Activate s_params_Activate = new UsersController.ActionParamsClass_Activate();
    private static readonly UsersController.ActionParamsClass_Deactivate s_params_Deactivate = new UsersController.ActionParamsClass_Deactivate();
    private static readonly UsersController.ActionParamsClass_AssignToCounty s_params_AssignToCounty = new UsersController.ActionParamsClass_AssignToCounty();
    private static readonly UsersController.ActionParamsClass_AssignToSupervisor s_params_AssignToSupervisor = new UsersController.ActionParamsClass_AssignToSupervisor();
    private static readonly UsersController.ActionParamsClass_GetStaffForRoleData s_params_GetStaffForRoleData = new UsersController.ActionParamsClass_GetStaffForRoleData();
    private static readonly UsersController.ActionParamsClass_GetAvailableStaff s_params_GetAvailableStaff = new UsersController.ActionParamsClass_GetAvailableStaff();
    private static readonly UsersController.ActionParamsClass_GetAssignedStaff s_params_GetAssignedStaff = new UsersController.ActionParamsClass_GetAssignedStaff();
    private static readonly UsersController.ActionParamsClass_Data s_params_Data = new UsersController.ActionParamsClass_Data();
    private static readonly UsersController.ActionParamsClass_ActiveDirData s_params_ActiveDirData = new UsersController.ActionParamsClass_ActiveDirData();
    private static readonly UsersController.ActionParamsClass_SearchFilter s_params_SearchFilter = new UsersController.ActionParamsClass_SearchFilter();
    private static readonly UsersController.ViewsClass s_views = new UsersController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Administration";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Users";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Users";
    private readonly IService<LU_County> _contactCountyService;
    private readonly IUpdatableService<Contact> _contactService;
    private readonly IService<LU_ContactType> _contactTypeService;
    private readonly IService<LU_Title> _contactTitleService;
    private readonly IUpdatableService<ContactCommunicationMethod> _contactCommunicationMethod;
    private readonly IService<LU_CommunicationMethod> _contactCommunicationMethodId;
    private readonly IService<LU_State> _stateService;
    private readonly IMembershipAuthenticationService _membershipService;
    private readonly IUpdatableService<ProjectManager> _projectManagerService;
    private readonly IUpdatableService<Supervisor> _supervisorService;
    private readonly IUpdatableService<ProjectManagerCounty> _projectManagerCounty;
    private readonly IUpdatableService<DivisionChief> _divisionChiefService;
    private readonly IUpdatableService<Secretary> _secretaryService;
    private readonly IUpdatableService<ComplianceManager> _complianceManagerService;
    private readonly IUpdatableService<ComplianceStaff> _complianceStaffService;
    private readonly IUpdatableService<AdministrativeSpecialist> _administrativeSpecialistService;

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UsersController Actions
    {
      get
      {
        return MVC.Administration.Users;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UsersController.ActionNamesClass ActionNames
    {
      get
      {
        return UsersController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UsersController.ActionParamsClass_CreateInternal CreateInternalParams
    {
      get
      {
        return UsersController.s_params_CreateInternal;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UsersController.ActionParamsClass_CreateExternal CreateExternalParams
    {
      get
      {
        return UsersController.s_params_CreateExternal;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UsersController.ActionParamsClass_CreatePublic CreatePublicParams
    {
      get
      {
        return UsersController.s_params_CreatePublic;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_EditInternal EditInternalParams
    {
      get
      {
        return UsersController.s_params_EditInternal;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_EditExternal EditExternalParams
    {
      get
      {
        return UsersController.s_params_EditExternal;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_EditPublic EditPublicParams
    {
      get
      {
        return UsersController.s_params_EditPublic;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_Activate ActivateParams
    {
      get
      {
        return UsersController.s_params_Activate;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_Deactivate DeactivateParams
    {
      get
      {
        return UsersController.s_params_Deactivate;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_AssignToCounty AssignToCountyParams
    {
      get
      {
        return UsersController.s_params_AssignToCounty;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UsersController.ActionParamsClass_AssignToSupervisor AssignToSupervisorParams
    {
      get
      {
        return UsersController.s_params_AssignToSupervisor;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UsersController.ActionParamsClass_GetStaffForRoleData GetStaffForRoleDataParams
    {
      get
      {
        return UsersController.s_params_GetStaffForRoleData;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_GetAvailableStaff GetAvailableStaffParams
    {
      get
      {
        return UsersController.s_params_GetAvailableStaff;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_GetAssignedStaff GetAssignedStaffParams
    {
      get
      {
        return UsersController.s_params_GetAssignedStaff;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_Data DataParams
    {
      get
      {
        return UsersController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_ActiveDirData ActiveDirDataParams
    {
      get
      {
        return UsersController.s_params_ActiveDirData;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ActionParamsClass_SearchFilter SearchFilterParams
    {
      get
      {
        return UsersController.s_params_SearchFilter;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UsersController.ViewsClass Views
    {
      get
      {
        return UsersController.s_views;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected UsersController(Dummy d)
    {
    }

    public UsersController(IService<LU_County> contactCountyService, IUpdatableService<Contact> contactService, IService<LU_ContactType> contactTypeService, IService<LU_Title> contactTitleService, IUpdatableService<ContactCommunicationMethod> contactCommunicationMethod, IService<LU_CommunicationMethod> contactCommunicationMethodId, IService<LU_State> stateService, IMembershipAuthenticationService membershipService, IUpdatableService<ProjectManager> projectManagerService, IUpdatableService<Supervisor> supervisorService, IUpdatableService<ProjectManagerCounty> projectManagerCounty, IUpdatableService<DivisionChief> divisionChiefService, IUpdatableService<Secretary> secretaryService, IUpdatableService<ComplianceManager> complianceManagerService, IUpdatableService<ComplianceStaff> complianceStaffService, IUpdatableService<AdministrativeSpecialist> administrativeSpecialistService)
    {
      this._contactCountyService = contactCountyService;
      this._contactService = contactService;
      this._contactTypeService = contactTypeService;
      this._contactTitleService = contactTitleService;
      this._contactCommunicationMethod = contactCommunicationMethod;
      this._contactCommunicationMethodId = contactCommunicationMethodId;
      this._stateService = stateService;
      this._membershipService = membershipService;
      this._projectManagerService = projectManagerService;
      this._supervisorService = supervisorService;
      this._projectManagerCounty = projectManagerCounty;
      this._divisionChiefService = divisionChiefService;
      this._secretaryService = secretaryService;
      this._complianceManagerService = complianceManagerService;
      this._complianceStaffService = complianceStaffService;
      this._administrativeSpecialistService = administrativeSpecialistService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual ActionResult CreateInternal()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreateInternal, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult EditInternal()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditInternal, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult EditExternal()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditExternal, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult EditPublic()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPublic, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult Activate()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Activate, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult Deactivate()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Deactivate, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult GetStaffForRoleData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetStaffForRoleData, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult GetAvailableStaff()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetAvailableStaff, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult GetAssignedStaff()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetAssignedStaff, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult ActiveDirData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ActiveDirData, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult SearchFilter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchFilter, (string) null);
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult Index()
    {
      UsersSearchForm sessionVar = SessionHandler.GetSessionVar<UsersSearchForm>(SessionVariables.UserSearchFilter);
      if (sessionVar != null)
      {
        ViewBag.SearchFirstName = sessionVar.FirstName;
        ViewBag.SearchLastName = sessionVar.LastName;
        ViewBag.SearchUserName = sessionVar.UserName;
        ViewBag.SearchUserTypeId = sessionVar.UserTypeId;
        ViewBag.SearchRole = sessionVar.UserRole;
        ViewBag.SearchActive = sessionVar.Active;
      }
      this.GetMemberTypes();
      this.GetRoles();
      if (this.TempData["ErrorMessage"] != null)
        this.ModelState.AddModelError("", "Unable to deactivate account until county and / or staff assignments are modified");
      return (ActionResult) this.View();
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult SearchInternal()
    {
      return (ActionResult) this.View();
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult CreateInternal(string userName, string firstName, string lastName)
    {
      new List<string>() { "MDE Staff" };
      InternalContact internalContact = new InternalContact()
      {
        UserName = userName,
        FirstName = firstName,
        LastName = lastName,
        Active = true
      };
      this.GetRoles();
      ViewBag.HasDefaultRole = true;
      return (ActionResult) this.View((object) internalContact);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult CreateInternal(InternalContact contact)
    {
            ActionResult returnAction;
            try
            {
                if (ModelState.IsValid)
                {
                    // Create membership Record
                    _membershipService.CreateAccount(contact.UserName, contact.EmailAddress, "Internal", ViewBag.UserAccount);
                    contact.UserId = _membershipService.GetUserIdByName(contact.UserName);
                    _membershipService.UpdateUserInfo(contact.UserId, null, contact.LastName);

                    // Create User Permissions
                    var userRoles = GetUserRoles(contact).ToList();
                    var errorMessage = new List<string>();
                    _membershipService.AssignRoles(contact.UserName, userRoles.ToArray());

                    // Save internal user
                    Contact intContact = Mapper.Map<InternalContact, Contact>(contact);
                    intContact.ApplicationNotification = true;
                    intContact.EmailNotification = false;
                    intContact.Active = true;

                    // Save a contact and contact methods (email, phone)
                    var id = _contactService.Save(intContact);
                    CreateContactMethods(id, contact.EmailAddress, null);

                    UpdateRoles(id, userRoles, errorMessage, false);

                    returnAction = RedirectToAction(MVC.Administration.Users.Index());
                }
                else
                {
                    GetRoles();
                    ViewBag.PageTitle = "Create Internal User";
                    returnAction = View(contact);
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
                returnAction = RedirectToAction(MVC.Administration.Users.Index());
            }

            return returnAction;
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult CreateExternal()
    {
      ExternalContact externalContact = new ExternalContact()
      {
        Active = true
      };
      this.GetUserTypes();
      this.GetRoles();
      this.GetCounties();
      this.GetTitles();
      return (ActionResult) this.View((object) externalContact);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult CreateExternal(ExternalContact contact, string[] userRoles)
    {
            ActionResult returnAction;
            try
            {
                if (ModelState.IsValid)
                {
                    // Check for possible duplicates
                    var existing = _contactCommunicationMethod.GetRange(0, 1, null, new[] { new DynamicFilter("CommunicationValue == \"" + contact.EmailAddress + "\"") });
                    if (existing != null)
                    {
                        if (existing.Any())
                        {
                            ModelState.AddModelError(String.Empty, "The user with this email address already exists.");
                            GetUserTypes();
                            GetCounties();
                            GetRoles();
                            GetTitles();
                            ViewBag.PageTitle = "Create External Authority";

                            return View(contact);
                        }
                    }

                    // Create membership Record
                    _membershipService.CreateAccount(contact.EmailAddress, contact.EmailAddress, "External Authority", ViewBag.UserAccount);
                    contact.UserId = _membershipService.GetUserIdByName(contact.EmailAddress);
                    _membershipService.UpdateUserInfo(contact.UserId, null, contact.LastName);

                    // Create User Permissions
                    _membershipService.AssignRoles(contact.EmailAddress, userRoles);

                    // Save external user record
                    var external = Mapper.Map<ExternalContact, Contact>(contact);
                    external.ApplicationNotification = true;
                    external.EmailNotification = true;
                    external.Active = true;

                    var id = _contactService.Save(external);
                    CreateContactMethods(id, contact.EmailAddress, contact.DaytimePhone);

                    returnAction = RedirectToAction(MVC.Administration.Users.Index());
                }
                else
                {
                    GetUserTypes();
                    GetCounties();
                    GetRoles();
                    GetTitles();
                    ViewBag.PageTitle = "Create External Authority";
                    returnAction = View(contact);
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
                returnAction = RedirectToAction(MVC.Administration.Users.Index());
            }

            return returnAction;
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult CreatePublic()
    {
      PublicContact publicContact = new PublicContact()
      {
        Active = true
      };
      this.GetStates();
      return (ActionResult) this.View((object) publicContact);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult CreatePublic(PublicContact contact, string[] userRoles)
    {
            ActionResult returnAction;
            try
            {
                if (ModelState.IsValid)
                {
                    // Check for possible duplicates
                    var existingUserId = _membershipService.GetUserIdByName(contact.EmailAddress);
                    if (existingUserId > 0)
                    {
                        ModelState.AddModelError(String.Empty, "A user with this email address already exists.");
                        GetStates();
                        return View(contact);
                    }

                    // Create membership Record
                    _membershipService.CreateAccount(contact.EmailAddress, contact.EmailAddress, "Public", ViewBag.UserAccount);
                    contact.UserId = _membershipService.GetUserIdByName(contact.EmailAddress);
                    _membershipService.UpdateUserInfo(contact.UserId, null, contact.LastName);

                    // Create User Permissions
                    _membershipService.AssignRoles(contact.EmailAddress, userRoles);

                    // Create public user record
                    var pubContact = Mapper.Map<PublicContact, Contact>(contact);
                    pubContact.ApplicationNotification = true;
                    pubContact.EmailNotification = true;

                    if (!String.IsNullOrWhiteSpace(pubContact.BusinessName)) pubContact.IsBusiness = true;

                    var id = _contactService.Save(pubContact);
                    CreateContactMethods(id, contact.EmailAddress, contact.DaytimePhone);

                    returnAction = RedirectToAction(MVC.Administration.Users.Index());
                }
                else
                {
                    GetStates();
                    returnAction = View(contact);
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
                returnAction = RedirectToAction(MVC.Administration.Users.Index());
            }

            return returnAction;
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult EditInternal(int id)
    {
      InternalContact intContact = Mapper.Map<Contact, InternalContact>(this._contactService.GetById(id, "ContactCommunicationMethods"));
      UserListing userById = this._membershipService.GetUserById(intContact.UserId);
      List<string> list = this._membershipService.GetUserRoles(userById.UserName).ToList<string>();
      intContact.UserName = userById.UserName;
      UsersController.BuildRoles(intContact, list);
      this.GetRoles();
      this.GetUserTypes();
      ViewBag.HasDefaultRole = list.Count == 0;
      intContact.LastNameHashCode = new int?(intContact.LastName == null ? 0 : intContact.LastName.GetHashCode());
      if (this.TempData["ErrorMessage"] != null)
      {
        foreach (string errorMessage in (List<string>) this.TempData["ErrorMessage"])
          this.ModelState.AddModelError("", errorMessage);
      }
      return (ActionResult) this.View((object) intContact);
    }

    private static void BuildRoles(InternalContact intContact, List<string> roles)
    {
      intContact.AdministrativeSpecialist = roles.Contains("Administrative Specialist");
      intContact.ComplianceManager = roles.Contains("Compliance / Enforcement Manager");
      intContact.ComplianceStaff = roles.Contains("Compliance / Enforcement Staff");
      intContact.DivisionChief = roles.Contains("Division Chief");
      intContact.ITAdministrator = roles.Contains("IT Administrator");
      intContact.OperationalAdministrator = roles.Contains("Operational Administrator");
      intContact.PermitSupervisor = roles.Contains("Permit Supervisor");
      intContact.ProjectManager = roles.Contains("Project Manager");
      intContact.Secretary = roles.Contains("Secretary");
      intContact.ECommerceMember = roles.Contains("WSIPS eCommerce Member");
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult EditInternal(InternalContact contact)
    {
      ActionResult actionResult;
      try
      {
        if (this.ModelState.IsValid)
        {
          List<string> list = ((IEnumerable<string>) this.GetUserRoles(contact)).ToList<string>();
          List<string> errorMessages = new List<string>();
          if (contact.IsLastNameDirty)
            this._membershipService.UpdateUserInfo(contact.UserId, (string) null, contact.LastName);
          Contact byId = this._contactService.GetById(contact.ContactId, (string) null);
          Mapper.Map<InternalContact, Contact>(contact, byId);
          this.UpdateCommunicationMethods(contact.ContactId, contact.EmailAddress, (string) null);
          this._contactService.Save(byId);
          this.UpdateRoles(contact.ContactId, list, errorMessages, false);
          this._membershipService.AssignRoles(contact.UserName, list.ToArray());
          if (errorMessages.Count == 0)
          {
            actionResult = (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
          }
          else
          {
            this.TempData["ErrorMessage"] = (object) errorMessages;
            return (ActionResult) this.RedirectToAction(MVC.Administration.Users.EditInternal(contact.ContactId));
          }
        }
        else
        {
          ViewBag.PageTitle = "Edit Internal User";
          this.GetRoles();
          actionResult = (ActionResult) this.View((object) contact);
        }
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        actionResult = (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
      }
      this.GetRoles();
      return actionResult;
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult EditExternal(int id)
    {
      this.GetUserTypes();
      this.GetCounties();
      this.GetTitles();
      ExternalContact externalContact = Mapper.Map<Contact, ExternalContact>(this._contactService.GetById(id, "ContactCommunicationMethods"));
      externalContact.UserNameHashCode = new int?(externalContact.EmailAddress == null ? 0 : externalContact.EmailAddress.GetHashCode());
      externalContact.LastNameHashCode = new int?(externalContact.LastName == null ? 0 : externalContact.LastName.GetHashCode());
      return (ActionResult) this.View((object) externalContact);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult EditExternal(ExternalContact contact)
    {
      ActionResult actionResult;
      try
      {
        if (this.ModelState.IsValid)
        {
          int userIdByName = this._membershipService.GetUserIdByName(contact.EmailAddress);
          if (userIdByName > 0 && userIdByName != contact.UserId)
          {
            this.ModelState.AddModelError(string.Empty, "A user with this email address already exists.");
            this.GetUserTypes();
            this.GetCounties();
            this.GetTitles();
            return (ActionResult) this.View((object) contact);
          }
          Contact byId = this._contactService.GetById(contact.ContactId, (string) null);
          Mapper.Map<ExternalContact, Contact>(contact, byId);
          if (contact.IsEmailAddressDirty || contact.IsLastNameDirty)
            this._membershipService.UpdateUserInfo(contact.UserId, contact.EmailAddress, contact.LastName);
          this.UpdateCommunicationMethods(contact.ContactId, contact.EmailAddress, contact.DaytimePhone);
          if (byId.ContactTypeId != 9)
            byId.CountyId = new int?();
          this._contactService.Save(byId);
          actionResult = (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
        }
        else
        {
          this.GetUserTypes();
          this.GetCounties();
          this.GetTitles();
          ViewBag.PageTitle = "Edit External Authority";
          actionResult = (ActionResult) this.View((object) contact);
        }
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        actionResult = (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
      }
      return actionResult;
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult EditPublic(int id)
    {
      this.GetStates();
      PublicContact publicContact = Mapper.Map<Contact, PublicContact>(this._contactService.GetById(id, "ContactCommunicationMethods"));
      publicContact.UserNameHashCode = new int?(publicContact.EmailAddress == null ? 0 : publicContact.EmailAddress.GetHashCode());
      publicContact.LastNameHashCode = new int?(publicContact.LastName == null ? 0 : publicContact.LastName.GetHashCode());
      return (ActionResult) this.View((object) publicContact);
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    [HttpPost]
    public virtual ActionResult EditPublic(PublicContact contact)
    {
      ActionResult actionResult;
      try
      {
        if (this.ModelState.IsValid)
        {
          int userIdByName = this._membershipService.GetUserIdByName(contact.EmailAddress);
          if (userIdByName > 0 && userIdByName != contact.UserId)
          {
            this.ModelState.AddModelError(string.Empty, "A user with this email address already exists.");
            this.GetStates();
            return (ActionResult) this.View((object) contact);
          }
          Contact byId = this._contactService.GetById(contact.ContactId, (string) null);
          Mapper.Map<PublicContact, Contact>(contact, byId);
          if (contact.IsEmailAddressDirty || contact.IsLastNameDirty)
            this._membershipService.UpdateUserInfo(contact.UserId, contact.EmailAddress, contact.LastName);
          this.UpdateCommunicationMethods(contact.ContactId, contact.EmailAddress, contact.DaytimePhone);
          this._contactService.Save(byId);
          actionResult = (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
        }
        else
        {
          this.GetStates();
          actionResult = (ActionResult) this.View((object) contact);
        }
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        actionResult = (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
      }
      return actionResult;
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    [HttpGet]
    public virtual ActionResult Activate(int id)
    {
      try
      {
        Contact byId = this._contactService.GetById(id, (string) null);
        byId.Active = true;
        this._contactService.Save(byId);
        return (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
      }
      catch
      {
        return (ActionResult) this.View();
      }
    }

    [HttpGet]
    [Authorization(AuthorizedRoles = new string[] {"IT Administrator"})]
    public virtual ActionResult Deactivate(int id)
    {
      try
      {
        Contact byId = this._contactService.GetById(id, (string) null);
        UserListing userById = this._membershipService.GetUserById(int.Parse(byId.UserId.ToString()));
        if (userById.UserTypeId == 1)
        {
          List<string> errorMessages = new List<string>();
          List<string> list = this._membershipService.GetUserRoles(userById.UserName).ToList<string>();
          this.UpdateRoles(id, list, errorMessages, true);
          if (errorMessages.Count == 0)
          {
            byId.Active = false;
            this._membershipService.AssignRoles(userById.UserName, new string[0]);
            this._contactService.Save(byId);
          }
          else
            this.TempData["ErrorMessage"] = (object) errorMessages;
        }
        else
        {
          byId.Active = false;
          this._contactService.Save(byId);
        }
        return (ActionResult) this.RedirectToAction(MVC.Administration.Users.Index());
      }
      catch
      {
        return (ActionResult) this.View();
      }
    }

    [HttpGet]
    [Authorization(AuthorizedRoles = new string[] {"Operational Administrator"})]
    public virtual ActionResult AssignToCounty()
    {
      List<ListProjectManager> list = this._projectManagerService.GetAll("Contact,ProjectManagerCounties").OrderBy<ProjectManager, string>((Func<ProjectManager, string>) (x => x.Contact.LastName)).Where<ProjectManager>((Func<ProjectManager, bool>) (manager => manager.SupervisorId.HasValue)).Select<ProjectManager, ListProjectManager>((Func<ProjectManager, ListProjectManager>) (manager => new ListProjectManager()
      {
        ProjectManagerId = manager.Id,
        ContactId = manager.ContactId,
        Name = string.Format("{0} {1}", (object) manager.Contact.FirstName, (object) manager.Contact.LastName),
        Counties = this._projectManagerCounty.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("ProjectManagerId == " + (object) manager.Id, new object[0])
        }, (string) null).Select<ProjectManagerCounty, int?>((Func<ProjectManagerCounty, int?>) (r => r.CountyId)).ToArray<int?>()
      })).ToList<ListProjectManager>();
      ViewBag.Counties = this._contactCountyService.GetAll((string) null).Where<LU_County>((Func<LU_County, bool>) (c =>
      {
        if (c.Id != 10)
          return c.Id != 999;
        return false;
      })).OrderBy<LU_County, string>((Func<LU_County, string>) (x => x.Description)).ToList<LU_County>();
      return (ActionResult) this.View((object) list);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"Operational Administrator"})]
    public virtual ActionResult AssignToCounty(List<ListProjectManagerCounty> data)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          foreach (ProjectManagerCounty projectManagerCounty in this._projectManagerCounty.GetAll((string) null))
            this._projectManagerCounty.Delete(projectManagerCounty.Id);
          foreach (ProjectManagerCounty entity in data.Select<ListProjectManagerCounty, ProjectManagerCounty>(new Func<ListProjectManagerCounty, ProjectManagerCounty>(Mapper.Map<ListProjectManagerCounty, ProjectManagerCounty>)))
            this._projectManagerCounty.Save(entity);
        }
        else
        {
          this.GetCounties();
          return (ActionResult) this.View();
        }
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        this.GetCounties();
        return (ActionResult) this.View();
      }
      return (ActionResult) this.RedirectToAction(MVC.Home.Index());
    }

    [Authorization(AuthorizedRoles = new string[] {"Operational Administrator"})]
    public virtual ActionResult AssignToSupervisor()
    {
      this.GetSupervisorRoles();
      ViewBag.DivisionChiefs = this.GetStaffForRole("Permit Supervisor");
      return (ActionResult) this.View();
    }

    [Authorization(AuthorizedRoles = new string[] {"Operational Administrator"})]
    [HttpPost]
    public virtual JsonResult AssignToSupervisor(string role, int assignTo, int[] available, int[] assigned)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
        if (role.ToUpper() == "Permit Supervisor".ToUpper())
        {
          if (available != null)
          {
            foreach (int num in available)
            {
              ProjectManager entity = this._projectManagerService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("ContactId == " + (object) num, new object[0])
              }, (string) null).SingleOrDefault<ProjectManager>();
              if (entity != null)
              {
                entity.SupervisorId = new int?();
                this._projectManagerService.Save(entity);
              }
            }
          }
          if (assigned != null)
          {
            int id = this._supervisorService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
            {
              new DynamicFilter("ContactId == " + (object) assignTo, new object[0])
            }, (string) null).Single<Supervisor>().Id;
            foreach (int num in assigned)
            {
              ProjectManager entity = this._projectManagerService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("ContactId == " + (object) num, new object[0])
              }, (string) null).SingleOrDefault<ProjectManager>();
              if (entity != null)
              {
                entity.SupervisorId = new int?(id);
                this._projectManagerService.Save(entity);
              }
            }
          }
        }
        else
        {
          if (available != null)
          {
            foreach (int num in available)
            {
              Supervisor entity = this._supervisorService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("ContactId == " + (object) num, new object[0])
              }, (string) null).SingleOrDefault<Supervisor>();
              if (entity != null)
              {
                entity.DivisionChiefId = new int?();
                this._supervisorService.Save(entity);
              }
            }
          }
          if (assigned != null)
          {
            int id = this._divisionChiefService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
            {
              new DynamicFilter("ContactId == " + (object) assignTo, new object[0])
            }, (string) null).Single<DivisionChief>().Id;
            foreach (int num in assigned)
            {
              Supervisor entity = this._supervisorService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("ContactId == " + (object) num, new object[0])
              }, (string) null).SingleOrDefault<Supervisor>();
              if (entity != null)
              {
                entity.DivisionChiefId = new int?(id);
                this._supervisorService.Save(entity);
              }
            }
          }
        }
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual JsonResult GetStaffForRoleData(string role)
    {
      return this.Json((object) this.GetStaffForRole(role), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetAvailableStaff(string role, int id)
    {
      return this.Json((object) this.GetAvailableStaffForRole(role.ToUpper() == "Permit Supervisor".ToUpper() ? "Project Manager" : "Permit Supervisor", id), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetAssignedStaff(string role, int id)
    {
      List<ListStaffMember> listStaffMemberList = new List<ListStaffMember>();
      if (role.ToUpper() == "Permit Supervisor".ToUpper())
      {
        Supervisor supervisor = this._supervisorService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("ContactId == " + (object) id, new object[0])
        }, (string) null).FirstOrDefault<Supervisor>();
        if (supervisor != null)
        {
          IEnumerable<ProjectManager> range = this._projectManagerService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("SupervisorId == " + (object) supervisor.Id, new object[0])
          }, (string) null);
          listStaffMemberList.AddRange(range.Select<ProjectManager, Contact>((Func<ProjectManager, Contact>) (manager => this._contactService.GetById(manager.ContactId, (string) null))).Select<Contact, ListStaffMember>(new Func<Contact, ListStaffMember>(Mapper.Map<Contact, ListStaffMember>)).Where<ListStaffMember>((Func<ListStaffMember, bool>) (listManager => listManager.ContactId != id)));
        }
      }
      else
      {
        DivisionChief divisionChief = this._divisionChiefService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("ContactId == " + (object) id, new object[0])
        }, (string) null).FirstOrDefault<DivisionChief>();
        if (divisionChief != null)
        {
          IEnumerable<Supervisor> range = this._supervisorService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("DivisionChiefId == " + (object) divisionChief.Id, new object[0])
          }, (string) null);
          listStaffMemberList.AddRange(range.Select<Supervisor, Contact>((Func<Supervisor, Contact>) (supervisor => this._contactService.GetById(supervisor.ContactId, (string) null))).Select<Contact, ListStaffMember>(new Func<Contact, ListStaffMember>(Mapper.Map<Contact, ListStaffMember>)).Where<ListStaffMember>((Func<ListStaffMember, bool>) (listSupervisor => listSupervisor.ContactId != id)));
        }
      }
      return this.Json((object) listStaffMemberList, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult Data(GridCommand command)
    {
      UsersSearchForm usersSearchForm = SessionHandler.GetSessionVar<UsersSearchForm>(SessionVariables.UserSearchFilter);
      if (usersSearchForm == null)
        usersSearchForm = new UsersSearchForm()
        {
          Active = true
        };
      List<ListContact> contacts = Mapper.Map<IEnumerable<Contact>, IEnumerable<ListContact>>(this._contactService.GetRange(0, int.MaxValue, (string) null, usersSearchForm.BuildFilter(), (string) null)).ToList<ListContact>();
      string userName = usersSearchForm.UserName;
      int userTypeId = usersSearchForm.UserTypeId;
      string userRole = usersSearchForm.UserRole;
      List<UserListing> list1 = this._membershipService.GetRange(contacts.Select<ListContact, int>((Func<ListContact, int>) (u => u.UserId)).Distinct<int>().ToArray<int>(), userName, userTypeId, userRole).ToList<UserListing>();
      List<UserListing> source = !(command.SortField == string.Empty) ? (command.SortOrder.ToLower() == "desc" ? list1.OrderByDescending<UserListing, string>((Func<UserListing, string>) (m => m.LastName)).ToList<UserListing>() : list1.OrderBy<UserListing, string>((Func<UserListing, string>) (m => m.LastName)).ToList<UserListing>()) : list1.OrderBy<UserListing, string>((Func<UserListing, string>) (m => m.LastName)).ToList<UserListing>();
      List<ListContact> list2 = source.Skip<UserListing>(command.PageSize * (command.Page - 1)).Take<UserListing>(command.PageSize).Select(member => new
      {
        member = member,
        contactInfo = contacts.First<ListContact>((Func<ListContact, bool>) (c => c.UserId == member.UserId))
      }).Select(param0 => new ListContact()
      {
        Active = param0.contactInfo.Active,
        ContactId = param0.contactInfo.ContactId,
        EmailAddress = param0.contactInfo.EmailAddress,
        FirstName = param0.contactInfo.FirstName,
        LastName = param0.contactInfo.LastName,
        UserName = param0.member.UserName,
        ContactTypeId = param0.member.UserTypeId,
        ContactType = param0.member.UserType
      }).ToList<ListContact>();
      int num1 = source.Count<UserListing>();
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) list2,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult ActiveDirData(GridCommand command, string filter = null)
    {
      DirectoryHelper directoryHelper = new DirectoryHelper();
      List<DirectoryUser> source = string.IsNullOrWhiteSpace(filter) ? directoryHelper.GetAllUsers(command.PageSize).ToList<DirectoryUser>() : directoryHelper.FindUsers(filter, command.PageSize).ToList<DirectoryUser>();
      List<DirectoryUser> list1 = source.Skip<DirectoryUser>((command.Page - 1) * command.PageSize).Take<DirectoryUser>(command.PageSize).ToList<DirectoryUser>();
      foreach (DirectoryUser directoryUser in list1)
      {
        directoryUser.UserId = this._membershipService.GetUserIdByName(directoryUser.UserName);
        if (directoryUser.UserId == 0)
        {
          directoryUser.ContactId = 0;
        }
        else
        {
          List<Contact> list2 = this._contactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("UserId == " + (object) directoryUser.UserId, new object[0])
          }, (string) null).ToList<Contact>();
          directoryUser.ContactId = list2.Any<Contact>() ? list2.First<Contact>().Id : 0;
        }
      }
      int num1 = source.Count<DirectoryUser>();
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) list1,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult ClearGridSession()
    {
      SessionHandler.SetSessionVar(SessionVariables.UserSearchFilter, (object) null);
      return (JsonResult) null;
    }

    [HttpPost]
    public virtual JsonResult SearchFilter(UsersSearchForm form)
    {
      SessionHandler.SetSessionVar(SessionVariables.UserSearchFilter, (object) form);
      return this.Json((object) new
      {
        IsSaveSearchResult = false,
        IsSuccess = true
      });
    }

    private void GetUserTypes()
    {
      ViewBag.UserTypes = this._contactTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("IsExternalUser == " + (object) true, new object[0])
      }, (string) null);
    }

    private void GetMemberTypes()
    {
      ViewBag.MemberTypes = this._membershipService.GetUserTypes();
    }

    private void GetCounties()
    {
      ViewBag.Counties = this._contactCountyService.GetAll((string) null).OrderBy<LU_County, string>((Func<LU_County, string>) (x => x.Description));
    }

    private void GetTitles()
    {
      ViewBag.Titles = this._contactTitleService.GetAll((string) null).OrderBy<LU_Title, int?>((Func<LU_Title, int?>) (x => x.Sequence));
    }

    private void GetStates()
    {
      ViewBag.States = this._stateService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == " + (object) true, new object[0])
      }, (string) null);
    }

    private void GetRoles()
    {
      IEnumerable<string> source = this._membershipService.GetRoles().Except<string>((IEnumerable<string>) new List<string>()
      {
        "Applicant / Permittee",
        "External Authority"
      });
      ViewBag.Roles = source.ToList<string>();
      ViewBag.DefaultRole = "MDE Staff";
      ViewBag.ApplicantRole = "Applicant / Permittee";
      ViewBag.ExternalAuthorityRole = "External Authority";
    }

    private void GetSupervisorRoles()
    {
      List<string> stringList = new List<string>()
      {
        "Permit Supervisor",
        "Division Chief"
      };
      ViewBag.SupervisorRoles = stringList;
    }

    private IEnumerable<ListStaffMember> GetStaffForRole(string role)
    {
      return (IEnumerable<ListStaffMember>) this._membershipService.GetUsersInRole(role).Select<int, Contact>((Func<int, Contact>) (id => this._contactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("UserId == " + (object) id, new object[0])
      }, (string) null).FirstOrDefault<Contact>())).Where<Contact>((Func<Contact, bool>) (contact => contact != null)).Select<Contact, ListStaffMember>((Func<Contact, ListStaffMember>) (contact => Mapper.Map<Contact, ListStaffMember>(contact))).ToList<ListStaffMember>().OrderBy<ListStaffMember, string>((Func<ListStaffMember, string>) (c => c.LastName));
    }

    private IEnumerable<ListStaffMember> GetAvailableStaffForRole(string role, int contactId)
    {
      List<ListStaffMember> source1 = new List<ListStaffMember>();
      foreach (int num in this._membershipService.GetUsersInRole(role))
      {
        Contact source2 = this._contactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("UserId == " + (object) num, new object[0])
        }, (string) null).FirstOrDefault<Contact>();
        if (source2 != null)
        {
          ListStaffMember listStaffMember = Mapper.Map<Contact, ListStaffMember>(source2);
          if (role.ToUpper() == "Project Manager".ToUpper())
          {
            ProjectManager projectManager = this._projectManagerService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
            {
              new DynamicFilter("ContactId == " + (object) source2.Id, new object[0])
            }, (string) null).FirstOrDefault<ProjectManager>();
            if (projectManager != null)
              listStaffMember.SupervisorId = projectManager.SupervisorId;
            if (!listStaffMember.SupervisorId.HasValue && listStaffMember.ContactId != contactId)
              source1.Add(listStaffMember);
          }
          else
          {
            Supervisor supervisor = this._supervisorService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
            {
              new DynamicFilter("ContactId == " + (object) source2.Id, new object[0])
            }, (string) null).FirstOrDefault<Supervisor>();
            if (supervisor != null)
              listStaffMember.DivisionChiefId = supervisor.DivisionChiefId;
            if (!listStaffMember.DivisionChiefId.HasValue && listStaffMember.ContactId != contactId)
              source1.Add(listStaffMember);
          }
        }
      }
      return (IEnumerable<ListStaffMember>) source1.OrderBy<ListStaffMember, string>((Func<ListStaffMember, string>) (c => c.LastName));
    }

    private void CreateContactMethods(int contactId, string emailAddress, string dayPhone)
    {
      if (contactId == 0)
        throw new ArgumentException("Invalid Value", "contactId");
      if (!string.IsNullOrEmpty(emailAddress))
      {
        LU_CommunicationMethod communicationMethod = this._contactCommunicationMethodId.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("Description == \"Email\"", new object[0])
        }, (string) null).FirstOrDefault<LU_CommunicationMethod>();
        if (communicationMethod != null)
          this._contactCommunicationMethod.Save(new ContactCommunicationMethod()
          {
            CommunicationMethodId = communicationMethod.Id,
            ContactId = contactId,
            IsPrimaryEmail = new bool?(true),
            CommunicationValue = emailAddress
          });
      }
      if (string.IsNullOrEmpty(dayPhone))
        return;
      LU_CommunicationMethod communicationMethod1 = this._contactCommunicationMethodId.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Description == \"Mobile\"", new object[0])
      }, (string) null).FirstOrDefault<LU_CommunicationMethod>();
      if (communicationMethod1 == null)
        return;
      this._contactCommunicationMethod.Save(new ContactCommunicationMethod()
      {
        CommunicationMethodId = communicationMethod1.Id,
        ContactId = contactId,
        IsPrimaryPhone = new bool?(true),
        CommunicationValue = dayPhone
      });
    }

    private void UpdateCommunicationMethods(int contactId, string emailAddress, string dayPhone)
    {
      IEnumerable<ContactCommunicationMethod> range = this._contactCommunicationMethod.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0])
      }, (string) null);
      if (!string.IsNullOrEmpty(emailAddress))
      {
        ContactCommunicationMethod entity = range.SingleOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (m => m.IsPrimaryEmail.GetValueOrDefault()));
        if (entity == null)
        {
          entity = new ContactCommunicationMethod();
          entity.ContactId = contactId;
          entity.CommunicationMethodId = 3;
          entity.CommunicationValue = emailAddress;
          entity.IsPrimaryEmail = new bool?(true);
        }
        else
        {
          entity.CommunicationValue = emailAddress;
          entity.IsPrimaryEmail = new bool?(true);
        }
        this._contactCommunicationMethod.Save(entity);
      }
      if (string.IsNullOrEmpty(dayPhone))
        return;
      ContactCommunicationMethod entity1 = range.SingleOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (m => m.IsPrimaryPhone.GetValueOrDefault()));
      if (entity1 == null)
      {
        entity1 = new ContactCommunicationMethod();
        entity1.ContactId = contactId;
        entity1.CommunicationMethodId = 2;
        entity1.CommunicationValue = dayPhone;
        entity1.IsPrimaryPhone = new bool?(true);
      }
      else
      {
        entity1.CommunicationValue = dayPhone;
        entity1.IsPrimaryPhone = new bool?(true);
      }
      this._contactCommunicationMethod.Save(entity1);
    }

    private void UpdateProjectManager(int contactId, bool roleSelected, List<string> userRoles, List<string> errorMessages, List<Action> userRoleDeleteActions = null)
    {
      List<ProjectManager> managers = this._projectManagerService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      }, (string) null).ToList<ProjectManager>();
      if (!managers.Any<ProjectManager>() && roleSelected)
      {
        this._projectManagerService.Save(new ProjectManager()
        {
          Active = true,
          ContactId = contactId
        });
      }
      else
      {
        if (!managers.Any<ProjectManager>() || roleSelected)
          return;
        if (this._projectManagerCounty.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("ProjectManagerId == " + (object) managers.Single<ProjectManager>().Id, new object[0])
        }, (string) null).ToList<ProjectManagerCounty>().Any<ProjectManagerCounty>())
        {
          userRoles.Add("Project Manager");
          errorMessages.Add("Removal of the Project Manager role cannot be completed  until all Counties are reassigned");
        }
        else if (userRoleDeleteActions != null)
        {
          userRoleDeleteActions.Add((Action) (() => this._projectManagerService.Delete(managers.Single<ProjectManager>().Id)));
        }
        else
        {
          userRoles.ToList<string>().Remove("Project Manager");
          this._projectManagerService.Delete(managers.Single<ProjectManager>().Id);
        }
      }
    }

    private void UpdateSupervisor(int contactId, bool roleSelected, List<string> userRoles, List<string> errorMessages, List<Action> userRoleDeleteActions = null)
    {
      List<Supervisor> supervisors = this._supervisorService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      }, (string) null).ToList<Supervisor>();
      if (!supervisors.Any<Supervisor>() && roleSelected)
      {
        this._supervisorService.Save(new Supervisor()
        {
          Active = true,
          ContactId = contactId
        });
      }
      else
      {
        if (!supervisors.Any<Supervisor>() || roleSelected)
          return;
        if (this._projectManagerService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("SupervisorId == " + (object) supervisors.Single<Supervisor>().Id, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, (string) null).ToList<ProjectManager>().Any<ProjectManager>())
        {
          userRoles.Add("Permit Supervisor");
          errorMessages.Add("Removal of the Supervisor role cannot be completed  until all Project Managers are reassigned");
        }
        else if (userRoleDeleteActions != null)
        {
          userRoleDeleteActions.Add((Action) (() => this._supervisorService.Delete(supervisors.Single<Supervisor>().Id)));
        }
        else
        {
          userRoles.ToList<string>().Remove("Permit Supervisor");
          this._supervisorService.Delete(supervisors.Single<Supervisor>().Id);
        }
      }
    }

    private void UpdateDivisionChief(int contactId, bool roleSelected, List<string> userRoles, List<string> errorMessages, List<Action> userRoleDeleteActions = null)
    {
      List<DivisionChief> divisionChiefs = this._divisionChiefService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      }, (string) null).ToList<DivisionChief>();
      if (!divisionChiefs.Any<DivisionChief>() && roleSelected)
      {
        this._divisionChiefService.Save(new DivisionChief()
        {
          Active = true,
          ContactId = contactId
        });
      }
      else
      {
        if (!divisionChiefs.Any<DivisionChief>() || roleSelected)
          return;
        if (this._supervisorService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("DivisionChiefId == " + (object) divisionChiefs.Single<DivisionChief>().Id, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, (string) null).ToList<Supervisor>().Any<Supervisor>())
        {
          userRoles.Add("Division Chief");
          errorMessages.Add("Removal of the Division Chief role cannot be completed  until all Supervisors are reassigned");
        }
        else if (userRoleDeleteActions != null)
        {
          userRoleDeleteActions.Add((Action) (() => this._divisionChiefService.Delete(divisionChiefs.Single<DivisionChief>().Id)));
        }
        else
        {
          userRoles.ToList<string>().Remove("Division Chief");
          this._divisionChiefService.Delete(divisionChiefs.Single<DivisionChief>().Id);
        }
      }
    }

    private void UpdateSecretary(int contactId, bool active)
    {
      Secretary secretary = this._secretaryService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0]),
        new DynamicFilter("Active", new object[0])
      }, (string) null).SingleOrDefault<Secretary>();
      if (secretary == null)
      {
        if (!active)
          return;
        this._secretaryService.Save(new Secretary()
        {
          Active = true,
          ContactId = contactId
        });
      }
      else
      {
        if (active)
          return;
        this._secretaryService.Delete(secretary.Id);
      }
    }

    private void UpdateComplianceManager(int contactId, bool active)
    {
      ComplianceManager complianceManager = this._complianceManagerService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0]),
        new DynamicFilter("Active", new object[0])
      }, (string) null).SingleOrDefault<ComplianceManager>();
      if (complianceManager == null)
      {
        if (!active)
          return;
        this._complianceManagerService.Save(new ComplianceManager()
        {
          Active = true,
          ContactId = contactId
        });
      }
      else
      {
        if (active)
          return;
        this._complianceManagerService.Delete(complianceManager.Id);
      }
    }

    private void UpdateComplianceStaff(int contactId, bool active)
    {
      ComplianceStaff complianceStaff = this._complianceStaffService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0]),
        new DynamicFilter("Active", new object[0])
      }, (string) null).SingleOrDefault<ComplianceStaff>();
      if (complianceStaff == null)
      {
        if (!active)
          return;
        this._complianceStaffService.Save(new ComplianceStaff()
        {
          Active = true,
          ContactId = contactId
        });
      }
      else
      {
        if (active)
          return;
        this._complianceStaffService.Delete(complianceStaff.Id);
      }
    }

    private void UpdateAdministrativeSpecialist(int contactId, bool active)
    {
      AdministrativeSpecialist administrativeSpecialist = this._administrativeSpecialistService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("ContactId == " + (object) contactId, new object[0]),
        new DynamicFilter("Active", new object[0])
      }, (string) null).SingleOrDefault<AdministrativeSpecialist>();
      if (administrativeSpecialist == null)
      {
        if (!active)
          return;
        this._administrativeSpecialistService.Save(new AdministrativeSpecialist()
        {
          Active = true,
          ContactId = contactId
        });
      }
      else
      {
        if (active)
          return;
        this._administrativeSpecialistService.Delete(administrativeSpecialist.Id);
      }
    }

    private string[] GetUserRoles(InternalContact contact)
    {
      List<string> stringList = new List<string>();
      if (contact.ComplianceManager)
        stringList.Add("Compliance / Enforcement Manager");
      if (contact.AdministrativeSpecialist)
        stringList.Add("Administrative Specialist");
      if (contact.ComplianceStaff)
        stringList.Add("Compliance / Enforcement Staff");
      if (contact.DivisionChief)
        stringList.Add("Division Chief");
      if (contact.ITAdministrator)
        stringList.Add("IT Administrator");
      if (contact.OperationalAdministrator)
        stringList.Add("Operational Administrator");
      if (contact.PermitSupervisor)
        stringList.Add("Permit Supervisor");
      if (contact.ProjectManager)
        stringList.Add("Project Manager");
      if (contact.Secretary)
        stringList.Add("Secretary");
      if (contact.ECommerceMember)
        stringList.Add("WSIPS eCommerce Member");
      return stringList.ToArray();
    }

    private void UpdateRoles(int contactId, List<string> userRoles, List<string> errorMessages, bool deactivateUser)
    {
      if (deactivateUser)
      {
        List<Action> userRoleDeleteActions = new List<Action>();
        this.UpdateProjectManager(contactId, false, userRoles, errorMessages, userRoleDeleteActions);
        this.UpdateSupervisor(contactId, false, userRoles, errorMessages, userRoleDeleteActions);
        this.UpdateDivisionChief(contactId, false, userRoles, errorMessages, userRoleDeleteActions);
        if (errorMessages.Any<string>())
          return;
        userRoleDeleteActions.ForEach((Action<Action>) (t => t()));
        this.UpdateSecretary(contactId, false);
        this.UpdateComplianceManager(contactId, false);
        this.UpdateComplianceStaff(contactId, false);
        this.UpdateAdministrativeSpecialist(contactId, false);
      }
      else
      {
        this.UpdateProjectManager(contactId, userRoles.Contains("Project Manager"), userRoles, errorMessages, (List<Action>) null);
        this.UpdateSupervisor(contactId, userRoles.Contains("Permit Supervisor"), userRoles, errorMessages, (List<Action>) null);
        this.UpdateDivisionChief(contactId, userRoles.Contains("Division Chief"), userRoles, errorMessages, (List<Action>) null);
        this.UpdateSecretary(contactId, userRoles.Contains("Secretary"));
        this.UpdateComplianceManager(contactId, userRoles.Contains("Compliance / Enforcement Manager"));
        this.UpdateComplianceStaff(contactId, userRoles.Contains("Compliance / Enforcement Staff"));
        this.UpdateAdministrativeSpecialist(contactId, userRoles.Contains("Administrative Specialist"));
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string SearchInternal = "SearchInternal";
      public readonly string CreateInternal = "CreateInternal";
      public readonly string CreateExternal = "CreateExternal";
      public readonly string CreatePublic = "CreatePublic";
      public readonly string EditInternal = "EditInternal";
      public readonly string EditExternal = "EditExternal";
      public readonly string EditPublic = "EditPublic";
      public readonly string Activate = "Activate";
      public readonly string Deactivate = "Deactivate";
      public readonly string AssignToCounty = "AssignToCounty";
      public readonly string AssignToSupervisor = "AssignToSupervisor";
      public readonly string GetStaffForRoleData = "GetStaffForRoleData";
      public readonly string GetAvailableStaff = "GetAvailableStaff";
      public readonly string GetAssignedStaff = "GetAssignedStaff";
      public readonly string Data = "Data";
      public readonly string ActiveDirData = "ActiveDirData";
      public readonly string ClearGridSession = "ClearGridSession";
      public readonly string SearchFilter = "SearchFilter";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string SearchInternal = "SearchInternal";
      public const string CreateInternal = "CreateInternal";
      public const string CreateExternal = "CreateExternal";
      public const string CreatePublic = "CreatePublic";
      public const string EditInternal = "EditInternal";
      public const string EditExternal = "EditExternal";
      public const string EditPublic = "EditPublic";
      public const string Activate = "Activate";
      public const string Deactivate = "Deactivate";
      public const string AssignToCounty = "AssignToCounty";
      public const string AssignToSupervisor = "AssignToSupervisor";
      public const string GetStaffForRoleData = "GetStaffForRoleData";
      public const string GetAvailableStaff = "GetAvailableStaff";
      public const string GetAssignedStaff = "GetAssignedStaff";
      public const string Data = "Data";
      public const string ActiveDirData = "ActiveDirData";
      public const string ClearGridSession = "ClearGridSession";
      public const string SearchFilter = "SearchFilter";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CreateInternal
    {
      public readonly string userName = "userName";
      public readonly string firstName = "firstName";
      public readonly string lastName = "lastName";
      public readonly string contact = "contact";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CreateExternal
    {
      public readonly string contact = "contact";
      public readonly string userRoles = "userRoles";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CreatePublic
    {
      public readonly string contact = "contact";
      public readonly string userRoles = "userRoles";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_EditInternal
    {
      public readonly string id = "id";
      public readonly string contact = "contact";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_EditExternal
    {
      public readonly string id = "id";
      public readonly string contact = "contact";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_EditPublic
    {
      public readonly string id = "id";
      public readonly string contact = "contact";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Activate
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Deactivate
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AssignToCounty
    {
      public readonly string data = "data";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AssignToSupervisor
    {
      public readonly string role = "role";
      public readonly string assignTo = "assignTo";
      public readonly string available = "available";
      public readonly string assigned = "assigned";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetStaffForRoleData
    {
      public readonly string role = "role";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetAvailableStaff
    {
      public readonly string role = "role";
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetAssignedStaff
    {
      public readonly string role = "role";
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string command = "command";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_ActiveDirData
    {
      public readonly string command = "command";
      public readonly string filter = "filter";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SearchFilter
    {
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly UsersController.ViewsClass._ViewNamesClass s_ViewNames = new UsersController.ViewsClass._ViewNamesClass();
      public readonly string Activate = "~/Areas/Administration/Views/Users/Activate.cshtml";
      public readonly string AssignToCounty = "~/Areas/Administration/Views/Users/AssignToCounty.cshtml";
      public readonly string AssignToSupervisor = "~/Areas/Administration/Views/Users/AssignToSupervisor.cshtml";
      public readonly string CreateExternal = "~/Areas/Administration/Views/Users/CreateExternal.cshtml";
      public readonly string CreateInternal = "~/Areas/Administration/Views/Users/CreateInternal.cshtml";
      public readonly string CreatePublic = "~/Areas/Administration/Views/Users/CreatePublic.cshtml";
      public readonly string Deactivate = "~/Areas/Administration/Views/Users/Deactivate.cshtml";
      public readonly string EditExternal = "~/Areas/Administration/Views/Users/EditExternal.cshtml";
      public readonly string EditInternal = "~/Areas/Administration/Views/Users/EditInternal.cshtml";
      public readonly string EditPublic = "~/Areas/Administration/Views/Users/EditPublic.cshtml";
      public readonly string Index = "~/Areas/Administration/Views/Users/Index.cshtml";
      public readonly string SearchInternal = "~/Areas/Administration/Views/Users/SearchInternal.cshtml";

      public UsersController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return UsersController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string Activate = "Activate";
        public readonly string AssignToCounty = "AssignToCounty";
        public readonly string AssignToSupervisor = "AssignToSupervisor";
        public readonly string CreateExternal = "CreateExternal";
        public readonly string CreateInternal = "CreateInternal";
        public readonly string CreatePublic = "CreatePublic";
        public readonly string Deactivate = "Deactivate";
        public readonly string EditExternal = "EditExternal";
        public readonly string EditInternal = "EditInternal";
        public readonly string EditPublic = "EditPublic";
        public readonly string Index = "Index";
        public readonly string SearchInternal = "SearchInternal";
      }
    }
  }
}
