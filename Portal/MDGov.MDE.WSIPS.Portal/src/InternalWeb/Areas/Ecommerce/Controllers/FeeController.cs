﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers.FeeController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers
{
  [Authorization(AuthorizedRoles = new string[] {"IT Administrator", "Administrative Specialist", "Compliance / Enforcement Manager", "Compliance / Enforcement Staff", "Division Chief", "WSIPS eCommerce Member", "Operational Administrator", "Permit Supervisor", "Project Manager", "Secretary"})]
  public class FeeController : Controller
  {
    private static readonly FeeController.ActionNamesClass s_actions = new FeeController.ActionNamesClass();
    private static readonly FeeController.ActionParamsClass_Data s_params_Data = new FeeController.ActionParamsClass_Data();
    private static readonly FeeController.ActionParamsClass_GraduatedData s_params_GraduatedData = new FeeController.ActionParamsClass_GraduatedData();
    private static readonly FeeController.ActionParamsClass_Create s_params_Create = new FeeController.ActionParamsClass_Create();
    private static readonly FeeController.ActionParamsClass_CreateGraduated s_params_CreateGraduated = new FeeController.ActionParamsClass_CreateGraduated();
    private static readonly FeeController.ActionParamsClass_Edit s_params_Edit = new FeeController.ActionParamsClass_Edit();
    private static readonly FeeController.ActionParamsClass_EditGraduated s_params_EditGraduated = new FeeController.ActionParamsClass_EditGraduated();
    private static readonly FeeController.ActionParamsClass_Activate s_params_Activate = new FeeController.ActionParamsClass_Activate();
    private static readonly FeeController.ActionParamsClass_Deactivate s_params_Deactivate = new FeeController.ActionParamsClass_Deactivate();
    private static readonly FeeController.ViewsClass s_views = new FeeController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Area = "Ecommerce";
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Name = "Fee";
    [GeneratedCode("T4MVC", "2.0")]
    public const string NameConst = "Fee";
    private readonly IService<ProductGridModel> _productGridModelService;
    private readonly IUpdatableService<Product> _productService;
    private readonly IService<LU_ProductType> _productTypeService;
    private readonly IService<LU_ApplicationType> _applicationTypeService;
    private readonly IService<LU_PermitType> _permitTypeService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeController Actions
    {
      get
      {
        return MVC.Ecommerce.Fee;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeController.ActionNamesClass ActionNames
    {
      get
      {
        return FeeController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeController.ActionParamsClass_Data DataParams
    {
      get
      {
        return FeeController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeController.ActionParamsClass_GraduatedData GraduatedDataParams
    {
      get
      {
        return FeeController.s_params_GraduatedData;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeController.ActionParamsClass_Create CreateParams
    {
      get
      {
        return FeeController.s_params_Create;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeController.ActionParamsClass_CreateGraduated CreateGraduatedParams
    {
      get
      {
        return FeeController.s_params_CreateGraduated;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeController.ActionParamsClass_Edit EditParams
    {
      get
      {
        return FeeController.s_params_Edit;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeController.ActionParamsClass_EditGraduated EditGraduatedParams
    {
      get
      {
        return FeeController.s_params_EditGraduated;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeController.ActionParamsClass_Activate ActivateParams
    {
      get
      {
        return FeeController.s_params_Activate;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeController.ActionParamsClass_Deactivate DeactivateParams
    {
      get
      {
        return FeeController.s_params_Deactivate;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeController.ViewsClass Views
    {
      get
      {
        return FeeController.s_views;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected FeeController(Dummy d)
    {
    }

    public FeeController(IService<ProductGridModel> productGridModelService, IUpdatableService<Product> productService, IService<LU_ProductType> productTypeService, IService<LU_ApplicationType> applicationTypeService, IService<LU_PermitType> permitTypeService)
    {
      this._productGridModelService = productGridModelService;
      this._productService = productService;
      this._productTypeService = productTypeService;
      this._applicationTypeService = applicationTypeService;
      this._permitTypeService = permitTypeService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult GraduatedData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GraduatedData, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual PartialViewResult Edit()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult EditGraduated()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.EditGraduated, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult Activate()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Activate, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult Deactivate()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Deactivate, (string) null);
    }

    [HttpGet]
    public virtual ActionResult Index()
    {
      List<SelectListItem> selectListItemList = new List<SelectListItem>()
      {
        new SelectListItem() { Text = "Flat Fee", Value = "1" },
        new SelectListItem() { Text = "Graduated Fee", Value = "2" }
      };
      return (ActionResult) this.View((object) new ManageFeesForm()
      {
        FeeCategoryId = 1,
        FeeCategoryList = new SelectList((IEnumerable) selectListItemList, "Value", "Text")
      });
    }

    [HttpGet]
    public virtual JsonResult Data(GridCommand command)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>()
      {
        {
          "producttype",
          "LU_ProductType.Description"
        },
        {
          "effectivedate",
          "EffectiveDate"
        },
        {
          "lastmodified",
          "LastModifiedDate"
        }
      };
      int num1 = this._productGridModelService.Count((IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("IsGraduated == false", new object[0])
      });
      if (num1 == 0)
        return this.Json((object) new GridData()
        {
          Data = (IEnumerable) null,
          PageTotal = 0,
          CurrentPage = 0,
          RecordCount = 0
        }, JsonRequestBehavior.AllowGet);
      IEnumerable<ProductGridModel> range = this._productGridModelService.GetRange(command.Skip(), command.PageSize, command.BuildSort((IDictionary<string, string>) dictionary) ?? "LastModifiedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("IsGraduated == false", new object[0])
      }, (string) null);
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) range,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GraduatedData(GridCommand command)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>()
      {
        {
          "producttype",
          "LU_ProductType.Description"
        },
        {
          "effectivedate",
          "EffectiveDate"
        },
        {
          "lastmodified",
          "LastModifiedDate"
        }
      };
      int num1 = this._productGridModelService.Count((IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("IsGraduated == true", new object[0])
      });
      if (num1 == 0)
        return this.Json((object) new GridData()
        {
          Data = (IEnumerable) null,
          PageTotal = 0,
          CurrentPage = 0,
          RecordCount = 0
        }, JsonRequestBehavior.AllowGet);
      IEnumerable<ProductGridModel> range = this._productGridModelService.GetRange(command.Skip(), command.PageSize, command.BuildSort((IDictionary<string, string>) dictionary) ?? "LastModifiedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("IsGraduated == true", new object[0])
      }, (string) null);
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) range,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual PartialViewResult Create()
    {
      return this.PartialView((object) new FeeForm()
      {
        FeeTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._productTypeService.GetAll((string) null)),
        ApplicationTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._applicationTypeService.GetAll((string) null)),
        PermitTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._permitTypeService.GetAll((string) null))
      });
    }

    [HttpPost]
    public virtual JsonResult Create(FeeForm form)
    {
      this._productService.Save(Mapper.Map<FeeForm, Product>(form));
      return this.Json((object) new{ Success = true });
    }

    [HttpGet]
    public virtual PartialViewResult CreateGraduated()
    {
      return this.PartialView((object) new GraduatedFeeForm()
      {
        FeeTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._productTypeService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("CanBeGraduated == true", new object[0])
        }, (string) null)),
        ApplicationTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._applicationTypeService.GetAll((string) null)),
        PermitTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._permitTypeService.GetAll((string) null))
      });
    }

    [HttpPost]
    public virtual JsonResult CreateGraduated(GraduatedFeeForm form)
    {
      return this.Json((object) new
      {
        Success = true,
        Id = this._productService.Save(Mapper.Map<GraduatedFeeForm, Product>(form))
      });
    }

    [HttpGet]
    public virtual PartialViewResult Edit(int id)
    {
      FeeForm feeForm = Mapper.Map<Product, FeeForm>(this._productService.GetById(id, (string) null));
      feeForm.FeeTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._productTypeService.GetAll((string) null));
      feeForm.ApplicationTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._applicationTypeService.GetAll((string) null));
      feeForm.PermitTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._permitTypeService.GetAll((string) null));
      return this.PartialView((object) feeForm);
    }

    [HttpPost]
    public virtual JsonResult Edit(FeeForm form)
    {
      this._productService.Save(Mapper.Map<FeeForm, Product>(form));
      return this.Json((object) new{ Success = true });
    }

    [HttpGet]
    public virtual PartialViewResult EditGraduated(int id)
    {
      GraduatedFeeForm graduatedFeeForm = Mapper.Map<Product, GraduatedFeeForm>(this._productService.GetById(id, "GraduatedProductRules"));
      graduatedFeeForm.FeeTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._productTypeService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("CanBeGraduated == true", new object[0])
      }, (string) null));
      graduatedFeeForm.ApplicationTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._applicationTypeService.GetAll((string) null));
      graduatedFeeForm.PermitTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._permitTypeService.GetAll((string) null));
      return this.PartialView((object) graduatedFeeForm);
    }

    [HttpPost]
    public virtual JsonResult EditGraduated(GraduatedFeeForm form)
    {
      this._productService.Save(Mapper.Map<GraduatedFeeForm, Product>(form));
      return this.Json((object) new{ Success = true });
    }

    [HttpPost]
    public virtual JsonResult Activate(int id)
    {
      Product byId = this._productService.GetById(id, (string) null);
      if (byId == null)
        return this.Json((object) new
        {
          Success = false,
          Message = ("Product " + (object) id + " does not exist")
        });
      byId.Active = true;
      this._productService.Save(byId);
      return this.Json((object) new{ Success = true });
    }

    [HttpPost]
    public virtual JsonResult Deactivate(int id)
    {
      Product byId = this._productService.GetById(id, (string) null);
      if (byId == null)
        return this.Json((object) new
        {
          Success = false,
          Message = ("Product " + (object) id + " does not exist")
        });
      byId.Active = false;
      this._productService.Save(byId);
      return this.Json((object) new{ Success = true });
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string Data = "Data";
      public readonly string GraduatedData = "GraduatedData";
      public readonly string Create = "Create";
      public readonly string CreateGraduated = "CreateGraduated";
      public readonly string Edit = "Edit";
      public readonly string EditGraduated = "EditGraduated";
      public readonly string Activate = "Activate";
      public readonly string Deactivate = "Deactivate";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string Data = "Data";
      public const string GraduatedData = "GraduatedData";
      public const string Create = "Create";
      public const string CreateGraduated = "CreateGraduated";
      public const string Edit = "Edit";
      public const string EditGraduated = "EditGraduated";
      public const string Activate = "Activate";
      public const string Deactivate = "Deactivate";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Data
    {
      public readonly string command = "command";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GraduatedData
    {
      public readonly string command = "command";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Create
    {
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_CreateGraduated
    {
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Edit
    {
      public readonly string id = "id";
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EditGraduated
    {
      public readonly string id = "id";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Activate
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Deactivate
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly FeeController.ViewsClass._ViewNamesClass s_ViewNames = new FeeController.ViewsClass._ViewNamesClass();
      public readonly string Create = "~/Areas/Ecommerce/Views/Fee/Create.cshtml";
      public readonly string CreateGraduated = "~/Areas/Ecommerce/Views/Fee/CreateGraduated.cshtml";
      public readonly string Edit = "~/Areas/Ecommerce/Views/Fee/Edit.cshtml";
      public readonly string EditGraduated = "~/Areas/Ecommerce/Views/Fee/EditGraduated.cshtml";
      public readonly string Index = "~/Areas/Ecommerce/Views/Fee/Index.cshtml";

      public FeeController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return FeeController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string Create = "Create";
        public readonly string CreateGraduated = "CreateGraduated";
        public readonly string Edit = "Edit";
        public readonly string EditGraduated = "EditGraduated";
        public readonly string Index = "Index";
      }
    }
  }
}
