﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers.InvoiceController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using iTextSharp.text.pdf;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers
{
  [Authorization(AuthorizedRoles = new string[] {"WSIPS eCommerce Member"})]
  public class InvoiceController : Controller
  {
    private static readonly InvoiceController.ActionNamesClass s_actions = new InvoiceController.ActionNamesClass();
    private static readonly InvoiceController.ActionParamsClass_Data s_params_Data = new InvoiceController.ActionParamsClass_Data();
    private static readonly InvoiceController.ActionParamsClass_Edit s_params_Edit = new InvoiceController.ActionParamsClass_Edit();
    private static readonly InvoiceController.ActionParamsClass_Delete s_params_Delete = new InvoiceController.ActionParamsClass_Delete();
    private static readonly InvoiceController.ActionParamsClass_Print s_params_Print = new InvoiceController.ActionParamsClass_Print();
    private static readonly InvoiceController.ViewsClass s_views = new InvoiceController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Area = "Ecommerce";
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Name = "Invoice";
    [GeneratedCode("T4MVC", "2.0")]
    public const string NameConst = "Invoice";
    private readonly IUpdatableService<ContactSale> _invoiceService;
    private readonly IService<ContactSaleGridModel> _invoiceGridModelService;
    private readonly IService<LU_ContactSaleStatus> _invoiceStatusService;
    private readonly IService<Contact> _contactService;
    private readonly IService<Penalty> _penaltyService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public InvoiceController Actions
    {
      get
      {
        return MVC.Ecommerce.Invoice;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public InvoiceController.ActionNamesClass ActionNames
    {
      get
      {
        return InvoiceController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public InvoiceController.ActionParamsClass_Data DataParams
    {
      get
      {
        return InvoiceController.s_params_Data;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public InvoiceController.ActionParamsClass_Edit EditParams
    {
      get
      {
        return InvoiceController.s_params_Edit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public InvoiceController.ActionParamsClass_Delete DeleteParams
    {
      get
      {
        return InvoiceController.s_params_Delete;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public InvoiceController.ActionParamsClass_Print PrintParams
    {
      get
      {
        return InvoiceController.s_params_Print;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public InvoiceController.ViewsClass Views
    {
      get
      {
        return InvoiceController.s_views;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected InvoiceController(Dummy d)
    {
    }

    public InvoiceController(IUpdatableService<ContactSale> invoiceService, IService<ContactSaleGridModel> invoiceGridModelService, IService<LU_ContactSaleStatus> invoiceStatusService, IService<Contact> contactService, IService<Penalty> penaltyService)
    {
      this._invoiceService = invoiceService;
      this._invoiceGridModelService = invoiceGridModelService;
      this._invoiceStatusService = invoiceStatusService;
      this._contactService = contactService;
      this._penaltyService = penaltyService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult Edit()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult Delete()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual FileResult Print()
    {
      return (FileResult) new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.Print, (string) null);
    }

    [HttpGet]
    public virtual ActionResult Index()
    {
      return (ActionResult) this.View();
    }

    [HttpGet]
    public virtual JsonResult Data(GridCommand command, string permitNumber)
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (!string.IsNullOrEmpty(permitNumber))
        dynamicFilterList.Add(new DynamicFilter("PermitName.Contains(@0)", new object[1]
        {
          (object) permitNumber
        }));
      Dictionary<string, string> dictionary = new Dictionary<string, string>()
      {
        {
          "invoicenumber",
          "Id"
        },
        {
          "permitnumber",
          "PermitName"
        },
        {
          "description",
          "Product.Description"
        },
        {
          "invoiceamount",
          "Amount"
        },
        {
          "invoicedate",
          "CreatedDate"
        },
        {
          "status",
          "LU_ContactSaleStatus.Description"
        },
        {
          "paiddate",
          "PaidDate"
        }
      };
      int num1 = this._invoiceGridModelService.Count((IEnumerable<DynamicFilter>) dynamicFilterList);
      if (num1 == 0)
        return this.Json((object) new GridData()
        {
          Data = (IEnumerable) null,
          PageTotal = 0,
          CurrentPage = 0,
          RecordCount = 0
        }, JsonRequestBehavior.AllowGet);
      IEnumerable<ContactSaleGridModel> range = this._invoiceGridModelService.GetRange(command.Skip(), command.PageSize, command.BuildSort((IDictionary<string, string>) dictionary) ?? "CreatedDate desc", (IEnumerable<DynamicFilter>) dynamicFilterList, (string) null);
      foreach (ContactSaleGridModel contactSaleGridModel in range)
      {
        IService<Penalty> penaltyService = this._penaltyService;
        int skip = 0;
        int maxValue = int.MaxValue;
        DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]
        {
          new DynamicFilter("ContactSaleId == @0", new object[1]
          {
            (object) contactSaleGridModel.Id
          })
        };
        contactSaleGridModel.HasRelatedPenalty = penaltyService.GetRange(skip, maxValue, null, (IEnumerable<DynamicFilter>) dynamicFilterArray, null).Any<Penalty>();
      }
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) range,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual PartialViewResult Edit(int id)
    {
      InvoiceForm invoiceForm = Mapper.Map<ContactSale, InvoiceForm>(this._invoiceService.GetById(id, (string) null));
      invoiceForm.InvoiceStatusList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._invoiceStatusService.GetAll((string) null));
      return this.PartialView(MVC.Ecommerce.Invoice.Views.EditInvoice, (object) invoiceForm);
    }

    [HttpPost]
    public virtual ActionResult Edit(InvoiceForm form)
    {
      ContactSale entity = Mapper.Map<InvoiceForm, ContactSale>(form, this._invoiceService.GetById(form.Id, (string) null));
      if (entity.ContactSaleStatusId == 2)
      {
        entity.IsOnlinePayment = new bool?(false);
        entity.PaidDate = new DateTime?(DateTime.Today);
      }
      else
      {
        entity.IsOnlinePayment = new bool?();
        entity.PaidDate = new DateTime?();
      }
      this._invoiceService.Save(entity);
      return (ActionResult) this.Json((object) new
      {
        Success = true
      });
    }

    [HttpDelete]
    public virtual JsonResult Delete(int id)
    {
      if (this._penaltyService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("ContactSaleId == @0", new object[1]
        {
          (object) id
        })
      }, (string) null).Any<Penalty>())
        return this.Json((object) new
        {
          Success = false,
          Message = "Cannot delete this invoice because there are related penalties."
        });
      this._invoiceService.Delete(id);
      return this.Json((object) new{ Success = true });
    }

    [HttpGet]
    public virtual FileResult Print(int id)
    {
      ContactSale byId1 = this._invoiceService.GetById(id, "Product.LU_ProductType");
      if (byId1 == null)
        return (FileResult) null;
      Contact byId2 = this._contactService.GetById(byId1.ContactId, "LU_State");
      if (byId2 == null)
        return (FileResult) null;
      string path = this.Server.MapPath(Path.Combine(ConfigurationManager.AppSettings["DocumentTemplateFolderPath"], "Invoice.pdf"));
      MemoryStream memoryStream = new MemoryStream();
      using (FileStream fileStream = new FileStream(path, FileMode.Open))
      {
        PdfReader reader = new PdfReader((Stream) fileStream);
        PdfStamper pdfStamper = new PdfStamper(reader, (Stream) memoryStream);
        AcroFields acroFields = pdfStamper.AcroFields;
        foreach (string key in (IEnumerable<string>) acroFields.Fields.Keys)
        {
          if (key.Contains("{INVOICE_NUMBER}"))
            acroFields.SetField(key, byId1.Id.ToString());
          else if (key.Contains("{INVOICE_DATE}"))
            acroFields.SetField(key, byId1.CreatedDate.ToShortDateString());
          else if (key.Contains("{DUE_DATE}"))
            acroFields.SetField(key, byId1.CreatedDate.AddDays(30.0).ToShortDateString());
          else if (key.Contains("{INVOICE_AMOUNT}"))
            acroFields.SetField(key, byId1.Amount.ToString("C"));
          else if (key.Contains("{INVOICE_DESCRIPTION}"))
            acroFields.SetField(key, (byId1.Product.Description ?? "").ToUpper());
          else if (key.Contains("{CONTACT_ID}"))
            acroFields.SetField(key, byId1.ContactId.ToString());
          else if (key.Contains("{PERMIT_NUMBER}"))
            acroFields.SetField(key, byId1.PermitName);
          else if (key.Contains("{MDE_CONTACT}"))
            acroFields.SetField(key, "Water Supply Program, 410-537-3714".ToUpper());
          else if (key.Contains("{PCA_NUM}"))
            acroFields.SetField(key, byId1.Product.LU_ProductType.PcaCode.ToString());
          else if (key.Contains("{AGY_OBJ}"))
            acroFields.SetField(key, byId1.Product.LU_ProductType.AobjCode.ToString());
          else if (key.Contains("{COMPANY_NAME}"))
            acroFields.SetField(key, (byId2.BusinessName ?? "").ToUpper());
          else if (key.Contains("{CONTACT_NAME}"))
            acroFields.SetField(key, (byId2.FirstName ?? "").ToUpper() + " " + (byId2.LastName ?? "").ToUpper());
          else if (key.Contains("{CONTACT_STREETADDRESS}"))
            acroFields.SetField(key, (byId2.Address1 ?? "").ToUpper());
          else if (key.Contains("{CONTACT_CITYSTATEZIP}"))
            acroFields.SetField(key, (byId2.City ?? "").ToUpper() + ", " + (byId2.LU_State.Key ?? "").ToUpper() + " " + (byId2.ZipCode ?? "").ToUpper());
        }
        pdfStamper.FormFlattening = true;
        pdfStamper.Close();
        reader.Close();
      }
      this.Response.AppendHeader("Content-Disposition", new ContentDisposition()
      {
        FileName = ("Invoice " + (object) byId1.Id + ".pdf"),
        Inline = true
      }.ToString());
      return (FileResult) this.File(memoryStream.ToArray(), "application/pdf");
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string Data = "Data";
      public readonly string Edit = "Edit";
      public readonly string Delete = "Delete";
      public readonly string Print = "Print";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string Data = "Data";
      public const string Edit = "Edit";
      public const string Delete = "Delete";
      public const string Print = "Print";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Data
    {
      public readonly string command = "command";
      public readonly string permitNumber = "permitNumber";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Edit
    {
      public readonly string id = "id";
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Delete
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Print
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly InvoiceController.ViewsClass._ViewNamesClass s_ViewNames = new InvoiceController.ViewsClass._ViewNamesClass();
      public readonly string EditInvoice = "~/Areas/Ecommerce/Views/Invoice/EditInvoice.cshtml";
      public readonly string Index = "~/Areas/Ecommerce/Views/Invoice/Index.cshtml";

      public InvoiceController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return InvoiceController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string EditInvoice = "EditInvoice";
        public readonly string Index = "Index";
      }
    }
  }
}
