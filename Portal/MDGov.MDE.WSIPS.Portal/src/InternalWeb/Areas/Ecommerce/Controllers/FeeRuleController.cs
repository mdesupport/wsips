﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers.FeeRuleController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Controllers
{
  public class FeeRuleController : Controller
  {
    private static readonly FeeRuleController.ActionNamesClass s_actions = new FeeRuleController.ActionNamesClass();
    private static readonly FeeRuleController.ActionParamsClass_Data s_params_Data = new FeeRuleController.ActionParamsClass_Data();
    private static readonly FeeRuleController.ActionParamsClass_Create s_params_Create = new FeeRuleController.ActionParamsClass_Create();
    private static readonly FeeRuleController.ActionParamsClass_Edit s_params_Edit = new FeeRuleController.ActionParamsClass_Edit();
    private static readonly FeeRuleController.ActionParamsClass_Delete s_params_Delete = new FeeRuleController.ActionParamsClass_Delete();
    private static readonly FeeRuleController.ViewsClass s_views = new FeeRuleController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Area = "Ecommerce";
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Name = "FeeRule";
    [GeneratedCode("T4MVC", "2.0")]
    public const string NameConst = "FeeRule";
    private readonly IService<GraduatedProductRuleGridModel> _graduatedProductRuleGridModelService;
    private readonly IUpdatableService<GraduatedProductRule> _graduatedProductRuleService;
    private readonly IService<LU_ProductRuleOperator> _productRuleOperatorService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeRuleController Actions
    {
      get
      {
        return MVC.Ecommerce.FeeRule;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeRuleController.ActionNamesClass ActionNames
    {
      get
      {
        return FeeRuleController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeRuleController.ActionParamsClass_Data DataParams
    {
      get
      {
        return FeeRuleController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeRuleController.ActionParamsClass_Create CreateParams
    {
      get
      {
        return FeeRuleController.s_params_Create;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeRuleController.ActionParamsClass_Edit EditParams
    {
      get
      {
        return FeeRuleController.s_params_Edit;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public FeeRuleController.ActionParamsClass_Delete DeleteParams
    {
      get
      {
        return FeeRuleController.s_params_Delete;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public FeeRuleController.ViewsClass Views
    {
      get
      {
        return FeeRuleController.s_views;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected FeeRuleController(Dummy d)
    {
    }

    public FeeRuleController(IService<GraduatedProductRuleGridModel> graduatedProductRuleGridModelService, IUpdatableService<GraduatedProductRule> graduatedProductRuleService, IService<LU_ProductRuleOperator> productRuleOperatorService)
    {
      this._graduatedProductRuleGridModelService = graduatedProductRuleGridModelService;
      this._graduatedProductRuleService = graduatedProductRuleService;
      this._productRuleOperatorService = productRuleOperatorService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult Edit()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Delete()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
    }

    [HttpGet]
    public virtual JsonResult Data(GridCommand command, int graduatedProductId)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>()
      {
        {
          "rule",
          "ProductRuleOperand"
        },
        {
          "amount",
          "Amount"
        }
      };
      int num1 = this._graduatedProductRuleGridModelService.Count((IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("GraduatedProductId == @0", new object[1]
        {
          (object) graduatedProductId
        })
      });
      if (num1 == 0)
        return this.Json((object) new GridData()
        {
          Data = (IEnumerable) null,
          PageTotal = 0,
          CurrentPage = 0,
          RecordCount = 0
        }, JsonRequestBehavior.AllowGet);
      IEnumerable<GraduatedProductRuleGridModel> range = this._graduatedProductRuleGridModelService.GetRange(command.Skip(), command.PageSize, command.BuildSort((IDictionary<string, string>) dictionary), (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("GraduatedProductId == @0", new object[1]
        {
          (object) graduatedProductId
        })
      }, (string) null);
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) range,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual PartialViewResult Create()
    {
      return this.PartialView((object) new FeeRuleForm()
      {
        OperatorList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._productRuleOperatorService.GetAll((string) null))
      });
    }

    [HttpPost]
    public virtual JsonResult Create(FeeRuleForm form)
    {
      this._graduatedProductRuleService.Save(Mapper.Map<FeeRuleForm, GraduatedProductRule>(form));
      return this.Json((object) new{ Success = true });
    }

    [HttpGet]
    public virtual PartialViewResult Edit(int id)
    {
      FeeRuleForm feeRuleForm = Mapper.Map<GraduatedProductRule, FeeRuleForm>(this._graduatedProductRuleService.GetById(id, (string) null));
      feeRuleForm.OperatorList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._productRuleOperatorService.GetAll((string) null));
      return this.PartialView((object) feeRuleForm);
    }

    [HttpPost]
    public virtual JsonResult Edit(FeeRuleForm form)
    {
      this._graduatedProductRuleService.Save(Mapper.Map<FeeRuleForm, GraduatedProductRule>(form));
      return this.Json((object) new{ Success = true });
    }

    [HttpPost]
    public virtual JsonResult Delete(int id)
    {
      this._graduatedProductRuleService.Delete(id);
      return this.Json((object) new{ Success = true });
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string Data = "Data";
      public readonly string Create = "Create";
      public readonly string Edit = "Edit";
      public readonly string Delete = "Delete";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNameConstants
    {
      public const string Data = "Data";
      public const string Create = "Create";
      public const string Edit = "Edit";
      public const string Delete = "Delete";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string command = "command";
      public readonly string graduatedProductId = "graduatedProductId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Create
    {
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Edit
    {
      public readonly string id = "id";
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Delete
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly FeeRuleController.ViewsClass._ViewNamesClass s_ViewNames = new FeeRuleController.ViewsClass._ViewNamesClass();
      public readonly string Create = "~/Areas/Ecommerce/Views/FeeRule/Create.cshtml";
      public readonly string Edit = "~/Areas/Ecommerce/Views/FeeRule/Edit.cshtml";

      public FeeRuleController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return FeeRuleController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string Create = "Create";
        public readonly string Edit = "Edit";
      }
    }
  }
}
