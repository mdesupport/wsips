﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Helpers.Configuration.EcommerceAreaAutoMapperProfile
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Helpers.Configuration
{
  public class EcommerceAreaAutoMapperProfile : Profile
  {
    public override string ProfileName
    {
      get
      {
        return "Ecommerce";
      }
    }

    protected override void Configure()
    {
      Mapper.CreateMap<Product, FeeForm>().ForMember((Expression<Func<FeeForm, object>>) (dest => (object) dest.FeeTypeId), (Action<IMemberConfigurationExpression<Product>>) (opt => opt.MapFrom<int>((Expression<Func<Product, int>>) (src => src.ProductTypeId)))).ForMember((Expression<Func<FeeForm, object>>) (dest => dest.FeeAmount), (Action<IMemberConfigurationExpression<Product>>) (opt => opt.MapFrom<Decimal?>((Expression<Func<Product, Decimal?>>) (src => src.Amount))));
      Mapper.CreateMap<FeeForm, Product>().ForMember((Expression<Func<Product, object>>) (dest => (object) dest.ProductTypeId), (Action<IMemberConfigurationExpression<FeeForm>>) (opt => opt.MapFrom<int?>((Expression<Func<FeeForm, int?>>) (src => src.FeeTypeId)))).ForMember((Expression<Func<Product, object>>) (dest => (object) dest.Amount), (Action<IMemberConfigurationExpression<FeeForm>>) (opt => opt.MapFrom<string>((Expression<Func<FeeForm, string>>) (src => src.FeeAmount.Replace("$", "").Replace(",", ""))))).ForMember((Expression<Func<Product, object>>) (dest => (object) dest.IsGraduated), (Action<IMemberConfigurationExpression<FeeForm>>) (opt => opt.UseValue<bool>(false)));
      Mapper.CreateMap<Product, GraduatedFeeForm>().ForMember((Expression<Func<GraduatedFeeForm, object>>) (dest => (object) dest.FeeTypeId), (Action<IMemberConfigurationExpression<Product>>) (opt => opt.MapFrom<int>((Expression<Func<Product, int>>) (src => src.ProductTypeId)))).ForMember((Expression<Func<GraduatedFeeForm, object>>) (dest => (object) dest.HasAssociatedRules), (Action<IMemberConfigurationExpression<Product>>) (opt => opt.MapFrom<bool>((Expression<Func<Product, bool>>) (src => src.GraduatedProductRules.Any<GraduatedProductRule>()))));
      Mapper.CreateMap<GraduatedFeeForm, Product>().ForMember((Expression<Func<Product, object>>) (dest => (object) dest.ProductTypeId), (Action<IMemberConfigurationExpression<GraduatedFeeForm>>) (opt => opt.MapFrom<int?>((Expression<Func<GraduatedFeeForm, int?>>) (src => src.FeeTypeId)))).ForMember((Expression<Func<Product, object>>) (dest => (object) dest.IsGraduated), (Action<IMemberConfigurationExpression<GraduatedFeeForm>>) (opt => opt.UseValue<bool>(true)));
      Mapper.CreateMap<GraduatedProductRule, FeeRuleForm>().ForMember((Expression<Func<FeeRuleForm, object>>) (dest => dest.FeeAmount), (Action<IMemberConfigurationExpression<GraduatedProductRule>>) (opt => opt.MapFrom<Decimal>((Expression<Func<GraduatedProductRule, Decimal>>) (src => src.Amount)))).ForMember((Expression<Func<FeeRuleForm, object>>) (dest => (object) dest.OperatorId), (Action<IMemberConfigurationExpression<GraduatedProductRule>>) (opt => opt.MapFrom<int>((Expression<Func<GraduatedProductRule, int>>) (src => src.ProductRuleOperatorId)))).ForMember((Expression<Func<FeeRuleForm, object>>) (dest => (object) dest.Operand), (Action<IMemberConfigurationExpression<GraduatedProductRule>>) (opt => opt.MapFrom<long>((Expression<Func<GraduatedProductRule, long>>) (src => src.ProductRuleOperand)))).ForMember((Expression<Func<FeeRuleForm, object>>) (dest => (object) dest.GraduatedFeeId), (Action<IMemberConfigurationExpression<GraduatedProductRule>>) (opt => opt.MapFrom<int>((Expression<Func<GraduatedProductRule, int>>) (src => src.GraduatedProductId))));
      Mapper.CreateMap<FeeRuleForm, GraduatedProductRule>().ForMember((Expression<Func<GraduatedProductRule, object>>) (dest => (object) dest.Amount), (Action<IMemberConfigurationExpression<FeeRuleForm>>) (opt => opt.MapFrom<string>((Expression<Func<FeeRuleForm, string>>) (src => src.FeeAmount.Replace("$", "").Replace(",", ""))))).ForMember((Expression<Func<GraduatedProductRule, object>>) (dest => (object) dest.ProductRuleOperatorId), (Action<IMemberConfigurationExpression<FeeRuleForm>>) (opt => opt.MapFrom<int?>((Expression<Func<FeeRuleForm, int?>>) (src => src.OperatorId)))).ForMember((Expression<Func<GraduatedProductRule, object>>) (dest => (object) dest.ProductRuleOperand), (Action<IMemberConfigurationExpression<FeeRuleForm>>) (opt => opt.MapFrom<long?>((Expression<Func<FeeRuleForm, long?>>) (src => src.Operand)))).ForMember((Expression<Func<GraduatedProductRule, object>>) (dest => (object) dest.GraduatedProductId), (Action<IMemberConfigurationExpression<FeeRuleForm>>) (opt => opt.MapFrom<int>((Expression<Func<FeeRuleForm, int>>) (src => src.GraduatedFeeId))));
      Mapper.CreateMap<InvoiceForm, ContactSale>().ForMember((Expression<Func<ContactSale, object>>) (dest => (object) dest.Amount), (Action<IMemberConfigurationExpression<InvoiceForm>>) (opt => opt.MapFrom<Decimal>((Expression<Func<InvoiceForm, Decimal>>) (src => Decimal.Parse(src.Amount.Replace("$", "").Replace(",", "")))))).ForMember((Expression<Func<ContactSale, object>>) (dest => (object) dest.ContactSaleStatusId), (Action<IMemberConfigurationExpression<InvoiceForm>>) (opt => opt.MapFrom<int>((Expression<Func<InvoiceForm, int>>) (src => src.SelectedInvoiceStatusId))));
      Mapper.CreateMap<ContactSale, InvoiceForm>().ForMember((Expression<Func<InvoiceForm, object>>) (dest => (object) dest.SelectedInvoiceStatusId), (Action<IMemberConfigurationExpression<ContactSale>>) (opt => opt.MapFrom<int>((Expression<Func<ContactSale, int>>) (src => src.ContactSaleStatusId))));
    }
  }
}
