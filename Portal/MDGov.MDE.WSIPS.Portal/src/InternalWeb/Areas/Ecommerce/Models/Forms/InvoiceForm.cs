﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms.InvoiceForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms
{
  public class InvoiceForm
  {
    public int Id { get; set; }

    [Display(Name = "Amount")]
    [Required]
    public string Amount { get; set; }

    [Required]
    [Display(Name = "Status")]
    public int SelectedInvoiceStatusId { get; set; }

    public SelectList InvoiceStatusList { get; set; }

    [Display(Name = "Check Number")]
    public string CheckNumber { get; set; }

    [DataType(DataType.Date)]
    [Display(Name = "Deposit Date")]
    public DateTime? DepositDate { get; set; }
  }
}
