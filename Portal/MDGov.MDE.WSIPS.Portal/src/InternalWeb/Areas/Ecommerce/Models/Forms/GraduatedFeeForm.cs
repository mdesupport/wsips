﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms.GraduatedFeeForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms
{
  public class GraduatedFeeForm
  {
    public int Id { get; set; }

    [Display(Name = "Fee Type")]
    [Required(ErrorMessage = "Please select a {0}")]
    public int? FeeTypeId { get; set; }

    public SelectList FeeTypeList { get; set; }

    [RequiredIf("FeeTypeId", new object[] {1}, ErrorMessage = "Please select an {0}")]
    [Display(Name = "Application Type")]
    public int? ApplicationTypeId { get; set; }

    public SelectList ApplicationTypeList { get; set; }

    [Display(Name = "Permit Type")]
    [RequiredIf("FeeTypeId", new object[] {2}, ErrorMessage = "Please select a {0}")]
    public int? PermitTypeId { get; set; }

    public SelectList PermitTypeList { get; set; }

    [Required(ErrorMessage = "{0} is required")]
    public string Description { get; set; }

    [Display(Name = "Effective Date")]
    public DateTime? EffectiveDate { get; set; }

    public bool Active { get; set; }

    public bool HasAssociatedRules { get; set; }
  }
}
