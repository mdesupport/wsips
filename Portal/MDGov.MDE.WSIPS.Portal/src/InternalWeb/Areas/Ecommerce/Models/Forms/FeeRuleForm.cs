﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms.FeeRuleForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Models.Forms
{
  public class FeeRuleForm
  {
    public int Id { get; set; }

    public int GraduatedFeeId { get; set; }

    [Display(Name = "Operator")]
    [Required(ErrorMessage = "Please select a {0}")]
    public int? OperatorId { get; set; }

    public SelectList OperatorList { get; set; }

    [Range(0.0, 9.22337203685478E+18, ErrorMessage = "Number out of range")]
    [Required(ErrorMessage = "{0} is required")]
    [Display(Name = "Operand")]
    public long? Operand { get; set; }

    [RegularExpression("^\\$?(((\\d+)|([1-9]{1}[0-9]{0,2}(,\\d{3})*))|(((\\d+)|([1-9]{1}[0-9]{0,2}(,\\d{3})*))(\\.\\d{2}))|(\\.\\d{2}))$", ErrorMessage = "Invalid Amount")]
    [Required(ErrorMessage = "{0} is required")]
    [Display(Name = "Amount")]
    public string FeeAmount { get; set; }
  }
}
