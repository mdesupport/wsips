﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.PermittingAreaRegistration
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting
{
  public class PermittingAreaRegistration : AreaRegistration
  {
    public override string AreaName
    {
      get
      {
        return "Permitting";
      }
    }

    public override void RegisterArea(AreaRegistrationContext context)
    {
      context.MapRoute("Permitting_Files", "Permitting/{controller}/{action}/{id}/{filename}");
      context.MapRoute("Permitting_details", "Permitting/Permit", (object) new
      {
        controller = "Permit",
        action = "Index"
      });
      context.MapRoute("Permitting_default", "Permitting/{controller}/{action}/{id}", (object) new
      {
        action = "Index",
        id = UrlParameter.Optional
      });
    }
  }
}
