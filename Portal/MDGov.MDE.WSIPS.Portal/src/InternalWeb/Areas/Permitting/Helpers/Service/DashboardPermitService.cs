﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.DashboardPermitService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System.Net;
using System.Net.Http;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service
{
  public class DashboardPermitService : HttpClientBase<DashboardPermit>, IDashboardPermitService
  {
    public DashboardSearchResult GetRange(int skip, int take, string orderBy, DashboardFilterWrapper filterWrapper, string includeProperties = null)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync<DashboardFilterWrapper>(string.Format("GetRange?skip={0}&take={1}&orderby={2}", (object) skip, (object) take, (object) orderBy) + string.Format("&includeProperties={0}", (object) includeProperties), filterWrapper).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<DashboardSearchResult>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (DashboardSearchResult) null;
    }

    public DashboardPermit GetById(int id)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetById?Id={0}", (object) id)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<DashboardPermit>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (DashboardPermit) null;
    }

    private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      if (statusCode == HttpStatusCode.NotFound)
      {
        exception.ExceptionType = "NotFound";
        exception.ExceptionMessage = exception.Message;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }
  }
}
