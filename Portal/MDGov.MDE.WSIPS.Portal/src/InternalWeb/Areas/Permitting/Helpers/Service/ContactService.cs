﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.ContactService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service;
using StructureMap;
using System.Net;
using System.Net.Http;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service
{
  public class ContactService : UpdatableService<Contact>, IContactService, IUpdatableService<Contact>, IService<Contact>
  {
    private static void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      if (statusCode == HttpStatusCode.NotFound)
      {
        exception.ExceptionType = "NotFound";
        exception.ExceptionMessage = exception.Message;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }

    public int MergeContacts(int fromContactId, int toContactId)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("MergeContacts?fromContactId={0}&toContactId={1}", (object) fromContactId, (object) toContactId)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      ContactService.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }
  }
}
