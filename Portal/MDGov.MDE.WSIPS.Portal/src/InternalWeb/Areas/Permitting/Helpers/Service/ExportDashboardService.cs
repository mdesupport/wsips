﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.ExportDashboardService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using StructureMap;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.DynamicData;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service
{
  public class ExportDashboardService : HttpClientBase<ExportDashboardPermit>, IExportDashboardService
  {
    public IEnumerable<ExportDashboardPermit> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetRange?skip={0}&take={1}&orderby={2}", (object) includeProperties)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<ExportDashboardPermit>>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (IEnumerable<ExportDashboardPermit>) null;
    }

    public int GetReportParameterId(IEnumerable<int> PermitIds)
    {
      HttpResponseMessage result = this.Client.PostAsJsonAsync<IEnumerable<int>>(string.Format("GetReportParameterId"), PermitIds).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      if (statusCode == HttpStatusCode.NotFound)
      {
        exception.ExceptionType = "NotFound";
        exception.ExceptionMessage = exception.Message;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }
  }
}
