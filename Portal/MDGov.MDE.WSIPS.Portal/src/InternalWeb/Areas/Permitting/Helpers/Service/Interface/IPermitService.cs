﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface.IPermitService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface
{
  public interface IPermitService : IUpdatableService<Permit>, IService<Permit>
  {
    int ClonePermit(int PermitId, int PermitTypeId, string PermitNumber, string userName);

    string GeneratePermitNumber(int permitId);

    bool IsUniquePermitNumber(string PermitNumber);

    int CalculateStatus(int permitId);

    bool ChangeStatus(int permitId);

    StatusChange GetLastStatusChange(int permitId, int permitStatusId);

    bool CancelPermitApplication(int permitId);
  }
}
