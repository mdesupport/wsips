﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.IDashboardPermitService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service
{
  public interface IDashboardPermitService
  {
    DashboardSearchResult GetRange(int skip, int take, string orderBy, DashboardFilterWrapper filterWrapper, string includeProperties = null);

    DashboardPermit GetById(int id);
  }
}
