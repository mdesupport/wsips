﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.PermitLocationService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System.Net;
using System.Net.Http;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service
{
  public class PermitLocationService : HttpClientBase<PermitLocation>, IPermitLocationService
  {
    public int Save(PermitLocation entity)
    {
      entity.LastModifiedBy = HttpContext.Current.User.Identity.Name;
      HttpResponseMessage result = this.Client.PostAsXmlAsync<PermitLocation>("Save", entity).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    public void Delete(int id)
    {
      HttpResponseMessage result = this.Client.DeleteAsync(string.Format("Delete?Id={0}", (object) id)).Result;
      if (result.IsSuccessStatusCode)
        return;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
    }

    private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      if (statusCode == HttpStatusCode.NotFound)
      {
        exception.ExceptionType = "NotFound";
        exception.ExceptionMessage = exception.Message;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }
  }
}
