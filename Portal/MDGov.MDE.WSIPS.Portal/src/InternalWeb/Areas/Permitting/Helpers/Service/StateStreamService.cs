﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.StateStreamService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service
{
  public class StateStreamService : ServiceBase<LU_StateStream>, IStateStreamService, IService<LU_StateStream>
  {
    public IEnumerable<LU_StateStream> GetByWatershed(double x, double y, int srid)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetByWatershed?x={0}&y={1}&srid={2}", (object) x, (object) y, (object) srid)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<IEnumerable<LU_StateStream>>().Result;
      throw new Exception(result.Content.ReadAsAsync<ApiException>().Result.Message);
    }
  }
}
