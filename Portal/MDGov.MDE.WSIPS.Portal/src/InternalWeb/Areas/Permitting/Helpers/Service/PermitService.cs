﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.PermitService
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service;
using StructureMap;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service
{
  public class PermitService : UpdatableService<Permit>, IPermitService, IUpdatableService<Permit>, IService<Permit>
  {
    public int ClonePermit(int PermitId, int PermitTypeId, string PermitNumber, string userName)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("ClonePermit?PermitId={0}&PermitTypeId={1}&PermitNumber={2}&userName={3}", (object) PermitId, (object) PermitTypeId, (object) PermitNumber, (object) userName)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    public string GeneratePermitNumber(int permitId)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GeneratePermitNumber?PermitId={0}", (object) permitId)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<string>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return string.Empty;
    }

    public bool IsUniquePermitNumber(string PermitNumber)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("IsUniquePermitNumber?PermitNumber={0}", (object) PermitNumber)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<bool>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    public int CalculateStatus(int permitId)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("CalculateStatus?permitId={0}", (object) permitId)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<int>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return 0;
    }

    public bool ChangeStatus(int permitId)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("ChangeStatus?permitId={0}", (object) permitId)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<bool>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    public StatusChange GetLastStatusChange(int permitId, int permitStatusId)
    {
      HttpResponseMessage result = this.Client.GetAsync(string.Format("GetLastStatusChange?permitId={0}&permitStatusId={1}", (object) permitId, (object) permitStatusId)).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<StatusChange>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return (StatusChange) null;
    }

    public bool CancelPermitApplication(int permitId)
    {
      HttpResponseMessage result = this.Client.PostAsync<StringContent>(string.Format("CancelPermitApplication?permitId={0}", (object) permitId), new StringContent(string.Empty), (MediaTypeFormatter) GlobalConfiguration.Configuration.Formatters.JsonFormatter).Result;
      if (result.IsSuccessStatusCode)
        return result.Content.ReadAsAsync<bool>().Result;
      this.HandleApiException(result.Content.ReadAsAsync<ApiException>().Result, result.StatusCode);
      return false;
    }

    private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
    {
      if (statusCode == HttpStatusCode.NotFound)
      {
        exception.ExceptionType = "NotFound";
        exception.ExceptionMessage = exception.Message;
      }
      ObjectFactory.GetInstance<IErrorLogging>().Log(exception);
    }
  }
}
