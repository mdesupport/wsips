﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.MapResolvers.ComplianceConditionDueDateResolver
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Permitting.Model;
using System;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.MapResolvers
{
  public class ComplianceConditionDueDateResolver : ValueResolver<PermitCondition, DateTime?>
  {
    protected override DateTime? ResolveCore(PermitCondition source)
    {
      ConditionCompliance conditionCompliance = source.ConditionCompliances.OrderBy<ConditionCompliance, DateTime>((Func<ConditionCompliance, DateTime>) (cc => cc.ComplianceReportingDueDate)).FirstOrDefault<ConditionCompliance>((Func<ConditionCompliance, bool>) (c => c.PermitConditionComplianceStatusId == 4));
      if (conditionCompliance != null)
        return new DateTime?(conditionCompliance.ComplianceReportingDueDate);
      return new DateTime?();
    }
  }
}
