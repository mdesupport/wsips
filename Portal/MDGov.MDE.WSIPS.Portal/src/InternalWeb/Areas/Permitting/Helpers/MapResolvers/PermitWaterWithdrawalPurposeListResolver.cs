﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.MapResolvers.PermitWaterWithdrawalPurposeListResolver
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using System.Collections.Generic;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.MapResolvers
{
  public class PermitWaterWithdrawalPurposeListResolver : ValueResolver<PermitWaterWithdrawalLocation, IDictionary<PermitWaterWithdrawalPurposeForm, bool>>
  {
    private readonly IService<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeService;

    public PermitWaterWithdrawalPurposeListResolver(IService<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeService)
    {
      this._permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
    }

    protected override IDictionary<PermitWaterWithdrawalPurposeForm, bool> ResolveCore(PermitWaterWithdrawalLocation source)
    {
      IDictionary<PermitWaterWithdrawalPurposeForm, bool> dictionary = (IDictionary<PermitWaterWithdrawalPurposeForm, bool>) new Dictionary<PermitWaterWithdrawalPurposeForm, bool>();
      foreach (PermitWaterWithdrawalPurpose source1 in this._permitWaterWithdrawalPurposeService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId=" + (object) source.PermitId, new object[0])
      }, "LU_WaterWithdrawalPurpose"))
      {
        bool flag = source.SelectedPermitWaterWithdrawalPurposes != null && source.SelectedPermitWaterWithdrawalPurposes.Contains<int>(source1.Id);
        dictionary.Add(Mapper.Map<PermitWaterWithdrawalPurpose, PermitWaterWithdrawalPurposeForm>(source1), flag);
      }
      return dictionary;
    }
  }
}
