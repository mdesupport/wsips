﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.MapResolvers.ComplianceConditionCanBeDeletedResolver
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.MapResolvers
{
  public class ComplianceConditionCanBeDeletedResolver : ValueResolver<PermitCondition, bool>
  {
    private readonly IService<PumpageReport> _pumpageReportService;
    private readonly IService<Document> _documentService;

    public ComplianceConditionCanBeDeletedResolver(IService<PumpageReport> pumpageReportService, IService<Document> documentService)
    {
      this._pumpageReportService = pumpageReportService;
      this._documentService = documentService;
    }

    protected override bool ResolveCore(PermitCondition source)
    {
      int? standardConditionTypeId = source.StandardConditionTypeId;
      if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0)
      {
        foreach (ConditionCompliance conditionCompliance in (IEnumerable<ConditionCompliance>) source.ConditionCompliances)
        {
          if (this._pumpageReportService.Count((IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("ConditionComplianceId == @0", new object[1]
            {
              (object) conditionCompliance.Id
            })
          }) > 0)
            return false;
        }
      }
      else
      {
        foreach (ConditionCompliance conditionCompliance in (IEnumerable<ConditionCompliance>) source.ConditionCompliances)
        {
          if (this._documentService.Count((IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("RefId == @0 && RefTableId == @1", new object[2]
            {
              (object) conditionCompliance.Id,
              (object) 69
            })
          }) > 0)
            return false;
        }
      }
      return true;
    }
  }
}
