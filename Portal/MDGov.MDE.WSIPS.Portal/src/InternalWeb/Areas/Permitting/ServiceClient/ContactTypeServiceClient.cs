﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient.ContactTypeServiceClient
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient
{
  public class ContactTypeServiceClient : IContactTypeServiceClient
  {
    private readonly IPermitService _permittingService;
    private readonly IUpdatableService<PermitContact> _permitContactService;
    private readonly IService<DivisionChief> _divisionChiefService;
    private readonly IService<Supervisor> _supervisorService;
    private readonly IService<ProjectManager> _projectManagerService;
    private readonly IService<ComplianceManager> _complianceManagerService;
    private readonly IService<Contact> _contactService;
    private readonly IService<LU_ContactType> _contactTypeService;
    private readonly IUpdatableService<Message> _messageService;

    public ContactTypeServiceClient(IPermitService permittingService, IUpdatableService<PermitContact> permitContactService, IService<DivisionChief> divisionChiefService, IService<Supervisor> supervisorService, IService<ProjectManager> projectManagerService, IService<ComplianceManager> complianceManagerService, IService<Contact> contactService, IService<LU_ContactType> contactTypeService, IUpdatableService<Message> messageService)
    {
      this._permittingService = permittingService;
      this._permitContactService = permitContactService;
      this._divisionChiefService = divisionChiefService;
      this._supervisorService = supervisorService;
      this._projectManagerService = projectManagerService;
      this._complianceManagerService = complianceManagerService;
      this._contactService = contactService;
      this._contactTypeService = contactTypeService;
      this._messageService = messageService;
    }

    public int SaveContactType(int permitId, int contactTypeId, int contactId, string permitDetailsUrl)
    {
      LU_ContactType byId = this._contactTypeService.GetById(contactTypeId, (string) null);
      if (byId == null)
        throw new ArgumentException("Contact Type Id " + (object) contactTypeId + " does not exist");
      PermitContact permitContact = this._permitContactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0 && ContactTypeId == @1 && IsPrimary", new object[2]{ (object) permitId, (object) contactTypeId }) }, "PermitContactInformation").SingleOrDefault<PermitContact>();
      if (permitContact != null)
      {
        if (permitContact.ContactId == contactId)
          return contactId;
        this._permitContactService.Delete(permitContact.Id);
      }
      if (contactId <= 0)
        return 0;
      int num = this._permitContactService.Save(new PermitContact() { PermitId = permitId, ContactId = contactId, ContactTypeId = new int?(contactTypeId), IsApplicant = false, IsPrimary = true, Active = true });
      this.SendMessage(permitDetailsUrl, contactId, byId, permitId);
      return num;
    }

    private void SendMessage(string permitDetailsUrl, int contactId, LU_ContactType contactType, int permitId)
    {
      if (contactType == null)
        throw new ArgumentNullException("contactType", "Value cannot be null");
      Message entity = new Message();
      if (contactType.Id == 9 || contactType.Id == 10)
      {
        entity.NotificationText = "A water appropriation permit is under review by the MD Department of the Environment that requires your review.";
        entity.Subject = "A water appropriation permit is under review by the MD Department of the Environment that requires your review";
        entity.Body = "A water appropriation permit is under review by the MD Department of the Environment that requires your review. Please click on the link below to review the permit and provide your comments.<p>" + permitDetailsUrl + "</p>";
      }
      else if (contactType.Id == 35)
      {
        entity.NotificationText = "A permit exemption has been submitted that requires your attention.";
        entity.Subject = "A permit exemption has been submitted that requires your attention.";
        entity.Body = "A permit exemption has been submitted that requires your attention.<p>" + permitDetailsUrl + "</p>";
      }
      else
      {
        entity.NotificationText = "You have been assigned as the " + contactType.Description + " for the following permit";
        entity.Subject = "You have been assigned as the " + contactType.Description + " for the following permit";
        entity.Body = "You have been assigned as the " + contactType.Description + " for the following permit.<p>" + permitDetailsUrl + "</p>";
      }
      entity.ContactIds = new List<int>() { contactId };
      entity.NotificationTypeId = 1;
      entity.PermitId = permitId;
      entity.StartDate = new DateTime?(DateTime.MinValue);
      entity.EndDate = new DateTime?(DateTime.MaxValue);
      entity.From = ConfigurationManager.AppSettings["SenderEmail"];
      this._messageService.Save(entity);
    }

    public IEnumerable<DivisionChief> GetAllDivisionChief()
    {
      return this._divisionChiefService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active == True", new object[0]) }, "Contact");
    }

    public IEnumerable<Supervisor> GetAllSupervisor()
    {
      return this._supervisorService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active == True", new object[0]) }, "Contact");
    }

    public IEnumerable<ProjectManager> GetAllProjectManager()
    {
      return this._projectManagerService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active == True", new object[0]) }, "Contact");
    }

    public IEnumerable<ComplianceManager> GetAllComplianceManager()
    {
      return this._complianceManagerService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active == True", new object[0]) }, "Contact");
    }

    public IEnumerable<Contact> GetAllCountyOfficial()
    {
      return this._contactService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("Active == True", new object[0]), new DynamicFilter("ContactTypeId==@0", new object[1]{ (object) 9 }) }, (string) null);
    }

    public IEnumerable<Contact> GetAllDepartmentOfNaturalResource()
    {
      return this._contactService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("Active == True", new object[0]), new DynamicFilter("ContactTypeId==@0", new object[1]{ (object) 10 }) }, (string) null);
    }
  }
}
