﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient.IPermitServiceClient
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient
{
  public interface IPermitServiceClient
  {
    ApplicationWizard InitializeApplicationWizardFrom();

    ApplicationWizard GetApplicationWizardForExistingPermit(int permitId);

    int SaveApplicantInformation(ApplicationWizard form);

    IEnumerable<PermitApplication> GetApplicationsInProgress();

    List<ContactSearchModel> BuildResult(IEnumerable<Contact> result);

    IEnumerable<DynamicFilter> BuildFilter(ContactSearchForm searchForm, IEnumerable<int> contactIds);

    List<ContactForm> BuildContactForm(IEnumerable<Contact> contacts);

    IEnumerable<LU_PermitStatus> GetWorkFlowSteps(int permitId);

    void BuildAndSaveConditionCompliance(int permitId, int pendingStatusId, IEnumerable<PermitCondition> permitConditions);

    int ClonePermit(PendingPermitForm form, string userName);
  }
}
