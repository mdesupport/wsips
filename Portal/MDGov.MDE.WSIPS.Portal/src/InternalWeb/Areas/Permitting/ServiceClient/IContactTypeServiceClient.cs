﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient.IContactTypeServiceClient
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient
{
  public interface IContactTypeServiceClient
  {
    int SaveContactType(int permitid, int contactTypeId, int contactId, string permitDetailsUrl);

    IEnumerable<DivisionChief> GetAllDivisionChief();

    IEnumerable<Supervisor> GetAllSupervisor();

    IEnumerable<ProjectManager> GetAllProjectManager();

    IEnumerable<ComplianceManager> GetAllComplianceManager();

    IEnumerable<Contact> GetAllCountyOfficial();

    IEnumerable<Contact> GetAllDepartmentOfNaturalResource();
  }
}
