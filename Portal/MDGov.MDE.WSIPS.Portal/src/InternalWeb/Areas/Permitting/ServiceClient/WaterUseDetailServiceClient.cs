﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient.WaterUseDetailServiceClient
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient
{
  public class WaterUseDetailServiceClient : IWaterUseDetailServiceClient
  {
    private readonly IUpdatableService<PermitWwLivestock> _permitWaterWithdrawalLivestockService;
    private readonly IUpdatableService<PermitWwAgricultural> _permitWwAgriculturalService;
    private readonly IUpdatableService<PermitWwNonAgricultureIrrigation> _permitWwNonAgricultureIrrigationService;
    private readonly IUpdatableService<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeService;
    private readonly IUpdatableService<PermitWwPoultry> _permitWwPoultryService;
    private readonly IUpdatableService<PermitWwCrop> _permitWwCropService;
    private readonly IUpdatableService<PermitWwPrivateWaterSupplier> _permitWwPrivateWaterSupplier;
    private readonly IUpdatableService<PermitWastewaterTreatmentDisposal> _permitWastewaterTreatmentDisposalService;
    private readonly IService<PermitWwPrivateWaterSupplier> _permitWaterWithdrawalPrivateWaterSupplierService;
    private readonly IUpdatableService<PermitWwFarmPortableSupply> _permitWwFarmPotableWaterSupplyService;
    private readonly IUpdatableService<PermitWwFoodProcessing> _permitWwFoodProcessing;
    private readonly IUpdatableService<PermitWwAquacultureOrAquarium> _permitWwAquacultureOrAquariumService;
    private readonly IUpdatableService<PermitWwCommercialDrinkingOrSanitary> _permitWwCommertialDrinkingOrSanitaryService;
    private readonly IUpdatableService<PermitWwInstitutionalEducational_drinkingOrsanitary> _permitWwEducationalDrinkingOrSanitaryService;
    private readonly IUpdatableService<PermitWwInstitutionalReligious_DringkingOrSanitary> _permitWwReligiousDrinkingOrSanitaryService;
    private readonly IUpdatableService<PermitWwWildlifePond> _permitWwWildlifePondService;
    private readonly IUpdatableService<PermitWwConstructionDewatering> _permitWwConstructionDewateringService;
    private readonly IUpdatableService<PermitWwGolfCourseIrrigation> _permitWwGolfCourseIrrigationService;

    public List<PermitWaterWithdrawalPurpose> SavedWaterWithdrawalPurpose { get; set; }

    public WaterUseDetailServiceClient(IUpdatableService<PermitWwLivestock> permitWaterWithdrawalLivestockService, IUpdatableService<PermitWwAgricultural> permitWwAgriculturalService, IUpdatableService<PermitWwNonAgricultureIrrigation> permitWwNonAgricultureIrrigationService, IUpdatableService<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeService, IUpdatableService<PermitWwPoultry> permitWwPoultryService, IUpdatableService<PermitWwPrivateWaterSupplier> permitWwPrivateWaterSupplier, IUpdatableService<PermitWwCrop> permitWwCropService, IUpdatableService<PermitWastewaterTreatmentDisposal> permitWastewaterTreatmentDisposalService, IService<PermitWwPrivateWaterSupplier> permitWaterWithdrawalPrivateWaterSupplierService, IUpdatableService<PermitWwFarmPortableSupply> permitWwFarmPotableWaterSupplyService, IUpdatableService<PermitWwFoodProcessing> permitWwFoodProcessing, IUpdatableService<PermitWwAquacultureOrAquarium> permitWwAquacultureOrAquariumService, IUpdatableService<PermitWwCommercialDrinkingOrSanitary> permitWwCommertialDrinkingOrSanitaryService, IUpdatableService<PermitWwInstitutionalEducational_drinkingOrsanitary> permitWwEducationalDrinkingOrSanitaryService, IUpdatableService<PermitWwInstitutionalReligious_DringkingOrSanitary> permitWwReligiousDrinkingOrSanitaryService, IUpdatableService<PermitWwWildlifePond> permitWwWildlifePondService, IUpdatableService<PermitWwConstructionDewatering> permitWwConstructionDewateringService, IUpdatableService<PermitWwGolfCourseIrrigation> permitWwGolfCourseIrrigationService)
    {
      this._permitWaterWithdrawalLivestockService = permitWaterWithdrawalLivestockService;
      this._permitWwAgriculturalService = permitWwAgriculturalService;
      this._permitWwNonAgricultureIrrigationService = permitWwNonAgricultureIrrigationService;
      this._permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
      this._permitWwPoultryService = permitWwPoultryService;
      this._permitWwCropService = permitWwCropService;
      this._permitWwPrivateWaterSupplier = permitWwPrivateWaterSupplier;
      this._permitWastewaterTreatmentDisposalService = permitWastewaterTreatmentDisposalService;
      this._permitWaterWithdrawalPrivateWaterSupplierService = permitWaterWithdrawalPrivateWaterSupplierService;
      this._permitWwFarmPotableWaterSupplyService = permitWwFarmPotableWaterSupplyService;
      this._permitWwFoodProcessing = permitWwFoodProcessing;
      this._permitWwAquacultureOrAquariumService = permitWwAquacultureOrAquariumService;
      this._permitWwCommertialDrinkingOrSanitaryService = permitWwCommertialDrinkingOrSanitaryService;
      this._permitWwEducationalDrinkingOrSanitaryService = permitWwEducationalDrinkingOrSanitaryService;
      this._permitWwReligiousDrinkingOrSanitaryService = permitWwReligiousDrinkingOrSanitaryService;
      this._permitWwWildlifePondService = permitWwWildlifePondService;
      this._permitWwConstructionDewateringService = permitWwConstructionDewateringService;
      this._permitWwGolfCourseIrrigationService = permitWwGolfCourseIrrigationService;
    }

    public void SaveWaterUseDetails(ApplicationWizard form)
    {
      if (form.WaterUseDetailForm == null)
        return;
      this.SavePermitLivestockWatering(form);
      this.SaveOtherLivestockWatering(form);
      this.SaveDairyAnimalWatering(form);
      this.SaveFarmPotableSupplies(form);
      this.SavePoultryWatering(form);
      this.SaveCrops(form);
      this.SaveFoodProcessing(form);
      this.SaveAquacultureAndAquarium(form);
      this.SavePermitWwIrrigation(form);
      this.SavePrivateWaterSupplierVariants(form);
      this.SavePrivateWaterSuppiler(form);
    }

    public List<WastewaterTreatmentAndDisposalForm> GetWasteWaterTreatmentAndDisposal(string waterUseCategoryTypeids, int permitId)
    {
      List<WastewaterTreatmentAndDisposalForm> treatmentAndDisposalFormList = new List<WastewaterTreatmentAndDisposalForm>();
      List<string> list1 = ((IEnumerable<string>) waterUseCategoryTypeids.Split(',')).ToList<string>();
      list1.RemoveAll((Predicate<string>) (x => x == ""));
      IEnumerable<PermitWaterWithdrawalPurpose> range1 = this._permitWaterWithdrawalPurposeService.GetRange(0, 9999, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("@0.Contains(outerIt.WaterWithdrawalPurposeId) and PermitId=@1", new object[2]{ (object) list1, (object) permitId }) }, "LU_WaterWithdrawalPurpose");
      IEnumerable<PermitWastewaterTreatmentDisposal> range2 = this._permitWastewaterTreatmentDisposalService.GetRange(0, 9999, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("@0.Contains(outerIt.PermitWaterWithdrawalPurposeId)", new object[1]{ (object) range1.Select<PermitWaterWithdrawalPurpose, string>((Func<PermitWaterWithdrawalPurpose, string>) (x => x.Id.ToString())).ToList<string>() }) }, "PermitWaterWithdrawalPurpose");
      IEnumerable<string> _savedIds = range2.Select<PermitWastewaterTreatmentDisposal, PermitWaterWithdrawalPurpose>((Func<PermitWastewaterTreatmentDisposal, PermitWaterWithdrawalPurpose>) (x => x.PermitWaterWithdrawalPurpose)).ToList<PermitWaterWithdrawalPurpose>().Select<PermitWaterWithdrawalPurpose, string>((Func<PermitWaterWithdrawalPurpose, string>) (x => x.WaterWithdrawalPurposeId.ToString()));
      list1.RemoveAll((Predicate<string>) (x => _savedIds.Contains<string>(x)));
      List<PermitWastewaterTreatmentDisposal> list2 = range2.ToList<PermitWastewaterTreatmentDisposal>();
      foreach (string s in list1)
      {
        PermitWastewaterTreatmentDisposal treatmentDisposal = new PermitWastewaterTreatmentDisposal();
        treatmentDisposal.PermitWaterWithdrawalPurpose = new PermitWaterWithdrawalPurpose();
        treatmentDisposal.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId = new int?(int.Parse(s));
        list2.Add(treatmentDisposal);
      }
      foreach (PermitWastewaterTreatmentDisposal treatmentDisposal in list2)
      {
        PermitWastewaterTreatmentDisposal model = treatmentDisposal;
        PermitWaterWithdrawalPurpose withdrawalPurpose = range1.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
        {
          int id = x.LU_WaterWithdrawalPurpose.Id;
          int? withdrawalPurposeId = model.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId;
          if (id == withdrawalPurposeId.GetValueOrDefault())
            return withdrawalPurposeId.HasValue;
          return false;
        }));
        if (withdrawalPurpose != null)
        {
          WastewaterTreatmentAndDisposalForm treatmentAndDisposalForm = Mapper.Map<PermitWastewaterTreatmentDisposal, WastewaterTreatmentAndDisposalForm>(model);
          treatmentAndDisposalForm.WaterWithdrawalPurposeId = withdrawalPurpose.WaterWithdrawalPurposeId.Value;
          treatmentAndDisposalForm.Title = withdrawalPurpose.LU_WaterWithdrawalPurpose.Description;
          if (treatmentAndDisposalForm.Id == 0)
            treatmentAndDisposalForm.IsWastewaterDischarge = true;
          treatmentAndDisposalFormList.Add(treatmentAndDisposalForm);
        }
      }
      return treatmentAndDisposalFormList;
    }

    public List<PermitWwPrivateWaterSupplierForm> GetPrivateWaterSupplierForms(string waterUseCategoryTypeids, int permitId)
    {
      List<PermitWwPrivateWaterSupplierForm> waterSupplierFormList = new List<PermitWwPrivateWaterSupplierForm>();
      List<string> list1 = ((IEnumerable<string>) waterUseCategoryTypeids.Split(',')).ToList<string>();
      list1.RemoveAll((Predicate<string>) (x =>
      {
        if (!(x == "") && !(x == "40") && (!(x == "42") && !(x == "44")) && !(x == "57"))
          return x == "61";
        return true;
      }));
      IEnumerable<PermitWaterWithdrawalPurpose> range1 = this._permitWaterWithdrawalPurposeService.GetRange(0, 9999, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("@0.Contains(outerIt.WaterWithdrawalPurposeId) and PermitId=@1", new object[2]{ (object) list1, (object) permitId }) }, "LU_WaterWithdrawalPurpose");
      IEnumerable<PermitWwPrivateWaterSupplier> range2 = this._permitWaterWithdrawalPrivateWaterSupplierService.GetRange(0, 9999, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("@0.Contains(outerIt.PermitWaterWithdrawalPurposeId)", new object[1]{ (object) range1.Select<PermitWaterWithdrawalPurpose, string>((Func<PermitWaterWithdrawalPurpose, string>) (x => x.Id.ToString())).ToList<string>() }) }, "PermitWaterWithdrawalPurpose");
      IEnumerable<string> _savedIds = range2.Select<PermitWwPrivateWaterSupplier, PermitWaterWithdrawalPurpose>((Func<PermitWwPrivateWaterSupplier, PermitWaterWithdrawalPurpose>) (x => x.PermitWaterWithdrawalPurpose)).ToList<PermitWaterWithdrawalPurpose>().Select<PermitWaterWithdrawalPurpose, string>((Func<PermitWaterWithdrawalPurpose, string>) (x => x.WaterWithdrawalPurposeId.ToString()));
      list1.RemoveAll((Predicate<string>) (x => _savedIds.Contains<string>(x)));
      List<PermitWwPrivateWaterSupplier> list2 = range2.ToList<PermitWwPrivateWaterSupplier>();
      foreach (string s in list1)
      {
        PermitWwPrivateWaterSupplier privateWaterSupplier = new PermitWwPrivateWaterSupplier();
        privateWaterSupplier.PermitWaterWithdrawalPurpose = new PermitWaterWithdrawalPurpose();
        privateWaterSupplier.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId = new int?(int.Parse(s));
        list2.Add(privateWaterSupplier);
      }
      foreach (PermitWwPrivateWaterSupplier privateWaterSupplier in list2)
      {
        PermitWwPrivateWaterSupplier model = privateWaterSupplier;
        PermitWaterWithdrawalPurpose withdrawalPurpose = range1.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
        {
          int id = x.LU_WaterWithdrawalPurpose.Id;
          int? withdrawalPurposeId = model.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId;
          if (id == withdrawalPurposeId.GetValueOrDefault())
            return withdrawalPurposeId.HasValue;
          return false;
        }));
        if (withdrawalPurpose != null)
        {
          PermitWwPrivateWaterSupplierForm waterSupplierForm = Mapper.Map<PermitWwPrivateWaterSupplier, PermitWwPrivateWaterSupplierForm>(model);
          waterSupplierForm.PermitWaterWithdrawalPurposeId = withdrawalPurpose.WaterWithdrawalPurposeId.Value;
          waterSupplierForm.WaterWithdrawalPurposeId = withdrawalPurpose.WaterWithdrawalPurposeId.Value;
          waterSupplierForm.UsePercentage = withdrawalPurpose.UsePercent;
          waterSupplierForm.Title = withdrawalPurpose.LU_WaterWithdrawalPurpose.Description;
          waterSupplierFormList.Add(waterSupplierForm);
        }
      }
      return waterSupplierFormList;
    }

    public WaterUseDetailForm InitializeWaterUseDetailFrom(int permitId)
    {
      if (permitId == 0)
        return this.CreateWaterUseDetailForm();
      return this.LoadWaterUseDetailForm(permitId);
    }

    private WaterUseDetailForm CreateWaterUseDetailForm()
    {
      return new WaterUseDetailForm() { GolfCourseIrrigationForm = Mapper.Map<PermitWwGolfCourseIrrigation, PermitWwGolfCourseIrrigationForm>(new PermitWwGolfCourseIrrigation()), PermitWwCommercialDrinkingOrSanitaryForm = Mapper.Map<PermitWwCommercialDrinkingOrSanitary, PermitWwCommercialDrinkingOrSanitaryForm>(new PermitWwCommercialDrinkingOrSanitary()), PermitWwEducationalDrinkingOrsanitaryForm = Mapper.Map<PermitWwInstitutionalEducational_drinkingOrsanitary, PermitWwEducationalDrinkingOrsanitaryForm>(new PermitWwInstitutionalEducational_drinkingOrsanitary()), PermitWwConstructionDewateringForm = Mapper.Map<PermitWwConstructionDewatering, PermitWwConstructionDewateringForm>(new PermitWwConstructionDewatering()) };
    }

    private WaterUseDetailForm LoadWaterUseDetailForm(int permitId)
    {
      WaterUseDetailForm waterUseDetailForm = this.CreateWaterUseDetailForm();
      IEnumerable<PermitWaterWithdrawalPurpose> range = this._permitWaterWithdrawalPurposeService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId=" + (object) permitId, new object[0]) }, (string) null);
      if (range == null || range.Count<PermitWaterWithdrawalPurpose>() <= 0)
        return this.CreateWaterUseDetailForm();
      PermitWaterWithdrawalPurpose withdrawalPurpose1 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 2)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose1 != null)
      {
        this.LoadPoultryWateringEvaporative(waterUseDetailForm, withdrawalPurpose1.Id);
        waterUseDetailForm.PoultryWateringEvaporativeForm.UsePercentage = withdrawalPurpose1.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose2 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 3)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose2 != null)
      {
        this.LoadPoultryWateringFogger(waterUseDetailForm, withdrawalPurpose2.Id);
        waterUseDetailForm.PoultryWateringFoggerForm.UsePercentage = withdrawalPurpose2.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose3 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 5)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose3 != null)
      {
        this.LoadOtherLivestockWatering(waterUseDetailForm, withdrawalPurpose3.Id);
        waterUseDetailForm.OtherLivestockWateringForm.UsePercentage = withdrawalPurpose3.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose4 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 6)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose4 != null)
      {
        this.LoadFarmPotableSupplies(waterUseDetailForm, withdrawalPurpose4.Id);
        waterUseDetailForm.PermitWwFarmPotableSuppliesForm.UsePercentage = withdrawalPurpose4.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose5 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 4)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose5 != null)
      {
        this.LoadDairyAnimalWatering(waterUseDetailForm, withdrawalPurpose5.Id);
        waterUseDetailForm.DairyAnimalWateringForm.UsePercentage = withdrawalPurpose5.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose6 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 7)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose6 != null)
      {
        this.LoadAgriculturalIrrigation(waterUseDetailForm, withdrawalPurpose6.Id);
        waterUseDetailForm.CropUseForm.UsePercentage = withdrawalPurpose6.UsePercent;
        waterUseDetailForm.CropUseForm.TotalNumberOfAcres = new Decimal?(withdrawalPurpose6.TotalIrrigatedAcres ?? new Decimal(0));
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose7 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 8)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose7 != null)
      {
        this.LoadNurseryIrrigation(waterUseDetailForm, withdrawalPurpose7.Id);
        waterUseDetailForm.NurseryStockIrrigationForm.UsePercentage = withdrawalPurpose7.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose8 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 9)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose8 != null)
      {
        this.LoadSodFarmIrrigation(waterUseDetailForm, withdrawalPurpose8.Id);
        waterUseDetailForm.SodIrrigationForm.UsePercentage = withdrawalPurpose8.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose9 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 10)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose9 != null)
      {
        this.LoadFoodProcessing(waterUseDetailForm, withdrawalPurpose9.Id);
        waterUseDetailForm.PermitWwFoodProcessingForm.UsePercentage = withdrawalPurpose9.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose10 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 11)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose10 != null)
      {
        this.LoadAquacultureOrAquarium(waterUseDetailForm, withdrawalPurpose10.Id);
        waterUseDetailForm.PermitWwAquacultureAquariumForm.UsePercentage = withdrawalPurpose10.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose11 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 12)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose11 != null)
      {
        this.LoadIrrigationUndefined(waterUseDetailForm, withdrawalPurpose11.Id);
        waterUseDetailForm.IrrigationUndefinedForm.UsePercentage = withdrawalPurpose11.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose12 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 13)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose12 != null)
      {
        this.LoadGolfCourseIrrigation(waterUseDetailForm, withdrawalPurpose12.Id);
        waterUseDetailForm.GolfCourseIrrigationForm.UsePercentage = withdrawalPurpose12.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose13 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 14)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose13 != null)
      {
        this.LoadLawnAndParkIrrigation(waterUseDetailForm, withdrawalPurpose13.Id);
        waterUseDetailForm.LawnAndParkIrrigationForm.UsePercentage = withdrawalPurpose13.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose14 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 15)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose14 != null)
      {
        this.LoadSmallIntermitentIrrigation(waterUseDetailForm, withdrawalPurpose14.Id);
        waterUseDetailForm.SmallIntermitentIrrigationForm.UsePercentage = withdrawalPurpose14.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose15 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 40)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose15 != null)
      {
        this.LoadCommercialDrinkingOrSanitary(waterUseDetailForm, withdrawalPurpose15.Id);
        waterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.UsePercentage = withdrawalPurpose15.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose16 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 42)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose16 != null)
      {
        this.LoadEducationalDrinkingOrSanitary(waterUseDetailForm, withdrawalPurpose16.Id);
        waterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.UsePercentage = withdrawalPurpose16.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose17 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 44)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose17 != null)
      {
        this.LoadReligiousDrinkingOrSanitary(waterUseDetailForm, withdrawalPurpose17.Id);
        waterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.UsePercentage = withdrawalPurpose17.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose18 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 57)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose18 != null)
      {
        this.LoadWildlifePonds(waterUseDetailForm, withdrawalPurpose18.Id);
        waterUseDetailForm.PermitWwWildlifePondForm.UsePercentage = withdrawalPurpose18.UsePercent;
      }
      PermitWaterWithdrawalPurpose withdrawalPurpose19 = range.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 61)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (withdrawalPurpose19 != null)
      {
        this.LoadConstructionDewatering(waterUseDetailForm, withdrawalPurpose19.Id);
        waterUseDetailForm.PermitWwConstructionDewateringForm.UsePercentage = withdrawalPurpose19.UsePercent;
      }
      IEnumerable<PermitWaterWithdrawalPurpose> source = range.Where<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() > 15)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (source != null)
      {
        string waterUseCategoryTypeids = string.Join<int?>(",", source.Select<PermitWaterWithdrawalPurpose, int?>((Func<PermitWaterWithdrawalPurpose, int?>) (x => x.WaterWithdrawalPurposeId)));
        waterUseDetailForm.PrivateWaterSuppilerForm.UsePercentage = source.Sum<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, int?>) (x => x.UsePercent));
        waterUseDetailForm.PrivateWaterSuppilerForm.PrivateWaterSupplierForms = this.GetPrivateWaterSupplierForms(waterUseCategoryTypeids, permitId);
      }
      return waterUseDetailForm;
    }

    private void LoadPoultryWateringFogger(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
    {
      List<PermitWwPoultry> list = this._permitWwPoultryService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").ToList<PermitWwPoultry>();
      waterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms = Mapper.Map<List<PermitWwPoultry>, List<PoultryWateringForm>>(list ?? new List<PermitWwPoultry>());
    }

    private void LoadPoultryWateringEvaporative(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
    {
      List<PermitWwPoultry> list = this._permitWwPoultryService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").ToList<PermitWwPoultry>();
      waterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms = Mapper.Map<List<PermitWwPoultry>, List<PoultryWateringForm>>(list ?? new List<PermitWwPoultry>());
    }

    private void LoadOtherLivestockWatering(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
    {
      List<PermitWwLivestock> list = this._permitWaterWithdrawalLivestockService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").ToList<PermitWwLivestock>();
      waterUseDetailForm.OtherLivestockWateringForm.LivestockForms = Mapper.Map<List<PermitWwLivestock>, List<LivestockForm>>(list ?? new List<PermitWwLivestock>());
    }

    private void LoadDairyAnimalWatering(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
    {
      List<PermitWwLivestock> list = this._permitWaterWithdrawalLivestockService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").ToList<PermitWwLivestock>();
      waterUseDetailForm.DairyAnimalWateringForm.LivestockForms = Mapper.Map<List<PermitWwLivestock>, List<LivestockForm>>(list ?? new List<PermitWwLivestock>());
    }

    private void LoadFarmPotableSupplies(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
    {
      PermitWwFarmPortableSupply farmPortableSupply = this._permitWwFarmPotableWaterSupplyService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwFarmPortableSupply>();
      waterUseDetailForm.PermitWwFarmPotableSuppliesForm = Mapper.Map<PermitWwFarmPortableSupply, PermitWwFarmPotableSuppliesForm>(farmPortableSupply ?? new PermitWwFarmPortableSupply());
    }

    private void LoadAgriculturalIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      List<PermitWwCrop> list = this._permitWwCropService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").ToList<PermitWwCrop>();
      waterUseDetailform.CropUseForm.CropForms = Mapper.Map<List<PermitWwCrop>, List<CropForm>>(list ?? new List<PermitWwCrop>());
    }

    private void LoadNurseryIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      List<PermitWwAgricultural> list = this._permitWwAgriculturalService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").ToList<PermitWwAgricultural>();
      waterUseDetailform.NurseryStockIrrigationForm.IrrigationForms = Mapper.Map<List<PermitWwAgricultural>, List<PermitWwIrrigationForm>>(list ?? new List<PermitWwAgricultural>());
    }

    private void LoadSodFarmIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      List<PermitWwAgricultural> list = this._permitWwAgriculturalService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").ToList<PermitWwAgricultural>();
      waterUseDetailform.SodIrrigationForm.IrrigationForms = Mapper.Map<List<PermitWwAgricultural>, List<PermitWwIrrigationForm>>(list ?? new List<PermitWwAgricultural>());
    }

    private void LoadFoodProcessing(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
    {
      PermitWwFoodProcessing wwFoodProcessing = this._permitWwFoodProcessing.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwFoodProcessing>();
      waterUseDetailForm.PermitWwFoodProcessingForm = Mapper.Map<PermitWwFoodProcessing, PermitWwFoodProcessingForm>(wwFoodProcessing ?? new PermitWwFoodProcessing());
    }

    private void LoadAquacultureOrAquarium(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
    {
      PermitWwAquacultureOrAquarium aquacultureOrAquarium = this._permitWwAquacultureOrAquariumService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwAquacultureOrAquarium>();
      waterUseDetailForm.PermitWwAquacultureAquariumForm = Mapper.Map<PermitWwAquacultureOrAquarium, PermitWwAquacultureAquariumForm>(aquacultureOrAquarium ?? new PermitWwAquacultureOrAquarium());
    }

    private void LoadIrrigationUndefined(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwNonAgricultureIrrigation agricultureIrrigation = this._permitWwNonAgricultureIrrigationService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwNonAgricultureIrrigation>();
      waterUseDetailform.IrrigationUndefinedForm = Mapper.Map<PermitWwNonAgricultureIrrigation, PermitWwNonAgricultureIrrigationForm>(agricultureIrrigation ?? new PermitWwNonAgricultureIrrigation());
    }

    private void LoadGolfCourseIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwGolfCourseIrrigation courseIrrigation = this._permitWwGolfCourseIrrigationService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwGolfCourseIrrigation>();
      waterUseDetailform.GolfCourseIrrigationForm = Mapper.Map<PermitWwGolfCourseIrrigation, PermitWwGolfCourseIrrigationForm>(courseIrrigation ?? new PermitWwGolfCourseIrrigation());
    }

    private void LoadLawnAndParkIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwNonAgricultureIrrigation agricultureIrrigation = this._permitWwNonAgricultureIrrigationService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwNonAgricultureIrrigation>();
      waterUseDetailform.LawnAndParkIrrigationForm = Mapper.Map<PermitWwNonAgricultureIrrigation, PermitWwNonAgricultureIrrigationForm>(agricultureIrrigation ?? new PermitWwNonAgricultureIrrigation());
    }

    private void LoadSmallIntermitentIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwNonAgricultureIrrigation agricultureIrrigation = this._permitWwNonAgricultureIrrigationService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwNonAgricultureIrrigation>();
      waterUseDetailform.SmallIntermitentIrrigationForm = Mapper.Map<PermitWwNonAgricultureIrrigation, PermitWwNonAgricultureIrrigationForm>(agricultureIrrigation ?? new PermitWwNonAgricultureIrrigation());
    }

    private void LoadCommercialDrinkingOrSanitary(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwCommercialDrinkingOrSanitary drinkingOrSanitary = this._permitWwCommertialDrinkingOrSanitaryService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwCommercialDrinkingOrSanitary>();
      waterUseDetailform.PermitWwCommercialDrinkingOrSanitaryForm = Mapper.Map<PermitWwCommercialDrinkingOrSanitary, PermitWwCommercialDrinkingOrSanitaryForm>(drinkingOrSanitary ?? new PermitWwCommercialDrinkingOrSanitary());
    }

    private void LoadEducationalDrinkingOrSanitary(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwInstitutionalEducational_drinkingOrsanitary drinkingOrsanitary = this._permitWwEducationalDrinkingOrSanitaryService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwInstitutionalEducational_drinkingOrsanitary>();
      waterUseDetailform.PermitWwEducationalDrinkingOrsanitaryForm = Mapper.Map<PermitWwInstitutionalEducational_drinkingOrsanitary, PermitWwEducationalDrinkingOrsanitaryForm>(drinkingOrsanitary ?? new PermitWwInstitutionalEducational_drinkingOrsanitary());
    }

    private void LoadReligiousDrinkingOrSanitary(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwInstitutionalReligious_DringkingOrSanitary dringkingOrSanitary = this._permitWwReligiousDrinkingOrSanitaryService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwInstitutionalReligious_DringkingOrSanitary>();
      waterUseDetailform.PermitWwReligiousDringkingOrSanitaryForm = Mapper.Map<PermitWwInstitutionalReligious_DringkingOrSanitary, PermitWwReligiousDringkingOrSanitaryForm>(dringkingOrSanitary ?? new PermitWwInstitutionalReligious_DringkingOrSanitary());
    }

    private void LoadWildlifePonds(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwWildlifePond permitWwWildlifePond = this._permitWwWildlifePondService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, (string) null).FirstOrDefault<PermitWwWildlifePond>();
      waterUseDetailform.PermitWwWildlifePondForm = Mapper.Map<PermitWwWildlifePond, PermitWwWildlifePondForm>(permitWwWildlifePond ?? new PermitWwWildlifePond());
    }

    private void LoadConstructionDewatering(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
    {
      PermitWwConstructionDewatering constructionDewatering = this._permitWwConstructionDewateringService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitWaterWithdrawalPurposeId=" + (object) permitWaterWithdrawalPurposeId, new object[0]) }, "PermitWaterWithdrawalPurpose").FirstOrDefault<PermitWwConstructionDewatering>();
      waterUseDetailform.PermitWwConstructionDewateringForm = Mapper.Map<PermitWwConstructionDewatering, PermitWwConstructionDewateringForm>(constructionDewatering ?? new PermitWwConstructionDewatering());
    }

    private void SavePoultryWatering(ApplicationWizard form)
    {
      PoultryWateringUseDetailGroupForm wateringEvaporativeForm = form.WaterUseDetailForm.PoultryWateringEvaporativeForm;
      if (wateringEvaporativeForm.PoultryWateringForms != null && wateringEvaporativeForm.PoultryWateringForms.Count > 0)
      {
        PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
        {
          int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
          if (withdrawalPurposeId.GetValueOrDefault() == 2)
            return withdrawalPurposeId.HasValue;
          return false;
        }));
        if (entity1 != null)
        {
          entity1.UsePercent = wateringEvaporativeForm.UsePercentage;
          this._permitWaterWithdrawalPurposeService.Save(entity1);
          wateringEvaporativeForm.PoultryWateringForms.RemoveAll((Predicate<PoultryWateringForm>) (x => x.HasDeleted));
          foreach (PermitWwPoultry entity2 in Mapper.Map<List<PoultryWateringForm>, List<PermitWwPoultry>>(wateringEvaporativeForm.PoultryWateringForms))
          {
            entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
            this._permitWwPoultryService.Save(entity2);
          }
        }
      }
      PoultryWateringUseDetailGroupForm wateringFoggerForm = form.WaterUseDetailForm.PoultryWateringFoggerForm;
      if (wateringFoggerForm.PoultryWateringForms == null || wateringFoggerForm.PoultryWateringForms.Count <= 0)
        return;
      PermitWaterWithdrawalPurpose entity3 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 3)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity3 == null)
        return;
      entity3.UsePercent = wateringFoggerForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity3);
      wateringFoggerForm.PoultryWateringForms.RemoveAll((Predicate<PoultryWateringForm>) (x => x.HasDeleted));
      foreach (PermitWwPoultry entity1 in Mapper.Map<List<PoultryWateringForm>, List<PermitWwPoultry>>(wateringFoggerForm.PoultryWateringForms))
      {
        entity1.PermitWaterWithdrawalPurposeId = new int?(entity3.Id);
        this._permitWwPoultryService.Save(entity1);
      }
    }

    private void SavePermitLivestockWatering(ApplicationWizard form)
    {
      PermitWwLivestockUseDetailGroupForm permitWwLivestockForm = form.WaterUseDetailForm.PermitWwLivestockForm;
      if (permitWwLivestockForm.LivestockForms == null || permitWwLivestockForm.LivestockForms.Count <= 0)
        return;
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 1)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 == null)
        return;
      entity1.UsePercent = permitWwLivestockForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity1);
      permitWwLivestockForm.LivestockForms.RemoveAll((Predicate<LivestockForm>) (x => x.HasDeleted));
      foreach (PermitWwLivestock entity2 in Mapper.Map<List<LivestockForm>, List<PermitWwLivestock>>(permitWwLivestockForm.LivestockForms))
      {
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
        this._permitWaterWithdrawalLivestockService.Save(entity2);
      }
    }

    private void SavePermitWwIrrigation(ApplicationWizard form)
    {
      if (form.SavedWaterWithdrawalPurpose == null)
        return;
      PermitWwNonAgricultureIrrigation agricultureIrrigation = new PermitWwNonAgricultureIrrigation();
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 12)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 != null)
      {
        entity1.UsePercent = form.WaterUseDetailForm.IrrigationUndefinedForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity1);
        PermitWwNonAgricultureIrrigation entity2 = Mapper.Map<PermitWwNonAgricultureIrrigationForm, PermitWwNonAgricultureIrrigation>(form.WaterUseDetailForm.IrrigationUndefinedForm ?? new PermitWwNonAgricultureIrrigationForm());
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
        this._permitWwNonAgricultureIrrigationService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity3 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 13)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity3 != null && form.WaterUseDetailForm.GolfCourseIrrigationForm != null)
      {
        entity3.UsePercent = form.WaterUseDetailForm.GolfCourseIrrigationForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity3);
        PermitWwGolfCourseIrrigation courseIrrigation = new PermitWwGolfCourseIrrigation();
        PermitWwGolfCourseIrrigation entity2 = Mapper.Map<PermitWwGolfCourseIrrigationForm, PermitWwGolfCourseIrrigation>(form.WaterUseDetailForm.GolfCourseIrrigationForm ?? new PermitWwGolfCourseIrrigationForm());
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity3.Id);
        this._permitWwGolfCourseIrrigationService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity4 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 14)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity4 != null)
      {
        entity4.UsePercent = form.WaterUseDetailForm.LawnAndParkIrrigationForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity4);
        PermitWwNonAgricultureIrrigation entity2 = Mapper.Map<PermitWwNonAgricultureIrrigationForm, PermitWwNonAgricultureIrrigation>(form.WaterUseDetailForm.LawnAndParkIrrigationForm ?? new PermitWwNonAgricultureIrrigationForm());
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity4.Id);
        this._permitWwNonAgricultureIrrigationService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity5 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 15)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity5 != null)
      {
        entity5.UsePercent = form.WaterUseDetailForm.SmallIntermitentIrrigationForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity5);
        PermitWwNonAgricultureIrrigation entity2 = Mapper.Map<PermitWwNonAgricultureIrrigationForm, PermitWwNonAgricultureIrrigation>(form.WaterUseDetailForm.SmallIntermitentIrrigationForm ?? new PermitWwNonAgricultureIrrigationForm());
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity5.Id);
        this._permitWwNonAgricultureIrrigationService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity6 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 8)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity6 != null)
      {
        entity6.UsePercent = form.WaterUseDetailForm.NurseryStockIrrigationForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity6);
        if (form.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms != null)
        {
          form.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms.RemoveAll((Predicate<PermitWwIrrigationForm>) (x => x.HasDeleted));
          foreach (PermitWwAgricultural entity2 in Mapper.Map<List<PermitWwIrrigationForm>, List<PermitWwAgricultural>>(form.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms))
          {
            entity2.PermitWaterWithdrawalPurposeId = new int?(entity6.Id);
            this._permitWwAgriculturalService.Save(entity2);
          }
        }
      }
      PermitWaterWithdrawalPurpose entity7 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 9)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity7 == null)
        return;
      entity7.UsePercent = form.WaterUseDetailForm.SodIrrigationForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity7);
      if (form.WaterUseDetailForm.SodIrrigationForm.IrrigationForms == null)
        return;
      form.WaterUseDetailForm.SodIrrigationForm.IrrigationForms.RemoveAll((Predicate<PermitWwIrrigationForm>) (x => x.HasDeleted));
      foreach (PermitWwAgricultural entity2 in Mapper.Map<List<PermitWwIrrigationForm>, List<PermitWwAgricultural>>(form.WaterUseDetailForm.SodIrrigationForm.IrrigationForms))
      {
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity7.Id);
        this._permitWwAgriculturalService.Save(entity2);
      }
    }

    private void SaveDairyAnimalWatering(ApplicationWizard form)
    {
      if (form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms == null || form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms.Count <= 0)
        return;
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 4)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 == null)
        return;
      entity1.UsePercent = form.WaterUseDetailForm.DairyAnimalWateringForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity1);
      form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms.RemoveAll((Predicate<LivestockForm>) (x => x.HasDeleted));
      foreach (PermitWwLivestock entity2 in Mapper.Map<List<LivestockForm>, List<PermitWwLivestock>>(form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms))
      {
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
        this._permitWaterWithdrawalLivestockService.Save(entity2);
      }
    }

    private void SaveFarmPotableSupplies(ApplicationWizard form)
    {
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 6)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 == null)
        return;
      entity1.UsePercent = form.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity1);
      PermitWwFarmPortableSupply entity2 = Mapper.Map<PermitWwFarmPotableSuppliesForm, PermitWwFarmPortableSupply>(form.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm);
      entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
      this._permitWwFarmPotableWaterSupplyService.Save(entity2);
    }

    private void SaveOtherLivestockWatering(ApplicationWizard form)
    {
      if (form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms == null || form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms.Count <= 0)
        return;
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 5)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 == null)
        return;
      entity1.UsePercent = form.WaterUseDetailForm.OtherLivestockWateringForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity1);
      form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms.RemoveAll((Predicate<LivestockForm>) (x => x.HasDeleted));
      foreach (PermitWwLivestock entity2 in Mapper.Map<List<LivestockForm>, List<PermitWwLivestock>>(form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms))
      {
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
        this._permitWaterWithdrawalLivestockService.Save(entity2);
      }
    }

    private void SaveCrops(ApplicationWizard form)
    {
      if (form.WaterUseDetailForm.CropUseForm.CropForms == null || form.WaterUseDetailForm.CropUseForm.CropForms.Count <= 0)
        return;
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 7)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 == null)
        return;
      entity1.UsePercent = form.WaterUseDetailForm.CropUseForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity1);
      form.WaterUseDetailForm.CropUseForm.CropForms.RemoveAll((Predicate<CropForm>) (x => x.HasDeleted));
      foreach (PermitWwCrop entity2 in Mapper.Map<List<CropForm>, List<PermitWwCrop>>(form.WaterUseDetailForm.CropUseForm.CropForms))
      {
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
        this._permitWwCropService.Save(entity2);
      }
    }

    private void SaveFoodProcessing(ApplicationWizard form)
    {
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 10)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 == null)
        return;
      entity1.UsePercent = form.WaterUseDetailForm.PermitWwFoodProcessingForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity1);
      PermitWwFoodProcessing entity2 = Mapper.Map<PermitWwFoodProcessingForm, PermitWwFoodProcessing>(form.WaterUseDetailForm.PermitWwFoodProcessingForm ?? new PermitWwFoodProcessingForm());
      entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
      this._permitWwFoodProcessing.Save(entity2);
    }

    private void SaveAquacultureAndAquarium(ApplicationWizard form)
    {
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 11)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 == null)
        return;
      entity1.UsePercent = form.WaterUseDetailForm.PermitWwAquacultureAquariumForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity1);
      PermitWwAquacultureOrAquarium entity2 = Mapper.Map<PermitWwAquacultureAquariumForm, PermitWwAquacultureOrAquarium>(form.WaterUseDetailForm.PermitWwAquacultureAquariumForm);
      entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
      this._permitWwAquacultureOrAquariumService.Save(entity2);
    }

    private void SavePrivateWaterSupplierVariants(ApplicationWizard form)
    {
      PermitWaterWithdrawalPurpose entity1 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 40)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity1 != null)
      {
        entity1.UsePercent = form.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity1);
        PermitWwCommercialDrinkingOrSanitary entity2 = Mapper.Map<PermitWwCommercialDrinkingOrSanitaryForm, PermitWwCommercialDrinkingOrSanitary>(form.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm);
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity1.Id);
        this._permitWwCommertialDrinkingOrSanitaryService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity3 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 42)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity3 != null)
      {
        entity3.UsePercent = form.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity3);
        PermitWwInstitutionalEducational_drinkingOrsanitary entity2 = Mapper.Map<PermitWwEducationalDrinkingOrsanitaryForm, PermitWwInstitutionalEducational_drinkingOrsanitary>(form.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm);
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity3.Id);
        this._permitWwEducationalDrinkingOrSanitaryService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity4 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 44)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity4 != null)
      {
        entity4.UsePercent = form.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity4);
        PermitWwInstitutionalReligious_DringkingOrSanitary entity2 = Mapper.Map<PermitWwReligiousDringkingOrSanitaryForm, PermitWwInstitutionalReligious_DringkingOrSanitary>(form.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm);
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity4.Id);
        this._permitWwReligiousDrinkingOrSanitaryService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity5 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 57)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity5 != null)
      {
        entity5.UsePercent = form.WaterUseDetailForm.PermitWwWildlifePondForm.UsePercentage;
        this._permitWaterWithdrawalPurposeService.Save(entity5);
        PermitWwWildlifePond entity2 = Mapper.Map<PermitWwWildlifePondForm, PermitWwWildlifePond>(form.WaterUseDetailForm.PermitWwWildlifePondForm);
        entity2.PermitWaterWithdrawalPurposeId = new int?(entity5.Id);
        this._permitWwWildlifePondService.Save(entity2);
      }
      PermitWaterWithdrawalPurpose entity6 = form.SavedWaterWithdrawalPurpose.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 61)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (entity6 == null)
        return;
      entity6.UsePercent = form.WaterUseDetailForm.PermitWwConstructionDewateringForm.UsePercentage;
      this._permitWaterWithdrawalPurposeService.Save(entity6);
      PermitWwConstructionDewatering entity7 = Mapper.Map<PermitWwConstructionDewateringForm, PermitWwConstructionDewatering>(form.WaterUseDetailForm.PermitWwConstructionDewateringForm);
      entity7.PermitWaterWithdrawalPurposeId = new int?(entity6.Id);
      this._permitWwConstructionDewateringService.Save(entity7);
    }

    private void SavePrivateWaterSuppiler(ApplicationWizard form)
    {
      PermitWwPrivateWaterSupplierGroupForm waterSuppilerForm = form.WaterUseDetailForm.PrivateWaterSuppilerForm;
      if (waterSuppilerForm.PrivateWaterSupplierForms == null || waterSuppilerForm.PrivateWaterSupplierForms.Count <= 0)
        return;
      IEnumerable<PermitWaterWithdrawalPurpose> source = form.SavedWaterWithdrawalPurpose.Where<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() > 15)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      foreach (PermitWwPrivateWaterSupplierForm waterSupplierForm in waterSuppilerForm.PrivateWaterSupplierForms)
      {
        PermitWwPrivateWaterSupplierForm _form = waterSupplierForm;
        PermitWwPrivateWaterSupplier entity1 = Mapper.Map<PermitWwPrivateWaterSupplierForm, PermitWwPrivateWaterSupplier>(_form);
        PermitWaterWithdrawalPurpose entity2 = source.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
        {
          int? withdrawalPurposeId1 = x.WaterWithdrawalPurposeId;
          int withdrawalPurposeId2 = _form.PermitWaterWithdrawalPurposeId;
          if (withdrawalPurposeId1.GetValueOrDefault() == withdrawalPurposeId2)
            return withdrawalPurposeId1.HasValue;
          return false;
        }));
        if (entity2 != null)
        {
          entity2.UsePercent = _form.UsePercentage;
          this._permitWaterWithdrawalPurposeService.Save(entity2);
          entity1.PermitWaterWithdrawalPurposeId = new int?(entity2.Id);
          this._permitWwPrivateWaterSupplier.Save(entity1);
        }
      }
    }
  }
}
