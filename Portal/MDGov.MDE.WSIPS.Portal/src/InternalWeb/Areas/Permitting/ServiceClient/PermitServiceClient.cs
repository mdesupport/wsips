﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient.PermitServiceClient
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient
{
  public class PermitServiceClient : IPermitServiceClient
  {
    private readonly IPermitService _permittingService;
    private readonly IUpdatableService<Contact> _contactService;
    private readonly IPermitWaterWithdrawalPurposeService _permitWaterWithdrawalPurposeService;
    private readonly IUpdatableService<PermitContact> _permitContactService;
    private readonly IUpdatableService<PermitContactInformation> _permitContactInformationService;
    private readonly IWaterUseDetailServiceClient _waterUseDetailServiceClient;
    private readonly IUpdatableService<PermitWastewaterTreatmentDisposal> _permitWastewaterTreatmentDisposalService;
    private readonly IUpdatableService<ContactCommunicationMethod> _contactCommunicationMethodService;
    private readonly IService<LU_PermitStatus> _permitStatusService;
    private readonly IService<PermitStatusWorkflow> _permitStatusWorkflowService;
    private readonly IService<PermitStatusHistory> _permitStatusHistoryService;
    private readonly IUpdatableService<ConditionCompliance> _conditionComplianceService;
    private readonly IService<Product> _productService;
    private readonly IUpdatableService<ContactSale> _contactSaleService;

    public PermitServiceClient(IPermitService permittingService, IUpdatableService<Contact> contactService, IPermitWaterWithdrawalPurposeService permitWaterWithdrawalPurposeService, IUpdatableService<PermitContact> permitContactService, IUpdatableService<PermitContactInformation> permitContactInformationService, IUpdatableService<PermitWastewaterTreatmentDisposal> permitWastewaterTreatmentDisposalService, IUpdatableService<ContactCommunicationMethod> contactCommunicationMethodService, IWaterUseDetailServiceClient waterUseDetailServiceClient, IService<LU_PermitStatus> permitStatusService, IService<PermitStatusWorkflow> permitStatusWorkflowService, IService<PermitStatusHistory> permitStatusHistoryService, IUpdatableService<ConditionCompliance> conditionComplianceService, IService<Product> productService, IUpdatableService<ContactSale> contactSaleService)
    {
      this._permittingService = permittingService;
      this._contactService = contactService;
      this._permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
      this._permitContactService = permitContactService;
      this._permitContactInformationService = permitContactInformationService;
      this._waterUseDetailServiceClient = waterUseDetailServiceClient;
      this._permitWastewaterTreatmentDisposalService = permitWastewaterTreatmentDisposalService;
      this._contactCommunicationMethodService = contactCommunicationMethodService;
      this._permitStatusService = permitStatusService;
      this._permitStatusWorkflowService = permitStatusWorkflowService;
      this._permitStatusHistoryService = permitStatusHistoryService;
      this._conditionComplianceService = conditionComplianceService;
      this._productService = productService;
      this._contactSaleService = contactSaleService;
    }

    public ApplicationWizard InitializeApplicationWizardFrom()
    {
      ApplicationWizard applicationWizard = new ApplicationWizard() { Contacts = new List<ContactForm>() };
      ContactForm contactForm1 = Mapper.Map<Contact, ContactForm>(new Contact());
      ContactForm contactForm2 = Mapper.Map<Contact, ContactForm>(new Contact());
      ContactForm contactForm3 = Mapper.Map<Contact, ContactForm>(new Contact());
      contactForm1.ContactType = ContactType.WaterUser;
      contactForm1.Active = true;
      contactForm2.ContactType = ContactType.LandOwner;
      contactForm2.Active = true;
      contactForm3.ContactType = ContactType.Consultant;
      contactForm3.Active = true;
      applicationWizard.Contacts.Add(contactForm1);
      applicationWizard.Contacts.Add(contactForm2);
      applicationWizard.Contacts.Add(contactForm3);
      applicationWizard.IsWaterUser = true;
      applicationWizard.IsLandOwner = true;
      applicationWizard.IsConsultant = false;
      return applicationWizard;
    }

    public ApplicationWizard GetApplicationWizardForExistingPermit(int permitId)
    {
      ApplicationWizard form = new ApplicationWizard() { ContactSearchForm = new ContactSearchForm(), PermitId = permitId, Contacts = new List<ContactForm>() };
      Permit byId = this._permittingService.GetById(permitId, (string) null);
      form.PermitStatusId = byId.PermitStatusId.Value;
      form.IsWaterUser = Convert.ToInt32(byId.ApplicantIdentification.Substring(1, 1)) != 0;
      form.IsLandOwner = Convert.ToInt32(byId.ApplicantIdentification.Substring(2, 1)) != 0;
      form.IsConsultant = Convert.ToInt32(byId.ApplicantIdentification.Substring(3, 1)) != 0;
      form.PermitIssuedTo = Convert.ToInt32(byId.ApplicantIdentification.Substring(4, 1));
      form.CurrentStep = "5";
      form.UseDescription = byId.UseDescription;
      this.GetPermitContactInformation(form, permitId);
      IEnumerable<PermitWaterWithdrawalPurpose> range = this._permitWaterWithdrawalPurposeService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId=" + (object) permitId, new object[0]) }, "LU_WaterWithdrawalPurpose");
      if (range != null && range.Any<PermitWaterWithdrawalPurpose>())
        form.SelectedWaterUsePurposeIds = string.Join<int?>(",", range.Select<PermitWaterWithdrawalPurpose, int?>((Func<PermitWaterWithdrawalPurpose, int?>) (x => x.WaterWithdrawalPurposeId)));
      string waterUseCategoryTypeids = string.Join<int?>(",", range.Select<PermitWaterWithdrawalPurpose, int?>((Func<PermitWaterWithdrawalPurpose, int?>) (x => x.WaterWithdrawalPurposeId)).Where<int?>((Func<int?, bool>) (x =>
      {
        int? nullable = x;
        if (nullable.GetValueOrDefault() > 11)
          return nullable.HasValue;
        return false;
      })));
      form.WastewaterTreatmentDisposalForm = this._waterUseDetailServiceClient.GetWasteWaterTreatmentAndDisposal(waterUseCategoryTypeids, form.PermitId);
      return form;
    }

    public IEnumerable<PermitApplication> GetApplicationsInProgress()
    {
      return this._permittingService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("PermitStatusId == @0", new object[1]{ (object) 1 }), new DynamicFilter("ApplicantIdentification.Substring(0, 1).ToLower() == \"i\"", new object[0]) }, "PermitContacts.PermitContactInformation").SelectMany<Permit, PermitContact>((Func<Permit, IEnumerable<PermitContact>>) (x => (IEnumerable<PermitContact>) x.PermitContacts)).Where<PermitContact>((Func<PermitContact, bool>) (x => x.IsPermittee)).Select<PermitContact, PermitApplication>((Func<PermitContact, PermitApplication>) (x => new PermitApplication() { Permittee = x.PermitContactInformation.IsBusiness ? x.PermitContactInformation.BusinessName : x.PermitContactInformation.FirstName + " " + x.PermitContactInformation.LastName, Id = x.Permit.Id, UseDescription = x.Permit.UseDescription, CreatedDate = x.Permit.CreatedDate, CreatedBy = x.Permit.CreatedBy }));
    }

    public int SaveApplicantInformation(ApplicationWizard form)
    {
      Permit entity;
      if (form.PermitId > 0)
        entity = this._permittingService.GetById(form.PermitId, (string) null);
      else
        entity = new Permit()
        {
          RefId = form.RefId,
          ApplicationTypeId = form.ApplicationTypeId,
          PermitStatusId = new int?(1)
        };
      entity.UseDescription = form.UseDescription;
      if (form.IsWaterUser && form.IsLandOwner)
        form.PermitIssuedTo = 2;
      entity.ApplicantIdentification = string.Format("I{0}{1}{2}{3}{4}", (object) (form.IsWaterUser ? 1 : 0), (object) (form.IsLandOwner ? 1 : 0), (object) (form.IsConsultant ? 1 : 0), (object) form.PermitIssuedTo, (object) "5");
      int permitId = this._permittingService.Save(entity);
      if (permitId == 0)
        return 0;
      this.SaveWaterUsePurposes(form, permitId);
      this.SaveWastewaterTreatmentAndDisposal(form);
      this.DeleteExistingPermitContacts(permitId);
      if (form.PermitIssuedTo == 1)
      {
        if (form.IsWaterUser)
          form.PermitIssuedTo = 2;
        else if (form.IsLandOwner)
          form.PermitIssuedTo = 3;
      }
      this.SavePermitContacts(form, permitId);
      return permitId;
    }

    private void DeleteExistingPermitContacts(int permitId)
    {
      IEnumerable<PermitContact> range = this._permitContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0 && @1.Contains(outerIt.ContactTypeId)", new object[2]{ (object) permitId, (object) new int[3]{ 8, 16, 27 } }) }, (string) null);
      if (!range.Any<PermitContact>())
        return;
      foreach (int id in range.Select<PermitContact, int>((Func<PermitContact, int>) (x => x.Id)))
        this._permitContactService.Delete(id);
    }

    public List<ContactSearchModel> BuildResult(IEnumerable<Contact> result)
    {
      List<ContactSearchModel> contactSearchModelList = new List<ContactSearchModel>();
      if (result != null)
        contactSearchModelList = result.Select<Contact, ContactSearchModel>((Func<Contact, ContactSearchModel>) (x => new ContactSearchModel()
        {
          FirstName = x.FirstName,
          MiddleName = x.MiddleInitial,
          LastName = x.LastName,
          Address1 = x.Address1 + " " + x.City + " MD",
          EmailAddress = x.ContactCommunicationMethods.Any<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (y =>
          {
            if (y.IsPrimaryEmail.HasValue)
              return y.IsPrimaryEmail.Value;
            return false;
          })) ? x.ContactCommunicationMethods.Single<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (y =>
          {
            if (y.IsPrimaryEmail.HasValue)
              return y.IsPrimaryEmail.Value;
            return false;
          })).CommunicationValue : "",
          BusinessName = x.BusinessName,
          ContactId = x.Id,
          UserId = x.UserId
        })).ToList<ContactSearchModel>();
      return contactSearchModelList;
    }

    public IEnumerable<DynamicFilter> BuildFilter(ContactSearchForm searchForm, IEnumerable<int> contactIds)
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      string str = string.Empty;
      List<object> objectList = new List<object>();
      if (!string.IsNullOrEmpty(searchForm.SearchFirstName))
      {
        objectList.Add((object) searchForm.SearchFirstName);
        str = "FirstName.Contains(@0)";
      }
      if (!string.IsNullOrEmpty(searchForm.SearchLastName))
      {
        objectList.Add((object) searchForm.SearchLastName);
        str = str.Length <= 0 ? "LastName.Contains(@0)" : str + " and LastName.Contains(@1)";
      }
      if (!string.IsNullOrEmpty(searchForm.SearchBusinessName))
      {
        objectList.Add((object) searchForm.SearchBusinessName);
        str = string.IsNullOrEmpty(searchForm.SearchLastName) || string.IsNullOrEmpty(searchForm.SearchFirstName) ? (str.Length <= 0 ? str + "BusinessName.Contains(@0)" : str + "or BusinessName.Contains(@1)") : str + "or BusinessName.Contains(@2)";
      }
      if (contactIds.Any<int>())
      {
        int count = objectList.Count;
        objectList.Add((object) contactIds);
        if (count > 0)
          str = str + " or @" + (object) count + ".Contains(outerIt.Id)";
        else
          str = str + "@" + (object) count + ".Contains(outerIt.Id)";
      }
      if (objectList.Count > 0)
      {
        string predicate = str + " and ContactTypeId != 9 and ContactTypeId != 10 and ContactTypeId != 30 and ContactTypeId != 20";
        dynamicFilterList.Add(new DynamicFilter(predicate, objectList.ToArray()));
      }
      else
        dynamicFilterList = (List<DynamicFilter>) null;
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public List<ContactForm> BuildContactForm(IEnumerable<Contact> contacts)
    {
      return Mapper.Map<IEnumerable<Contact>, IEnumerable<ContactForm>>(contacts).ToList<ContactForm>();
    }

    public IEnumerable<LU_PermitStatus> GetWorkFlowSteps(int permitId)
    {
      Permit byId = this._permittingService.GetById(permitId, "LU_PermitStatus");
      int valueOrDefault1 = byId.PermitTypeId.GetValueOrDefault();
      int valueOrDefault2 = byId.LU_PermitStatus.PermitCategoryId.GetValueOrDefault();
      if (valueOrDefault2 == 6)
      {
        PermitStatusHistory permitStatusHistory = this._permitStatusHistoryService.GetRange(0, 1, "Id desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0 && LU_PermitStatus.PermitCategoryId != @1", new object[2]{ (object) permitId, (object) 6 }) }, "LU_PermitStatus").SingleOrDefault<PermitStatusHistory>();
        if (permitStatusHistory != null)
          valueOrDefault2 = permitStatusHistory.LU_PermitStatus.PermitCategoryId.GetValueOrDefault();
      }
      return this._permitStatusWorkflowService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitTypeId == @0 && LU_PermitStatus.PermitCategoryId == @1", new object[2]{ (object) valueOrDefault1, (object) valueOrDefault2 }) }, "LU_PermitStatus").Select<PermitStatusWorkflow, LU_PermitStatus>((Func<PermitStatusWorkflow, LU_PermitStatus>) (x => x.LU_PermitStatus));
    }

    public void BuildAndSaveConditionCompliance(int permitId, int pendingStatusId, IEnumerable<PermitCondition> permitConditions)
    {
      DateTime dateTime1 = this._permittingService.GetById(permitId, (string) null).EffectiveDate ?? DateTime.Today;
      foreach (PermitCondition permitCondition in permitConditions)
      {
        int year1 = dateTime1.Year;
        List<ConditionCompliance> conditionComplianceList = new List<ConditionCompliance>();
        int result;
        int.TryParse(ConfigurationManager.AppSettings["PermitLifeYears"], out result);
        int? reportingPeriodId = permitCondition.ConditionReportingPeriodId;
        int valueOrDefault = reportingPeriodId.GetValueOrDefault();
        DateTime dateTime2;
        DateTime dateTime3;
        // ISSUE: explicit reference operation
        if (reportingPeriodId.HasValue)
        {
          switch (valueOrDefault)
          {
            case 1:
              DateTime dateTime4 = DateTime.ParseExact(permitCondition.ComplianceReportingDueDate, "M/d", (IFormatProvider) CultureInfo.InvariantCulture).AddYears(1);
              DateTime exact1 = DateTime.ParseExact(ConfigurationManager.AppSettings["Annual.StartDate"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture);
              DateTime exact2 = DateTime.ParseExact(ConfigurationManager.AppSettings["Annual.EndDate"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture);
              for (int index = 0; index < result; ++index)
              {
                if (index > 0)
                  ++year1;
                dateTime2 = new DateTime(year1 + 1, dateTime4.Month, dateTime4.Day);
                DateTime dateTime5 = new DateTime(year1, exact1.Month, exact1.Day);
                dateTime3 = new DateTime(year1, exact2.Month, exact2.Day);
                ConditionCompliance conditionCompliance = new ConditionCompliance() { PermitConditionComplianceStatusId = pendingStatusId, PermitConditionId = permitCondition.Id, ComplianceReportingStartDate = dateTime5, ComplianceReportingEndDate = dateTime3, ComplianceReportingDueDate = dateTime2 };
                if (dateTime5 != dateTime2)
                  conditionComplianceList.Add(conditionCompliance);
              }
              break;
            case 3:
              DateTime exact3 = DateTime.ParseExact(ConfigurationManager.AppSettings["SemiAnnual.DueDate1"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture);
              DateTime exact4 = DateTime.ParseExact(ConfigurationManager.AppSettings["SemiAnnual.StartDate1"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture);
              DateTime exact5 = DateTime.ParseExact(ConfigurationManager.AppSettings["SemiAnnual.EndDate1"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture);
              DateTime dateTime6 = DateTime.ParseExact(ConfigurationManager.AppSettings["SemiAnnual.DueDate2"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture).AddYears(1);
              DateTime exact6 = DateTime.ParseExact(ConfigurationManager.AppSettings["SemiAnnual.StartDate2"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture);
              DateTime exact7 = DateTime.ParseExact(ConfigurationManager.AppSettings["SemiAnnual.EndDate2"], "M/d", (IFormatProvider) CultureInfo.InvariantCulture);
              int year2 = DateTime.Now.Year;
              for (int index = 0; index < result; ++index)
              {
                if (index > 0)
                  ++year2;
                dateTime2 = new DateTime(year2, exact3.Month, exact3.Day);
                DateTime dateTime5 = new DateTime(year2, exact4.Month, exact4.Day);
                dateTime3 = new DateTime(year2, exact5.Month, exact5.Day);
                ConditionCompliance conditionCompliance1 = new ConditionCompliance() { PermitConditionComplianceStatusId = pendingStatusId, PermitConditionId = permitCondition.Id, ComplianceReportingStartDate = dateTime5, ComplianceReportingEndDate = dateTime3, ComplianceReportingDueDate = dateTime2 };
                if (dateTime5 < dateTime2)
                  conditionComplianceList.Add(conditionCompliance1);
                dateTime2 = new DateTime(year2 + 1, dateTime6.Month, dateTime6.Day);
                dateTime5 = new DateTime(year2, exact6.Month, exact6.Day);
                dateTime3 = new DateTime(year2, exact7.Month, exact7.Day);
                ConditionCompliance conditionCompliance2 = new ConditionCompliance() { PermitConditionComplianceStatusId = pendingStatusId, PermitConditionId = permitCondition.Id, ComplianceReportingStartDate = dateTime5, ComplianceReportingEndDate = dateTime3, ComplianceReportingDueDate = dateTime2 };
                conditionComplianceList.Add(conditionCompliance2);
              }
              break;
            case 5:
              if (permitCondition.OneTimeReportDays.HasValue)
              {
                dateTime2 = dateTime1.AddDays((double) permitCondition.OneTimeReportDays.Value);
                dateTime3 = dateTime1.AddDays((double) permitCondition.OneTimeReportDays.Value);
                DateTime dateTime5 = dateTime1;
                ConditionCompliance conditionCompliance = new ConditionCompliance() { PermitConditionComplianceStatusId = pendingStatusId, PermitConditionId = permitCondition.Id, ComplianceReportingStartDate = dateTime5, ComplianceReportingEndDate = dateTime3, ComplianceReportingDueDate = dateTime2 };
                conditionComplianceList.Add(conditionCompliance);
                break;
              }
              break;
          }
        }
        foreach (ConditionCompliance entity in conditionComplianceList)
          this._conditionComplianceService.Save(entity);
      }
    }

    public int ClonePermit(PendingPermitForm form, string userName)
    {
      int id = this._permittingService.ClonePermit(form.PermitId, form.PermitTypeId, form.PermitNumber, userName);
      Permit byId = this._permittingService.GetById(id, "LU_PermitStatus,PermitWithdrawalGroundwaters,PermitWithdrawalSurfacewaters,PermitContacts");
      int? pendingPermitTypeId1 = form.PendingPermitTypeId;
      if ((pendingPermitTypeId1.GetValueOrDefault() != 3 ? 0 : (pendingPermitTypeId1.HasValue ? 1 : 0)) != 0)
      {
        byId.PermitStatusId = new int?(26);
        byId.ApplicationTypeId = new int?(2);
      }
      else
      {
        int? pendingPermitTypeId2 = form.PendingPermitTypeId;
        if ((pendingPermitTypeId2.GetValueOrDefault() != 4 ? 0 : (pendingPermitTypeId2.HasValue ? 1 : 0)) != 0)
        {
          byId.PermitStatusId = new int?(35);
          byId.ApplicationTypeId = new int?(3);
        }
      }
      this._permittingService.Save(byId);
      return id;
    }

    private void SavePermitContacts(ApplicationWizard form, int permitId)
    {
      if (form.IsWaterUser && form.IsLandOwner)
      {
        ContactForm form1 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.WaterUser));
        int contactId = this.SaveContact(form1);
        this.SavePermitContact(form1, contactId, permitId, ContactType.WaterUser, true, true);
        this.SavePermitContact(form1, contactId, permitId, ContactType.LandOwner, false, false);
      }
      else if (form.IsWaterUser && !form.IsLandOwner)
      {
        ContactForm form1 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.WaterUser));
        ContactForm form2 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.LandOwner));
        int contactId1 = this.SaveContact(form1);
        int contactId2 = this.SaveContact(form2);
        this.SavePermitContact(form1, contactId1, permitId, ContactType.WaterUser, true, form.PermitIssuedTo == 2 || form.PermitIssuedTo == 4);
        this.SavePermitContact(form2, contactId2, permitId, ContactType.LandOwner, false, form.PermitIssuedTo == 3);
      }
      else if (!form.IsWaterUser && form.IsLandOwner)
      {
        ContactForm form1 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.WaterUser));
        ContactForm form2 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.LandOwner));
        int contactId1 = this.SaveContact(form1);
        int contactId2 = this.SaveContact(form2);
        this.SavePermitContact(form1, contactId1, permitId, ContactType.WaterUser, false, form.PermitIssuedTo == 2 || form.PermitIssuedTo == 4);
        this.SavePermitContact(form2, contactId2, permitId, ContactType.LandOwner, true, form.PermitIssuedTo == 3);
      }
      else
      {
        if (!form.IsConsultant)
          return;
        ContactForm form1 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.WaterUser));
        ContactForm form2 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.LandOwner));
        if (form.PermitIssuedTo == 4)
          form2 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.WaterUser));
        ContactForm form3 = form.Contacts.Single<ContactForm>((Func<ContactForm, bool>) (x => x.ContactType == ContactType.Consultant));
        int contactId1 = this.SaveContact(form1);
        int contactId2 = this.SaveContact(form2);
        int contactId3 = this.SaveContact(form3);
        this.SavePermitContact(form1, contactId1, permitId, ContactType.WaterUser, false, form.PermitIssuedTo == 2 || form.PermitIssuedTo == 4);
        this.SavePermitContact(form2, contactId2, permitId, ContactType.LandOwner, false, form.PermitIssuedTo == 3);
        this.SavePermitContact(form3, contactId3, permitId, ContactType.Consultant, true, false);
      }
    }

    private int SaveContact(ContactForm form)
    {
      if (form == null)
        return 0;
      Contact contact;
      if (form.Id > 0)
      {
        contact = this._contactService.GetById(form.Id, (string) null);
        Mapper.Map<ContactForm, Contact>(form, contact);
      }
      else
        contact = Mapper.Map<ContactForm, Contact>(form);
      form.Id = this._contactService.Save(contact);
      this.SaveCommunicationMethods(form);
      return form.Id;
    }

    private void SaveCommunicationMethods(ContactForm form)
    {
      IEnumerable<ContactCommunicationMethod> range = this._contactCommunicationMethodService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ContactId == @0", new object[1]{ (object) form.Id }) }, (string) null);
      ContactCommunicationMethod entity1 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
      {
        if (x.IsPrimaryEmail.HasValue)
          return x.IsPrimaryEmail.Value;
        return false;
      }));
      if (entity1 != null)
      {
        entity1.CommunicationValue = form.EmailAddress.CommunicationValue;
        this._contactCommunicationMethodService.Save(entity1);
      }
      else
        this._contactCommunicationMethodService.Save(new ContactCommunicationMethod()
        {
          IsPrimaryEmail = new bool?(true),
          ContactId = form.Id,
          CommunicationValue = form.EmailAddress.CommunicationValue,
          CommunicationMethodId = 3
        });
      ContactCommunicationMethod entity2 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
      {
        if (x.IsPrimaryPhone.HasValue)
          return x.IsPrimaryPhone.Value;
        return false;
      }));
      if (entity2 != null)
      {
        entity2.CommunicationValue = form.PrimaryTelephoneNumber.CommunicationValue;
        entity2.CommunicationMethodId = form.PrimaryTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault();
        this._contactCommunicationMethodService.Save(entity2);
      }
      else
        this._contactCommunicationMethodService.Save(new ContactCommunicationMethod()
        {
          IsPrimaryPhone = new bool?(true),
          ContactId = form.Id,
          CommunicationValue = form.PrimaryTelephoneNumber.CommunicationValue,
          CommunicationMethodId = form.PrimaryTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault()
        });
      ContactCommunicationMethod entity3 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
      {
        if (x.IsPrimaryPhone.HasValue)
          return !x.IsPrimaryPhone.Value;
        return false;
      }));
      if (entity3 != null)
      {
        entity3.CommunicationValue = form.AltTelephoneNumber.CommunicationValue;
        entity3.CommunicationMethodId = form.AltTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault();
        this._contactCommunicationMethodService.Save(entity3);
      }
      else
        this._contactCommunicationMethodService.Save(new ContactCommunicationMethod()
        {
          IsPrimaryPhone = new bool?(false),
          ContactId = form.Id,
          CommunicationValue = form.AltTelephoneNumber.CommunicationValue,
          CommunicationMethodId = form.AltTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault()
        });
    }

    private void SavePermitContact(ContactForm form, int contactId, int permitId, ContactType contactType, bool isApplicant, bool isPermittee)
    {
      int permitContactInformationId = this.SavePermitContactInformation(form);
      this._permitContactService.Save(this.CreatePermitContact(contactId, permitId, contactType, permitContactInformationId, isApplicant, isPermittee));
    }

    private int SavePermitContactInformation(ContactForm form)
    {
      if (form == null)
        return 0;
      return this._permitContactInformationService.Save(Mapper.Map<ContactForm, PermitContactInformation>(form));
    }

    private void AddContactFromPermitContactInformation(ApplicationWizard form, PermitContact permitContact, ContactType contactType)
    {
      ContactForm contactForm = permitContact == null || permitContact.PermitContactInformation == null ? Mapper.Map<PermitContact, ContactForm>(new PermitContact()) : Mapper.Map<PermitContact, ContactForm>(permitContact);
      contactForm.ContactType = contactType;
      IEnumerable<ContactCommunicationMethod> range = this._contactCommunicationMethodService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ContactId == @0", new object[1]{ (object) contactForm.Id }) }, (string) null);
      ContactCommunicationMethod communicationMethod1 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
      {
        if (x.IsPrimaryEmail.HasValue)
          return x.IsPrimaryEmail.Value;
        return false;
      }));
      if (communicationMethod1 != null)
        contactForm.EmailAddress = new CommunicationMethodForm()
        {
          CommunicationValue = communicationMethod1.CommunicationValue,
          SelectedCommunicationMethodId = new int?(communicationMethod1.CommunicationMethodId)
        };
      ContactCommunicationMethod communicationMethod2 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
      {
        if (x.IsPrimaryPhone.HasValue)
          return x.IsPrimaryPhone.Value;
        return false;
      }));
      if (communicationMethod2 != null)
        contactForm.PrimaryTelephoneNumber = new CommunicationMethodForm()
        {
          CommunicationValue = communicationMethod2.CommunicationValue,
          SelectedCommunicationMethodId = new int?(communicationMethod2.CommunicationMethodId)
        };
      ContactCommunicationMethod communicationMethod3 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
      {
        if (x.IsPrimaryPhone.HasValue)
          return !x.IsPrimaryPhone.Value;
        return false;
      }));
      if (communicationMethod3 != null)
        contactForm.AltTelephoneNumber = new CommunicationMethodForm()
        {
          CommunicationValue = communicationMethod3.CommunicationValue,
          SelectedCommunicationMethodId = new int?(communicationMethod3.CommunicationMethodId)
        };
      form.Contacts.Add(contactForm);
    }

    private void SaveWaterUsePurposes(ApplicationWizard form, int permitId)
    {
      form.SavedWaterWithdrawalPurpose = new List<PermitWaterWithdrawalPurpose>();
      this._permitWaterWithdrawalPurposeService.DeleteAll(permitId);
      if (form.SelectedWaterUsePurposeIds == null)
        return;
      string[] strArray = form.SelectedWaterUsePurposeIds.Split(',');
      if (strArray.Length <= 0)
        return;
      foreach (string str in strArray)
      {
        if (!string.IsNullOrEmpty(str) && !(str == "undefined"))
        {
          PermitWaterWithdrawalPurpose entity = new PermitWaterWithdrawalPurpose() { PermitId = new int?(permitId), WaterWithdrawalPurposeId = new int?(Convert.ToInt32(str)) };
          int? withdrawalPurposeId = entity.WaterWithdrawalPurposeId;
          if ((withdrawalPurposeId.GetValueOrDefault() != 7 ? 0 : (withdrawalPurposeId.HasValue ? 1 : 0)) != 0)
            entity.TotalIrrigatedAcres = form.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres;
          entity.Id = this._permitWaterWithdrawalPurposeService.Save(entity);
          form.SavedWaterWithdrawalPurpose.Add(entity);
        }
      }
    }

    private void SaveWastewaterTreatmentAndDisposal(ApplicationWizard form)
    {
      if (form.WastewaterTreatmentDisposalForm == null || form.WastewaterTreatmentDisposalForm.Count <= 0)
        return;
      IEnumerable<PermitWaterWithdrawalPurpose> source = form.SavedWaterWithdrawalPurpose.Where<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
      {
        int? withdrawalPurposeId = x.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() > 11)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      if (source == null)
        return;
      foreach (WastewaterTreatmentAndDisposalForm treatmentAndDisposalForm in form.WastewaterTreatmentDisposalForm)
      {
        WastewaterTreatmentAndDisposalForm _form = treatmentAndDisposalForm;
        PermitWastewaterTreatmentDisposal entity = Mapper.Map<WastewaterTreatmentAndDisposalForm, PermitWastewaterTreatmentDisposal>(_form);
        PermitWaterWithdrawalPurpose withdrawalPurpose = source.SingleOrDefault<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (x =>
        {
          int? withdrawalPurposeId1 = x.WaterWithdrawalPurposeId;
          int withdrawalPurposeId2 = _form.WaterWithdrawalPurposeId;
          if (withdrawalPurposeId1.GetValueOrDefault() == withdrawalPurposeId2)
            return withdrawalPurposeId1.HasValue;
          return false;
        }));
        if (withdrawalPurpose != null)
        {
          entity.PermitWaterWithdrawalPurposeId = new int?(withdrawalPurpose.Id);
          this._permitWastewaterTreatmentDisposalService.Save(entity);
        }
      }
    }

    private void GetPermitContactInformation(ApplicationWizard form, int permitId)
    {
      IEnumerable<PermitContact> range = this._permitContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId==" + (object) permitId, new object[0]) }, "PermitContactInformation,Contact.ContactCommunicationMethods");
      if (form.IsWaterUser && form.IsLandOwner)
      {
        PermitContact permitContact = range.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (x => x.IsApplicant));
        this.AddContactFromPermitContactInformation(form, permitContact, ContactType.WaterUser);
        this.AddContactFromPermitContactInformation(form, (PermitContact) null, ContactType.LandOwner);
        this.AddContactFromPermitContactInformation(form, (PermitContact) null, ContactType.Consultant);
      }
      else if (form.IsWaterUser && !form.IsLandOwner || !form.IsWaterUser && form.IsLandOwner)
      {
        PermitContact permitContact1 = range.SingleOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 27 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        }));
        this.AddContactFromPermitContactInformation(form, permitContact1, ContactType.WaterUser);
        PermitContact permitContact2 = range.SingleOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 16 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        }));
        this.AddContactFromPermitContactInformation(form, permitContact2, ContactType.LandOwner);
        this.AddContactFromPermitContactInformation(form, (PermitContact) null, ContactType.Consultant);
      }
      else
      {
        PermitContact permitContact1 = range.SingleOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 27 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        }));
        this.AddContactFromPermitContactInformation(form, permitContact1, ContactType.WaterUser);
        PermitContact permitContact2 = range.SingleOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 16 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        }));
        this.AddContactFromPermitContactInformation(form, permitContact2, ContactType.LandOwner);
        PermitContact permitContact3 = range.SingleOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 8 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        }));
        this.AddContactFromPermitContactInformation(form, permitContact3, ContactType.Consultant);
      }
    }

    private PermitContact CreatePermitContact(int contactId, int permitId, ContactType contactType, int permitContactInformationId, bool isApplicant, bool isPermittee)
    {
      return new PermitContact() { ContactId = contactId, ContactTypeId = new int?((int) contactType), PermitContactInformationId = new int?(permitContactInformationId), PermitId = permitId, IsApplicant = isApplicant, IsPrimary = true, IsPermittee = isPermittee, Active = true };
    }
  }
}
