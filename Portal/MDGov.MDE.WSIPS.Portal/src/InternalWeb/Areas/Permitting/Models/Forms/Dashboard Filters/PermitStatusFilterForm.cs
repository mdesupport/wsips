﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitStatusFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitStatusFilterForm : IDashboardFilter
  {
    [Display(Name = "Status")]
    public int SelectedPermitStatusId { get; set; }

    [XmlIgnore]
    public SelectList PermitStatusList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      dynamicFilterList.Add(new DynamicFilter("PermitStatusId != @0", new object[1]
      {
        (object) 1
      }));
      if (this.SelectedPermitStatusId > 0)
        dynamicFilterList.Add(new DynamicFilter("PermitStatusId == @0", new object[1]
        {
          (object) this.SelectedPermitStatusId
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      this.PermitStatusList = this.GetPermitStatusList();
    }

    private SelectList GetPermitStatusList()
    {
      return new SelectList((IEnumerable) new SelectListItem[1]
      {
        new SelectListItem() { Text = "Display All", Value = "0" }
      }, "Value", "Text");
    }
  }
}
