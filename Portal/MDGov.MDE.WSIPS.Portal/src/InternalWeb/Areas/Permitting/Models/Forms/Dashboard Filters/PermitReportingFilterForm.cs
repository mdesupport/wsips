﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitReportingFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitReportingFilterForm : IDashboardFilter
  {
    [Display(Name = "2 Consecutive Cycles Missed")]
    public bool FailedToReportTwoPeriods { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      return (IEnumerable<DynamicFilter>) new List<DynamicFilter>();
    }

    public void Rehydrate()
    {
    }
  }
}
