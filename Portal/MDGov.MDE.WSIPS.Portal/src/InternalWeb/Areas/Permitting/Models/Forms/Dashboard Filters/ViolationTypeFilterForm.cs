﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.ViolationTypeFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class ViolationTypeFilterForm : IDashboardFilter
  {
    [Display(Name = "Violation Type")]
    public List<int> SelectedViolationTypeIds { get; set; }

    [XmlIgnore]
    public SelectList ViolationTypeList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.SelectedViolationTypeIds != null && this.SelectedViolationTypeIds.Count > 0 && this.SelectedViolationTypeIds.All<int>((Func<int, bool>) (p => p != 0)))
        dynamicFilterList.Add(new DynamicFilter("Violations.Any(ResolutionDate == null && @0.Contains(outerIt.ViolationTypeId))", new object[1]
        {
          (object) this.SelectedViolationTypeIds
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      if (this.SelectedViolationTypeIds == null || this.SelectedViolationTypeIds.Count == 0)
        this.SelectedViolationTypeIds = new List<int>()
        {
          0
        };
      this.ViolationTypeList = this.GetViolationTypeList(ObjectFactory.GetInstance<IService<LU_ViolationType>>());
    }

    private SelectList GetViolationTypeList(IService<LU_ViolationType> violationTypeService)
    {
      List<SelectListItem> list = violationTypeService.GetAll((string) null).OrderBy<LU_ViolationType, string>((Func<LU_ViolationType, string>) (x => x.Description)).Select<LU_ViolationType, SelectListItem>((Func<LU_ViolationType, SelectListItem>) (p => new SelectListItem()
      {
        Text = p.Description,
        Value = p.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
