﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitReviewForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitReviewForm
  {
    public PermitStatusDetailForm PermitStatusDetail { get; set; }

    public CheckListForm CheckListForm { get; set; }

    public int RefId { get; set; }

    public int PermitStatusId { get; set; }

    public string PermitType { get; set; }

    public string HasPermissionToFinalizePermit { get; set; }

    public PermitReviewForm()
    {
      this.PermitStatusDetail = new PermitStatusDetailForm();
      this.CheckListForm = new CheckListForm();
      this.HasPermissionToFinalizePermit = "false";
    }
  }
}
