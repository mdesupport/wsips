﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.EnforcementActionFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class EnforcementActionFilterForm : IDashboardFilter
  {
    [Display(Name = "Response Date Elapsed")]
    public bool HasResponseDateElapsed { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.HasResponseDateElapsed)
        dynamicFilterList.Add(new DynamicFilter("Violations.Any(PermitViolationEnforcements.Any(Enforcement.ResponseReceivedDate == null && Enforcement.ResponseDueDate < @0))", new object[1]
        {
          (object) DateTime.Now
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
    }
  }
}
