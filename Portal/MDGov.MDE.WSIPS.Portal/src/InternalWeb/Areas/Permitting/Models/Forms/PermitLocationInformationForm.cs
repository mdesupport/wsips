﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitLocationInformationForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitLocationInformationForm
  {
    [Display(Name = "Water Management Strategy Area")]
    public bool IsInWaterManagementStrategyArea { get; set; }

    [Display(Name = "Tier II Watershed")]
    public bool IsInTier2Watershed { get; set; }

    [Display(Name = "Aquifer")]
    public int? SelectedAquiferId { get; set; }

    public SelectList AquiferList { get; set; }

    [Display(Name = "Aquifer Test Performed")]
    public bool AquiferTestRequired { get; set; }

    private int? selectedWaterTypeId;
    [Display(Name = "Freshwater/Saltwater")]
    public int? SelectedWaterTypeId 
    {
        get { return selectedWaterTypeId ?? 1; }
        set { selectedWaterTypeId = value; }
    }

    public SelectList WaterTypeList { get; set; }

    private int? selectedTidalTypeId;
    [Display(Name = "Tidal")]
    public int? SelectedTidalTypeId 
    {
        get { return selectedTidalTypeId ?? 1; }
        set { selectedTidalTypeId = value; }
    }

    public SelectList TidalTypeList { get; set; }

    [Display(Name = "Source Description")]
    public string SourceDescription { get; set; }

    [Display(Name = "Location")]
    public string Location { get; set; }

    [Display(Name = "Remarks")]
    public string Remarks { get; set; }

    [Display(Name = "Col")]
    public string ADCXCoord { get; set; }

    [Display(Name = "Row")]
    public string ADCYCoord { get; set; }

    [Display(Name = "ADC Page")]
    public string ADCPageLetterGrid { get; set; }

    [Display(Name = "County Hydro Map")]
    public string CountyHydroGeoMap { get; set; }

    [Display(Name = "USGS Topo Map")]
    public string USGSTopoMap { get; set; }
  }
}
