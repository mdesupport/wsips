﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.AllocationInformationForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class AllocationInformationForm
  {
    public int PermitId { get; set; }

    public GroundwaterAllocationForm RequestedGroundwaterAllocation { get; set; }

    public SurfacewaterAllocationForm RequestedSurfacewaterAllocation { get; set; }

    public GroundwaterAllocationForm ApprovedGroundwaterAllocation { get; set; }

    public SurfacewaterAllocationForm ApprovedSurfacewaterAllocation { get; set; }

    public AllocationInformationForm()
    {
      this.RequestedGroundwaterAllocation = new GroundwaterAllocationForm();
      this.RequestedSurfacewaterAllocation = new SurfacewaterAllocationForm();
      this.ApprovedGroundwaterAllocation = new GroundwaterAllocationForm();
      this.ApprovedSurfacewaterAllocation = new SurfacewaterAllocationForm();
    }
  }
}
