﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.DashboardSearchForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class DashboardSearchForm
  {
    public DashboardMode DisplayMode { get; set; }

    public DashboardPermit SelectedPermit { get; set; }

    [Display(Name = "Filter Template")]
    public FilterTemplate SelectedFilterTemplate { get; set; }

    public SelectList FilterTemplateList { get; set; }

    public DashboardFilterForm FilterForm { get; set; }

    public DashboardSpatialFilterForm SpatialFilterForm { get; set; }

    public void Rehydrate(bool includeCustomFilter)
    {
      List<SelectListItem> selectListItemList = new List<SelectListItem>();
      if (includeCustomFilter)
        selectListItemList.Add(new SelectListItem()
        {
          Text = ((DisplayAttribute) ((IEnumerable<object>) typeof (FilterTemplate).GetField(FilterTemplate.MySavedFilter.ToString()).GetCustomAttributes(typeof (DisplayAttribute), false)).First<object>()).Name,
          Value = FilterTemplate.MySavedFilter.ToString()
        });
      selectListItemList.Add(new SelectListItem()
      {
        Text = ((DisplayAttribute) ((IEnumerable<object>) typeof (FilterTemplate).GetField(FilterTemplate.Permitting.ToString()).GetCustomAttributes(typeof (DisplayAttribute), false)).First<object>()).Name,
        Value = FilterTemplate.Permitting.ToString()
      });
      selectListItemList.Add(new SelectListItem()
      {
        Text = ((DisplayAttribute) ((IEnumerable<object>) typeof (FilterTemplate).GetField(FilterTemplate.Compliance.ToString()).GetCustomAttributes(typeof (DisplayAttribute), false)).First<object>()).Name,
        Value = FilterTemplate.Compliance.ToString()
      });
      this.FilterTemplateList = new SelectList((IEnumerable) selectListItemList, "Value", "Text", (object) this.SelectedFilterTemplate);
      if (this.FilterForm == null)
        return;
      this.FilterForm.Rehydrate();
    }
  }
}
