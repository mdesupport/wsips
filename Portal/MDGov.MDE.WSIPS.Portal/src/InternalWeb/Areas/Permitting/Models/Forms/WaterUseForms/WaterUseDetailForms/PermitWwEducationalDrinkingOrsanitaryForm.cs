﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitWwEducationalDrinkingOrsanitaryForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PermitWwEducationalDrinkingOrsanitaryForm
  {
    public int Id { get; set; }

    public int? UsePercentage { get; set; }

    public int? PermitWaterWithdrawalPurposeId { get; set; }

    public string AverageGallonPerDayFromGroundWater { get; set; }

    public string MaximumGallonPerDayFromGroundWater { get; set; }

    public string AverageGallonPerDayFromSurfaceWater { get; set; }

    public string MaximumGallonPerDayFromSurfaceWater { get; set; }

    [StringLength(500)]
    public string EstimateDescription { get; set; }

    public IEnumerable<SelectListItem> EducationInstituteTypeList { get; set; }

    public int? EducationInstituteTypeId { get; set; }

    public int? NoOfStaff { get; set; }

    public int? NoOfStudents { get; set; }

    public bool IsAthleticFieldIrrigation { get; set; }

    public int? NoOfIrrigatedAcres { get; set; }

    public bool Chillers { get; set; }

    public int? NoOfStaffWorkingMorethan20hrsPerWeek { get; set; }

    public int? NoOfStaffWorkingLessthan20hrsPerWeek { get; set; }

    public int? NoOfStudentsAttendingMorethan20hrsPerWeek { get; set; }

    public int? NoOfStudentsAttendingLessthan20hrsPerWeek { get; set; }
  }
}
