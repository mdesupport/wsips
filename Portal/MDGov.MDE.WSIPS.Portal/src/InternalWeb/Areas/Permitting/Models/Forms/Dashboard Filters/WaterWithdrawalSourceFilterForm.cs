﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.WaterWithdrawalSourceFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class WaterWithdrawalSourceFilterForm : IDashboardFilter
  {
    [Display(Name = "Water Source")]
    public int WaterWithdrawalSourceId { get; set; }

    [XmlIgnore]
    public SelectList WaterWithdrawalSourceList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.WaterWithdrawalSourceId == 1)
        dynamicFilterList.Add(new DynamicFilter("PermitWithdrawalGroundwaters.Count > 0", new object[0]));
      else if (this.WaterWithdrawalSourceId == 2)
        dynamicFilterList.Add(new DynamicFilter("PermitWithdrawalSurfaceWaters.Count > 0", new object[0]));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      this.WaterWithdrawalSourceList = this.GetWaterWithdrawalSourceList(ObjectFactory.GetInstance<IService<LU_WaterWithdrawalSource>>());
    }

    private SelectList GetWaterWithdrawalSourceList(IService<LU_WaterWithdrawalSource> waterWithdrawalSourceService)
    {
      List<SelectListItem> list = waterWithdrawalSourceService.GetAll((string) null).Select<LU_WaterWithdrawalSource, SelectListItem>((Func<LU_WaterWithdrawalSource, SelectListItem>) (t => new SelectListItem()
      {
        Text = t.Description,
        Value = t.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text", (object) this.WaterWithdrawalSourceId);
    }
  }
}
