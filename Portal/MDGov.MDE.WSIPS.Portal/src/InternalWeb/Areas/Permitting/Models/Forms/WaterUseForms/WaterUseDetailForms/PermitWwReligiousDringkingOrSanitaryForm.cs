﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitWwReligiousDringkingOrSanitaryForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PermitWwReligiousDringkingOrSanitaryForm : StandardQuestionsForm
  {
    public int? UsePercentage { get; set; }

    public int? NoOfParishioners { get; set; }

    public int? NoOfServicesPerWeek { get; set; }

    public bool Daycare { get; set; }

    public int? NoOfStaffWorkingMorethan20hrsPerWeek { get; set; }

    public int? NoOfStaffWorkingLessthan20hrsPerWeek { get; set; }

    public int? NoOfStudentsAttendingMorethan20hrsPerWeek { get; set; }

    public int? NoOfStudentsAttendingLessthan20hrsPerWeek { get; set; }
  }
}
