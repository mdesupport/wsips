﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.WaterUseDetailForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class WaterUseDetailForm
  {
    public PermitWwLivestockUseDetailGroupForm PermitWwLivestockForm { get; set; }

    public PermitWwLivestockUseDetailGroupForm OtherLivestockWateringForm { get; set; }

    public PermitWwLivestockUseDetailGroupForm DairyAnimalWateringForm { get; set; }

    public PoultryWateringUseDetailGroupForm PoultryWateringFoggerForm { get; set; }

    public PoultryWateringUseDetailGroupForm PoultryWateringEvaporativeForm { get; set; }

    public CropUseDetailGroupForm CropUseForm { get; set; }

    public PermitWwIrrigationUseDetailGroupForm NurseryStockIrrigationForm { get; set; }

    public PermitWwIrrigationUseDetailGroupForm SodIrrigationForm { get; set; }

    public PermitWwPrivateWaterSupplierGroupForm PrivateWaterSuppilerForm { get; set; }

    public PermitWwNonAgricultureIrrigationForm IrrigationUndefinedForm { get; set; }

    public PermitWwNonAgricultureIrrigationForm LawnAndParkIrrigationForm { get; set; }

    public PermitWwNonAgricultureIrrigationForm SmallIntermitentIrrigationForm { get; set; }

    public PermitWwGolfCourseIrrigationForm GolfCourseIrrigationForm { get; set; }

    public PermitWwCommercialDrinkingOrSanitaryForm PermitWwCommercialDrinkingOrSanitaryForm { get; set; }

    public PermitWwAquacultureAquariumForm PermitWwAquacultureAquariumForm { get; set; }

    public PermitWwConstructionDewateringForm PermitWwConstructionDewateringForm { get; set; }

    public PermitWwEducationalDrinkingOrsanitaryForm PermitWwEducationalDrinkingOrsanitaryForm { get; set; }

    public PermitWwFarmPotableSuppliesForm PermitWwFarmPotableSuppliesForm { get; set; }

    public PermitWwFoodProcessingForm PermitWwFoodProcessingForm { get; set; }

    public PermitWwReligiousDringkingOrSanitaryForm PermitWwReligiousDringkingOrSanitaryForm { get; set; }

    public PermitWwWildlifePondForm PermitWwWildlifePondForm { get; set; }

    public WaterUseDetailForm()
    {
      this.PermitWwLivestockForm = new PermitWwLivestockUseDetailGroupForm();
      this.OtherLivestockWateringForm = new PermitWwLivestockUseDetailGroupForm();
      this.DairyAnimalWateringForm = new PermitWwLivestockUseDetailGroupForm();
      this.PoultryWateringFoggerForm = new PoultryWateringUseDetailGroupForm();
      this.PoultryWateringEvaporativeForm = new PoultryWateringUseDetailGroupForm();
      this.CropUseForm = new CropUseDetailGroupForm();
      this.NurseryStockIrrigationForm = new PermitWwIrrigationUseDetailGroupForm();
      this.SodIrrigationForm = new PermitWwIrrigationUseDetailGroupForm();
      this.PrivateWaterSuppilerForm = new PermitWwPrivateWaterSupplierGroupForm();
      this.IrrigationUndefinedForm = new PermitWwNonAgricultureIrrigationForm();
      this.LawnAndParkIrrigationForm = new PermitWwNonAgricultureIrrigationForm();
      this.SmallIntermitentIrrigationForm = new PermitWwNonAgricultureIrrigationForm();
      this.GolfCourseIrrigationForm = new PermitWwGolfCourseIrrigationForm();
      this.PermitWwCommercialDrinkingOrSanitaryForm = new PermitWwCommercialDrinkingOrSanitaryForm();
      this.PermitWwAquacultureAquariumForm = new PermitWwAquacultureAquariumForm();
      this.PermitWwConstructionDewateringForm = new PermitWwConstructionDewateringForm();
      this.PermitWwEducationalDrinkingOrsanitaryForm = new PermitWwEducationalDrinkingOrsanitaryForm();
      this.PermitWwFarmPotableSuppliesForm = new PermitWwFarmPotableSuppliesForm();
      this.PermitWwFoodProcessingForm = new PermitWwFoodProcessingForm();
      this.PermitWwReligiousDringkingOrSanitaryForm = new PermitWwReligiousDringkingOrSanitaryForm();
      this.PermitWwWildlifePondForm = new PermitWwWildlifePondForm();
    }
  }
}
