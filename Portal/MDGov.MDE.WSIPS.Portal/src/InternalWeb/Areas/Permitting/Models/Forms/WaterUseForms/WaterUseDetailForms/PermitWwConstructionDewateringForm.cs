﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitWwConstructionDewateringForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PermitWwConstructionDewateringForm : StandardQuestionsForm
  {
    public int? UsePercentage { get; set; }

    public int? AreaOfExcavation { get; set; }

    public int? DepthOfExcavation { get; set; }

    public int? NoOfPumps { get; set; }

    public int? CapacityOfPumps { get; set; }

    public int? HoursOfOperationPerDay { get; set; }

    public IEnumerable<SelectListItem> WaterRemovalExcavationTypeList { get; set; }

    public int? WaterRemovalExcavationTypeId { get; set; }
  }
}
