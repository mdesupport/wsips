﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.WaterUsageFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class WaterUsageFilterForm : IDashboardFilter
  {
    [Display(Name = "Average Gallons Per/Day Greater Than")]
    public int? WaterUsageAmount { get; set; }

    [Display(Name = "% Overuse of Water by Annual Average")]
    public double? AnnualAverageOverusePercent { get; set; }

    [Display(Name = "% Overuse of Water by Monthly Max")]
    public double? MonthlyMaxOverusePercent { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.WaterUsageAmount.HasValue)
        dynamicFilterList.Add(new DynamicFilter("(PermitWithdrawalGroundwaters.Any() ? PermitWithdrawalGroundwaters.Sum(AvgGalPerDay) : 0) + (PermitWithdrawalSurfacewaters.Any() ? PermitWithdrawalSurfacewaters.Sum(AvgGalPerDay) : 0) > @0", new object[1]
        {
          (object) this.WaterUsageAmount.Value
        }));
      if (this.AnnualAverageOverusePercent.HasValue)
        dynamicFilterList.Add(new DynamicFilter("PermitConditions.Any(ConditionCompliances.Any(ComplianceReportingEndDate.Year == @1 && AnnualAverageOverusePercentage > @0))", new object[2]
        {
          (object) this.AnnualAverageOverusePercent.Value,
          (object) DateTime.Today.AddYears(-1).Year
        }));
      if (this.MonthlyMaxOverusePercent.HasValue)
        dynamicFilterList.Add(new DynamicFilter("PermitConditions.Any(ConditionCompliances.Any(ComplianceReportingEndDate.Year == @1 && MonthlyMaximumOverusePercentage > @0))", new object[2]
        {
          (object) this.MonthlyMaxOverusePercent.Value,
          (object) DateTime.Today.AddYears(-1).Year
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
    }
  }
}
