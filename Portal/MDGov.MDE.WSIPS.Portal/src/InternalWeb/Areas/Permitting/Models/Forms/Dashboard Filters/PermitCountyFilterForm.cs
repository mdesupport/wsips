﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitCountyFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitCountyFilterForm : IDashboardFilter
  {
    [Display(Name = "County")]
    public List<int> SelectedPermitCountyIds { get; set; }

    [XmlIgnore]
    public SelectList CountyList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.SelectedPermitCountyIds != null && this.SelectedPermitCountyIds.Count > 0 && this.SelectedPermitCountyIds.All<int>((Func<int, bool>) (p => p != 0)))
        dynamicFilterList.Add(new DynamicFilter("PermitCounties.Any(@0.Contains(outerIt.CountyId))", new object[1]
        {
          (object) this.SelectedPermitCountyIds
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      if (this.SelectedPermitCountyIds == null || this.SelectedPermitCountyIds.Count == 0)
        this.SelectedPermitCountyIds = new List<int>() { 0 };
      this.CountyList = this.GetCountyList(ObjectFactory.GetInstance<IService<LU_County>>());
    }

    private SelectList GetCountyList(IService<LU_County> countyService)
    {
      List<SelectListItem> list = countyService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active", new object[0])
      }, (string) null).Select<LU_County, SelectListItem>((Func<LU_County, SelectListItem>) (t => new SelectListItem()
      {
        Text = t.Description,
        Value = t.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
