﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.ContactsTabForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class ContactsTabForm
  {
    public int PermitId { get; set; }

    [Display(Name = "Division Chief")]
    public int? SelectedDivisionChiefId { get; set; }

    public SelectList DivisionChiefList { get; set; }

    [Display(Name = "Supervisor")]
    public int? SelectedSupervisorId { get; set; }

    public SelectList SupervisorList { get; set; }

    [Display(Name = "Project Manager")]
    public int? SelectedProjectManagerId { get; set; }

    public SelectList ProjectManagerList { get; set; }

    [Display(Name = "Compliance Manager")]
    public int? SelectedComplianceManagerId { get; set; }

    public SelectList ComplianceManagerList { get; set; }

    [Display(Name = "County Official")]
    public int? SelectedCountyOfficialId { get; set; }

    public SelectList CountyOfficialList { get; set; }

    [Display(Name = "Department of Natural Resources")]
    public int? SelectedDNROfficialId { get; set; }

    public SelectList DNROfficialList { get; set; }
  }
}
