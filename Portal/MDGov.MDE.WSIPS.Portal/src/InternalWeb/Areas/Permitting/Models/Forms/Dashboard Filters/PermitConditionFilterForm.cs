﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitConditionFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitConditionFilterForm : IDashboardFilter
  {
    [Display(Name = "Standard Condition Type")]
    public List<int> SelectedStandardConditionTypeIds { get; set; }

    [XmlIgnore]
    public SelectList StandardConditionTypeList { get; set; }

    [Display(Name = "Number of Days Overdue Greater Than")]
    public int? DaysOverdue { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.SelectedStandardConditionTypeIds != null && this.SelectedStandardConditionTypeIds.Count > 0 && this.SelectedStandardConditionTypeIds.All<int>((Func<int, bool>) (p => p != 0)))
      {
        if (this.DaysOverdue.HasValue)
          dynamicFilterList.Add(new DynamicFilter("PermitConditions.Any(@2.Contains(outerIt.StandardConditionTypeId) && ConditionCompliances.Any(ComplianceReportingDueDate.Year == @0 && ComplianceReportingDueDate < @1 && (PermitConditionComplianceStatusId == 3 || PermitConditionComplianceStatusId == 4)))", new object[3]
          {
            (object) DateTime.Today.Year,
            (object) DateTime.Now.AddDays((double) -this.DaysOverdue.Value),
            (object) this.SelectedStandardConditionTypeIds
          }));
        else
          dynamicFilterList.Add(new DynamicFilter("PermitConditions.Any(@0.Contains(outerIt.StandardConditionTypeId))", new object[1]
          {
            (object) this.SelectedStandardConditionTypeIds
          }));
      }
      else if (this.DaysOverdue.HasValue)
        dynamicFilterList.Add(new DynamicFilter("PermitConditions.Any(ConditionCompliances.Any(ComplianceReportingDueDate.Year == @0 && ComplianceReportingDueDate < @1 && (PermitConditionComplianceStatusId == 3 || PermitConditionComplianceStatusId == 4)))", new object[2]
        {
          (object) DateTime.Today.Year,
          (object) DateTime.Now.AddDays((double) -this.DaysOverdue.Value)
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      if (this.SelectedStandardConditionTypeIds == null || this.SelectedStandardConditionTypeIds.Count == 0)
        this.SelectedStandardConditionTypeIds = new List<int>()
        {
          0
        };
      this.StandardConditionTypeList = this.GetStandardConditionTypeList(ObjectFactory.GetInstance<IService<LU_StandardConditionType>>());
    }

    private SelectList GetStandardConditionTypeList(IService<LU_StandardConditionType> standardConditionTypeService)
    {
      List<SelectListItem> list = standardConditionTypeService.GetRange(0, int.MaxValue, "Description", (IEnumerable<DynamicFilter>) null, (string) null).Select<LU_StandardConditionType, SelectListItem>((Func<LU_StandardConditionType, SelectListItem>) (t => new SelectListItem()
      {
        Text = t.Description,
        Value = t.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
