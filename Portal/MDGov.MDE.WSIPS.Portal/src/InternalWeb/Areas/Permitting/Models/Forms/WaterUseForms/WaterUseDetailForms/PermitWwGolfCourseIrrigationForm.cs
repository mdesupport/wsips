﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitWwGolfCourseIrrigationForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PermitWwGolfCourseIrrigationForm : StandardQuestionsForm
  {
    public int? UsePercentage { get; set; }

    public Decimal? NoOfIrrigatedAcres_teesOrGreens { get; set; }

    public Decimal? NoOfIrrigatedAcres_fairways { get; set; }

    public string TypeOfGrass_tees_greens { get; set; }

    public string TypeOfGrass_fairways { get; set; }

    public IEnumerable<SelectListItem> GolfIrrigationSystemTypeList { get; set; }

    public int? GolfIrrigationSystemTypeId { get; set; }

    public IEnumerable<SelectListItem> IrrigationLayoutTypeList { get; set; }

    public int? IrrigationLayoutTypeId { get; set; }

    public bool IsWellwaterPumpedIntoPond { get; set; }
  }
}
