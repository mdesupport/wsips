﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.EnforcementForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class EnforcementForm
  {
    public int Id { get; set; }

    [Required(ErrorMessage = "Action is required")]
    public int EnforcementActionId { get; set; }

    public string ActionName { get; set; }

    public string Comments { get; set; }

    [Required(ErrorMessage = "Action Date is required")]
    public DateTime ActionDate { get; set; }

    public bool ResponseRequired { get; set; }

    [RequiredIf("ResponseRequired", new object[] {true}, ErrorMessage = "Response Due Date is Required")]
    public DateTime? ResponseDueDate { get; set; }

    public DateTime? ResponseReceivedDate { get; set; }

    [Required(ErrorMessage = "The action must be assigned")]
    public int ContactId { get; set; }

    public DateTime CreatedDate { get; set; }

    [Required(ErrorMessage = "Please select applicable violation(s)")]
    public List<int> ViolationIds { get; set; }

    public bool IsEditDeleteAllowed { get; set; }
  }
}
