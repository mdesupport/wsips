﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitExpirationDateFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitExpirationDateFilterForm : IDashboardFilter
  {
    [Display(Name = "Start Date")]
    public DateTime? StartDate { get; set; }

    [Display(Name = "End Date")]
    public DateTime? EndDate { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.StartDate.HasValue)
      {
        if (this.EndDate.HasValue)
          dynamicFilterList.Add(new DynamicFilter("@0 <= ExpirationDate && ExpirationDate < @1", new object[2]
          {
            (object) this.StartDate.Value.Date,
            (object) this.EndDate.Value.AddDays(1.0).Date
          }));
        else
          dynamicFilterList.Add(new DynamicFilter("@0 <= ExpirationDate", new object[1]
          {
            (object) this.StartDate.Value.Date
          }));
      }
      else if (this.EndDate.HasValue)
        dynamicFilterList.Add(new DynamicFilter("ExpirationDate < @0", new object[1]
        {
          (object) this.EndDate.Value.AddDays(1.0).Date
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
    }
  }
}
