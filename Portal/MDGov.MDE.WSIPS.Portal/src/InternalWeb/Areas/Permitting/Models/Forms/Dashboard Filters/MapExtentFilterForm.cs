﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.MapExtentFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Attributes;
using System;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [SpatialFilter]
  public class MapExtentFilterForm
  {
    public Decimal? XMin { get; set; }

    public Decimal? YMin { get; set; }

    public Decimal? XMax { get; set; }

    public Decimal? YMax { get; set; }

    public int? SRID { get; set; }

    public bool IsApplied { get; set; }

    public MapExtentFilter BuildFilter()
    {
      if (!this.IsApplied)
        return (MapExtentFilter) null;
      return new MapExtentFilter()
      {
        XMin = this.XMin.Value,
        YMin = this.YMin.Value,
        XMax = this.XMax.Value,
        YMax = this.YMax.Value,
        SRID = this.SRID.Value
      };
    }
  }
}
