﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.LivestockForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class LivestockForm
  {
    public int Id { get; set; }

    public int PermitWaterWithdrawalPurposeId { get; set; }

    [StringLength(50)]
    [DataType(DataType.Text)]
    public string Livestock { get; set; }

    [RegularExpression("^\\d+$", ErrorMessage = "Please enter a number.")]
    public int? NumberOfLivestock { get; set; }

    public IEnumerable<SelectListItem> TypeOfLivestockList { get; set; }

    public bool HasDeleted { get; set; }
  }
}
