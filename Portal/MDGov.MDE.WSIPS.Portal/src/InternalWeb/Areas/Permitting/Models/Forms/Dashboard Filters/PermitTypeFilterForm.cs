﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitTypeFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitTypeFilterForm : IDashboardFilter
  {
    [Display(Name = "Type")]
    public List<int> SelectedPermitTypeIds { get; set; }

    [XmlIgnore]
    public SelectList PermitTypeList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.SelectedPermitTypeIds != null && this.SelectedPermitTypeIds.Count > 0 && this.SelectedPermitTypeIds.All<int>((Func<int, bool>) (p => p != 0)))
        dynamicFilterList.Add(new DynamicFilter("@0.Contains(outerIt.PermitTypeId)", new object[1]
        {
          (object) this.SelectedPermitTypeIds
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      if (this.SelectedPermitTypeIds == null || this.SelectedPermitTypeIds.Count == 0)
        this.SelectedPermitTypeIds = new List<int>() { 0 };
      this.PermitTypeList = this.GetPermitTypeList(ObjectFactory.GetInstance<IService<LU_PermitType>>());
    }

    private SelectList GetPermitTypeList(IService<LU_PermitType> permitTypeService)
    {
      List<SelectListItem> list = permitTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active", new object[0])
      }, (string) null).Select<LU_PermitType, SelectListItem>((Func<LU_PermitType, SelectListItem>) (t => new SelectListItem()
      {
        Text = t.Description,
        Value = t.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
