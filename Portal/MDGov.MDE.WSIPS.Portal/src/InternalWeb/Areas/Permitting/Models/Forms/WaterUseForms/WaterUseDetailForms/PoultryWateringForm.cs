﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PoultryWateringForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PoultryWateringForm
  {
    public int Id { get; set; }

    public int PermitWaterWithdrawalPurposeId { get; set; }

    public IEnumerable<SelectListItem> PoultryCoolingSystemList { get; set; }

    public int? PoultryCoolingSystemId { get; set; }

    public IEnumerable<SelectListItem> PoultryTypeList { get; set; }

    public int? PoultryId { get; set; }

    public int? NumberOfHouses { get; set; }

    public int? NumberOfFlocksPerYear { get; set; }

    public int? BirdsPerFlock { get; set; }

    public int NumberOfDaysPerYearFoggersUsed { get; set; }

    public bool HasDeleted { get; set; }
  }
}
