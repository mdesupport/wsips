﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.WastewaterTreatmentAndDisposalForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class WastewaterTreatmentAndDisposalForm
  {
    public int Id { get; set; }

    public string Title { get; set; }

    public bool IsWastewaterDischarge { get; set; }

    public IEnumerable<SelectListItem> WaterDispersementTypeList { get; set; }

    public int? WaterDispersementTypeId { get; set; }

    public string GroundwaterDischargeDiscription { get; set; }

    public string PermitDischargeNumber { get; set; }

    public int WaterWithdrawalPurposeId { get; set; }
  }
}
