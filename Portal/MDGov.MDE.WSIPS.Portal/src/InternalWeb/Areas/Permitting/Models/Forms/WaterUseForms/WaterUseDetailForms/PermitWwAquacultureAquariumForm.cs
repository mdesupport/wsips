﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitWwAquacultureAquariumForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System.ComponentModel.DataAnnotations;
using System.Web.Http.ModelBinding;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PermitWwAquacultureAquariumForm
  {
    public int? UsePercentage { get; set; }

    public bool IsRecycledWater { get; set; }

    public int Id { get; set; }

    public int? PermitWaterWithdrawalPurposeId { get; set; }

    public string AverageGallonPerDayFromGroundWater { get; set; }

    public string MaximumGallonPerDayFromGroundWater { get; set; }

    public string AverageGallonPerDayFromSurfaceWater { get; set; }

    public string MaximumGallonPerDayFromSurfaceWater { get; set; }

    [StringLength(500)]
    public string EstimateDescription { get; set; }
  }
}
