﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.DashboardFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Attributes;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using StructureMap;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class DashboardFilterForm
  {
    public FilterTemplate SelectedFilterTemplate { get; set; }

    [Display(Name = "Only those associated with me")]
    public bool? AssociatedWithMe { get; set; }

    [Display(Name = "Only the most recent version")]
    public bool? MostRecentVersion { get; set; }

    public PermitCategoryFilterForm PermitCategoryFilter { get; set; }

    public PermitStatusFilterForm PermitStatusFilter { get; set; }

    public PermitTypeFilterForm PermitTypeFilter { get; set; }

    public PermitComplianceStatusFilterForm PermitComplianceStatusFilter { get; set; }

    public PermitConditionFilterForm PermitConditionFilter { get; set; }

    public ViolationTypeFilterForm PermitViolationTypeFilter { get; set; }

    public EnforcementActionFilterForm EnforcementActionFilter { get; set; }

    public PermitReportingFilterForm PermitReportingFilter { get; set; }

    public WaterUsageFilterForm WaterUsageFilter { get; set; }

    public WaterWithdrawalPurposeCategoryFilterForm WaterWithdrawalPurposeCategoryFilter { get; set; }

    public PermitCountyFilterForm PermitCountyFilter { get; set; }

    public AquiferFilterForm AquiferFilter { get; set; }

    public WaterWithdrawalSourceFilterForm WaterWithdrawalSourceFilter { get; set; }

    public PermitIssuanceDateFilterForm PermitIssuanceDateFilter { get; set; }

    public PermitExpirationDateFilterForm PermitExpirationDateFilter { get; set; }

    public DashboardFilterForm()
    {
    }

    public DashboardFilterForm(FilterTemplate filterTemplate)
    {
      this.SelectedFilterTemplate = filterTemplate;
      switch (filterTemplate)
      {
        case FilterTemplate.Permitting:
        case FilterTemplate.Analysis:
          this.AssociatedWithMe = new bool?(false);
          this.PermitCategoryFilter = ObjectFactory.GetInstance<PermitCategoryFilterForm>();
          this.PermitStatusFilter = ObjectFactory.GetInstance<PermitStatusFilterForm>();
          this.PermitTypeFilter = ObjectFactory.GetInstance<PermitTypeFilterForm>();
          this.WaterWithdrawalPurposeCategoryFilter = ObjectFactory.GetInstance<WaterWithdrawalPurposeCategoryFilterForm>();
          this.PermitCountyFilter = ObjectFactory.GetInstance<PermitCountyFilterForm>();
          this.AquiferFilter = ObjectFactory.GetInstance<AquiferFilterForm>();
          this.WaterWithdrawalSourceFilter = ObjectFactory.GetInstance<WaterWithdrawalSourceFilterForm>();
          this.PermitIssuanceDateFilter = ObjectFactory.GetInstance<PermitIssuanceDateFilterForm>();
          this.PermitExpirationDateFilter = ObjectFactory.GetInstance<PermitExpirationDateFilterForm>();
          break;
        case FilterTemplate.Compliance:
          this.PermitCategoryFilter = ObjectFactory.GetInstance<PermitCategoryFilterForm>();
          this.PermitCategoryFilter.SelectedPermitCategoryIds = new List<int>()
          {
            6
          };
          this.PermitStatusFilter = ObjectFactory.GetInstance<PermitStatusFilterForm>();
          this.PermitStatusFilter.SelectedPermitStatusId = 46;
          this.PermitTypeFilter = ObjectFactory.GetInstance<PermitTypeFilterForm>();
          this.PermitComplianceStatusFilter = ObjectFactory.GetInstance<PermitComplianceStatusFilterForm>();
          this.PermitConditionFilter = ObjectFactory.GetInstance<PermitConditionFilterForm>();
          this.PermitViolationTypeFilter = ObjectFactory.GetInstance<ViolationTypeFilterForm>();
          this.EnforcementActionFilter = ObjectFactory.GetInstance<EnforcementActionFilterForm>();
          this.PermitReportingFilter = ObjectFactory.GetInstance<PermitReportingFilterForm>();
          this.WaterUsageFilter = ObjectFactory.GetInstance<WaterUsageFilterForm>();
          this.WaterWithdrawalPurposeCategoryFilter = ObjectFactory.GetInstance<WaterWithdrawalPurposeCategoryFilterForm>();
          this.PermitCountyFilter = ObjectFactory.GetInstance<PermitCountyFilterForm>();
          this.PermitIssuanceDateFilter = ObjectFactory.GetInstance<PermitIssuanceDateFilterForm>();
          this.PermitExpirationDateFilter = ObjectFactory.GetInstance<PermitExpirationDateFilterForm>();
          break;
      }
    }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.AssociatedWithMe.HasValue && this.AssociatedWithMe.Value)
      {
        int sessionVar = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);
        dynamicFilterList.Add(new DynamicFilter("PermitContacts.Any(ContactId==@0 && IsPrimary)", new object[1]
        {
          (object) sessionVar
        }));
      }
      foreach (PropertyInfo propertyInfo in ((IEnumerable<PropertyInfo>) this.GetType().GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>) (prop =>
      {
        if (prop.PropertyType.GetInterface(typeof (IDashboardFilter).Name, false) != (Type) null)
          return !((IEnumerable<object>) prop.PropertyType.GetCustomAttributes(typeof (SpatialFilterAttribute), false)).Any<object>();
        return false;
      })))
      {
        object obj = propertyInfo.GetValue((object) this);
        if (obj != null)
        {
          IEnumerable<DynamicFilter> dynamicFilters = ((IDashboardFilter) obj).BuildFilter();
          if (dynamicFilters.Any<DynamicFilter>())
            dynamicFilterList.AddRange(dynamicFilters);
        }
      }
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      foreach (PropertyInfo propertyInfo in ((IEnumerable<PropertyInfo>) this.GetType().GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>) (prop => prop.PropertyType.GetInterface(typeof (IDashboardFilter).Name, false) != (Type) null)))
      {
        object obj = propertyInfo.GetValue((object) this);
        if (obj != null)
          ((IDashboardFilter) obj).Rehydrate();
      }
    }
  }
}
