﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.ContactForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class ContactForm
  {
    [Display(Name = "Contact Id")]
    public int Id { get; set; }

    public int? UserId { get; set; }

    public bool Active { get; set; }

    public bool IsPrimary { get; set; }

    public bool IsBusiness { get; set; }

    public ContactType ContactType { get; set; }

    [StringLength(40)]
    [Display(Name = "First Name")]
    [RequiredIf("IsBusiness", new object[] {false}, ErrorMessage = "First Name is required")]
    public string FirstName { get; set; }

    [Display(Name = "Middle Name or Initial ")]
    [StringLength(40)]
    public string MiddleInitial { get; set; }

    [RequiredIf("IsBusiness", new object[] {false}, ErrorMessage = "Last Name is required")]
    [Display(Name = "Last Name")]
    [StringLength(50)]
    public string LastName { get; set; }

    /*[StringLength(100)]
    [Display(Name = "Secondary Name")]
    public string SecondaryName { get; set; }*/

    [RequiredIf("IsBusiness", new object[] {true}, ErrorMessage = "Business Name is required")]
    [StringLength(120)]
    [Display(Name = "Business Name")]
    public string BusinessName { get; set; }

    [Required(ErrorMessage = "Address Line1 is required")]
    [Display(Name = "Address (Line 1)")]
    [StringLength(100)]
    public string Address1 { get; set; }

    [Display(Name = "Address (Line 2)")]
    [StringLength(100)]
    public string Address2 { get; set; }

    [Required(ErrorMessage = "City is required")]
    [StringLength(50)]
    [Display(Name = "City")]
    public string City { get; set; }

    [Display(Name = "State")]
    [Required(ErrorMessage = "State is required")]
    public int? StateId { get; set; }

    public IEnumerable<SelectListItem> StateList { get; set; }

    [Display(Name = "Zip Code")]
    [DataType(DataType.PostalCode)]
    [StringLength(10)]
    [Required(ErrorMessage = "Zip Code is required")]
    public string ZipCode { get; set; }

    [Display(Name = "Primary Telephone No")]
    [RequiredIf("ContactType", new object[] {ContactType.CarbonCopy, ContactType.InterestedParty}, ComparisonType = ComparisonType.NotEqual, ErrorMessage = "Primary telephone number and type are required")]
    public CommunicationMethodForm PrimaryTelephoneNumber { get; set; }

    [Display(Name = "Alt Telephone No")]
    public CommunicationMethodForm AltTelephoneNumber { get; set; }

    [Display(Name = "Email Address")]
    public CommunicationMethodForm EmailAddress { get; set; }

    public IEnumerable<SelectListItem> CommunicationMethodList { get; set; }

    public ContactForm()
    {
      this.PrimaryTelephoneNumber = new CommunicationMethodForm();
      this.AltTelephoneNumber = new CommunicationMethodForm();
      this.EmailAddress = new CommunicationMethodForm();
    }
  }
}
