﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.AquiferFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class AquiferFilterForm : IDashboardFilter
  {
    [Display(Name = "Aquifer")]
    public List<int> SelectedAquiferIds { get; set; }

    [XmlIgnore]
    public SelectList AquiferList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.SelectedAquiferIds != null && this.SelectedAquiferIds.Count<int>() > 0 && this.SelectedAquiferIds.All<int>((Func<int, bool>) (p => p != 0)))
        dynamicFilterList.Add(new DynamicFilter("@0.Contains(outerIt.AquiferId)", new object[1]
        {
          (object) this.SelectedAquiferIds
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      if (this.SelectedAquiferIds == null || this.SelectedAquiferIds.Count == 0)
        this.SelectedAquiferIds = new List<int>() { 0 };
      this.AquiferList = this.GetAquiferList(ObjectFactory.GetInstance<IService<LU_Aquifer>>());
    }

    private SelectList GetAquiferList(IService<LU_Aquifer> aquiferService)
    {
      List<SelectListItem> list = aquiferService.GetAll((string) null).OrderBy<LU_Aquifer, string>((Func<LU_Aquifer, string>) (x => x.Description)).Select<LU_Aquifer, SelectListItem>((Func<LU_Aquifer, SelectListItem>) (t => new SelectListItem()
      {
        Text = t.Description,
        Value = t.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
