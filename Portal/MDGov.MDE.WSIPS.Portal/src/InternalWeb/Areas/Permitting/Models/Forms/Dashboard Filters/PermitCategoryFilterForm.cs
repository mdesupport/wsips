﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitCategoryFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitCategoryFilterForm : IDashboardFilter
  {
    [Display(Name = "Category")]
    public List<int> SelectedPermitCategoryIds { get; set; }

    [XmlIgnore]
    public SelectList PermitCategoryList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.SelectedPermitCategoryIds != null && this.SelectedPermitCategoryIds.Count > 0 && this.SelectedPermitCategoryIds.All<int>((Func<int, bool>) (p => p != 0)))
        dynamicFilterList.Add(new DynamicFilter("@0.Contains(outerIt.LU_PermitStatus.PermitCategoryId)", new object[1]
        {
          (object) this.SelectedPermitCategoryIds
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      if (this.SelectedPermitCategoryIds == null || this.SelectedPermitCategoryIds.Count == 0)
        this.SelectedPermitCategoryIds = new List<int>()
        {
          0
        };
      this.PermitCategoryList = this.GetPermitCategoryList(ObjectFactory.GetInstance<IService<LU_PermitCategory>>());
    }

    private SelectList GetPermitCategoryList(IService<LU_PermitCategory> permitCategoryService)
    {
      List<SelectListItem> list = permitCategoryService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) null, (string) null).Select<LU_PermitCategory, SelectListItem>((Func<LU_PermitCategory, SelectListItem>) (t => new SelectListItem()
      {
        Text = t.Description,
        Value = t.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
