﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitParcelForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitParcelForm
  {
    [Display(Name = "Parcel Id")]
    public string ParcelId { get; set; }

    [Display(Name = "Tax Map")]
    public string TaxMapNumber { get; set; }

    [Display(Name = "Grid")]
    public string TaxMapBlockNumber { get; set; }

    [Display(Name = "Parcel")]
    public string TaxMapParcel { get; set; }

    [Display(Name = "Block")]
    public string TaxMapSubBlockNumber { get; set; }

    [Display(Name = "Lot")]
    public string LotNumber { get; set; }
  }
}
