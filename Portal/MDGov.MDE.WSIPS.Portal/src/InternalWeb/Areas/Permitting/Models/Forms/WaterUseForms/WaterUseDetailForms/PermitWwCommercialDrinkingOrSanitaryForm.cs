﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitWwCommercialDrinkingOrSanitaryForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PermitWwCommercialDrinkingOrSanitaryForm : StandardQuestionsForm
  {
    public int? UsePercentage { get; set; }

    public int? NoOfLotsOrUnits { get; set; }

    public IEnumerable<SelectListItem> FacilityList { get; set; }

    public int? TypeOfFacilityId { get; set; }

    public Decimal? AreaOrSquareFootage { get; set; }

    public int? NoOfEmployees { get; set; }

    public int? AvgNumberOfCustomersPerDay { get; set; }

    public IEnumerable<SelectListItem> RestaurantTypeList { get; set; }

    public int? RestaurantTypeId { get; set; }

    public int? NoOfSeats { get; set; }

    public int? NoOfSlips { get; set; }
  }
}
