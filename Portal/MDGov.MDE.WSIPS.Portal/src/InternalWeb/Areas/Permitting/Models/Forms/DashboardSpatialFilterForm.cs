﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.DashboardSpatialFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class DashboardSpatialFilterForm
  {
    public MapExtentFilterForm MapExtentFilter { get; set; }

    public PermitDistanceFilterForm PermitDistanceFilter { get; set; }

    public PermitFeatureFilterForm PermitFeatureFilter { get; set; }

    public DashboardSpatialFilters BuildFilter()
    {
      DashboardSpatialFilters dashboardSpatialFilters = new DashboardSpatialFilters();
      if (this.MapExtentFilter != null && this.MapExtentFilter.IsApplied)
        dashboardSpatialFilters.MapExtentFilter = this.MapExtentFilter.BuildFilter();
      if (this.PermitDistanceFilter != null && this.PermitDistanceFilter.IsApplied)
        dashboardSpatialFilters.PermitDistanceFilter = this.PermitDistanceFilter.BuildFilter();
      if (this.PermitFeatureFilter != null && this.PermitFeatureFilter.IsApplied)
        dashboardSpatialFilters.PermitFeatureFilter = this.PermitFeatureFilter.BuildFilter();
      return dashboardSpatialFilters;
    }
  }
}
