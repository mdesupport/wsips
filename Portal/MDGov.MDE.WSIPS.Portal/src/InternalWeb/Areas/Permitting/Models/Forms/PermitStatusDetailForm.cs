﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitStatusDetailForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitStatusDetailForm
  {
    public int RefId { get; set; }

    public int PermitStatusId { get; set; }

    public int? PreviousPermitStatusId { get; set; }

    public IEnumerable<LU_PermitStatus> PermitStatusNav { get; set; }

    public int PermitCategoryId { get; set; }

    public string PermitStatusLastChangedBy { get; set; }

    public DateTime PermitStatusLastChangedDate { get; set; }

    public string PermitStatusDescription { get; set; }

    public bool HasPendingPermit { get; set; }

    public bool HasPermissionToWithdrawActivateReinstate { get; set; }
  }
}
