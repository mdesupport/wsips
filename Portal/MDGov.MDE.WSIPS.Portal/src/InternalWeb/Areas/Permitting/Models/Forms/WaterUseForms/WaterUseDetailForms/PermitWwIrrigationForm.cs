﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitWwIrrigationForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.ModelBinders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [ModelBinder(typeof (RemoveCommaModelBinder))]
  public class PermitWwIrrigationForm
  {
    public int Id { get; set; }

    public string Title { get; set; }

    public int PermitWaterWithdrawalPurposeId { get; set; }

    [StringLength(500)]
    public string PurposeDescription { get; set; }

    [Required]
    public Decimal? IrrigatedAcres { get; set; }

    [Required]
    public int? IrrigationSystemTypeId { get; set; }

    public IEnumerable<SelectListItem> IrrigationSystemTypeList { get; set; }

    [StringLength(500)]
    public string IrrigationSystemDescription { get; set; }

    public IrrigationType IrrigationType { get; set; }

    [Required]
    public int? AverageGallonPerDayFromGroundWater { get; set; }

    [Required]
    public int? MaximumGallonPerDayFromGroundWater { get; set; }

    [Required]
    public int? AverageGallonPerDayFromSurfaceWater { get; set; }

    [Required]
    public int? MaximumGallonPerDayFromSurfaceWater { get; set; }

    [StringLength(500)]
    public string EstimateDescription { get; set; }

    public bool HasDeleted { get; set; }

    public int? NoOfLotsOrResidentialUnits { get; set; }
  }
}
