﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.CheckListForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class CheckListForm
  {
    public int RefId { get; set; }

    public int PermitStatusId { get; set; }

    public string CheckListCompleted { get; set; }

    public string CompletedByAndDate { get; set; }

    public string CountySignOffBy { get; set; }

    public DateTime? CountySignOffDate { get; set; }

    public bool? CountySignOffYesNo { get; set; }

    public IEnumerable<LU_CheckListAnswer> AnswerOptions { get; set; }

    public IEnumerable<CheckListQuestion> ChecklistQuestions { get; set; }

    public IEnumerable<CheckListAnswer> ChecklistAnswers { get; set; }
  }
}
