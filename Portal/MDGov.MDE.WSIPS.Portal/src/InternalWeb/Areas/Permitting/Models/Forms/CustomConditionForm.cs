﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.CustomConditionForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class CustomConditionForm
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public int CurrentPermitStatusId { get; set; }

    [DisplayName("Condition Text")]
    [Required(ErrorMessage = "Condition Text is required")]
    public string ConditionText { get; set; }

    public string Sequence { get; set; }

    public bool RequiresSelfReporting { get; set; }

    [DisplayName("Condition Category")]
    [Required(ErrorMessage = "Condition Category is required")]
    public int StandardConditionTypeId { get; set; }

    public string Category { get; set; }

    public int? PumpageReportTypeId { get; set; }

    [RequiredIf("RequiresSelfReporting", new object[] {true}, ErrorMessage = "Reporting Frequency is required")]
    public int? ConditionReportingPeriodId { get; set; }

    [RequiredIf("ConditionReportingPeriodId", new object[] {1}, ErrorMessage = "Compliance Reporting Due Date is required")]
    public DateTime? ComplianceReportingDueDate { get; set; }

    public int? OneTimeReportDays { get; set; }

    public bool RequiresValidation { get; set; }

    public bool CanBeDeleted { get; set; }

    public bool Standard { get; set; }

    public bool PumpageReports { get; set; }

    public string Status { get; set; }

    public DateTime? DueDate { get; set; }

    public int? ConditionComplianceId { get; set; }
  }
}
