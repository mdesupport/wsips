﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitHearingForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class PermitHearingForm
  {
    public int Id { get; set; }

    public IEnumerable<SelectListItem> PermitHearingStatusList { get; set; }

    public int? PermitHearingStatusId { get; set; }

    public int? PermitId { get; set; }

    public IEnumerable<SelectListItem> StateList { get; set; }

    [Required(ErrorMessage = "State is required")]
    [DisplayName("State")]
    public int? StateId { get; set; }

    [Required(ErrorMessage = "Date is required")]
    [DisplayName("Hearing Date")]
    public string DateOfHearing { get; set; }

    [Required(ErrorMessage = "Time is required")]
    [DisplayName("Hearing Time")]
    public string TimeOfHearing { get; set; }

    [Required(ErrorMessage = "Address Line1 is required")]
    [DisplayName("Address (Line 1)")]
    [StringLength(100)]
    public string Address1 { get; set; }

    [DisplayName("Address (Line 2)")]
    [StringLength(100)]
    public string Address2 { get; set; }

    [Required(ErrorMessage = "City is required")]
    [DisplayName("City")]
    [StringLength(50)]
    public string City { get; set; }

    [DataType(DataType.PostalCode)]
    [Required(ErrorMessage = "Zip Code is required")]
    [DisplayName("Zip Code")]
    [StringLength(10)]
    public string ZipCode { get; set; }

    [DisplayName("Hearing Officer")]
    [Required(ErrorMessage = "Hearing Officer is required")]
    [StringLength(100)]
    public string HearingOfficer { get; set; }
  }
}
