﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.ApplicationWizard
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class ApplicationWizard
  {
    public int? ApplicationTypeId { get; set; }

    public int? RefId { get; set; }

    public string RefIdPermitNumber { get; set; }

    public int PermitId { get; set; }

    public int PermitStatusId { get; set; }

    public string CurrentStep { get; set; }

    public int PermitCategoryId { get; set; }

    public bool IsWaterUser { get; set; }

    public bool IsLandOwner { get; set; }

    public bool IsConsultant { get; set; }

    public int PermitIssuedTo { get; set; }

    public List<ContactForm> Contacts { get; set; }

    [Required(ErrorMessage = "Water use description is required.")]
    public string UseDescription { get; set; }

    public ContactSearchForm ContactSearchForm { get; set; }

    public bool NotSureWhatTheWaterUseTypeIs { get; set; }

    public string SelectedWaterUsePurposeIds { get; set; }

    public List<PermitWaterWithdrawalPurpose> SavedWaterWithdrawalPurpose { get; set; }

    public WaterUseDetailForm WaterUseDetailForm { get; set; }

    public List<WastewaterTreatmentAndDisposalForm> WastewaterTreatmentDisposalForm { get; set; }

    public PermitStatusDetailForm PermitStatusDetailForm { get; set; }

    public bool IsExternalApplicaion { get; set; }

    public ApplicationWizard()
    {
      this.PermitStatusDetailForm = new PermitStatusDetailForm();
      this.ContactSearchForm = new ContactSearchForm()
      {
        SearchContext = SearchContext.ApplicationWizard
      };
    }
  }
}
