﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.PermitDistanceFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  [SpatialFilter]
  public class PermitDistanceFilterForm
  {
    public Decimal? PointX { get; set; }

    public Decimal? PointY { get; set; }

    [Display(Name = "SRID")]
    public int? SRID { get; set; }

    [Display(Name = "Distance")]
    public float? Distance { get; set; }

    public bool IsApplied { get; set; }

    public PermitDistanceFilter BuildFilter()
    {
      if (!this.IsApplied)
        return (PermitDistanceFilter) null;
      return new PermitDistanceFilter()
      {
        PointX = this.PointX.Value,
        PointY = this.PointY.Value,
        SRID = this.SRID.Value,
        Distance = this.Distance.Value
      };
    }
  }
}
