﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.WithdrawalLocationForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Attributes.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class WithdrawalLocationForm
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    [Required(ErrorMessage = "Please Select a {0}")]
    [Display(Name = "Withdrawal Source")]
    public int WithdrawalSourceId { get; set; }

    public IEnumerable WithdrawalSourceList { get; set; }

    [Display(Name = "Water Withdrawal Type")]
    [Required(ErrorMessage = "Please Select a {0}")]
    public int WithdrawalTypeId { get; set; }

    public AttributedSelectList WithdrawalTypeList { get; set; }

    [Display(Name = "Water Body Name")]
    [RequiredIf("WithdrawalSourceId", new object[] {2}, ErrorMessage = "Please Select a {0}")]
    public int? SelectedStateStreamId { get; set; }

    public SelectList StateStreamList { get; set; }

    [RequiredIf("SelectedStateStreamId", new object[] {-1}, ErrorMessage = "{0} is required")]
    [Display(Name = "Water Body Name")]
    public string OtherStreamName { get; set; }

    [Display(Name = "Location Of Intake")]
    public string IntakeLocation { get; set; }

    [Display(Name = "Is this an Existing Well?")]
    [RequiredIf("WaterWithdrawalTypeId", new object[] {1})]
    public bool IsExistingWell { get; set; }

    [Display(Name = "Well Depth (feet)")]
    public Decimal? WellDepth { get; set; }

    [Display(Name = "Well Diameter (inches)")]
    public Decimal? WellDiameter { get; set; }

    [Display(Name = "Well Tag No.")]
    public string WellTagNo { get; set; }

    [Display(Name = "Comments")]
    public string Comments { get; set; }

    public double WithdrawalLocationX { get; set; }

    public double WithdrawalLocationY { get; set; }

    public int SpatialReferenceId { get; set; }

    [Display(Name = "Water Use Category and Type")]
    public IDictionary<PermitWaterWithdrawalPurposeForm, bool> PermitWaterWithdrawalPurposeList { get; set; }
  }
}
