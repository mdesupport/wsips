﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms.WaterWithdrawalPurposeCategoryFilterForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms
{
  public class WaterWithdrawalPurposeCategoryFilterForm : IDashboardFilter
  {
    [Display(Name = "Use Code")]
    public List<int> SelectedWaterWithdrawalPurposeIds { get; set; }

    [XmlIgnore]
    public SelectList WaterWithdrawalPurposeCategoryList { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (this.SelectedWaterWithdrawalPurposeIds != null && this.SelectedWaterWithdrawalPurposeIds.Count<int>() > 0 && this.SelectedWaterWithdrawalPurposeIds.All<int>((Func<int, bool>) (p => p != 0)))
        dynamicFilterList.Add(new DynamicFilter("PermitWaterWithdrawalPurposes.Any(@0.Contains(outerIt.WaterWithdrawalPurposeId))", new object[1]
        {
          (object) this.SelectedWaterWithdrawalPurposeIds
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    public void Rehydrate()
    {
      if (this.SelectedWaterWithdrawalPurposeIds == null || this.SelectedWaterWithdrawalPurposeIds.Count == 0)
        this.SelectedWaterWithdrawalPurposeIds = new List<int>()
        {
          0
        };
      this.WaterWithdrawalPurposeCategoryList = this.GetWaterWithdrawalPurposeCategoryList(ObjectFactory.GetInstance<IService<LU_WaterWithdrawalPurposeCategory>>());
    }

    private SelectList GetWaterWithdrawalPurposeCategoryList(IService<LU_WaterWithdrawalPurposeCategory> waterWithdrawalPurposeCategoryService)
    {
      List<SelectListItem> list = waterWithdrawalPurposeCategoryService.GetAll("LU_WaterWithdrawalPurpose").SelectMany<LU_WaterWithdrawalPurposeCategory, LU_WaterWithdrawalPurpose>((Func<LU_WaterWithdrawalPurposeCategory, IEnumerable<LU_WaterWithdrawalPurpose>>) (t => (IEnumerable<LU_WaterWithdrawalPurpose>) t.LU_WaterWithdrawalPurpose)).OrderBy<LU_WaterWithdrawalPurpose, string>((Func<LU_WaterWithdrawalPurpose, string>) (x => x.Description)).Select<LU_WaterWithdrawalPurpose, SelectListItem>((Func<LU_WaterWithdrawalPurpose, SelectListItem>) (p => new SelectListItem()
      {
        Text = p.Description,
        Value = p.Id.ToString()
      })).ToList<SelectListItem>();
      list.Insert(0, new SelectListItem()
      {
        Text = "Display All",
        Value = "0"
      });
      return new SelectList((IEnumerable) list, "Value", "Text");
    }
  }
}
