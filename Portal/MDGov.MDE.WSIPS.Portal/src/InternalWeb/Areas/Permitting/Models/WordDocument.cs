﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.WordDocument
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models
{
  public class WordDocument
  {
    public int DocumentByteId { get; set; }

    public int PermitId { get; set; }

    public int? PermitStatusId { get; set; }

    public int TableId { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public int? DocumentTypeId { get; set; }

    public bool SaveToFileSystem { get; set; }

    public bool SaveToDatabase { get; set; }

    public string SaveToLocation { get; set; }

    public int TemplateTypeId { get; set; }

    public IEnumerable<LU_Template> Templates { get; set; }
  }
}
