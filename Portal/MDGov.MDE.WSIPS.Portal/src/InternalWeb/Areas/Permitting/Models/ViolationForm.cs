﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.ViolationForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models
{
  public class ViolationForm
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public string FileNumber { get; set; }

    [Required(ErrorMessage = "Violation Type is required")]
    public int ViolationTypeId { get; set; }

    public string ViolationType { get; set; }

    [Display(Name = "Violation Date")]
    public DateTime ViolationDate { get; set; }

    public string Description { get; set; }

    public string Status { get; set; }

    public bool Resolved { get; set; }

    public DateTime? ResolutionDate { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public bool IsEditDeleteAllowed { get; set; }
  }
}
