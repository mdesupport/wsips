﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.ApplicationSummary
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models
{
  public class ApplicationSummary
  {
    public SummaryContact Applicant { get; set; }

    public SummaryContact WaterUser { get; set; }

    public SummaryContact PrimaryLandOwner { get; set; }

    public List<SummaryContact> AdditionalLandOwners { get; set; }

    public string Permittee { get; set; }

    public Permit Permit { get; set; }

    public List<string> SelectedUseDetailCategories { get; set; }

    public ApplicationWizard AppWizard { get; set; }

    public PermitWithdrawalGroundwater PermitWithdrawalGroundwater { get; set; }

    public PermitWithdrawalSurfacewater PermitWithdrawalSurfacewater { get; set; }

    public string PermitCounty { get; set; }

    public string ReceivedDate { get; set; }

    public int TotalIrrigationAcresForCrop { get; set; }

    public string RequiresWithdrawalReporting { get; set; }

    public ApplicationSummary()
    {
      this.AdditionalLandOwners = new List<SummaryContact>();
    }
  }
}
