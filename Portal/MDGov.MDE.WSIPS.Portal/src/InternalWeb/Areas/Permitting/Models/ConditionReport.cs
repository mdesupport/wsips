﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.ConditionReport
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models
{
  public class ConditionReport
  {
    public int Id { get; set; }

    public int PermitConditionId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public DateTime DueDate { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public DateTime? SubmittedDate { get; set; }

    public string Status { get; set; }

    public int ConditionComplianceStatusId { get; set; }

    public int? StandardConditionTypeId { get; set; }

    public int Uploads { get; set; }

    public int? ExistingDocumentId { get; set; }

    public double? ReportAverageUsage { get; set; }

    public double? ReportTotalUsage { get; set; }

    public double AnnualAverageOverusePercentage { get; set; }

    public double AnnualAverageUsage { get; set; }

    public double? AnnualTotalUsage { get; set; }
  }
}
