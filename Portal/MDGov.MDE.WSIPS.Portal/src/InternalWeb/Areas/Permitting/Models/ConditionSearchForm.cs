﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.ConditionSearchForm
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models
{
  public class ConditionSearchForm
  {
    public int PermitTemplateId { get; set; }

    public IEnumerable<DynamicFilter> BuildFilter()
    {
      return (IEnumerable<DynamicFilter>) new List<DynamicFilter>()
      {
        new DynamicFilter("PermitTemplateId == " + (object) this.PermitTemplateId, new object[0])
      };
    }
  }
}
