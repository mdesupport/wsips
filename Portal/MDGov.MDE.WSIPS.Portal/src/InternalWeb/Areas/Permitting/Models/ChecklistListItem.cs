﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.ChecklistListItem
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models
{
  public class ChecklistListItem
  {
    public bool Active;

    public int Id { get; set; }

    public int PermitStatusId { get; set; }

    public string Status { get; set; }

    public int PermitCategoryId { get; set; }

    public string Category { get; set; }

    public int PermitTypeId { get; set; }

    public string Type { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public int Steps { get; set; }
  }
}
