﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.EnforcementController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization]
  public class EnforcementController : BaseController
  {
    private static readonly EnforcementController.ActionNamesClass s_actions = new EnforcementController.ActionNamesClass();
    private static readonly EnforcementController.ActionParamsClass_ViolationsData s_params_ViolationsData = new EnforcementController.ActionParamsClass_ViolationsData();
    private static readonly EnforcementController.ActionParamsClass_EnforcementActionData s_params_EnforcementActionData = new EnforcementController.ActionParamsClass_EnforcementActionData();
    private static readonly EnforcementController.ActionParamsClass_ViolationEnforcementActionData s_params_ViolationEnforcementActionData = new EnforcementController.ActionParamsClass_ViolationEnforcementActionData();
    private static readonly EnforcementController.ActionParamsClass_PenaltyData s_params_PenaltyData = new EnforcementController.ActionParamsClass_PenaltyData();
    private static readonly EnforcementController.ActionParamsClass_ViolationPenaltyData s_params_ViolationPenaltyData = new EnforcementController.ActionParamsClass_ViolationPenaltyData();
    private static readonly EnforcementController.ActionParamsClass_CreateViolation s_params_CreateViolation = new EnforcementController.ActionParamsClass_CreateViolation();
    private static readonly EnforcementController.ActionParamsClass_EditViolation s_params_EditViolation = new EnforcementController.ActionParamsClass_EditViolation();
    private static readonly EnforcementController.ActionParamsClass_DeleteViolation s_params_DeleteViolation = new EnforcementController.ActionParamsClass_DeleteViolation();
    private static readonly EnforcementController.ActionParamsClass_CreateEnforcementAction s_params_CreateEnforcementAction = new EnforcementController.ActionParamsClass_CreateEnforcementAction();
    private static readonly EnforcementController.ActionParamsClass_EditEnforcementAction s_params_EditEnforcementAction = new EnforcementController.ActionParamsClass_EditEnforcementAction();
    private static readonly EnforcementController.ActionParamsClass_DeleteEnforcementAction s_params_DeleteEnforcementAction = new EnforcementController.ActionParamsClass_DeleteEnforcementAction();
    private static readonly EnforcementController.ActionParamsClass_CreatePenalty s_params_CreatePenalty = new EnforcementController.ActionParamsClass_CreatePenalty();
    private static readonly EnforcementController.ActionParamsClass_EditPenalty s_params_EditPenalty = new EnforcementController.ActionParamsClass_EditPenalty();
    private static readonly EnforcementController.ActionParamsClass_DeletePenalty s_params_DeletePenalty = new EnforcementController.ActionParamsClass_DeletePenalty();
    private static readonly EnforcementController.ActionParamsClass_LoadAdvertisementFee s_params_LoadAdvertisementFee = new EnforcementController.ActionParamsClass_LoadAdvertisementFee();
    private static readonly EnforcementController.ActionParamsClass_GetAdvertisementFeesByPermitId s_params_GetAdvertisementFeesByPermitId = new EnforcementController.ActionParamsClass_GetAdvertisementFeesByPermitId();
    private static readonly EnforcementController.ActionParamsClass_CreateContactSale s_params_CreateContactSale = new EnforcementController.ActionParamsClass_CreateContactSale();
    private static readonly EnforcementController.ActionParamsClass_DeleteAdvertisementFee s_params_DeleteAdvertisementFee = new EnforcementController.ActionParamsClass_DeleteAdvertisementFee();
    private static readonly EnforcementController.ViewsClass s_views = new EnforcementController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Permitting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Enforcement";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Enforcement";
    private readonly IUpdatableService<Violation> _violationService;
    private readonly IUpdatableService<Enforcement> _enforcementService;
    private readonly IUpdatableService<PermitViolationEnforcement> _permitViolationEnforcementService;
    private readonly IService<LU_ViolationType> _violationTypeService;
    private readonly IService<LU_EnforcementAction> _enforcementActionTypeService;
    private readonly IService<ComplianceStaff> _complianceStaffService;
    private readonly IService<Contact> _contactService;
    private readonly IUpdatableService<PermitContact> _permitContactService;
    private readonly IUpdatableService<Penalty> _penaltyService;
    private readonly IUpdatableService<PermitViolationPenalty> _permitViolationPenaltyService;
    private readonly IService<LU_ProductType> _productTypeService;
    private readonly IService<Product> _productService;
    private readonly IUpdatableService<ContactSale> _contactSaleService;
    private readonly IService<Permit> _permitService;
    private readonly IUpdatableService<Message> _messageService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public EnforcementController Actions
    {
      get
      {
        return MVC.Permitting.Enforcement;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionNamesClass ActionNames
    {
      get
      {
        return EnforcementController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public EnforcementController.ActionParamsClass_ViolationsData ViolationsDataParams
    {
      get
      {
        return EnforcementController.s_params_ViolationsData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_EnforcementActionData EnforcementActionDataParams
    {
      get
      {
        return EnforcementController.s_params_EnforcementActionData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_ViolationEnforcementActionData ViolationEnforcementActionDataParams
    {
      get
      {
        return EnforcementController.s_params_ViolationEnforcementActionData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_PenaltyData PenaltyDataParams
    {
      get
      {
        return EnforcementController.s_params_PenaltyData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_ViolationPenaltyData ViolationPenaltyDataParams
    {
      get
      {
        return EnforcementController.s_params_ViolationPenaltyData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_CreateViolation CreateViolationParams
    {
      get
      {
        return EnforcementController.s_params_CreateViolation;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_EditViolation EditViolationParams
    {
      get
      {
        return EnforcementController.s_params_EditViolation;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_DeleteViolation DeleteViolationParams
    {
      get
      {
        return EnforcementController.s_params_DeleteViolation;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_CreateEnforcementAction CreateEnforcementActionParams
    {
      get
      {
        return EnforcementController.s_params_CreateEnforcementAction;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_EditEnforcementAction EditEnforcementActionParams
    {
      get
      {
        return EnforcementController.s_params_EditEnforcementAction;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_DeleteEnforcementAction DeleteEnforcementActionParams
    {
      get
      {
        return EnforcementController.s_params_DeleteEnforcementAction;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_CreatePenalty CreatePenaltyParams
    {
      get
      {
        return EnforcementController.s_params_CreatePenalty;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public EnforcementController.ActionParamsClass_EditPenalty EditPenaltyParams
    {
      get
      {
        return EnforcementController.s_params_EditPenalty;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public EnforcementController.ActionParamsClass_DeletePenalty DeletePenaltyParams
    {
      get
      {
        return EnforcementController.s_params_DeletePenalty;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public EnforcementController.ActionParamsClass_LoadAdvertisementFee LoadAdvertisementFeeParams
    {
      get
      {
        return EnforcementController.s_params_LoadAdvertisementFee;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_GetAdvertisementFeesByPermitId GetAdvertisementFeesByPermitIdParams
    {
      get
      {
        return EnforcementController.s_params_GetAdvertisementFeesByPermitId;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public EnforcementController.ActionParamsClass_CreateContactSale CreateContactSaleParams
    {
      get
      {
        return EnforcementController.s_params_CreateContactSale;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ActionParamsClass_DeleteAdvertisementFee DeleteAdvertisementFeeParams
    {
      get
      {
        return EnforcementController.s_params_DeleteAdvertisementFee;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public EnforcementController.ViewsClass Views
    {
      get
      {
        return EnforcementController.s_views;
      }
    }

    public EnforcementController(IUpdatableService<Violation> violationService, IUpdatableService<Enforcement> enforcementService, IUpdatableService<PermitViolationEnforcement> permitViolationEnforcementService, IService<LU_ViolationType> violationTypeService, IService<LU_EnforcementAction> enforcementActionTypeService, IService<ComplianceStaff> complianceStaffService, IService<Contact> contactService, IUpdatableService<PermitContact> permitContactService, IUpdatableService<Penalty> penaltyService, IUpdatableService<PermitViolationPenalty> permitViolationPenaltyService, IService<LU_ProductType> productTypeService, IService<Product> productService, IUpdatableService<ContactSale> contactSaleService, IService<Permit> permitService, IUpdatableService<Message> messageService)
    {
      this._violationService = violationService;
      this._enforcementService = enforcementService;
      this._permitViolationEnforcementService = permitViolationEnforcementService;
      this._violationTypeService = violationTypeService;
      this._enforcementActionTypeService = enforcementActionTypeService;
      this._complianceStaffService = complianceStaffService;
      this._contactService = contactService;
      this._permitContactService = permitContactService;
      this._penaltyService = penaltyService;
      this._permitViolationPenaltyService = permitViolationPenaltyService;
      this._productTypeService = productTypeService;
      this._productService = productService;
      this._contactSaleService = contactSaleService;
      this._permitService = permitService;
      this._messageService = messageService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected EnforcementController(Dummy d)
    {
    }

    [HttpGet]
    public virtual JsonResult ViolationsData(GridCommand command, int permitId)
    {
      List<ViolationForm> list = Mapper.Map<IEnumerable<Violation>, IEnumerable<ViolationForm>>(this._violationService.GetRange(0, int.MaxValue, "LastModifiedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0", new object[1]{ (object) permitId }) }, "LU_ViolationType")).ToList<ViolationForm>();
      int count = list.Count;
      int num = (int) Math.Ceiling((double) count / (double) command.PageSize);
      return this.Json((object) new GridData() { Data = (IEnumerable) list, PageTotal = num, CurrentPage = command.Page, RecordCount = count }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult EnforcementActionData(GridCommand command, int permitId)
    {
      List<EnforcementForm> list = Mapper.Map<IEnumerable<Enforcement>, IEnumerable<EnforcementForm>>(this._permitViolationEnforcementService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Violation.PermitId == @0", new object[1]{ (object) permitId }) }, "Enforcement.LU_EnforcementAction").Select<PermitViolationEnforcement, Enforcement>((Func<PermitViolationEnforcement, Enforcement>) (pve => pve.Enforcement))).ToList<EnforcementForm>();
      int count = list.Count;
      int num = (int) Math.Ceiling((double) count / (double) command.PageSize);
      return this.Json((object) new GridData() { Data = (IEnumerable) list, PageTotal = num, CurrentPage = command.Page, RecordCount = count }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult ViolationEnforcementActionData(int violationId)
    {
      return this.Json((object) new{ Data = Mapper.Map<IEnumerable<Enforcement>, IEnumerable<EnforcementForm>>(this._permitViolationEnforcementService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ViolationId == @0", new object[1]{ (object) violationId }) }, "Enforcement.LU_EnforcementAction").Select<PermitViolationEnforcement, Enforcement>((Func<PermitViolationEnforcement, Enforcement>) (pve => pve.Enforcement))) }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult PenaltyData(GridCommand command, int permitId)
    {
      IEnumerable<PermitViolationPenalty> range = this._permitViolationPenaltyService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Violation.PermitId == @0", new object[1]{ (object) permitId }) }, "Penalty");
      List<PenaltyForm> list = Mapper.Map<IEnumerable<Penalty>, IEnumerable<PenaltyForm>>(range.Select<PermitViolationPenalty, Penalty>((Func<PermitViolationPenalty, Penalty>) (t => t.Penalty))).ToList<PenaltyForm>();
      foreach (PenaltyForm penaltyForm in list)
      {
        ContactSale byId = this._contactSaleService.GetById(penaltyForm.ContactSaleId, (string) null);
        if (byId != null)
        {
          penaltyForm.Amount = new Decimal?(byId.Amount);
          penaltyForm.IsEditDeleteAllowed = !byId.PaidDate.HasValue;
        }
        penaltyForm.ViolationIds = range.Select<PermitViolationPenalty, int>((Func<PermitViolationPenalty, int>) (pvp => pvp.ViolationId)).ToList<int>();
      }
      int count = list.Count;
      int num = (int) Math.Ceiling((double) count / (double) command.PageSize);
      return this.Json((object) new GridData() { Data = (IEnumerable) list, PageTotal = num, CurrentPage = command.Page, RecordCount = count }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult ViolationPenaltyData(int violationId)
    {
      return this.Json((object) new{ Data = Mapper.Map<IEnumerable<Penalty>, IEnumerable<PenaltyForm>>(this._permitViolationPenaltyService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ViolationId == @0", new object[1]{ (object) violationId }) }, "Penalty").Select<PermitViolationPenalty, Penalty>((Func<PermitViolationPenalty, Penalty>) (t => t.Penalty))) }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual ActionResult CreateViolation()
    {
      ViolationForm violationForm = Mapper.Map<Violation, ViolationForm>(new Violation());
      this.GetViolationTypes();
      return (ActionResult) this.PartialView(MVC.Permitting.Enforcement.Views._CreateOrEditViolation, (object) violationForm);
    }

    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    [HttpPost]
    public virtual JsonResult CreateViolation(ViolationForm violation)
    {
      try
      {
        if (violation.Resolved && !violation.ResolutionDate.HasValue)
          this.ModelState.AddModelError(string.Empty, "Date Resolved is required if the case is resolved");
        if (!this.ModelState.IsValid)
          return this.Json((object) new{ IsSuccess = false, Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)) }, JsonRequestBehavior.AllowGet);
        this._violationService.Save(Mapper.Map<ViolationForm, Violation>(violation));
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    [HttpGet]
    public virtual ActionResult EditViolation(int id)
    {
      ViolationForm violationForm = Mapper.Map<Violation, ViolationForm>(this._violationService.GetById(id, (string) null));
      this.GetViolationTypes();
      return (ActionResult) this.PartialView(MVC.Permitting.Enforcement.Views._CreateOrEditViolation, (object) violationForm);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual JsonResult EditViolation(ViolationForm violation)
    {
      try
      {
        if (violation.Resolved && !violation.ResolutionDate.HasValue)
          this.ModelState.AddModelError(string.Empty, "Date Resolved is required if the case is resolved");
        if (!this.ModelState.IsValid)
          return this.Json((object) new{ IsSuccess = false, Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)) }, JsonRequestBehavior.AllowGet);
        this._violationService.Save(Mapper.Map<ViolationForm, Violation>(violation));
        return this.Json((object) new{ IsSuccess = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    [HttpDelete]
    public virtual JsonResult DeleteViolation(int id)
    {
      try
      {
        foreach (PermitViolationEnforcement violationEnforcement in this._permitViolationEnforcementService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ViolationId == " + (object) id, new object[0]) }, (string) null))
        {
          int enforcementId = violationEnforcement.EnforcementId;
          this._permitViolationEnforcementService.Delete(violationEnforcement.Id);
          this._enforcementService.Delete(enforcementId);
        }
        foreach (PermitViolationPenalty violationPenalty in this._permitViolationPenaltyService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ViolationId == " + (object) id, new object[0]) }, (string) null))
        {
          int penaltyId = violationPenalty.PenaltyId;
          this._permitViolationPenaltyService.Delete(violationPenalty.Id);
          this._enforcementService.Delete(penaltyId);
        }
        this._violationService.Delete(id);
        return this.Json((object) new{ success = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    [HttpGet]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual ActionResult CreateEnforcementAction(int permitId)
    {
      EnforcementForm enforcementForm = Mapper.Map<Enforcement, EnforcementForm>(new Enforcement());
      this.GetActionTypes();
      this.GetPermitViolations(permitId, false);
      this.GetAssignees(0);
      return (ActionResult) this.PartialView(MVC.Permitting.Enforcement.Views._CreateOrEditEnforcementAction, (object) enforcementForm);
    }

    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    [HttpPost]
    public virtual JsonResult CreateEnforcementAction(EnforcementForm form)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return this.Json((object) new{ IsSuccess = false, Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)) }, JsonRequestBehavior.AllowGet);
        int id = this._enforcementService.Save(Mapper.Map<EnforcementForm, Enforcement>(form));
        foreach (int violationId in form.ViolationIds)
          this._permitViolationEnforcementService.Save(new PermitViolationEnforcement()
          {
            ViolationId = violationId,
            EnforcementId = id
          });
        int permitId = this._enforcementService.GetById(id, "PermitViolationEnforcements.Violation").PermitViolationEnforcements.First<PermitViolationEnforcement>().Violation.PermitId;
        this.FindAndCreateComplianceStaffPermitContact(permitId, form.ContactId);
        Uri uri = new Uri(new Uri(ConfigurationManager.AppSettings["InternalPortalUrl"]), ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(permitId, new PermitDetailsTab?())));
        this._messageService.Save(new Message()
        {
          ContactIds = new List<int>() { form.ContactId },
          Subject = "New Enforcement Action Assigned",
          Body = "A new enforcement action has been assigned to you. Please click the following link and navigate to the Enforcement Tab to view:<p>" + (object) uri + "</p>",
          From = ConfigurationManager.AppSettings["SenderEmail"],
          NotificationTypeId = 1,
          NotificationText = "A new enforcement action has been assigned to you.",
          PermitId = permitId
        });
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual ActionResult EditEnforcementAction(int id, int permitId)
    {
      EnforcementForm enforcementForm = Mapper.Map<Enforcement, EnforcementForm>(this._enforcementService.GetById(id, "PermitViolationEnforcements"));
      this.GetActionTypes();
      this.GetPermitViolations(permitId, true);
      this.GetAssignees(id);
      return (ActionResult) this.PartialView(MVC.Permitting.Enforcement.Views._CreateOrEditEnforcementAction, (object) enforcementForm);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual JsonResult EditEnforcementAction(EnforcementForm form)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return this.Json((object) new{ IsSuccess = false, Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)) }, JsonRequestBehavior.AllowGet);
        Enforcement entity = Mapper.Map<EnforcementForm, Enforcement>(form);
        this._enforcementService.Save(entity);
        IUpdatableService<PermitViolationEnforcement> enforcementService = this._permitViolationEnforcementService;
        int skip = 0;
        int maxValue = int.MaxValue;
        DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]{ new DynamicFilter("EnforcementId == @0", new object[1]{ (object) entity.Id }) };
        foreach (PermitViolationEnforcement violationEnforcement in enforcementService.GetRange(skip, maxValue, null, (IEnumerable<DynamicFilter>) dynamicFilterArray, null))
          this._permitViolationEnforcementService.Delete(violationEnforcement.Id);
        foreach (int violationId in form.ViolationIds)
          this._permitViolationEnforcementService.Save(new PermitViolationEnforcement()
          {
            ViolationId = violationId,
            EnforcementId = entity.Id
          });
        int permitId = this._enforcementService.GetById(entity.Id, "PermitViolationEnforcements.Violation").PermitViolationEnforcements.First<PermitViolationEnforcement>().Violation.PermitId;
        if (!this.FindAndCreateComplianceStaffPermitContact(permitId, form.ContactId))
        {
          Uri uri = new Uri(new Uri(ConfigurationManager.AppSettings["InternalPortalUrl"]), ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(permitId, new PermitDetailsTab?())));
          this._messageService.Save(new Message()
          {
            ContactIds = new List<int>() { form.ContactId },
            Subject = "Existing Enforcement Action Assigned",
            Body = "An existing enforcement action has been assigned to you. Please click the following link and navigate to the Enforcement Tab to view:<p>" + (object) uri + "</p>",
            From = ConfigurationManager.AppSettings["SenderEmail"],
            NotificationTypeId = 1,
            NotificationText = "An existing enforcement action has been assigned to you.",
            PermitId = permitId
          });
        }
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpDelete]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual JsonResult DeleteEnforcementAction(int id)
    {
      try
      {
        foreach (PermitViolationEnforcement violationEnforcement in this._permitViolationEnforcementService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("EnforcementId == " + (object) id, new object[0]) }, (string) null))
          this._permitViolationEnforcementService.Delete(violationEnforcement.Id);
        this._enforcementService.Delete(id);
        return this.Json((object) new{ success = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    [HttpGet]
    public virtual ActionResult CreatePenalty(int permitId)
    {
      PenaltyForm penaltyForm = Mapper.Map<Penalty, PenaltyForm>(new Penalty());
      List<AttributedSelectListItem> list = this._productService.GetRange(0, int.MaxValue, "Description", (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("ProductTypeId == @0", new object[1]{ (object) 4 }), new DynamicFilter("Active", new object[0]) }, (string) null).Select<Product, AttributedSelectListItem>((Func<Product, AttributedSelectListItem>) (pp => new AttributedSelectListItem() { Text = pp.Description, Value = pp.Id.ToString(), HtmlAttributes = (object) new{ data_amount = Decimal.Round(pp.Amount.GetValueOrDefault(), 2) } })).ToList<AttributedSelectListItem>();
      list.Insert(0, new AttributedSelectListItem()
      {
        Text = "Select",
        Value = ""
      });
      penaltyForm.ProductList = (IEnumerable<AttributedSelectListItem>) list;
      this.GetPermitViolations(permitId, false);
      return (ActionResult) this.PartialView(MVC.Permitting.Enforcement.Views._CreateOrEditPenalty, (object) penaltyForm);
    }

    [HttpPost]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual JsonResult CreatePenalty(PenaltyForm form)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return this.Json((object) new{ IsSuccess = false, Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)) }, JsonRequestBehavior.AllowGet);
        PermitContact permitContact = this._permitContactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[3]{ new DynamicFilter("PermitId == @0", new object[1]{ (object) form.PermitId }), new DynamicFilter("IsPermittee", new object[0]), new DynamicFilter("IsPrimary", new object[0]) }, "Permit").SingleOrDefault<PermitContact>();
        if (permitContact == null)
          return this.Json((object) new{ IsSuccess = false, Errors = new string[1]{ "No permittee exists for permit " + (object) form.PermitId } }, JsonRequestBehavior.AllowGet);
        Penalty entity1 = Mapper.Map<PenaltyForm, Penalty>(form);
        ContactSale entity2 = new ContactSale() { ContactId = permitContact.ContactId, PermitId = new int?(permitContact.PermitId), PermitName = permitContact.Permit.PermitName, ProductId = form.ProductId, Amount = form.Amount.Value, ContactSaleStatusId = 1 };
        entity1.ContactSaleId = this._contactSaleService.Save(entity2);
        int num = this._penaltyService.Save(entity1);
        foreach (int violationId in form.ViolationIds)
          this._permitViolationPenaltyService.Save(new PermitViolationPenalty()
          {
            ViolationId = violationId,
            PenaltyId = num
          });
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual ActionResult EditPenalty(int id, int permitId)
    {
      PenaltyForm penaltyForm = Mapper.Map<Penalty, PenaltyForm>(this._penaltyService.GetById(id, "PermitViolationPenalties"));
      penaltyForm.Amount = new Decimal?(this._contactSaleService.GetById(penaltyForm.ContactSaleId, (string) null).Amount);
      List<AttributedSelectListItem> list = this._productService.GetRange(0, int.MaxValue, "Description", (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("ProductTypeId == @0", new object[1]{ (object) 4 }), new DynamicFilter("Active", new object[0]) }, (string) null).Select<Product, AttributedSelectListItem>((Func<Product, AttributedSelectListItem>) (pp => new AttributedSelectListItem() { Text = pp.Description, Value = pp.Id.ToString(), HtmlAttributes = (object) new{ data_amount = Decimal.Round(pp.Amount.GetValueOrDefault(), 2) } })).ToList<AttributedSelectListItem>();
      list.Insert(0, new AttributedSelectListItem()
      {
        Text = "Select",
        Value = ""
      });
      penaltyForm.ProductList = (IEnumerable<AttributedSelectListItem>) list;
      this.GetPermitViolations(permitId, true);
      return (ActionResult) this.PartialView(MVC.Permitting.Enforcement.Views._CreateOrEditPenalty, (object) penaltyForm);
    }

    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    [HttpPost]
    public virtual JsonResult EditPenalty(PenaltyForm form)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return this.Json((object) new{ IsSuccess = false, Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)) }, JsonRequestBehavior.AllowGet);
        Penalty byId1 = this._penaltyService.GetById(form.Id, (string) null);
        Penalty entity = Mapper.Map<PenaltyForm, Penalty>(form, byId1);
        this._penaltyService.Save(entity);
        IUpdatableService<PermitViolationPenalty> violationPenaltyService = this._permitViolationPenaltyService;
        int skip = 0;
        int maxValue = int.MaxValue;
        DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]{ new DynamicFilter("PenaltyId == @0", new object[1]{ (object) byId1.Id }) };
        foreach (PermitViolationPenalty violationPenalty in violationPenaltyService.GetRange(skip, maxValue, null, (IEnumerable<DynamicFilter>) dynamicFilterArray, null))
          this._permitViolationPenaltyService.Delete(violationPenalty.Id);
        foreach (int violationId in form.ViolationIds)
          this._permitViolationPenaltyService.Save(new PermitViolationPenalty()
          {
            ViolationId = violationId,
            PenaltyId = byId1.Id
          });
        ContactSale byId2 = this._contactSaleService.GetById(entity.ContactSaleId, (string) null);
        byId2.Amount = form.Amount.Value;
        this._contactSaleService.Save(byId2);
        return this.Json((object) new{ IsSuccess = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpDelete]
    [Authorization(AuthorizedRoles = new string[] {"Compliance / Enforcement Staff", "Compliance / Enforcement Manager"})]
    public virtual JsonResult DeletePenalty(int id)
    {
      try
      {
        Penalty byId = this._penaltyService.GetById(id, "PermitViolationPenalties");
        foreach (PermitViolationPenalty violationPenalty in (IEnumerable<PermitViolationPenalty>) byId.PermitViolationPenalties)
          this._permitViolationPenaltyService.Delete(violationPenalty.Id);
        this._contactSaleService.Delete(byId.ContactSaleId);
        this._penaltyService.Delete(id);
        return this.Json((object) new{ success = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    private void GetViolationTypes()
    {
      ViewBag.ConditionTypes = this._violationTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active", new object[0]) }, (string) null);
    }

    private void GetActionTypes()
    {
      ViewBag.ActionTypes = this._enforcementActionTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active", new object[0]) }, (string) null);
    }

    [HttpGet]
    public void GetAssignees(int enforcementId)
    {
      List<ListStaffMember> list = Mapper.Map<IEnumerable<Contact>, IEnumerable<ListStaffMember>>(this._contactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("@0.Contains(outerIt.Id)", new object[1]{ (object) this._complianceStaffService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active", new object[0]) }, (string) null).Select<ComplianceStaff, int>((Func<ComplianceStaff, int>) (c => c.ContactId)) }) }, (string) null)).ToList<ListStaffMember>();
      if (enforcementId != 0)
      {
        ListStaffMember assignee = Mapper.Map<Contact, ListStaffMember>(this._contactService.GetById(this._enforcementService.GetById(enforcementId, (string) null).ContactId, (string) null));
        if (!list.Any<ListStaffMember>((Func<ListStaffMember, bool>) (c => c.ContactId == assignee.ContactId)))
          list.Add(assignee);
      }
      ViewBag.Contacts = list.OrderBy<ListStaffMember, string>((Func<ListStaffMember, string>) (c => c.LastName));
    }

    public virtual ActionResult LoadAdvertisementFee(int permitId)
    {
      ViewBag.Permitid = permitId;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.DetailsPartials._AdvertisementFee, (object) new AdvertisementFeesForm() { Products = this._productService.GetAll((string) null).Where<Product>((Func<Product, bool>) (x =>
      {
        if (x.ProductTypeId != 3 || !x.Active)
          return false;
        DateTime? effectiveDate = x.EffectiveDate;
        DateTime now = DateTime.Now;
        if (!effectiveDate.HasValue)
          return false;
        return effectiveDate.GetValueOrDefault() <= now;
      })).ToList<Product>() });
    }

    public virtual JsonResult GetAdvertisementFeesByPermitId(int permitId)
    {
      return this.Json((object) new{ Data = this._contactSaleService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId==@0", new object[1]{ (object) permitId }) }, (string) null).Select(x => new{ FeeAmount = "$" + string.Format("{0:n}", (object) x.Amount), FeeDate = Convert.ToDateTime(x.CreatedDate).AddDays(30.0).ToShortDateString(), CreatedDate = Convert.ToDateTime(x.CreatedDate).ToShortDateString() + " " + Convert.ToDateTime(x.CreatedDate).ToShortTimeString(), CreatedBy = x.CreatedBy, Id = x.Id }) }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult CreateContactSale(ContactSale contactSale)
    {
      Permit byId = this._permitService.GetById(contactSale.PermitId.Value, "PermitContacts");
      contactSale.ContactId = byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (x => x.IsPermittee)).ContactId;
      contactSale.PermitName = byId.PermitName;
      contactSale.ContactSaleStatusId = 1;
      this._contactSaleService.Save(contactSale);
      return (JsonResult) null;
    }

    public virtual JsonResult DeleteAdvertisementFee(int id)
    {
      try
      {
        this._contactSaleService.Delete(id);
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    private void GetPermitViolations(int permitId, bool existingItem)
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>() { new DynamicFilter("PermitId == " + (object) permitId, new object[0]) };
      if (!existingItem)
        dynamicFilterList.Add(new DynamicFilter("ResolutionDate == null", new object[0]));
      ViewBag.PermitViolations = this._violationService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) dynamicFilterList, (string) null);
    }

    private bool FindAndCreateComplianceStaffPermitContact(int permitId, int contactId)
    {
      IEnumerable<PermitContact> range = this._permitContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0 && ContactTypeId == @1", new object[2]{ (object) permitId, (object) 34 }) }, (string) null);
      bool flag = false;
      foreach (PermitContact permitContact in range)
      {
        if (permitContact.ContactId == contactId)
          flag = true;
      }
      if (!flag)
        this._permitContactService.Save(new PermitContact()
        {
          PermitId = permitId,
          ContactId = contactId,
          ContactTypeId = new int?(34),
          IsApplicant = false,
          IsPrimary = true,
          Active = true
        });
      return flag;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult ViolationsData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ViolationsData, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult EnforcementActionData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.EnforcementActionData, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult ViolationEnforcementActionData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ViolationEnforcementActionData, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult PenaltyData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.PenaltyData, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult ViolationPenaltyData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ViolationPenaltyData, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult EditViolation()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditViolation, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult DeleteViolation()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeleteViolation, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual ActionResult CreateEnforcementAction()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreateEnforcementAction, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult EditEnforcementAction()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditEnforcementAction, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult DeleteEnforcementAction()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeleteEnforcementAction, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult CreatePenalty()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreatePenalty, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual ActionResult EditPenalty()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPenalty, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult DeletePenalty()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeletePenalty, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual ActionResult LoadAdvertisementFee()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadAdvertisementFee, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult GetAdvertisementFeesByPermitId()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetAdvertisementFeesByPermitId, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult CreateContactSale()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CreateContactSale, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult DeleteAdvertisementFee()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeleteAdvertisementFee, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string ViolationsData = "ViolationsData";
      public readonly string EnforcementActionData = "EnforcementActionData";
      public readonly string ViolationEnforcementActionData = "ViolationEnforcementActionData";
      public readonly string PenaltyData = "PenaltyData";
      public readonly string ViolationPenaltyData = "ViolationPenaltyData";
      public readonly string CreateViolation = "CreateViolation";
      public readonly string EditViolation = "EditViolation";
      public readonly string DeleteViolation = "DeleteViolation";
      public readonly string CreateEnforcementAction = "CreateEnforcementAction";
      public readonly string EditEnforcementAction = "EditEnforcementAction";
      public readonly string DeleteEnforcementAction = "DeleteEnforcementAction";
      public readonly string CreatePenalty = "CreatePenalty";
      public readonly string EditPenalty = "EditPenalty";
      public readonly string DeletePenalty = "DeletePenalty";
      public readonly string LoadAdvertisementFee = "LoadAdvertisementFee";
      public readonly string GetAdvertisementFeesByPermitId = "GetAdvertisementFeesByPermitId";
      public readonly string CreateContactSale = "CreateContactSale";
      public readonly string DeleteAdvertisementFee = "DeleteAdvertisementFee";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string ViolationsData = "ViolationsData";
      public const string EnforcementActionData = "EnforcementActionData";
      public const string ViolationEnforcementActionData = "ViolationEnforcementActionData";
      public const string PenaltyData = "PenaltyData";
      public const string ViolationPenaltyData = "ViolationPenaltyData";
      public const string CreateViolation = "CreateViolation";
      public const string EditViolation = "EditViolation";
      public const string DeleteViolation = "DeleteViolation";
      public const string CreateEnforcementAction = "CreateEnforcementAction";
      public const string EditEnforcementAction = "EditEnforcementAction";
      public const string DeleteEnforcementAction = "DeleteEnforcementAction";
      public const string CreatePenalty = "CreatePenalty";
      public const string EditPenalty = "EditPenalty";
      public const string DeletePenalty = "DeletePenalty";
      public const string LoadAdvertisementFee = "LoadAdvertisementFee";
      public const string GetAdvertisementFeesByPermitId = "GetAdvertisementFeesByPermitId";
      public const string CreateContactSale = "CreateContactSale";
      public const string DeleteAdvertisementFee = "DeleteAdvertisementFee";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_ViolationsData
    {
      public readonly string command = "command";
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EnforcementActionData
    {
      public readonly string command = "command";
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_ViolationEnforcementActionData
    {
      public readonly string violationId = "violationId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_PenaltyData
    {
      public readonly string command = "command";
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_ViolationPenaltyData
    {
      public readonly string violationId = "violationId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CreateViolation
    {
      public readonly string violation = "violation";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EditViolation
    {
      public readonly string id = "id";
      public readonly string violation = "violation";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_DeleteViolation
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CreateEnforcementAction
    {
      public readonly string permitId = "permitId";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_EditEnforcementAction
    {
      public readonly string id = "id";
      public readonly string permitId = "permitId";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_DeleteEnforcementAction
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CreatePenalty
    {
      public readonly string permitId = "permitId";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_EditPenalty
    {
      public readonly string id = "id";
      public readonly string permitId = "permitId";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_DeletePenalty
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_LoadAdvertisementFee
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetAdvertisementFeesByPermitId
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_CreateContactSale
    {
      public readonly string contactSale = "contactSale";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_DeleteAdvertisementFee
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly EnforcementController.ViewsClass._ViewNamesClass s_ViewNames = new EnforcementController.ViewsClass._ViewNamesClass();
      public readonly string _CreateOrEditEnforcementAction = "~/Areas/Permitting/Views/Enforcement/_CreateOrEditEnforcementAction.cshtml";
      public readonly string _CreateOrEditPenalty = "~/Areas/Permitting/Views/Enforcement/_CreateOrEditPenalty.cshtml";
      public readonly string _CreateOrEditViolation = "~/Areas/Permitting/Views/Enforcement/_CreateOrEditViolation.cshtml";

      public EnforcementController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return EnforcementController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string _CreateOrEditEnforcementAction = "_CreateOrEditEnforcementAction";
        public readonly string _CreateOrEditPenalty = "_CreateOrEditPenalty";
        public readonly string _CreateOrEditViolation = "_CreateOrEditViolation";
      }
    }
  }
}
