﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.DashboardController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using iTextSharp.text;
using iTextSharp.text.pdf;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Geometry;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using MDGov.MDE.WSIPS.Portal.Model;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization]
  public class DashboardController : BaseController
  {
    private static readonly DashboardController.ActionNamesClass s_actions = new DashboardController.ActionNamesClass();
    private static readonly DashboardController.ActionParamsClass_Index s_params_Index = new DashboardController.ActionParamsClass_Index();
    private static readonly DashboardController.ActionParamsClass_Data s_params_Data = new DashboardController.ActionParamsClass_Data();
    private static readonly DashboardController.ActionParamsClass_Filter s_params_Filter = new DashboardController.ActionParamsClass_Filter();
    private static readonly DashboardController.ActionParamsClass_SpatialFilter s_params_SpatialFilter = new DashboardController.ActionParamsClass_SpatialFilter();
    private static readonly DashboardController.ActionParamsClass_Search s_params_Search = new DashboardController.ActionParamsClass_Search();
    private static readonly DashboardController.ActionParamsClass_SaveFilter s_params_SaveFilter = new DashboardController.ActionParamsClass_SaveFilter();
    private static readonly DashboardController.ActionParamsClass_GetFilterControl s_params_GetFilterControl = new DashboardController.ActionParamsClass_GetFilterControl();
    private static readonly DashboardController.ActionParamsClass_BuildKmlFile s_params_BuildKmlFile = new DashboardController.ActionParamsClass_BuildKmlFile();
    private static readonly DashboardController.ViewsClass s_views = new DashboardController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Permitting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Dashboard";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Dashboard";
    private readonly IRepository<DashboardFilter> _filterRepository;
    private readonly IDashboardPermitService _dashboardPermitService;
    private readonly IExportDashboardService _exportDashboardPermitService;
    private readonly IService<Contact> _contactService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DashboardController Actions
    {
      get
      {
        return MVC.Permitting.Dashboard;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DashboardController.ActionNamesClass ActionNames
    {
      get
      {
        return DashboardController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DashboardController.ActionParamsClass_Index IndexParams
    {
      get
      {
        return DashboardController.s_params_Index;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DashboardController.ActionParamsClass_Data DataParams
    {
      get
      {
        return DashboardController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DashboardController.ActionParamsClass_Filter FilterParams
    {
      get
      {
        return DashboardController.s_params_Filter;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DashboardController.ActionParamsClass_SpatialFilter SpatialFilterParams
    {
      get
      {
        return DashboardController.s_params_SpatialFilter;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DashboardController.ActionParamsClass_Search SearchParams
    {
      get
      {
        return DashboardController.s_params_Search;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DashboardController.ActionParamsClass_SaveFilter SaveFilterParams
    {
      get
      {
        return DashboardController.s_params_SaveFilter;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DashboardController.ActionParamsClass_GetFilterControl GetFilterControlParams
    {
      get
      {
        return DashboardController.s_params_GetFilterControl;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DashboardController.ActionParamsClass_BuildKmlFile BuildKmlFileParams
    {
      get
      {
        return DashboardController.s_params_BuildKmlFile;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DashboardController.ViewsClass Views
    {
      get
      {
        return DashboardController.s_views;
      }
    }

    public DashboardController(IRepository<DashboardFilter> filterRepository, IDashboardPermitService dashboardPermitService, IExportDashboardService exportDashboardPermitService, IService<Contact> contactService)
    {
      this._filterRepository = filterRepository;
      this._dashboardPermitService = dashboardPermitService;
      this._exportDashboardPermitService = exportDashboardPermitService;
      this._contactService = contactService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected DashboardController(Dummy d)
    {
    }

    [HttpGet]
    public virtual ActionResult Index(DashboardMode? mode = null, int? permitId = null)
    {
      DashboardSearchForm dashboardSearchForm = new DashboardSearchForm();
      int num = mode.HasValue ? (int)mode.GetValueOrDefault() : 0;

      dashboardSearchForm.DisplayMode = (DashboardMode)num;
      dashboardSearchForm.SpatialFilterForm = new DashboardSpatialFilterForm();
      switch (dashboardSearchForm.DisplayMode)
      {
        case DashboardMode.Tabular:
        case DashboardMode.Map:
        case DashboardMode.Analysis:
          if (dashboardSearchForm.DisplayMode == DashboardMode.Analysis)
          {
            if (!permitId.HasValue)
              return (ActionResult) this.RedirectToAction(MVC.Permitting.Dashboard.Index(new DashboardMode?(DashboardMode.Tabular), new int?()));
            dashboardSearchForm.SelectedPermit = this._dashboardPermitService.GetById(permitId.Value);
          }
          bool includeCustomFilter = this._filterRepository.Count((IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("UserId == " + (object) SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserUserId), new object[0])
          }) > 0;
          object sessionVar = SessionHandler.GetSessionVar(SessionVariables.SelectedDashboardFilterTemplate);
          if (sessionVar != null)
          {
            dashboardSearchForm.SelectedFilterTemplate = (FilterTemplate) sessionVar;
            dashboardSearchForm.FilterForm = SessionHandler.GetSessionVar<DashboardFilterForm>(SessionVariables.DashboardFilter);
            dashboardSearchForm.SpatialFilterForm = SessionHandler.GetSessionVar<DashboardSpatialFilterForm>(SessionVariables.DashboardSpatialFilter);
          }
          else if (includeCustomFilter)
          {
            dashboardSearchForm.SelectedFilterTemplate = FilterTemplate.MySavedFilter;
            dashboardSearchForm.FilterForm = this.RetrieveSavedFilter();
          }
          else
          {
            dashboardSearchForm.SelectedFilterTemplate = FilterTemplate.Permitting;
            dashboardSearchForm.FilterForm = new DashboardFilterForm(dashboardSearchForm.SelectedFilterTemplate);
          }
          SessionHandler.SetSessionVar(SessionVariables.SelectedDashboardFilterTemplate, (object) dashboardSearchForm.SelectedFilterTemplate);
          SessionHandler.SetSessionVar(SessionVariables.DashboardFilter, (object) dashboardSearchForm.FilterForm);
          SessionHandler.SetSessionVar(SessionVariables.DashboardSpatialFilter, (object) dashboardSearchForm.SpatialFilterForm);
          dashboardSearchForm.Rehydrate(includeCustomFilter);
          return (ActionResult) this.View((object) dashboardSearchForm);
        default:
          return (ActionResult) this.RedirectToAction(MVC.Home.Index());
      }
    }

    [HttpGet]
    public virtual JsonResult ExportToExcel()
    {
      List<int> list = SessionHandler.GetSessionVar<IEnumerable<int>>(SessionVariables.PermitIds).ToList<int>();
      if (list == null || !list.Any<int>())
        return this.Json((object) new
        {
          ReportParameterId = 0
        }, JsonRequestBehavior.AllowGet);
      int reportParameterId = this._exportDashboardPermitService.GetReportParameterId((IEnumerable<int>) list);
      string str = "http://" + ConfigurationManager.AppSettings["ReportsServer"].ToString() + "/ReportServer/Pages/ReportViewer.aspx?%2fWSIPS%2fPermitSearchResultReport&ReportParameterId=" + (object) reportParameterId;
      return this.Json((object) new
      {
        ReportParameterId = reportParameterId,
        Url = str
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual ActionResult GenerateAveryLabels()
    {
      DashboardFilterForm sessionVar1 = SessionHandler.GetSessionVar<DashboardFilterForm>(SessionVariables.DashboardFilter);
      if (sessionVar1 == null)
        return (ActionResult) this.Content("No Data was found");
      string sessionVar2 = SessionHandler.GetSessionVar<string>(SessionVariables.DashboardSearch);
      DashboardFilterWrapper filterWrapper = new DashboardFilterWrapper()
      {
        Filters = sessionVar1.BuildFilter().Concat<DynamicFilter>(DashboardController.BuildSearchFilter(sessionVar2)),
        FailedToReportTwoPeriods = sessionVar1.PermitReportingFilter != null && sessionVar1.PermitReportingFilter.FailedToReportTwoPeriods
      };
      DashboardSpatialFilterForm sessionVar3 = SessionHandler.GetSessionVar<DashboardSpatialFilterForm>(SessionVariables.DashboardSpatialFilter);
      if (sessionVar3 != null)
        filterWrapper.SpatialFilters = sessionVar3.BuildFilter();
      DashboardSearchResult range = this._dashboardPermitService.GetRange(0, int.MaxValue, (string) null, filterWrapper, (string) null);
      if (range == null || !range.DashboardPermits.Any<DashboardPermit>())
        return (ActionResult) this.Content("No Data was found");
      IEnumerable<int> ints = range.DashboardPermits.Where<DashboardPermit>((Func<DashboardPermit, bool>) (x => x.PermitteeContactId.HasValue)).Select<DashboardPermit, int>((Func<DashboardPermit, int>) (x => x.PermitteeContactId.Value)).Distinct<int>();
      iTextSharp.text.Document document = new iTextSharp.text.Document();
      document.SetMargins(5f, 5f, 5f, 5f);
      MemoryStream memoryStream = new MemoryStream();
      PdfWriter instance = PdfWriter.GetInstance(document, (Stream) memoryStream);
      document.Open();
      PdfPTable pdfPtable = new PdfPTable(2)
      {
        WidthPercentage = 100f
      };
      pdfPtable.DefaultCell.Border = 0;
      BaseFont font = BaseFont.CreateFont("Helvetica", "Cp1252", false);
      IService<Contact> contactService = this._contactService;
      int skip = 0;
      int maxValue = int.MaxValue;
      DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]
      {
        new DynamicFilter("@0.Contains(outerIt.Id)", new object[1]
        {
          (object) ints
        })
      };
      string includeProperties = "LU_State";
      foreach (Contact contact in contactService.GetRange(skip, maxValue, null, (IEnumerable<DynamicFilter>) dynamicFilterArray, includeProperties))
      {
        PdfPCell pdfPcell = new PdfPCell();
        pdfPcell.Border = 0;
        pdfPcell.PaddingLeft = 40f;
        pdfPcell.FixedHeight = (float) (((double) document.PageSize.Height - 10.0) / 7.0);
        pdfPcell.VerticalAlignment = 5;
        PdfPCell cell = pdfPcell;
        Paragraph paragraph1 = new Paragraph();
        paragraph1.Add(contact.IsBusiness ? (IElement) new Chunk(string.Format("{0}\n", (object) contact.BusinessName), new iTextSharp.text.Font(font, 12f)) : (IElement) new Chunk(string.Format("{0} {1} {2}\n", (object) contact.FirstName, (object) contact.MiddleInitial, (object) contact.LastName), new iTextSharp.text.Font(font, 12f)));
        paragraph1.Add((IElement) new Chunk(string.Format("{0}\n", (object) contact.Address1), new iTextSharp.text.Font(font, 12f)));
        Paragraph paragraph2 = paragraph1;
        if (!string.IsNullOrEmpty(contact.Address2))
          paragraph2.Add((IElement) new Chunk(string.Format("{0}\n", (object) contact.Address2), new iTextSharp.text.Font(font, 12f)));
        paragraph2.Add((IElement) new Chunk(string.Format("{0}, {1} {2}", (object) contact.City, contact.LU_State == null ? (object) string.Empty : (object) contact.LU_State.Description, (object) contact.ZipCode), new iTextSharp.text.Font(font, 12f)));
        cell.AddElement((IElement) paragraph2);
        pdfPtable.AddCell(cell);
      }
      pdfPtable.CompleteRow();
      document.Add((IElement) pdfPtable);
      instance.CloseStream = false;
      document.Close();
      memoryStream.Position = 0L;
      return (ActionResult) this.File((Stream) memoryStream, "application/pdf");
    }

    [HttpGet]
    public virtual JsonResult Data(GridCommand command)
    {
      DashboardFilterForm sessionVar1 = SessionHandler.GetSessionVar<DashboardFilterForm>(SessionVariables.DashboardFilter);
      if (sessionVar1 == null)
        return this.Json((object) new
        {
          GridModel = new GridData()
          {
            Data = (IEnumerable) null,
            PageTotal = 0,
            CurrentPage = 0,
            RecordCount = 0
          }
        }, JsonRequestBehavior.AllowGet);
      Dictionary<string, string> dictionary = new Dictionary<string, string>()
      {
        {
          "permitname",
          "PermitName"
        },
        {
          "county",
          "CountyCode"
        },
        {
          "permittee",
          "PermitteeName"
        },
        {
          "permitcategory",
          "PermitCategory"
        },
        {
          "permitstatus",
          "PermitStatus"
        },
        {
          "islarge",
          "IsLargePermit"
        },
        {
          "reqadvertising",
          "RequiresAdvertising"
        },
        {
          "dayspending",
          "DaysPending"
        },
        {
          "projectmanager",
          "ProjectManagerName"
        },
        {
          "date",
          "Date"
        }
      };
      string sessionVar2 = SessionHandler.GetSessionVar<string>(SessionVariables.DashboardSearch);
      DashboardFilterWrapper filterWrapper = new DashboardFilterWrapper()
      {
        Filters = sessionVar1.BuildFilter().Concat<DynamicFilter>(DashboardController.BuildSearchFilter(sessionVar2)),
        FailedToReportTwoPeriods = sessionVar1.PermitReportingFilter != null && sessionVar1.PermitReportingFilter.FailedToReportTwoPeriods
      };
      DashboardSpatialFilterForm sessionVar3 = SessionHandler.GetSessionVar<DashboardSpatialFilterForm>(SessionVariables.DashboardSpatialFilter);
      if (sessionVar3 != null)
        filterWrapper.SpatialFilters = sessionVar3.BuildFilter();
      DashboardSearchResult range = this._dashboardPermitService.GetRange(command.Skip(), command.PageSize, command.BuildSort((IDictionary<string, string>) dictionary) ?? "PermitName", filterWrapper, (string) null);
      object obj1 = (object) null;
      if (range.Feature != null)
        obj1 = new EsriJsonWktParser(range.Feature.WellKnownText, range.Feature.CoordinateSystemId).Parse();
      if (!range.PermitIds.Any<int>())
      {
        object obj2 = obj1;
        GridData gridData = new GridData()
        {
          Data = (IEnumerable) null,
          PageTotal = 0,
          CurrentPage = 0,
          RecordCount = 0
        };
        return this.Json((object) new
        {
          SpatialFilterFeature = obj2,
          GridModel = gridData
        }, JsonRequestBehavior.AllowGet);
      }
      int num1 = range.PermitIds.Count<int>();
      SessionHandler.SetSessionVar(SessionVariables.PermitIds, (object) range.PermitIds);
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      IEnumerable<int> permitIds = range.PermitIds;
      object obj3 = obj1;
      GridData gridData1 = new GridData()
      {
        Data = (IEnumerable) range.DashboardPermits,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      };
      return this.Json((object) new
      {
        PermitIds = permitIds,
        SpatialFilterFeature = obj3,
        GridModel = gridData1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult Filter(DashboardFilterForm form)
    {
      if (form != null)
        SessionHandler.SetSessionVar(SessionVariables.DashboardFilter, (object) form);
      return this.Json((object) new
      {
        IsSaveFilterResult = false,
        IsSuccess = true
      });
    }

    [HttpPost]
    public virtual JsonResult SpatialFilter(DashboardSpatialFilterForm form)
    {
      if (form != null)
        SessionHandler.SetSessionVar(SessionVariables.DashboardSpatialFilter, (object) form);
      return this.Json((object) new{ IsSuccess = true });
    }

    [HttpPost]
    public virtual JsonResult Search(string search)
    {
      SessionHandler.SetSessionVar(SessionVariables.DashboardSearch, (object) search);
      return this.Json((object) new{ IsSuccess = true });
    }

    [HttpPost]
    public virtual JsonResult SaveFilter(DashboardFilterForm form)
    {
      SessionHandler.SetSessionVar(SessionVariables.SelectedDashboardFilterTemplate, (object) FilterTemplate.MySavedFilter);
      int sessionVar = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserUserId);
      DashboardFilter dashboardFilter = this._filterRepository.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("UserId == " + (object) sessionVar, new object[0])
      }, (string) null).SingleOrDefault<DashboardFilter>();
      if (dashboardFilter == null)
        dashboardFilter = new DashboardFilter()
        {
          LastModifiedBy = this.HttpContext.User.Identity.Name
        };
      DashboardFilter entity = dashboardFilter;
      entity.UserId = sessionVar;
      using (StringWriter stringWriter = new StringWriter())
      {
        using (XmlWriter xmlWriter = XmlWriter.Create((TextWriter) stringWriter))
          new XmlSerializer(form.GetType()).Serialize(xmlWriter, (object) form);
        entity.FilterString = stringWriter.ToString();
      }
      this._filterRepository.Save(entity);
      return this.Json((object) new
      {
        IsSaveFilterResult = true,
        IsSuccess = true
      });
    }

    [HttpGet]
    public virtual PartialViewResult GetFilterControl(FilterTemplate filterTemplate)
    {
      SessionHandler.SetSessionVar(SessionVariables.SelectedDashboardFilterTemplate, (object) filterTemplate);
      DashboardFilterForm dashboardFilterForm;
      if (filterTemplate == FilterTemplate.MySavedFilter)
      {
        dashboardFilterForm = this.RetrieveSavedFilter();
        if (dashboardFilterForm == null)
          throw new InvalidOperationException("Saved filter does not exist");
      }
      else
        dashboardFilterForm = new DashboardFilterForm(filterTemplate);
      dashboardFilterForm.Rehydrate();
      return this.PartialView(typeof (DashboardFilterForm).Name, (object) dashboardFilterForm);
    }

    private static IEnumerable<DynamicFilter> BuildSearchFilter(string searchText)
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (!string.IsNullOrEmpty(searchText))
        /*dynamicFilterList.Add(new DynamicFilter("PermitName.Contains(@0) || PermitContacts.Any(((ContactTypeId == 29 || IsPermittee) && (PermitContactInformation.FirstName.Contains(@0) || PermitContactInformation.LastName.Contains(@0) || PermitContactInformation.SecondaryName.Contains(@0) || PermitContactInformation.BusinessName.Contains(@0))) || ((ContactTypeId == 24) && (Contact.FirstName.Contains(@0) || Contact.LastName.Contains(@0) || Contact.SecondaryName.Contains(@0) || Contact.BusinessName.Contains(@0))))", new object[1]*/
        dynamicFilterList.Add(new DynamicFilter("PermitName.Contains(@0) || PermitContacts.Any(((ContactTypeId == 29 || IsPermittee) && (PermitContactInformation.FirstName.Contains(@0) || PermitContactInformation.LastName.Contains(@0) || PermitContactInformation.SecondaryName.Contains(@0) || PermitContactInformation.BusinessName.Contains(@0))) || ((ContactTypeId == 24) && (Contact.FirstName.Contains(@0) || Contact.LastName.Contains(@0) || Contact.BusinessName.Contains(@0))))", new object[1]
        {
          (object) searchText
        }));
      return (IEnumerable<DynamicFilter>) dynamicFilterList;
    }

    private DashboardFilterForm RetrieveSavedFilter()
    {
      DashboardFilter dashboardFilter = this._filterRepository.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("UserId == " + (object) SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserUserId), new object[0])
      }, (string) null).SingleOrDefault<DashboardFilter>();
      if (dashboardFilter == null)
        return (DashboardFilterForm) null;
      using (StringReader stringReader = new StringReader(dashboardFilter.FilterString))
      {
        using (XmlReader xmlReader = XmlReader.Create((TextReader) stringReader))
          return new XmlSerializer(typeof (DashboardFilterForm)).Deserialize(xmlReader) as DashboardFilterForm;
      }
    }

    [HttpPost]
    public virtual JsonResult BuildKmlFile(double[][][] geometry)
    {
      MemoryStream memoryStream = new MemoryStream();
      using (XmlWriter xmlWriter = XmlWriter.Create((Stream) memoryStream))
      {
        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("kml", "http://www.opengis.net/kml/2.2");
        xmlWriter.WriteStartElement("Document");
        xmlWriter.WriteStartElement("name");
        xmlWriter.WriteString("WSIPSKml.kml");
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("Style");
        xmlWriter.WriteStartAttribute("id");
        xmlWriter.WriteString("polyStyle");
        xmlWriter.WriteEndAttribute();
        xmlWriter.WriteStartElement("LineStyle");
        xmlWriter.WriteStartElement("width");
        xmlWriter.WriteString("3");
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("color");
        xmlWriter.WriteString("FF00FFFF");
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("PolyStyle");
        xmlWriter.WriteStartElement("color");
        xmlWriter.WriteString("00000000");
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("Placemark");
        xmlWriter.WriteStartElement("name");
        xmlWriter.WriteString("WSIPS Parcel");
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("styleUrl");
        xmlWriter.WriteString("#polyStyle");
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("Polygon");
        xmlWriter.WriteStartElement("outerBoundaryIs");
        xmlWriter.WriteStartElement("LinearRing");
        xmlWriter.WriteStartElement("coordinates");
        foreach (double[] numArray in ((IEnumerable<double[][]>) geometry).SelectMany<double[][], double[]>((Func<double[][], IEnumerable<double[]>>) (ring => (IEnumerable<double[]>) ring)))
          xmlWriter.WriteString(numArray[0].ToString() + "," + (object) numArray[1] + ",1" + Environment.NewLine);
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
      }
      memoryStream.Seek(0L, SeekOrigin.Begin);
      SessionHandler.SetSessionVar(SessionVariables.KmlFileMemoryStream, (object) memoryStream);
      return this.Json((object) new
      {
        Url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Dashboard.RetrieveKmlFile())
      });
    }

    [HttpGet]
    public virtual FileResult RetrieveKmlFile()
    {
      return (FileResult) this.File((Stream) SessionHandler.GetSessionVar<MemoryStream>(SessionVariables.KmlFileMemoryStream), "application/vnd.google-earth.kml+xml", "WSIPSKml.kml");
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Filter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Filter, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult SpatialFilter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SpatialFilter, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult Search()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Search, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult SaveFilter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SaveFilter, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual PartialViewResult GetFilterControl()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.GetFilterControl, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult BuildKmlFile()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.BuildKmlFile, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string ExportToExcel = "ExportToExcel";
      public readonly string GenerateAveryLabels = "GenerateAveryLabels";
      public readonly string Data = "Data";
      public readonly string Filter = "Filter";
      public readonly string SpatialFilter = "SpatialFilter";
      public readonly string Search = "Search";
      public readonly string SaveFilter = "SaveFilter";
      public readonly string GetFilterControl = "GetFilterControl";
      public readonly string BuildKmlFile = "BuildKmlFile";
      public readonly string RetrieveKmlFile = "RetrieveKmlFile";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string ExportToExcel = "ExportToExcel";
      public const string GenerateAveryLabels = "GenerateAveryLabels";
      public const string Data = "Data";
      public const string Filter = "Filter";
      public const string SpatialFilter = "SpatialFilter";
      public const string Search = "Search";
      public const string SaveFilter = "SaveFilter";
      public const string GetFilterControl = "GetFilterControl";
      public const string BuildKmlFile = "BuildKmlFile";
      public const string RetrieveKmlFile = "RetrieveKmlFile";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Index
    {
      public readonly string mode = "mode";
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string command = "command";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Filter
    {
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SpatialFilter
    {
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Search
    {
      public readonly string search = "search";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_SaveFilter
    {
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetFilterControl
    {
      public readonly string filterTemplate = "filterTemplate";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_BuildKmlFile
    {
      public readonly string geometry = "geometry";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly DashboardController.ViewsClass._ViewNamesClass s_ViewNames = new DashboardController.ViewsClass._ViewNamesClass();
      private static readonly DashboardController.ViewsClass._PartialsClass s_Partials = new DashboardController.ViewsClass._PartialsClass();
      public readonly string AquiferFilterForm = "~/Areas/Permitting/Views/Dashboard/AquiferFilterForm.cshtml";
      public readonly string DashboardFilterForm = "~/Areas/Permitting/Views/Dashboard/DashboardFilterForm.cshtml";
      public readonly string DashboardSpatialFilterForm = "~/Areas/Permitting/Views/Dashboard/DashboardSpatialFilterForm.cshtml";
      public readonly string EnforcementActionFilterForm = "~/Areas/Permitting/Views/Dashboard/EnforcementActionFilterForm.cshtml";
      public readonly string Index = "~/Areas/Permitting/Views/Dashboard/Index.cshtml";
      public readonly string PermitCategoryFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitCategoryFilterForm.cshtml";
      public readonly string PermitComplianceStatusFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitComplianceStatusFilterForm.cshtml";
      public readonly string PermitConditionFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitConditionFilterForm.cshtml";
      public readonly string PermitCountyFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitCountyFilterForm.cshtml";
      public readonly string PermitExpirationDateFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitExpirationDateFilterForm.cshtml";
      public readonly string PermitIssuanceDateFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitIssuanceDateFilterForm.cshtml";
      public readonly string PermitReportingFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitReportingFilterForm.cshtml";
      public readonly string PermitStatusFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitStatusFilterForm.cshtml";
      public readonly string PermitTypeFilterForm = "~/Areas/Permitting/Views/Dashboard/PermitTypeFilterForm.cshtml";
      public readonly string ViolationTypeFilterForm = "~/Areas/Permitting/Views/Dashboard/ViolationTypeFilterForm.cshtml";
      public readonly string WaterUsageFilterForm = "~/Areas/Permitting/Views/Dashboard/WaterUsageFilterForm.cshtml";
      public readonly string WaterWithdrawalPurposeCategoryFilterForm = "~/Areas/Permitting/Views/Dashboard/WaterWithdrawalPurposeCategoryFilterForm.cshtml";
      public readonly string WaterWithdrawalSourceFilterForm = "~/Areas/Permitting/Views/Dashboard/WaterWithdrawalSourceFilterForm.cshtml";

      public DashboardController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return DashboardController.ViewsClass.s_ViewNames;
        }
      }

      public DashboardController.ViewsClass._PartialsClass Partials
      {
        get
        {
          return DashboardController.ViewsClass.s_Partials;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string AquiferFilterForm = "AquiferFilterForm";
        public readonly string DashboardFilterForm = "DashboardFilterForm";
        public readonly string DashboardSpatialFilterForm = "DashboardSpatialFilterForm";
        public readonly string EnforcementActionFilterForm = "EnforcementActionFilterForm";
        public readonly string Index = "Index";
        public readonly string PermitCategoryFilterForm = "PermitCategoryFilterForm";
        public readonly string PermitComplianceStatusFilterForm = "PermitComplianceStatusFilterForm";
        public readonly string PermitConditionFilterForm = "PermitConditionFilterForm";
        public readonly string PermitCountyFilterForm = "PermitCountyFilterForm";
        public readonly string PermitExpirationDateFilterForm = "PermitExpirationDateFilterForm";
        public readonly string PermitIssuanceDateFilterForm = "PermitIssuanceDateFilterForm";
        public readonly string PermitReportingFilterForm = "PermitReportingFilterForm";
        public readonly string PermitStatusFilterForm = "PermitStatusFilterForm";
        public readonly string PermitTypeFilterForm = "PermitTypeFilterForm";
        public readonly string ViolationTypeFilterForm = "ViolationTypeFilterForm";
        public readonly string WaterUsageFilterForm = "WaterUsageFilterForm";
        public readonly string WaterWithdrawalPurposeCategoryFilterForm = "WaterWithdrawalPurposeCategoryFilterForm";
        public readonly string WaterWithdrawalSourceFilterForm = "WaterWithdrawalSourceFilterForm";
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public class _PartialsClass
      {
        private static readonly DashboardController.ViewsClass._PartialsClass._ViewNamesClass s_ViewNames = new DashboardController.ViewsClass._PartialsClass._ViewNamesClass();
        public readonly string DashboardAnalysisView = "~/Areas/Permitting/Views/Dashboard/Partials/DashboardAnalysisView.cshtml";
        public readonly string DashboardMapView = "~/Areas/Permitting/Views/Dashboard/Partials/DashboardMapView.cshtml";
        public readonly string DashboardTabularView = "~/Areas/Permitting/Views/Dashboard/Partials/DashboardTabularView.cshtml";

        public DashboardController.ViewsClass._PartialsClass._ViewNamesClass ViewNames
        {
          get
          {
            return DashboardController.ViewsClass._PartialsClass.s_ViewNames;
          }
        }

        public class _ViewNamesClass
        {
          public readonly string DashboardAnalysisView = "DashboardAnalysisView";
          public readonly string DashboardMapView = "DashboardMapView";
          public readonly string DashboardTabularView = "DashboardTabularView";
        }
      }
    }
  }
}
