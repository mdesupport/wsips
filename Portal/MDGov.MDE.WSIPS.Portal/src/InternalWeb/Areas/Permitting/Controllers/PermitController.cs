﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.PermitController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.ServiceClient;
using Microsoft.CSharp.RuntimeBinder;
using StructureMap;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization]
  public class PermitController : BaseController
  {
    private static readonly PermitController.ActionNamesClass s_actions = new PermitController.ActionNamesClass();
    private static readonly PermitController.ActionParamsClass_Index s_params_Index = new PermitController.ActionParamsClass_Index();
    private static readonly PermitController.ActionParamsClass_GetPermit s_params_GetPermit = new PermitController.ActionParamsClass_GetPermit();
    private static readonly PermitController.ActionParamsClass_GetPermitId s_params_GetPermitId = new PermitController.ActionParamsClass_GetPermitId();
    private static readonly PermitController.ActionParamsClass_LoadDetailsTab s_params_LoadDetailsTab = new PermitController.ActionParamsClass_LoadDetailsTab();
    private static readonly PermitController.ActionParamsClass_LoadReviewTab s_params_LoadReviewTab = new PermitController.ActionParamsClass_LoadReviewTab();
    private static readonly PermitController.ActionParamsClass_LoadConditionsTab s_params_LoadConditionsTab = new PermitController.ActionParamsClass_LoadConditionsTab();
    private static readonly PermitController.ActionParamsClass_LoadIPLTab s_params_LoadIPLTab = new PermitController.ActionParamsClass_LoadIPLTab();
    private static readonly PermitController.ActionParamsClass_GetIplContact s_params_GetIplContact = new PermitController.ActionParamsClass_GetIplContact();
    private static readonly PermitController.ActionParamsClass_LoadCCListTab s_params_LoadCCListTab = new PermitController.ActionParamsClass_LoadCCListTab();
    private static readonly PermitController.ActionParamsClass_LoadPumpageContactTab s_params_LoadPumpageContactTab = new PermitController.ActionParamsClass_LoadPumpageContactTab();
    private static readonly PermitController.ActionParamsClass_LoadDocumentsTab s_params_LoadDocumentsTab = new PermitController.ActionParamsClass_LoadDocumentsTab();
    private static readonly PermitController.ActionParamsClass_LoadCommentsTab s_params_LoadCommentsTab = new PermitController.ActionParamsClass_LoadCommentsTab();
    private static readonly PermitController.ActionParamsClass_LoadComplianceTab s_params_LoadComplianceTab = new PermitController.ActionParamsClass_LoadComplianceTab();
    private static readonly PermitController.ActionParamsClass_LoadEnforcementTab s_params_LoadEnforcementTab = new PermitController.ActionParamsClass_LoadEnforcementTab();
    private static readonly PermitController.ActionParamsClass_LoadContactsTab s_params_LoadContactsTab = new PermitController.ActionParamsClass_LoadContactsTab();
    private static readonly PermitController.ActionParamsClass_GetPermitHistory s_params_GetPermitHistory = new PermitController.ActionParamsClass_GetPermitHistory();
    private static readonly PermitController.ActionParamsClass_SavePermitType s_params_SavePermitType = new PermitController.ActionParamsClass_SavePermitType();
    private static readonly PermitController.ActionParamsClass_SearchSupplementalGroupGridData s_params_SearchSupplementalGroupGridData = new PermitController.ActionParamsClass_SearchSupplementalGroupGridData();
    private static readonly PermitController.ActionParamsClass_ViewSupplementalGroupGridData s_params_ViewSupplementalGroupGridData = new PermitController.ActionParamsClass_ViewSupplementalGroupGridData();
    private static readonly PermitController.ActionParamsClass_SetSupplementalGroupType s_params_SetSupplementalGroupType = new PermitController.ActionParamsClass_SetSupplementalGroupType();
    private static readonly PermitController.ActionParamsClass_AddToSupplementalGroup s_params_AddToSupplementalGroup = new PermitController.ActionParamsClass_AddToSupplementalGroup();
    private static readonly PermitController.ActionParamsClass_RemoveFromSupplementalGroup s_params_RemoveFromSupplementalGroup = new PermitController.ActionParamsClass_RemoveFromSupplementalGroup();
    private static readonly PermitController.ActionParamsClass_CreateNewSupplementalGroup s_params_CreateNewSupplementalGroup = new PermitController.ActionParamsClass_CreateNewSupplementalGroup();
    private static readonly PermitController.ActionParamsClass_DenyPermit s_params_DenyPermit = new PermitController.ActionParamsClass_DenyPermit();
    private static readonly PermitController.ActionParamsClass_ApprovePermit s_params_ApprovePermit = new PermitController.ActionParamsClass_ApprovePermit();
    private static readonly PermitController.ActionParamsClass_SetPermitDates s_params_SetPermitDates = new PermitController.ActionParamsClass_SetPermitDates();
    private static readonly PermitController.ActionParamsClass_GetPermitDates s_params_GetPermitDates = new PermitController.ActionParamsClass_GetPermitDates();
    private static readonly PermitController.ActionParamsClass_DateElevenMonthsElevenYears s_params_DateElevenMonthsElevenYears = new PermitController.ActionParamsClass_DateElevenMonthsElevenYears();
    private static readonly PermitController.ActionParamsClass_LoadAdditionalLandOwners s_params_LoadAdditionalLandOwners = new PermitController.ActionParamsClass_LoadAdditionalLandOwners();
    private static readonly PermitController.ActionParamsClass_LoadPermitHearing s_params_LoadPermitHearing = new PermitController.ActionParamsClass_LoadPermitHearing();
    private static readonly PermitController.ActionParamsClass_SavePermitHearing s_params_SavePermitHearing = new PermitController.ActionParamsClass_SavePermitHearing();
    private static readonly PermitController.ActionParamsClass_DeletePermitHearing s_params_DeletePermitHearing = new PermitController.ActionParamsClass_DeletePermitHearing();
    private static readonly PermitController.ActionParamsClass_GetPermitHearing s_params_GetPermitHearing = new PermitController.ActionParamsClass_GetPermitHearing();
    private static readonly PermitController.ActionParamsClass_GetPermitHearingByPermitId s_params_GetPermitHearingByPermitId = new PermitController.ActionParamsClass_GetPermitHearingByPermitId();
    private static readonly PermitController.ActionParamsClass_AddPumpageContact s_params_AddPumpageContact = new PermitController.ActionParamsClass_AddPumpageContact();
    private static readonly PermitController.ActionParamsClass_RemovePermitContact s_params_RemovePermitContact = new PermitController.ActionParamsClass_RemovePermitContact();
    private static readonly PermitController.ActionParamsClass_LoadChecklist s_params_LoadChecklist = new PermitController.ActionParamsClass_LoadChecklist();
    private static readonly PermitController.ActionParamsClass_SaveContactsInDetailsContactsTab s_params_SaveContactsInDetailsContactsTab = new PermitController.ActionParamsClass_SaveContactsInDetailsContactsTab();
    private static readonly PermitController.ActionParamsClass_LoadPermitInformation s_params_LoadPermitInformation = new PermitController.ActionParamsClass_LoadPermitInformation();
    private static readonly PermitController.ActionParamsClass_SavePermitInformation s_params_SavePermitInformation = new PermitController.ActionParamsClass_SavePermitInformation();
    private static readonly PermitController.ActionParamsClass_LoadApprovedAllocation s_params_LoadApprovedAllocation = new PermitController.ActionParamsClass_LoadApprovedAllocation();
    private static readonly PermitController.ActionParamsClass_SaveApprovedAllocation s_params_SaveApprovedAllocation = new PermitController.ActionParamsClass_SaveApprovedAllocation();
    private static readonly PermitController.ActionParamsClass_GenerateAveryLabelsForIPL s_params_GenerateAveryLabelsForIPL = new PermitController.ActionParamsClass_GenerateAveryLabelsForIPL();
    private static readonly PermitController.ActionParamsClass_CopyConditions s_params_CopyConditions = new PermitController.ActionParamsClass_CopyConditions();
    private static readonly PermitController.ActionParamsClass_LoadApplicationsInProgress s_params_LoadApplicationsInProgress = new PermitController.ActionParamsClass_LoadApplicationsInProgress();
    private static readonly PermitController.ActionParamsClass_ApplicationWizard s_params_ApplicationWizard = new PermitController.ActionParamsClass_ApplicationWizard();
    private static readonly PermitController.ActionParamsClass_CancelPermitApplication s_params_CancelPermitApplication = new PermitController.ActionParamsClass_CancelPermitApplication();
    private static readonly PermitController.ActionParamsClass_LinksModal s_params_LinksModal = new PermitController.ActionParamsClass_LinksModal();
    private static readonly PermitController.ActionParamsClass_CreatePendingPermit s_params_CreatePendingPermit = new PermitController.ActionParamsClass_CreatePendingPermit();
    private static readonly PermitController.ActionParamsClass_GetPendingPermitCount s_params_GetPendingPermitCount = new PermitController.ActionParamsClass_GetPendingPermitCount();
    private static readonly PermitController.ActionParamsClass_GeneratePermitNumber s_params_GeneratePermitNumber = new PermitController.ActionParamsClass_GeneratePermitNumber();
    private static readonly PermitController.ActionParamsClass_IsUniquePermitNumber s_params_IsUniquePermitNumber = new PermitController.ActionParamsClass_IsUniquePermitNumber();
    private static readonly PermitController.ActionParamsClass_ClonePermit s_params_ClonePermit = new PermitController.ActionParamsClass_ClonePermit();
    private static readonly PermitController.ActionParamsClass_GetPermitNumber s_params_GetPermitNumber = new PermitController.ActionParamsClass_GetPermitNumber();
    private static readonly PermitController.ActionParamsClass_SetPermitNumber s_params_SetPermitNumber = new PermitController.ActionParamsClass_SetPermitNumber();
    private static readonly PermitController.ActionParamsClass_GetPermitStatusesByCategory s_params_GetPermitStatusesByCategory = new PermitController.ActionParamsClass_GetPermitStatusesByCategory();
    private static readonly PermitController.ActionParamsClass_SetPermitStatus s_params_SetPermitStatus = new PermitController.ActionParamsClass_SetPermitStatus();
    private static readonly PermitController.ActionParamsClass_SetPreviousPermitStatus s_params_SetPreviousPermitStatus = new PermitController.ActionParamsClass_SetPreviousPermitStatus();
    private static readonly PermitController.ActionParamsClass_SearchContact s_params_SearchContact = new PermitController.ActionParamsClass_SearchContact();
    private static readonly PermitController.ActionParamsClass_AddPermitLocation s_params_AddPermitLocation = new PermitController.ActionParamsClass_AddPermitLocation();
    private static readonly PermitController.ActionParamsClass_RemovePermitLocation s_params_RemovePermitLocation = new PermitController.ActionParamsClass_RemovePermitLocation();
    private static readonly PermitController.ActionParamsClass_AddWaterWithdrawalLocation s_params_AddWaterWithdrawalLocation = new PermitController.ActionParamsClass_AddWaterWithdrawalLocation();
    private static readonly PermitController.ActionParamsClass_GetConservationEasement s_params_GetConservationEasement = new PermitController.ActionParamsClass_GetConservationEasement();
    private static readonly PermitController.ActionParamsClass_SetConservationEasement s_params_SetConservationEasement = new PermitController.ActionParamsClass_SetConservationEasement();
    private static readonly PermitController.ActionParamsClass_EditWaterWithdrawalLocation s_params_EditWaterWithdrawalLocation = new PermitController.ActionParamsClass_EditWaterWithdrawalLocation();
    private static readonly PermitController.ActionParamsClass_RemoveWaterWithdrawalLocation s_params_RemoveWaterWithdrawalLocation = new PermitController.ActionParamsClass_RemoveWaterWithdrawalLocation();
    private static readonly PermitController.ActionParamsClass_Summary s_params_Summary = new PermitController.ActionParamsClass_Summary();
    private static readonly PermitController.ActionParamsClass_Affirmation s_params_Affirmation = new PermitController.ActionParamsClass_Affirmation();
    private static readonly PermitController.ActionParamsClass_UpdateStatus s_params_UpdateStatus = new PermitController.ActionParamsClass_UpdateStatus();
    private static readonly PermitController.ActionParamsClass_GenerateComplianceReports s_params_GenerateComplianceReports = new PermitController.ActionParamsClass_GenerateComplianceReports();
    private static readonly PermitController.ActionParamsClass_DeleteComplianceReports s_params_DeleteComplianceReports = new PermitController.ActionParamsClass_DeleteComplianceReports();
    private static readonly PermitController.ActionParamsClass_CreatePdf s_params_CreatePdf = new PermitController.ActionParamsClass_CreatePdf();
    private static readonly PermitController.ViewsClass s_views = new PermitController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Permitting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Permit";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Permit";
    private readonly IMembershipAuthenticationService _membershipService;
    private readonly IPermitService _permittingService;
    private readonly IService<Contact> _contactService;
    private readonly IService<ContactCommunicationMethod> _contactCommunicationMethod;
    private readonly IPermitServiceClient _permitServiceClient;
    private readonly IService<LU_WaterWithdrawalSource> _waterWithdrawalSourceService;
    private readonly IService<LU_WaterWithdrawalType> _waterWithdrawalTypeService;
    private readonly IUpdatableService<PermitWithdrawalGroundwater> _permitWithdrawalGroundwaterService;
    private readonly IUpdatableService<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterService;
    private readonly IWaterUseDetailServiceClient _waterUseDetailsServiceClient;
    private readonly IUpdatableService<PermitContact> _permitContactService;
    private readonly IService<ProjectManagerCounty> _projectManagerCountyService;
    private readonly IService<LU_WaterWithdrawalPurposeCategory> _waterWithdrawalPurposeCategory;
    private readonly IPermitLocationService _permitLocationService;
    private readonly IUpdatableService<PermitWaterWithdrawalLocation> _permitWaterWithdrawalLocationService;
    private readonly IService<LU_CheckListAnswer> _answerService;
    private readonly IService<CheckListQuestion> _checkListQuestionService;
    private readonly IService<CheckListAnswer> _checkListAnswerService;
    private readonly IService<CheckList> _permitCheckListService;
    private readonly IService<LU_StandardCondition> _standardConditionService;
    private readonly IUpdatableService<ContactCommunicationMethod> _contactCommunicationMethodService;
    private readonly IService<LU_PermitType> _permitTypeService;
    private readonly IUpdatableService<PermitStatusHistory> _permitStatusHistoryService;
    private readonly IUpdatableService<PermitHearing> _permitHearingService;
    private readonly IService<Secretary> _secretaryService;
    private readonly IService<AdministrativeSpecialist> _administrativeSpecialistService;
    private readonly IContactServiceClient _contactServiceClient;
    private readonly IService<LU_Aquifer> _aquiferService;
    private readonly IService<LU_WaterType> _waterTypeService;
    private readonly IService<LU_TidalType> _tidalTypeService;
    private readonly IUpdatableService<PermitStatusState> _permitStatusStateService;
    private readonly IService<LU_PermitTemplate> _permitTemplateService;
    private readonly IUpdatableService<PermitCondition> _permitConditionService;
    private readonly IUpdatableService<PermitSupplementalGroup> _permitSupplementalGroupService;
    private readonly IUpdatableService<SupplementalGroup> _supplementalGroupService;
    private readonly IUpdatableService<ConditionCompliance> _conditionComplianceService;
    private readonly IStateStreamService _stateStreamService;
    private readonly IService<LU_SupplementalType> _supplementalTypeService;
    private readonly IContactTypeServiceClient _contactTypeServiceClient;
    private readonly IService<LU_PermitCategory> _permitCatagoryService;

    public IUpdatableService<Message> MessageService { get; set; }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController Actions
    {
      get
      {
        return MVC.Permitting.Permit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionNamesClass ActionNames
    {
      get
      {
        return PermitController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_Index IndexParams
    {
      get
      {
        return PermitController.s_params_Index;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_GetPermit GetPermitParams
    {
      get
      {
        return PermitController.s_params_GetPermit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetPermitId GetPermitIdParams
    {
      get
      {
        return PermitController.s_params_GetPermitId;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadDetailsTab LoadDetailsTabParams
    {
      get
      {
        return PermitController.s_params_LoadDetailsTab;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadReviewTab LoadReviewTabParams
    {
      get
      {
        return PermitController.s_params_LoadReviewTab;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadConditionsTab LoadConditionsTabParams
    {
      get
      {
        return PermitController.s_params_LoadConditionsTab;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadIPLTab LoadIPLTabParams
    {
      get
      {
        return PermitController.s_params_LoadIPLTab;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetIplContact GetIplContactParams
    {
      get
      {
        return PermitController.s_params_GetIplContact;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadCCListTab LoadCCListTabParams
    {
      get
      {
        return PermitController.s_params_LoadCCListTab;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadPumpageContactTab LoadPumpageContactTabParams
    {
      get
      {
        return PermitController.s_params_LoadPumpageContactTab;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadDocumentsTab LoadDocumentsTabParams
    {
      get
      {
        return PermitController.s_params_LoadDocumentsTab;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadCommentsTab LoadCommentsTabParams
    {
      get
      {
        return PermitController.s_params_LoadCommentsTab;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadComplianceTab LoadComplianceTabParams
    {
      get
      {
        return PermitController.s_params_LoadComplianceTab;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadEnforcementTab LoadEnforcementTabParams
    {
      get
      {
        return PermitController.s_params_LoadEnforcementTab;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadContactsTab LoadContactsTabParams
    {
      get
      {
        return PermitController.s_params_LoadContactsTab;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetPermitHistory GetPermitHistoryParams
    {
      get
      {
        return PermitController.s_params_GetPermitHistory;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_SavePermitType SavePermitTypeParams
    {
      get
      {
        return PermitController.s_params_SavePermitType;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_SearchSupplementalGroupGridData SearchSupplementalGroupGridDataParams
    {
      get
      {
        return PermitController.s_params_SearchSupplementalGroupGridData;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_ViewSupplementalGroupGridData ViewSupplementalGroupGridDataParams
    {
      get
      {
        return PermitController.s_params_ViewSupplementalGroupGridData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_SetSupplementalGroupType SetSupplementalGroupTypeParams
    {
      get
      {
        return PermitController.s_params_SetSupplementalGroupType;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_AddToSupplementalGroup AddToSupplementalGroupParams
    {
      get
      {
        return PermitController.s_params_AddToSupplementalGroup;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_RemoveFromSupplementalGroup RemoveFromSupplementalGroupParams
    {
      get
      {
        return PermitController.s_params_RemoveFromSupplementalGroup;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_CreateNewSupplementalGroup CreateNewSupplementalGroupParams
    {
      get
      {
        return PermitController.s_params_CreateNewSupplementalGroup;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_DenyPermit DenyPermitParams
    {
      get
      {
        return PermitController.s_params_DenyPermit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_ApprovePermit ApprovePermitParams
    {
      get
      {
        return PermitController.s_params_ApprovePermit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_SetPermitDates SetPermitDatesParams
    {
      get
      {
        return PermitController.s_params_SetPermitDates;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetPermitDates GetPermitDatesParams
    {
      get
      {
        return PermitController.s_params_GetPermitDates;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_DateElevenMonthsElevenYears DateElevenMonthsElevenYearsParams
    {
      get
      {
        return PermitController.s_params_DateElevenMonthsElevenYears;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadAdditionalLandOwners LoadAdditionalLandOwnersParams
    {
      get
      {
        return PermitController.s_params_LoadAdditionalLandOwners;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadPermitHearing LoadPermitHearingParams
    {
      get
      {
        return PermitController.s_params_LoadPermitHearing;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_SavePermitHearing SavePermitHearingParams
    {
      get
      {
        return PermitController.s_params_SavePermitHearing;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_DeletePermitHearing DeletePermitHearingParams
    {
      get
      {
        return PermitController.s_params_DeletePermitHearing;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_GetPermitHearing GetPermitHearingParams
    {
      get
      {
        return PermitController.s_params_GetPermitHearing;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetPermitHearingByPermitId GetPermitHearingByPermitIdParams
    {
      get
      {
        return PermitController.s_params_GetPermitHearingByPermitId;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_AddPumpageContact AddPumpageContactParams
    {
      get
      {
        return PermitController.s_params_AddPumpageContact;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_RemovePermitContact RemovePermitContactParams
    {
      get
      {
        return PermitController.s_params_RemovePermitContact;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadChecklist LoadChecklistParams
    {
      get
      {
        return PermitController.s_params_LoadChecklist;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_SaveContactsInDetailsContactsTab SaveContactsInDetailsContactsTabParams
    {
      get
      {
        return PermitController.s_params_SaveContactsInDetailsContactsTab;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadPermitInformation LoadPermitInformationParams
    {
      get
      {
        return PermitController.s_params_LoadPermitInformation;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_SavePermitInformation SavePermitInformationParams
    {
      get
      {
        return PermitController.s_params_SavePermitInformation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LoadApprovedAllocation LoadApprovedAllocationParams
    {
      get
      {
        return PermitController.s_params_LoadApprovedAllocation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_SaveApprovedAllocation SaveApprovedAllocationParams
    {
      get
      {
        return PermitController.s_params_SaveApprovedAllocation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GenerateAveryLabelsForIPL GenerateAveryLabelsForIPLParams
    {
      get
      {
        return PermitController.s_params_GenerateAveryLabelsForIPL;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_CopyConditions CopyConditionsParams
    {
      get
      {
        return PermitController.s_params_CopyConditions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_LoadApplicationsInProgress LoadApplicationsInProgressParams
    {
      get
      {
        return PermitController.s_params_LoadApplicationsInProgress;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_ApplicationWizard ApplicationWizardParams
    {
      get
      {
        return PermitController.s_params_ApplicationWizard;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_CancelPermitApplication CancelPermitApplicationParams
    {
      get
      {
        return PermitController.s_params_CancelPermitApplication;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_LinksModal LinksModalParams
    {
      get
      {
        return PermitController.s_params_LinksModal;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_CreatePendingPermit CreatePendingPermitParams
    {
      get
      {
        return PermitController.s_params_CreatePendingPermit;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_GetPendingPermitCount GetPendingPermitCountParams
    {
      get
      {
        return PermitController.s_params_GetPendingPermitCount;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_GeneratePermitNumber GeneratePermitNumberParams
    {
      get
      {
        return PermitController.s_params_GeneratePermitNumber;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_IsUniquePermitNumber IsUniquePermitNumberParams
    {
      get
      {
        return PermitController.s_params_IsUniquePermitNumber;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_ClonePermit ClonePermitParams
    {
      get
      {
        return PermitController.s_params_ClonePermit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetPermitNumber GetPermitNumberParams
    {
      get
      {
        return PermitController.s_params_GetPermitNumber;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_SetPermitNumber SetPermitNumberParams
    {
      get
      {
        return PermitController.s_params_SetPermitNumber;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetPermitStatusesByCategory GetPermitStatusesByCategoryParams
    {
      get
      {
        return PermitController.s_params_GetPermitStatusesByCategory;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_SetPermitStatus SetPermitStatusParams
    {
      get
      {
        return PermitController.s_params_SetPermitStatus;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_SetPreviousPermitStatus SetPreviousPermitStatusParams
    {
      get
      {
        return PermitController.s_params_SetPreviousPermitStatus;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_SearchContact SearchContactParams
    {
      get
      {
        return PermitController.s_params_SearchContact;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_AddPermitLocation AddPermitLocationParams
    {
      get
      {
        return PermitController.s_params_AddPermitLocation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_RemovePermitLocation RemovePermitLocationParams
    {
      get
      {
        return PermitController.s_params_RemovePermitLocation;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_AddWaterWithdrawalLocation AddWaterWithdrawalLocationParams
    {
      get
      {
        return PermitController.s_params_AddWaterWithdrawalLocation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GetConservationEasement GetConservationEasementParams
    {
      get
      {
        return PermitController.s_params_GetConservationEasement;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_SetConservationEasement SetConservationEasementParams
    {
      get
      {
        return PermitController.s_params_SetConservationEasement;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_EditWaterWithdrawalLocation EditWaterWithdrawalLocationParams
    {
      get
      {
        return PermitController.s_params_EditWaterWithdrawalLocation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_RemoveWaterWithdrawalLocation RemoveWaterWithdrawalLocationParams
    {
      get
      {
        return PermitController.s_params_RemoveWaterWithdrawalLocation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_Summary SummaryParams
    {
      get
      {
        return PermitController.s_params_Summary;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_Affirmation AffirmationParams
    {
      get
      {
        return PermitController.s_params_Affirmation;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_UpdateStatus UpdateStatusParams
    {
      get
      {
        return PermitController.s_params_UpdateStatus;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_GenerateComplianceReports GenerateComplianceReportsParams
    {
      get
      {
        return PermitController.s_params_GenerateComplianceReports;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ActionParamsClass_DeleteComplianceReports DeleteComplianceReportsParams
    {
      get
      {
        return PermitController.s_params_DeleteComplianceReports;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PermitController.ActionParamsClass_CreatePdf CreatePdfParams
    {
      get
      {
        return PermitController.s_params_CreatePdf;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PermitController.ViewsClass Views
    {
      get
      {
        return PermitController.s_views;
      }
    }

    public PermitController(IMembershipAuthenticationService membershipService, IPermitService permittingService, IService<Contact> contactService, IService<ContactCommunicationMethod> contactCommunicationMethod, IPermitServiceClient permitServiceClient, IService<LU_WaterWithdrawalSource> waterWithdrawalSourceService, IService<LU_WaterWithdrawalType> waterWithdrawalTypeService, IUpdatableService<PermitWithdrawalGroundwater> permitWithdrawalGroundwaterService, IUpdatableService<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterService, IWaterUseDetailServiceClient waterUseDetailsServiceClient, IUpdatableService<PermitContact> permitContactService, IService<ProjectManagerCounty> projectManagerCountyService, IService<LU_WaterWithdrawalPurposeCategory> waterWithdrawalPurposeCategory, IPermitLocationService permitLocationService, IUpdatableService<PermitWaterWithdrawalLocation> permitWaterWithdrawalLocationService, IService<LU_CheckListAnswer> answerService, IService<CheckListQuestion> checkListQuestionService, IService<CheckListAnswer> checkListAnswerService, IService<CheckList> permitCheckListService, IService<LU_StandardCondition> standardConditionService, IUpdatableService<ContactCommunicationMethod> contactCommunicationMethodService, IService<LU_PermitType> permitTypeService, IUpdatableService<PermitStatusHistory> permitStatusHistoryService, IUpdatableService<PermitHearing> permitHearingService, IService<Secretary> secretaryService, IService<AdministrativeSpecialist> administrativeSpecialistService, IContactServiceClient contactServiceClient, IService<LU_Aquifer> aquiferService, IService<LU_WaterType> waterTypeService, IService<LU_TidalType> tidalTypeService, IUpdatableService<PermitStatusState> permitStatusStateService, IService<LU_PermitTemplate> permitTemplateService, IUpdatableService<PermitCondition> permitConditionService, IUpdatableService<PermitSupplementalGroup> permitSupplementalGroupService, IUpdatableService<SupplementalGroup> supplementalGroupService, IUpdatableService<ConditionCompliance> conditionComplianceService, IStateStreamService stateStreamService, IService<LU_SupplementalType> supplementalTypeService, IContactTypeServiceClient contactTypeServiceClient, IService<LU_PermitCategory> permitCatagoryService)
    {
      this._membershipService = membershipService;
      this._permittingService = permittingService;
      this._contactService = contactService;
      this._contactCommunicationMethod = contactCommunicationMethod;
      this._permitServiceClient = permitServiceClient;
      this._waterWithdrawalSourceService = waterWithdrawalSourceService;
      this._waterWithdrawalTypeService = waterWithdrawalTypeService;
      this._permitWithdrawalGroundwaterService = permitWithdrawalGroundwaterService;
      this._permitWithdrawalSurfacewaterService = permitWithdrawalSurfacewaterService;
      this._waterUseDetailsServiceClient = waterUseDetailsServiceClient;
      this._permitContactService = permitContactService;
      this._projectManagerCountyService = projectManagerCountyService;
      this._waterWithdrawalPurposeCategory = waterWithdrawalPurposeCategory;
      this._permitLocationService = permitLocationService;
      this._permitWaterWithdrawalLocationService = permitWaterWithdrawalLocationService;
      this._answerService = answerService;
      this._checkListQuestionService = checkListQuestionService;
      this._checkListAnswerService = checkListAnswerService;
      this._permitCheckListService = permitCheckListService;
      this._standardConditionService = standardConditionService;
      this._contactCommunicationMethodService = contactCommunicationMethodService;
      this._permitTypeService = permitTypeService;
      this._permitStatusHistoryService = permitStatusHistoryService;
      this._permitHearingService = permitHearingService;
      this._secretaryService = secretaryService;
      this._administrativeSpecialistService = administrativeSpecialistService;
      this._contactServiceClient = contactServiceClient;
      this._aquiferService = aquiferService;
      this._waterTypeService = waterTypeService;
      this._tidalTypeService = tidalTypeService;
      this._permitStatusStateService = permitStatusStateService;
      this._permitTemplateService = permitTemplateService;
      this._permitConditionService = permitConditionService;
      this._permitSupplementalGroupService = permitSupplementalGroupService;
      this._supplementalGroupService = supplementalGroupService;
      this._conditionComplianceService = conditionComplianceService;
      this._stateStreamService = stateStreamService;
      this._supplementalTypeService = supplementalTypeService;
      this._contactTypeServiceClient = contactTypeServiceClient;
      this._permitCatagoryService = permitCatagoryService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected PermitController(Dummy d)
    {
    }

    [HttpGet]
    public virtual ActionResult Index(int id, PermitDetailsTab? activeTab = null)
    {
      if (id <= 0)
        return (ActionResult) this.RedirectToAction(MVC.Permitting.Dashboard.Index(new DashboardMode?(), new int?()));
      PermitDetailsForm permitDetailsForm1 = new PermitDetailsForm();
      permitDetailsForm1.PermitId = id;
      PermitDetailsForm permitDetailsForm2 = permitDetailsForm1;
      PermitDetailsTab? nullable = activeTab;
      int num = nullable.HasValue ? (int) nullable.GetValueOrDefault() : 5;
      permitDetailsForm2.ActiveTab = (PermitDetailsTab) num;
      return (ActionResult) this.View((object) permitDetailsForm1);
    }

    [HttpGet]
    public virtual JsonResult GetPermit(int id)
    {
      Permit byId = this._permittingService.GetById(id, "LU_PermitStatus,LU_ApplicationType,LU_PermitType,PermitContacts.PermitContactInformation");
      PermitForm permitForm = new PermitForm() { Id = byId.Id, CategoryId = byId.LU_PermitStatus == null ? 0 : byId.LU_PermitStatus.PermitCategoryId.GetValueOrDefault(), PermitStatus = byId.LU_PermitStatus == null ? "" : byId.LU_PermitStatus.Description, ApplicationTypeDescription = byId.LU_ApplicationType == null ? "" : byId.LU_ApplicationType.Description, PermitTypeId = byId.PermitTypeId, PermitTypeKey = byId.LU_PermitType == null ? "" : byId.LU_PermitType.Key };
      permitForm.PermitName = permitForm.CategoryId == 1 ? "Application" : byId.PermitName ?? "No Permit #";
      PermitContact permitContact = byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
      {
        if (x.IsPermittee)
          return x.IsPrimary;
        return false;
      }));
      if (permitContact != null && permitContact.PermitContactInformation != null)
      {
        if (permitContact.PermitContactInformation.IsBusiness && !string.IsNullOrEmpty(permitContact.PermitContactInformation.BusinessName))
          permitForm.Permittee = permitContact.PermitContactInformation.BusinessName;
        else if (!string.IsNullOrEmpty(permitContact.PermitContactInformation.FirstName) || !string.IsNullOrEmpty(permitContact.PermitContactInformation.LastName))
            permitForm.Permittee = permitContact.PermitContactInformation.FirstName + (string.IsNullOrEmpty(permitContact.PermitContactInformation.MiddleInitial) ? " " : " " + permitContact.PermitContactInformation.MiddleInitial + " ") + permitContact.PermitContactInformation.LastName;
      }
      permitForm.Permittee = string.IsNullOrEmpty(permitForm.Permittee) ? "No Permittee" : permitForm.Permittee;
      permitForm.ProjectName = string.IsNullOrEmpty(byId.ProjectName) ? "No Project Name" : byId.ProjectName;
      return this.Json((object) permitForm, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetPermitId(string permitName)
    {
      int num = 0;
      if (!string.IsNullOrEmpty(permitName))
      {
        Permit permit = this._permittingService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitName.ToLower() == @0", new object[1]{ (object) permitName.ToLower() }) }, (string) null).SingleOrDefault<Permit>();
        if (permit != null)
          num = permit.Id;
      }
      return this.Json((object) num, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual ActionResult LoadDetailsTab(int permitId)
    {
      ApplicationWizard forExistingPermit = this._permitServiceClient.GetApplicationWizardForExistingPermit(permitId);
      Permit byId = this._permittingService.GetById(permitId, "LU_PermitStatus");
      forExistingPermit.ApplicationTypeId = byId.ApplicationTypeId;
      forExistingPermit.PermitId = byId.Id;
      forExistingPermit.PermitCategoryId = byId.LU_PermitStatus.PermitCategoryId ?? 0;
      forExistingPermit.RefId = byId.RefId;
      forExistingPermit.PermitStatusDetailForm.PermitStatusId = byId.PermitStatusId ?? 0;
      forExistingPermit.PermitStatusDetailForm.PermitCategoryId = byId.LU_PermitStatus.PermitCategoryId ?? 0;
      StatusChange lastStatusChange = this._permittingService.GetLastStatusChange(byId.Id, byId.PermitStatusId.GetValueOrDefault());
      if (lastStatusChange != null)
      {
        forExistingPermit.PermitStatusDetailForm.PreviousPermitStatusId = lastStatusChange.PreviousPermitStatusId;
        forExistingPermit.PermitStatusDetailForm.PermitStatusId = lastStatusChange.PermitStatusId;
        forExistingPermit.PermitStatusDetailForm.PermitStatusLastChangedBy = lastStatusChange.ChangedBy;
        forExistingPermit.PermitStatusDetailForm.PermitStatusLastChangedDate = lastStatusChange.ChangedDate;
        forExistingPermit.PermitStatusDetailForm.PermitStatusDescription = byId.LU_PermitStatus.Description;
      }
      forExistingPermit.PermitStatusDetailForm.RefId = byId.Id;
      forExistingPermit.IsExternalApplicaion = byId.ApplicantIdentification.Substring(0, 1) == "E";
      if (SessionHandler.GetSessionVar<IEnumerable<string>>(SessionVariables.AuthenticatedUserRoles).Any<string>((Func<string, bool>) (role =>
      {
        if (!(role == "Permit Supervisor") && !(role == "Division Chief") && !(role == "Administrative Specialist"))
          return role == "Compliance / Enforcement Manager";
        return true;
      })))
        forExistingPermit.PermitStatusDetailForm.HasPermissionToWithdrawActivateReinstate = true;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.Tabs._Details, (object) forExistingPermit);
    }

    [HttpGet]
    public virtual PartialViewResult LoadReviewTab(int permitId)
    {
      ReviewTabForm reviewTabForm = new ReviewTabForm();
      Permit byId = this._permittingService.GetById(permitId, "LU_PermitStatus,LU_PermitType,PermitContacts");
      reviewTabForm.PermitId = byId.Id;
      reviewTabForm.PermitStatusId = byId.LU_PermitStatus == null ? 0 : byId.LU_PermitStatus.Id;
      reviewTabForm.PermitType = byId.LU_PermitType == null ? "Unknown Permit Type" : byId.LU_PermitType.Description;
      reviewTabForm.PermitStatusDetail.PermitStatusId = byId.LU_PermitStatus.Id;
      reviewTabForm.PermitStatusDetail.PermitCategoryId = byId.LU_PermitStatus.PermitCategoryId ?? 0;
      StatusChange lastStatusChange = this._permittingService.GetLastStatusChange(byId.Id, byId.PermitStatusId.GetValueOrDefault());
      if (lastStatusChange != null)
      {
        reviewTabForm.PermitStatusDetail.PreviousPermitStatusId = lastStatusChange.PreviousPermitStatusId;
        reviewTabForm.PermitStatusDetail.PermitStatusId = lastStatusChange.PermitStatusId;
        reviewTabForm.PermitStatusDetail.PermitStatusLastChangedBy = lastStatusChange.ChangedBy;
        reviewTabForm.PermitStatusDetail.PermitStatusLastChangedDate = lastStatusChange.ChangedDate;
      }
      reviewTabForm.PermitStatusDetail.PermitStatusDescription = byId.LU_PermitStatus.Description;
      reviewTabForm.PermitStatusDetail.PermitStatusNav = this._permitServiceClient.GetWorkFlowSteps(permitId);
      int currentUserContactId = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);
      if (SessionHandler.GetSessionVar<IEnumerable<string>>(SessionVariables.AuthenticatedUserRoles).Any<string>((Func<string, bool>) (role =>
      {
        if (!(role == "Permit Supervisor") && !(role == "Division Chief") && !(role == "Administrative Specialist"))
          return role == "Compliance / Enforcement Manager";
        return true;
      })))
      {
        reviewTabForm.PermitStatusDetail.HasPermissionToWithdrawActivateReinstate = true;
        if (byId.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (x => x.ContactId == currentUserContactId)))
        {
          int? permitStatusId1 = byId.PermitStatusId;
          if ((permitStatusId1.GetValueOrDefault() != 59 ? 0 : (permitStatusId1.HasValue ? 1 : 0)) == 0)
          {
            int? permitStatusId2 = byId.PermitStatusId;
            if ((permitStatusId2.GetValueOrDefault() != 62 ? 0 : (permitStatusId2.HasValue ? 1 : 0)) == 0)
            {
              int? permitStatusId3 = byId.PermitStatusId;
              if ((permitStatusId3.GetValueOrDefault() != 65 ? 0 : (permitStatusId3.HasValue ? 1 : 0)) == 0)
              {
                int? permitStatusId4 = byId.PermitStatusId;
                if ((permitStatusId4.GetValueOrDefault() != 45 ? 0 : (permitStatusId4.HasValue ? 1 : 0)) == 0)
                  goto label_9;
              }
            }
          }
          reviewTabForm.HasPermissionToFinalizePermit = true;
        }
      }
label_9:
      if (SessionHandler.GetSessionVar<IEnumerable<string>>(SessionVariables.AuthenticatedUserRoles).Any<string>((Func<string, bool>) (role =>
      {
        if (!(role == "Compliance / Enforcement Manager"))
          return role == "Administrative Specialist";
        return true;
      })))
        reviewTabForm.HasPermissionToFinalizeExemption = true;
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Review, (object) reviewTabForm);
    }

    [HttpGet]
    public virtual PartialViewResult LoadConditionsTab(int permitId)
    {
      ConditionsTabForm conditionsTabForm = new ConditionsTabForm();
      Permit byId = this._permittingService.GetById(permitId, "LU_PermitStatus");
      conditionsTabForm.PermitId = byId.Id;
      conditionsTabForm.PermitStatusId = byId.PermitStatusId;
      conditionsTabForm.PermitCategoryId = byId.LU_PermitStatus == null ? 0 : byId.LU_PermitStatus.PermitCategoryId.GetValueOrDefault();
      IEnumerable<LU_PermitTemplate> range = this._permitTemplateService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active == True", new object[0]) }, (string) null);
      conditionsTabForm.PermitTemplateList = new SelectList((IEnumerable) range, "Id", "Description", (object) "Select");
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Conditions, (object) conditionsTabForm);
    }

    [HttpGet]
    public virtual PartialViewResult LoadIPLTab(int permitId)
    {
      Permit byId = this._permittingService.GetById(permitId, "LU_PermitStatus");
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Ipl, (object) new IplTabForm() { PermitId = byId.Id, PermitName = byId.PermitName, PermitCategoryId = byId.LU_PermitStatus.PermitCategoryId, IplContact = Mapper.Map<Contact, ContactForm>(new Contact() { ContactTypeId = 15 }) });
    }

    [HttpGet]
    public virtual PartialViewResult GetIplContact(int permitContactId)
    {
      return this.PartialView(MVC.Permitting.Permit.Views.DetailsPartials._iplContact, (object) Mapper.Map<PermitContact, ContactForm>(this._permitContactService.GetById(permitContactId, "Contact.ContactCommunicationMethods,PermitContactInformation")));
    }

    [HttpGet]
    public virtual PartialViewResult LoadCCListTab(int permitId)
    {
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._CCList);
    }

    [HttpGet]
    public virtual PartialViewResult LoadPumpageContactTab(int permitId)
    {
      PumpageContactForm pumpageContactForm = new PumpageContactForm();
      pumpageContactForm.PermitId = permitId;
      pumpageContactForm.Contact = Mapper.Map<PermitContact, ContactForm>(new PermitContact()
      {
        ContactTypeId = new int?(22)
      });
      PermitContact source = this._permitContactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("PermitId==" + (object) permitId, new object[0]), new DynamicFilter("ContactTypeId==" + (object) 22, new object[0]) }, "PermitContactInformation,Contact.ContactCommunicationMethods").SingleOrDefault<PermitContact>();
      if (source != null)
        pumpageContactForm.Contact = Mapper.Map<PermitContact, ContactForm>(source);
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._PumpageContact, (object) pumpageContactForm);
    }

    [HttpGet]
    public virtual PartialViewResult LoadDocumentsTab(int permitId)
    {
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Documents, (object) new DocumentsTabForm() { PermitId = permitId });
    }

    [HttpGet]
    public virtual PartialViewResult LoadCommentsTab(int permitId)
    {
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Comments, (object) new CommentsTabForm() { PermitId = permitId });
    }

    [HttpGet]
    public virtual PartialViewResult LoadComplianceTab(int permitId)
    {
      ComplianceTabForm complianceTabForm = new ComplianceTabForm();
      Permit byId = this._permittingService.GetById(permitId, (string) null);
      complianceTabForm.PermitId = byId.Id;
      complianceTabForm.PermitName = byId.PermitName;
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Compliance, (object) complianceTabForm);
    }

    [HttpGet]
    public virtual PartialViewResult LoadEnforcementTab(int permitId)
    {
      EnforcementTabForm enforcementTabForm = new EnforcementTabForm();
      Permit byId = this._permittingService.GetById(permitId, "LU_PermitComplianceStatus");
      enforcementTabForm.PermitId = byId.Id;
      enforcementTabForm.ComplianceStatus = byId.LU_PermitComplianceStatus == null ? "Compliant" : byId.LU_PermitComplianceStatus.Description;
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Enforcement, (object) enforcementTabForm);
    }

    [HttpGet]
    public virtual PartialViewResult LoadContactsTab(int permitId)
    {
      ContactsTabForm contactsTabForm = new ContactsTabForm();
      contactsTabForm.PermitId = permitId;
      Permit byId = this._permittingService.GetById(permitId, "PermitContacts");
      List<SelectListItem> list1 = this._contactTypeServiceClient.GetAllDivisionChief().Select<DivisionChief, SelectListItem>((Func<DivisionChief, SelectListItem>) (x => new SelectListItem() { Text = x.Contact.FirstName + " " + x.Contact.LastName, Value = x.ContactId.ToString() })).ToList<SelectListItem>();
      list1.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = "0"
      });
      contactsTabForm.DivisionChiefList = new SelectList((IEnumerable) list1, "Value", "Text");
      if (byId.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (x =>
      {
        int? contactTypeId = x.ContactTypeId;
        if ((contactTypeId.GetValueOrDefault() != 32 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
          return x.IsPrimary;
        return false;
      })))
        contactsTabForm.SelectedDivisionChiefId = new int?(byId.PermitContacts.First<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 32 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        })).ContactId);
      List<SelectListItem> list2 = this._contactTypeServiceClient.GetAllSupervisor().Select<Supervisor, SelectListItem>((Func<Supervisor, SelectListItem>) (x => new SelectListItem() { Text = x.Contact.FirstName + " " + x.Contact.LastName, Value = x.ContactId.ToString() })).ToList<SelectListItem>();
      list2.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = "0"
      });
      contactsTabForm.SupervisorList = new SelectList((IEnumerable) list2, "Value", "Text");
      if (byId.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (x =>
      {
        int? contactTypeId = x.ContactTypeId;
        if ((contactTypeId.GetValueOrDefault() != 24 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
          return x.IsPrimary;
        return false;
      })))
        contactsTabForm.SelectedSupervisorId = new int?(byId.PermitContacts.First<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 24 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        })).ContactId);
      List<SelectListItem> list3 = this._contactTypeServiceClient.GetAllProjectManager().Select<ProjectManager, SelectListItem>((Func<ProjectManager, SelectListItem>) (x => new SelectListItem() { Text = x.Contact.FirstName + " " + x.Contact.LastName, Value = x.ContactId.ToString() })).ToList<SelectListItem>();
      list3.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = "0"
      });
      contactsTabForm.ProjectManagerList = new SelectList((IEnumerable) list3, "Value", "Text");
      if (byId.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (x =>
      {
        int? contactTypeId = x.ContactTypeId;
        if ((contactTypeId.GetValueOrDefault() != 20 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
          return x.IsPrimary;
        return false;
      })))
        contactsTabForm.SelectedProjectManagerId = new int?(byId.PermitContacts.First<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 20 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        })).ContactId);
      List<SelectListItem> list4 = this._contactTypeServiceClient.GetAllComplianceManager().Select<ComplianceManager, SelectListItem>((Func<ComplianceManager, SelectListItem>) (x => new SelectListItem() { Text = x.Contact.FirstName + " " + x.Contact.LastName, Value = x.ContactId.ToString() })).ToList<SelectListItem>();
      list4.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = "0"
      });
      contactsTabForm.ComplianceManagerList = new SelectList((IEnumerable) list4, "Value", "Text");
      if (byId.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (x =>
      {
        int? contactTypeId = x.ContactTypeId;
        if ((contactTypeId.GetValueOrDefault() != 35 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
          return x.IsPrimary;
        return false;
      })))
        contactsTabForm.SelectedComplianceManagerId = new int?(byId.PermitContacts.First<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 35 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        })).ContactId);
      List<SelectListItem> list5 = this._contactTypeServiceClient.GetAllCountyOfficial().Select<Contact, SelectListItem>((Func<Contact, SelectListItem>) (x => new SelectListItem() { Text = x.FirstName + " " + x.LastName, Value = x.Id.ToString() })).ToList<SelectListItem>();
      list5.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = "0"
      });
      contactsTabForm.CountyOfficialList = new SelectList((IEnumerable) list5, "Value", "Text");
      if (byId.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (x =>
      {
        int? contactTypeId = x.ContactTypeId;
        if ((contactTypeId.GetValueOrDefault() != 9 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
          return x.IsPrimary;
        return false;
      })))
        contactsTabForm.SelectedCountyOfficialId = new int?(byId.PermitContacts.First<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 9 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        })).ContactId);
      List<SelectListItem> list6 = this._contactTypeServiceClient.GetAllDepartmentOfNaturalResource().Select<Contact, SelectListItem>((Func<Contact, SelectListItem>) (x => new SelectListItem() { Text = x.FirstName + " " + x.LastName, Value = x.Id.ToString() })).ToList<SelectListItem>();
      list6.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = "0"
      });
      contactsTabForm.DNROfficialList = new SelectList((IEnumerable) list6, "Value", "Text");
      if (byId.PermitContacts.Any<PermitContact>((Func<PermitContact, bool>) (x =>
      {
        int? contactTypeId = x.ContactTypeId;
        if ((contactTypeId.GetValueOrDefault() != 10 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
          return x.IsPrimary;
        return false;
      })))
        contactsTabForm.SelectedDNROfficialId = new int?(byId.PermitContacts.First<PermitContact>((Func<PermitContact, bool>) (x =>
        {
          int? contactTypeId = x.ContactTypeId;
          if ((contactTypeId.GetValueOrDefault() != 10 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0)
            return x.IsPrimary;
          return false;
        })).ContactId);
      return this.PartialView(MVC.Permitting.Permit.Views.Tabs._Contacts, (object) contactsTabForm);
    }

    [HttpGet]
    public virtual JsonResult GetPermitHistory(int id)
    {
      PermitHistoryForm permitHistoryForm = new PermitHistoryForm();
      permitHistoryForm.PermitHistory = this.GetRelatedPermits(id);
      Permit byId = this._permittingService.GetById(id, "PermitSupplementalGroups.SupplementalGroup");
      if (byId.PermitSupplementalGroups != null && byId.PermitSupplementalGroups.Any<PermitSupplementalGroup>())
      {
        PermitSupplementalGroup supplementalGroup = byId.PermitSupplementalGroups.First<PermitSupplementalGroup>();
        permitHistoryForm.SupplementalGroupId = supplementalGroup.SupplementalGroupId;
        permitHistoryForm.SupplementalGroupName = supplementalGroup.SupplementalGroup.SupplementalGroupName;
      }
      return this.Json((object) permitHistoryForm, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetPermitTypes()
    {
      return this.Json((object) this._permitTypeService.GetAll((string) null), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult SavePermitType(int id, int permitTypeId)
    {
      Permit byId = this._permittingService.GetById(id, (string) null);
      byId.PermitTypeId = new int?(permitTypeId);
      this._permittingService.Save(byId);
      byId.PermitStatusId = new int?(this._permittingService.CalculateStatus(id));
      this._permittingService.Save(byId);
      return this.Json((object) new{ IsSuccess = true });
    }

    [HttpGet]
    public virtual JsonResult GetSupplementalTypeList()
    {
      return this.Json((object) this._supplementalTypeService.GetAll((string) null), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult SearchSupplementalGroupGridData(GridCommand command, string permitNames)
    {
            var nameList = from x in permitNames.Split(',')
                           where !string.IsNullOrEmpty(x)
                           select x;

            var permitIds = _permittingService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("@0.Contains(outerIt.PermitName)", nameList) }).Select(x => x.Id);

            var supplementalGroups = _permitSupplementalGroupService.GetRange(0, int.MaxValue, "Id", new[] { new DynamicFilter("@0.Contains(outerIt.PermitId)", permitIds) }, "SupplementalGroup");

            var groups = supplementalGroups.Select(x =>
                new
                {
                    Id = x.SupplementalGroup.Id,
                    Name = x.SupplementalGroup.SupplementalGroupName
                });

            return Json(new GridData { Data = groups }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult ViewSupplementalGroupGridData(GridCommand command, int id)
    {
            var supplementalGroups = _permitSupplementalGroupService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("SupplementalGroupId==@0", id) }, "Permit");

            var permits = supplementalGroups.Select(x => new { PermitId = x.Permit.Id, PermitName = x.Permit.PermitName, SupplementalTypeId = x.SupplementalTypeId });

            return Json(new GridData { Data = permits }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult SetSupplementalGroupType(int permitId, int newSupplementalTypeId)
    {
      PermitSupplementalGroup entity = this._permitSupplementalGroupService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0", new object[1]{ (object) permitId }) }, (string) null).SingleOrDefault<PermitSupplementalGroup>();
      entity.SupplementalTypeId = newSupplementalTypeId;
      this._permitSupplementalGroupService.Save(entity);
      return this.Json((object) new{ Success = true });
    }

    [HttpPost]
    public virtual JsonResult AddToSupplementalGroup(int permitId, int groupId)
    {
      Permit byId = this._permittingService.GetById(permitId, "PermitSupplementalGroups");
      if (byId == null || byId.PermitSupplementalGroups.Count != 0)
        return this.Json((object) new{ Success = false });
      this._permitSupplementalGroupService.Save(new PermitSupplementalGroup()
      {
        PermitId = byId.Id,
        SupplementalGroupId = groupId,
        SupplementalTypeId = 1
      });
      return this.Json((object) new{ Success = true });
    }

    [HttpPost]
    public virtual JsonResult RemoveFromSupplementalGroup(int permitId)
    {
      PermitSupplementalGroup supplementalGroup = this._permitSupplementalGroupService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId==@0", new object[1]{ (object) permitId }) }, (string) null).FirstOrDefault<PermitSupplementalGroup>();
      if (supplementalGroup == null)
        return this.Json((object) new{ Success = false });
      int supplementalGroupId = supplementalGroup.SupplementalGroupId;
      this._permitSupplementalGroupService.Delete(supplementalGroup.Id);
      if (this._permitSupplementalGroupService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("SupplementalGroupId==@0", new object[1]{ (object) supplementalGroupId }) }, (string) null).Any<PermitSupplementalGroup>())
        return this.Json((object) new{ Success = true, GroupDeleted = false });
      this._supplementalGroupService.Delete(supplementalGroupId);
      return this.Json((object) new{ Success = true, GroupDeleted = true });
    }

    [HttpPost]
    public virtual JsonResult CreateNewSupplementalGroup(int activePermitId, string additionalPermitNames)
    {
      IEnumerable<int> ints = this._permittingService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("@0.Contains(outerIt.PermitName)", new object[1]{ (object) ((IEnumerable<string>) additionalPermitNames.Split(',')).Where<string>((Func<string, bool>) (x => !string.IsNullOrEmpty(x))) }) }, (string) null).Select<Permit, int>((Func<Permit, int>) (x => x.Id));
      int num1 = this._supplementalGroupService.Save(new SupplementalGroup());
      this._permitSupplementalGroupService.Save(new PermitSupplementalGroup()
      {
        PermitId = activePermitId,
        SupplementalGroupId = num1,
        SupplementalTypeId = 1
      });
      foreach (int num2 in ints)
        this._permitSupplementalGroupService.Save(new PermitSupplementalGroup()
        {
          PermitId = num2,
          SupplementalGroupId = num1,
          SupplementalTypeId = 1
        });
      return this.Json((object) new{ GroupId = num1 });
    }

    [HttpGet]
    public virtual JsonResult DenyPermit(int PermitId)
    {
      Permit byId = this._permittingService.GetById(PermitId, (string) null);
      Permit permit = byId;
      int? permitStatusId = byId.PermitStatusId;
      int? nullable = new int?((permitStatusId.GetValueOrDefault() != 45 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0 ? 53 : 57);
      permit.PermitStatusId = nullable;
      byId.DateOut = new DateTime?(DateTime.Now);
      if (this._permittingService.Save(byId) > 0)
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      return this.Json((object) new{ success = false }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult ApprovePermit(int PermitId)
    {
      Permit byId1 = this._permittingService.GetById(PermitId, "LU_PermitStatus");
      if (byId1.LU_PermitStatus != null)
      {
        int? permitCategoryId1 = byId1.LU_PermitStatus.PermitCategoryId;
        if ((permitCategoryId1.GetValueOrDefault() != 3 ? 0 : (permitCategoryId1.HasValue ? 1 : 0)) == 0)
        {
          int? permitCategoryId2 = byId1.LU_PermitStatus.PermitCategoryId;
          if ((permitCategoryId2.GetValueOrDefault() != 4 ? 0 : (permitCategoryId2.HasValue ? 1 : 0)) == 0)
            goto label_8;
        }
        if (byId1.RefId.HasValue)
        {
          Permit byId2 = this._permittingService.GetById(byId1.RefId.Value, (string) null);
          if (byId2 != null)
          {
            int? permitStatusId = byId2.PermitStatusId;
            if ((permitStatusId.GetValueOrDefault() != 46 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0)
            {
              byId2.PermitStatusId = new int?(50);
              this._permittingService.Save(byId2);
            }
          }
        }
        byId1.PermitStatusId = new int?(46);
        goto label_17;
      }
label_8:
      int? permitStatusId1 = byId1.PermitStatusId;
      if ((permitStatusId1.GetValueOrDefault() != 45 ? 0 : (permitStatusId1.HasValue ? 1 : 0)) != 0)
      {
        if (byId1.RefId.HasValue)
        {
          Permit byId2;
          for (byId2 = this._permittingService.GetById(byId1.RefId.Value, (string) null); byId2 != null && byId2.RefId.HasValue; byId2 = this._permittingService.GetById(byId2.RefId.Value, (string) null))
          {
            int? permitStatusId2 = byId2.PermitStatusId;
            if ((permitStatusId2.GetValueOrDefault() != 46 ? 1 : (!permitStatusId2.HasValue ? 1 : 0)) == 0)
              break;
          }
          byId2.PermitStatusId = new int?(50);
          this._permittingService.Save(byId2);
        }
        byId1.PermitStatusId = new int?(52);
      }
      else
        byId1.PermitStatusId = new int?(46);
label_17:
      byId1.DateOut = new DateTime?(DateTime.Now);
      if (this._permittingService.Save(byId1) > 0)
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      return this.Json((object) new{ success = false }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult SetPermitDates(int permitId, string effectiveDate, string expirationDate, string appropriationDate)
    {
      Permit byId = this._permittingService.GetById(permitId, (string) null);
      byId.EffectiveDate = new DateTime?(Convert.ToDateTime(effectiveDate));
      byId.AppropriationDate = new DateTime?(Convert.ToDateTime(appropriationDate));
      byId.ExpirationDate = new DateTime?(Convert.ToDateTime(expirationDate));
      this._permittingService.Save(byId);
      return this.Json((object) new{ Success = true }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetPermitDates(int permitId)
    {
      Permit byId = this._permittingService.GetById(permitId, (string) null);
      if (byId.EffectiveDate.HasValue && byId.AppropriationDate.HasValue && byId.ExpirationDate.HasValue)
        return this.Json((object) new{ ExpirationDate = byId.ExpirationDate.Value.ToString("MM/dd/yyyy"), EffectiveDate = byId.EffectiveDate.Value.ToString("MM/dd/yyyy"), FirstAppropriationDate = byId.AppropriationDate.Value.ToString("MM/dd/yyyy") }, JsonRequestBehavior.AllowGet);
      int? permitStatusId = byId.PermitStatusId;
      if ((permitStatusId.GetValueOrDefault() != 62 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0)
      {
        Permit permit1 = this._permittingService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitNumber.ToLower() == @0 && RevisionNumber == @1", new object[2]{ (object) byId.PermitNumber, (object) "01" }) }, (string) null).SingleOrDefault<Permit>();
        Permit permit2 = this._permittingService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitNumber.ToLower() == @0 && PermitStatusId == @1", new object[2]{ (object) byId.PermitNumber.ToLower(), (object) 46 }) }, (string) null).SingleOrDefault<Permit>();
        return this.Json((object) new{ ExpirationDate = (permit2 == null ? DateTime.Now.ToString("MM/dd/yyyy") : (!permit2.ExpirationDate.HasValue ? DateTime.Today.ToString("MM/dd/yyyy") : Convert.ToDateTime((object) permit2.ExpirationDate).ToString("MM/dd/yyyy"))), EffectiveDate = DateTime.Today.ToString("MM/dd/yyyy"), FirstAppropriationDate = permit1.EffectiveDate.GetValueOrDefault().ToString("MM/dd/yyyy") }, JsonRequestBehavior.AllowGet);
      }
      if (!(byId.RevisionNumber != "01"))
        return this.Json((object) new{ ExpirationDate = this.AddElevenMonthsElevenYears(DateTime.Now), EffectiveDate = DateTime.Today.ToString("MM/dd/yyyy"), FirstAppropriationDate = DateTime.Today.ToString("MM/dd/yyyy") }, JsonRequestBehavior.AllowGet);
      return this.Json((object) new{ ExpirationDate = this.AddElevenMonthsElevenYears(DateTime.Today), EffectiveDate = DateTime.Today.ToString("MM/dd/yyyy"), FirstAppropriationDate = this._permittingService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitNumber.ToLower() == @0 && RevisionNumber == @1", new object[2]{ (object) byId.PermitNumber, (object) "01" }) }, (string) null).SingleOrDefault<Permit>().EffectiveDate.GetValueOrDefault().ToString("MM/dd/yyyy") }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult DateElevenMonthsElevenYears(DateTime date)
    {
      return this.Json((object) this.AddElevenMonthsElevenYears(date), JsonRequestBehavior.AllowGet);
    }

    private string AddElevenMonthsElevenYears(DateTime date)
    {
      DateTime dateTime = date.AddMonths(143);
      return dateTime.Month.ToString() + "/" + (object) DateTime.DaysInMonth(dateTime.Year, dateTime.Month) + "/" + (object) dateTime.Year;
    }

    [HttpGet]
    public virtual ActionResult LoadAdditionalLandOwners(int PermitId)
    {
      Permit byId = this._permittingService.GetById(PermitId, "PermitContacts.Contact,LU_PermitStatus");
      List<ContactForm> contactFormList = new List<ContactForm>();
      foreach (PermitContact permitContact in (IEnumerable<PermitContact>) byId.PermitContacts)
      {
        int? contactTypeId = permitContact.ContactTypeId;
        if ((contactTypeId.GetValueOrDefault() != 16 ? 0 : (contactTypeId.HasValue ? 1 : 0)) != 0 && !permitContact.IsPrimary)
        {
          ContactForm contactForm = Mapper.Map<Contact, ContactForm>(permitContact.Contact);
          IEnumerable<ContactCommunicationMethod> range = this._contactCommunicationMethodService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ContactId == @0", new object[1]{ (object) contactForm.Id }) }, (string) null);
          ContactCommunicationMethod communicationMethod1 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
          {
            if (x.IsPrimaryEmail.HasValue)
              return x.IsPrimaryEmail.Value;
            return false;
          }));
          if (communicationMethod1 != null)
          {
            contactForm.EmailAddress.CommunicationValue = communicationMethod1.CommunicationValue;
            contactForm.EmailAddress.SelectedCommunicationMethodId = new int?(communicationMethod1.CommunicationMethodId);
          }
          ContactCommunicationMethod communicationMethod2 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
          {
            if (x.IsPrimaryPhone.HasValue)
              return x.IsPrimaryPhone.Value;
            return false;
          }));
          if (communicationMethod2 != null)
          {
            contactForm.PrimaryTelephoneNumber.CommunicationValue = communicationMethod2.CommunicationValue;
            contactForm.PrimaryTelephoneNumber.SelectedCommunicationMethodId = new int?(communicationMethod2.CommunicationMethodId);
          }
          ContactCommunicationMethod communicationMethod3 = range.FirstOrDefault<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
          {
            if (x.IsPrimaryPhone.HasValue)
              return !x.IsPrimaryPhone.Value;
            return false;
          }));
          if (communicationMethod3 != null)
          {
            contactForm.AltTelephoneNumber.CommunicationValue = communicationMethod3.CommunicationValue;
            contactForm.AltTelephoneNumber.SelectedCommunicationMethodId = new int?(communicationMethod3.CommunicationMethodId);
          }
          contactFormList.Add(contactForm);
        }
      }
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.DetailsPartials._AdditionalLandOwnersForm, (object) contactFormList);
    }

    public virtual ActionResult LoadPermitHearing(int Permitid, int PermitStatusId)
    {
      ObjectFactory.GetInstance<IService<LU_PermitHearingStatus>>().GetAll((string) null);
      ViewBag.Permitid = Permitid;
      ViewBag.PermitStatusId = PermitStatusId;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.DetailsPartials._PermitHearing);
    }

    [HttpPost]
    public virtual JsonResult SavePermitHearing(PermitHearingForm permitHearing)
    {
      PermitHearing entity = Mapper.Map<PermitHearingForm, PermitHearing>(permitHearing);
      if (entity.Id != 0)
      {
        this._permitHearingService.Delete(entity.Id);
        entity.Id = 0;
      }
      if (!entity.PermitHearingStatusId.HasValue)
        entity.PermitHearingStatusId = new int?(1);
      this._permitHearingService.Save(entity);
      return (JsonResult) null;
    }

    public virtual JsonResult DeletePermitHearing(int id)
    {
      try
      {
        this._permitHearingService.Delete(id);
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    public virtual JsonResult GetPermitHearing(int id)
    {
      PermitHearing byId = this._permitHearingService.GetById(id, (string) null);
      Convert.ToDateTime((object) byId.PermitHearingDateTime).ToShortDateString();
      return this.Json((object) new{ Address1 = byId.Address1, Address2 = byId.Address2, City = byId.City, StateId = byId.StateId, ZipCode = byId.ZipCode, DateOfHearing = Convert.ToDateTime((object) byId.PermitHearingDateTime).ToShortDateString(), TimeOfHearing = Convert.ToDateTime((object) byId.PermitHearingDateTime).ToShortTimeString(), HearingOfficer = byId.HearingOfficer, Id = byId.Id }, JsonRequestBehavior.AllowGet);
    }

    public virtual JsonResult GetPermitHearingByPermitId(int PermitId)
    {
      return this.Json((object) new{ Data = this._permitHearingService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId==@0", new object[1]{ (object) PermitId }) }, (string) null).Select(x => new{ Date = Convert.ToDateTime((object) x.PermitHearingDateTime).ToShortDateString(), Time = Convert.ToDateTime((object) x.PermitHearingDateTime).ToShortTimeString(), Location = x.Address1, Officer = x.HearingOfficer, Id = x.Id }) }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult AddPumpageContact(PumpageContactForm form)
    {
      try
      {
        IUpdatableService<PermitContact> permitContactService = this._permitContactService;
        int skip = 0;
        int maxValue = int.MaxValue;
        DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]{ new DynamicFilter("PermitId==@0 && ContactTypeId==@1", new object[2]{ (object) form.PermitId, (object) form.Contact.ContactType }) };
        foreach (PermitContact permitContact in permitContactService.GetRange(skip, maxValue, null, (IEnumerable<DynamicFilter>) dynamicFilterArray, null))
        {
          if (form.Contact.Id != permitContact.ContactId)
            this._permitContactService.Delete(permitContact.Id);
        }
        form.Contact.IsPrimary = true;
        this._contactServiceClient.SavePermitContact(form.PermitId, form.Contact);
        return this.Json((object) new{ success = true });
      }
      catch
      {
        return this.Json((object) new{ success = false });
      }
    }

    [HttpPost]
    public virtual JsonResult RemovePermitContact(int permitContactId)
    {
      this._permitContactService.Delete(permitContactId);
      return this.Json((object) new{ Success = true });
    }

    private IEnumerable<RelatedPermitForm> GetRelatedPermits(int id)
    {
      Permit byId = this._permittingService.GetById(id, "LU_PermitStatus");
      int? permitCategoryId = byId.LU_PermitStatus.PermitCategoryId;
      int num1;
      if ((permitCategoryId.GetValueOrDefault() != 1 ? 0 : (permitCategoryId.HasValue ? 1 : 0)) != 0)
        num1 = id;
      else if (byId.RefId.HasValue)
      {
        num1 = this._permittingService.GetById(byId.RefId.Value, (string) null).Id;
      }
      else
      {
        if (byId.PermitNumber == null)
          return (IEnumerable<RelatedPermitForm>) null;
        return Mapper.Map<IEnumerable<Permit>, IEnumerable<RelatedPermitForm>>(this._permittingService.GetRange(0, int.MaxValue, "RevisionNumber", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitNumber==@0", new object[1]{ (object) byId.PermitNumber }) }, (string) null));
      }
      List<int> first = new List<int>() { num1 };
      List<int> source1 = new List<int>();
      List<Permit> source2 = new List<Permit>();
      while (first.Except<int>((IEnumerable<int>) source1).Any<int>())
      {
        int id1 = first.Except<int>((IEnumerable<int>) source1).First<int>();
        source2.Add(this._permittingService.GetById(id1, "LU_PermitStatus"));
        IPermitService permittingService = this._permittingService;
        int skip = 0;
        int maxValue = int.MaxValue;
        string orderBy = "";
        DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]{ new DynamicFilter("RefId==@0", new object[1]{ (object) id1 }) };
        foreach (Permit permit in permittingService.GetRange(skip, maxValue, orderBy, (IEnumerable<DynamicFilter>) dynamicFilterArray, null))
        {
          source2.Add(permit);
          if (permit.PermitNumber != null)
          {
            IEnumerable<Permit> range = this._permittingService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitNumber==@0 && Id!=@1", new object[2]{ (object) permit.PermitNumber, (object) permit.Id }) }, (string) null);
            first.AddRange(range.Where<Permit>((Func<Permit, bool>) (item2 => item2.RefId.HasValue)).Select<Permit, int>((Func<Permit, int>) (item2 => item2.RefId ?? 0)));
          }
        }
        source1.Add(id1);
      }
      List<int> list = source1.OrderBy<int, int>((Func<int, int>) (x => x)).ToList<int>();
      List<Permit> source3 = new List<Permit>();
      foreach (int num2 in list)
      {
        int item = num2;
        if (source3.All<Permit>((Func<Permit, bool>) (x => x.Id != item)))
          source3.Add(source2.First<Permit>((Func<Permit, bool>) (x => x.Id == item)));
        foreach (Permit permit in source2.Where<Permit>((Func<Permit, bool>) (x =>
        {
          int? refId = x.RefId;
          if (refId.GetValueOrDefault() == item)
            return refId.HasValue;
          return false;
        })))
        {
          Permit itemInner = permit;
          if (source3.All<Permit>((Func<Permit, bool>) (x => x.Id != itemInner.Id)))
            source3.Add(itemInner);
        }
      }
      return (IEnumerable<RelatedPermitForm>) Mapper.Map<IEnumerable<Permit>, IEnumerable<RelatedPermitForm>>((IEnumerable<Permit>) source3).OrderBy<RelatedPermitForm, string>((Func<RelatedPermitForm, string>) (t => t.PermitName));
    }

    [HttpGet]
    public virtual PartialViewResult LoadChecklist(int id, int permitStatus)
    {
      CheckListForm form = new CheckListForm();
      this.BuildCheckListFrom(form, id, permitStatus);
      form.RefId = id;
      form.PermitStatusId = permitStatus;
      PermitStatusState permitStatusState = this._permitStatusStateService.GetRange(0, int.MaxValue, "CreatedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0 && PermitStatusId == @1", new object[2]{ (object) id, (object) permitStatus }) }, (string) null).FirstOrDefault<PermitStatusState>();
      if (permitStatusState != null)
      {
        form.CheckListCompleted = permitStatusState.Completed ? "true" : "false";
        form.CompletedByAndDate = "Completed by " + permitStatusState.LastModifiedBy + " on " + (object) permitStatusState.LastModifiedDate;
      }
      else
        form.CheckListCompleted = "false";
      return this.PartialView(MVC.Permitting.Permit.Views.DetailsPartials._CheckList, (object) form);
    }

    private void BuildCheckListFrom(CheckListForm form, int permitId, int permitStatusId)
    {
      Permit byId = this._permittingService.GetById(permitId, (string) null);
      if (byId == null)
        throw new ArgumentException("Invalid Permit Id: " + (object) permitId, "permitId");
      form.RefId = permitId;
      form.PermitStatusId = permitStatusId;
      form.CountySignOffBy = byId.CountyOfficialSignOffBy;
      form.CountySignOffDate = byId.CountyOfficialSignOffDate;
      form.CountySignOffYesNo = byId.CountyOfficialSignOffYesNo;
      CheckList checkList = this._permitCheckListService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("PermitStatusId == " + (object) permitStatusId, new object[0]), new DynamicFilter("Active == True", new object[0]) }, (string) null).FirstOrDefault<CheckList>();
      int num = checkList == null ? 0 : checkList.Id;
      form.ChecklistQuestions = this._checkListQuestionService.GetRange(0, int.MaxValue, "QuestionNumber", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("CheckListId == " + (object) num, new object[0])
      }, (string) null);
      form.AnswerOptions = this._answerService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
      form.ChecklistAnswers = this._checkListAnswerService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == " + (object) permitId, new object[0])
      }, (string) null);
    }

    [HttpPost]
    public virtual JsonResult SaveContactsInDetailsContactsTab(DetailsPermitContactForm form)
    {
      try
      {
        Uri uri1 = new Uri(new Uri(ConfigurationManager.AppSettings["InternalPortalUrl"]), ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(form.PermitId, new PermitDetailsTab?())));
        Uri uri2 = new Uri(new Uri(ConfigurationManager.AppSettings["ExternalPortalUrl"]), "/Security/Account/Login?ReturnUrl=%2fPermitting%2fPermit%2fDetails%2f" + (object) form.PermitId);
        this._contactTypeServiceClient.SaveContactType(form.PermitId, 32, form.SelectedDivisionChiefId.Value, uri1.ToString());
        this._contactTypeServiceClient.SaveContactType(form.PermitId, 24, form.SelectedSupervisorId.Value, uri1.ToString());
        this._contactTypeServiceClient.SaveContactType(form.PermitId, 20, form.SelectedProjectManagerId.Value, uri1.ToString());
        this._contactTypeServiceClient.SaveContactType(form.PermitId, 35, form.SelectedComplianceManagerId.Value, uri1.ToString());
        this._contactTypeServiceClient.SaveContactType(form.PermitId, 9, form.SelectedCountyOfficialId.Value, uri2.ToString());
        this._contactTypeServiceClient.SaveContactType(form.PermitId, 10, form.SelectedDNROfficialId.Value, uri2.ToString());
        return this.Json((object) new{ success = true });
      }
      catch
      {
        return this.Json((object) new{ success = false });
      }
    }

    [HttpGet]
    public virtual PartialViewResult LoadPermitInformation(int id)
    {
      Permit byId = this._permittingService.GetById(id, "PermitWithdrawalGroundwaters.PermitWithdrawalGroundwaterDetails,PermitWithdrawalSurfacewaters.PermitWithdrawalSurfacewaterDetails");
      if (byId == null)
        throw new Exception("Invalid Permit Id: " + (object) id);
      PermitInformationForm permitInformationForm = Mapper.Map<Permit, PermitInformationForm>(byId);
      permitInformationForm.PermitLocationInformation.AquiferList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>) this._aquiferService.GetAll((string) null).Select<LU_Aquifer, LU_Aquifer>((Func<LU_Aquifer, LU_Aquifer>) (x => new LU_Aquifer()
      {
        Id = x.Id,
        Description = x.Description + " | " + x.Key
      })));
      permitInformationForm.PermitLocationInformation.WaterTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>)this._waterTypeService.GetAll((string)null).Select<LU_WaterType, LU_WaterType>((Func<LU_WaterType, LU_WaterType>)(x => new LU_WaterType()
      {
          Id = x.Id,
          Description = x.Description
      })));
      permitInformationForm.PermitLocationInformation.TidalTypeList = Mapper.Map<IEnumerable<ILookup>, SelectList>((IEnumerable<ILookup>)this._tidalTypeService.GetAll((string)null).Select<LU_TidalType, LU_TidalType>((Func<LU_TidalType, LU_TidalType>)(x => new LU_TidalType()
      {
          Id = x.Id,
          Description = x.Description
      })));
      List<string> stringList = new List<string>();
      if (byId.PermitWithdrawalGroundwaters != null && byId.PermitWithdrawalGroundwaters.Any<PermitWithdrawalGroundwater>())
        stringList.AddRange(byId.PermitWithdrawalGroundwaters.First<PermitWithdrawalGroundwater>().PermitWithdrawalGroundwaterDetails.Select<PermitWithdrawalGroundwaterDetail, string>((Func<PermitWithdrawalGroundwaterDetail, string>) (d => d.Street + ", " + d.City + ", " + d.State + " " + d.Zip)));
      if (byId.PermitWithdrawalSurfacewaters != null && byId.PermitWithdrawalSurfacewaters.Any<PermitWithdrawalSurfacewater>())
        stringList.AddRange(byId.PermitWithdrawalSurfacewaters.First<PermitWithdrawalSurfacewater>().PermitWithdrawalSurfacewaterDetails.Select<PermitWithdrawalSurfacewaterDetail, string>((Func<PermitWithdrawalSurfacewaterDetail, string>) (d => d.Street + ", " + d.City + ", " + d.State + " " + d.Zip)));
      ViewBag.PermitAddresses = stringList;
      return this.PartialView(MVC.Permitting.Permit.Views.PermitInformation, (object) permitInformationForm);
    }

    [HttpPost]
    public virtual JsonResult SavePermitInformation(PermitInformationForm form)
    {
      Permit byId = this._permittingService.GetById(form.PermitId, (string) null);
      if (byId == null)
        return this.Json((object) new{ success = false, message = ("Invalid Permit Id: " + (object) form.PermitId) });
      Mapper.Map<PermitLocationInformationForm, Permit>(form.PermitLocationInformation, byId);
      Mapper.Map<PermitGeneralInformationForm, Permit>(form.PermitGeneralInformation, byId);
      byId.Description = form.SubscriptionSummary;
      if (this._permittingService.Save(byId) == 0)
        return this.Json((object) new{ success = false, message = ("An error has occurred saving permit " + (object) form.PermitId) });
      return this.Json((object) new{ success = true });
    }

    [HttpGet]
    public virtual PartialViewResult LoadApprovedAllocation(int id)
    {
      Permit byId = this._permittingService.GetById(id, "Permit2.PermitWithdrawalGroundwaters,Permit2.PermitWithdrawalSurfacewaters,PermitWithdrawalGroundwaters,PermitWithdrawalSurfacewaters");
      if (byId == null)
        throw new Exception("Invalid Permit Id: " + (object) id);
      AllocationInformationForm allocationInformationForm = new AllocationInformationForm() { PermitId = byId.Id };
      Permit permit2 = byId.Permit2;
      if (permit2 != null)
      {
        PermitWithdrawalGroundwater withdrawalGroundwater = permit2.PermitWithdrawalGroundwaters.SingleOrDefault<PermitWithdrawalGroundwater>();
        if (withdrawalGroundwater != null)
          allocationInformationForm.RequestedGroundwaterAllocation = new GroundwaterAllocationForm()
          {
            DailyAverage = new long?(withdrawalGroundwater.AvgGalPerDay),
            DailyAverageForMonthOfMaximumUse = new long?(withdrawalGroundwater.MaxGalPerDay)
          };
        PermitWithdrawalSurfacewater withdrawalSurfacewater = permit2.PermitWithdrawalSurfacewaters.SingleOrDefault<PermitWithdrawalSurfacewater>();
        if (withdrawalSurfacewater != null)
          allocationInformationForm.RequestedSurfacewaterAllocation = new SurfacewaterAllocationForm()
          {
            DailyAverage = new long?(withdrawalSurfacewater.AvgGalPerDay),
            MaximumDailyWithdrawal = new long?(withdrawalSurfacewater.MaxGalPerDay)
          };
      }
      PermitWithdrawalGroundwater withdrawalGroundwater1 = byId.PermitWithdrawalGroundwaters.SingleOrDefault<PermitWithdrawalGroundwater>();
      if (withdrawalGroundwater1 != null)
        allocationInformationForm.ApprovedGroundwaterAllocation = new GroundwaterAllocationForm()
        {
          DailyAverage = new long?(withdrawalGroundwater1.AvgGalPerDay),
          DailyAverageForMonthOfMaximumUse = new long?(withdrawalGroundwater1.MaxGalPerDay)
        };
      PermitWithdrawalSurfacewater withdrawalSurfacewater1 = byId.PermitWithdrawalSurfacewaters.SingleOrDefault<PermitWithdrawalSurfacewater>();
      if (withdrawalSurfacewater1 != null)
        allocationInformationForm.ApprovedSurfacewaterAllocation = new SurfacewaterAllocationForm()
        {
          DailyAverage = new long?(withdrawalSurfacewater1.AvgGalPerDay),
          MaximumDailyWithdrawal = new long?(withdrawalSurfacewater1.MaxGalPerDay)
        };
      return this.PartialView(MVC.Permitting.Permit.Views.ApprovedAllocation, (object) allocationInformationForm);
    }

    [HttpPost]
    public virtual JsonResult SaveApprovedAllocation(AllocationInformationForm form)
    {
      if (form.ApprovedGroundwaterAllocation != null)
      {
        PermitWithdrawalGroundwater entity = this._permitWithdrawalGroundwaterService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId = @0", new object[1]{ (object) form.PermitId }) }, (string) null).SingleOrDefault<PermitWithdrawalGroundwater>();
        if (entity != null)
        {
          GroundwaterAllocationForm groundwaterAllocation = form.ApprovedGroundwaterAllocation;
          if (groundwaterAllocation.DailyAverageForMonthOfMaximumUse.HasValue)
            entity.MaxGalPerDay = groundwaterAllocation.DailyAverageForMonthOfMaximumUse.Value;
          if (groundwaterAllocation.DailyAverage.HasValue)
            entity.AvgGalPerDay = groundwaterAllocation.DailyAverage.Value;
          this._permitWithdrawalGroundwaterService.Save(entity);
        }
      }
      if (form.ApprovedSurfacewaterAllocation != null)
      {
        PermitWithdrawalSurfacewater entity = this._permitWithdrawalSurfacewaterService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId = @0", new object[1]{ (object) form.PermitId }) }, (string) null).SingleOrDefault<PermitWithdrawalSurfacewater>();
        if (entity != null)
        {
          SurfacewaterAllocationForm surfacewaterAllocation = form.ApprovedSurfacewaterAllocation;
          if (surfacewaterAllocation.MaximumDailyWithdrawal.HasValue)
            entity.MaxGalPerDay = surfacewaterAllocation.MaximumDailyWithdrawal.Value;
          if (surfacewaterAllocation.DailyAverage.HasValue)
            entity.AvgGalPerDay = surfacewaterAllocation.DailyAverage.Value;
          this._permitWithdrawalSurfacewaterService.Save(entity);
        }
      }
      return this.Json((object) new{ success = true });
    }

    [HttpGet]
    public virtual ActionResult GenerateAveryLabelsForIPL(int permitId)
    {
      List<PermitContactInformation> list = this._permitContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("PermitId == @0", new object[1]{ (object) permitId }), new DynamicFilter("ContactTypeId == @0", new object[1]{ (object) 15 }) }, "PermitContactInformation.LU_State").Select<PermitContact, PermitContactInformation>((Func<PermitContact, PermitContactInformation>) (pc => pc.PermitContactInformation)).ToList<PermitContactInformation>();
      if (!list.Any<PermitContactInformation>())
        return (ActionResult) this.Content("No Data was found");
      iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.LETTER);
      document.SetMargins(9f, 9f, 58.5f, 58.5f);
      MemoryStream memoryStream = new MemoryStream();
      PdfWriter instance = PdfWriter.GetInstance(document, (Stream) memoryStream);
      document.Open();
      PdfPTable pdfPtable = new PdfPTable(2) { WidthPercentage = 100f };
      pdfPtable.DefaultCell.Border = 0;
      BaseFont font = BaseFont.CreateFont("Times-Roman", "Cp1252", false);
      for (int index = 0; index < list.Count; ++index)
      {
        PermitContactInformation contactInformation = list[index];
        PdfPCell pdfPcell = new PdfPCell();
        pdfPcell.Border = 0;
        pdfPcell.FixedHeight = 96f;
        pdfPcell.PaddingLeft = 5.76f;
        pdfPcell.PaddingRight = 5.76f;
        pdfPcell.VerticalAlignment = 5;
        PdfPCell cell = pdfPcell;
        if (index % 2 != 0)
          cell.PaddingLeft += 4.5f;
        Paragraph paragraph = new Paragraph();
        if (contactInformation.IsBusiness)
        {
          if (!string.IsNullOrEmpty(contactInformation.FirstName) && !string.IsNullOrEmpty(contactInformation.LastName))
            paragraph.Add((IElement) new Chunk(string.Format("{0}\n", (object) this.FormatFullName(contactInformation.FirstName, contactInformation.MiddleInitial, contactInformation.LastName)), new iTextSharp.text.Font(font, 12f)));
          paragraph.Add((IElement) new Chunk(string.Format("{0}\n", (object) contactInformation.BusinessName), new iTextSharp.text.Font(font, 12f)));
        }
        else
          paragraph.Add((IElement) new Chunk(string.Format("{0}\n", (object) this.FormatFullName(contactInformation.FirstName, contactInformation.MiddleInitial, contactInformation.LastName)), new iTextSharp.text.Font(font, 12f)));
        paragraph.Add((IElement) new Chunk(string.Format("{0}\n", (object) contactInformation.Address1), new iTextSharp.text.Font(font, 12f)));
        if (!string.IsNullOrEmpty(contactInformation.Address2))
          paragraph.Add((IElement) new Chunk(string.Format("{0}\n", (object) contactInformation.Address2), new iTextSharp.text.Font(font, 12f)));
        paragraph.Add((IElement) new Chunk(string.Format("{0}, {1} {2}", (object) contactInformation.City, contactInformation.LU_State == null ? (object) string.Empty : (object) contactInformation.LU_State.Description, (object) contactInformation.ZipCode), new iTextSharp.text.Font(font, 12f)));
        cell.AddElement((IElement) paragraph);
        pdfPtable.AddCell(cell);
      }
      pdfPtable.CompleteRow();
      document.Add((IElement) pdfPtable);
      instance.CloseStream = false;
      document.Close();
      memoryStream.Position = 0L;
      return (ActionResult) this.File((Stream) memoryStream, "application/pdf");
    }

    private string FormatFullName(string first, string middle, string last)
    {
      List<string> stringList = new List<string>();
      if (!string.IsNullOrEmpty(first))
        stringList.Add(first);
      if (!string.IsNullOrEmpty(middle))
        stringList.Add(middle);
      if (!string.IsNullOrEmpty(last))
        stringList.Add(last);
      return string.Join(" ", (IEnumerable<string>) stringList);
    }

    [HttpGet]
    public virtual JsonResult CopyConditions(int permitId, int permitTemplateId)
    {
      SessionHandler.SetSessionVar(SessionVariables.PermitConditionsTemplateId, (object) permitTemplateId);
      IService<LU_StandardCondition> conditionService = this._standardConditionService;
      int skip = 0;
      int maxValue = int.MaxValue;
      DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]{ new DynamicFilter("Active && PermitTemplateId == @0", new object[1]{ (object) permitTemplateId }) };
      foreach (LU_StandardCondition standardCondition in conditionService.GetRange(skip, maxValue, null, (IEnumerable<DynamicFilter>) dynamicFilterArray, null))
      {
        PermitCondition entity = new PermitCondition() { ConditionText = standardCondition.Description, PermitId = new int?(permitId), ConditionReportingPeriodId = standardCondition.ConditionReportingPeriodID, StandardConditionTypeId = standardCondition.StandardConditionTypeId, RequiresSelfReporting = standardCondition.RequiresSelfReporting, OneTimeReportDays = standardCondition.OneTimeReportDays, RequiresValidation = standardCondition.RequiresValidation, ComplianceReportingDueDate = standardCondition.DefaultComplianceReportingDueDate, PumpageReportTypeId = standardCondition.PumpageReportTypeId, LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name, LastModifiedDate = DateTime.Now };
        int? reportingPeriodId = entity.ConditionReportingPeriodId;
        if ((reportingPeriodId.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId.HasValue ? 1 : 0)) != 0)
          entity.ConditionText = entity.ConditionText + " Report is due on [OneTimeReporting.DueDate]";
        this._permitConditionService.Save(entity);
      }
      return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual ActionResult ResumeApplication()
    {
      return (ActionResult) this.View();
    }

    [HttpGet]
    public virtual JsonResult LoadApplicationsInProgress(GridCommand command)
    {
      try
      {
        List<PermitApplication> list = this._permitServiceClient.GetApplicationsInProgress().ToList<PermitApplication>();
        if (list.Count > 0)
        {
          int count = list.Count;
          return this.Json((object) new{ Data = list.Skip<PermitApplication>(command.Skip()).Take<PermitApplication>(command.PageSize).ToList<PermitApplication>(), PageTotal = (int) Math.Ceiling((double) count / (double) command.PageSize), CurrentPage = command.Page, RecordCount = count }, JsonRequestBehavior.AllowGet);
        }
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
      }
      return (JsonResult) null;
    }

    [HttpGet]
    [Authorization(AuthorizedRoles = new string[] {"IT Administrator", "Administrative Specialist", "Compliance / Enforcement Manager", "Compliance / Enforcement Staff", "Division Chief", "WSIPS eCommerce Member", "Operational Administrator", "Permit Supervisor", "Project Manager", "Secretary"})]
    public virtual ActionResult ApplicationWizard(int? id, int? applicationType)
    {
      try
      {
        ApplicationWizard applicationWizard;
        if (id.HasValue)
        {
          applicationWizard = this._permitServiceClient.GetApplicationWizardForExistingPermit(id.Value);
        }
        else
        {
          applicationWizard = this._permitServiceClient.InitializeApplicationWizardFrom();
          applicationWizard.ApplicationTypeId = new int?(applicationType ?? 1);
          if (this.TempData["RefId"] != null)
          {
            applicationWizard.RefId = new int?((int) this.TempData["RefId"]);
            applicationWizard.RefIdPermitNumber = (string) this.TempData["RefIdPermitNumber"];
          }
          else
            applicationWizard.RefId = new int?();
        }
        return (ActionResult) this.View((object) applicationWizard);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return (ActionResult) this.View();
      }
    }

    [HttpPost]
    public virtual JsonResult CancelPermitApplication(int permitId)
    {
      this._permittingService.CancelPermitApplication(permitId);
      return this.Json((object) new{ Success = true });
    }

    [HttpGet]
    public virtual JsonResult LinksModal(string permitNumber, string linkType)
    {
      IEnumerable<Permit> source = this._permittingService.GetRange(0, int.MaxValue, "RevisionNumber", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitNumber==@0", new object[1]{ (object) permitNumber }) }, "LU_PermitStatus, PermitConditions").Where<Permit>((Func<Permit, bool>) (x =>
      {
        if (x.PermitNumber != null)
          return x.RevisionNumber != null;
        return false;
      }));
      if (!source.Any<Permit>())
        return this.Json((object) new{ success = false, errormsg = "There is a problem with the permit number you provided. Please verify the completeness and accuracy of the permit number and try again" }, JsonRequestBehavior.AllowGet);
      Permit permit1 = source.Last<Permit>();
      Permit permit2 = source.Reverse<Permit>().FirstOrDefault<Permit>((Func<Permit, bool>) (x =>
      {
        int? permitStatusId = x.PermitStatusId;
        if (permitStatusId.GetValueOrDefault() == 46)
          return permitStatusId.HasValue;
        return false;
      }));
      switch (linkType)
      {
        case "RenewApplication":
          if (permit2 == null)
            return this.Json((object) new{ success = true, message = "There is no active revision for this permit." }, JsonRequestBehavior.AllowGet);
          this.TempData["RefId"] = (object) permit2.Id;
          this.TempData["RefIdPermitNumber"] = (object) permitNumber;
          return this.Json((object) new{ success = true, url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.ApplicationWizard(new int?(), new int?(3))) }, JsonRequestBehavior.AllowGet);
        case "ModificationApplication":
          if (permit2 == null)
            return this.Json((object) new{ success = true, message = "There is no active revision for this permit." }, JsonRequestBehavior.AllowGet);
          this.TempData["RefId"] = (object) permit2.Id;
          this.TempData["RefIdPermitNumber"] = (object) permitNumber;
          return this.Json((object) new{ success = true, url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.ApplicationWizard(new int?(), new int?(2))) }, JsonRequestBehavior.AllowGet);
        case "ViewOtherWaterWithdrawal":
          if (permit2 == null)
            return this.Json((object) new{ success = true, message = "This permit is not Active." }, JsonRequestBehavior.AllowGet);
          return this.Json((object) new{ success = true, url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(permit2.Id, new PermitDetailsTab?(PermitDetailsTab.Compliance))) }, JsonRequestBehavior.AllowGet);
        case "ViewWaterWithdrawal":
        case "EnterWaterWithdrawal":
          if (permit2 == null)
            return this.Json((object) new{ success = true, message = "There is no active revision for this permit." }, JsonRequestBehavior.AllowGet);
          if (permit2.PermitConditions.Count <= 0 || permit2.PermitConditions.FirstOrDefault<PermitCondition>((Func<PermitCondition, bool>) (x =>
          {
            int? standardConditionTypeId = x.StandardConditionTypeId;
            if (standardConditionTypeId.GetValueOrDefault() == 14)
              return standardConditionTypeId.HasValue;
            return false;
          })) == null)
            return this.Json((object) new{ success = true, message = "The active revision for this permit does not require withdrawal reporting." }, JsonRequestBehavior.AllowGet);
          this.TempData.Add("ShowReportingHistory", (object) true);
          return this.Json((object) new{ success = true, url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(permit2.Id, new PermitDetailsTab?(PermitDetailsTab.Compliance))) }, JsonRequestBehavior.AllowGet);
        case "ViewPermits":
          return this.Json((object) new{ success = true, url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(permit1.Id, new PermitDetailsTab?())) }, JsonRequestBehavior.AllowGet);
        case "ViewIPL":
          if (permit1.LU_PermitStatus != null)
          {
            int? permitCategoryId1 = permit1.LU_PermitStatus.PermitCategoryId;
            if ((permitCategoryId1.GetValueOrDefault() != 6 ? 0 : (permitCategoryId1.HasValue ? 1 : 0)) == 0)
            {
              int? permitCategoryId2 = permit1.LU_PermitStatus.PermitCategoryId;
              if ((permitCategoryId2.GetValueOrDefault() != 2 ? 0 : (permitCategoryId2.HasValue ? 1 : 0)) == 0)
                goto label_22;
            }
            return this.Json((object) new{ success = true, url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(permit1.Id, new PermitDetailsTab?(PermitDetailsTab.IPL))) }, JsonRequestBehavior.AllowGet);
          }
label_22:
          return this.Json((object) new{ success = true, message = "There is no active or pending revision for this permit." }, JsonRequestBehavior.AllowGet);
        case "ChangeContactInformation":
        case "ModifyExistingPermit":
        case "DeactivatePermit":
        case "ChangePermitToExemption":
          if (permit2 == null)
            return this.Json((object) new{ success = true, message = "Permit is not Active." }, JsonRequestBehavior.AllowGet);
          return this.Json((object) new{ success = true, url = ((UrlHelper) this.Url).Action((ActionResult) MVC.Permitting.Permit.Index(permit2.Id, new PermitDetailsTab?(PermitDetailsTab.Details))) }, JsonRequestBehavior.AllowGet);
        default:
          return this.Json((object) new{ success = false, errormsg = "There is a problem with the permit number you provided. Please verify the completeness and accuracy of the permit number and try again" }, JsonRequestBehavior.AllowGet);
      }
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator", "Administrative Specialist", "Compliance / Enforcement Manager", "Compliance / Enforcement Staff", "Division Chief", "WSIPS eCommerce Member", "Operational Administrator", "Permit Supervisor", "Project Manager", "Secretary"})]
    [HttpPost]
    public virtual JsonResult ApplicationWizard(ApplicationWizard form, WaterUseDetailForm waterUserDetailForm)
    {
      try
      {
        form.PermitId = this._permitServiceClient.SaveApplicantInformation(form);
        if (form.SavedWaterWithdrawalPurpose != null)
        {
          if (!form.NotSureWhatTheWaterUseTypeIs)
          {
            form.WaterUseDetailForm.PrivateWaterSuppilerForm = waterUserDetailForm.PrivateWaterSuppilerForm;
            this._waterUseDetailsServiceClient.SaveWaterUseDetails(form);
          }
        }
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
      return this.Json((object) new{ success = true, permitId = form.PermitId, permitStatusId = form.PermitStatusId, waterUserContactId = form.Contacts[0].Id, landOwnerContactId = form.Contacts[1].Id, consultantContactId = form.Contacts[2].Id });
    }

    [HttpGet]
    public virtual ActionResult CreatePendingPermit(int permitId)
    {
      Permit byId = this._permittingService.GetById(permitId, "LU_PermitStatus");
      PendingPermitForm pendingPermitForm = new PendingPermitForm() { PermitStausId = byId.PermitStatusId, PermitId = permitId, PermitCategoryId = byId.LU_PermitStatus.PermitCategoryId, ParentPermitNumber = this._permittingService.GetById(permitId, (string) null).PermitNumber };
      List<SelectListItem> list1 = new SelectList((IEnumerable) this._permitCatagoryService.GetAll((string) null).Where<LU_PermitCategory>((Func<LU_PermitCategory, bool>) (x =>
      {
        if (x.Id != 3)
          return x.Id == 4;
        return true;
      })), "Id", "Description").ToList<SelectListItem>();
      list1.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = ""
      });
      IEnumerable<LU_PermitType> source = this._permitTypeService.GetAll((string) null);
      int? permitStatusId = byId.PermitStatusId;
      if ((permitStatusId.GetValueOrDefault() != 46 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0)
        source = source.Where<LU_PermitType>((Func<LU_PermitType, bool>) (pt => pt.Id != 5));
      List<SelectListItem> list2 = new SelectList((IEnumerable) source, "Id", "Description").ToList<SelectListItem>();
      list2.Insert(0, new SelectListItem()
      {
        Text = "Select",
        Value = ""
      });
      pendingPermitForm.PendingPermitTypeList = (IEnumerable<SelectListItem>) list1;
      pendingPermitForm.PermitTypeList = (IEnumerable<SelectListItem>) list2;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.DetailsPartials._CreatePendingPermit, (object) pendingPermitForm);
    }

    [HttpGet]
    public virtual JsonResult GetPendingPermitCount(int permitId)
    {
      return this.Json((object) new{ count = this._permittingService.Count((IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("RefId == @0 && PermitStatusId != @1", new object[2]{ (object) permitId, (object) 51 }) }) }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual ActionResult CreatePendingPermit(PendingPermitForm form)
    {
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.DetailsPartials._CreatePendingPermit, (object) new PendingPermitForm());
    }

    [HttpGet]
    public virtual JsonResult GeneratePermitNumber(int permitId)
    {
      try
      {
        return this.Json((object) new{ PermitNumber = this._permittingService.GeneratePermitNumber(permitId) }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        return this.Json((object) new{ PermitNumber = "" }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual JsonResult IsUniquePermitNumber(string permitNumber)
    {
      try
      {
        return this.Json((object) new{ isUnique = this._permittingService.IsUniquePermitNumber(permitNumber) }, JsonRequestBehavior.AllowGet);
      }
      catch
      {
        return this.Json((object) new{ isUnique = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult ClonePermit(PendingPermitForm form)
    {
      string name = this.User.Identity.Name;
      return this.Json((object) new{ clonedPermitId = this._permitServiceClient.ClonePermit(form, name), permitNumber = form.PermitNumber }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetPermitNumber(int permitId)
    {
      return this.Json((object) new{ permitNumber = this._permittingService.GetById(permitId, (string) null).PermitNumber }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult SetPermitNumber(int permitId, string permitNumber)
    {
      if (this._permittingService.IsUniquePermitNumber(permitNumber))
      {
        Permit byId = this._permittingService.GetById(permitId, (string) null);
        if (byId != null)
        {
          byId.PermitNumber = permitNumber;
          this._permittingService.Save(byId);
          return this.Json((object) new{ success = true });
        }
      }
      return this.Json((object) new{ success = false });
    }

    [HttpGet]
    public virtual JsonResult GetPermitStatusesByCategory(int categoryId)
    {
      return this.Json((object) this._permitCatagoryService.GetById(categoryId, "LU_PermitStatus").LU_PermitStatus.Select(t => new{ Id = t.Id, PermitCategoryId = t.PermitCategoryId, Description = t.Description }), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult SetPermitStatus(int permitId, int permitStatusId)
    {
      try
      {
        int id = 0;
        Permit byId1 = this._permittingService.GetById(permitId, (string) null);
        byId1.PermitStatusId = new int?(permitStatusId);
        if (permitStatusId == 46 || permitStatusId == 51)
          byId1.DateOut = new DateTime?(DateTime.Now);
        if (permitStatusId == 52)
        {
          id = this._permittingService.ClonePermit(permitId, byId1.PermitTypeId.Value, byId1.PermitNumber, System.Web.HttpContext.Current.User.Identity.Name);
          Permit byId2 = this._permittingService.GetById(id, (string) null);
          byId2.PermitStatusId = new int?(52);
          byId2.DateOut = new DateTime?(DateTime.Now);
          byId2.RevisionNumber = (string) null;
          byId2.PermitTypeId = new int?(5);
          byId2.ApplicationTypeId = new int?(5);
          byId2.LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
          byId2.LastModifiedDate = DateTime.Now;
          this._permittingService.Save(byId2);
          byId1.DateOut = new DateTime?(DateTime.Now);
          byId1.PermitStatusId = new int?(50);
          byId1.LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
          byId1.LastModifiedDate = DateTime.Now;
        }
        this._permittingService.Save(byId1);
        string empty = string.Empty;
        return this.Json((object) new{ success = true, Url = (id <= 0 ? new UrlHelper(this.Request.RequestContext).Action(MVC.Permitting.Permit.ActionNames.Index, MVC.Permitting.Permit.Name, (object) new{ area = MVC.Permitting.Name, id = permitId }) : new UrlHelper(this.Request.RequestContext).Action(MVC.Permitting.Permit.ActionNames.Index, MVC.Permitting.Permit.Name, (object) new{ area = MVC.Permitting.Name, id = id })) });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    [HttpPost]
    public virtual JsonResult SetPreviousPermitStatus(int permitId)
    {
      try
      {
        Permit byId = this._permittingService.GetById(permitId, (string) null);
        PermitStatusHistory permitStatusHistory = (PermitStatusHistory) null;
        IEnumerable<PermitStatusHistory> range = this._permitStatusHistoryService.GetRange(0, 9999, "LastModifiedDate", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId=" + (object) permitId, new object[0]) }, (string) null);
        if (range != null && range.Count<PermitStatusHistory>() > 0)
          permitStatusHistory = range.ToArray<PermitStatusHistory>()[range.Count<PermitStatusHistory>() - 2];
        if (permitStatusHistory != null)
          byId.PermitStatusId = permitStatusHistory.PermitStatusId;
        this._permittingService.Save(byId);
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual JsonResult SearchContact(GridCommand command, ContactSearchForm searchForm)
    {
      List<int> intList = new List<int>();
      IEnumerable<Contact> contacts = (IEnumerable<Contact>) new List<Contact>();
      int num1 = 0;
      command.SortField = command.SortField == "" ? "LastName" : command.SortField;
      try
      {
        if (!string.IsNullOrEmpty(searchForm.SearchEmailAddress))
        {
          IEnumerable<ContactCommunicationMethod> range = this._contactCommunicationMethod.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("CommunicationValue.Contains(@0)", new object[1]{ (object) searchForm.SearchEmailAddress }) }, (string) null);
          intList.AddRange(range.Select<ContactCommunicationMethod, int>((Func<ContactCommunicationMethod, int>) (y => Convert.ToInt32(y.ContactId))));
        }
        if (!string.IsNullOrEmpty(searchForm.SearchPermitName))
        {
          IEnumerable<Permit> range1 = this._permittingService.GetRange(0, int.MaxValue, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitNumber==@0", new object[1]{ (object) searchForm.SearchPermitName }) }, (string) null);
          if (range1 != null)
          {
            foreach (Permit permit in range1)
            {
              IEnumerable<PermitContact> range2 = this._permitContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId==" + (object) permit.Id, new object[0]) }, (string) null);
              if (range2 != null && range2.Any<PermitContact>())
                intList.AddRange(range2.Select<PermitContact, int>((Func<PermitContact, int>) (y => Convert.ToInt32(y.ContactId))));
            }
          }
        }
        IEnumerable<DynamicFilter> filters = this._permitServiceClient.BuildFilter(searchForm, (IEnumerable<int>) intList);
        if (filters != null)
        {
          num1 = this._contactService.Count(filters);
          contacts = this._contactService.GetRange(command.Skip(), command.PageSize, command.SortField, filters, "ContactCommunicationMethods");
        }
        List<ContactSearchModel> contactSearchModelList = this._permitServiceClient.BuildResult(contacts);
        List<ContactForm> contactFormList = this._permitServiceClient.BuildContactForm(contacts);
        int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
        return this.Json((object) new{ Data = contactSearchModelList, PageTotal = num2, CurrentPage = command.Page, RecordCount = num1, ActualResult = contactFormList }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return (JsonResult) null;
      }
    }

    [HttpPost]
    public virtual JsonResult AddPermitLocation(int permitId, double x, double y, int srid)
    {
      try
      {
        this._permitLocationService.Save(new PermitLocation()
        {
          PermitId = permitId,
          WithdrawalLocationX = x,
          WithdrawalLocationY = y,
          SpatialReferenceId = srid
        });
        return this.Json((object) new{ Success = true });
      }
      catch
      {
        return this.Json((object) new{ Success = false });
      }
    }

    [HttpPost]
    public virtual JsonResult RemovePermitLocation(int id)
    {
      try
      {
        this._permitLocationService.Delete(id);
      }
      catch (Exception ex)
      {
        return this.Json((object) new{ Success = false });
      }
      return this.Json((object) new{ Success = true });
    }

    [HttpGet]
    public virtual PartialViewResult AddWaterWithdrawalLocation(int permitId, double x, double y, int srid)
    {
      try
      {
        WithdrawalLocationForm withdrawalLocationForm = Mapper.Map<PermitWaterWithdrawalLocation, WithdrawalLocationForm>(new PermitWaterWithdrawalLocation() { PermitId = permitId, WithdrawalSourceId = 1, IsExistingWell = new bool?(true), WithdrawalLocationX = x, WithdrawalLocationY = y, SpatialReferenceId = srid });
        withdrawalLocationForm.WithdrawalSourceList = (IEnumerable) this._waterWithdrawalSourceService.GetAll((string) null);
        withdrawalLocationForm.WithdrawalTypeList = Mapper.Map<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>(this._waterWithdrawalTypeService.GetAll((string) null));
        List<SelectListItem> list = Mapper.Map<IEnumerable<LU_StateStream>, SelectList>(this._stateStreamService.GetByWatershed(x, y, srid)).ToList<SelectListItem>();
        list.Add(new SelectListItem()
        {
          Text = "Other",
          Value = "-1"
        });
        withdrawalLocationForm.StateStreamList = new SelectList((IEnumerable) list, "Value", "Text");
        return this.PartialView((object) withdrawalLocationForm);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.PartialView(MVC.Shared.Views.Error);
      }
    }

    [HttpPost]
    public virtual JsonResult AddWaterWithdrawalLocation(WithdrawalLocationForm form)
    {
      try
      {
        this._permitWaterWithdrawalLocationService.Save(Mapper.Map<WithdrawalLocationForm, PermitWaterWithdrawalLocation>(form));
        return this.Json((object) new{ Success = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ Success = false });
      }
    }

    [HttpGet]
    public virtual JsonResult GetConservationEasement(int permitId)
    {
      Permit byId = this._permittingService.GetById(permitId, (string) null);
      return this.Json((object) new{ IsSubjectToEasement = byId.IsSubjectToEasement, HaveNotifiedEasementHolder = byId.HaveNotifiedEasementHolder }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult SetConservationEasement(int permitId, bool? isSubjectToEasement, bool? haveNotifiedEasementHolder)
    {
      try
      {
        Permit byId = this._permittingService.GetById(permitId, (string) null);
        byId.IsSubjectToEasement = isSubjectToEasement;
        byId.HaveNotifiedEasementHolder = haveNotifiedEasementHolder;
        this._permittingService.Save(byId);
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual PartialViewResult EditWaterWithdrawalLocation(int id)
    {
      try
      {
        if (id > 0)
        {
          PermitWaterWithdrawalLocation byId = this._permitWaterWithdrawalLocationService.GetById(id, (string) null);
          if (byId != null)
          {
            WithdrawalLocationForm withdrawalLocationForm = Mapper.Map<PermitWaterWithdrawalLocation, WithdrawalLocationForm>(byId);
            withdrawalLocationForm.WithdrawalSourceList = (IEnumerable) this._waterWithdrawalSourceService.GetAll((string) null);
            withdrawalLocationForm.WithdrawalTypeList = Mapper.Map<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>(this._waterWithdrawalTypeService.GetAll((string) null));
            List<SelectListItem> list = Mapper.Map<IEnumerable<LU_StateStream>, SelectList>(this._stateStreamService.GetByWatershed(withdrawalLocationForm.WithdrawalLocationX, withdrawalLocationForm.WithdrawalLocationY, withdrawalLocationForm.SpatialReferenceId)).ToList<SelectListItem>();
            list.Add(new SelectListItem()
            {
              Text = "Other",
              Value = "-1"
            });
            withdrawalLocationForm.StateStreamList = new SelectList((IEnumerable) list, "Value", "Text");
            return this.PartialView((object) withdrawalLocationForm);
          }
        }
        return (PartialViewResult) null;
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.PartialView(MVC.Shared.Views.Error);
      }
    }

    [HttpPost]
    public virtual JsonResult EditWaterWithdrawalLocation(WithdrawalLocationForm form)
    {
      try
      {
        this._permitWaterWithdrawalLocationService.Save(Mapper.Map<WithdrawalLocationForm, PermitWaterWithdrawalLocation>(form));
        return this.Json((object) new{ Success = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ Success = false });
      }
    }

    [HttpPost]
    public virtual JsonResult RemoveWaterWithdrawalLocation(int id)
    {
      try
      {
        this._permitWaterWithdrawalLocationService.Delete(id);
      }
      catch (Exception ex)
      {
        return this.Json((object) new{ Success = false });
      }
      return this.Json((object) new{ Success = true });
    }

    private ApplicationSummary CreateApplicationSummary(int id, string waterDetailsSelected)
    {
      ApplicationSummary applicationSummary = new ApplicationSummary();
      Permit byId = this._permittingService.GetById(id, string.Join(",", "LU_Aquifer", "LU_PermitStatus", "PermitConditions", "PermitCounties.LU_County", "PermitContacts.PermitContactInformation.LU_State", "PermitContacts.Contact.ContactCommunicationMethods", "PermitWaterWithdrawalPurposes.LU_WaterWithdrawalPurpose", "PermitWithdrawalGroundwaters.PermitWithdrawalGroundwaterDetails.LU_WaterWithdrawalType", "PermitWithdrawalGroundwaters.PermitWithdrawalGroundwaterDetails.LU_Watershed", "PermitWithdrawalSurfacewaters.PermitWithdrawalSurfacewaterDetails.LU_WaterWithdrawalType", "PermitWithdrawalSurfacewaters.PermitWithdrawalSurfacewaterDetails.LU_Watershed", "PermitWithdrawalSurfacewaters.PermitWithdrawalSurfacewaterDetails.LU_StateStream"));
      if (byId != null)
      {
        applicationSummary.AppWizard = new ApplicationWizard();
        applicationSummary.Permit = byId;
        int[] array = ((IEnumerable<string>) waterDetailsSelected.Split(',')).Where<string>((Func<string, bool>) (x =>
        {
          if (!string.IsNullOrEmpty(x))
            return Convert.ToInt32(x) != 0;
          return false;
        })).Select<string, int>((Func<string, int>) (x => Convert.ToInt32(x))).ToArray<int>();
        if (byId.LU_PermitStatus != null && byId.LU_PermitStatus.PermitCategoryId.HasValue)
        {
          applicationSummary.AppWizard.PermitCategoryId = byId.LU_PermitStatus.PermitCategoryId.Value;
          int? permitCategoryId1 = byId.LU_PermitStatus.PermitCategoryId;
          if ((permitCategoryId1.GetValueOrDefault() != 1 ? 0 : (permitCategoryId1.HasValue ? 1 : 0)) != 0 && byId.ReceivedDate.HasValue)
          {
            applicationSummary.ReceivedDate = byId.ReceivedDate.ToString();
          }
          else
          {
            int? permitCategoryId2 = byId.LU_PermitStatus.PermitCategoryId;
            if ((permitCategoryId2.GetValueOrDefault() != 1 ? 0 : (permitCategoryId2.HasValue ? 1 : 0)) != 0 && !byId.ReceivedDate.HasValue)
              applicationSummary.ReceivedDate = "Unknown";
          }
        }
        if (byId.PermitContacts != null)
        {
          applicationSummary.PrimaryLandOwner = this.GetSummaryContact(byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
          {
            if (!x.IsPrimary)
              return false;
            int? contactTypeId = x.ContactTypeId;
            if (contactTypeId.GetValueOrDefault() == 16)
              return contactTypeId.HasValue;
            return false;
          })));
          applicationSummary.WaterUser = this.GetSummaryContact(byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
          {
            if (!x.IsPrimary)
              return false;
            int? contactTypeId = x.ContactTypeId;
            if (contactTypeId.GetValueOrDefault() == 27)
              return contactTypeId.HasValue;
            return false;
          })));
          applicationSummary.Applicant = this.GetSummaryContact(byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
          {
            if (x.IsPrimary)
              return x.IsApplicant;
            return false;
          })));
          PermitContact permitContact = byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (x =>
          {
            if (x.IsPermittee)
              return x.IsPrimary;
            return false;
          }));
          if (permitContact != null && permitContact.PermitContactInformation != null)
            applicationSummary.Permittee = !permitContact.PermitContactInformation.IsBusiness || string.IsNullOrEmpty(permitContact.PermitContactInformation.BusinessName) ? permitContact.PermitContactInformation.FirstName + (string.IsNullOrEmpty(permitContact.PermitContactInformation.MiddleInitial) ? " " : " " + permitContact.PermitContactInformation.MiddleInitial + " ") + permitContact.PermitContactInformation.LastName : permitContact.PermitContactInformation.BusinessName;
          foreach (PermitContact contact in byId.PermitContacts.Where<PermitContact>((Func<PermitContact, bool>) (x =>
          {
            if (x.IsPrimary)
              return false;
            int? contactTypeId = x.ContactTypeId;
            if (contactTypeId.GetValueOrDefault() == 16)
              return contactTypeId.HasValue;
            return false;
          })))
            applicationSummary.AdditionalLandOwners.Add(this.GetSummaryContact(contact));
        }
        applicationSummary.AppWizard.UseDescription = byId.UseDescription;
        applicationSummary.AppWizard.WaterUseDetailForm = this._waterUseDetailsServiceClient.InitializeWaterUseDetailFrom(id);
        applicationSummary.AppWizard.WastewaterTreatmentDisposalForm = this._waterUseDetailsServiceClient.GetWasteWaterTreatmentAndDisposal(string.Join<int>(",", (IEnumerable<int>) array), id).Where<WastewaterTreatmentAndDisposalForm>((Func<WastewaterTreatmentAndDisposalForm, bool>) (x => x.Id != 0)).ToList<WastewaterTreatmentAndDisposalForm>();
        if (((IEnumerable<int>) array).Any<int>())
        {
          IEnumerable<LU_WaterWithdrawalPurposeCategory> all = this._waterWithdrawalPurposeCategory.GetAll("LU_WaterWithdrawalPurpose");
          applicationSummary.SelectedUseDetailCategories = new List<string>();
          foreach (LU_WaterWithdrawalPurposeCategory withdrawalPurposeCategory in all)
          {
            foreach (LU_WaterWithdrawalPurpose withdrawalPurpose in (IEnumerable<LU_WaterWithdrawalPurpose>) withdrawalPurposeCategory.LU_WaterWithdrawalPurpose)
            {
              if (((IEnumerable<int>) array).Contains<int>(withdrawalPurpose.Id))
                applicationSummary.SelectedUseDetailCategories.Add(withdrawalPurpose.Description);
            }
          }
        }
        applicationSummary.PermitWithdrawalGroundwater = byId.PermitWithdrawalGroundwaters.FirstOrDefault<PermitWithdrawalGroundwater>();
        applicationSummary.PermitWithdrawalSurfacewater = byId.PermitWithdrawalSurfacewaters.FirstOrDefault<PermitWithdrawalSurfacewater>();
        applicationSummary.PermitCounty = byId.PermitCounties.Any<PermitCounty>() ? byId.PermitCounties.First<PermitCounty>().LU_County.Description : "Not Known";
        applicationSummary.RequiresWithdrawalReporting = byId.PermitConditions.Any<PermitCondition>((Func<PermitCondition, bool>) (x =>
        {
          int? standardConditionTypeId = x.StandardConditionTypeId;
          if (standardConditionTypeId.GetValueOrDefault() == 14)
            return standardConditionTypeId.HasValue;
          return false;
        })) ? "Yes" : "No";
      }
      return applicationSummary;
    }

    private string FormatAddress(PermitContactInformation address)
    {
      if (address == null)
        return "";
      return (address.Address1 == null ? "" : (address.Address1.Trim().Length > 0 ? address.Address1.Trim() + ", " : "")) + (address.Address2 == null ? "" : (address.Address2.Trim().Length > 0 ? address.Address2.Trim() + ", " : "")) + (address.City == null ? "" : (address.City.Trim().Length > 0 ? address.City.Trim() + ", " : "")) + (!address.StateId.HasValue ? "" : ((object) address.LU_State == null ? "" : address.LU_State.Description + ", ")) + (address.ZipCode ?? "");
    }

    private SummaryContact GetSummaryContact(PermitContact contact)
    {
      SummaryContact summaryContact = (SummaryContact) null;
      if (contact != null && contact.PermitContactInformationId.HasValue)
      {
        summaryContact = new SummaryContact()
        {
          ContactId = contact.ContactId,
          Name = contact.PermitContactInformation.FirstName + (string.IsNullOrEmpty(contact.PermitContactInformation.MiddleInitial) ? " " : " " + contact.PermitContactInformation.MiddleInitial + " ") + contact.PermitContactInformation.LastName,
          /*SecondaryName = contact.PermitContactInformation.SecondaryName,*/
          Address = this.FormatAddress(contact.PermitContactInformation),
          BusinessName = contact.PermitContactInformation.BusinessName
        };
        ICollection<ContactCommunicationMethod> communicationMethods = contact.Contact.ContactCommunicationMethods;
        if (communicationMethods != null)
        {
          if (communicationMethods.Any<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
          {
            if (x.IsPrimaryEmail.HasValue)
              return x.IsPrimaryEmail.Value;
            return false;
          })))
            summaryContact.Email = communicationMethods.Single<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
            {
              if (x.IsPrimaryEmail.HasValue)
                return x.IsPrimaryEmail.Value;
              return false;
            })).CommunicationValue;
          if (communicationMethods.Any<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
          {
            if (x.IsPrimaryPhone.HasValue)
              return x.IsPrimaryPhone.Value;
            return false;
          })))
            summaryContact.Phone = communicationMethods.Single<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (x =>
            {
              if (x.IsPrimaryPhone.HasValue)
                return x.IsPrimaryPhone.Value;
              return false;
            })).CommunicationValue;
        }
      }
      return summaryContact;
    }

    public virtual ActionResult Summary(int id, string waterDetailsSelected)
    {
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.Summary, (object) this.CreateApplicationSummary(id, waterDetailsSelected));
    }

    [HttpPost]
    public virtual JsonResult Affirmation(int id, string submittedBy, string submittedDate)
    {
      List<Action> actionList = new List<Action>();
      try
      {
        this._permittingService.Save(this._permittingService.GetById(id, (string) null));
        Permit byId = this._permittingService.GetById(id, "PermitContacts.Contact.ContactCommunicationMethods,PermitCounties,PermitWithdrawalGroundwaters,PermitWithdrawalSurfacewaters,PermitWaterWithdrawalPurposes.LU_WaterWithdrawalPurpose");
        if (byId == null)
          throw new Exception("Permit " + (object) id + " does not exist");
        if (byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (pc =>
        {
          if (pc.IsApplicant)
            return pc.IsPrimary;
          return false;
        })) == null)
          throw new Exception("Permit " + (object) id + " does not contain an applicant contact");
        PermitCounty permitCounty = byId.PermitCounties.FirstOrDefault<PermitCounty>();
        if (permitCounty == null)
          throw new Exception("Permit " + (object) id + " does not contain any counties");
        if (PermitController.IsExemption(byId))
        {
          AdministrativeSpecialist administrativeSpecialist = this._administrativeSpecialistService.GetRange(0, 1, "CreatedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active", new object[0]) }, (string) null).SingleOrDefault<AdministrativeSpecialist>();
          if (administrativeSpecialist != null)
          {
            int asPermitContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = administrativeSpecialist.ContactId, ContactTypeId = new int?(36), IsApplicant = false, IsPrimary = true, Active = true });
            if (asPermitContactId <= 0)
              throw new Exception("Error saving Administrative Specialist Permit Contact");
            actionList.Add((Action) (() => this._permitContactService.Delete(asPermitContactId)));
          }
          byId.PermitStatusId = new int?(3);
          byId.SubmittedBy = submittedBy;
          byId.SubmittedDate = new DateTime?(Convert.ToDateTime(submittedDate));
          byId.PermitContacts = (ICollection<PermitContact>) null;
          byId.PermitCounties = (ICollection<PermitCounty>) null;
          byId.PermitWaterWithdrawalPurposes = (ICollection<PermitWaterWithdrawalPurpose>) null;
          byId.PermitWithdrawalGroundwaters = (ICollection<PermitWithdrawalGroundwater>) null;
          byId.PermitWithdrawalSurfacewaters = (ICollection<PermitWithdrawalSurfacewater>) null;
          return this.Json((object) new{ Success = true, Message = "Based on the information provided in this application, the requested water appropriation may be eligible for an exemption. MDE will review the application and make a final determination.", PermitId = this._permittingService.Save(byId) }, JsonRequestBehavior.AllowGet);
        }
        ProjectManagerCounty projectManagerCounty = this._projectManagerCountyService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("CountyId==@0", new object[1]{ (object) permitCounty.CountyId }) }, "ProjectManager.Supervisor.DivisionChief").FirstOrDefault<ProjectManagerCounty>();
        if (projectManagerCounty == null)
          throw new Exception("No project manager assigned to county " + (object) permitCounty.CountyId);
        ProjectManager projectManager = projectManagerCounty.ProjectManager;
        if (projectManager == null)
          throw new Exception("Project manager " + (object) projectManagerCounty.ProjectManagerId + " does not exist");
        int pmContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = projectManager.ContactId, ContactTypeId = new int?(20), IsApplicant = false, IsPrimary = true, Active = true });
        if (pmContactId <= 0)
          throw new Exception("Error saving Project Manager Contact");
        actionList.Add((Action) (() => this._permitContactService.Delete(pmContactId)));
        Supervisor supervisor = projectManager.Supervisor;
        if (supervisor != null)
        {
          int supContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = supervisor.ContactId, ContactTypeId = new int?(24), IsApplicant = false, IsPrimary = true, Active = true });
          if (supContactId <= 0)
            throw new Exception("Error saving Supervisor Contact");
          actionList.Add((Action) (() => this._permitContactService.Delete(supContactId)));
          DivisionChief divisionChief = supervisor.DivisionChief;
          if (divisionChief != null)
          {
            int dcContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = divisionChief.ContactId, ContactTypeId = new int?(32), IsApplicant = false, IsPrimary = true, Active = true });
            if (dcContactId <= 0)
              throw new Exception("Error saving Division Chief Contact");
            actionList.Add((Action) (() => this._permitContactService.Delete(dcContactId)));
          }
        }
        Secretary secretary = this._secretaryService.GetRange(0, 1, "CreatedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Active", new object[0]) }, (string) null).SingleOrDefault<Secretary>();
        if (secretary != null)
        {
          int secPermitContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = secretary.ContactId, ContactTypeId = new int?(31), IsApplicant = false, IsPrimary = true, Active = true });
          if (secPermitContactId <= 0)
            throw new Exception("Error saving Secretary Permit Contact");
          actionList.Add((Action) (() => this._permitContactService.Delete(secPermitContactId)));
        }
        Contact contact1 = this._contactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("CountyId==@0&&ContactTypeId==@1", new object[2]{ (object) permitCounty.CountyId, (object) 10 }) }, "ContactCommunicationMethods").FirstOrDefault<Contact>();
        if (contact1 != null)
        {
          int dnrContactContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = contact1.Id, ContactTypeId = new int?(10), IsApplicant = false, IsPrimary = true, Active = true });
          if (dnrContactContactId <= 0)
            throw new Exception("Error saving Department or Natural Resources Contact");
          actionList.Add((Action) (() => this._permitContactService.Delete(dnrContactContactId)));
        }
        Contact contact2 = this._contactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("CountyId==@0&&ContactTypeId==@1", new object[2]{ (object) permitCounty.CountyId, (object) 33 }) }, "ContactCommunicationMethods").FirstOrDefault<Contact>();
        if (contact2 != null)
        {
          int environmentalHealthDirectorContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = contact2.Id, ContactTypeId = new int?(33), IsApplicant = false, IsPrimary = true, Active = true });
          if (environmentalHealthDirectorContactId <= 0)
            throw new Exception("Error saving Environmental Health Director Contact");
          actionList.Add((Action) (() => this._permitContactService.Delete(environmentalHealthDirectorContactId)));
        }
        if (byId.PermitWaterWithdrawalPurposes.Any<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (pwp =>
        {
          int? purposeCategoryId = pwp.LU_WaterWithdrawalPurpose.WaterWithdrawalPurposeCategoryId;
          if (purposeCategoryId.GetValueOrDefault() > 1)
            return purposeCategoryId.HasValue;
          return false;
        })))
        {
          Contact contact3 = this._contactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("CountyId==@0&&ContactTypeId==@1", new object[2]{ (object) permitCounty.CountyId, (object) 9 }) }, "ContactCommunicationMethods").FirstOrDefault<Contact>();
          if (contact3 != null)
          {
            int countyOfficialContactId = this._permitContactService.Save(new PermitContact() { PermitId = byId.Id, ContactId = contact3.Id, ContactTypeId = new int?(9), IsApplicant = false, IsPrimary = true, Active = true });
            if (countyOfficialContactId <= 0)
              throw new Exception("Error saving County Official Contact");
            actionList.Add((Action) (() => this._permitContactService.Delete(countyOfficialContactId)));
          }
        }
        byId.PermitStatusId = new int?(3);
        byId.SubmittedBy = submittedBy;
        byId.SubmittedDate = new DateTime?(Convert.ToDateTime(submittedDate));
        byId.PermitContacts = (ICollection<PermitContact>) null;
        byId.PermitCounties = (ICollection<PermitCounty>) null;
        byId.PermitWaterWithdrawalPurposes = (ICollection<PermitWaterWithdrawalPurpose>) null;
        byId.PermitWithdrawalGroundwaters = (ICollection<PermitWithdrawalGroundwater>) null;
        byId.PermitWithdrawalSurfacewaters = (ICollection<PermitWithdrawalSurfacewater>) null;
        return this.Json((object) new{ Success = true, Message = "Based on the information provided in this application, the requested water appropriation is not eligible for an exemption. This application will enter the water appropriation permitting process.", PermitId = this._permittingService.Save(byId) }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        actionList.ForEach((Action<Action>) (t => t()));
        return this.Json((object) new{ Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult UpdateStatus(int permitId, int permitStatusId, bool completed)
    {
      try
      {
        IEnumerable<PermitStatusState> range = this._permitStatusStateService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == @0 && PermitStatusId == @1", new object[2]{ (object) permitId, (object) permitStatusId }) }, (string) null);
        if (range != null && range.Any<PermitStatusState>())
        {
          PermitStatusState entity = range.First<PermitStatusState>();
          entity.Completed = completed;
          this._permitStatusStateService.Save(entity);
        }
        else
          this._permitStatusStateService.Save(new PermitStatusState()
          {
            PermitId = new int?(permitId),
            PermitStatusId = new int?(permitStatusId),
            Completed = completed
          });
        Permit byId = this._permittingService.GetById(permitId, (string) null);
        int status = this._permittingService.CalculateStatus(permitId);
        byId.PermitStatusId = new int?(status);
        this._permittingService.Save(byId);
        string sessionVar = SessionHandler.GetSessionVar<string>(SessionVariables.AuthenticatedUserFullName);
        return this.Json((object) new{ IsSuccess = true, statusId = status, completedBy = ("Completed by " + sessionVar + " " + (object) DateTime.Now) });
      }
      catch (Exception ex)
      {
        return this.Json((object) new{ IsSuccess = false });
      }
    }

    private static bool IsExemption(Permit application)
    {
      bool flag1 = application.PermitWaterWithdrawalPurposes.Any<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (pwp =>
      {
        int? purposeCategoryId = pwp.LU_WaterWithdrawalPurpose.WaterWithdrawalPurposeCategoryId;
        if (purposeCategoryId.GetValueOrDefault() == 1)
          return purposeCategoryId.HasValue;
        return false;
      }));
      long num = 0;
      PermitWithdrawalGroundwater withdrawalGroundwater = application.PermitWithdrawalGroundwaters.FirstOrDefault<PermitWithdrawalGroundwater>();
      if (withdrawalGroundwater != null)
        num += withdrawalGroundwater.AvgGalPerDay;
      bool flag2 = application.PermitWithdrawalSurfacewaters.Any<PermitWithdrawalSurfacewater>();
      bool flag3 = application.PermitWaterWithdrawalPurposes.Any<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (pwp =>
      {
        int? purposeCategoryId = pwp.LU_WaterWithdrawalPurpose.WaterWithdrawalPurposeCategoryId;
        if (purposeCategoryId.GetValueOrDefault() == 5)
          return purposeCategoryId.HasValue;
        return false;
      }));
      bool flag4 = application.PermitWaterWithdrawalPurposes.Any<PermitWaterWithdrawalPurpose>((Func<PermitWaterWithdrawalPurpose, bool>) (pwp =>
      {
        int? withdrawalPurposeId = pwp.WaterWithdrawalPurposeId;
        if (withdrawalPurposeId.GetValueOrDefault() == 55)
          return withdrawalPurposeId.HasValue;
        return false;
      }));
      bool valueOrDefault = application.IsInWMStrategyArea.GetValueOrDefault();
      if (!flag1 && !flag2 && (num > 0L && num <= 5000L) && (!flag3 && !flag4))
        return !valueOrDefault;
      return false;
    }

    [HttpPost]
    public virtual JsonResult GenerateComplianceReports(int permitId)
    {
      try
      {
        IEnumerable<PermitCondition> range = this._permitConditionService.GetRange(0, int.MaxValue, string.Empty, (IEnumerable<DynamicFilter>) new DynamicFilter[2]{ new DynamicFilter("PermitId == " + (object) permitId, new object[0]), new DynamicFilter("RequiresSelfReporting == True", new object[0]) }, (string) null);
        this._permitServiceClient.BuildAndSaveConditionCompliance(permitId, 4, range);
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpDelete]
    public virtual JsonResult DeleteComplianceReports(int permitId)
    {
      try
      {
        foreach (PermitCondition permitCondition in this._permitConditionService.GetRange(0, int.MaxValue, string.Empty, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId == " + (object) permitId, new object[0]) }, (string) null))
        {
          foreach (ConditionCompliance conditionCompliance in this._conditionComplianceService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitConditionId == " + (object) permitCondition.Id, new object[0]) }, (string) null))
            this._conditionComplianceService.Delete(conditionCompliance.Id);
        }
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual FileResult CreatePdf(int id, string waterDetailsSelected)
    {
      ApplicationSummary summary = this.CreateApplicationSummary(id, waterDetailsSelected);
      MemoryStream memoryStream = new MemoryStream();
      iTextSharp.text.Document document1 = new iTextSharp.text.Document();
      document1.SetMargins(document1.LeftMargin, document1.RightMargin, 180f, document1.BottomMargin);
      iTextSharp.text.Font font = FontFactory.GetFont("Times-Roman", "Cp1252", false, 12f, 1);
      FontFactory.GetFont("Times-Roman", "Cp1252", false, 12f);
      FontFactory.GetFont("Times-Roman", "Cp1252", false, 10f);
      PdfWriter instance = PdfWriter.GetInstance(document1, (Stream) memoryStream);
      instance.CloseStream = false;
      instance.PageEvent = (IPdfPageEvent) new PermitController.PdfHeader();
      document1.Open();
      Paragraph paragraph1 = new Paragraph(" ");
      document1.Add((IElement) new Paragraph(DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString()));
      document1.Add((IElement) paragraph1);
      document1.Add((IElement) new Phrase("Permit Number: ", font));
      document1.Add((IElement) new Phrase(summary.Permit.PermitName));
      document1.Add((IElement) paragraph1);
      if (!string.IsNullOrEmpty(summary.Permit.SubmittedBy))
      {
        document1.Add((IElement) new Phrase("Submitted By: ", font));
        document1.Add((IElement) new Phrase(summary.Permit.SubmittedBy));
        document1.Add((IElement) paragraph1);
      }
      DateTime? nullable1;
      if (summary.AppWizard.PermitCategoryId == 1)
      {
        nullable1 = summary.Permit.SubmittedDate;
        if (nullable1.HasValue)
        {
          document1.Add((IElement) new Phrase("Submitted On: ", font));
          document1.Add((IElement) new Phrase(summary.Permit.SubmittedDate.Value.ToShortDateString()));
          document1.Add((IElement) paragraph1);
          goto label_23;
        }
      }
      if (summary.AppWizard.PermitCategoryId == 2 || summary.AppWizard.PermitCategoryId == 3 || (summary.AppWizard.PermitCategoryId == 4 || summary.AppWizard.PermitCategoryId == 5))
      {
        if (summary.Permit.ReceivedDate.HasValue)
        {
          Paragraph paragraph2 = new Paragraph();
          paragraph2.Add((IElement) new Phrase("Received On: ", font));
          paragraph2.Add((IElement) new Phrase(summary.Permit.ReceivedDate.Value.ToShortDateString()));
          document1.Add((IElement) paragraph2);
        }
      }
      else if (summary.AppWizard.PermitCategoryId == 6)
      {
        if (summary.Permit.ReceivedDate.HasValue)
        {
          Paragraph paragraph2 = new Paragraph();
          paragraph2.Add((IElement) new Phrase("Received On: ", font));
          Paragraph paragraph3 = paragraph2;
          nullable1 = summary.Permit.ReceivedDate;
          Phrase phrase = new Phrase(nullable1.Value.ToShortDateString());
          paragraph3.Add((IElement) phrase);
          document1.Add((IElement) paragraph2);
        }
        nullable1 = summary.Permit.DateOut;
        if (nullable1.HasValue)
        {
          Paragraph paragraph2 = new Paragraph();
          paragraph2.Add((IElement) new Phrase("Issued Date: ", font));
          Paragraph paragraph3 = paragraph2;
          nullable1 = summary.Permit.DateOut;
          Phrase phrase = new Phrase(nullable1.Value.ToShortDateString());
          paragraph3.Add((IElement) phrase);
          document1.Add((IElement) paragraph2);
        }
        nullable1 = summary.Permit.AppropriationDate;
        if (nullable1.HasValue)
        {
          Paragraph paragraph2 = new Paragraph();
          paragraph2.Add((IElement) new Phrase("First Appropriation Date: ", font));
          Paragraph paragraph3 = paragraph2;
          nullable1 = summary.Permit.AppropriationDate;
          Phrase phrase = new Phrase(nullable1.Value.ToShortDateString());
          paragraph3.Add((IElement) phrase);
          document1.Add((IElement) paragraph2);
        }
        nullable1 = summary.Permit.EffectiveDate;
        if (nullable1.HasValue)
        {
          Paragraph paragraph2 = new Paragraph();
          paragraph2.Add((IElement) new Phrase("Effective Date: ", font));
          Paragraph paragraph3 = paragraph2;
          nullable1 = summary.Permit.EffectiveDate;
          Phrase phrase = new Phrase(nullable1.Value.ToShortDateString());
          paragraph3.Add((IElement) phrase);
          document1.Add((IElement) paragraph2);
        }
        nullable1 = summary.Permit.ExpirationDate;
        if (nullable1.HasValue)
        {
          Paragraph paragraph2 = new Paragraph();
          paragraph2.Add((IElement) new Phrase("Expiration Date: ", font));
          Paragraph paragraph3 = paragraph2;
          nullable1 = summary.Permit.ExpirationDate;
          Phrase phrase = new Phrase(nullable1.Value.ToShortDateString());
          paragraph3.Add((IElement) phrase);
          document1.Add((IElement) paragraph2);
        }
        document1.Add((IElement) new Phrase("Requires Withdrawal Reporting? ", font));
        document1.Add((IElement) new Phrase(summary.RequiresWithdrawalReporting));
        document1.Add((IElement) paragraph1);
      }
      else if (summary.AppWizard.PermitCategoryId == 7)
      {
        nullable1 = summary.Permit.DateOut;
        if (nullable1.HasValue)
        {
          Paragraph paragraph2 = new Paragraph();
          paragraph2.Add((IElement) new Phrase("Issued Date: ", font));
          Paragraph paragraph3 = paragraph2;
          nullable1 = summary.Permit.DateOut;
          Phrase phrase = new Phrase(nullable1.Value.ToShortDateString());
          paragraph3.Add((IElement) phrase);
          document1.Add((IElement) paragraph2);
        }
      }
label_23:
      document1.Add((IElement) new Paragraph("Applicant", font));
      if (summary.Applicant != null)
      {
        if (!string.IsNullOrEmpty(summary.Applicant.BusinessName))
          document1.Add((IElement) new Paragraph(" Business Name: " + summary.Applicant.BusinessName));
        document1.Add((IElement) new Paragraph(" Name: " + summary.Applicant.Name));
        /*document1.Add((IElement) new Paragraph(" Secondary Name: " + summary.Applicant.SecondaryName));*/
        document1.Add((IElement) new Paragraph(" Address: " + summary.Applicant.Address));
        document1.Add((IElement) new Paragraph(" Phone: " + summary.Applicant.Phone));
        document1.Add((IElement) new Paragraph(" Email: " + summary.Applicant.Email));
      }
      else
        document1.Add((IElement) new Paragraph(" Applicant information could not be found"));
      document1.Add((IElement) new Paragraph(" "));
      document1.Add((IElement) new Paragraph("Water User", font));
      if (summary.WaterUser != null)
      {
        if (summary.Applicant != null && summary.Applicant.ContactId == summary.WaterUser.ContactId)
        {
          document1.Add((IElement) new Paragraph(" Same as Applicant"));
        }
        else
        {
          if (!string.IsNullOrEmpty(summary.WaterUser.BusinessName))
            document1.Add((IElement) new Paragraph(" Business Name: " + summary.WaterUser.BusinessName));
          document1.Add((IElement) new Paragraph(" Name: " + summary.WaterUser.Name));
          /*document1.Add((IElement) new Paragraph(" Secondary Name: " + summary.WaterUser.SecondaryName));*/
          document1.Add((IElement) new Paragraph(" Address: " + summary.WaterUser.Address));
          document1.Add((IElement) new Paragraph(" Phone: " + summary.WaterUser.Phone));
          document1.Add((IElement) new Paragraph(" Email: " + summary.WaterUser.Email));
        }
      }
      else
        document1.Add((IElement) new Paragraph(" Water User information could not be found"));
      document1.Add((IElement) new Paragraph(" "));
      document1.Add((IElement) new Paragraph("Primary Land Owner", font));
      if (summary.PrimaryLandOwner != null)
      {
        if (summary.Applicant != null && summary.Applicant.ContactId == summary.PrimaryLandOwner.ContactId)
        {
          document1.Add((IElement) new Paragraph(" Same as Applicant"));
        }
        else
        {
          if (!string.IsNullOrEmpty(summary.PrimaryLandOwner.BusinessName))
            document1.Add((IElement) new Paragraph(" Business Name: " + summary.PrimaryLandOwner.BusinessName));
          document1.Add((IElement) new Paragraph(" Name: " + summary.PrimaryLandOwner.Name));
          /*document1.Add((IElement) new Paragraph(" Secondary Name: " + summary.PrimaryLandOwner.SecondaryName));*/
          document1.Add((IElement) new Paragraph(" Address: " + summary.PrimaryLandOwner.Address));
          document1.Add((IElement) new Paragraph(" Phone: " + summary.PrimaryLandOwner.Phone));
          document1.Add((IElement) new Paragraph(" Email: " + summary.PrimaryLandOwner.Email));
        }
      }
      else
        document1.Add((IElement) new Paragraph(" Land Owner information could not be found"));
      document1.Add((IElement) new Paragraph(" "));
      if (summary.AdditionalLandOwners != null && summary.AdditionalLandOwners.Count > 0)
      {
        document1.Add((IElement) new Paragraph("Additional Land Owners", font));
        foreach (SummaryContact additionalLandOwner in summary.AdditionalLandOwners)
        {
          if (!string.IsNullOrEmpty(additionalLandOwner.BusinessName))
            document1.Add((IElement) new Paragraph(" Business Name: " + additionalLandOwner.BusinessName));
          document1.Add((IElement) new Paragraph(" Name: " + additionalLandOwner.Name));
          /*document1.Add((IElement) new Paragraph(" Secondary Name: " + additionalLandOwner.SecondaryName));*/
          document1.Add((IElement) new Paragraph(" Address: " + additionalLandOwner.Address));
          document1.Add((IElement) new Paragraph(" Phone: " + additionalLandOwner.Phone));
          document1.Add((IElement) new Paragraph(" Email: " + additionalLandOwner.Email));
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      document1.Add((IElement) new Paragraph("Permittee", font));
      if (!string.IsNullOrEmpty(summary.Permittee) && !string.IsNullOrEmpty(summary.Permittee.Trim()))
        document1.Add((IElement) new Paragraph(" " + summary.Permittee));
      else
        document1.Add((IElement) new Paragraph(" Permittee information could not be found"));
      document1.Add((IElement) paragraph1);
      document1.Add((IElement) new Paragraph("Water-Use Description", font));
      document1.Add((IElement) new Paragraph(" " + summary.AppWizard.UseDescription));
      document1.Add((IElement) paragraph1);
      if (summary.SelectedUseDetailCategories != null)
      {
        document1.Add((IElement) new Paragraph("Selected Water Use Details", font));
        foreach (string str in summary.SelectedUseDetailCategories.Where<string>((Func<string, bool>) (cat => !string.IsNullOrEmpty(cat))))
          document1.Add((IElement) new Paragraph(" " + str));
        document1.Add((IElement) paragraph1);
      }
      if (summary.AppWizard.WaterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms != null && summary.AppWizard.WaterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms.Count > 0)
      {
        document1.Add((IElement) new Paragraph("Poultry Evaporative", font));
        foreach (PoultryWateringForm poultryWateringForm in summary.AppWizard.WaterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms)
        {
          PoultryWateringForm poultryWateringEvaporativeForm = poultryWateringForm;
          int? nullable2 = poultryWateringEvaporativeForm.PoultryId;
          if (nullable2.HasValue)
            document1.Add((IElement) new Paragraph(" Poultry Type: " + poultryWateringEvaporativeForm.PoultryTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == poultryWateringEvaporativeForm.PoultryId.Value.ToString())).Text));
          nullable2 = poultryWateringEvaporativeForm.NumberOfHouses;
          if (nullable2.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Houses: ";
            nullable2 = poultryWateringEvaporativeForm.NumberOfHouses;
            Paragraph paragraph2 = new Paragraph(str + nullable2.Value);
            document2.Add((IElement) paragraph2);
          }
          nullable2 = poultryWateringEvaporativeForm.NumberOfFlocksPerYear;
          if (nullable2.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Flocks per Year: ";
            nullable2 = poultryWateringEvaporativeForm.NumberOfFlocksPerYear;
            Paragraph paragraph2 = new Paragraph(str + nullable2.Value);
            document2.Add((IElement) paragraph2);
          }
          nullable2 = poultryWateringEvaporativeForm.BirdsPerFlock;
          if (nullable2.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Birds per Flock: ";
            nullable2 = poultryWateringEvaporativeForm.BirdsPerFlock;
            Paragraph paragraph2 = new Paragraph(str + nullable2.Value);
            document2.Add((IElement) paragraph2);
          }
          document1.Add((IElement) new Paragraph(" No. of days per year foggers used: " + (object) poultryWateringEvaporativeForm.NumberOfDaysPerYearFoggersUsed));
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      if (summary.AppWizard.WaterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms != null && summary.AppWizard.WaterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms.Count > 0)
      {
        document1.Add((IElement) new Paragraph("Poultry Fogger", font));
        foreach (PoultryWateringForm poultryWateringForm in summary.AppWizard.WaterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms)
        {
          PoultryWateringForm poultryWateringFoggerForm = poultryWateringForm;
          int? nullable2 = poultryWateringFoggerForm.PoultryId;
          if (nullable2.HasValue)
            document1.Add((IElement) new Paragraph(" Poultry Type: " + poultryWateringFoggerForm.PoultryTypeList.Single<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == poultryWateringFoggerForm.PoultryId.Value.ToString())).Text));
          nullable2 = poultryWateringFoggerForm.NumberOfHouses;
          if (nullable2.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Houses: ";
            nullable2 = poultryWateringFoggerForm.NumberOfHouses;
            Paragraph paragraph2 = new Paragraph(str + nullable2.Value);
            document2.Add((IElement) paragraph2);
          }
          nullable2 = poultryWateringFoggerForm.NumberOfFlocksPerYear;
          if (nullable2.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Flocks per Year: ";
            nullable2 = poultryWateringFoggerForm.NumberOfFlocksPerYear;
            Paragraph paragraph2 = new Paragraph(str + nullable2.Value);
            document2.Add((IElement) paragraph2);
          }
          nullable2 = poultryWateringFoggerForm.BirdsPerFlock;
          if (nullable2.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Birds per Flock: ";
            nullable2 = poultryWateringFoggerForm.BirdsPerFlock;
            Paragraph paragraph2 = new Paragraph(str + nullable2.Value);
            document2.Add((IElement) paragraph2);
          }
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      if (summary.AppWizard.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms != null && summary.AppWizard.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms.Count > 0)
      {
        document1.Add((IElement) new Paragraph("Dairy Animal Watering", font));
        foreach (LivestockForm livestockForm in summary.AppWizard.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms)
        {
          if (!string.IsNullOrEmpty(livestockForm.Livestock))
            document1.Add((IElement) new Paragraph(" Type of Livestock: " + livestockForm.Livestock));
          int? numberOfLivestock = livestockForm.NumberOfLivestock;
          if (numberOfLivestock.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Animals: ";
            numberOfLivestock = livestockForm.NumberOfLivestock;
            Paragraph paragraph2 = new Paragraph(str + numberOfLivestock.Value);
            document2.Add((IElement) paragraph2);
          }
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      if (summary.AppWizard.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms != null && summary.AppWizard.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms.Count > 0)
      {
        document1.Add((IElement) new Paragraph("Other Livestock Watering", font));
        foreach (LivestockForm livestockForm in summary.AppWizard.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms)
        {
          if (!string.IsNullOrEmpty(livestockForm.Livestock))
            document1.Add((IElement) new Paragraph(" Type of Livestock: " + livestockForm.Livestock));
          int? numberOfLivestock = livestockForm.NumberOfLivestock;
          if (numberOfLivestock.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Animals: ";
            numberOfLivestock = livestockForm.NumberOfLivestock;
            Paragraph paragraph2 = new Paragraph(str + numberOfLivestock.Value);
            document2.Add((IElement) paragraph2);
          }
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Farm Potable Supplies", font));
        document1.Add((IElement) new Paragraph(" No. of employees: " + summary.AppWizard.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm.NoOfEmployees.ToString()));
        document1.Add((IElement) new Paragraph(" "));
      }
      Decimal? nullable3 = summary.AppWizard.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres;
      if (nullable3.HasValue)
      {
        nullable3 = summary.AppWizard.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres;
        if (nullable3.Value > new Decimal(0))
        {
          document1.Add((IElement) new Paragraph("Crop Irrigation", font));
          iTextSharp.text.Document document2 = document1;
          string str1 = " Total Irrigated Acres: ";
          nullable3 = summary.AppWizard.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres;
          string str2 = nullable3.Value.ToString();
          Paragraph paragraph2 = new Paragraph(str1 + str2);
          document2.Add((IElement) paragraph2);
          document1.Add((IElement) new Paragraph(" "));
          if (summary.AppWizard.WaterUseDetailForm.CropUseForm.CropForms != null && summary.AppWizard.WaterUseDetailForm.CropUseForm.CropForms.Count > 0)
          {
            foreach (CropForm cropForm1 in summary.AppWizard.WaterUseDetailForm.CropUseForm.CropForms)
            {
              CropForm cropForm = cropForm1;
              int? nullable2 = cropForm.CropId;
              if (nullable2.HasValue && cropForm.CropTypeList != null)
                document1.Add((IElement) new Paragraph(" Type of Crop: " + cropForm.CropTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == cropForm.CropId.Value.ToString())).Text));
              nullable3 = cropForm.IrrigatedAcres;
              if (nullable3.HasValue)
              {
                iTextSharp.text.Document document3 = document1;
                string str3 = " No. of Irrigated Acres: ";
                nullable3 = cropForm.IrrigatedAcres;
                Paragraph paragraph3 = new Paragraph(str3 + nullable3.Value);
                document3.Add((IElement) paragraph3);
              }
              nullable2 = cropForm.IrrigationSystemTypeId;
              if (nullable2.HasValue && cropForm.IrrigationSystemTypeList != null)
              {
                SelectListItem selectListItem = cropForm.IrrigationSystemTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Text == "Other"));
                int num = -1;
                if (selectListItem != null)
                  num = Convert.ToInt32(selectListItem.Value);
                nullable2 = cropForm.IrrigationSystemTypeId;
                if (nullable2.Value == num)
                  document1.Add((IElement) new Paragraph(" Irrigation System Used: " + cropForm.IrrigationSystemDescription));
                else
                  document1.Add((IElement) new Paragraph(" Irrigation System Used: " + cropForm.IrrigationSystemTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == cropForm.IrrigationSystemTypeId.Value.ToString())).Text));
              }
              nullable2 = cropForm.CropYieldUnitId;
              if (nullable2.HasValue && cropForm.CropYieldUnitList != null)
                document1.Add((IElement) new Paragraph(" Crop Yield Goal: " + (object) cropForm.CropYieldGoal + " " + cropForm.CropYieldUnitList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == cropForm.CropYieldUnitId.Value.ToString())).Text));
              document1.Add((IElement) new Paragraph(" "));
            }
          }
        }
      }
      if (summary.AppWizard.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms != null && summary.AppWizard.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms.Count > 0)
      {
        document1.Add((IElement) new Paragraph("Nursery Irrigation", font));
        foreach (PermitWwIrrigationForm irrigationForm in summary.AppWizard.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms)
        {
          PermitWwIrrigationForm nurseryStockItem = irrigationForm;
          if (!string.IsNullOrEmpty(nurseryStockItem.PurposeDescription))
            document1.Add((IElement) new Paragraph(" Describe What is Being Irrigated: " + nurseryStockItem.PurposeDescription));
          nullable3 = nurseryStockItem.IrrigatedAcres;
          if (nullable3.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Irrigated Acres: ";
            nullable3 = nurseryStockItem.IrrigatedAcres;
            Paragraph paragraph2 = new Paragraph(str + nullable3.Value);
            document2.Add((IElement) paragraph2);
          }
          if (nurseryStockItem.IrrigationSystemTypeId.HasValue && nurseryStockItem.IrrigationSystemTypeList != null)
          {
            SelectListItem selectListItem = nurseryStockItem.IrrigationSystemTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Text == "Other"));
            int num = -1;
            if (selectListItem != null)
              num = Convert.ToInt32(selectListItem.Value);
            if (nurseryStockItem.IrrigationSystemTypeId.Value == num)
              document1.Add((IElement) new Paragraph(" Irrigation System Used: " + nurseryStockItem.IrrigationSystemDescription));
            else
              document1.Add((IElement) new Paragraph(" Irrigation System Used: " + nurseryStockItem.IrrigationSystemTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == nurseryStockItem.IrrigationSystemTypeId.Value.ToString())).Text));
          }
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      if (summary.AppWizard.WaterUseDetailForm.SodIrrigationForm.IrrigationForms != null && summary.AppWizard.WaterUseDetailForm.SodIrrigationForm.IrrigationForms.Count > 0)
      {
        document1.Add((IElement) new Paragraph("Sod Farm Irrigation", font));
        foreach (PermitWwIrrigationForm irrigationForm in summary.AppWizard.WaterUseDetailForm.SodIrrigationForm.IrrigationForms)
        {
          PermitWwIrrigationForm sodItem = irrigationForm;
          if (!string.IsNullOrEmpty(sodItem.PurposeDescription))
            document1.Add((IElement) new Paragraph(" Describe What is Being Irrigated: " + sodItem.PurposeDescription));
          nullable3 = sodItem.IrrigatedAcres;
          if (nullable3.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str = " No. of Irrigated Acres: ";
            nullable3 = sodItem.IrrigatedAcres;
            Paragraph paragraph2 = new Paragraph(str + nullable3.Value);
            document2.Add((IElement) paragraph2);
          }
          if (sodItem.IrrigationSystemTypeId.HasValue && sodItem.IrrigationSystemTypeList != null)
          {
            SelectListItem selectListItem = sodItem.IrrigationSystemTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Text == "Other"));
            int num = -1;
            if (selectListItem != null)
              num = Convert.ToInt32(selectListItem.Value);
            if (sodItem.IrrigationSystemTypeId.Value == num)
              document1.Add((IElement) new Paragraph(" Irrigation System Used: " + sodItem.IrrigationSystemDescription));
            else
              document1.Add((IElement) new Paragraph(" Irrigation System Used: " + sodItem.IrrigationSystemTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == sodItem.IrrigationSystemTypeId.Value.ToString())).Text));
          }
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Food Processing (Onsite)", font));
        document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.AverageGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.MaximumGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.MaximumGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Aquaculture/Aquariums", font));
        document1.Add((IElement) new Paragraph(" Is the water recycled?: " + (summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.IsRecycledWater ? "Yes" : "No")));
        document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.AverageGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.MaximumGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.MaximumGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm != null && summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Irrigation (Undefined)", font));
        document1.Add((IElement) new Paragraph(" Describe What is Being Irrigated: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.PurposeDescription));
        document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.AverageGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + (object) summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.MaximumGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + (object) summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      int? nullable4;
      if (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm != null && summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Golf Course Irrigation", font));
        nullable4 = summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.GolfIrrigationSystemTypeId;
        if (nullable4.HasValue)
          document1.Add((IElement) new Paragraph(" Irrigation system: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.GolfIrrigationSystemTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.GolfIrrigationSystemTypeId.Value.ToString())).Text));
        nullable4 = summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IrrigationLayoutTypeId;
        if (nullable4.HasValue)
          document1.Add((IElement) new Paragraph(" Irrigation layout: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IrrigationLayoutTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IrrigationLayoutTypeId.Value.ToString())).Text));
        document1.Add((IElement) new Paragraph(" Is well water pumped into pond: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IsWellwaterPumpedIntoPond ? "Yes" : "No")));
        iTextSharp.text.Document document2 = document1;
        string str1 = " No. of irrigated acres – tees/greens: ";
        nullable3 = summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_teesOrGreens;
        string str2;
        if (!nullable3.HasValue)
        {
          str2 = "";
        }
        else
        {
          nullable3 = summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_teesOrGreens;
          str2 = nullable3.Value.ToString();
        }
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
        iTextSharp.text.Document document3 = document1;
        string str3 = " No. of irrigated acres – fairways: ";
        nullable3 = summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_fairways;
        string str4;
        if (!nullable3.HasValue)
        {
          str4 = "";
        }
        else
        {
          nullable3 = summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_fairways;
          str4 = nullable3.Value.ToString();
        }
        Paragraph paragraph3 = new Paragraph(str3 + str4);
        document3.Add((IElement) paragraph3);
        document1.Add((IElement) new Paragraph(" Type of grass – tees/greens: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_tees_greens == null ? summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_tees_greens : "")));
        document1.Add((IElement) new Paragraph(" Type of grass – fairways: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_fairways == null ? summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_fairways : "")));
        document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.AverageGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + (object) summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.MaximumGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + (object) summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm != null && summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Lawn and Park Irrigation", font));
        nullable3 = summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfIrrigatedAcres;
        if (nullable3.HasValue)
        {
          iTextSharp.text.Document document2 = document1;
          string str = " No of irrigated acres: ";
          nullable3 = summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfIrrigatedAcres;
          Paragraph paragraph2 = new Paragraph(str + nullable3.Value);
          document2.Add((IElement) paragraph2);
        }
        nullable4 = summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfDaysPerWeekIrrigated;
        if (nullable4.HasValue)
        {
          iTextSharp.text.Document document2 = document1;
          string str = " No of days per week irrigated : ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfDaysPerWeekIrrigated;
          Paragraph paragraph2 = new Paragraph(str + nullable4.Value);
          document2.Add((IElement) paragraph2);
        }
        document1.Add((IElement) new Paragraph(" Describe What is Being Irrigated: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.PurposeDescription));
        document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.AverageGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + (object) summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.MaximumGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + (object) summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm != null && summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Small Intermitent Irrigation", font));
        nullable3 = summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfIrrigatedAcres;
        if (nullable3.HasValue)
        {
          iTextSharp.text.Document document2 = document1;
          string str = " No of irrigated acres: ";
          nullable3 = summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfIrrigatedAcres;
          Paragraph paragraph2 = new Paragraph(str + nullable3.Value);
          document2.Add((IElement) paragraph2);
        }
        nullable4 = summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfDaysPerWeekIrrigated;
        if (nullable4.HasValue)
        {
          iTextSharp.text.Document document2 = document1;
          string str = " No of days per week irrigated : ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfDaysPerWeekIrrigated;
          Paragraph paragraph2 = new Paragraph(str + nullable4.Value);
          document2.Add((IElement) paragraph2);
        }
        document1.Add((IElement) new Paragraph(" Describe What is Being Irrigated: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.PurposeDescription));
        document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.AverageGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + (object) summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.MaximumGallonPerDayFromGroundWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + (object) summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.AverageGallonPerDayFromSurfaceWater));
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Commercial drinking/sanitary", font));
        int? optionId = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.TypeOfFacilityId;
        document1.Add((IElement) new Paragraph(" Type of facility: " + summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.FacilityList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == optionId.ToString())).Text));
        iTextSharp.text.Document document2 = document1;
        string str1 = " Area (Sq footage) : ";
        nullable3 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AreaOrSquareFootage;
        string str2;
        if (!nullable3.HasValue)
        {
          str2 = "";
        }
        else
        {
          nullable3 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AreaOrSquareFootage;
          str2 = nullable3.ToString();
        }
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
        iTextSharp.text.Document document3 = document1;
        string str3 = " No of employees : ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.NoOfEmployees;
        string str4;
        if (!nullable4.HasValue)
        {
          str4 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.NoOfEmployees;
          str4 = nullable4.ToString();
        }
        Paragraph paragraph3 = new Paragraph(str3 + str4);
        document3.Add((IElement) paragraph3);
        iTextSharp.text.Document document4 = document1;
        string str5 = " Avg number of customers per day : ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AvgNumberOfCustomersPerDay;
        string str6;
        if (!nullable4.HasValue)
        {
          str6 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AvgNumberOfCustomersPerDay;
          str6 = nullable4.ToString();
        }
        Paragraph paragraph4 = new Paragraph(str5 + str6);
        document4.Add((IElement) paragraph4);
        nullable4 = optionId;
        if ((nullable4.GetValueOrDefault() != 1 ? 0 : (nullable4.HasValue ? 1 : 0)) != 0)
        {
          int? restaurantTypeId = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.RestaurantTypeId;
          document1.Add((IElement) new Paragraph(" Restaurant Type: " + summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.RestaurantTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == optionId.ToString())).Text));
          iTextSharp.text.Document document5 = document1;
          string str7 = " No. of seats: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.NoOfSeats;
          string str8;
          if (!nullable4.HasValue)
          {
            str8 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.NoOfSeats;
            str8 = nullable4.ToString();
          }
          Paragraph paragraph5 = new Paragraph(str7 + str8);
          document5.Add((IElement) paragraph5);
        }
        else
        {
          nullable4 = optionId;
          if ((nullable4.GetValueOrDefault() != 5 ? 0 : (nullable4.HasValue ? 1 : 0)) != 0)
          {
            iTextSharp.text.Document document5 = document1;
            string str7 = " No. of slips: ";
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.NoOfSlips;
            string str8;
            if (!nullable4.HasValue)
            {
              str8 = "";
            }
            else
            {
              nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.NoOfSlips;
              str8 = nullable4.ToString();
            }
            Paragraph paragraph5 = new Paragraph(str7 + str8);
            document5.Add((IElement) paragraph5);
          }
        }
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AverageGallonPerDayFromGroundWater;
        if (nullable4.HasValue)
        {
          iTextSharp.text.Document document5 = document1;
          string str7 = " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AverageGallonPerDayFromGroundWater;
          string str8;
          if (!nullable4.HasValue)
          {
            str8 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AverageGallonPerDayFromGroundWater;
            str8 = nullable4.ToString();
          }
          Paragraph paragraph5 = new Paragraph(str7 + str8);
          document5.Add((IElement) paragraph5);
        }
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.MaximumGallonPerDayFromGroundWater;
        if (nullable4.HasValue)
        {
          iTextSharp.text.Document document5 = document1;
          string str7 = " For Groundwater, Gallons per day to be withdrawn during month of maximum use : ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.MaximumGallonPerDayFromGroundWater;
          string str8;
          if (!nullable4.HasValue)
          {
            str8 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.MaximumGallonPerDayFromGroundWater;
            str8 = nullable4.ToString();
          }
          Paragraph paragraph5 = new Paragraph(str7 + str8);
          document5.Add((IElement) paragraph5);
        }
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AverageGallonPerDayFromSurfaceWater;
        if (nullable4.HasValue)
        {
          iTextSharp.text.Document document5 = document1;
          string str7 = " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AverageGallonPerDayFromSurfaceWater;
          string str8;
          if (!nullable4.HasValue)
          {
            str8 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.AverageGallonPerDayFromSurfaceWater;
            str8 = nullable4.ToString();
          }
          Paragraph paragraph5 = new Paragraph(str7 + str8);
          document5.Add((IElement) paragraph5);
        }
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.MaximumGallonPerDayFromSurfaceWater;
        if (nullable4.HasValue)
        {
          iTextSharp.text.Document document5 = document1;
          string str7 = " For Surface Water, Maximum gallons per day to be withdrawn : ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.MaximumGallonPerDayFromSurfaceWater;
          string str8;
          if (!nullable4.HasValue)
          {
            str8 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.MaximumGallonPerDayFromSurfaceWater;
            str8 = nullable4.ToString();
          }
          Paragraph paragraph5 = new Paragraph(str7 + str8);
          document5.Add((IElement) paragraph5);
        }
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Institutional Educational (drinking/sanitary)", font));
        int? facilityoptionId = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.EducationInstituteTypeId;
        document1.Add((IElement) new Paragraph(" Type of facility: " + summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.EducationInstituteTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == facilityoptionId.ToString())).Text));
        nullable4 = facilityoptionId;
        if ((nullable4.GetValueOrDefault() != 1 ? 0 : (nullable4.HasValue ? 1 : 0)) != 0)
        {
          iTextSharp.text.Document document2 = document1;
          string str1 = " No. of staff: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStaff;
          string str2;
          if (!nullable4.HasValue)
          {
            str2 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStaff;
            str2 = nullable4.ToString();
          }
          Paragraph paragraph2 = new Paragraph(str1 + str2);
          document2.Add((IElement) paragraph2);
          iTextSharp.text.Document document3 = document1;
          string str3 = " No. of students: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStudents;
          string str4;
          if (!nullable4.HasValue)
          {
            str4 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStudents;
            str4 = nullable4.ToString();
          }
          Paragraph paragraph3 = new Paragraph(str3 + str4);
          document3.Add((IElement) paragraph3);
          document1.Add((IElement) new Paragraph(" Athletic field irrigation: " + (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.IsAthleticFieldIrrigation ? "Yes" : "No")));
          if (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.IsAthleticFieldIrrigation)
          {
            iTextSharp.text.Document document4 = document1;
            string str5 = " No. of irrigated acres: ";
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfIrrigatedAcres;
            string str6;
            if (!nullable4.HasValue)
            {
              str6 = "";
            }
            else
            {
              nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfIrrigatedAcres;
              str6 = nullable4.ToString();
            }
            Paragraph paragraph4 = new Paragraph(str5 + str6);
            document4.Add((IElement) paragraph4);
          }
          document1.Add((IElement) new Paragraph(" Chillers: " + (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.Chillers ? "Yes" : "No")));
        }
        else
        {
          nullable4 = facilityoptionId;
          if ((nullable4.GetValueOrDefault() != 2 ? 0 : (nullable4.HasValue ? 1 : 0)) != 0)
          {
            iTextSharp.text.Document document2 = document1;
            string str1 = " No. staff working more than 20 hrs per week: ";
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStaffWorkingMorethan20hrsPerWeek;
            string str2;
            if (!nullable4.HasValue)
            {
              str2 = "";
            }
            else
            {
              nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStaffWorkingMorethan20hrsPerWeek;
              str2 = nullable4.ToString();
            }
            Paragraph paragraph2 = new Paragraph(str1 + str2);
            document2.Add((IElement) paragraph2);
            iTextSharp.text.Document document3 = document1;
            string str3 = " No. staff working less than 20 hrs per week: ";
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStaffWorkingLessthan20hrsPerWeek;
            string str4;
            if (!nullable4.HasValue)
            {
              str4 = "";
            }
            else
            {
              nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStaffWorkingLessthan20hrsPerWeek;
              str4 = nullable4.ToString();
            }
            Paragraph paragraph3 = new Paragraph(str3 + str4);
            document3.Add((IElement) paragraph3);
            iTextSharp.text.Document document4 = document1;
            string str5 = " No. students attending more than 20 hrs per week: ";
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStudentsAttendingMorethan20hrsPerWeek;
            string str6;
            if (!nullable4.HasValue)
            {
              str6 = "";
            }
            else
            {
              nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStudentsAttendingMorethan20hrsPerWeek;
              str6 = nullable4.ToString();
            }
            Paragraph paragraph4 = new Paragraph(str5 + str6);
            document4.Add((IElement) paragraph4);
            iTextSharp.text.Document document5 = document1;
            string str7 = " No. students attending less than 20 hrs per week: ";
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStudentsAttendingLessthan20hrsPerWeek;
            string str8;
            if (!nullable4.HasValue)
            {
              str8 = "";
            }
            else
            {
              nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.NoOfStudentsAttendingLessthan20hrsPerWeek;
              str8 = nullable4.ToString();
            }
            Paragraph paragraph5 = new Paragraph(str7 + str8);
            document5.Add((IElement) paragraph5);
          }
        }
        document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.AverageGallonPerDayFromGroundWater != null ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.AverageGallonPerDayFromGroundWater.ToString() : "")));
        document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.MaximumGallonPerDayFromGroundWater != null ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.MaximumGallonPerDayFromGroundWater.ToString() : "")));
        document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.AverageGallonPerDayFromSurfaceWater != null ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.AverageGallonPerDayFromSurfaceWater.ToString() : "")));
        document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.MaximumGallonPerDayFromSurfaceWater != null ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.MaximumGallonPerDayFromSurfaceWater.ToString() : "")));
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Institutional religious (drinking/sanitary)", font));
        iTextSharp.text.Document document2 = document1;
        string str1 = " No. of parishioners: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfParishioners;
        string str2;
        if (!nullable4.HasValue)
        {
          str2 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfParishioners;
          str2 = nullable4.ToString();
        }
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
        iTextSharp.text.Document document3 = document1;
        string str3 = " No. of services per week: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfServicesPerWeek;
        string str4;
        if (!nullable4.HasValue)
        {
          str4 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfServicesPerWeek;
          str4 = nullable4.ToString();
        }
        Paragraph paragraph3 = new Paragraph(str3 + str4);
        document3.Add((IElement) paragraph3);
        document1.Add((IElement) new Paragraph(" Athletic field irrigation: " + (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.Daycare ? "Yes" : "No")));
        if (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.Daycare)
        {
          iTextSharp.text.Document document4 = document1;
          string str5 = " No. staff working more than 20 hrs per week: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStaffWorkingMorethan20hrsPerWeek;
          string str6;
          if (!nullable4.HasValue)
          {
            str6 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStaffWorkingMorethan20hrsPerWeek;
            str6 = nullable4.ToString();
          }
          Paragraph paragraph4 = new Paragraph(str5 + str6);
          document4.Add((IElement) paragraph4);
          iTextSharp.text.Document document5 = document1;
          string str7 = " No. staff working less than 20 hrs per week: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStaffWorkingLessthan20hrsPerWeek;
          string str8;
          if (!nullable4.HasValue)
          {
            str8 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStaffWorkingLessthan20hrsPerWeek;
            str8 = nullable4.ToString();
          }
          Paragraph paragraph5 = new Paragraph(str7 + str8);
          document5.Add((IElement) paragraph5);
          iTextSharp.text.Document document6 = document1;
          string str9 = " No. students attending more than 20 hrs per week: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStudentsAttendingMorethan20hrsPerWeek;
          string str10;
          if (!nullable4.HasValue)
          {
            str10 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStudentsAttendingMorethan20hrsPerWeek;
            str10 = nullable4.ToString();
          }
          Paragraph paragraph6 = new Paragraph(str9 + str10);
          document6.Add((IElement) paragraph6);
          iTextSharp.text.Document document7 = document1;
          string str11 = " No. students attending less than 20 hrs per week: ";
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStudentsAttendingLessthan20hrsPerWeek;
          string str12;
          if (!nullable4.HasValue)
          {
            str12 = "";
          }
          else
          {
            nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.NoOfStudentsAttendingLessthan20hrsPerWeek;
            str12 = nullable4.ToString();
          }
          Paragraph paragraph7 = new Paragraph(str11 + str12);
          document7.Add((IElement) paragraph7);
        }
        iTextSharp.text.Document document8 = document1;
        string str13 = " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.AverageGallonPerDayFromGroundWater;
        string str14;
        if (!nullable4.HasValue)
        {
          str14 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.AverageGallonPerDayFromGroundWater;
          str14 = nullable4.ToString();
        }
        Paragraph paragraph8 = new Paragraph(str13 + str14);
        document8.Add((IElement) paragraph8);
        iTextSharp.text.Document document9 = document1;
        string str15 = " For Groundwater, Gallons per day to be withdrawn during month of maximum use: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.MaximumGallonPerDayFromGroundWater;
        string str16;
        if (!nullable4.HasValue)
        {
          str16 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.MaximumGallonPerDayFromGroundWater;
          str16 = nullable4.ToString();
        }
        Paragraph paragraph9 = new Paragraph(str15 + str16);
        document9.Add((IElement) paragraph9);
        iTextSharp.text.Document document10 = document1;
        string str17 = " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.AverageGallonPerDayFromSurfaceWater;
        string str18;
        if (!nullable4.HasValue)
        {
          str18 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.AverageGallonPerDayFromSurfaceWater;
          str18 = nullable4.ToString();
        }
        Paragraph paragraph10 = new Paragraph(str17 + str18);
        document10.Add((IElement) paragraph10);
        iTextSharp.text.Document document11 = document1;
        string str19 = " For Surface Water, Maximum gallons per day to be withdrawn: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.MaximumGallonPerDayFromSurfaceWater;
        string str20;
        if (!nullable4.HasValue)
        {
          str20 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.MaximumGallonPerDayFromSurfaceWater;
          str20 = nullable4.ToString();
        }
        Paragraph paragraph11 = new Paragraph(str19 + str20);
        document11.Add((IElement) paragraph11);
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Wildlife Ponds", font));
        iTextSharp.text.Document document2 = document1;
        string str1 = " No. of ponds: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.NoOfPonds;
        string str2;
        if (!nullable4.HasValue)
        {
          str2 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.NoOfPonds;
          str2 = nullable4.ToString();
        }
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
        iTextSharp.text.Document document3 = document1;
        string str3 = " Area of pond(s) (in acres): ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.AreaOfPonds_inAcres;
        string str4;
        if (!nullable4.HasValue)
        {
          str4 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.AreaOfPonds_inAcres;
          str4 = nullable4.ToString();
        }
        Paragraph paragraph3 = new Paragraph(str3 + str4);
        document3.Add((IElement) paragraph3);
        iTextSharp.text.Document document4 = document1;
        string str5 = " Depth of pond(s) (in feet): ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.DepthOfPonds_inFeet;
        string str6;
        if (!nullable4.HasValue)
        {
          str6 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.DepthOfPonds_inFeet;
          str6 = nullable4.ToString();
        }
        Paragraph paragraph4 = new Paragraph(str5 + str6);
        document4.Add((IElement) paragraph4);
        iTextSharp.text.Document document5 = document1;
        string str7 = " No. of months per year that pond(s) contain water: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.NoOfMonthsPerYearPondContainWater;
        string str8;
        if (!nullable4.HasValue)
        {
          str8 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.NoOfMonthsPerYearPondContainWater;
          str8 = nullable4.ToString();
        }
        Paragraph paragraph5 = new Paragraph(str7 + str8);
        document5.Add((IElement) paragraph5);
        iTextSharp.text.Document document6 = document1;
        string str9 = " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.AverageGallonPerDayFromGroundWater;
        string str10;
        if (!nullable4.HasValue)
        {
          str10 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.AverageGallonPerDayFromGroundWater;
          str10 = nullable4.ToString();
        }
        Paragraph paragraph6 = new Paragraph(str9 + str10);
        document6.Add((IElement) paragraph6);
        iTextSharp.text.Document document7 = document1;
        string str11 = " For Groundwater, Gallons per day to be withdrawn during month of maximum use: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.MaximumGallonPerDayFromGroundWater;
        string str12;
        if (!nullable4.HasValue)
        {
          str12 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.MaximumGallonPerDayFromGroundWater;
          str12 = nullable4.ToString();
        }
        Paragraph paragraph7 = new Paragraph(str11 + str12);
        document7.Add((IElement) paragraph7);
        iTextSharp.text.Document document8 = document1;
        string str13 = " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.AverageGallonPerDayFromSurfaceWater;
        string str14;
        if (!nullable4.HasValue)
        {
          str14 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.AverageGallonPerDayFromSurfaceWater;
          str14 = nullable4.ToString();
        }
        Paragraph paragraph8 = new Paragraph(str13 + str14);
        document8.Add((IElement) paragraph8);
        iTextSharp.text.Document document9 = document1;
        string str15 = " For Surface Water, Maximum gallons per day to be withdrawn: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.MaximumGallonPerDayFromSurfaceWater;
        string str16;
        if (!nullable4.HasValue)
        {
          str16 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.MaximumGallonPerDayFromSurfaceWater;
          str16 = nullable4.ToString();
        }
        Paragraph paragraph9 = new Paragraph(str15 + str16);
        document9.Add((IElement) paragraph9);
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm != null && summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.Id != 0)
      {
        document1.Add((IElement) new Paragraph("Construction Dewatering", font));
        iTextSharp.text.Document document2 = document1;
        string str1 = " Area of excavation (in sq. ft): ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.AreaOfExcavation;
        string str2;
        if (!nullable4.HasValue)
        {
          str2 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.AreaOfExcavation;
          str2 = nullable4.ToString();
        }
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
        iTextSharp.text.Document document3 = document1;
        string str3 = " Depth of excavation (in feet): ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.DepthOfExcavation;
        string str4;
        if (!nullable4.HasValue)
        {
          str4 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.DepthOfExcavation;
          str4 = nullable4.ToString();
        }
        Paragraph paragraph3 = new Paragraph(str3 + str4);
        document3.Add((IElement) paragraph3);
        iTextSharp.text.Document document4 = document1;
        string str5 = " No. of pumps: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.NoOfPumps;
        string str6;
        if (!nullable4.HasValue)
        {
          str6 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.NoOfPumps;
          str6 = nullable4.ToString();
        }
        Paragraph paragraph4 = new Paragraph(str5 + str6);
        document4.Add((IElement) paragraph4);
        iTextSharp.text.Document document5 = document1;
        string str7 = " Capacity of pumps: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.CapacityOfPumps;
        string str8;
        if (!nullable4.HasValue)
        {
          str8 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.CapacityOfPumps;
          str8 = nullable4.ToString();
        }
        Paragraph paragraph5 = new Paragraph(str7 + str8);
        document5.Add((IElement) paragraph5);
        iTextSharp.text.Document document6 = document1;
        string str9 = " Hours of operation per day: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.HoursOfOperationPerDay;
        string str10;
        if (!nullable4.HasValue)
        {
          str10 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.HoursOfOperationPerDay;
          str10 = nullable4.ToString();
        }
        Paragraph paragraph6 = new Paragraph(str9 + str10);
        document6.Add((IElement) paragraph6);
        int? removalExcavationTypeId = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.WaterRemovalExcavationTypeId;
        document1.Add((IElement) new Paragraph(" How is water removed from excavation? " + summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.WaterRemovalExcavationTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == removalExcavationTypeId.ToString())).Text));
        iTextSharp.text.Document document7 = document1;
        string str11 = " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.AverageGallonPerDayFromGroundWater;
        string str12;
        if (!nullable4.HasValue)
        {
          str12 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.AverageGallonPerDayFromGroundWater;
          str12 = nullable4.ToString();
        }
        Paragraph paragraph7 = new Paragraph(str11 + str12);
        document7.Add((IElement) paragraph7);
        iTextSharp.text.Document document8 = document1;
        string str13 = " For Groundwater, Gallons per day to be withdrawn during month of maximum use: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.MaximumGallonPerDayFromGroundWater;
        string str14;
        if (!nullable4.HasValue)
        {
          str14 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.MaximumGallonPerDayFromGroundWater;
          str14 = nullable4.ToString();
        }
        Paragraph paragraph8 = new Paragraph(str13 + str14);
        document8.Add((IElement) paragraph8);
        iTextSharp.text.Document document9 = document1;
        string str15 = " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.AverageGallonPerDayFromSurfaceWater;
        string str16;
        if (!nullable4.HasValue)
        {
          str16 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.AverageGallonPerDayFromSurfaceWater;
          str16 = nullable4.ToString();
        }
        Paragraph paragraph9 = new Paragraph(str15 + str16);
        document9.Add((IElement) paragraph9);
        iTextSharp.text.Document document10 = document1;
        string str17 = " For Surface Water, Maximum gallons per day to be withdrawn: ";
        nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.MaximumGallonPerDayFromSurfaceWater;
        string str18;
        if (!nullable4.HasValue)
        {
          str18 = "";
        }
        else
        {
          nullable4 = summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.MaximumGallonPerDayFromSurfaceWater;
          str18 = nullable4.ToString();
        }
        Paragraph paragraph10 = new Paragraph(str17 + str18);
        document10.Add((IElement) paragraph10);
        document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.EstimateDescription));
        document1.Add((IElement) new Paragraph(" "));
      }
      if (summary.AppWizard.WaterUseDetailForm.PrivateWaterSuppilerForm.PrivateWaterSupplierForms != null)
      {
        foreach (PermitWwPrivateWaterSupplierForm waterSupplierForm in summary.AppWizard.WaterUseDetailForm.PrivateWaterSuppilerForm.PrivateWaterSupplierForms)
        {
          nullable4 = waterSupplierForm.AverageGallonPerDayFromGroundWater;
          if (!nullable4.HasValue)
          {
            nullable4 = waterSupplierForm.MaximumGallonPerDayFromGroundWater;
            if (!nullable4.HasValue)
            {
              nullable4 = waterSupplierForm.AverageGallonPerDayFromSurfaceWater;
              if (!nullable4.HasValue)
              {
                nullable4 = waterSupplierForm.MaximumGallonPerDayFromSurfaceWater;
                if (!nullable4.HasValue && string.IsNullOrEmpty(waterSupplierForm.EstimateDescription))
                  continue;
              }
            }
          }
          document1.Add((IElement) new Paragraph(waterSupplierForm.Title, font));
          if (waterSupplierForm.PermitWaterWithdrawalPurposeId == 16 || waterSupplierForm.PermitWaterWithdrawalPurposeId == 17 || (waterSupplierForm.PermitWaterWithdrawalPurposeId == 23 || waterSupplierForm.PermitWaterWithdrawalPurposeId == 24) || (waterSupplierForm.PermitWaterWithdrawalPurposeId == 45 || waterSupplierForm.PermitWaterWithdrawalPurposeId == 46))
          {
            iTextSharp.text.Document document2 = document1;
            string str1 = " No. of employees: ";
            nullable4 = waterSupplierForm.NoOfEmployees;
            string str2;
            if (!nullable4.HasValue)
            {
              str2 = "";
            }
            else
            {
              nullable4 = waterSupplierForm.NoOfEmployees;
              str2 = nullable4.ToString();
            }
            Paragraph paragraph2 = new Paragraph(str1 + str2);
            document2.Add((IElement) paragraph2);
          }
          if (waterSupplierForm.PermitWaterWithdrawalPurposeId == 46)
          {
            iTextSharp.text.Document document2 = document1;
            string str1 = " No. of other regular users per day: ";
            nullable4 = waterSupplierForm.NoOfRegularUsersPerDay;
            string str2;
            if (!nullable4.HasValue)
            {
              str2 = "";
            }
            else
            {
              nullable4 = waterSupplierForm.NoOfRegularUsersPerDay;
              str2 = nullable4.ToString();
            }
            Paragraph paragraph2 = new Paragraph(str1 + str2);
            document2.Add((IElement) paragraph2);
          }
          if (waterSupplierForm.PermitWaterWithdrawalPurposeId == 39 || waterSupplierForm.PermitWaterWithdrawalPurposeId == 54)
          {
            iTextSharp.text.Document document2 = document1;
            string str1 = " No. of lots/Units: ";
            nullable4 = waterSupplierForm.NoOfLotsOrUnits;
            string str2;
            if (!nullable4.HasValue)
            {
              str2 = "";
            }
            else
            {
              nullable4 = waterSupplierForm.NoOfLotsOrUnits;
              str2 = nullable4.ToString();
            }
            Paragraph paragraph2 = new Paragraph(str1 + str2);
            document2.Add((IElement) paragraph2);
          }
          if (waterSupplierForm.PermitWaterWithdrawalPurposeId == 39)
            document1.Add((IElement) new Paragraph(" Swimming pool? " + (waterSupplierForm.SwimmingPool ? "Yes" : "No")));
          document1.Add((IElement) new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) waterSupplierForm.AverageGallonPerDayFromGroundWater));
          document1.Add((IElement) new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + (object) waterSupplierForm.MaximumGallonPerDayFromGroundWater));
          document1.Add((IElement) new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + (object) waterSupplierForm.AverageGallonPerDayFromSurfaceWater));
          document1.Add((IElement) new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + (object) waterSupplierForm.AverageGallonPerDayFromSurfaceWater));
          document1.Add((IElement) new Paragraph(" Describe how estimate was derived: " + waterSupplierForm.EstimateDescription));
          document1.Add((IElement) new Paragraph(" "));
        }
      }
      if (summary.AppWizard != null && summary.AppWizard.WastewaterTreatmentDisposalForm != null)
      {
        foreach (WastewaterTreatmentAndDisposalForm treatmentAndDisposalForm in summary.AppWizard.WastewaterTreatmentDisposalForm)
        {
          if (treatmentAndDisposalForm != null && treatmentAndDisposalForm.Id > 0)
          {
            document1.Add((IElement) new Paragraph(treatmentAndDisposalForm.Title + " Water Treatment Disposal", font));
            document1.Add((IElement) new Paragraph(" Will this water withdrawal result in any wastewater discharge: " + (treatmentAndDisposalForm.IsWastewaterDischarge ? "Yes" : "No")));
            if (treatmentAndDisposalForm.IsWastewaterDischarge)
            {
              nullable4 = treatmentAndDisposalForm.WaterDispersementTypeId;
              if (nullable4.HasValue)
              {
                nullable4 = treatmentAndDisposalForm.WaterDispersementTypeId;
                string tempID = nullable4.ToString();
                document1.Add((IElement) new Paragraph(" Select the kind of wastewater treatment and disposal system that will be used: " + treatmentAndDisposalForm.WaterDispersementTypeList.FirstOrDefault<SelectListItem>((Func<SelectListItem, bool>) (x => x.Value == tempID)).Text));
              }
              if (!string.IsNullOrEmpty(treatmentAndDisposalForm.GroundwaterDischargeDiscription))
                document1.Add((IElement) new Paragraph(" Describe the nature of the groundwater discharge: " + treatmentAndDisposalForm.GroundwaterDischargeDiscription));
              if (!string.IsNullOrEmpty(treatmentAndDisposalForm.PermitDischargeNumber))
                document1.Add((IElement) new Paragraph(" Enter the permit discharge number: " + treatmentAndDisposalForm.PermitDischargeNumber));
            }
            document1.Add((IElement) paragraph1);
          }
        }
      }
      if (summary.PermitWithdrawalGroundwater != null)
      {
        document1.Add((IElement) new Paragraph("Groundwater Details", font));
        document1.Add((IElement) new Paragraph(" Average Gallon Per Day: " + (summary.PermitWithdrawalGroundwater.AvgGalPerDay == 0L ? "To Be Determined" : string.Format("{0:n0}", (object) summary.PermitWithdrawalGroundwater.AvgGalPerDay))));
        document1.Add((IElement) new Paragraph(" Maximum Gallon Per Day: " + (summary.PermitWithdrawalGroundwater.MaxGalPerDay == 0L ? "To Be Determined" : string.Format("{0:n0}", (object) summary.PermitWithdrawalGroundwater.MaxGalPerDay))));
        document1.Add((IElement) paragraph1);
        foreach (PermitWithdrawalGroundwaterDetail groundwaterDetail in (IEnumerable<PermitWithdrawalGroundwaterDetail>) summary.PermitWithdrawalGroundwater.PermitWithdrawalGroundwaterDetails)
        {
          PermitWithdrawalGroundwaterDetail item = groundwaterDetail;
          if (!string.IsNullOrEmpty(item.LU_WaterWithdrawalType.Description))
            document1.Add((IElement) new Paragraph(" Groundwater Type: " + item.LU_WaterWithdrawalType.Description));
          bool? isExisting = item.IsExisting;
          if (isExisting.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str1 = " Is this an Existing Well? ";
            isExisting = item.IsExisting;
            string str2;
            if (!isExisting.HasValue)
            {
              str2 = "";
            }
            else
            {
              isExisting = item.IsExisting;
              str2 = isExisting.Value ? "Yes" : "No";
            }
            Paragraph paragraph2 = new Paragraph(str1 + str2);
            document2.Add((IElement) paragraph2);
          }
          nullable3 = item.WellDepthFeet;
          if (nullable3.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str1 = " Well Depth (feet): ";
            nullable3 = item.WellDepthFeet;
            string str2;
            if (!nullable3.HasValue)
            {
              str2 = "";
            }
            else
            {
              nullable3 = item.WellDepthFeet;
              str2 = nullable3.ToString();
            }
            Paragraph paragraph2 = new Paragraph(str1 + str2);
            document2.Add((IElement) paragraph2);
          }
          nullable3 = item.WellDiameterInches;
          if (nullable3.HasValue)
          {
            iTextSharp.text.Document document2 = document1;
            string str1 = " Well Diameter (inches): ";
            nullable3 = item.WellDiameterInches;
            string str2;
            if (!nullable3.HasValue)
            {
              str2 = "";
            }
            else
            {
              nullable3 = item.WellDiameterInches;
              str2 = nullable3.Value.ToString();
            }
            Paragraph paragraph2 = new Paragraph(str1 + str2);
            document2.Add((IElement) paragraph2);
          }
          if (!string.IsNullOrEmpty(item.WellTagNumber))
            document1.Add((IElement) new Paragraph(" Well Tag No.: " + item.WellTagNumber));
          if (!string.IsNullOrEmpty(item.TaxMapNumber))
            document1.Add((IElement) new Paragraph(" Tax Map: " + item.TaxMapNumber));
          if (!string.IsNullOrEmpty(item.TaxMapBlockNumber))
            document1.Add((IElement) new Paragraph(" Grid: " + item.TaxMapBlockNumber));
          if (!string.IsNullOrEmpty(item.TaxMapParcel))
            document1.Add((IElement) new Paragraph(" Parcel: " + item.TaxMapParcel));
          if (!string.IsNullOrEmpty(item.TaxMapSubBlockNumber))
            document1.Add((IElement) new Paragraph(" Block: " + item.TaxMapSubBlockNumber));
          if (!string.IsNullOrEmpty(item.LotNumber))
            document1.Add((IElement) new Paragraph(" Lot: " + item.LotNumber));
          if (!string.IsNullOrEmpty(item.Street))
            document1.Add((IElement) new Paragraph(" Street: " + item.Street));
          if (!string.IsNullOrEmpty(item.City))
            document1.Add((IElement) new Paragraph(" City: " + item.City));
          if (!string.IsNullOrEmpty(item.State))
            document1.Add((IElement) new Paragraph(" State: " + item.State));
          if (!string.IsNullOrEmpty(item.Zip))
            document1.Add((IElement) new Paragraph(" Zip: " + item.Zip));
          if (item.LU_Watershed != null && !string.IsNullOrEmpty(item.LU_Watershed.Description))
            document1.Add((IElement) new Paragraph(" Watershed: " + item.LU_Watershed.Description));
          document1.Add((IElement) new Paragraph(" Comment: " + item.Comment));
          if (summary.Permit.WaterWithdrawalLocationPurposes.Any<WaterWithdrawalLocationPurpose>((Func<WaterWithdrawalLocationPurpose, bool>) (x => x.PermitWithdrawalId == item.Id)))
          {
            string str = string.Join(", ", summary.Permit.WaterWithdrawalLocationPurposes.Where<WaterWithdrawalLocationPurpose>((Func<WaterWithdrawalLocationPurpose, bool>) (x => x.PermitWithdrawalId == item.Id)).Select<WaterWithdrawalLocationPurpose, string>((Func<WaterWithdrawalLocationPurpose, string>) (x => x.PermitWaterWithdrawalPurpose.LU_WaterWithdrawalPurpose.Description)));
            document1.Add((IElement) new Paragraph(" Associated Withdrawal Purposes: " + str));
          }
          document1.Add((IElement) paragraph1);
        }
      }
      if (summary.PermitWithdrawalSurfacewater != null)
      {
        document1.Add((IElement) new Paragraph("Surface Water Details", font));
        document1.Add((IElement) new Paragraph(" Average Gallon Per Day: " + (summary.PermitWithdrawalSurfacewater.AvgGalPerDay == 0L ? "To Be Determined" : string.Format("{0:n0}", (object) summary.PermitWithdrawalSurfacewater.AvgGalPerDay))));
        document1.Add((IElement) new Paragraph(" Maximum Gallon Per Day: " + (summary.PermitWithdrawalSurfacewater.MaxGalPerDay == 0L ? "To Be Determined" : string.Format("{0:n0}", (object) summary.PermitWithdrawalSurfacewater.MaxGalPerDay))));
        document1.Add((IElement) paragraph1);
        foreach (PermitWithdrawalSurfacewaterDetail surfacewaterDetail in (IEnumerable<PermitWithdrawalSurfacewaterDetail>) summary.PermitWithdrawalSurfacewater.PermitWithdrawalSurfacewaterDetails)
        {
          PermitWithdrawalSurfacewaterDetail item = surfacewaterDetail;
          if (!string.IsNullOrEmpty(item.LU_WaterWithdrawalType.Description))
            document1.Add((IElement) new Paragraph(" Surface Water Type:" + item.LU_WaterWithdrawalType.Description));
          if (!string.IsNullOrEmpty(item.ExactLocationOfIntake))
            document1.Add((IElement) new Paragraph(" Exact Location Of Intake:" + item.ExactLocationOfIntake));
          if (!string.IsNullOrEmpty(item.LU_StateStream.Description))
            document1.Add((IElement) new Paragraph(" Water Source Name:" + (item.LU_StateStream != null ? item.LU_StateStream.Description + " | " + item.LU_StateStream.Key : "")));
          if (!string.IsNullOrEmpty(item.TaxMapNumber))
            document1.Add((IElement) new Paragraph(" Tax Map: " + item.TaxMapNumber));
          if (!string.IsNullOrEmpty(item.TaxMapBlockNumber))
            document1.Add((IElement) new Paragraph(" Grid: " + item.TaxMapBlockNumber));
          if (!string.IsNullOrEmpty(item.TaxMapParcel))
            document1.Add((IElement) new Paragraph(" Parcel: " + item.TaxMapParcel));
          if (!string.IsNullOrEmpty(item.TaxMapSubBlockNumber))
            document1.Add((IElement) new Paragraph(" Block: " + item.TaxMapSubBlockNumber));
          if (!string.IsNullOrEmpty(item.LotNumber))
            document1.Add((IElement) new Paragraph(" Lot: " + item.LotNumber));
          if (!string.IsNullOrEmpty(item.Street))
            document1.Add((IElement) new Paragraph(" Street: " + item.Street));
          if (!string.IsNullOrEmpty(item.City))
            document1.Add((IElement) new Paragraph(" City: " + item.City));
          if (!string.IsNullOrEmpty(item.State))
            document1.Add((IElement) new Paragraph(" State: " + item.State));
          if (!string.IsNullOrEmpty(item.Zip))
            document1.Add((IElement) new Paragraph(" Zip: " + item.Zip));
          if (item.LU_Watershed != null && !string.IsNullOrEmpty(item.LU_Watershed.Description))
            document1.Add((IElement) new Paragraph(" Watershed: " + item.LU_Watershed.Description));
          document1.Add((IElement) new Paragraph(" Comment: " + item.Comment));
          if (summary.Permit.WaterWithdrawalLocationPurposes.Any<WaterWithdrawalLocationPurpose>((Func<WaterWithdrawalLocationPurpose, bool>) (x => x.PermitWithdrawalId == item.Id)))
          {
            string str = string.Join(", ", summary.Permit.WaterWithdrawalLocationPurposes.Where<WaterWithdrawalLocationPurpose>((Func<WaterWithdrawalLocationPurpose, bool>) (x => x.PermitWithdrawalId == item.Id)).Select<WaterWithdrawalLocationPurpose, string>((Func<WaterWithdrawalLocationPurpose, string>) (x => x.PermitWaterWithdrawalPurpose.LU_WaterWithdrawalPurpose.Description)));
            document1.Add((IElement) new Paragraph(" Associated Withdrawal Purposes: " + str));
          }
          document1.Add((IElement) paragraph1);
        }
      }
      bool? nullable5 = summary.Permit.IsInWMStrategyArea;
      if (!nullable5.HasValue)
      {
        nullable5 = summary.Permit.IsInTier2Watershed;
        if (!nullable5.HasValue)
        {
          nullable4 = summary.Permit.AquiferId;
          if (!nullable4.HasValue)
          {
            nullable5 = summary.Permit.AquiferTest;
            if (!nullable5.HasValue && string.IsNullOrEmpty(summary.Permit.SourceDescription) && string.IsNullOrEmpty(summary.Permit.Location))
              goto label_496;
          }
        }
      }
      document1.Add((IElement) new Paragraph("Location Information", font));
      nullable5 = summary.Permit.IsInWMStrategyArea;
      if (nullable5.HasValue)
      {
        iTextSharp.text.Document document2 = document1;
        string str1 = " Water Management Strategy Area: ";
        nullable5 = summary.Permit.IsInWMStrategyArea;
        string str2 = nullable5.Value ? "Yes" : "No";
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
      }
      nullable5 = summary.Permit.IsInTier2Watershed;
      if (nullable5.HasValue)
      {
        iTextSharp.text.Document document2 = document1;
        string str1 = " Tier II Watershed: ";
        nullable5 = summary.Permit.IsInTier2Watershed;
        string str2 = nullable5.Value ? "Yes" : "No";
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
      }
      nullable4 = summary.Permit.AquiferId;
      if (nullable4.HasValue && summary.Permit.LU_Aquifer != null)
        document1.Add((IElement) new Paragraph(" Aquifer: " + summary.Permit.LU_Aquifer.Description + " | " + summary.Permit.LU_Aquifer.Key));
      nullable5 = summary.Permit.AquiferTest;
      if (nullable5.HasValue)
      {
        iTextSharp.text.Document document2 = document1;
        string str1 = " Aquifer Test Required: ";
        nullable5 = summary.Permit.AquiferTest;
        string str2 = nullable5.Value ? "Yes" : "No";
        Paragraph paragraph2 = new Paragraph(str1 + str2);
        document2.Add((IElement) paragraph2);
      }
      if (!string.IsNullOrEmpty(summary.Permit.SourceDescription))
        document1.Add((IElement) new Paragraph(" Source Description: " + summary.Permit.SourceDescription));
      if (!string.IsNullOrEmpty(summary.Permit.Location))
        document1.Add((IElement) new Paragraph(" Location: " + summary.Permit.Location));
      document1.Add((IElement) new Paragraph(" ADC Map Page: " + summary.Permit.ADCPageLetterGrid + " Row: " + summary.Permit.ADCXCoord + " Column: " + summary.Permit.ADCYCoord));
      document1.Add((IElement) new Paragraph(" County hydro map: " + summary.Permit.CountyHydroGeoMap));
      document1.Add((IElement) new Paragraph(" USGS Topo map: " + summary.Permit.USGSTopoMap));
      document1.Add((IElement) new Paragraph(" "));
label_496:
      document1.Add((IElement) new Paragraph("Easement Information", font));
      iTextSharp.text.Document document12 = document1;
      string str21 = " Conservation Easement Value: ";
      nullable5 = summary.Permit.IsSubjectToEasement;
      string str22;
      if (!nullable5.HasValue)
      {
        str22 = "";
      }
      else
      {
        nullable5 = summary.Permit.IsSubjectToEasement;
        str22 = nullable5.Value ? "Yes" : "No";
      }
      Paragraph paragraph12 = new Paragraph(str21 + str22);
      document12.Add((IElement) paragraph12);
      iTextSharp.text.Document document13 = document1;
      string str23 = " Notified Holder Value: ";
      nullable5 = summary.Permit.HaveNotifiedEasementHolder;
      string str24;
      if (!nullable5.HasValue)
      {
        str24 = "";
      }
      else
      {
        nullable5 = summary.Permit.HaveNotifiedEasementHolder;
        str24 = nullable5.Value ? "Yes" : "No";
      }
      Paragraph paragraph13 = new Paragraph(str23 + str24);
      document13.Add((IElement) paragraph13);
      document1.Add((IElement) paragraph1);
      document1.Close();
      memoryStream.Seek(0L, SeekOrigin.Begin);
      return (FileResult) this.File((Stream) memoryStream, "application/pdf");
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual ActionResult Index()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Index, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GetPermit()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermit, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult GetPermitId()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitId, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult LoadDetailsTab()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadDetailsTab, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual PartialViewResult LoadReviewTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadReviewTab, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult LoadConditionsTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadConditionsTab, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual PartialViewResult LoadIPLTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadIPLTab, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult GetIplContact()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.GetIplContact, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual PartialViewResult LoadCCListTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadCCListTab, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual PartialViewResult LoadPumpageContactTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadPumpageContactTab, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult LoadDocumentsTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadDocumentsTab, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual PartialViewResult LoadCommentsTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadCommentsTab, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual PartialViewResult LoadComplianceTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadComplianceTab, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual PartialViewResult LoadEnforcementTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadEnforcementTab, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual PartialViewResult LoadContactsTab()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadContactsTab, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult GetPermitHistory()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitHistory, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult SavePermitType()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SavePermitType, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult SearchSupplementalGroupGridData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchSupplementalGroupGridData, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult ViewSupplementalGroupGridData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ViewSupplementalGroupGridData, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult SetSupplementalGroupType()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetSupplementalGroupType, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult AddToSupplementalGroup()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddToSupplementalGroup, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult RemoveFromSupplementalGroup()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemoveFromSupplementalGroup, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult CreateNewSupplementalGroup()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CreateNewSupplementalGroup, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult DenyPermit()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DenyPermit, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult ApprovePermit()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ApprovePermit, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult SetPermitDates()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPermitDates, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult GetPermitDates()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitDates, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult DateElevenMonthsElevenYears()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DateElevenMonthsElevenYears, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual ActionResult LoadAdditionalLandOwners()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadAdditionalLandOwners, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult LoadPermitHearing()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadPermitHearing, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult SavePermitHearing()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SavePermitHearing, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult DeletePermitHearing()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeletePermitHearing, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GetPermitHearing()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitHearing, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult GetPermitHearingByPermitId()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitHearingByPermitId, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult AddPumpageContact()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddPumpageContact, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult RemovePermitContact()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemovePermitContact, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult LoadChecklist()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadChecklist, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult SaveContactsInDetailsContactsTab()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SaveContactsInDetailsContactsTab, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult LoadPermitInformation()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadPermitInformation, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult SavePermitInformation()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SavePermitInformation, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual PartialViewResult LoadApprovedAllocation()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.LoadApprovedAllocation, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult SaveApprovedAllocation()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SaveApprovedAllocation, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult GenerateAveryLabelsForIPL()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.GenerateAveryLabelsForIPL, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult CopyConditions()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CopyConditions, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult LoadApplicationsInProgress()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.LoadApplicationsInProgress, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult ApplicationWizard()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.ApplicationWizard, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult CancelPermitApplication()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CancelPermitApplication, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult LinksModal()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.LinksModal, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult CreatePendingPermit()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreatePendingPermit, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GetPendingPermitCount()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPendingPermitCount, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GeneratePermitNumber()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GeneratePermitNumber, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult IsUniquePermitNumber()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.IsUniquePermitNumber, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult ClonePermit()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ClonePermit, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult GetPermitNumber()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitNumber, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult SetPermitNumber()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPermitNumber, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult GetPermitStatusesByCategory()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitStatusesByCategory, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult SetPermitStatus()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPermitStatus, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult SetPreviousPermitStatus()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetPreviousPermitStatus, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult SearchContact()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchContact, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult AddPermitLocation()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddPermitLocation, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult RemovePermitLocation()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemovePermitLocation, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual PartialViewResult AddWaterWithdrawalLocation()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.AddWaterWithdrawalLocation, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GetConservationEasement()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetConservationEasement, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult SetConservationEasement()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SetConservationEasement, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual PartialViewResult EditWaterWithdrawalLocation()
    {
      return (PartialViewResult) new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.EditWaterWithdrawalLocation, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult RemoveWaterWithdrawalLocation()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.RemoveWaterWithdrawalLocation, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult Summary()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Summary, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Affirmation()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Affirmation, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult UpdateStatus()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateStatus, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult GenerateComplianceReports()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateComplianceReports, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult DeleteComplianceReports()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeleteComplianceReports, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual FileResult CreatePdf()
    {
      return (FileResult) new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.CreatePdf, (string) null);
    }

    public class PdfHeader : PdfPageEventHelper
    {
      public override void OnStartPage(PdfWriter writer, iTextSharp.text.Document doc)
      {
        iTextSharp.text.Font font1 = FontFactory.GetFont("Times-Roman", "Cp1252", false, 16f, 1);
        iTextSharp.text.Font font2 = FontFactory.GetFont("Times-Roman", "Cp1252", false, 10f);
        Paragraph paragraph1 = new Paragraph("MARYLAND DEPARTMENT OF THE ENVIRONMENT", font1);
        Paragraph paragraph2 = new Paragraph("WATER MANAGEMENT ADMINISTRATION - WATER SUPPLY PROGRAM");
        Paragraph paragraph3 = new Paragraph("1800 Washington Boulevard, Baltimore, Maryland 21230", font2);
        Paragraph paragraph4 = new Paragraph("(410) 537-3714   1-800-633-6101   fax (410)537-3157   http://www.mde.state.md.us");
        Paragraph paragraph5 = new Paragraph("APPLICATION FOR A PERMIT TO APPROPRIATE AND USE WATERS OF THE STATE");
        paragraph1.Alignment = 1;
        paragraph2.Alignment = 1;
        paragraph3.Alignment = 1;
        paragraph4.Alignment = 1;
        paragraph5.Alignment = 1;
        PdfPTable pdfPtable = new PdfPTable(1) { WidthPercentage = 100f, TotalWidth = 550f, HorizontalAlignment = 1 };
        PdfPCell pdfPcell = new PdfPCell();
        pdfPcell.Border = 0;
        pdfPcell.PaddingLeft = 10f;
        PdfPCell cell = pdfPcell;
        cell.AddElement((IElement) paragraph1);
        cell.AddElement((IElement) paragraph2);
        cell.AddElement((IElement) paragraph3);
        cell.AddElement((IElement) paragraph4);
        cell.AddElement((IElement) paragraph5);
        pdfPtable.AddCell(cell);
        double num = (double) pdfPtable.WriteSelectedRows(0, -1, 10f, 800f, writer.DirectContent);
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string GetPermit = "GetPermit";
      public readonly string GetPermitId = "GetPermitId";
      public readonly string LoadDetailsTab = "LoadDetailsTab";
      public readonly string LoadReviewTab = "LoadReviewTab";
      public readonly string LoadConditionsTab = "LoadConditionsTab";
      public readonly string LoadIPLTab = "LoadIPLTab";
      public readonly string GetIplContact = "GetIplContact";
      public readonly string LoadCCListTab = "LoadCCListTab";
      public readonly string LoadPumpageContactTab = "LoadPumpageContactTab";
      public readonly string LoadDocumentsTab = "LoadDocumentsTab";
      public readonly string LoadCommentsTab = "LoadCommentsTab";
      public readonly string LoadComplianceTab = "LoadComplianceTab";
      public readonly string LoadEnforcementTab = "LoadEnforcementTab";
      public readonly string LoadContactsTab = "LoadContactsTab";
      public readonly string GetPermitHistory = "GetPermitHistory";
      public readonly string GetPermitTypes = "GetPermitTypes";
      public readonly string SavePermitType = "SavePermitType";
      public readonly string GetSupplementalTypeList = "GetSupplementalTypeList";
      public readonly string SearchSupplementalGroupGridData = "SearchSupplementalGroupGridData";
      public readonly string ViewSupplementalGroupGridData = "ViewSupplementalGroupGridData";
      public readonly string SetSupplementalGroupType = "SetSupplementalGroupType";
      public readonly string AddToSupplementalGroup = "AddToSupplementalGroup";
      public readonly string RemoveFromSupplementalGroup = "RemoveFromSupplementalGroup";
      public readonly string CreateNewSupplementalGroup = "CreateNewSupplementalGroup";
      public readonly string DenyPermit = "DenyPermit";
      public readonly string ApprovePermit = "ApprovePermit";
      public readonly string SetPermitDates = "SetPermitDates";
      public readonly string GetPermitDates = "GetPermitDates";
      public readonly string DateElevenMonthsElevenYears = "DateElevenMonthsElevenYears";
      public readonly string LoadAdditionalLandOwners = "LoadAdditionalLandOwners";
      public readonly string LoadPermitHearing = "LoadPermitHearing";
      public readonly string SavePermitHearing = "SavePermitHearing";
      public readonly string DeletePermitHearing = "DeletePermitHearing";
      public readonly string GetPermitHearing = "GetPermitHearing";
      public readonly string GetPermitHearingByPermitId = "GetPermitHearingByPermitId";
      public readonly string AddPumpageContact = "AddPumpageContact";
      public readonly string RemovePermitContact = "RemovePermitContact";
      public readonly string LoadChecklist = "LoadChecklist";
      public readonly string SaveContactsInDetailsContactsTab = "SaveContactsInDetailsContactsTab";
      public readonly string LoadPermitInformation = "LoadPermitInformation";
      public readonly string SavePermitInformation = "SavePermitInformation";
      public readonly string LoadApprovedAllocation = "LoadApprovedAllocation";
      public readonly string SaveApprovedAllocation = "SaveApprovedAllocation";
      public readonly string GenerateAveryLabelsForIPL = "GenerateAveryLabelsForIPL";
      public readonly string CopyConditions = "CopyConditions";
      public readonly string ResumeApplication = "ResumeApplication";
      public readonly string LoadApplicationsInProgress = "LoadApplicationsInProgress";
      public readonly string ApplicationWizard = "ApplicationWizard";
      public readonly string CancelPermitApplication = "CancelPermitApplication";
      public readonly string LinksModal = "LinksModal";
      public readonly string CreatePendingPermit = "CreatePendingPermit";
      public readonly string GetPendingPermitCount = "GetPendingPermitCount";
      public readonly string GeneratePermitNumber = "GeneratePermitNumber";
      public readonly string IsUniquePermitNumber = "IsUniquePermitNumber";
      public readonly string ClonePermit = "ClonePermit";
      public readonly string GetPermitNumber = "GetPermitNumber";
      public readonly string SetPermitNumber = "SetPermitNumber";
      public readonly string GetPermitStatusesByCategory = "GetPermitStatusesByCategory";
      public readonly string SetPermitStatus = "SetPermitStatus";
      public readonly string SetPreviousPermitStatus = "SetPreviousPermitStatus";
      public readonly string SearchContact = "SearchContact";
      public readonly string AddPermitLocation = "AddPermitLocation";
      public readonly string RemovePermitLocation = "RemovePermitLocation";
      public readonly string AddWaterWithdrawalLocation = "AddWaterWithdrawalLocation";
      public readonly string GetConservationEasement = "GetConservationEasement";
      public readonly string SetConservationEasement = "SetConservationEasement";
      public readonly string EditWaterWithdrawalLocation = "EditWaterWithdrawalLocation";
      public readonly string RemoveWaterWithdrawalLocation = "RemoveWaterWithdrawalLocation";
      public readonly string Summary = "Summary";
      public readonly string Affirmation = "Affirmation";
      public readonly string UpdateStatus = "UpdateStatus";
      public readonly string GenerateComplianceReports = "GenerateComplianceReports";
      public readonly string DeleteComplianceReports = "DeleteComplianceReports";
      public readonly string CreatePdf = "CreatePdf";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string GetPermit = "GetPermit";
      public const string GetPermitId = "GetPermitId";
      public const string LoadDetailsTab = "LoadDetailsTab";
      public const string LoadReviewTab = "LoadReviewTab";
      public const string LoadConditionsTab = "LoadConditionsTab";
      public const string LoadIPLTab = "LoadIPLTab";
      public const string GetIplContact = "GetIplContact";
      public const string LoadCCListTab = "LoadCCListTab";
      public const string LoadPumpageContactTab = "LoadPumpageContactTab";
      public const string LoadDocumentsTab = "LoadDocumentsTab";
      public const string LoadCommentsTab = "LoadCommentsTab";
      public const string LoadComplianceTab = "LoadComplianceTab";
      public const string LoadEnforcementTab = "LoadEnforcementTab";
      public const string LoadContactsTab = "LoadContactsTab";
      public const string GetPermitHistory = "GetPermitHistory";
      public const string GetPermitTypes = "GetPermitTypes";
      public const string SavePermitType = "SavePermitType";
      public const string GetSupplementalTypeList = "GetSupplementalTypeList";
      public const string SearchSupplementalGroupGridData = "SearchSupplementalGroupGridData";
      public const string ViewSupplementalGroupGridData = "ViewSupplementalGroupGridData";
      public const string SetSupplementalGroupType = "SetSupplementalGroupType";
      public const string AddToSupplementalGroup = "AddToSupplementalGroup";
      public const string RemoveFromSupplementalGroup = "RemoveFromSupplementalGroup";
      public const string CreateNewSupplementalGroup = "CreateNewSupplementalGroup";
      public const string DenyPermit = "DenyPermit";
      public const string ApprovePermit = "ApprovePermit";
      public const string SetPermitDates = "SetPermitDates";
      public const string GetPermitDates = "GetPermitDates";
      public const string DateElevenMonthsElevenYears = "DateElevenMonthsElevenYears";
      public const string LoadAdditionalLandOwners = "LoadAdditionalLandOwners";
      public const string LoadPermitHearing = "LoadPermitHearing";
      public const string SavePermitHearing = "SavePermitHearing";
      public const string DeletePermitHearing = "DeletePermitHearing";
      public const string GetPermitHearing = "GetPermitHearing";
      public const string GetPermitHearingByPermitId = "GetPermitHearingByPermitId";
      public const string AddPumpageContact = "AddPumpageContact";
      public const string RemovePermitContact = "RemovePermitContact";
      public const string LoadChecklist = "LoadChecklist";
      public const string SaveContactsInDetailsContactsTab = "SaveContactsInDetailsContactsTab";
      public const string LoadPermitInformation = "LoadPermitInformation";
      public const string SavePermitInformation = "SavePermitInformation";
      public const string LoadApprovedAllocation = "LoadApprovedAllocation";
      public const string SaveApprovedAllocation = "SaveApprovedAllocation";
      public const string GenerateAveryLabelsForIPL = "GenerateAveryLabelsForIPL";
      public const string CopyConditions = "CopyConditions";
      public const string ResumeApplication = "ResumeApplication";
      public const string LoadApplicationsInProgress = "LoadApplicationsInProgress";
      public const string ApplicationWizard = "ApplicationWizard";
      public const string CancelPermitApplication = "CancelPermitApplication";
      public const string LinksModal = "LinksModal";
      public const string CreatePendingPermit = "CreatePendingPermit";
      public const string GetPendingPermitCount = "GetPendingPermitCount";
      public const string GeneratePermitNumber = "GeneratePermitNumber";
      public const string IsUniquePermitNumber = "IsUniquePermitNumber";
      public const string ClonePermit = "ClonePermit";
      public const string GetPermitNumber = "GetPermitNumber";
      public const string SetPermitNumber = "SetPermitNumber";
      public const string GetPermitStatusesByCategory = "GetPermitStatusesByCategory";
      public const string SetPermitStatus = "SetPermitStatus";
      public const string SetPreviousPermitStatus = "SetPreviousPermitStatus";
      public const string SearchContact = "SearchContact";
      public const string AddPermitLocation = "AddPermitLocation";
      public const string RemovePermitLocation = "RemovePermitLocation";
      public const string AddWaterWithdrawalLocation = "AddWaterWithdrawalLocation";
      public const string GetConservationEasement = "GetConservationEasement";
      public const string SetConservationEasement = "SetConservationEasement";
      public const string EditWaterWithdrawalLocation = "EditWaterWithdrawalLocation";
      public const string RemoveWaterWithdrawalLocation = "RemoveWaterWithdrawalLocation";
      public const string Summary = "Summary";
      public const string Affirmation = "Affirmation";
      public const string UpdateStatus = "UpdateStatus";
      public const string GenerateComplianceReports = "GenerateComplianceReports";
      public const string DeleteComplianceReports = "DeleteComplianceReports";
      public const string CreatePdf = "CreatePdf";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Index
    {
      public readonly string id = "id";
      public readonly string activeTab = "activeTab";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetPermit
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetPermitId
    {
      public readonly string permitName = "permitName";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadDetailsTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadReviewTab
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_LoadConditionsTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadIPLTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetIplContact
    {
      public readonly string permitContactId = "permitContactId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadCCListTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadPumpageContactTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadDocumentsTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadCommentsTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadComplianceTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadEnforcementTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadContactsTab
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetPermitHistory
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SavePermitType
    {
      public readonly string id = "id";
      public readonly string permitTypeId = "permitTypeId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_SearchSupplementalGroupGridData
    {
      public readonly string command = "command";
      public readonly string permitNames = "permitNames";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_ViewSupplementalGroupGridData
    {
      public readonly string command = "command";
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SetSupplementalGroupType
    {
      public readonly string permitId = "permitId";
      public readonly string newSupplementalTypeId = "newSupplementalTypeId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddToSupplementalGroup
    {
      public readonly string permitId = "permitId";
      public readonly string groupId = "groupId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_RemoveFromSupplementalGroup
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CreateNewSupplementalGroup
    {
      public readonly string activePermitId = "activePermitId";
      public readonly string additionalPermitNames = "additionalPermitNames";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_DenyPermit
    {
      public readonly string PermitId = "PermitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_ApprovePermit
    {
      public readonly string PermitId = "PermitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SetPermitDates
    {
      public readonly string permitId = "permitId";
      public readonly string effectiveDate = "effectiveDate";
      public readonly string expirationDate = "expirationDate";
      public readonly string appropriationDate = "appropriationDate";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetPermitDates
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_DateElevenMonthsElevenYears
    {
      public readonly string date = "date";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_LoadAdditionalLandOwners
    {
      public readonly string PermitId = "PermitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_LoadPermitHearing
    {
      public readonly string Permitid = "Permitid";
      public readonly string PermitStatusId = "PermitStatusId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SavePermitHearing
    {
      public readonly string permitHearing = "permitHearing";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_DeletePermitHearing
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetPermitHearing
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetPermitHearingByPermitId
    {
      public readonly string PermitId = "PermitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AddPumpageContact
    {
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_RemovePermitContact
    {
      public readonly string permitContactId = "permitContactId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadChecklist
    {
      public readonly string id = "id";
      public readonly string permitStatus = "permitStatus";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SaveContactsInDetailsContactsTab
    {
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadPermitInformation
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SavePermitInformation
    {
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadApprovedAllocation
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SaveApprovedAllocation
    {
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GenerateAveryLabelsForIPL
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CopyConditions
    {
      public readonly string permitId = "permitId";
      public readonly string permitTemplateId = "permitTemplateId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadApplicationsInProgress
    {
      public readonly string command = "command";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_ApplicationWizard
    {
      public readonly string id = "id";
      public readonly string applicationType = "applicationType";
      public readonly string form = "form";
      public readonly string waterUserDetailForm = "waterUserDetailForm";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_CancelPermitApplication
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_LinksModal
    {
      public readonly string permitNumber = "permitNumber";
      public readonly string linkType = "linkType";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_CreatePendingPermit
    {
      public readonly string permitId = "permitId";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetPendingPermitCount
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GeneratePermitNumber
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_IsUniquePermitNumber
    {
      public readonly string permitNumber = "permitNumber";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_ClonePermit
    {
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetPermitNumber
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_SetPermitNumber
    {
      public readonly string permitId = "permitId";
      public readonly string permitNumber = "permitNumber";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetPermitStatusesByCategory
    {
      public readonly string categoryId = "categoryId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SetPermitStatus
    {
      public readonly string permitId = "permitId";
      public readonly string permitStatusId = "permitStatusId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_SetPreviousPermitStatus
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_SearchContact
    {
      public readonly string command = "command";
      public readonly string searchForm = "searchForm";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddPermitLocation
    {
      public readonly string permitId = "permitId";
      public readonly string x = "x";
      public readonly string y = "y";
      public readonly string srid = "srid";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_RemovePermitLocation
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddWaterWithdrawalLocation
    {
      public readonly string permitId = "permitId";
      public readonly string x = "x";
      public readonly string y = "y";
      public readonly string srid = "srid";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetConservationEasement
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_SetConservationEasement
    {
      public readonly string permitId = "permitId";
      public readonly string isSubjectToEasement = "isSubjectToEasement";
      public readonly string haveNotifiedEasementHolder = "haveNotifiedEasementHolder";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EditWaterWithdrawalLocation
    {
      public readonly string id = "id";
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_RemoveWaterWithdrawalLocation
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Summary
    {
      public readonly string id = "id";
      public readonly string waterDetailsSelected = "waterDetailsSelected";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Affirmation
    {
      public readonly string id = "id";
      public readonly string submittedBy = "submittedBy";
      public readonly string submittedDate = "submittedDate";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_UpdateStatus
    {
      public readonly string permitId = "permitId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string completed = "completed";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GenerateComplianceReports
    {
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_DeleteComplianceReports
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_CreatePdf
    {
      public readonly string id = "id";
      public readonly string waterDetailsSelected = "waterDetailsSelected";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly PermitController.ViewsClass._ViewNamesClass s_ViewNames = new PermitController.ViewsClass._ViewNamesClass();
      private static readonly PermitController.ViewsClass._DetailsPartialsClass s_DetailsPartials = new PermitController.ViewsClass._DetailsPartialsClass();
      private static readonly PermitController.ViewsClass._TabsClass s_Tabs = new PermitController.ViewsClass._TabsClass();
      private static readonly PermitController.ViewsClass._WaterUseDetailTemplatesClass s_WaterUseDetailTemplates = new PermitController.ViewsClass._WaterUseDetailTemplatesClass();
      public readonly string _ApplicationWizardContacts = "~/Areas/Permitting/Views/Permit/_ApplicationWizardContacts.cshtml";
      public readonly string _permitDetailsNavControls = "~/Areas/Permitting/Views/Permit/_permitDetailsNavControls.cshtml";
      public readonly string Affirmation = "~/Areas/Permitting/Views/Permit/Affirmation.cshtml";
      public readonly string ApplicantIdentification = "~/Areas/Permitting/Views/Permit/ApplicantIdentification.cshtml";
      public readonly string ApplicationWizard = "~/Areas/Permitting/Views/Permit/ApplicationWizard.cshtml";
      public readonly string ApprovedAllocation = "~/Areas/Permitting/Views/Permit/ApprovedAllocation.cshtml";
      public readonly string ContactSearch = "~/Areas/Permitting/Views/Permit/ContactSearch.cshtml";
      public readonly string ContactSearchResult = "~/Areas/Permitting/Views/Permit/ContactSearchResult.cshtml";
      public readonly string Error = "~/Areas/Permitting/Views/Permit/Error.cshtml";
      public readonly string IdentifyLocation = "~/Areas/Permitting/Views/Permit/IdentifyLocation.cshtml";
      public readonly string Index = "~/Areas/Permitting/Views/Permit/Index.cshtml";
      public readonly string PermitInformation = "~/Areas/Permitting/Views/Permit/PermitInformation.cshtml";
      public readonly string PermtHearingForm = "~/Areas/Permitting/Views/Permit/PermtHearingForm.cshtml";
      public readonly string ResumeApplication = "~/Areas/Permitting/Views/Permit/ResumeApplication.cshtml";
      public readonly string ReviewApplication = "~/Areas/Permitting/Views/Permit/ReviewApplication.cshtml";
      public readonly string Summary = "~/Areas/Permitting/Views/Permit/Summary.cshtml";
      public readonly string WastewaterTreatmentAndDisposal = "~/Areas/Permitting/Views/Permit/WastewaterTreatmentAndDisposal.cshtml";
      public readonly string WaterUseCategoryAndType = "~/Areas/Permitting/Views/Permit/WaterUseCategoryAndType.cshtml";
      public readonly string WaterUseDescription = "~/Areas/Permitting/Views/Permit/WaterUseDescription.cshtml";
      public readonly string WaterUseDetails = "~/Areas/Permitting/Views/Permit/WaterUseDetails.cshtml";
      public readonly string WithdrawalSourceInformation = "~/Areas/Permitting/Views/Permit/WithdrawalSourceInformation.cshtml";

      public PermitController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return PermitController.ViewsClass.s_ViewNames;
        }
      }

      public PermitController.ViewsClass._DetailsPartialsClass DetailsPartials
      {
        get
        {
          return PermitController.ViewsClass.s_DetailsPartials;
        }
      }

      public PermitController.ViewsClass._TabsClass Tabs
      {
        get
        {
          return PermitController.ViewsClass.s_Tabs;
        }
      }

      public PermitController.ViewsClass._WaterUseDetailTemplatesClass WaterUseDetailTemplates
      {
        get
        {
          return PermitController.ViewsClass.s_WaterUseDetailTemplates;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string _ApplicationWizardContacts = "_ApplicationWizardContacts";
        public readonly string _permitDetailsNavControls = "_permitDetailsNavControls";
        public readonly string Affirmation = "Affirmation";
        public readonly string ApplicantIdentification = "ApplicantIdentification";
        public readonly string ApplicationWizard = "ApplicationWizard";
        public readonly string ApprovedAllocation = "ApprovedAllocation";
        public readonly string ContactSearch = "ContactSearch";
        public readonly string ContactSearchResult = "ContactSearchResult";
        public readonly string Error = "Error";
        public readonly string IdentifyLocation = "IdentifyLocation";
        public readonly string Index = "Index";
        public readonly string PermitInformation = "PermitInformation";
        public readonly string PermtHearingForm = "PermtHearingForm";
        public readonly string ResumeApplication = "ResumeApplication";
        public readonly string ReviewApplication = "ReviewApplication";
        public readonly string Summary = "Summary";
        public readonly string WastewaterTreatmentAndDisposal = "WastewaterTreatmentAndDisposal";
        public readonly string WaterUseCategoryAndType = "WaterUseCategoryAndType";
        public readonly string WaterUseDescription = "WaterUseDescription";
        public readonly string WaterUseDetails = "WaterUseDetails";
        public readonly string WithdrawalSourceInformation = "WithdrawalSourceInformation";
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public class _DetailsPartialsClass
      {
        private static readonly PermitController.ViewsClass._DetailsPartialsClass._ViewNamesClass s_ViewNames = new PermitController.ViewsClass._DetailsPartialsClass._ViewNamesClass();
        public readonly string _AdditionalLandOwnersForm = "~/Areas/Permitting/Views/Permit/DetailsPartials/_AdditionalLandOwnersForm.cshtml";
        public readonly string _AdvertisementFee = "~/Areas/Permitting/Views/Permit/DetailsPartials/_AdvertisementFee.cshtml";
        public readonly string _CheckList = "~/Areas/Permitting/Views/Permit/DetailsPartials/_CheckList.cshtml";
        public readonly string _ContactSearch = "~/Areas/Permitting/Views/Permit/DetailsPartials/_ContactSearch.cshtml";
        public readonly string _CreatePendingPermit = "~/Areas/Permitting/Views/Permit/DetailsPartials/_CreatePendingPermit.cshtml";
        public readonly string _FinalizePermit = "~/Areas/Permitting/Views/Permit/DetailsPartials/_FinalizePermit.cshtml";
        public readonly string _iplContact = "~/Areas/Permitting/Views/Permit/DetailsPartials/_iplContact.cshtml";
        public readonly string _PermitHearing = "~/Areas/Permitting/Views/Permit/DetailsPartials/_PermitHearing.cshtml";
        public readonly string _PermitStatusOptions = "~/Areas/Permitting/Views/Permit/DetailsPartials/_PermitStatusOptions.cshtml";
        public readonly string _ProjectManager = "~/Areas/Permitting/Views/Permit/DetailsPartials/_ProjectManager.cshtml";
        public readonly string _PublicComment = "~/Areas/Permitting/Views/Permit/DetailsPartials/_PublicComment.cshtml";
        public readonly string _Supervisor = "~/Areas/Permitting/Views/Permit/DetailsPartials/_Supervisor.cshtml";

        public PermitController.ViewsClass._DetailsPartialsClass._ViewNamesClass ViewNames
        {
          get
          {
            return PermitController.ViewsClass._DetailsPartialsClass.s_ViewNames;
          }
        }

        public class _ViewNamesClass
        {
          public readonly string _AdditionalLandOwnersForm = "_AdditionalLandOwnersForm";
          public readonly string _AdvertisementFee = "_AdvertisementFee";
          public readonly string _CheckList = "_CheckList";
          public readonly string _ContactSearch = "_ContactSearch";
          public readonly string _CreatePendingPermit = "_CreatePendingPermit";
          public readonly string _FinalizePermit = "_FinalizePermit";
          public readonly string _iplContact = "_iplContact";
          public readonly string _PermitHearing = "_PermitHearing";
          public readonly string _PermitStatusOptions = "_PermitStatusOptions";
          public readonly string _ProjectManager = "_ProjectManager";
          public readonly string _PublicComment = "_PublicComment";
          public readonly string _Supervisor = "_Supervisor";
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public class _TabsClass
      {
        private static readonly PermitController.ViewsClass._TabsClass._ViewNamesClass s_ViewNames = new PermitController.ViewsClass._TabsClass._ViewNamesClass();
        public readonly string _CCList = "~/Areas/Permitting/Views/Permit/Tabs/_CCList.cshtml";
        public readonly string _Comments = "~/Areas/Permitting/Views/Permit/Tabs/_Comments.cshtml";
        public readonly string _Compliance = "~/Areas/Permitting/Views/Permit/Tabs/_Compliance.cshtml";
        public readonly string _Conditions = "~/Areas/Permitting/Views/Permit/Tabs/_Conditions.cshtml";
        public readonly string _Contacts = "~/Areas/Permitting/Views/Permit/Tabs/_Contacts.cshtml";
        public readonly string _Details = "~/Areas/Permitting/Views/Permit/Tabs/_Details.cshtml";
        public readonly string _Documents = "~/Areas/Permitting/Views/Permit/Tabs/_Documents.cshtml";
        public readonly string _Enforcement = "~/Areas/Permitting/Views/Permit/Tabs/_Enforcement.cshtml";
        public readonly string _Ipl = "~/Areas/Permitting/Views/Permit/Tabs/_Ipl.cshtml";
        public readonly string _PumpageContact = "~/Areas/Permitting/Views/Permit/Tabs/_PumpageContact.cshtml";
        public readonly string _Review = "~/Areas/Permitting/Views/Permit/Tabs/_Review.cshtml";

        public PermitController.ViewsClass._TabsClass._ViewNamesClass ViewNames
        {
          get
          {
            return PermitController.ViewsClass._TabsClass.s_ViewNames;
          }
        }

        public class _ViewNamesClass
        {
          public readonly string _CCList = "_CCList";
          public readonly string _Comments = "_Comments";
          public readonly string _Compliance = "_Compliance";
          public readonly string _Conditions = "_Conditions";
          public readonly string _Contacts = "_Contacts";
          public readonly string _Details = "_Details";
          public readonly string _Documents = "_Documents";
          public readonly string _Enforcement = "_Enforcement";
          public readonly string _Ipl = "_Ipl";
          public readonly string _PumpageContact = "_PumpageContact";
          public readonly string _Review = "_Review";
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public class _WaterUseDetailTemplatesClass
      {
        private static readonly PermitController.ViewsClass._WaterUseDetailTemplatesClass._ViewNamesClass s_ViewNames = new PermitController.ViewsClass._WaterUseDetailTemplatesClass._ViewNamesClass();
        public readonly string AquacultureAquarium = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/AquacultureAquarium.cshtml";
        public readonly string CommercialDrinkingOrSanitary = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/CommercialDrinkingOrSanitary.cshtml";
        public readonly string ConstructionDewatering = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/ConstructionDewatering.cshtml";
        public readonly string Crop = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/Crop.cshtml";
        public readonly string DairyAnimalWatering = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/DairyAnimalWatering.cshtml";
        public readonly string EducationalDrinkingOrsanitary = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/EducationalDrinkingOrsanitary.cshtml";
        public readonly string FarmPotableSupplies = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/FarmPotableSupplies.cshtml";
        public readonly string FoodProcesssing = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/FoodProcesssing.cshtml";
        public readonly string GolfCourseIrrigation = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/GolfCourseIrrigation.cshtml";
        public readonly string IrrigationUndefined = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/IrrigationUndefined.cshtml";
        public readonly string LawnAndParkIrrigation = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/LawnAndParkIrrigation.cshtml";
        public readonly string LivestockWatering = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/LivestockWatering.cshtml";
        public readonly string NurseryStock = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/NurseryStock.cshtml";
        public readonly string OtherLivestockWatering = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/OtherLivestockWatering.cshtml";
        public readonly string PoultryWateringEvaporative = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/PoultryWateringEvaporative.cshtml";
        public readonly string PoultryWateringFogger = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/PoultryWateringFogger.cshtml";
        public readonly string PrivateWaterSuppliers = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/PrivateWaterSuppliers.cshtml";
        public readonly string ReligiousDringkingOrSanitary = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/ReligiousDringkingOrSanitary.cshtml";
        public readonly string SmallIntermitentIrrigation = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/SmallIntermitentIrrigation.cshtml";
        public readonly string Sod = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/Sod.cshtml";
        public readonly string WildlifePond = "~/Areas/Permitting/Views/Permit/WaterUseDetailTemplates/WildlifePond.cshtml";

        public PermitController.ViewsClass._WaterUseDetailTemplatesClass._ViewNamesClass ViewNames
        {
          get
          {
            return PermitController.ViewsClass._WaterUseDetailTemplatesClass.s_ViewNames;
          }
        }

        public class _ViewNamesClass
        {
          public readonly string AquacultureAquarium = "AquacultureAquarium";
          public readonly string CommercialDrinkingOrSanitary = "CommercialDrinkingOrSanitary";
          public readonly string ConstructionDewatering = "ConstructionDewatering";
          public readonly string Crop = "Crop";
          public readonly string DairyAnimalWatering = "DairyAnimalWatering";
          public readonly string EducationalDrinkingOrsanitary = "EducationalDrinkingOrsanitary";
          public readonly string FarmPotableSupplies = "FarmPotableSupplies";
          public readonly string FoodProcesssing = "FoodProcesssing";
          public readonly string GolfCourseIrrigation = "GolfCourseIrrigation";
          public readonly string IrrigationUndefined = "IrrigationUndefined";
          public readonly string LawnAndParkIrrigation = "LawnAndParkIrrigation";
          public readonly string LivestockWatering = "LivestockWatering";
          public readonly string NurseryStock = "NurseryStock";
          public readonly string OtherLivestockWatering = "OtherLivestockWatering";
          public readonly string PoultryWateringEvaporative = "PoultryWateringEvaporative";
          public readonly string PoultryWateringFogger = "PoultryWateringFogger";
          public readonly string PrivateWaterSuppliers = "PrivateWaterSuppliers";
          public readonly string ReligiousDringkingOrSanitary = "ReligiousDringkingOrSanitary";
          public readonly string SmallIntermitentIrrigation = "SmallIntermitentIrrigation";
          public readonly string Sod = "Sod";
          public readonly string WildlifePond = "WildlifePond";
        }
      }
    }
  }
}
