﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.DocumentController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using MDGov.MDE.Common.Helpers;
using MDGov.MDE.Common.Helpers.DocumentUtility.Interface;
using MDGov.MDE.Common.Helpers.OpenXml.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization]
  public class DocumentController : BaseController
  {
    private static readonly DocumentController.ActionNamesClass s_actions = new DocumentController.ActionNamesClass();
    private static readonly DocumentController.ActionParamsClass_TemplatesCount s_params_TemplatesCount = new DocumentController.ActionParamsClass_TemplatesCount();
    private static readonly DocumentController.ActionParamsClass_GetTemplates s_params_GetTemplates = new DocumentController.ActionParamsClass_GetTemplates();
    private static readonly DocumentController.ActionParamsClass_GetDocumentParts s_params_GetDocumentParts = new DocumentController.ActionParamsClass_GetDocumentParts();
    private static readonly DocumentController.ActionParamsClass_GenerateDraftPermit s_params_GenerateDraftPermit = new DocumentController.ActionParamsClass_GenerateDraftPermit();
    private static readonly DocumentController.ActionParamsClass_GenerateFinalPermit s_params_GenerateFinalPermit = new DocumentController.ActionParamsClass_GenerateFinalPermit();
    private static readonly DocumentController.ActionParamsClass_GenerateCustomPackage s_params_GenerateCustomPackage = new DocumentController.ActionParamsClass_GenerateCustomPackage();
    private static readonly DocumentController.ActionParamsClass_Generate6MonthLetter s_params_Generate6MonthLetter = new DocumentController.ActionParamsClass_Generate6MonthLetter();
    private static readonly DocumentController.ActionParamsClass_GenerateRenewalExemptionLetters s_params_GenerateRenewalExemptionLetters = new DocumentController.ActionParamsClass_GenerateRenewalExemptionLetters();
    private static readonly DocumentController.ActionParamsClass_GenerateSelectedPumpageLetters s_params_GenerateSelectedPumpageLetters = new DocumentController.ActionParamsClass_GenerateSelectedPumpageLetters();
    private static readonly DocumentController.ActionParamsClass_GenerateWithdrawalLetter s_params_GenerateWithdrawalLetter = new DocumentController.ActionParamsClass_GenerateWithdrawalLetter();
    private static readonly DocumentController.ActionParamsClass_GenerateDecisionLetter s_params_GenerateDecisionLetter = new DocumentController.ActionParamsClass_GenerateDecisionLetter();
    private static readonly DocumentController.ActionParamsClass_GeneratePumpageLetters s_params_GeneratePumpageLetters = new DocumentController.ActionParamsClass_GeneratePumpageLetters();
    private static readonly DocumentController.ViewsClass s_views = new DocumentController.ViewsClass();
    private readonly List<string> _documentErrors = new List<string>();
    private readonly int _contactId = (int) SessionHandler.GetSessionVar(SessionVariables.AuthenticatedUserContactId);
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Permitting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Document";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Document";
    private readonly IOpenXmlWordHelper _wordHelper;
    private readonly IDocumentUtility _documentUtility;
    private readonly IService<Permit> _permittingService;
    private readonly IService<PermitCondition> _permitConditionService;
    private readonly IUpdatableService<MDGov.MDE.Permitting.Model.Document> _documentService;
    private readonly IUpdatableService<DocumentByte> _documentByteService;
    private readonly IService<LU_TemplateType> _templateTypeService;
    private readonly IService<LU_Template> _templateService;
    private readonly IService<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterService;
    private readonly IUpdatableService<DocumentJob> _documentJobService;
    private string _templateFolder;
    private string _digitalSignatureFolder;

    public string TemplateFolder
    {
      get
      {
        if (string.IsNullOrEmpty(this._templateFolder))
          this._templateFolder = this.Server.MapPath(ConfigurationManager.AppSettings["DocumentTemplateFolderPath"]);
        return this._templateFolder;
      }
    }

    public string DigitalSignatureFolder
    {
      get
      {
        if (string.IsNullOrEmpty(this._digitalSignatureFolder))
          this._digitalSignatureFolder = this.Server.MapPath(ConfigurationManager.AppSettings["DigitalSignatureFolderPath"]);
        return this._digitalSignatureFolder;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DocumentController Actions
    {
      get
      {
        return MVC.Permitting.Document;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ActionNamesClass ActionNames
    {
      get
      {
        return DocumentController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DocumentController.ActionParamsClass_TemplatesCount TemplatesCountParams
    {
      get
      {
        return DocumentController.s_params_TemplatesCount;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DocumentController.ActionParamsClass_GetTemplates GetTemplatesParams
    {
      get
      {
        return DocumentController.s_params_GetTemplates;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DocumentController.ActionParamsClass_GetDocumentParts GetDocumentPartsParams
    {
      get
      {
        return DocumentController.s_params_GetDocumentParts;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ActionParamsClass_GenerateDraftPermit GenerateDraftPermitParams
    {
      get
      {
        return DocumentController.s_params_GenerateDraftPermit;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DocumentController.ActionParamsClass_GenerateFinalPermit GenerateFinalPermitParams
    {
      get
      {
        return DocumentController.s_params_GenerateFinalPermit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ActionParamsClass_GenerateCustomPackage GenerateCustomPackageParams
    {
      get
      {
        return DocumentController.s_params_GenerateCustomPackage;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DocumentController.ActionParamsClass_Generate6MonthLetter Generate6MonthLetterParams
    {
      get
      {
        return DocumentController.s_params_Generate6MonthLetter;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public DocumentController.ActionParamsClass_GenerateRenewalExemptionLetters GenerateRenewalExemptionLettersParams
    {
      get
      {
        return DocumentController.s_params_GenerateRenewalExemptionLetters;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ActionParamsClass_GenerateSelectedPumpageLetters GenerateSelectedPumpageLettersParams
    {
      get
      {
        return DocumentController.s_params_GenerateSelectedPumpageLetters;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ActionParamsClass_GenerateWithdrawalLetter GenerateWithdrawalLetterParams
    {
      get
      {
        return DocumentController.s_params_GenerateWithdrawalLetter;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ActionParamsClass_GenerateDecisionLetter GenerateDecisionLetterParams
    {
      get
      {
        return DocumentController.s_params_GenerateDecisionLetter;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ActionParamsClass_GeneratePumpageLetters GeneratePumpageLettersParams
    {
      get
      {
        return DocumentController.s_params_GeneratePumpageLetters;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public DocumentController.ViewsClass Views
    {
      get
      {
        return DocumentController.s_views;
      }
    }

    public DocumentController(IOpenXmlWordHelper wordHelper, IDocumentUtility documentUtility, IUpdatableService<Permit> permittingService, IService<PermitCondition> permitConditionService, IUpdatableService<MDGov.MDE.Permitting.Model.Document> documentService, IUpdatableService<DocumentByte> documentByteService, IService<LU_TemplateType> templateTypeService, IService<LU_Template> templateService, IService<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterService, IUpdatableService<DocumentJob> documentJobService)
    {
      this._wordHelper = wordHelper;
      this._documentUtility = documentUtility;
      this._permittingService = (IService<Permit>) permittingService;
      this._permitConditionService = permitConditionService;
      this._documentService = documentService;
      this._documentByteService = documentByteService;
      this._templateTypeService = templateTypeService;
      this._templateService = templateService;
      this._permitWithdrawalSurfacewaterService = permitWithdrawalSurfacewaterService;
      this._documentJobService = documentJobService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected DocumentController(Dummy d)
    {
    }

    [HttpGet]
    public virtual JsonResult TemplatesCount(int id)
    {
      var data = new
      {
        TotalTemplates = this._templateService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("TemplateTypeId == " + (object) id, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, (string) null).Count<LU_Template>()
      };
      return this.Json((object) data, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual ActionResult GetTemplates(int id)
    {
      ViewBag.DocumentTemplates = this._templateService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("TemplateTypeId == " + (object) id, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
      return (ActionResult) this.PartialView("_SelectTemplates");
    }

    [HttpGet]
    public virtual ActionResult GetDocumentParts(int id)
    {
      DynamicFilter[] dynamicFilterArray = new DynamicFilter[2]
      {
        new DynamicFilter("TemplateTypeId == " + (object) id, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      };
      ViewBag.DocumentTemplates = this._templateService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) dynamicFilterArray, (string) null);
      switch (id)
      {
        case 3:
          return (ActionResult) this.PartialView("_SelectDocument6MonthsParts");
        case 4:
          return (ActionResult) this.PartialView("_SelectDocumentWithdrawalParts");
        case 7:
          return (ActionResult) this.PartialView("_SelectRenewalOrExemptionTemplates");
        case 8:
          return (ActionResult) this.PartialView("_SelectPumpageTemplates");
        default:
          return (ActionResult) this.PartialView("_SelectDocument6MonthsParts");
      }
    }

    [HttpGet]
    public virtual JsonResult GenerateDraftPermit(int permitId, int tableId, int permitStatusId)
    {
      try
      {
        int? permitTypeId = this._permittingService.GetById(permitId, (string) null).PermitTypeId;
        int? nullable1 = permitTypeId;
        string str1;
        if ((nullable1.GetValueOrDefault() != 1 ? 0 : (nullable1.HasValue ? 1 : 0)) == 0)
        {
          int? nullable2 = permitTypeId;
          if ((nullable2.GetValueOrDefault() != 2 ? 0 : (nullable2.HasValue ? 1 : 0)) == 0)
          {
            str1 = "(Non-Agricultural)";
            goto label_4;
          }
        }
        str1 = "(Agricultural)";
label_4:
        string desc = str1;
        LU_TemplateType luTemplateType = this._templateTypeService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("Id == " + (object) 1, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, "LU_Template").First<LU_TemplateType>();
        string destinationFileBaseName = luTemplateType.DestinationFileBaseName;
        bool document = this.GenerateDocument(new WordDocument()
        {
          PermitId = permitId,
          TableId = tableId,
          PermitStatusId = new int?(permitStatusId),
          Description = "Generated by WSIPS",
          Title = string.Format("{0}_{1}_{2}.docx", (object) destinationFileBaseName, (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm")),
          DocumentTypeId = new int?(1),
          SaveToFileSystem = false,
          SaveToDatabase = true,
          Templates = luTemplateType.LU_Template.Where<LU_Template>((Func<LU_Template, bool>) (t =>
          {
            int? templateTypeId = t.TemplateTypeId;
            if ((templateTypeId.GetValueOrDefault() != 1 ? 0 : (templateTypeId.HasValue ? 1 : 0)) != 0 && t.Active)
              return t.Description.Contains(desc);
            return false;
          }))
        }, this._permittingService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties"), "blankPermit", false);
        if (document || !this._documentErrors.Any<string>())
          return this.Json((object) new
          {
            IsSuccess = document
          }, JsonRequestBehavior.AllowGet);
        string str2 = string.Join(",", this._documentErrors.ToArray());
        this._documentErrors.Clear();
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = str2
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult GenerateFinalPermit(int permitId, int tableId, int permitStatusId, bool approved = false)
    {
      try
      {
        MDGov.MDE.Permitting.Model.Document document1 = this._documentService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("RefId == " + (object) permitId, new object[0]),
          new DynamicFilter("DocumentTypeId == " + (object) 1, new object[0])
        }, "DocumentByte").OrderByDescending<MDGov.MDE.Permitting.Model.Document, DateTime>((Func<MDGov.MDE.Permitting.Model.Document, DateTime>) (d => d.CreatedDate)).FirstOrDefault<MDGov.MDE.Permitting.Model.Document>();
        if (document1 == null)
          return this.Json((object) new
          {
            IsSuccess = false,
            Error = "A Draft Permit has not been generated"
          }, JsonRequestBehavior.AllowGet);
        string str1 = "Ready for Final Review";
        int num = 1;
        if (approved)
        {
          str1 = "Final Approved Permit";
          num = 3;
        }
        WordDocument document2 = new WordDocument()
        {
          PermitId = permitId,
          TableId = tableId,
          PermitStatusId = new int?(permitStatusId),
          Title = document1.DocumentTitle,
          Description = str1,
          DocumentTypeId = new int?(num),
          SaveToFileSystem = false,
          SaveToDatabase = true
        };
        if (approved)
          document2.Title = string.Format("{0}_{1}_{2}.docx", (object) "FinalPermit", (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm"));
        Permit byId = this._permittingService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties, PermitWithdrawalSurfacewaters, PermitWithdrawalGroundwaters");
        byte[] document3 = document1.DocumentByte.Document;
        bool flag;
        using (MemoryStream memoryStream = new MemoryStream())
        {
          memoryStream.Write(document3, 0, document3.Length);
          WordprocessingDocument document = this._wordHelper.GetDocument(memoryStream);
          try
          {
            this.FinalizeDocumentPart(document, byId, approved);
            if (document.MainDocumentPart != null)
            {
              List<AlternativeFormatImportPart> list = document.MainDocumentPart.AlternativeFormatImportParts.ToList<AlternativeFormatImportPart>();
              if (list.Count > 0)
              {
                foreach (Stream stream in list.Select(item => new
                {
                  item = item,
                  type = item.ContentType
                }).Where(param0 => param0.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml").Select(param0 => document.Package.GetPart(param0.item.Uri)).Select<PackagePart, Stream>((Func<PackagePart, Stream>) (packagePart => packagePart.GetStream(FileMode.Open, FileAccess.ReadWrite))))
                {
                  using (WordprocessingDocument document4 = WordprocessingDocument.Open(stream, true))
                    this.FinalizeDocumentPart(document4, byId, approved);
                }
              }
              document.MainDocumentPart.Document.Save();
            }
            document.Close();
            if (this._documentErrors.Any<string>())
            {
              string str2 = string.Join(",", this._documentErrors.Distinct<string>().ToArray<string>());
              this._documentErrors.Clear();
              return this.Json((object) new
              {
                IsSuccess = false,
                Error = str2
              }, JsonRequestBehavior.AllowGet);
            }
            byte[] array = memoryStream.ToArray();
            flag = this.SaveToFileSystem(this.TemplateFolder + document2.Title, array);
          }
          finally
          {
            if (document != null)
              document.Dispose();
          }
        }
        if (flag)
        {
            byte[] buffer = System.IO.File.ReadAllBytes(this.TemplateFolder + document2.Title);
          using (MemoryStream memoryStream = new MemoryStream())
          {
            memoryStream.Write(buffer, 0, buffer.Length);
            WordprocessingDocument document = this._wordHelper.GetDocument(memoryStream);
            try
            {
              this.InsertApprovalSignature(document, byId, true);
              if (document.MainDocumentPart != null)
              {
                List<AlternativeFormatImportPart> list = document.MainDocumentPart.AlternativeFormatImportParts.ToList<AlternativeFormatImportPart>();
                if (list.Count > 0)
                {
                  foreach (Stream stream in list.Select(item => new
                  {
                    item = item,
                    type = item.ContentType
                  }).Where(param0 => param0.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml").Select(param0 => document.Package.GetPart(param0.item.Uri)).Select<PackagePart, Stream>((Func<PackagePart, Stream>) (packagePart => packagePart.GetStream(FileMode.Open, FileAccess.ReadWrite))))
                  {
                    using (WordprocessingDocument document4 = WordprocessingDocument.Open(stream, true))
                      this.InsertApprovalSignature(document4, byId, true);
                  }
                }
                document.MainDocumentPart.Document.Save();
              }
              document.Close();
            }
            finally
            {
              if (document != null)
                document.Dispose();
            }
            if (this._documentErrors.Any<string>())
              return this.Json((object) new
              {
                IsSuccess = false,
                Error = string.Join(",", this._documentErrors.Distinct<string>().ToArray<string>())
              }, JsonRequestBehavior.AllowGet);
            byte[] array = memoryStream.ToArray();
            System.IO.File.Delete(this.TemplateFolder + document2.Title);
            if (document2.SaveToFileSystem)
              flag = this.SaveToFileSystem(this.TemplateFolder + document2.Title, array);
            if (document2.PermitStatusId.HasValue)
            {
              if (document2.SaveToDatabase)
                flag = this.SaveToDb(array, document2);
            }
          }
        }
        return this.Json((object) new{ IsSuccess = flag }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult GenerateCustomPackage(int permitId, int tableId, int permitStatusId, int[] templateIds)
    {
      try
      {
        string destinationFileBaseName = this._templateTypeService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("Id == " + (object) 2, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, "LU_Template").First<LU_TemplateType>().DestinationFileBaseName;
        List<LU_Template> list = ((IEnumerable<int>) templateIds).Select<int, LU_Template>((Func<int, LU_Template>) (item => this._templateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("Id == " + (object) item, new object[0])
        }, (string) null).First<LU_Template>())).ToList<LU_Template>();
        bool document = this.GenerateDocument(new WordDocument()
        {
          PermitId = permitId,
          TableId = tableId,
          PermitStatusId = new int?(permitStatusId),
          Description = "Generated by WSIPS",
          DocumentTypeId = new int?(2),
          Title = string.Format("{0}_{1}_{2}.docx", (object) destinationFileBaseName, (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm")),
          SaveToFileSystem = false,
          SaveToDatabase = true,
          TemplateTypeId = 2,
          Templates = (IEnumerable<LU_Template>) list
        }, this._permittingService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties"), "blankCustomPackage", true);
        if (!document && this._documentErrors.Any<string>())
          return this.Json((object) new
          {
            IsSuccess = false,
            Error = string.Join("\n", this._documentErrors.ToArray())
          }, JsonRequestBehavior.AllowGet);
        return this.Json((object) new
        {
          IsSuccess = document
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult Generate6MonthLetter(int permitId, int tableId, int permitStatusId, int templateId)
    {
      try
      {
        string destinationFileBaseName = this._templateTypeService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("Id == " + (object) 3, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, "LU_Template").First<LU_TemplateType>().DestinationFileBaseName;
        IEnumerable<LU_Template> range = this._templateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("Id == " + (object) templateId, new object[0])
        }, (string) null);
        bool document = this.GenerateDocument(new WordDocument()
        {
          PermitId = permitId,
          TableId = tableId,
          PermitStatusId = new int?(permitStatusId),
          Description = "Generated by WSIPS",
          DocumentTypeId = new int?(7),
          Title = string.Format("{0}_{1}_{2}.docx", (object) destinationFileBaseName, (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm")),
          SaveToFileSystem = false,
          SaveToDatabase = true,
          TemplateTypeId = 3,
          Templates = range
        }, this._permittingService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties"), "blank", true);
        if (!document && this._documentErrors.Any<string>())
          return this.Json((object) new
          {
            IsSuccess = false,
            Error = string.Join(",", this._documentErrors.ToArray())
          }, JsonRequestBehavior.AllowGet);
        return this.Json((object) new
        {
          IsSuccess = document
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult GenerateRenewalExemptionLetters(int tableId, int templateId)
    {
      string destinationFileBaseName = this._templateTypeService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("Id == " + (object) 7, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      }, "LU_Template").First<LU_TemplateType>().DestinationFileBaseName;
      IEnumerable<LU_Template> range = this._templateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Id == " + (object) templateId, new object[0])
      }, (string) null);
      try
      {
        IEnumerable<int> sessionVar = SessionHandler.GetSessionVar<IEnumerable<int>>(SessionVariables.PermitIds);
        if (sessionVar == null)
          return this.Json((object) new
          {
            IsSuccess = false,
            Error = "Your search results expired. Please search again before generating letters."
          }, JsonRequestBehavior.AllowGet);
        WordDocument documentGroup = this.GenerateDocumentGroup(range, sessionVar, destinationFileBaseName, false);
        if (!this._documentErrors.Any<string>())
          return this.Json((object) new
          {
            IsSuccess = true,
            FileId = documentGroup.DocumentByteId,
            FileTitle = documentGroup.Title
          }, JsonRequestBehavior.AllowGet);
        string str = string.Join(",", this._documentErrors.ToArray());
        this._documentErrors.Clear();
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = str
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult GenerateSelectedPumpageLetters(int tableId, int templateId)
    {
      string destinationFileBaseName = this._templateTypeService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("Id == " + (object) 8, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      }, "LU_Template").First<LU_TemplateType>().DestinationFileBaseName;
      IEnumerable<LU_Template> range = this._templateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Id == " + (object) templateId, new object[0])
      }, (string) null);
      try
      {
        IEnumerable<int> sessionVar = SessionHandler.GetSessionVar<IEnumerable<int>>(SessionVariables.PermitIds);
        if (sessionVar == null)
          return this.Json((object) new
          {
            IsSuccess = false,
            Error = "Your search results expired. Please search again before generating letters."
          }, JsonRequestBehavior.AllowGet);
        if (sessionVar.Count<int>() > 100)
          return this.Json((object) new
          {
            IsSuccess = false,
            Error = "You selected more than 100 permits to generate the letters for. Please use batch document generation under Reporting."
          }, JsonRequestBehavior.AllowGet);
        WordDocument documentGroup = this.GenerateDocumentGroup(range, sessionVar, destinationFileBaseName, false);
        if (!this._documentErrors.Any<string>())
          return this.Json((object) new
          {
            IsSuccess = true,
            FileId = documentGroup.DocumentByteId,
            FileTitle = documentGroup.Title
          }, JsonRequestBehavior.AllowGet);
        string str = string.Join(",", this._documentErrors.ToArray());
        this._documentErrors.Clear();
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = str
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult GenerateWithdrawalLetter(int permitId, int tableId, int permitStatusId, int templateId)
    {
      try
      {
        string destinationFileBaseName = this._templateTypeService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("Id == " + (object) 4, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, "LU_Template").First<LU_TemplateType>().DestinationFileBaseName;
        IEnumerable<LU_Template> range = this._templateService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("Id == " + (object) templateId, new object[0])
        }, (string) null);
        bool document = this.GenerateDocument(new WordDocument()
        {
          PermitId = permitId,
          TableId = tableId,
          PermitStatusId = new int?(permitStatusId),
          Description = "Generated by WSIPS",
          DocumentTypeId = new int?(4),
          Title = string.Format("{0}_{1}_{2}.docx", (object) destinationFileBaseName, (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm")),
          SaveToFileSystem = false,
          SaveToDatabase = true,
          TemplateTypeId = 4,
          Templates = range
        }, this._permittingService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties"), "blank", true);
        if (document || !this._documentErrors.Any<string>())
          return this.Json((object) new
          {
            IsSuccess = document
          }, JsonRequestBehavior.AllowGet);
        string str = string.Join(",", this._documentErrors.ToArray());
        this._documentErrors.Clear();
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = str
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult GenerateDecisionLetter(int permitId, int tableId, int permitStatusId, int templateId)
    {
      try
      {
        string destinationFileBaseName = this._templateTypeService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("Id == " + (object) 5, new object[0]),
          new DynamicFilter("Active == True", new object[0])
        }, "LU_Template").First<LU_TemplateType>().DestinationFileBaseName;
        List<LU_Template> list = this._templateService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("TemplateTypeId == " + (object) templateId, new object[0])
        }, (string) null).ToList<LU_Template>();
        WordDocument doc = new WordDocument()
        {
          PermitId = permitId,
          TableId = tableId,
          PermitStatusId = new int?(permitStatusId),
          Description = "Generated by WSIPS",
          DocumentTypeId = new int?(5),
          Title = string.Format("{0}_{1}_{2}.docx", (object) destinationFileBaseName, (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm")),
          SaveToFileSystem = false,
          SaveToDatabase = true,
          TemplateTypeId = 5,
          Templates = (IEnumerable<LU_Template>) list
        };
        Permit byId = this._permittingService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties");
        PermitWithdrawalSurfacewater withdrawalSurfacewater = this._permitWithdrawalSurfacewaterService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("PermitId == " + (object) byId.Id, new object[0])
        }, (string) null).FirstOrDefault<PermitWithdrawalSurfacewater>();
        doc.Templates = withdrawalSurfacewater == null ? list.Where<LU_Template>((Func<LU_Template, bool>) (t => t.FileName.Contains("Groundwater"))) : list.Where<LU_Template>((Func<LU_Template, bool>) (t => t.FileName.Contains("Surfacewater")));
        bool document = this.GenerateDocument(doc, byId, "blank", true);
        if (document || !this._documentErrors.Any<string>())
          return this.Json((object) new
          {
            IsSuccess = document
          }, JsonRequestBehavior.AllowGet);
        string str = string.Join(",", this._documentErrors.ToArray());
        this._documentErrors.Clear();
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = str
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual JsonResult GeneratePumpageLetters(int templateId)
    {
      try
      {
        this._documentJobService.Save(new DocumentJob()
        {
          TemplateId = new int?(templateId),
          ContactId = new int?(this._contactId),
          Status = new int?(0),
          Active = new bool?(true)
        });
        return this.Json((object) new
        {
          IsSuccess = true,
          Message = "Batch document generation is in process. You will be notified by email once the documents are ready for download."
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new
        {
          IsSuccess = false,
          Error = ex.Message
        }, JsonRequestBehavior.AllowGet);
      }
    }

    private bool GenerateDocument(WordDocument doc, Permit permit, string blank, bool pageBreaks = true)
    {
      bool flag = false;
      try
      {
        List<LU_Template> list = doc.Templates.ToList<LU_Template>();
        if (!list.Any<LU_Template>())
          return false;
        if (list.Count<LU_Template>() == 1)
        {
            byte[] buffer = System.IO.File.ReadAllBytes(this.TemplateFolder + list.First<LU_Template>().FileName);
          using (MemoryStream memoryStream = new MemoryStream())
          {
            memoryStream.Write(buffer, 0, buffer.Length);
            using (WordprocessingDocument documentFromTemplate = this._wordHelper.GetDocumentFromTemplate(memoryStream))
            {
              IDictionary<string, object> templateVariables = this._documentUtility.GetPermitTemplateVariables((IEnumerable<string>) this._wordHelper.GetAllContentControlTags(documentFromTemplate), permit, doc.Templates.Select<LU_Template, string>((Func<LU_Template, string>) (d => d.Description)).ToList<string>(), this.DigitalSignatureFolder, this._contactId, false);
              this.CheckForErrors(templateVariables);
              this._wordHelper.ReplaceContentControls(documentFromTemplate, templateVariables, false);
              documentFromTemplate.Close();
            }
            if (doc.SaveToFileSystem)
              flag = this.SaveToFileSystem(this.TemplateFolder + doc.Title, memoryStream.ToArray());
            if (doc.PermitStatusId.HasValue)
            {
              if (doc.SaveToDatabase)
                flag = this.SaveToDb(memoryStream.ToArray(), doc);
            }
          }
        }
        else
        {
          int num1 = 1;
          List<string> stringList = new List<string>();
          string str1 = string.Format("{0}{1}-temp.docx", (object) this.TemplateFolder, (object) permit.Id);
          foreach (LU_Template luTemplate in list)
          {
              byte[] buffer = System.IO.File.ReadAllBytes(this.TemplateFolder + luTemplate.FileName);
            using (MemoryStream memoryStream = new MemoryStream())
            {
              memoryStream.Write(buffer, 0, buffer.Length);
              using (WordprocessingDocument documentFromTemplate = this._wordHelper.GetDocumentFromTemplate(memoryStream))
              {
                IDictionary<string, object> templateVariables = this._documentUtility.GetPermitTemplateVariables((IEnumerable<string>) this._wordHelper.GetAllContentControlTags(documentFromTemplate), permit, doc.Templates.Where<LU_Template>((Func<LU_Template, bool>) (d => !d.Description.Contains("Cover Letter"))).Select<LU_Template, string>((Func<LU_Template, string>) (d => d.Description)).ToList<string>(), this.DigitalSignatureFolder, this._contactId, false);
                this.CheckForErrors(templateVariables);
                this._wordHelper.ReplaceContentControls(documentFromTemplate, templateVariables, false);
                documentFromTemplate.Close();
              }
              doc.SaveToFileSystem = true;
              doc.SaveToLocation = string.Format("{0}{1}-{2}.docx", (object) this.TemplateFolder, (object) permit.Id, (object) num1);
              flag = this.SaveToFileSystem(doc.SaveToLocation, memoryStream.ToArray());
              stringList.Add(string.Format("{0}-{1}.docx", (object) permit.Id, (object) num1));
            }
            ++num1;
          }
          doc.SaveToFileSystem = false;
          System.IO.File.Copy(this.TemplateFolder + blank + ".docx", str1);
          if (!pageBreaks)
            stringList.Reverse();
          int num2 = 1;
          foreach (string str2 in stringList)
          {
            using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(str1, true))
            {
              string id = "AltChunkId_" + (object) num2;
              MainDocumentPart mainDocumentPart = wordprocessingDocument.MainDocumentPart;
              using (FileStream fileStream = System.IO.File.Open(this.TemplateFolder + str2, FileMode.Open))
                mainDocumentPart.AddAlternativeFormatImportPart(AlternativeFormatImportPartType.WordprocessingML, id).FeedData((Stream) fileStream);
              AltChunk newChild1 = new AltChunk()
              {
                Id = (StringValue) id
              };
              mainDocumentPart.Document.Body.InsertAfter<AltChunk>(newChild1, (OpenXmlElement) mainDocumentPart.Document.Body.Elements<Paragraph>().Last<Paragraph>());
              if (pageBreaks)
              {
                Paragraph newChild2 = new Paragraph(new OpenXmlElement[1]
                {
                  (OpenXmlElement) new Run(new OpenXmlElement[1]
                  {
                    (OpenXmlElement) new Break()
                    {
                      Type = (EnumValue<BreakValues>) BreakValues.Page
                    }
                  })
                });
                if (num2 == stringList.Count)
                  newChild2 = new Paragraph(new OpenXmlElement[1]
                  {
                    (OpenXmlElement) new Run()
                  });
                mainDocumentPart.Document.Body.InsertAfter<Paragraph>(newChild2, mainDocumentPart.Document.Body.LastChild);
              }
              mainDocumentPart.Document.Save();
            }
            ++num2;
          }
          byte[] buffer1 = System.IO.File.ReadAllBytes(str1);
          using (MemoryStream memoryStream = new MemoryStream())
          {
            memoryStream.Write(buffer1, 0, buffer1.Length);
            using (WordprocessingDocument documentFromTemplate = this._wordHelper.GetDocumentFromTemplate(memoryStream))
            {
              IDictionary<string, object> templateVariables = this._documentUtility.GetPermitTemplateVariables((IEnumerable<string>) this._wordHelper.GetAllContentControlTags(documentFromTemplate), permit, doc.Templates.Select<LU_Template, string>((Func<LU_Template, string>) (d => d.Description)).ToList<string>(), this.DigitalSignatureFolder, this._contactId, false);
              this.CheckForErrors(templateVariables);
              this._wordHelper.ReplaceContentControls(documentFromTemplate, templateVariables, false);
              documentFromTemplate.Close();
            }
            if (doc.SaveToFileSystem)
              flag = this.SaveToFileSystem(this.TemplateFolder + doc.Title, memoryStream.ToArray());
            if (doc.PermitStatusId.HasValue)
            {
              if (doc.SaveToDatabase)
                flag = this.SaveToDb(memoryStream.ToArray(), doc);
            }
          }
          foreach (string str2 in stringList)
              System.IO.File.Delete(this.TemplateFolder + str2);
          System.IO.File.Delete(str1);
        }
      }
      catch (Exception ex)
      {
        flag = false;
        if (ex.GetType() == typeof (NullReferenceException))
          this._documentErrors.Add("The document cannot be generated because of missing data.");
        else
          this._documentErrors.Add("Error: " + ex.Message);
        this.HandleException(ex);
      }
      return flag;
    }

    private WordDocument GenerateDocumentGroup(IEnumerable<LU_Template> templates, IEnumerable<int> permitIds, string baseFileName, bool saveToFileSystem = false)
    {
        _documentErrors.Clear();
        try
        {
            var i = 1;
            var docs = new List<String>();
            var docsArray = new List<byte[]>();
            var tempFile = String.Format("{0}{1}-temp.docx", TemplateFolder, DateTime.Now.ToString("yyyyMMddhhmm"));

            foreach (var permitId in permitIds)
            {
                var permit = _permittingService.GetById(permitId, "PermitContacts.Contact, LU_PermitStatus, PermitCounties");

                var luTemplates = templates as IList<LU_Template> ?? templates.ToList();
                foreach (var buffer in luTemplates.Select(template => System.IO.File.ReadAllBytes(TemplateFolder + template.FileName)))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        memoryStream.Write(buffer, 0, buffer.Length);

                        // Create a Word document from the template
                        using (var document = _wordHelper.GetDocumentFromTemplate(memoryStream))
                        {
                            // Get the variable names from the document template
                            IEnumerable<string> variableNames = _wordHelper.GetAllContentControlTags(document);

                            var doc = new WordDocument();

                            // Get the variables for the document
                            var variables = _documentUtility.GetPermitTemplateVariables(variableNames, permit, null,
                                                                                        DigitalSignatureFolder,
                                                                                        _contactId);
                            CheckForErrors(variables);

                            // Replace the content controls in the document with the variable values
                            _wordHelper.ReplaceContentControls(document, variables);

                            document.Close();

                            doc.SaveToFileSystem = true;
                            doc.SaveToLocation = String.Format("{0}{1}-{2}.docx", TemplateFolder, permit.Id, i);
                            SaveToFileSystem(doc.SaveToLocation, memoryStream.ToArray());
                        }

                        docs.Add(String.Format("{0}-{1}.docx", permit.Id, i));
                        docsArray.Add(memoryStream.ToArray());
                    }

                    i++;
                }
            }

            // Prepare blank file for adding new document chunks
            System.IO.File.Copy(TemplateFolder + "blank.docx", tempFile);

            var title = String.Format("{0}_{1}_{2}.docx", baseFileName, DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("hhmm"));

            // Clean up
            foreach (var item in docs) System.IO.File.Delete(TemplateFolder + item);

            // Delete temp file
            System.IO.File.Delete(tempFile);
            var byteArray = _wordHelper.OpenAndCombine(docsArray);

            var groupDoc = new WordDocument
            {
                Title = title,
                DocumentByteId = SaveByteArrayToDb(byteArray, "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            };
            if (saveToFileSystem) SaveToFileSystem(TemplateFolder + title, byteArray);
            return groupDoc;
        }
        catch (Exception exc)
        {
            _documentErrors.Add(exc.Message);
            HandleException(exc);
        }
        return null;
    }

    //private WordDocument GenerateDocumentGroup(IEnumerable<LU_Template> templates, IEnumerable<int> permitIds, string baseFileName, bool saveToFileSystem = false)
    //{
    //  this._documentErrors.Clear();
    //  try
    //  {
    //    int num = 1;
    //    List<string> stringList = new List<string>();
    //    List<byte[]> numArrayList = new List<byte[]>();
    //    string str1 = string.Format("{0}{1}-temp.docx", (object) this.TemplateFolder, (object) DateTime.Now.ToString("yyyyMMddhhmm"));
    //    Dictionary<string, string> source = new Dictionary<string, string>();
    //    foreach (int permitId in permitIds)
    //    {
    //      Permit byId = this._permittingService.GetById(permitId, "PermitContacts.Contact, PermitContacts.PermitContactInformation, LU_PermitStatus, PermitCounties");
    //      PermitContact permitContact = byId.PermitContacts.FirstOrDefault<PermitContact>((Func<PermitContact, bool>) (pc =>
    //      {
    //        if (pc.IsPermittee)
    //          return pc.IsPrimary;
    //        return false;
    //      }));
    //      if (permitContact != null && permitContact.PermitContactInformation != null)
    //      {
    //        if (permitContact.PermitContactInformation.IsBusiness && !string.IsNullOrEmpty(permitContact.PermitContactInformation.BusinessName))
    //          source.Add(byId.PermitName, permitContact.PermitContactInformation.BusinessName);
    //        else if (!string.IsNullOrEmpty(permitContact.PermitContactInformation.FirstName) || !string.IsNullOrEmpty(permitContact.PermitContactInformation.LastName))
    //          source.Add(byId.PermitName, string.Format("{0} {1}", (object) permitContact.PermitContactInformation.FirstName, (object) permitContact.PermitContactInformation.LastName));
    //      }
    //      else
    //        source.Add(byId.PermitName, "No Permittee");
    //      foreach (byte[] buffer in (templates as IList<LU_Template> ?? (IList<LU_Template>)templates.ToList<LU_Template>()).Select<LU_Template, byte[]>((Func<LU_Template, byte[]>)(template => System.IO.File.ReadAllBytes(this.TemplateFolder + template.FileName))))
    //      {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        {
    //          memoryStream.Write(buffer, 0, buffer.Length);
    //          using (WordprocessingDocument documentFromTemplate = this._wordHelper.GetDocumentFromTemplate(memoryStream))
    //          {
    //            IEnumerable<string> contentControlTags = (IEnumerable<string>) this._wordHelper.GetAllContentControlTags(documentFromTemplate);
    //            WordDocument wordDocument = new WordDocument();
    //            IDictionary<string, object> templateVariables = this._documentUtility.GetPermitTemplateVariables(contentControlTags, byId, (List<string>) null, this.DigitalSignatureFolder, this._contactId, false);
    //            this.CheckForErrors(templateVariables);
    //            this._wordHelper.ReplaceContentControls(documentFromTemplate, templateVariables, false);
    //            documentFromTemplate.Close();
    //            wordDocument.SaveToFileSystem = false;
    //            wordDocument.SaveToLocation = string.Format("{0}{1}-{2}.docx", (object) this.TemplateFolder, (object) byId.Id, (object) num);
    //            this.SaveToFileSystem(wordDocument.SaveToLocation, memoryStream.ToArray());
    //          }
    //          stringList.Add(string.Format("{0}-{1}.docx", (object) byId.Id, (object) num));
    //          numArrayList.Add(memoryStream.ToArray());
    //        }
    //        ++num;
    //      }
    //    }
    //    using (MemoryStream memoryStream = new MemoryStream())
    //    {
    //        using (WordprocessingDocument newDocument = this._wordHelper.GetDocument(memoryStream))
    //        {
    //            newDocument.AddMainDocumentPart();
    //            newDocument.MainDocumentPart.Document = new DocumentFormat.OpenXml.Wordprocessing.Document();
    //            List<Paragraph> paragraphList = new List<Paragraph>();
    //            paragraphList.Add(new Paragraph(new OpenXmlElement[1]
    //        {
    //          (OpenXmlElement) new Run(new OpenXmlElement[1]
    //          {
    //            (OpenXmlElement) new Break()
    //            {
    //              Type = (EnumValue<BreakValues>) BreakValues.Page
    //            }
    //          })
    //        }));
    //            paragraphList.AddRange(Enumerable.Range(0, 4).Select<int, Paragraph>((Func<int, Paragraph>)(t => new Paragraph())));
    //            paragraphList.AddRange(source.Select<KeyValuePair<string, string>, Paragraph>((Func<KeyValuePair<string, string>, Paragraph>)(t => new Paragraph(new OpenXmlElement[1]
    //        {
    //          (OpenXmlElement) new Run(new OpenXmlElement[1]
    //          {
    //            (OpenXmlElement) new DocumentFormat.OpenXml.Wordprocessing.Text(string.Format("{0} {1}", (object) t.Key, (object) t.Value))
    //          })
    //        }))));
    //            Body body = new Body();
    //            body.Append((IEnumerable<OpenXmlElement>)paragraphList);
    //            newDocument.MainDocumentPart.Document.Append(new OpenXmlElement[1]
    //        {
    //          (OpenXmlElement) body
    //        });
    //        }
    //        numArrayList.Add(memoryStream.ToArray());
    //    }
    //    System.IO.File.Copy(this.TemplateFolder + "blank.docx", str1);
    //    string str2 = string.Format("{0}_{1}_{2}.docx", (object) baseFileName, (object) DateTime.Now.ToString("yyyyMMdd"), (object) DateTime.Now.ToString("hhmm"));
    //    foreach (string str3 in stringList)
    //        System.IO.File.Delete(this.TemplateFolder + str3);
    //    System.IO.File.Delete(str1);
    //    byte[] buffer1 = this._wordHelper.OpenAndCombine((IList<byte[]>) numArrayList);
    //    WordDocument wordDocument1 = new WordDocument()
    //    {
    //      Title = str2,
    //      DocumentByteId = this.SaveByteArrayToDb(buffer1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
    //    };
    //    if (saveToFileSystem)
    //      this.SaveToFileSystem(this.TemplateFolder + str2, buffer1);
    //    return wordDocument1;
    //  }
    //  catch (Exception ex)
    //  {
    //    this._documentErrors.Add(ex.Message);
    //    this.HandleException(ex);
    //  }
    //  return (WordDocument) null;
    //}

    private void FinalizeDocumentPart(WordprocessingDocument document, Permit permit, bool approved)
    {
      IDictionary<string, object> templateVariables = this._documentUtility.GetPermitTemplateVariables((IEnumerable<string>) this._wordHelper.GetAllContentControlTags(document), permit, (List<string>) null, this.DigitalSignatureFolder, this._contactId, approved);
      this.CheckForErrors(templateVariables);
      templateVariables.Remove("ChiefSignatureImd");
      this._wordHelper.ReplaceContentControls(document, templateVariables, false);
      document.MainDocumentPart.Document.Save();
      string labelPattern1 = DocumentController.CreateLabelPattern("EFFECTIVE DATE");
      string labelPattern2 = DocumentController.CreateLabelPattern("EXPIRATION DATE");
      string labelPattern3 = DocumentController.CreateLabelPattern("FIRST APPROPRIATION");
      string datePattern = DocumentController.CreateDatePattern();
      if (permit.EffectiveDate.HasValue)
        this._wordHelper.ReplaceBodyPattern(document, labelPattern1 + datePattern, labelPattern1 + Formatter.FormatDate(permit.EffectiveDate.Value));
      if (permit.ExpirationDate.HasValue)
        this._wordHelper.ReplaceBodyPattern(document, labelPattern2 + datePattern, labelPattern2 + Formatter.FormatDate(permit.ExpirationDate.Value));
      if (permit.AppropriationDate.HasValue)
        this._wordHelper.ReplaceBodyPattern(document, labelPattern3 + datePattern, labelPattern3 + Formatter.FormatDate(permit.AppropriationDate.Value));
      document.MainDocumentPart.Document.Save();
    }

    private void InsertApprovalSignature(WordprocessingDocument document, Permit permit, bool approved)
    {
      IDictionary<string, object> templateVariables = this._documentUtility.GetPermitTemplateVariables((IEnumerable<string>) this._wordHelper.GetAllContentControlTags(document), permit, (List<string>) null, this.DigitalSignatureFolder, this._contactId, approved);
      if (!templateVariables.ContainsKey("ChiefSignatureImd") && !templateVariables.ContainsKey("for"))
        return;
      Dictionary<string, object> dictionary = templateVariables.Where<KeyValuePair<string, object>>((Func<KeyValuePair<string, object>, bool>) (k =>
      {
        if (!(k.Key == "ChiefSignatureImd"))
          return k.Key == "for";
        return true;
      })).ToDictionary<KeyValuePair<string, object>, string, object>((Func<KeyValuePair<string, object>, string>) (o => o.Key), (Func<KeyValuePair<string, object>, object>) (o => o.Value));
      this._wordHelper.ReplaceContentControls(document, (IDictionary<string, object>) dictionary, false);
      document.MainDocumentPart.Document.Save();
    }

    private bool SaveToDb(byte[] buffer, WordDocument document)
    {
      MDGov.MDE.Permitting.Model.Document entity1 = new MDGov.MDE.Permitting.Model.Document()
      {
        DocumentTitle = document.Title,
        Description = document.Description,
        DocumentTypeId = document.DocumentTypeId,
        PermitStatusId = document.PermitStatusId,
        RefTableId = document.TableId,
        RefId = document.PermitId
      };
      DocumentByte entity2 = new DocumentByte()
      {
        Document = buffer,
        MimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
      };
      try
      {
        int num = this._documentByteService.Save(entity2);
        entity1.DocumentByteId = num;
        document.DocumentByteId = num;
        this._documentService.Save(entity1);
        return true;
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return false;
      }
    }

    private int SaveByteArrayToDb(byte[] buffer, string mimeType)
    {
      DocumentByte entity = new DocumentByte()
      {
        Document = buffer,
        MimeType = mimeType
      };
      try
      {
        return this._documentByteService.Save(entity);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return 0;
      }
    }

    private bool SaveToFileSystem(string fileName, byte[] buffer)
    {
      try
      {
          System.IO.File.WriteAllBytes(fileName, buffer);
        return true;
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return false;
      }
    }

    private static string CreateDatePattern()
    {
      return new Regex("(?<date>(" + string.Join("|", new CultureInfo("en-US").DateTimeFormat.MonthGenitiveNames, 0, 12) + ") \\d{2}, \\d{4})").ToString();
    }

    private static string CreateLabelPattern(string label)
    {
      return string.Format("{0}{1}", (object) label, (object) ": </w:t></w:r><w:r><w:rPr><w:rPr><w:rFonts w:cs=\"Courier New\" /><w:szCs w:val=\"20\" /></w:rPr></w:rPr><w:t>");
    }

    private void CheckForErrors(IDictionary<string, object> coll)
    {
      if (!coll.ContainsKey("Errors"))
        return;
      if (coll["Errors"] is string)
      {
        this._documentErrors.Add(coll["Errors"].ToString());
      }
      else
      {
        foreach (string str in coll.Where<KeyValuePair<string, object>>((Func<KeyValuePair<string, object>, bool>) (e => e.Key == "Errors")).Select<KeyValuePair<string, object>, List<string>>((Func<KeyValuePair<string, object>, List<string>>) (variable => (List<string>) variable.Value)).SelectMany<List<string>, string>((Func<List<string>, IEnumerable<string>>) (errors => (IEnumerable<string>) errors)))
          this._documentErrors.Add(str);
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult TemplatesCount()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.TemplatesCount, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult GetTemplates()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.GetTemplates, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult GetDocumentParts()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.GetDocumentParts, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult GenerateDraftPermit()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateDraftPermit, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GenerateFinalPermit()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateFinalPermit, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GenerateCustomPackage()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateCustomPackage, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Generate6MonthLetter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Generate6MonthLetter, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult GenerateRenewalExemptionLetters()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateRenewalExemptionLetters, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult GenerateSelectedPumpageLetters()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateSelectedPumpageLetters, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult GenerateWithdrawalLetter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateWithdrawalLetter, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult GenerateDecisionLetter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GenerateDecisionLetter, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GeneratePumpageLetters()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GeneratePumpageLetters, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string TemplatesCount = "TemplatesCount";
      public readonly string GetTemplates = "GetTemplates";
      public readonly string GetDocumentParts = "GetDocumentParts";
      public readonly string GenerateDraftPermit = "GenerateDraftPermit";
      public readonly string GenerateFinalPermit = "GenerateFinalPermit";
      public readonly string GenerateCustomPackage = "GenerateCustomPackage";
      public readonly string Generate6MonthLetter = "Generate6MonthLetter";
      public readonly string GenerateRenewalExemptionLetters = "GenerateRenewalExemptionLetters";
      public readonly string GenerateSelectedPumpageLetters = "GenerateSelectedPumpageLetters";
      public readonly string GenerateWithdrawalLetter = "GenerateWithdrawalLetter";
      public readonly string GenerateDecisionLetter = "GenerateDecisionLetter";
      public readonly string GeneratePumpageLetters = "GeneratePumpageLetters";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNameConstants
    {
      public const string TemplatesCount = "TemplatesCount";
      public const string GetTemplates = "GetTemplates";
      public const string GetDocumentParts = "GetDocumentParts";
      public const string GenerateDraftPermit = "GenerateDraftPermit";
      public const string GenerateFinalPermit = "GenerateFinalPermit";
      public const string GenerateCustomPackage = "GenerateCustomPackage";
      public const string Generate6MonthLetter = "Generate6MonthLetter";
      public const string GenerateRenewalExemptionLetters = "GenerateRenewalExemptionLetters";
      public const string GenerateSelectedPumpageLetters = "GenerateSelectedPumpageLetters";
      public const string GenerateWithdrawalLetter = "GenerateWithdrawalLetter";
      public const string GenerateDecisionLetter = "GenerateDecisionLetter";
      public const string GeneratePumpageLetters = "GeneratePumpageLetters";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_TemplatesCount
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetTemplates
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetDocumentParts
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GenerateDraftPermit
    {
      public readonly string permitId = "permitId";
      public readonly string tableId = "tableId";
      public readonly string permitStatusId = "permitStatusId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GenerateFinalPermit
    {
      public readonly string permitId = "permitId";
      public readonly string tableId = "tableId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string approved = "approved";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GenerateCustomPackage
    {
      public readonly string permitId = "permitId";
      public readonly string tableId = "tableId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string templateIds = "templateIds";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Generate6MonthLetter
    {
      public readonly string permitId = "permitId";
      public readonly string tableId = "tableId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string templateId = "templateId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GenerateRenewalExemptionLetters
    {
      public readonly string tableId = "tableId";
      public readonly string templateId = "templateId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GenerateSelectedPumpageLetters
    {
      public readonly string tableId = "tableId";
      public readonly string templateId = "templateId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GenerateWithdrawalLetter
    {
      public readonly string permitId = "permitId";
      public readonly string tableId = "tableId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string templateId = "templateId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GenerateDecisionLetter
    {
      public readonly string permitId = "permitId";
      public readonly string tableId = "tableId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string templateId = "templateId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GeneratePumpageLetters
    {
      public readonly string templateId = "templateId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly DocumentController.ViewsClass._ViewNamesClass s_ViewNames = new DocumentController.ViewsClass._ViewNamesClass();
      public readonly string _SelectDocument6MonthsParts = "~/Areas/Permitting/Views/Document/_SelectDocument6MonthsParts.cshtml";
      public readonly string _SelectDocumentWithdrawalParts = "~/Areas/Permitting/Views/Document/_SelectDocumentWithdrawalParts.cshtml";
      public readonly string _SelectPumpageTemplates = "~/Areas/Permitting/Views/Document/_SelectPumpageTemplates.cshtml";
      public readonly string _SelectRenewalOrExemptionTemplates = "~/Areas/Permitting/Views/Document/_SelectRenewalOrExemptionTemplates.cshtml";
      public readonly string _SelectTemplates = "~/Areas/Permitting/Views/Document/_SelectTemplates.cshtml";

      public DocumentController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return DocumentController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string _SelectDocument6MonthsParts = "_SelectDocument6MonthsParts";
        public readonly string _SelectDocumentWithdrawalParts = "_SelectDocumentWithdrawalParts";
        public readonly string _SelectPumpageTemplates = "_SelectPumpageTemplates";
        public readonly string _SelectRenewalOrExemptionTemplates = "_SelectRenewalOrExemptionTemplates";
        public readonly string _SelectTemplates = "_SelectTemplates";
      }
    }
  }
}
