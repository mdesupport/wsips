﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.ChecklistController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization(AuthorizedRoles = new string[] {"IT Administrator", "Administrative Specialist", "Compliance / Enforcement Manager", "Compliance / Enforcement Staff", "Division Chief", "WSIPS eCommerce Member", "Operational Administrator", "Permit Supervisor", "Project Manager", "Secretary"})]
  public class ChecklistController : BaseController
  {
    private static readonly ChecklistController.ActionNamesClass s_actions = new ChecklistController.ActionNamesClass();
    private static readonly ChecklistController.ActionParamsClass_Data s_params_Data = new ChecklistController.ActionParamsClass_Data();
    private static readonly ChecklistController.ActionParamsClass_StepsData s_params_StepsData = new ChecklistController.ActionParamsClass_StepsData();
    private static readonly ChecklistController.ActionParamsClass_GetPermitStatuses s_params_GetPermitStatuses = new ChecklistController.ActionParamsClass_GetPermitStatuses();
    private static readonly ChecklistController.ActionParamsClass_Edit s_params_Edit = new ChecklistController.ActionParamsClass_Edit();
    private static readonly ChecklistController.ActionParamsClass_EditStep s_params_EditStep = new ChecklistController.ActionParamsClass_EditStep();
    private static readonly ChecklistController.ActionParamsClass_Activate s_params_Activate = new ChecklistController.ActionParamsClass_Activate();
    private static readonly ChecklistController.ActionParamsClass_DeleteStep s_params_DeleteStep = new ChecklistController.ActionParamsClass_DeleteStep();
    private static readonly ChecklistController.ActionParamsClass_SearchFilter s_params_SearchFilter = new ChecklistController.ActionParamsClass_SearchFilter();
    private static readonly ChecklistController.ActionParamsClass_UpdateAnswer s_params_UpdateAnswer = new ChecklistController.ActionParamsClass_UpdateAnswer();
    private static readonly ChecklistController.ActionParamsClass_UpdateSequence s_params_UpdateSequence = new ChecklistController.ActionParamsClass_UpdateSequence();
    private static readonly ChecklistController.ViewsClass s_views = new ChecklistController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Permitting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Checklist";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Checklist";
    private readonly IService<LU_PermitCategory> _permitCategoryService;
    private readonly IUpdatableService<CheckList> _checklistService;
    private readonly IService<LU_PermitStatus> _permitStatusService;
    private readonly IUpdatableService<CheckListQuestion> _checklistStepService;
    private readonly IUpdatableService<CheckListAnswer> _checkListAnswerService;

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ChecklistController Actions
    {
      get
      {
        return MVC.Permitting.Checklist;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ChecklistController.ActionNamesClass ActionNames
    {
      get
      {
        return ChecklistController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ChecklistController.ActionParamsClass_Data DataParams
    {
      get
      {
        return ChecklistController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ChecklistController.ActionParamsClass_StepsData StepsDataParams
    {
      get
      {
        return ChecklistController.s_params_StepsData;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ChecklistController.ActionParamsClass_GetPermitStatuses GetPermitStatusesParams
    {
      get
      {
        return ChecklistController.s_params_GetPermitStatuses;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ChecklistController.ActionParamsClass_Edit EditParams
    {
      get
      {
        return ChecklistController.s_params_Edit;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ChecklistController.ActionParamsClass_EditStep EditStepParams
    {
      get
      {
        return ChecklistController.s_params_EditStep;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ChecklistController.ActionParamsClass_Activate ActivateParams
    {
      get
      {
        return ChecklistController.s_params_Activate;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ChecklistController.ActionParamsClass_DeleteStep DeleteStepParams
    {
      get
      {
        return ChecklistController.s_params_DeleteStep;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ChecklistController.ActionParamsClass_SearchFilter SearchFilterParams
    {
      get
      {
        return ChecklistController.s_params_SearchFilter;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ChecklistController.ActionParamsClass_UpdateAnswer UpdateAnswerParams
    {
      get
      {
        return ChecklistController.s_params_UpdateAnswer;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ChecklistController.ActionParamsClass_UpdateSequence UpdateSequenceParams
    {
      get
      {
        return ChecklistController.s_params_UpdateSequence;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ChecklistController.ViewsClass Views
    {
      get
      {
        return ChecklistController.s_views;
      }
    }

    public ChecklistController(IService<LU_PermitCategory> permitCategoryService, IUpdatableService<CheckList> checklistService, IService<LU_PermitStatus> permitStatusService, IUpdatableService<CheckListQuestion> checklistStepService, IUpdatableService<CheckListAnswer> checkListAnswerService)
    {
      this._permitCategoryService = permitCategoryService;
      this._checklistService = checklistService;
      this._permitStatusService = permitStatusService;
      this._checklistStepService = checklistStepService;
      this._checkListAnswerService = checkListAnswerService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected ChecklistController(Dummy d)
    {
    }

    public virtual ActionResult Index()
    {
      this.GetPermitCategories(0);
      return (ActionResult) this.View();
    }

    [HttpGet]
    public virtual JsonResult Data(GridCommand command)
    {
      ChecklistSearchForm checklistSearchForm = SessionHandler.GetSessionVar<ChecklistSearchForm>(SessionVariables.ChecklistSearchForm);
      if (checklistSearchForm == null)
        checklistSearchForm = new ChecklistSearchForm()
        {
          PermitCategoryId = new int?(2)
        };
      ChecklistSearchForm searchForm = checklistSearchForm;
      List<ChecklistListItem> list1 = Mapper.Map<IEnumerable<CheckList>, IEnumerable<ChecklistListItem>>(this._checklistService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) null, "LU_PermitStatus, CheckListQuestions").Where<CheckList>((Func<CheckList, bool>) (c =>
      {
        int? permitCategoryId1 = c.LU_PermitStatus.PermitCategoryId;
        int? permitCategoryId2 = searchForm.PermitCategoryId;
        if (permitCategoryId1.GetValueOrDefault() == permitCategoryId2.GetValueOrDefault())
          return permitCategoryId1.HasValue == permitCategoryId2.HasValue;
        return false;
      }))).ToList<ChecklistListItem>();
      int num1 = list1.Count<ChecklistListItem>();
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      List<ChecklistListItem> list2 = list1.Skip<ChecklistListItem>(command.PageSize * (command.Page - 1)).Take<ChecklistListItem>(command.PageSize).ToList<ChecklistListItem>();
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) list2,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult StepsData(GridCommand command, int checklistId)
    {
      List<ChecklistStep> list1 = Mapper.Map<IEnumerable<CheckListQuestion>, IEnumerable<ChecklistStep>>(this._checklistStepService.GetRange(0, int.MaxValue, "QuestionNumber", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("CheckListId == " + (object) checklistId, new object[0])
      }, (string) null)).ToList<ChecklistStep>();
      int num1 = list1.Count<ChecklistStep>();
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      List<ChecklistStep> list2 = list1.Skip<ChecklistStep>(command.PageSize * (command.Page - 1)).Take<ChecklistStep>(command.PageSize).ToList<ChecklistStep>();
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) list2,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetPermitStatuses(int categoryId)
    {
      IEnumerable<LU_PermitStatus> range = this._permitStatusService.GetRange(0, int.MaxValue, "Description", (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("PermitCategoryId == " + (object) categoryId, new object[0]),
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) range
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual ActionResult Edit(int id)
    {
            var checklist = _checklistService.GetById(id, "LU_PermitStatus");
            var checklistItem = Mapper.Map<CheckList, ChecklistListItem>(checklist);

            if (id == 0)
            {
                GetPermitCategories(0);
            }
            else
            {
                GetPermitCategories(checklistItem.PermitCategoryId);
                checklistItem.Category = ViewBag.PermitCategory;
            }

            return View(checklistItem ?? new ChecklistListItem { Id = 0 });
    }

    [HttpPost]
    public virtual ActionResult Edit(ChecklistListItem checkListItem)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          CheckList entity = Mapper.Map<ChecklistListItem, CheckList>(checkListItem);
          checkListItem.Id = this._checklistService.Save(entity);
        }
        return (ActionResult) this.RedirectToAction("Edit", (object) new
        {
          id = checkListItem.Id
        });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return (ActionResult) this.View((object) checkListItem);
      }
    }

    [HttpGet]
    public virtual ActionResult EditStep(int id)
    {
      ChecklistStep checklistStep1 = Mapper.Map<CheckListQuestion, ChecklistStep>(this._checklistStepService.GetById(id, (string) null));
      string viewName = "_CreateOrEditStep";
      ChecklistStep checklistStep2 = checklistStep1;
      if (checklistStep2 == null)
        checklistStep2 = new ChecklistStep()
        {
          QuestionId = 0
        };
      return (ActionResult) this.PartialView(viewName, (object) checklistStep2);
    }

    [HttpPost]
    public virtual JsonResult EditStep(ChecklistStep step)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return this.Json((object) new
          {
            IsSuccess = false,
            Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)).ToList<string>()
          }, JsonRequestBehavior.AllowGet);
        CheckListQuestion entity = Mapper.Map<ChecklistStep, CheckListQuestion>(step);
        List<int?> list = this._checklistStepService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("CheckListId == " + (object) step.CheckListId, new object[0])
        }, (string) null).Select<CheckListQuestion, int?>((Func<CheckListQuestion, int?>) (c => c.QuestionNumber)).ToList<int?>();
        CheckListQuestion checkListQuestion = entity;
        int? nullable1;
        if (list.Max().HasValue)
        {
          int? nullable2 = list.Max();
          nullable1 = nullable2.HasValue ? new int?(nullable2.GetValueOrDefault() + 1) : new int?();
        }
        else
          nullable1 = new int?(1);
        checkListQuestion.QuestionNumber = nullable1;
        this._checklistStepService.Save(entity);
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual ActionResult Activate(int id)
    {
      try
      {
        CheckList byId = this._checklistService.GetById(id, "LU_PermitStatus");
        int? permitStatusId = byId.PermitStatusId;
        int? permitCategoryId = byId.LU_PermitStatus.PermitCategoryId;
        foreach (CheckList entity in this._checklistService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("PermitStatusId == " + (object) permitStatusId, new object[0]),
          new DynamicFilter("LU_PermitStatus.PermitCategoryId == " + (object) permitCategoryId, new object[0])
        }, "LU_PermitStatus"))
        {
          entity.Active = false;
          this._checklistService.Save(entity);
        }
        byId.Active = true;
        this._checklistService.Save(byId);
        return (ActionResult) this.RedirectToAction("Index");
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return (ActionResult) this.View();
      }
    }

    [HttpGet]
    public virtual ActionResult DeleteStep(int id)
    {
      try
      {
        foreach (CheckListAnswer checkListAnswer in this._checkListAnswerService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("CheckListQuestionId == " + (object) id, new object[0])
        }, (string) null))
          this._checkListAnswerService.Delete(checkListAnswer.Id);
        this._checklistStepService.Delete(id);
        return (ActionResult) this.RedirectToAction("Index");
      }
      catch
      {
        return (ActionResult) this.View();
      }
    }

    [HttpPost]
    public virtual JsonResult SearchFilter(ChecklistSearchForm form)
    {
      SessionHandler.SetSessionVar(SessionVariables.ChecklistSearchForm, (object) form);
      return this.Json((object) new
      {
        IsSaveSearchResult = false,
        IsSuccess = true
      });
    }

    [HttpPost]
    public virtual JsonResult UpdateAnswer(CheckListAnswer answer)
    {
      foreach (CheckListAnswer checkListAnswer in this._checkListAnswerService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("CheckListQuestionId == " + (object) answer.CheckListQuestionId, new object[0]),
        new DynamicFilter("PermitId == " + (object) answer.PermitId, new object[0])
      }, (string) null))
        this._checkListAnswerService.Delete(checkListAnswer.Id);
      if (answer.AnswerId != 0)
        this._checkListAnswerService.Save(answer);
      return this.Json((object) new{ IsSuccess = true });
    }

    [HttpPost]
    public virtual JsonResult UpdateSequence(int[] ids)
    {
      int num = 1;
      foreach (CheckListQuestion entity in ((IEnumerable<int>) ids).Select<int, CheckListQuestion>((Func<int, CheckListQuestion>) (id => this._checklistStepService.GetById(id, (string) null))))
      {
        entity.QuestionNumber = new int?(num);
        this._checklistStepService.Save(entity);
        ++num;
      }
      return this.Json((object) new{ IsSuccess = true });
    }

    private void GetPermitCategories(int categoryId)
    {
      IEnumerable<LU_PermitCategory> range = this._permitCategoryService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("Description.Contains(@0)", new object[1]
        {
          (object) "Pending"
        }),
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
      ViewBag.PermitCategories = range;
      if (categoryId == 0)
        return;
      LU_PermitCategory luPermitCategory = range.SingleOrDefault<LU_PermitCategory>((Func<LU_PermitCategory, bool>) (c => c.Id == categoryId));
      if (luPermitCategory == null)
        return;
      ViewBag.PermitCategory = luPermitCategory.Description;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult StepsData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.StepsData, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GetPermitStatuses()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPermitStatuses, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult Edit()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult EditStep()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditStep, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult Activate()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Activate, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult DeleteStep()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.DeleteStep, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult SearchFilter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchFilter, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult UpdateAnswer()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateAnswer, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult UpdateSequence()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateSequence, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string Data = "Data";
      public readonly string StepsData = "StepsData";
      public readonly string GetPermitStatuses = "GetPermitStatuses";
      public readonly string Edit = "Edit";
      public readonly string EditStep = "EditStep";
      public readonly string Activate = "Activate";
      public readonly string DeleteStep = "DeleteStep";
      public readonly string SearchFilter = "SearchFilter";
      public readonly string UpdateAnswer = "UpdateAnswer";
      public readonly string UpdateSequence = "UpdateSequence";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string Data = "Data";
      public const string StepsData = "StepsData";
      public const string GetPermitStatuses = "GetPermitStatuses";
      public const string Edit = "Edit";
      public const string EditStep = "EditStep";
      public const string Activate = "Activate";
      public const string DeleteStep = "DeleteStep";
      public const string SearchFilter = "SearchFilter";
      public const string UpdateAnswer = "UpdateAnswer";
      public const string UpdateSequence = "UpdateSequence";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string command = "command";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_StepsData
    {
      public readonly string command = "command";
      public readonly string checklistId = "checklistId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetPermitStatuses
    {
      public readonly string categoryId = "categoryId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Edit
    {
      public readonly string id = "id";
      public readonly string checkListItem = "checkListItem";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_EditStep
    {
      public readonly string id = "id";
      public readonly string step = "step";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Activate
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_DeleteStep
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SearchFilter
    {
      public readonly string form = "form";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_UpdateAnswer
    {
      public readonly string answer = "answer";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_UpdateSequence
    {
      public readonly string ids = "ids";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly ChecklistController.ViewsClass._ViewNamesClass s_ViewNames = new ChecklistController.ViewsClass._ViewNamesClass();
      public readonly string _BackToChecklistsLink = "~/Areas/Permitting/Views/Checklist/_BackToChecklistsLink.cshtml";
      public readonly string _CreateOrEditStep = "~/Areas/Permitting/Views/Checklist/_CreateOrEditStep.cshtml";
      public readonly string Activate = "~/Areas/Permitting/Views/Checklist/Activate.cshtml";
      public readonly string DeleteStep = "~/Areas/Permitting/Views/Checklist/DeleteStep.cshtml";
      public readonly string Edit = "~/Areas/Permitting/Views/Checklist/Edit.cshtml";
      public readonly string Index = "~/Areas/Permitting/Views/Checklist/Index.cshtml";

      public ChecklistController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return ChecklistController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string _BackToChecklistsLink = "_BackToChecklistsLink";
        public readonly string _CreateOrEditStep = "_CreateOrEditStep";
        public readonly string Activate = "Activate";
        public readonly string DeleteStep = "DeleteStep";
        public readonly string Edit = "Edit";
        public readonly string Index = "Index";
      }
    }
  }
}
