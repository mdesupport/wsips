﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.ConditionController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Helpers;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Comparer;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization(AuthorizedRoles = new string[] {"IT Administrator", "Administrative Specialist", "Compliance / Enforcement Manager", "Compliance / Enforcement Staff", "Division Chief", "WSIPS eCommerce Member", "Operational Administrator", "Permit Supervisor", "Project Manager", "Secretary"})]
  public class ConditionController : BaseController
  {
    private static readonly ConditionController.ActionNamesClass s_actions = new ConditionController.ActionNamesClass();
    private static readonly ConditionController.ActionParamsClass_Data s_params_Data = new ConditionController.ActionParamsClass_Data();
    private static readonly ConditionController.ActionParamsClass_GetAllSpecialConditions s_params_GetAllSpecialConditions = new ConditionController.ActionParamsClass_GetAllSpecialConditions();
    private static readonly ConditionController.ActionParamsClass_PermitData s_params_PermitData = new ConditionController.ActionParamsClass_PermitData();
    private static readonly ConditionController.ActionParamsClass_Create s_params_Create = new ConditionController.ActionParamsClass_Create();
    private static readonly ConditionController.ActionParamsClass_AddSpecialCondition s_params_AddSpecialCondition = new ConditionController.ActionParamsClass_AddSpecialCondition();
    private static readonly ConditionController.ActionParamsClass_Edit s_params_Edit = new ConditionController.ActionParamsClass_Edit();
    private static readonly ConditionController.ActionParamsClass_Activate s_params_Activate = new ConditionController.ActionParamsClass_Activate();
    private static readonly ConditionController.ActionParamsClass_Deactivate s_params_Deactivate = new ConditionController.ActionParamsClass_Deactivate();
    private static readonly ConditionController.ActionParamsClass_SearchFilter s_params_SearchFilter = new ConditionController.ActionParamsClass_SearchFilter();
    private static readonly ConditionController.ActionParamsClass_UpdateSequence s_params_UpdateSequence = new ConditionController.ActionParamsClass_UpdateSequence();
    private static readonly ConditionController.ActionParamsClass_CustomDetails s_params_CustomDetails = new ConditionController.ActionParamsClass_CustomDetails();
    private static readonly ConditionController.ActionParamsClass_ComplianceCustomDetails s_params_ComplianceCustomDetails = new ConditionController.ActionParamsClass_ComplianceCustomDetails();
    private static readonly ConditionController.ActionParamsClass_GetReportHistory s_params_GetReportHistory = new ConditionController.ActionParamsClass_GetReportHistory();
    private static readonly ConditionController.ActionParamsClass_GetPumpageReports s_params_GetPumpageReports = new ConditionController.ActionParamsClass_GetPumpageReports();
    private static readonly ConditionController.ActionParamsClass_GetStandardReports s_params_GetStandardReports = new ConditionController.ActionParamsClass_GetStandardReports();
    private static readonly ConditionController.ActionParamsClass_ViewPumpageReport s_params_ViewPumpageReport = new ConditionController.ActionParamsClass_ViewPumpageReport();
    private static readonly ConditionController.ActionParamsClass_EditPumpageReport s_params_EditPumpageReport = new ConditionController.ActionParamsClass_EditPumpageReport();
    private static readonly ConditionController.ActionParamsClass_EditCustom s_params_EditCustom = new ConditionController.ActionParamsClass_EditCustom();
    private static readonly ConditionController.ActionParamsClass_CreateCustom s_params_CreateCustom = new ConditionController.ActionParamsClass_CreateCustom();
    private static readonly ConditionController.ActionParamsClass_UpdateCustomSequence s_params_UpdateCustomSequence = new ConditionController.ActionParamsClass_UpdateCustomSequence();
    private static readonly ConditionController.ActionParamsClass_DeleteCustom s_params_DeleteCustom = new ConditionController.ActionParamsClass_DeleteCustom();
    private static readonly ConditionController.ActionParamsClass_EditPermitComplianceStatus s_params_EditPermitComplianceStatus = new ConditionController.ActionParamsClass_EditPermitComplianceStatus();
    private static readonly ConditionController.ActionParamsClass_EditConditionComplianceStatus s_params_EditConditionComplianceStatus = new ConditionController.ActionParamsClass_EditConditionComplianceStatus();
    private static readonly ConditionController.ViewsClass s_views = new ConditionController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Permitting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Condition";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Condition";
    private readonly IUpdatableService<LU_StandardCondition> _standardConditionService;
    private readonly IService<LU_StandardConditionType> _standardConditionTypeService;
    private readonly IService<LU_PermitType> _permitTypeService;
    private readonly IService<LU_PermitTemplate> _permitTemplateService;
    private readonly IService<LU_ConditionReportingPeriod> _reportingPeriodService;
    private readonly IUpdatableService<PermitCondition> _permitConditionService;
    private readonly IUpdatableService<Permit> _permitService;
    private readonly IService<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterService;
    private readonly IService<PermitWithdrawalGroundwater> _permitWithdrawalGroundwaterService;
    private readonly IUpdatableService<ConditionCompliance> _conditionComplianceService;
    private readonly IUpdatableService<PumpageReport> _pumpageReportService;
    private readonly IService<LU_PermitComplianceStatus> _permitComplianceStatusService;
    private readonly IService<LU_PermitConditionComplianceStatus> _conditionComplianceStatusService;
    private readonly IUpdatableService<PumpageReportDetail> _pumpageReportDetailService;
    private readonly IService<LU_WaterWithdrawalEstimate> _waterWithdrawalEstimateService;
    private readonly IService<LU_Month> _monthService;
    private readonly IService<Document> _documentService;
    private readonly IService<LU_CropType> _cropTypeService;
    private readonly IService<LU_PumpageReportType> _pumpageReportTypeService;
    private readonly IPermitServiceClient _permitServiceClient;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController Actions
    {
      get
      {
        return MVC.Permitting.Condition;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionNamesClass ActionNames
    {
      get
      {
        return ConditionController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_Data DataParams
    {
      get
      {
        return ConditionController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_GetAllSpecialConditions GetAllSpecialConditionsParams
    {
      get
      {
        return ConditionController.s_params_GetAllSpecialConditions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_PermitData PermitDataParams
    {
      get
      {
        return ConditionController.s_params_PermitData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_Create CreateParams
    {
      get
      {
        return ConditionController.s_params_Create;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_AddSpecialCondition AddSpecialConditionParams
    {
      get
      {
        return ConditionController.s_params_AddSpecialCondition;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_Edit EditParams
    {
      get
      {
        return ConditionController.s_params_Edit;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_Activate ActivateParams
    {
      get
      {
        return ConditionController.s_params_Activate;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_Deactivate DeactivateParams
    {
      get
      {
        return ConditionController.s_params_Deactivate;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_SearchFilter SearchFilterParams
    {
      get
      {
        return ConditionController.s_params_SearchFilter;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_UpdateSequence UpdateSequenceParams
    {
      get
      {
        return ConditionController.s_params_UpdateSequence;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_CustomDetails CustomDetailsParams
    {
      get
      {
        return ConditionController.s_params_CustomDetails;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_ComplianceCustomDetails ComplianceCustomDetailsParams
    {
      get
      {
        return ConditionController.s_params_ComplianceCustomDetails;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_GetReportHistory GetReportHistoryParams
    {
      get
      {
        return ConditionController.s_params_GetReportHistory;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_GetPumpageReports GetPumpageReportsParams
    {
      get
      {
        return ConditionController.s_params_GetPumpageReports;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_GetStandardReports GetStandardReportsParams
    {
      get
      {
        return ConditionController.s_params_GetStandardReports;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_ViewPumpageReport ViewPumpageReportParams
    {
      get
      {
        return ConditionController.s_params_ViewPumpageReport;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_EditPumpageReport EditPumpageReportParams
    {
      get
      {
        return ConditionController.s_params_EditPumpageReport;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_EditCustom EditCustomParams
    {
      get
      {
        return ConditionController.s_params_EditCustom;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_CreateCustom CreateCustomParams
    {
      get
      {
        return ConditionController.s_params_CreateCustom;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_UpdateCustomSequence UpdateCustomSequenceParams
    {
      get
      {
        return ConditionController.s_params_UpdateCustomSequence;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_DeleteCustom DeleteCustomParams
    {
      get
      {
        return ConditionController.s_params_DeleteCustom;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ActionParamsClass_EditPermitComplianceStatus EditPermitComplianceStatusParams
    {
      get
      {
        return ConditionController.s_params_EditPermitComplianceStatus;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ConditionController.ActionParamsClass_EditConditionComplianceStatus EditConditionComplianceStatusParams
    {
      get
      {
        return ConditionController.s_params_EditConditionComplianceStatus;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ConditionController.ViewsClass Views
    {
      get
      {
        return ConditionController.s_views;
      }
    }

    public ConditionController(IUpdatableService<LU_StandardCondition> standardConditionService, IService<LU_StandardConditionType> standardConditionTypeService, IService<LU_PermitType> permitTypeService, IService<LU_PermitTemplate> permitTemplateService, IService<LU_ConditionReportingPeriod> reportingPeriodService, IUpdatableService<PermitCondition> permitConditionService, IUpdatableService<Permit> permitService, IService<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterService, IService<PermitWithdrawalGroundwater> permitWithdrawalGroundwaterService, IUpdatableService<ConditionCompliance> conditionComplianceService, IUpdatableService<PumpageReport> pumpageReportService, IService<LU_PermitComplianceStatus> permitComplianceStatusService, IService<LU_PermitConditionComplianceStatus> conditionComplianceStatus, IUpdatableService<PumpageReportDetail> pumpageReportDetailService, IService<LU_WaterWithdrawalEstimate> waterWithdrawalEstimateService, IService<LU_Month> monthService, IService<Document> documentService, IService<LU_CropType> cropTypeService, IService<LU_PumpageReportType> pumpageReportTypeService, IPermitServiceClient permitServiceClient)
    {
      this._standardConditionService = standardConditionService;
      this._standardConditionTypeService = standardConditionTypeService;
      this._permitTypeService = permitTypeService;
      this._permitTemplateService = permitTemplateService;
      this._reportingPeriodService = reportingPeriodService;
      this._permitConditionService = permitConditionService;
      this._permitService = permitService;
      this._permitWithdrawalSurfacewaterService = permitWithdrawalSurfacewaterService;
      this._permitWithdrawalGroundwaterService = permitWithdrawalGroundwaterService;
      this._conditionComplianceService = conditionComplianceService;
      this._pumpageReportService = pumpageReportService;
      this._permitComplianceStatusService = permitComplianceStatusService;
      this._conditionComplianceStatusService = conditionComplianceStatus;
      this._pumpageReportDetailService = pumpageReportDetailService;
      this._waterWithdrawalEstimateService = waterWithdrawalEstimateService;
      this._monthService = monthService;
      this._documentService = documentService;
      this._cropTypeService = cropTypeService;
      this._pumpageReportTypeService = pumpageReportTypeService;
      this._permitServiceClient = permitServiceClient;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected ConditionController(Dummy d)
    {
    }

    public virtual ActionResult Index()
    {
      ConditionSearchForm sessionVar = SessionHandler.GetSessionVar<ConditionSearchForm>(SessionVariables.PermitConditionsSearchFilter);
      if (sessionVar != null)
      {
        ViewBag.PermitTypeId = sessionVar.PermitTemplateId;
      }
      this.GetPermitTypes();
      this.GetPermitTemplates();
      return (ActionResult) this.View();
    }

    [HttpGet]
    public virtual JsonResult Data(GridCommand command)
    {
      ConditionSearchForm conditionSearchForm = SessionHandler.GetSessionVar<ConditionSearchForm>(SessionVariables.PermitConditionsSearchFilter);
      if (conditionSearchForm == null)
        conditionSearchForm = new ConditionSearchForm()
        {
          PermitTemplateId = 1
        };
      IEnumerable<DynamicFilter> filter = conditionSearchForm.BuildFilter();
      return this.Json((object) this.GridData(command, filter), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetAllSpecialConditions(GridCommand command)
    {
        List<LU_StandardCondition> list = this._standardConditionService.GetRange(0, int.MaxValue, "Description", new[] { new DynamicFilter("PermitTemplateId == 21") }, "LU_StandardConditionType").Distinct<LU_StandardCondition>((IEqualityComparer<LU_StandardCondition>)new StandardConditionComparer()).ToList<LU_StandardCondition>();
      IEnumerable<StandardCondition> specialConditions = Mapper.Map<IEnumerable<LU_StandardCondition>, IEnumerable<StandardCondition>>((IEnumerable<LU_StandardCondition>) list);
      int count = list.Count;
      int num = (int) Math.Ceiling((double) count / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
          Data = (IEnumerable)specialConditions,
        PageTotal = num,
        CurrentPage = command.Page,
        RecordCount = count
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult PermitData(GridCommand command, int permitId, bool complianceOnly = false)
    {
      Permit byId = this._permitService.GetById(permitId, "PermitConditions.ConditionCompliances,PermitWithdrawalGroundwaters,PermitWithdrawalSurfacewaters");
      PermitWithdrawalSurfacewater surface = byId.PermitWithdrawalSurfacewaters.FirstOrDefault<PermitWithdrawalSurfacewater>();
      PermitWithdrawalGroundwater ground = byId.PermitWithdrawalGroundwaters.FirstOrDefault<PermitWithdrawalGroundwater>();
      List<CustomConditionForm> list = Mapper.Map<IEnumerable<PermitCondition>, IEnumerable<CustomConditionForm>>((IEnumerable<PermitCondition>) byId.PermitConditions.OrderBy<PermitCondition, int?>((Func<PermitCondition, int?>) (pc => pc.Sequence))).ToList<CustomConditionForm>();
      if (complianceOnly)
        list = list.Where<CustomConditionForm>((Func<CustomConditionForm, bool>) (pc => pc.RequiresSelfReporting)).OrderBy<CustomConditionForm, DateTime?>((Func<CustomConditionForm, DateTime?>) (c => c.DueDate)).ToList<CustomConditionForm>();
      IEnumerable<CustomCondition> customConditions = Formatter.ReplaceVariables((IEnumerable<CustomCondition>) list.Select<CustomConditionForm, CustomCondition>((Func<CustomConditionForm, CustomCondition>) (item => new CustomCondition()
      {
        Id = item.Id,
        ConditionText = item.ConditionText,
        DueDate = item.DueDate,
        CanBeDeleted = item.CanBeDeleted,
        OneTimeReportDays = item.OneTimeReportDays,
        StandardConditionTypeId = item.StandardConditionTypeId
      })).ToList<CustomCondition>(), byId, surface, ground);
      int count = list.Count;
      Math.Ceiling((double) count / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) customConditions,
        PageTotal = 1,
        CurrentPage = command.Page,
        RecordCount = count
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult Create(StandardCondition condition)
    {
      try
      {
        if (condition.StandardConditionTypeId == 14 && !condition.PumpageReportTypeId.HasValue)
          this.ModelState.AddModelError(string.Empty, "For Withdrawal Report, please select Report Type");
        if (!this.ModelState.IsValid)
          return this.Json((object) new
          {
            IsSuccess = false,
            Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)).ToList<string>()
          }, JsonRequestBehavior.AllowGet);
        LU_StandardCondition entity = Mapper.Map<StandardCondition, LU_StandardCondition>(condition);
        IEnumerable<int?> source = this._standardConditionService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("PermitTemplateId == @0", new object[1]
          {
            (object) condition.PermitTemplateId
          })
        }, (string) null).Select<LU_StandardCondition, int?>((Func<LU_StandardCondition, int?>) (c => c.Sequence));
        LU_StandardCondition standardCondition = entity;
        int? nullable1;
        if (entity.Sequence.HasValue)
        {
          int? nullable2 = source.Max();
          nullable1 = nullable2.HasValue ? new int?(nullable2.GetValueOrDefault() + 10) : new int?();
        }
        else
          nullable1 = new int?(10);
        standardCondition.Sequence = nullable1;
        this._standardConditionService.Save(entity);
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult AddSpecialCondition(int permitId, int standardConditionId)
    {
      try
      {
        LU_StandardCondition byId = this._standardConditionService.GetById(standardConditionId, (string) null);
        if (permitId <= 0 || byId == null)
          return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
        PermitCondition entity = new PermitCondition()
        {
          ConditionText = byId.Description,
          PermitId = new int?(permitId),
          ConditionReportingPeriodId = byId.ConditionReportingPeriodID,
          StandardConditionTypeId = byId.StandardConditionTypeId,
          RequiresSelfReporting = byId.RequiresSelfReporting,
          OneTimeReportDays = byId.OneTimeReportDays,
          RequiresValidation = byId.RequiresValidation,
          ComplianceReportingDueDate = byId.DefaultComplianceReportingDueDate,
          PumpageReportTypeId = byId.PumpageReportTypeId,
          LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name,
          LastModifiedDate = DateTime.Now
        };
        entity.Id = this._permitConditionService.Save(entity);
        if (byId.RequiresSelfReporting.HasValue && byId.RequiresSelfReporting.Value)
        {
          int? permitStatusId = this._permitService.GetById(permitId, (string) null).PermitStatusId;
          if ((permitStatusId.GetValueOrDefault() != 46 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0)
          {
            List<PermitCondition> permitConditionList = new List<PermitCondition>()
            {
              entity
            };
            this._permitServiceClient.BuildAndSaveConditionCompliance(permitId, 4, (IEnumerable<PermitCondition>) permitConditionList);
          }
        }
        return this.Json(entity.Id > 0 ? (object) new
        {
          IsSuccess = true
        } : (object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual ActionResult Edit(int id)
    {
      LU_StandardCondition byId = this._standardConditionService.GetById(id, "LU_StandardConditionType");
      StandardCondition standardCondition = Mapper.Map<LU_StandardCondition, StandardCondition>(byId);
      if (id != 0)
      {
        int? standardConditionTypeId = byId.StandardConditionTypeId;
        if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0)
          standardCondition.PumpageReports = true;
      }
      this.GetConditionTypes();
      this.GetReportingPeriods();
      this.GetPumpageReportTypes();
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._CreateOrEditCondition, (object) standardCondition);
    }

    [HttpPost]
    public virtual JsonResult Edit(StandardCondition condition)
    {
      try
      {
        if (condition.StandardConditionTypeId == 14 && !condition.PumpageReportTypeId.HasValue)
          this.ModelState.AddModelError(string.Empty, "For Withdrawal Report, please select Report Type");
        int? reportingPeriodId1 = condition.ConditionReportingPeriodId;
        if ((reportingPeriodId1.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId1.HasValue ? 1 : 0)) != 0 && !condition.OneTimeReportDays.HasValue)
          this.ModelState.AddModelError(string.Empty, "For One time reporting, please enter the number of days from effective date");
        int? reportingPeriodId2 = condition.ConditionReportingPeriodId;
        if ((reportingPeriodId2.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId2.HasValue ? 1 : 0)) != 0)
        {
          int? oneTimeReportDays = condition.OneTimeReportDays;
          if ((oneTimeReportDays.GetValueOrDefault() >= 0 ? 0 : (oneTimeReportDays.HasValue ? 1 : 0)) != 0)
            this.ModelState.AddModelError(string.Empty, "The number of days must be a number greater than 0");
        }
        if (!this.ModelState.IsValid)
          return this.Json((object) new
          {
            IsSuccess = false,
            Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)).ToList<string>()
          }, JsonRequestBehavior.AllowGet);
        LU_StandardCondition entity = Mapper.Map<StandardCondition, LU_StandardCondition>(condition);
        bool? requiresSelfReporting = entity.RequiresSelfReporting;
        if ((requiresSelfReporting.GetValueOrDefault() ? 0 : (requiresSelfReporting.HasValue ? 1 : 0)) != 0)
        {
          entity.RequiresValidation = new bool?(false);
          entity.ConditionReportingPeriodID = new int?();
        }
        this._standardConditionService.Save(entity);
        return this.Json((object) new{ IsSuccess = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual ActionResult Activate(int id)
    {
      try
      {
        LU_StandardCondition byId = this._standardConditionService.GetById(id, (string) null);
        byId.Active = true;
        this._standardConditionService.Save(byId);
        return (ActionResult) this.RedirectToAction("Index");
      }
      catch
      {
        return (ActionResult) this.View();
      }
    }

    [HttpGet]
    public virtual ActionResult Deactivate(int id)
    {
      try
      {
        LU_StandardCondition byId = this._standardConditionService.GetById(id, (string) null);
        byId.Active = false;
        this._standardConditionService.Save(byId);
        return (ActionResult) this.RedirectToAction("Index");
      }
      catch
      {
        return (ActionResult) this.View();
      }
    }

    [HttpPost]
    public virtual JsonResult SearchFilter(ConditionSearchForm form)
    {
      SessionHandler.SetSessionVar(SessionVariables.PermitConditionsSearchFilter, (object) form);
      return this.Json((object) new
      {
        IsSaveSearchResult = false,
        IsSuccess = true
      });
    }

    [HttpPost]
    public virtual JsonResult UpdateSequence(int[] ids)
    {
      int num = 1;
      foreach (LU_StandardCondition entity in ((IEnumerable<int>) ids).Select<int, LU_StandardCondition>((Func<int, LU_StandardCondition>) (id => this._standardConditionService.GetById(id, (string) null))))
      {
        entity.Sequence = new int?(num);
        this._standardConditionService.Save(entity);
        ++num;
      }
      return this.Json((object) new{ IsSuccess = true });
    }

    [HttpGet]
    public virtual ActionResult CustomDetails(int id)
    {
      PermitCondition byId1 = this._permitConditionService.GetById(id, (string) null);
      List<CustomConditionForm> source = new List<CustomConditionForm>();
      if (!byId1.PermitId.HasValue)
        return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._ConditionDetails, (object) Mapper.Map<PermitCondition, CustomConditionForm>(byId1));
      CustomConditionForm customConditionForm = Mapper.Map<PermitCondition, CustomConditionForm>(byId1);
      Permit byId2 = this._permitService.GetById(byId1.PermitId.Value, (string) null);
      PermitWithdrawalSurfacewater surface = this._permitWithdrawalSurfacewaterService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == " + (object) byId2.Id, new object[0])
      }, (string) null).FirstOrDefault<PermitWithdrawalSurfacewater>();
      PermitWithdrawalGroundwater ground = this._permitWithdrawalGroundwaterService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == " + (object) byId2.Id, new object[0])
      }, (string) null).FirstOrDefault<PermitWithdrawalGroundwater>();
      source.Add(customConditionForm);
      CustomCondition customCondition = Formatter.ReplaceVariables((IEnumerable<CustomCondition>) source.Select<CustomConditionForm, CustomCondition>((Func<CustomConditionForm, CustomCondition>) (item => new CustomCondition()
      {
        ConditionText = item.ConditionText,
        DueDate = item.ComplianceReportingDueDate
      })).ToList<CustomCondition>(), byId2, surface, ground).FirstOrDefault<CustomCondition>();
      if (customCondition != null)
      {
        string conditionText = customCondition.ConditionText;
        customConditionForm.ConditionText = conditionText;
      }
      ConditionCompliance conditionCompliance1 = this._conditionComplianceService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitConditionId == " + (object) id, new object[0])
      }, "LU_PermitConditionComplianceStatus").FirstOrDefault<ConditionCompliance>((Func<ConditionCompliance, bool>) (c =>
      {
        if (c.ComplianceReportingDueDate <= DateTime.Now)
          return c.ComplianceReportingStartDate <= DateTime.Now;
        return false;
      }));
      if (conditionCompliance1 != null)
      {
        customConditionForm.Status = conditionCompliance1.LU_PermitConditionComplianceStatus.Description;
        customConditionForm.DueDate = new DateTime?(conditionCompliance1.ComplianceReportingDueDate);
      }
      else
      {
        ConditionCompliance conditionCompliance2 = this._conditionComplianceService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[2]
        {
          new DynamicFilter("PermitConditionId == " + (object) customConditionForm.Id, new object[0]),
          new DynamicFilter("PermitConditionComplianceStatusId != " + (object) 5, new object[0])
        }, "LU_PermitConditionComplianceStatus").FirstOrDefault<ConditionCompliance>((Func<ConditionCompliance, bool>) (c => c.ComplianceReportingDueDate > DateTime.Now));
        if (conditionCompliance2 != null)
          customConditionForm.Status = conditionCompliance2.LU_PermitConditionComplianceStatus.Description;
      }
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._ConditionDetails, (object) customConditionForm);
    }

    [HttpGet]
    public virtual ActionResult ComplianceCustomDetails(int permitConditionId, int conditionComplianceId)
    {
      PermitCondition byId1 = this._permitConditionService.GetById(permitConditionId, (string) null);
      ViewBag.ViewUploadBaseUrl = WebConfigurationManager.AppSettings["DocumentGetPath"];
      CustomConditionForm customConditionForm = Mapper.Map<PermitCondition, CustomConditionForm>(byId1);
      if (byId1.PermitId.HasValue)
      {
        List<CustomConditionForm> source = new List<CustomConditionForm>();
        Permit byId2 = this._permitService.GetById(byId1.PermitId.Value, (string) null);
        customConditionForm.CurrentPermitStatusId = byId2.PermitStatusId.GetValueOrDefault();
        int? standardConditionTypeId = byId1.StandardConditionTypeId;
        if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0)
          customConditionForm.PumpageReports = true;
        source.Add(customConditionForm);
        CustomCondition customCondition = Formatter.ReplaceVariables((IEnumerable<CustomCondition>) source.Select<CustomConditionForm, CustomCondition>((Func<CustomConditionForm, CustomCondition>) (item => new CustomCondition()
        {
          ConditionText = item.ConditionText,
          DueDate = item.ComplianceReportingDueDate
        })).ToList<CustomCondition>(), byId2, byId2.PermitWithdrawalSurfacewaters.FirstOrDefault<PermitWithdrawalSurfacewater>(), byId2.PermitWithdrawalGroundwaters.FirstOrDefault<PermitWithdrawalGroundwater>()).FirstOrDefault<CustomCondition>();
        if (customCondition != null)
        {
          string conditionText = customCondition.ConditionText;
          customConditionForm.ConditionText = conditionText;
        }
        ConditionCompliance byId3 = this._conditionComplianceService.GetById(conditionComplianceId, "LU_PermitConditionComplianceStatus");
        if (byId3 != null)
        {
          customConditionForm.Status = byId3.LU_PermitConditionComplianceStatus.Description;
          customConditionForm.ConditionComplianceId = new int?(byId3.Id);
        }
      }
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._ComplianceConditionDetails, (object) customConditionForm);
    }

    [HttpGet]
    public virtual JsonResult GetReportHistory(GridCommand command, int permitConditionId)
    {
      int? standardConditionTypeId = this._permitConditionService.GetById(permitConditionId, (string) null).StandardConditionTypeId;
      List<ConditionReport> list = Mapper.Map<IEnumerable<ConditionCompliance>, IEnumerable<ConditionReport>>(this._conditionComplianceService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitConditionId == " + (object) permitConditionId, new object[0])
      }, "LU_PermitConditionComplianceStatus")).OrderBy<ConditionReport, DateTime>((Func<ConditionReport, DateTime>) (r => r.DueDate)).ToList<ConditionReport>();
      foreach (ConditionReport conditionReport in list)
      {
        PumpageReport pumpageReport = this._pumpageReportService.GetRange(0, int.MaxValue, "ReceivedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("Active && ConditionComplianceId == @0", new object[1]
          {
            (object) conditionReport.Id
          })
        }, (string) null).FirstOrDefault<PumpageReport>();
        if (pumpageReport != null)
        {
          conditionReport.ReceivedDate = pumpageReport.ReceivedDate;
          conditionReport.SubmittedDate = new DateTime?(pumpageReport.CreatedDate);
        }
        conditionReport.StandardConditionTypeId = standardConditionTypeId;
        Document document = this._documentService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("RefId == @0 && RefTableId == @1", new object[2]
          {
            (object) conditionReport.Id,
            (object) 69
          })
        }, (string) null).SingleOrDefault<Document>();
        if (document != null)
        {
          conditionReport.ExistingDocumentId = new int?(document.Id);
          conditionReport.ReceivedDate = new DateTime?(document.CreatedDate);
          conditionReport.SubmittedDate = new DateTime?(document.CreatedDate);
        }
      }
      int num1 = list.Count<ConditionReport>();
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) list,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetPumpageReports(GridCommand command, int id)
    {
      List<PumpageReport> list = this._pumpageReportService.GetRange(0, int.MaxValue, "CreatedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("ConditionComplianceId == @0", new object[1]
        {
          (object) id
        })
      }, (string) null).ToList<PumpageReport>();
      int count = list.Count;
      int num = (int) Math.Ceiling((double) count / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) list,
        PageTotal = num,
        CurrentPage = command.Page,
        RecordCount = count
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual JsonResult GetStandardReports(GridCommand command, int id)
    {
      List<Document> list = this._documentService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("RefId == @0 && RefTableId == @1", new object[2]
        {
          (object) id,
          (object) 69
        })
      }, (string) null).ToList<Document>();
      int num1 = list.Count<Document>();
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return this.Json((object) new GridData()
      {
        Data = (IEnumerable) list,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual ActionResult ViewPumpageReport(int id)
    {
      PumpageReport byId1 = this._pumpageReportService.GetById(id, "PumpageReportDetails, LU_WaterWithdrawalEstimate, LU_CropType");
      PumpageReportDetailsForm reportDetailsForm = Mapper.Map<PumpageReport, PumpageReportDetailsForm>(byId1);
      ConditionCompliance byId2 = this._conditionComplianceService.GetById(byId1.ConditionComplianceId.Value, (string) null);
      int permitConditionId = byId2.PermitConditionId;
      reportDetailsForm.PermitConditionId = permitConditionId;
      reportDetailsForm.PumpageReportTypeId = this._permitConditionService.GetById(permitConditionId, (string) null).PumpageReportTypeId;
      this.GetWaterWithdrawalEstimateTypes();
      this.GetCropTypes();
      this.GetReportMonths(byId2);
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._ViewPumpageReport, (object) reportDetailsForm);
    }

    [HttpGet]
    public virtual ActionResult EditPumpageReport(int id, int conditionComplianceId = 0, bool resubmit = false)
    {
      ConditionCompliance compliance = (ConditionCompliance) null;
      PumpageReport byId = this._pumpageReportService.GetById(id, "PumpageReportDetails, LU_WaterWithdrawalEstimate");
      PumpageReportDetailsForm reportDetailsForm = Mapper.Map<PumpageReport, PumpageReportDetailsForm>(byId);
      if (id == 0)
      {
        reportDetailsForm = new PumpageReportDetailsForm();
        compliance = this._conditionComplianceService.GetById(conditionComplianceId, (string) null);
        if (compliance != null)
        {
          reportDetailsForm.PermitConditionId = compliance.PermitConditionId;
          reportDetailsForm.ReportYear = Convert.ToDateTime(compliance.ComplianceReportingStartDate).Year.ToString((IFormatProvider) CultureInfo.InvariantCulture);
          reportDetailsForm.PumpageReportTypeId = this._permitConditionService.GetById(compliance.PermitConditionId, (string) null).PumpageReportTypeId;
        }
        reportDetailsForm.Name = "Water Withdrawal Report";
        reportDetailsForm.IsResumbission = resubmit;
      }
      else if (byId.ConditionComplianceId.HasValue)
      {
        int permitConditionId = this._conditionComplianceService.GetById(byId.ConditionComplianceId.Value, (string) null).PermitConditionId;
        compliance = this._conditionComplianceService.GetById(byId.ConditionComplianceId.Value, (string) null);
        if (compliance != null)
          reportDetailsForm.PumpageReportTypeId = this._permitConditionService.GetById(compliance.PermitConditionId, (string) null).PumpageReportTypeId;
        reportDetailsForm.PermitConditionId = permitConditionId;
      }
      this.GetWaterWithdrawalEstimateTypes();
      this.GetCropTypes();
      this.GetReportMonths(compliance);
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._CreateOrEditPumpageReport, (object) reportDetailsForm);
    }

    [HttpPost]
    public virtual ActionResult EditPumpageReport(PumpageReportDetailsForm report)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          int? withdrawalEstimateId = report.WaterWithdrawalEstimateId;
          if ((withdrawalEstimateId.GetValueOrDefault() != 4 ? 0 : (withdrawalEstimateId.HasValue ? 1 : 0)) != 0 && report.OtherWaterWithdrawalMethod == null)
          {
            this.ModelState.AddModelError(string.Empty, "Please specify Other Withdrawal Estimate Method");
          }
          else
          {
            PumpageReport entity1 = Mapper.Map<PumpageReportDetailsForm, PumpageReport>(report);
            entity1.Active = true;
            if (entity1.Id == 0)
              entity1.ReceivedDate = new DateTime?(DateTime.Now);
            if (entity1.ConditionComplianceId.HasValue && report.IsResumbission)
            {
              IUpdatableService<PumpageReport> pumpageReportService = this._pumpageReportService;
              int skip = 0;
              int maxValue = int.MaxValue;
              DynamicFilter[] dynamicFilterArray = new DynamicFilter[1]
              {
                new DynamicFilter("ConditionComplianceId == @0 && Active", new object[1]
                {
                  (object) entity1.ConditionComplianceId
                })
              };
              foreach (PumpageReport entity2 in pumpageReportService.GetRange(skip, maxValue, null, (IEnumerable<DynamicFilter>) dynamicFilterArray, null))
              {
                entity2.Active = false;
                this._pumpageReportService.Save(entity2);
              }
            }
            entity1.PumpageReportDetails = report.PumpageReportDetails;
            foreach (PumpageReportDetail pumpageReportDetail in (IEnumerable<PumpageReportDetail>) entity1.PumpageReportDetails)
            {
              pumpageReportDetail.CreatedBy = this.User.Identity.Name;
              pumpageReportDetail.LastModifiedBy = this.User.Identity.Name;
              pumpageReportDetail.CreatedDate = DateTime.Now;
              pumpageReportDetail.LastModifiedDate = DateTime.Now;
              this._pumpageReportDetailService.Save(pumpageReportDetail);
            }
            this._pumpageReportService.Save(entity1);
            if (entity1.ConditionComplianceId.HasValue)
            {
              ConditionCompliance byId = this._conditionComplianceService.GetById(entity1.ConditionComplianceId.Value, (string) null);
              byId.PermitConditionComplianceStatusId = 5;
              this._conditionComplianceService.Save(byId);
            }
            return (ActionResult) this.Json((object) new
            {
              IsSuccess = true
            }, JsonRequestBehavior.AllowGet);
          }
        }
        return (ActionResult) this.Json((object) new
        {
          IsSuccess = false,
          Errors = this.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (v => v.Errors.Any<ModelError>())).SelectMany<ModelState, string>((Func<ModelState, IEnumerable<string>>) (t => t.Errors.Select<ModelError, string>((Func<ModelError, string>) (e => e.ErrorMessage))))
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return (ActionResult) this.Json((object) new
        {
          IsSuccess = false
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual ActionResult EditCustom(int id, int permitId)
    {
      PermitCondition byId = this._permitConditionService.GetById(id, (string) null);
      CustomConditionForm customConditionForm = Mapper.Map<PermitCondition, CustomConditionForm>(byId);
      ViewBag.PermitId = permitId;
      if (id != 0)
      {
        int? standardConditionTypeId = byId.StandardConditionTypeId;
        if ((standardConditionTypeId.GetValueOrDefault() != 14 ? 0 : (standardConditionTypeId.HasValue ? 1 : 0)) != 0)
          customConditionForm.PumpageReports = true;
      }
      this.GetConditionTypes();
      this.GetReportingPeriods();
      this.GetPumpageReportTypes();
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._CreateOrEditCustomCondition, (object) customConditionForm);
    }

    [HttpPost]
    public virtual JsonResult EditCustom(CustomConditionForm condition)
    {
      try
      {
        if (condition.StandardConditionTypeId == 14 && !condition.PumpageReportTypeId.HasValue)
          this.ModelState.AddModelError(string.Empty, "For Withdrawal Report, please select Report Type");
        int? reportingPeriodId1 = condition.ConditionReportingPeriodId;
        if ((reportingPeriodId1.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId1.HasValue ? 1 : 0)) != 0 && !condition.OneTimeReportDays.HasValue)
          this.ModelState.AddModelError(string.Empty, "For One time reporting, please enter the number of days from effective date");
        int? reportingPeriodId2 = condition.ConditionReportingPeriodId;
        if ((reportingPeriodId2.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId2.HasValue ? 1 : 0)) != 0)
        {
          int? oneTimeReportDays = condition.OneTimeReportDays;
          if ((oneTimeReportDays.GetValueOrDefault() >= 1 ? 0 : (oneTimeReportDays.HasValue ? 1 : 0)) != 0)
            this.ModelState.AddModelError(string.Empty, "The number of days must be a number greater than 0");
        }
        if (!this.ModelState.IsValid)
          return this.Json((object) new
          {
            IsSuccess = false,
            Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)).ToList<string>()
          }, JsonRequestBehavior.AllowGet);
        this._permitConditionService.Save(Mapper.Map<CustomConditionForm, PermitCondition>(condition));
        return this.Json((object) new{ IsSuccess = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult CreateCustom(CustomConditionForm condition)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          int? reportingPeriodId1 = condition.ConditionReportingPeriodId;
          if ((reportingPeriodId1.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId1.HasValue ? 1 : 0)) != 0 && !condition.OneTimeReportDays.HasValue)
          {
            this.ModelState.AddModelError(string.Empty, "For One time reporting, please enter the number of days from effective date");
          }
          else
          {
            int? reportingPeriodId2 = condition.ConditionReportingPeriodId;
            if ((reportingPeriodId2.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId2.HasValue ? 1 : 0)) != 0)
            {
              int? oneTimeReportDays = condition.OneTimeReportDays;
              if ((oneTimeReportDays.GetValueOrDefault() >= 1 ? 0 : (oneTimeReportDays.HasValue ? 1 : 0)) != 0)
              {
                this.ModelState.AddModelError(string.Empty, "The number of days must be a number greater than 0");
                goto label_12;
              }
            }
            int? reportingPeriodId3 = condition.ConditionReportingPeriodId;
            if ((reportingPeriodId3.GetValueOrDefault() != 5 ? 0 : (reportingPeriodId3.HasValue ? 1 : 0)) != 0)
              condition.ConditionText = condition.ConditionText + " Report is due on [OneTimeReporting.DueDate]";
            PermitCondition entity = Mapper.Map<CustomConditionForm, PermitCondition>(condition);
            int? nullable = this._permitConditionService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
            {
              new DynamicFilter("PermitId == @0", new object[1]
              {
                (object) condition.PermitId
              })
            }, (string) null).Select<PermitCondition, int?>((Func<PermitCondition, int?>) (c => c.Sequence)).Max();
            entity.Sequence = new int?(nullable.GetValueOrDefault() + 10);
            entity.Id = this._permitConditionService.Save(entity);
            if (condition.RequiresSelfReporting)
            {
              int? permitStatusId = this._permitService.GetById(condition.PermitId, (string) null).PermitStatusId;
              if ((permitStatusId.GetValueOrDefault() != 46 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0)
              {
                List<PermitCondition> permitConditionList = new List<PermitCondition>()
                {
                  entity
                };
                this._permitServiceClient.BuildAndSaveConditionCompliance(condition.PermitId, 4, (IEnumerable<PermitCondition>) permitConditionList);
              }
            }
            return this.Json((object) new
            {
              IsSuccess = true
            }, JsonRequestBehavior.AllowGet);
          }
        }
label_12:
        return this.Json((object) new
        {
          IsSuccess = false,
          Errors = this.ViewData.ModelState.Values.Where<ModelState>((Func<ModelState, bool>) (item => item.Errors.Any<ModelError>())).Select<ModelState, string>((Func<ModelState, string>) (item => item.Errors[0].ErrorMessage)).ToList<string>()
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpPost]
    public virtual JsonResult UpdateCustomSequence(int[] ids)
    {
      int num = 1;
      foreach (PermitCondition entity in ((IEnumerable<int>) ids).Select<int, PermitCondition>((Func<int, PermitCondition>) (id => this._permitConditionService.GetById(id, (string) null))))
      {
        entity.Sequence = new int?(num);
        this._permitConditionService.Save(entity);
        ++num;
      }
      return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
    }

    [HttpDelete]
    public virtual JsonResult DeleteCustom(int id)
    {
      try
      {
        this._permitConditionService.Delete(id);
        return this.Json((object) new{ IsSuccess = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ IsSuccess = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual ActionResult EditPermitComplianceStatus(int permitId)
    {
      int? complianceStatusId = this._permitService.GetById(permitId, (string) null).ComplianceStatusId;
      ViewBag.PermitId = permitId;
      ViewBag.PermitComplianceStatusId = complianceStatusId;
      this.GetComplianceStatuses();
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._EditPermitComplianceStatus);
    }

    [HttpPost]
    public virtual ActionResult EditPermitComplianceStatus(int complianceStatusId, int permitId)
    {
      try
      {
        Permit byId = this._permitService.GetById(permitId, (string) null);
        if (byId != null)
        {
          byId.ComplianceStatusId = new int?(complianceStatusId);
          this._permitService.Save(byId);
        }
        return (ActionResult) this.Json((object) new
        {
          IsSuccess = true
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return (ActionResult) this.Json((object) new
        {
          IsSuccess = false
        }, JsonRequestBehavior.AllowGet);
      }
    }

    [HttpGet]
    public virtual ActionResult EditConditionComplianceStatus(int id)
    {
      int complianceStatusId = this._conditionComplianceService.GetById(id, (string) null).PermitConditionComplianceStatusId;
      ViewBag.ConditionCompliance = id;
      ViewBag.PermitConditionComplianceStatusId = complianceStatusId;
      this.GetConditionComplianceStatuses();
      return (ActionResult) this.PartialView(MVC.Permitting.Condition.Views._EditPermitConditionComplianceStatus);
    }

    [HttpPost]
    public virtual ActionResult EditConditionComplianceStatus(int complianceStatusId, int id)
    {
      try
      {
        ConditionCompliance byId = this._conditionComplianceService.GetById(id, (string) null);
        if (byId != null)
        {
          byId.PermitConditionComplianceStatusId = complianceStatusId;
          this._conditionComplianceService.Save(byId);
        }
        return (ActionResult) this.Json((object) new
        {
          IsSuccess = true
        }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return (ActionResult) this.Json((object) new
        {
          IsSuccess = false
        }, JsonRequestBehavior.AllowGet);
      }
    }

    private void GetConditionTypes()
    {
      ViewBag.ConditionTypes = this._standardConditionTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetPermitTypes()
    {
      ViewBag.PermitTypes = this._permitTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetPermitTemplates()
    {
      ViewBag.PermitTemplates = this._permitTemplateService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetReportingPeriods()
    {
      ViewBag.ReportingPeriods = this._reportingPeriodService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetComplianceStatuses()
    {
      ViewBag.PermitComplianceStatuses = this._permitComplianceStatusService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetConditionComplianceStatuses()
    {
      ViewBag.ConditionComplianceStatuses = this._conditionComplianceStatusService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetWaterWithdrawalEstimateTypes()
    {
      ViewBag.WaterWithdrawalEstimateTypes = this._waterWithdrawalEstimateService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetCropTypes()
    {
      ViewBag.CropTypes = this._cropTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private void GetReportMonths(ConditionCompliance compliance)
    {
      IEnumerable<LU_Month> luMonths = (IEnumerable<LU_Month>) null;
      bool flag1 = false;
      if (compliance != null)
      {
        int? reportingPeriodId = this._permitConditionService.GetById(compliance.PermitConditionId, (string) null).ConditionReportingPeriodId;
        bool flag2 = reportingPeriodId.GetValueOrDefault() == 3 && reportingPeriodId.HasValue;
        bool flag3 = compliance.ComplianceReportingStartDate.Month == 1;
        bool flag4 = compliance.ComplianceReportingStartDate.Month == 7;
        if (flag2 && flag3)
          luMonths = this._monthService.GetRange(0, 6, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("Active == True", new object[0])
          }, (string) null);
        else if (flag2 && flag4)
          luMonths = this._monthService.GetRange(6, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("Active == True", new object[0])
          }, (string) null);
      }
      if (luMonths == null)
      {
        luMonths = this._monthService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("Active == True", new object[0])
        }, (string) null);
        flag1 = true;
      }
      ViewBag.ShowFullYear = flag1;
      ViewBag.Months = luMonths;
    }

    private void GetPumpageReportTypes()
    {
      ViewBag.PumpageReportTypes = this._pumpageReportTypeService.GetRange(0, int.MaxValue, "Sequence", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Active == True", new object[0])
      }, (string) null);
    }

    private GridData GridData(GridCommand command, IEnumerable<DynamicFilter> filter)
    {
      IEnumerable<StandardCondition> standardConditions = Mapper.Map<IEnumerable<LU_StandardCondition>, IEnumerable<StandardCondition>>((IEnumerable<LU_StandardCondition>) this._standardConditionService.GetRange(command.Skip(), command.PageSize, "Sequence", filter, "LU_StandardConditionType").ToList<LU_StandardCondition>());
      int num1 = this._standardConditionService.Count(filter);
      int num2 = (int) Math.Ceiling((double) num1 / (double) command.PageSize);
      return new GridData()
      {
        Data = (IEnumerable) standardConditions,
        PageTotal = num2,
        CurrentPage = command.Page,
        RecordCount = num1
      };
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult GetAllSpecialConditions()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetAllSpecialConditions, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult PermitData()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.PermitData, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Create()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Create, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult AddSpecialCondition()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddSpecialCondition, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult Edit()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult Activate()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Activate, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult Deactivate()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Deactivate, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult SearchFilter()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchFilter, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult UpdateSequence()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateSequence, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual ActionResult CustomDetails()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CustomDetails, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult ComplianceCustomDetails()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.ComplianceCustomDetails, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult GetReportHistory()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetReportHistory, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult GetPumpageReports()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPumpageReports, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual JsonResult GetStandardReports()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetStandardReports, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult ViewPumpageReport()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.ViewPumpageReport, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual ActionResult EditPumpageReport()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPumpageReport, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult EditCustom()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditCustom, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult CreateCustom()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CreateCustom, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult UpdateCustomSequence()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateCustomSequence, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult DeleteCustom()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeleteCustom, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual ActionResult EditPermitComplianceStatus()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPermitComplianceStatus, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    public virtual ActionResult EditConditionComplianceStatus()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditConditionComplianceStatus, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string Data = "Data";
      public readonly string GetAllSpecialConditions = "GetAllSpecialConditions";
      public readonly string PermitData = "PermitData";
      public readonly string Create = "Create";
      public readonly string AddSpecialCondition = "AddSpecialCondition";
      public readonly string Edit = "Edit";
      public readonly string Activate = "Activate";
      public readonly string Deactivate = "Deactivate";
      public readonly string SearchFilter = "SearchFilter";
      public readonly string UpdateSequence = "UpdateSequence";
      public readonly string CustomDetails = "CustomDetails";
      public readonly string ComplianceCustomDetails = "ComplianceCustomDetails";
      public readonly string GetReportHistory = "GetReportHistory";
      public readonly string GetPumpageReports = "GetPumpageReports";
      public readonly string GetStandardReports = "GetStandardReports";
      public readonly string ViewPumpageReport = "ViewPumpageReport";
      public readonly string EditPumpageReport = "EditPumpageReport";
      public readonly string EditCustom = "EditCustom";
      public readonly string CreateCustom = "CreateCustom";
      public readonly string UpdateCustomSequence = "UpdateCustomSequence";
      public readonly string DeleteCustom = "DeleteCustom";
      public readonly string EditPermitComplianceStatus = "EditPermitComplianceStatus";
      public readonly string EditConditionComplianceStatus = "EditConditionComplianceStatus";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string Data = "Data";
      public const string GetAllSpecialConditions = "GetAllSpecialConditions";
      public const string PermitData = "PermitData";
      public const string Create = "Create";
      public const string AddSpecialCondition = "AddSpecialCondition";
      public const string Edit = "Edit";
      public const string Activate = "Activate";
      public const string Deactivate = "Deactivate";
      public const string SearchFilter = "SearchFilter";
      public const string UpdateSequence = "UpdateSequence";
      public const string CustomDetails = "CustomDetails";
      public const string ComplianceCustomDetails = "ComplianceCustomDetails";
      public const string GetReportHistory = "GetReportHistory";
      public const string GetPumpageReports = "GetPumpageReports";
      public const string GetStandardReports = "GetStandardReports";
      public const string ViewPumpageReport = "ViewPumpageReport";
      public const string EditPumpageReport = "EditPumpageReport";
      public const string EditCustom = "EditCustom";
      public const string CreateCustom = "CreateCustom";
      public const string UpdateCustomSequence = "UpdateCustomSequence";
      public const string DeleteCustom = "DeleteCustom";
      public const string EditPermitComplianceStatus = "EditPermitComplianceStatus";
      public const string EditConditionComplianceStatus = "EditConditionComplianceStatus";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string command = "command";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetAllSpecialConditions
    {
      public readonly string command = "command";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_PermitData
    {
      public readonly string command = "command";
      public readonly string permitId = "permitId";
      public readonly string complianceOnly = "complianceOnly";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Create
    {
      public readonly string condition = "condition";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddSpecialCondition
    {
      public readonly string permitId = "permitId";
      public readonly string standardConditionId = "standardConditionId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Edit
    {
      public readonly string id = "id";
      public readonly string condition = "condition";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Activate
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Deactivate
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SearchFilter
    {
      public readonly string form = "form";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_UpdateSequence
    {
      public readonly string ids = "ids";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_CustomDetails
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_ComplianceCustomDetails
    {
      public readonly string permitConditionId = "permitConditionId";
      public readonly string conditionComplianceId = "conditionComplianceId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetReportHistory
    {
      public readonly string command = "command";
      public readonly string permitConditionId = "permitConditionId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetPumpageReports
    {
      public readonly string command = "command";
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetStandardReports
    {
      public readonly string command = "command";
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_ViewPumpageReport
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EditPumpageReport
    {
      public readonly string id = "id";
      public readonly string conditionComplianceId = "conditionComplianceId";
      public readonly string resubmit = "resubmit";
      public readonly string report = "report";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EditCustom
    {
      public readonly string id = "id";
      public readonly string permitId = "permitId";
      public readonly string condition = "condition";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_CreateCustom
    {
      public readonly string condition = "condition";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_UpdateCustomSequence
    {
      public readonly string ids = "ids";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_DeleteCustom
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EditPermitComplianceStatus
    {
      public readonly string permitId = "permitId";
      public readonly string complianceStatusId = "complianceStatusId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_EditConditionComplianceStatus
    {
      public readonly string id = "id";
      public readonly string complianceStatusId = "complianceStatusId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly ConditionController.ViewsClass._ViewNamesClass s_ViewNames = new ConditionController.ViewsClass._ViewNamesClass();
      public readonly string _ComplianceConditionDetails = "~/Areas/Permitting/Views/Condition/_ComplianceConditionDetails.cshtml";
      public readonly string _ConditionDetails = "~/Areas/Permitting/Views/Condition/_ConditionDetails.cshtml";
      public readonly string _CreateOrEditCondition = "~/Areas/Permitting/Views/Condition/_CreateOrEditCondition.cshtml";
      public readonly string _CreateOrEditCustomCondition = "~/Areas/Permitting/Views/Condition/_CreateOrEditCustomCondition.cshtml";
      public readonly string _CreateOrEditPumpageReport = "~/Areas/Permitting/Views/Condition/_CreateOrEditPumpageReport.cshtml";
      public readonly string _EditPermitComplianceStatus = "~/Areas/Permitting/Views/Condition/_EditPermitComplianceStatus.cshtml";
      public readonly string _EditPermitConditionComplianceStatus = "~/Areas/Permitting/Views/Condition/_EditPermitConditionComplianceStatus.cshtml";
      public readonly string _ViewPumpageReport = "~/Areas/Permitting/Views/Condition/_ViewPumpageReport.cshtml";
      public readonly string Activate = "~/Areas/Permitting/Views/Condition/Activate.cshtml";
      public readonly string Deactivate = "~/Areas/Permitting/Views/Condition/Deactivate.cshtml";
      public readonly string DeleteCustom = "~/Areas/Permitting/Views/Condition/DeleteCustom.cshtml";
      public readonly string Edit = "~/Areas/Permitting/Views/Condition/Edit.cshtml";
      public readonly string Index = "~/Areas/Permitting/Views/Condition/Index.cshtml";

      public ConditionController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return ConditionController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string _ComplianceConditionDetails = "_ComplianceConditionDetails";
        public readonly string _ConditionDetails = "_ConditionDetails";
        public readonly string _CreateOrEditCondition = "_CreateOrEditCondition";
        public readonly string _CreateOrEditCustomCondition = "_CreateOrEditCustomCondition";
        public readonly string _CreateOrEditPumpageReport = "_CreateOrEditPumpageReport";
        public readonly string _EditPermitComplianceStatus = "_EditPermitComplianceStatus";
        public readonly string _EditPermitConditionComplianceStatus = "_EditPermitConditionComplianceStatus";
        public readonly string _ViewPumpageReport = "_ViewPumpageReport";
        public readonly string Activate = "Activate";
        public readonly string Deactivate = "Deactivate";
        public readonly string DeleteCustom = "DeleteCustom";
        public readonly string Edit = "Edit";
        public readonly string Index = "Index";
      }
    }
  }
}
