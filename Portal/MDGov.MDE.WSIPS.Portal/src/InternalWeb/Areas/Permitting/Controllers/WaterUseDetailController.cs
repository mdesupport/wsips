﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.WaterUseDetailController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.ServiceClient;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization]
  public class WaterUseDetailController : BaseController
  {
    private static readonly WaterUseDetailController.ActionNamesClass s_actions = new WaterUseDetailController.ActionNamesClass();
    private static readonly WaterUseDetailController.ActionParamsClass_LoadWaterUseDetails s_params_LoadWaterUseDetails = new WaterUseDetailController.ActionParamsClass_LoadWaterUseDetails();
    private static readonly WaterUseDetailController.ActionParamsClass_AddPoultryWateringEvaporative s_params_AddPoultryWateringEvaporative = new WaterUseDetailController.ActionParamsClass_AddPoultryWateringEvaporative();
    private static readonly WaterUseDetailController.ActionParamsClass_AddPoultryWateringFogger s_params_AddPoultryWateringFogger = new WaterUseDetailController.ActionParamsClass_AddPoultryWateringFogger();
    private static readonly WaterUseDetailController.ActionParamsClass_AddLivestockWatering s_params_AddLivestockWatering = new WaterUseDetailController.ActionParamsClass_AddLivestockWatering();
    private static readonly WaterUseDetailController.ActionParamsClass_AddOtherLivestockWatering s_params_AddOtherLivestockWatering = new WaterUseDetailController.ActionParamsClass_AddOtherLivestockWatering();
    private static readonly WaterUseDetailController.ActionParamsClass_AddDairyAnimalWateing s_params_AddDairyAnimalWateing = new WaterUseDetailController.ActionParamsClass_AddDairyAnimalWateing();
    private static readonly WaterUseDetailController.ActionParamsClass_AddCrop s_params_AddCrop = new WaterUseDetailController.ActionParamsClass_AddCrop();
    private static readonly WaterUseDetailController.ActionParamsClass_AddNurseryStock s_params_AddNurseryStock = new WaterUseDetailController.ActionParamsClass_AddNurseryStock();
    private static readonly WaterUseDetailController.ActionParamsClass_AddSod s_params_AddSod = new WaterUseDetailController.ActionParamsClass_AddSod();
    private static readonly WaterUseDetailController.ActionParamsClass_AddPrivateWaterSupply s_params_AddPrivateWaterSupply = new WaterUseDetailController.ActionParamsClass_AddPrivateWaterSupply();
    private static readonly WaterUseDetailController.ActionParamsClass_AddWastewaterTreatmentDisposal s_params_AddWastewaterTreatmentDisposal = new WaterUseDetailController.ActionParamsClass_AddWastewaterTreatmentDisposal();
    private static readonly WaterUseDetailController.ViewsClass s_views = new WaterUseDetailController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Permitting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "WaterUseDetail";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "WaterUseDetail";
    private readonly IUpdatableService<PermitWwLivestock> _permitWwLivestockService;
    private readonly IUpdatableService<PermitWwNonAgricultureIrrigation> _permitWwNonAgricultureIrrigationService;
    private readonly IService<LU_WaterWithdrawalPurposeCategory> _WaterWithdrawalPurposeCategory;
    private readonly IUpdatableService<PermitWwPoultry> _permitWaterWithdrawalPoultryService;
    private readonly IUpdatableService<PermitWwCrop> _permitWwCropService;
    private readonly IWaterUseDetailServiceClient _waterUseDetailsServiceClient;
    private readonly IUpdatableService<LU_CropYieldUnit> _cropYieldUnitService;
    private readonly IService<PermitWwPrivateWaterSupplier> _permitWaterWithdrawalPrivateWaterSupplierService;
    private readonly IUpdatableService<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeService;
    private readonly IService<PermitWastewaterTreatmentDisposal> _permitWastewaterTreatmentDisposalService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public WaterUseDetailController Actions
    {
      get
      {
        return MVC.Permitting.WaterUseDetail;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public WaterUseDetailController.ActionNamesClass ActionNames
    {
      get
      {
        return WaterUseDetailController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public WaterUseDetailController.ActionParamsClass_LoadWaterUseDetails LoadWaterUseDetailsParams
    {
      get
      {
        return WaterUseDetailController.s_params_LoadWaterUseDetails;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public WaterUseDetailController.ActionParamsClass_AddPoultryWateringEvaporative AddPoultryWateringEvaporativeParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddPoultryWateringEvaporative;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public WaterUseDetailController.ActionParamsClass_AddPoultryWateringFogger AddPoultryWateringFoggerParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddPoultryWateringFogger;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public WaterUseDetailController.ActionParamsClass_AddLivestockWatering AddLivestockWateringParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddLivestockWatering;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public WaterUseDetailController.ActionParamsClass_AddOtherLivestockWatering AddOtherLivestockWateringParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddOtherLivestockWatering;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public WaterUseDetailController.ActionParamsClass_AddDairyAnimalWateing AddDairyAnimalWateingParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddDairyAnimalWateing;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public WaterUseDetailController.ActionParamsClass_AddCrop AddCropParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddCrop;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public WaterUseDetailController.ActionParamsClass_AddNurseryStock AddNurseryStockParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddNurseryStock;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public WaterUseDetailController.ActionParamsClass_AddSod AddSodParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddSod;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public WaterUseDetailController.ActionParamsClass_AddPrivateWaterSupply AddPrivateWaterSupplyParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddPrivateWaterSupply;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public WaterUseDetailController.ActionParamsClass_AddWastewaterTreatmentDisposal AddWastewaterTreatmentDisposalParams
    {
      get
      {
        return WaterUseDetailController.s_params_AddWastewaterTreatmentDisposal;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public WaterUseDetailController.ViewsClass Views
    {
      get
      {
        return WaterUseDetailController.s_views;
      }
    }

    public WaterUseDetailController(IUpdatableService<PermitWwLivestock> permitWwLivestockService, IUpdatableService<PermitWwNonAgricultureIrrigation> permitWwNonAgricultureIrrigationService, IService<LU_WaterWithdrawalPurposeCategory> WaterWithdrawalPurposeCategory, IUpdatableService<PermitWwPoultry> permitWaterWithdrawalPoultryService, IUpdatableService<PermitWwCrop> permitWwCropService, IWaterUseDetailServiceClient waterUseDetailsServiceClient, IUpdatableService<LU_CropYieldUnit> cropYieldUnitService, IService<PermitWwPrivateWaterSupplier> permitWaterWithdrawalPrivateWaterSupplierService, IUpdatableService<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeService, IService<PermitWastewaterTreatmentDisposal> permitWastewaterTreatmentDisposalService)
    {
      this._permitWwLivestockService = permitWwLivestockService;
      this._permitWwNonAgricultureIrrigationService = permitWwNonAgricultureIrrigationService;
      this._WaterWithdrawalPurposeCategory = WaterWithdrawalPurposeCategory;
      this._waterUseDetailsServiceClient = waterUseDetailsServiceClient;
      this._permitWaterWithdrawalPoultryService = permitWaterWithdrawalPoultryService;
      this._permitWwCropService = permitWwCropService;
      this._cropYieldUnitService = cropYieldUnitService;
      this._permitWaterWithdrawalPrivateWaterSupplierService = permitWaterWithdrawalPrivateWaterSupplierService;
      this._permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
      this._permitWastewaterTreatmentDisposalService = permitWastewaterTreatmentDisposalService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected WaterUseDetailController(Dummy d)
    {
    }

    [HttpGet]
    public virtual ActionResult LoadWaterUseCategoryAndType()
    {
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseCategoryAndType, (object) this._WaterWithdrawalPurposeCategory.GetAll("LU_WaterWithdrawalPurpose"));
    }

    [HttpGet]
    public virtual ActionResult LoadWaterUseDetails(int? permitId, string selectedWaterUserCategoryType)
    {
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetails, (object) new ApplicationWizard() { WaterUseDetailForm = this._waterUseDetailsServiceClient.InitializeWaterUseDetailFrom(Convert.ToInt32((object) permitId)) });
    }

    [HttpGet]
    public virtual ActionResult AddPoultryWateringEvaporative(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.PoultryWateringEvaporative, (object) Mapper.Map<PermitWwPoultry, PoultryWateringForm>(new PermitWwPoultry()));
    }

    [HttpGet]
    public virtual ActionResult AddPoultryWateringFogger(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.PoultryWateringFogger, (object) Mapper.Map<PermitWwPoultry, PoultryWateringForm>(new PermitWwPoultry()));
    }

    [HttpGet]
    public virtual ActionResult AddLivestockWatering(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.LivestockWatering, (object) Mapper.Map<PermitWwLivestock, LivestockForm>(new PermitWwLivestock()));
    }

    [HttpGet]
    public virtual ActionResult AddOtherLivestockWatering(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.OtherLivestockWatering, (object) Mapper.Map<PermitWwLivestock, LivestockForm>(new PermitWwLivestock()));
    }

    [HttpGet]
    public virtual ActionResult AddDairyAnimalWateing(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.DairyAnimalWatering, (object) Mapper.Map<PermitWwLivestock, LivestockForm>(new PermitWwLivestock()));
    }

    [HttpGet]
    public virtual ActionResult AddCrop(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.Crop, (object) Mapper.Map<PermitWwCrop, CropForm>(new PermitWwCrop()));
    }

    [HttpGet]
    public virtual ActionResult AddNurseryStock(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.NurseryStock, (object) Mapper.Map<PermitWwAgricultural, PermitWwIrrigationForm>(new PermitWwAgricultural()));
    }

    [HttpGet]
    public virtual ActionResult AddSod(int index)
    {
      this.ViewData["index"] = (object) index;
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.Sod, (object) Mapper.Map<PermitWwAgricultural, PermitWwIrrigationForm>(new PermitWwAgricultural()));
    }

    [HttpGet]
    public virtual ActionResult AddPrivateWaterSupply(string waterUseCategoryTypeids, int permitId)
    {
      List<PermitWwPrivateWaterSupplierForm> waterSupplierForms = this._waterUseDetailsServiceClient.GetPrivateWaterSupplierForms(waterUseCategoryTypeids, permitId);
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.PrivateWaterSuppliers, (object) new WaterUseDetailForm() { PrivateWaterSuppilerForm = { PrivateWaterSupplierForms = waterSupplierForms } });
    }

    [HttpGet]
    public virtual ActionResult AddWastewaterTreatmentDisposal(string waterUseCategoryTypeids, int permitId)
    {
      List<WastewaterTreatmentAndDisposalForm> treatmentAndDisposal = this._waterUseDetailsServiceClient.GetWasteWaterTreatmentAndDisposal(waterUseCategoryTypeids, permitId);
      return (ActionResult) this.PartialView(MVC.Permitting.Permit.Views.WastewaterTreatmentAndDisposal, (object) new ApplicationWizard() { WastewaterTreatmentDisposalForm = treatmentAndDisposal });
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult LoadWaterUseDetails()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadWaterUseDetails, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual ActionResult AddPoultryWateringEvaporative()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddPoultryWateringEvaporative, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult AddPoultryWateringFogger()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddPoultryWateringFogger, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult AddLivestockWatering()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddLivestockWatering, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult AddOtherLivestockWatering()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddOtherLivestockWatering, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult AddDairyAnimalWateing()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddDairyAnimalWateing, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult AddCrop()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddCrop, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult AddNurseryStock()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddNurseryStock, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult AddSod()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddSod, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult AddPrivateWaterSupply()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddPrivateWaterSupply, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual ActionResult AddWastewaterTreatmentDisposal()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddWastewaterTreatmentDisposal, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string LoadWaterUseCategoryAndType = "LoadWaterUseCategoryAndType";
      public readonly string LoadWaterUseDetails = "LoadWaterUseDetails";
      public readonly string AddPoultryWateringEvaporative = "AddPoultryWateringEvaporative";
      public readonly string AddPoultryWateringFogger = "AddPoultryWateringFogger";
      public readonly string AddLivestockWatering = "AddLivestockWatering";
      public readonly string AddOtherLivestockWatering = "AddOtherLivestockWatering";
      public readonly string AddDairyAnimalWateing = "AddDairyAnimalWateing";
      public readonly string AddCrop = "AddCrop";
      public readonly string AddNurseryStock = "AddNurseryStock";
      public readonly string AddSod = "AddSod";
      public readonly string AddPrivateWaterSupply = "AddPrivateWaterSupply";
      public readonly string AddWastewaterTreatmentDisposal = "AddWastewaterTreatmentDisposal";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string LoadWaterUseCategoryAndType = "LoadWaterUseCategoryAndType";
      public const string LoadWaterUseDetails = "LoadWaterUseDetails";
      public const string AddPoultryWateringEvaporative = "AddPoultryWateringEvaporative";
      public const string AddPoultryWateringFogger = "AddPoultryWateringFogger";
      public const string AddLivestockWatering = "AddLivestockWatering";
      public const string AddOtherLivestockWatering = "AddOtherLivestockWatering";
      public const string AddDairyAnimalWateing = "AddDairyAnimalWateing";
      public const string AddCrop = "AddCrop";
      public const string AddNurseryStock = "AddNurseryStock";
      public const string AddSod = "AddSod";
      public const string AddPrivateWaterSupply = "AddPrivateWaterSupply";
      public const string AddWastewaterTreatmentDisposal = "AddWastewaterTreatmentDisposal";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadWaterUseDetails
    {
      public readonly string permitId = "permitId";
      public readonly string selectedWaterUserCategoryType = "selectedWaterUserCategoryType";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddPoultryWateringEvaporative
    {
      public readonly string index = "index";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AddPoultryWateringFogger
    {
      public readonly string index = "index";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AddLivestockWatering
    {
      public readonly string index = "index";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AddOtherLivestockWatering
    {
      public readonly string index = "index";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AddDairyAnimalWateing
    {
      public readonly string index = "index";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddCrop
    {
      public readonly string index = "index";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AddNurseryStock
    {
      public readonly string index = "index";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_AddSod
    {
      public readonly string index = "index";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddPrivateWaterSupply
    {
      public readonly string waterUseCategoryTypeids = "waterUseCategoryTypeids";
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_AddWastewaterTreatmentDisposal
    {
      public readonly string waterUseCategoryTypeids = "waterUseCategoryTypeids";
      public readonly string permitId = "permitId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly WaterUseDetailController.ViewsClass._ViewNamesClass s_ViewNames = new WaterUseDetailController.ViewsClass._ViewNamesClass();

      public WaterUseDetailController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return WaterUseDetailController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
      }
    }
  }
}
