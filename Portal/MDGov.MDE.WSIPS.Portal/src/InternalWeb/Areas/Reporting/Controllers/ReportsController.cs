﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Reporting.Controllers.ReportsController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Reporting.Controllers
{
  [Authorization]
  public class ReportsController : BaseController
  {
    private static readonly ReportsController.ActionNamesClass s_actions = new ReportsController.ActionNamesClass();
    private static readonly ReportsController.ActionParamsClass_PumpageReport s_params_PumpageReport = new ReportsController.ActionParamsClass_PumpageReport();
    private static readonly ReportsController.ViewsClass s_views = new ReportsController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "Reporting";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Reports";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Reports";
    private readonly IPermitService _permittingService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ReportsController Actions
    {
      get
      {
        return MVC.Reporting.Reports;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ReportsController.ActionNamesClass ActionNames
    {
      get
      {
        return ReportsController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ReportsController.ActionParamsClass_PumpageReport PumpageReportParams
    {
      get
      {
        return ReportsController.s_params_PumpageReport;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ReportsController.ViewsClass Views
    {
      get
      {
        return ReportsController.s_views;
      }
    }

    public ReportsController(IPermitService permittingService)
    {
      this._permittingService = permittingService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected ReportsController(Dummy d)
    {
    }

    public virtual ActionResult Index()
    {
      return (ActionResult) this.View();
    }

    [HttpGet]
    public virtual JsonResult PumpageReport(int permitId)
    {
      Permit byId = this._permittingService.GetById(permitId, (string) null);
      if (byId == null)
        return this.Json((object) new{ showReport = false }, JsonRequestBehavior.AllowGet);
      if (string.IsNullOrEmpty(byId.PermitNumber))
        return this.Json((object) new{ showReport = false }, JsonRequestBehavior.AllowGet);
      return this.Json((object) new{ showReport = true, Url = ("http://" + ConfigurationManager.AppSettings["ReportsServer"].ToString() + "/ReportServer/Pages/ReportViewer.aspx?%2fWSIPS%2fPumpageReportSummaryByPermit&PermitName=" + byId.PermitName + "&rs:Command=Render") }, JsonRequestBehavior.AllowGet);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult PumpageReport()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.PumpageReport, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string PumpageReport = "PumpageReport";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string PumpageReport = "PumpageReport";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_PumpageReport
    {
      public readonly string permitId = "permitId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly ReportsController.ViewsClass._ViewNamesClass s_ViewNames = new ReportsController.ViewsClass._ViewNamesClass();
      public readonly string Index = "~/Areas/Reporting/Views/Reports/Index.cshtml";

      public ReportsController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return ReportsController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string Index = "Index";
      }
    }
  }
}
