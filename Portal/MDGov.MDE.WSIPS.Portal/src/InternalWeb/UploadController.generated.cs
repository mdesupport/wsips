﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.T4MVC_UploadController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Permitting.Model;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [DebuggerNonUserCode]
  [GeneratedCode("T4MVC", "2.0")]
  public class T4MVC_UploadController : UploadController
  {
    public T4MVC_UploadController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult Document(string refTable, int refId, int? permitStatusId, bool includeRefIdDocs)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Document, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "refTable", (object) refTable);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "refId", (object) refId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitStatusId", (object) permitStatusId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "includeRefIdDocs", (object) includeRefIdDocs);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override FileResult GetFile(int id)
    {
      T4MVC_System_Web_Mvc_FileResult webMvcFileResult = new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.GetFile, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcFileResult.RouteValueDictionary, "id", (object) id);
      return (FileResult) webMvcFileResult;
    }

    [NonAction]
    public override FileResult GetFileData(int id, string title)
    {
      T4MVC_System_Web_Mvc_FileResult webMvcFileResult = new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.GetFileData, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcFileResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcFileResult.RouteValueDictionary, "title", (object) title);
      return (FileResult) webMvcFileResult;
    }

    [NonAction]
    public override JsonResult Upload(Document doc)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Upload, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "doc", (object) doc);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Data(int id, int refTableId, int permitStatusId, bool includeRefIdDocs)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "refTableId", (object) refTableId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitStatusId", (object) permitStatusId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "includeRefIdDocs", (object) includeRefIdDocs);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Delete(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }
  }
}
