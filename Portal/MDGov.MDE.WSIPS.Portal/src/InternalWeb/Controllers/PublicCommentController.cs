﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers.PublicCommentController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers
{
  [Authorization]
  public class PublicCommentController : BaseController
  {
    private static readonly PublicCommentController.ActionNamesClass s_actions = new PublicCommentController.ActionNamesClass();
    private static readonly PublicCommentController.ActionParamsClass_Data s_params_Data = new PublicCommentController.ActionParamsClass_Data();
    private static readonly PublicCommentController.ActionParamsClass_Delete s_params_Delete = new PublicCommentController.ActionParamsClass_Delete();
    private static readonly PublicCommentController.ActionParamsClass_GetComment s_params_GetComment = new PublicCommentController.ActionParamsClass_GetComment();
    private static readonly PublicCommentController.ViewsClass s_views = new PublicCommentController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "PublicComment";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "PublicComment";
    private readonly IUpdatableService<PublicComment> _publicCommentService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PublicCommentController Actions
    {
      get
      {
        return MVC.PublicComment;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PublicCommentController.ActionNamesClass ActionNames
    {
      get
      {
        return PublicCommentController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PublicCommentController.ActionParamsClass_Data DataParams
    {
      get
      {
        return PublicCommentController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PublicCommentController.ActionParamsClass_Delete DeleteParams
    {
      get
      {
        return PublicCommentController.s_params_Delete;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public PublicCommentController.ActionParamsClass_GetComment GetCommentParams
    {
      get
      {
        return PublicCommentController.s_params_GetComment;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public PublicCommentController.ViewsClass Views
    {
      get
      {
        return PublicCommentController.s_views;
      }
    }

    public PublicCommentController(IUpdatableService<PublicComment> publicCommentService)
    {
      this._publicCommentService = publicCommentService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected PublicCommentController(Dummy d)
    {
    }

    [HttpGet]
    public virtual JsonResult Data(int id)
    {
      return this.Json((object) new
      {
        Data = this._publicCommentService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("PermitId==@0", new object[1]
          {
            (object) id
          })
        }, (string) null).Select(x => new
        {
          Comment = x.Comment.Length > 50 ? x.Comment.Substring(0, 50) + "..." : x.Comment,
          LastModifiedDate = Convert.ToDateTime(x.LastModifiedDate).ToShortDateString(),
          Name = x.Name,
          Id = x.Id
        })
      }, JsonRequestBehavior.AllowGet);
    }

    public virtual JsonResult Delete(int id)
    {
      try
      {
        this._publicCommentService.Delete(id);
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    public virtual JsonResult GetComment(int id)
    {
      return this.Json((object) new
      {
        comment = this._publicCommentService.GetById(id, (string) null)
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual void SaveComment(PublicComment publicComment)
    {
      this._publicCommentService.Save(publicComment);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Delete()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult GetComment()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetComment, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string Data = "Data";
      public readonly string Delete = "Delete";
      public readonly string GetComment = "GetComment";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Data = "Data";
      public const string Delete = "Delete";
      public const string GetComment = "GetComment";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Delete
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetComment
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly PublicCommentController.ViewsClass._ViewNamesClass s_ViewNames = new PublicCommentController.ViewsClass._ViewNamesClass();

      public PublicCommentController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return PublicCommentController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
      }
    }
  }
}
