﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers.HomeController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers
{
  [Authorization]
  public class HomeController : BaseController
  {
    private static readonly HomeController.ActionNamesClass s_actions = new HomeController.ActionNamesClass();
    private static readonly HomeController.ViewsClass s_views = new HomeController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Home";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Home";
    private readonly IService<ContactCommunicationMethod> _contactCommunicationMethodService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public HomeController Actions
    {
      get
      {
        return MVC.Home;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public HomeController.ActionNamesClass ActionNames
    {
      get
      {
        return HomeController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public HomeController.ViewsClass Views
    {
      get
      {
        return HomeController.s_views;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected HomeController(Dummy d)
    {
    }

    public HomeController(IService<ContactCommunicationMethod> contactCommunicationMethodService)
    {
      this._contactCommunicationMethodService = contactCommunicationMethodService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    public virtual ActionResult Index()
    {
        var contactId = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);

        // We need to get all communication methods and determine whether this contact
        // has an email address.  If they do not, we need to disable the email communication
        // preference in the notification area of the index page.  CommunicationMethodId 3 is
        // email
        var communicationMethods = _contactCommunicationMethodService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("ContactId == @0 && CommunicationMethodId == @1", contactId, 3) });

        ViewBag.DisabledEmail = !communicationMethods.Any();

        return View();
    }

    public virtual ActionResult Style()
    {
      return (ActionResult) this.View();
    }

    [HttpGet]
    public virtual ActionResult Map()
    {
      return (ActionResult) this.View();
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNamesClass
    {
      public readonly string Index = "Index";
      public readonly string Style = "Style";
      public readonly string Map = "Map";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNameConstants
    {
      public const string Index = "Index";
      public const string Style = "Style";
      public const string Map = "Map";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly HomeController.ViewsClass._ViewNamesClass s_ViewNames = new HomeController.ViewsClass._ViewNamesClass();
      public readonly string Index = "~/Views/Home/Index.cshtml";
      public readonly string Style = "~/Views/Home/Style.cshtml";

      public HomeController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return HomeController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string Index = "Index";
        public readonly string Style = "Style";
      }
    }
  }
}
