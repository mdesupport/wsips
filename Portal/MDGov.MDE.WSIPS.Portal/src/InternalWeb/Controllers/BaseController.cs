﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers.BaseController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using Microsoft.CSharp.RuntimeBinder;
using StructureMap;
using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using System.Web.Routing;
using T4MVC;
using System.Reflection;

using System.Linq;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers
{
  public class BaseController : Controller
  {
    private static readonly BaseController.ActionNamesClass s_actions = new BaseController.ActionNamesClass();
    private static readonly BaseController.ViewsClass s_views = new BaseController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Area = "";
    [GeneratedCode("T4MVC", "2.0")]
    public readonly string Name = "Base";
    [GeneratedCode("T4MVC", "2.0")]
    public const string NameConst = "Base";

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public BaseController Actions
    {
      get
      {
        return MVC.Base;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public BaseController.ActionNamesClass ActionNames
    {
      get
      {
        return BaseController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public BaseController.ViewsClass Views
    {
      get
      {
        return BaseController.s_views;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public BaseController()
    {
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected BaseController(Dummy d)
    {
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    protected override void Initialize(RequestContext requestContext)
    {
      base.Initialize(requestContext);

      ViewBag.Username = SessionHandler.GetSessionVar(SessionVariables.AuthenticatedUserFullName);
      ViewBag.ContactId = SessionHandler.GetSessionVar(SessionVariables.AuthenticatedUserContactId);
      ViewBag.UserAccount = SessionHandler.GetSessionVar(SessionVariables.AuthenticatedUserUsername);

      ViewBag.ProductVersion = "v" + ((AssemblyInformationalVersionAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false).FirstOrDefault()).InformationalVersion;
    }

    protected void HandleException(Exception exc)
    {
      if (exc == null)
        return;
      this.ModelState.AddModelError(string.Empty, exc.Message);
      ObjectFactory.GetInstance<IErrorLogging>().Log(exc);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly BaseController.ViewsClass._ViewNamesClass s_ViewNames = new BaseController.ViewsClass._ViewNamesClass();

      public BaseController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return BaseController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
      }
    }
  }
}
