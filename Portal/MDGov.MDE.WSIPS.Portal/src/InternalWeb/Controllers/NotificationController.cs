﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers.NotificationController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers
{
  [Authorization]
  public class NotificationController : BaseController
  {
    private static readonly NotificationController.ActionNamesClass s_actions = new NotificationController.ActionNamesClass();
    private static readonly NotificationController.ActionParamsClass_MarkNotifcationRead s_params_MarkNotifcationRead = new NotificationController.ActionParamsClass_MarkNotifcationRead();
    private static readonly NotificationController.ActionParamsClass_SaveNotificationPreference s_params_SaveNotificationPreference = new NotificationController.ActionParamsClass_SaveNotificationPreference();
    private static readonly NotificationController.ViewsClass s_views = new NotificationController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Notification";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Notification";
    private readonly IUpdatableService<Message> _messageService;
    private readonly IService<Notification> _notificationService;
    private readonly IUpdatableService<NotificationContact> _notificationContactService;
    private readonly IUpdatableService<Contact> _contactService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public NotificationController Actions
    {
      get
      {
        return MVC.Notification;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public NotificationController.ActionNamesClass ActionNames
    {
      get
      {
        return NotificationController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public NotificationController.ActionParamsClass_MarkNotifcationRead MarkNotifcationReadParams
    {
      get
      {
        return NotificationController.s_params_MarkNotifcationRead;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public NotificationController.ActionParamsClass_SaveNotificationPreference SaveNotificationPreferenceParams
    {
      get
      {
        return NotificationController.s_params_SaveNotificationPreference;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public NotificationController.ViewsClass Views
    {
      get
      {
        return NotificationController.s_views;
      }
    }

    public NotificationController(IUpdatableService<Message> messageService, IService<Notification> notificationService, IUpdatableService<NotificationContact> notificationContactService, IUpdatableService<Contact> contactService)
    {
      this._messageService = messageService;
      this._notificationService = notificationService;
      this._notificationContactService = notificationContactService;
      this._contactService = contactService;
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected NotificationController(Dummy d)
    {
    }

    [HttpGet]
    public virtual JsonResult GetNew()
    {
      if (SessionHandler.GetSessionVar(SessionVariables.AuthenticatedUserContactId) == null)
        return (JsonResult) null;
      return this.Json((object) Mapper.Map<IEnumerable<Notification>, IEnumerable<UserNotification>>(this._notificationService.GetRange(0, int.MaxValue, "CreatedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[2]
      {
        new DynamicFilter("Active == @0", new object[1]
        {
          (object) true
        }),
        new DynamicFilter("NotificationContacts.Any(ContactId == @0 && !IsViewed)", new object[1]
        {
          (object) SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId)
        })
      }, (string) null)), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult MarkNotifcationRead(int id)
    {
      int sessionVar = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);
      NotificationContact entity = this._notificationContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("NotificationId==@0 && ContactId==@1", new object[2]
        {
          (object) id,
          (object) sessionVar
        })
      }, (string) null).FirstOrDefault<NotificationContact>();
      entity.IsViewed = true;
      this._notificationContactService.Save(entity);
      return (JsonResult) null;
    }

    [HttpPost]
    public virtual JsonResult SaveNotificationPreference(bool email, bool notification)
    {
      Contact byId = this._contactService.GetById(SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId), "ContactCommunicationMethods");
      byId.ApplicationNotification = notification;
      if (byId.ContactCommunicationMethods.Any<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (ccm => ccm.CommunicationMethodId == 3)))
        byId.EmailNotification = email;
      this._contactService.Save(byId);
      return (JsonResult) null;
    }

    [HttpGet]
    public virtual JsonResult GetNotificationPreference()
    {
      Contact byId = this._contactService.GetById(SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId), (string) null);
      return this.Json((object) new
      {
        email = (byId != null && byId.EmailNotification),
        notification = (byId != null && byId.ApplicationNotification)
      }, JsonRequestBehavior.AllowGet);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult MarkNotifcationRead()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.MarkNotifcationRead, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult SaveNotificationPreference()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SaveNotificationPreference, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string GetNew = "GetNew";
      public readonly string MarkNotifcationRead = "MarkNotifcationRead";
      public readonly string SaveNotificationPreference = "SaveNotificationPreference";
      public readonly string GetNotificationPreference = "GetNotificationPreference";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string GetNew = "GetNew";
      public const string MarkNotifcationRead = "MarkNotifcationRead";
      public const string SaveNotificationPreference = "SaveNotificationPreference";
      public const string GetNotificationPreference = "GetNotificationPreference";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_MarkNotifcationRead
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_SaveNotificationPreference
    {
      public readonly string email = "email";
      public readonly string notification = "notification";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly NotificationController.ViewsClass._ViewNamesClass s_ViewNames = new NotificationController.ViewsClass._ViewNamesClass();

      public NotificationController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return NotificationController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
      }
    }
  }
}
