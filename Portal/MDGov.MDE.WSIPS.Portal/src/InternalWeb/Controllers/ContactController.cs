﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers.ContactController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.InternalWeb.ServiceClient;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers
{
  [Authorization]
  public class ContactController : BaseController
  {
    private static readonly ContactController.ActionNamesClass s_actions = new ContactController.ActionNamesClass();
    private static readonly ContactController.ActionParamsClass_Data s_params_Data = new ContactController.ActionParamsClass_Data();
    private static readonly ContactController.ActionParamsClass_GetPermitContact s_params_GetPermitContact = new ContactController.ActionParamsClass_GetPermitContact();
    private static readonly ContactController.ActionParamsClass_SavePermitContact s_params_SavePermitContact = new ContactController.ActionParamsClass_SavePermitContact();
    private static readonly ContactController.ActionParamsClass_MergeContacts s_params_MergeContacts = new ContactController.ActionParamsClass_MergeContacts();
    private static readonly ContactController.ActionParamsClass_Delete s_params_Delete = new ContactController.ActionParamsClass_Delete();
    private static readonly ContactController.ActionParamsClass_GetContact s_params_GetContact = new ContactController.ActionParamsClass_GetContact();
    private static readonly ContactController.ActionParamsClass_GetZipCodeAutoPopulateValues s_params_GetZipCodeAutoPopulateValues = new ContactController.ActionParamsClass_GetZipCodeAutoPopulateValues();
    private static readonly ContactController.ViewsClass s_views = new ContactController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Contact";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Contact";
    private readonly IContactService _contactService;
    private readonly IUpdatableService<PermitContact> _permitContactService;
    private readonly IUpdatableService<PermitContactInformation> _permitContactInformationService;
    private readonly IContactServiceClient _contactServiceClient;
    private readonly IService<LU_ZipCode> _zipCodeService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ContactController Actions
    {
      get
      {
        return MVC.Contact;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ContactController.ActionNamesClass ActionNames
    {
      get
      {
        return ContactController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ContactController.ActionParamsClass_Data DataParams
    {
      get
      {
        return ContactController.s_params_Data;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ContactController.ActionParamsClass_GetPermitContact GetPermitContactParams
    {
      get
      {
        return ContactController.s_params_GetPermitContact;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ContactController.ActionParamsClass_SavePermitContact SavePermitContactParams
    {
      get
      {
        return ContactController.s_params_SavePermitContact;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ContactController.ActionParamsClass_MergeContacts MergeContactsParams
    {
      get
      {
        return ContactController.s_params_MergeContacts;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ContactController.ActionParamsClass_Delete DeleteParams
    {
      get
      {
        return ContactController.s_params_Delete;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ContactController.ActionParamsClass_GetContact GetContactParams
    {
      get
      {
        return ContactController.s_params_GetContact;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public ContactController.ActionParamsClass_GetZipCodeAutoPopulateValues GetZipCodeAutoPopulateValuesParams
    {
      get
      {
        return ContactController.s_params_GetZipCodeAutoPopulateValues;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public ContactController.ViewsClass Views
    {
      get
      {
        return ContactController.s_views;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected ContactController(Dummy d)
    {
    }

    public ContactController(IContactService contactService, IUpdatableService<PermitContact> permitContactService, IUpdatableService<PermitContactInformation> permitContactInformationService, IContactServiceClient contactServiceClient, IService<LU_ZipCode> zipCodeService)
    {
      this._contactService = contactService;
      this._permitContactService = permitContactService;
      this._permitContactInformationService = permitContactInformationService;
      this._contactServiceClient = contactServiceClient;
      this._zipCodeService = zipCodeService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult GetPermitContact()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.GetPermitContact, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [NonAction]
    [DebuggerNonUserCode]
    public virtual JsonResult SavePermitContact()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SavePermitContact, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult Delete()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual ActionResult GetContact()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.GetContact, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult GetZipCodeAutoPopulateValues()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetZipCodeAutoPopulateValues, (string) null);
    }

    [HttpGet]
    public virtual JsonResult Data(int id, int contactTypeId)
    {
      return this.Json((object) new
      {
        Data = this._permitContactService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("PermitId==@0 && ContactTypeId==@1", new object[2]
          {
            (object) id,
            (object) contactTypeId
          })
        }, "PermitContactInformation,Contact.ContactCommunicationMethods,Contact.LU_State").Select(x => new
        {
          FirstName = x.PermitContactInformation == null ? "" : x.PermitContactInformation.FirstName,
          LastName = x.PermitContactInformation == null ? "" : x.PermitContactInformation.LastName,
          BusinessName = x.PermitContactInformation == null ? "" : x.PermitContactInformation.BusinessName,
          Address1 = ContactController.FormatAddress(x.PermitContactInformation),
          Id = x.Id,
          ContactId = x.ContactId,
          Email = x.Contact == null ? "" : (x.Contact.ContactCommunicationMethods == null ? "" : (x.Contact.ContactCommunicationMethods.Any<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (y =>
          {
            if (y.IsPrimaryEmail.HasValue)
              return y.IsPrimaryEmail.Value;
            return false;
          })) ? x.Contact.ContactCommunicationMethods.Single<ContactCommunicationMethod>((Func<ContactCommunicationMethod, bool>) (y =>
          {
            if (y.IsPrimaryEmail.HasValue)
              return y.IsPrimaryEmail.Value;
            return false;
          })).CommunicationValue : ""))
        })
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual ActionResult GetPermitContact(int permitContactId)
    {
      return (ActionResult) this.PartialView((object) Mapper.Map<PermitContact, ContactForm>(this._permitContactService.GetById(permitContactId, "PermitContactInformation,Contact.ContactCommunicationMethods")));
    }

    [HttpPost]
    public virtual JsonResult SavePermitContact(int permitId, ContactForm contactForm)
    {
      if (this.ModelState.IsValid)
      {
        try
        {
          this._contactServiceClient.SavePermitContact(permitId, contactForm);
          return this.Json((object) new{ success = true });
        }
        catch
        {
        }
      }
      return this.Json((object) new{ success = false });
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator", "Administrative Specialist"})]
    [HttpGet]
    public virtual ActionResult MergeContacts()
    {
      return (ActionResult) this.View();
    }

    [Authorization(AuthorizedRoles = new string[] {"IT Administrator", "Administrative Specialist"})]
    [HttpPost]
    public virtual JsonResult MergeContacts(int fromContactId, int toContactid)
    {
      this._contactService.MergeContacts(fromContactId, toContactid);
      return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public virtual JsonResult Delete(int id)
    {
      try
      {
        this._permitContactInformationService.Delete(this._permitContactService.GetById(id, "PermitContactInformation").PermitContactInformationId.GetValueOrDefault());
        return this.Json((object) new{ success = true });
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    [HttpGet]
    public virtual ActionResult GetContact(int? contactId)
    {
      Contact source = new Contact();
      if (contactId.HasValue && contactId.Value > 0)
        source = this._contactService.GetById(contactId.Value, "ContactCommunicationMethods");
      return (ActionResult) this.PartialView((object) Mapper.Map<Contact, ContactForm>(source));
    }

    [HttpGet]
    public virtual JsonResult GetZipCodeAutoPopulateValues(string zipCode)
    {
      LU_ZipCode luZipCode = this._zipCodeService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("ZipCode == @0", new object[1]
        {
          (object) zipCode
        })
      }, (string) null).SingleOrDefault<LU_ZipCode>();
      return this.Json(luZipCode != null ? (object) new
      {
        City = luZipCode.City,
        StateId = luZipCode.StateId
      } : (object) null, JsonRequestBehavior.AllowGet);
    }

    private static string FormatAddress(PermitContactInformation address)
    {
      if (address == null)
        return "";
      return (address.Address1 == null ? "" : (address.Address1.Trim().Length > 0 ? address.Address1.Trim() + ", " : "")) + (address.Address2 == null ? "" : (address.Address2.Trim().Length > 0 ? address.Address2.Trim() + ", " : "")) + (address.City == null ? "" : (address.City.Trim().Length > 0 ? address.City.Trim() + ", " : "")) + (!address.StateId.HasValue ? "" : ((object) address.LU_State == null ? "" : address.LU_State.Description + ", ")) + (address.ZipCode ?? "");
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNamesClass
    {
      public readonly string Data = "Data";
      public readonly string GetPermitContact = "GetPermitContact";
      public readonly string SavePermitContact = "SavePermitContact";
      public readonly string MergeContacts = "MergeContacts";
      public readonly string Delete = "Delete";
      public readonly string GetContact = "GetContact";
      public readonly string GetZipCodeAutoPopulateValues = "GetZipCodeAutoPopulateValues";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionNameConstants
    {
      public const string Data = "Data";
      public const string GetPermitContact = "GetPermitContact";
      public const string SavePermitContact = "SavePermitContact";
      public const string MergeContacts = "MergeContacts";
      public const string Delete = "Delete";
      public const string GetContact = "GetContact";
      public const string GetZipCodeAutoPopulateValues = "GetZipCodeAutoPopulateValues";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string id = "id";
      public readonly string contactTypeId = "contactTypeId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetPermitContact
    {
      public readonly string permitContactId = "permitContactId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_SavePermitContact
    {
      public readonly string permitId = "permitId";
      public readonly string contactForm = "contactForm";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_MergeContacts
    {
      public readonly string fromContactId = "fromContactId";
      public readonly string toContactid = "toContactid";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Delete
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_GetContact
    {
      public readonly string contactId = "contactId";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetZipCodeAutoPopulateValues
    {
      public readonly string zipCode = "zipCode";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly ContactController.ViewsClass._ViewNamesClass s_ViewNames = new ContactController.ViewsClass._ViewNamesClass();
      public readonly string GetContact = "~/Views/Contact/GetContact.cshtml";
      public readonly string GetPermitContact = "~/Views/Contact/GetPermitContact.cshtml";
      public readonly string MergeContactPartial = "~/Views/Contact/MergeContactPartial.cshtml";
      public readonly string MergeContacts = "~/Views/Contact/MergeContacts.cshtml";

      public ContactController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return ContactController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string GetContact = "GetContact";
        public readonly string GetPermitContact = "GetPermitContact";
        public readonly string MergeContactPartial = "MergeContactPartial";
        public readonly string MergeContacts = "MergeContacts";
      }
    }
  }
}
