﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.UploadController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [Authorization]
  public class UploadController : BaseController
  {
    private static readonly UploadController.ActionNamesClass s_actions = new UploadController.ActionNamesClass();
    private static readonly UploadController.ActionParamsClass_Document s_params_Document = new UploadController.ActionParamsClass_Document();
    private static readonly UploadController.ActionParamsClass_GetFile s_params_GetFile = new UploadController.ActionParamsClass_GetFile();
    private static readonly UploadController.ActionParamsClass_GetFileData s_params_GetFileData = new UploadController.ActionParamsClass_GetFileData();
    private static readonly UploadController.ActionParamsClass_Upload s_params_Upload = new UploadController.ActionParamsClass_Upload();
    private static readonly UploadController.ActionParamsClass_Data s_params_Data = new UploadController.ActionParamsClass_Data();
    private static readonly UploadController.ActionParamsClass_Delete s_params_Delete = new UploadController.ActionParamsClass_Delete();
    private static readonly UploadController.ViewsClass s_views = new UploadController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Upload";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Upload";
    private readonly IService<LU_TableList> _tableListService;
    private readonly IUpdatableService<Document> _documentService;
    private readonly IUpdatableService<DocumentByte> _documentByteService;
    private readonly IService<Permit> _permitService;
    private readonly IUpdatableService<ConditionCompliance> _conditionComplianceService;

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UploadController Actions
    {
      get
      {
        return MVC.Upload;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UploadController.ActionNamesClass ActionNames
    {
      get
      {
        return UploadController.s_actions;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UploadController.ActionParamsClass_Document DocumentParams
    {
      get
      {
        return UploadController.s_params_Document;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UploadController.ActionParamsClass_GetFile GetFileParams
    {
      get
      {
        return UploadController.s_params_GetFile;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UploadController.ActionParamsClass_GetFileData GetFileDataParams
    {
      get
      {
        return UploadController.s_params_GetFileData;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UploadController.ActionParamsClass_Upload UploadParams
    {
      get
      {
        return UploadController.s_params_Upload;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public UploadController.ActionParamsClass_Data DataParams
    {
      get
      {
        return UploadController.s_params_Data;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UploadController.ActionParamsClass_Delete DeleteParams
    {
      get
      {
        return UploadController.s_params_Delete;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public UploadController.ViewsClass Views
    {
      get
      {
        return UploadController.s_views;
      }
    }

    public UploadController(IService<LU_TableList> tableListService, IUpdatableService<Document> documentService, IUpdatableService<DocumentByte> documentByteService, IService<Permit> permitService, IUpdatableService<ConditionCompliance> conditionComplianceService)
    {
      this._tableListService = tableListService;
      this._documentService = documentService;
      this._documentByteService = documentByteService;
      this._permitService = permitService;
      this._conditionComplianceService = conditionComplianceService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected UploadController(Dummy d)
    {
    }

    public virtual ActionResult Document(string refTable, int refId, int? permitStatusId, bool includeRefIdDocs = false)
    {
      IEnumerable<LU_TableList> range = this._tableListService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter(string.Format("Description==\"{0}\"", (object) refTable), new object[0]) }, (string) null);
      ViewBag.RefTableId = range.First<LU_TableList>().Id;
      ViewBag.RefId = refId;
      ViewBag.PermitStatusId = permitStatusId;
      ViewBag.IncludeRefIdDocs = includeRefIdDocs;

      // set permissions for deletion and upload
      // 1= View Only
      // 2= Upload and View
      // 3= upload, view, and delete
      ViewBag.PermissionLevel = 3;

      return (ActionResult)this.PartialView(MVC.Upload.Views.UploadDocument);
    }

    private string GetDirectory(Document doc)
    {
      string str = WebConfigurationManager.AppSettings["UploadFolder"].ToString();
      if (!doc.PermitStatusId.HasValue)
      {
        if (!Directory.Exists(str + (object) doc.RefTableId + "\\" + (object) doc.RefId))
          Directory.CreateDirectory(str + (object) doc.RefTableId + "\\" + (object) doc.RefId);
        return doc.RefTableId.ToString() + "\\" + (object) doc.RefId;
      }
      if (!Directory.Exists(str + (object) doc.RefTableId + "\\" + (object) doc.RefId + "\\" + (object) doc.PermitStatusId))
        Directory.CreateDirectory(str + (object) doc.RefTableId + "\\" + (object) doc.RefId + "\\" + (object) doc.PermitStatusId);
      return doc.RefTableId.ToString() + "\\" + (object) doc.RefId + "\\" + (object) doc.PermitStatusId;
    }

    [HttpGet]
    public virtual FileResult GetFile(int id)
    {
      Document document = this._documentService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("Id == @0", new object[1]{ (object) id }) }, "DocumentByte").First<Document>();
      return (FileResult) this.File(document.DocumentByte.Document, document.DocumentByte.MimeType, document.DocumentTitle);
    }

    [HttpGet]
    public virtual FileResult GetFileData(int id, string title)
    {
      DocumentByte byId = this._documentByteService.GetById(id, (string) null);
      return (FileResult) this.File(byId.Document, byId.MimeType, title);
    }

    [HttpGet]
    public virtual string GetPermitDocument(int permitId)
    {
      List<Document> list = this._documentService.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter(string.Format("RefId=={0} and DocumentTypeId=={1}", (object) permitId, (object) 3), new object[0]) }, (string) null).ToList<Document>();
      if (list.Any<Document>())
        return ConfigurationManager.AppSettings["DocumentGetPath"] + (object) list.Last<Document>().Id;
      return string.Empty;
    }

    [HttpPost]
    public virtual JsonResult Upload(Document doc)
    {
      if (doc.RefId == 0 || this.Request.Files[0].ContentLength == 0)
        return this.Json((object) new{ IsSuccess = false }, "text/html");
      DocumentByte entity = new DocumentByte();
      if (doc.Id > 0)
      {
        Document byId = this._documentService.GetById(doc.Id, (string) null);
        if (byId == null)
          return this.Json((object) new{ IsSuccess = false }, "text/html");
        entity.Id = byId.DocumentByteId;
      }
      doc.DocumentTitle = Uri.EscapeDataString(Path.GetFileName(this.Request.Files[0].FileName));
      if (!doc.DocumentTypeId.HasValue && doc.DocumentTitle.Length > 10 && doc.DocumentTitle.Substring(0, 11).ToLower() == "draftpermit")
        doc.DocumentTypeId = new int?(1);
      HttpPostedFileBase file = this.Request.Files[0];
      MemoryStream memoryStream = new MemoryStream();
      file.InputStream.CopyTo((Stream) memoryStream);
      entity.Document = memoryStream.ToArray();
      entity.MimeType = file.ContentType;
      doc.DocumentByteId = this._documentByteService.Save(entity);
      this._documentService.Save(doc);
      if (doc.RefTableId == 69)
      {
        ConditionCompliance byId = this._conditionComplianceService.GetById(doc.RefId, (string) null);
        byId.PermitConditionComplianceStatusId = 5;
        this._conditionComplianceService.Save(byId);
      }
      return this.Json((object) new{ IsSuccess = true }, "text/html");
    }

    [HttpGet]
    public virtual JsonResult Data(int id, int refTableId, int permitStatusId, bool includeRefIdDocs = false)
    {
      List<List<int>> source = new List<List<int>>();
      source.Add(new List<int>()
      {
        22,
        23,
        32,
        33,
        41,
        42,
        59,
        62,
        65
      });
      source.Add(new List<int>() { 9, 10 });
      List<int> intList1 = new List<int>() { id };
      if (includeRefIdDocs)
      {
        Permit byId1 = this._permitService.GetById(id, (string) null);
        if (byId1 != null && byId1.RefId.HasValue)
        {
          Permit byId2 = this._permitService.GetById(byId1.RefId.Value, (string) null);
          if (byId2 != null)
          {
            int? permitStatusId1 = byId2.PermitStatusId;
            if ((permitStatusId1.GetValueOrDefault() != 3 ? 0 : (permitStatusId1.HasValue ? 1 : 0)) != 0)
              intList1.Add(byId1.RefId.Value);
          }
        }
      }
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      dynamicFilterList.Add(new DynamicFilter("@0.Contains(outerIt.RefId)", new object[1]
      {
        (object) intList1
      }));
      if (permitStatusId > 0)
      {
        List<int> intList2 = source.SingleOrDefault<List<int>>((Func<List<int>, bool>) (g => g.Contains(permitStatusId)));
        if (intList2 != null)
          dynamicFilterList.Add(new DynamicFilter("@0.Contains(outerIt.PermitStatusId)", new object[1]
          {
            (object) intList2
          }));
        else
          dynamicFilterList.Add(new DynamicFilter("PermitStatusId == @0", new object[1]
          {
            (object) permitStatusId
          }));
      }
      return this.Json((object) new{ Data = this._documentService.GetRange(0, int.MaxValue, "CreatedDate", (IEnumerable<DynamicFilter>) dynamicFilterList, "LU_PermitStatus").Select(x =>
      {
        int id1 = x.Id;
        string str1 = Uri.UnescapeDataString(x.DocumentTitle);
        int? documentTypeId1 = x.DocumentTypeId;
        string description = x.Description;
        DateTime createdDate = x.CreatedDate;
        string createdBy = x.CreatedBy;
        string str2 = x.LU_PermitStatus != null ? x.LU_PermitStatus.Description : "";
        int num1 = x.PermitStatusId ?? -1;
        int? documentTypeId2 = x.DocumentTypeId;
        int num2 = (documentTypeId2.GetValueOrDefault() != 3 ? 1 : (!documentTypeId2.HasValue ? 1 : 0)) != 0 ? 1 : (SessionHandler.GetSessionVar<IEnumerable<string>>(SessionVariables.AuthenticatedUserRoles).Any<string>((Func<string, bool>) (r => r == "Division Chief")) ? 1 : 0);
        return new{ Id = id1, DocumentTitle = str1, DocumentTypeId = documentTypeId1, Description = description, CreatedDate = createdDate, CreatedBy = createdBy, PermitStatusDescription = str2, PermitStatusId = num1, IsDeletable = num2 != 0 };
      }), DocumentGetPath = WebConfigurationManager.AppSettings["DocumentGetPath"].ToString() }, JsonRequestBehavior.AllowGet);
    }

    public virtual JsonResult Delete(int id)
    {
      try
      {
        Document byId = this._documentService.GetById(id, (string) null);
        int? documentTypeId = byId.DocumentTypeId;
        if ((documentTypeId.GetValueOrDefault() != 3 ? 0 : (documentTypeId.HasValue ? 1 : 0)) != 0 && !SessionHandler.GetSessionVar<IEnumerable<string>>(SessionVariables.AuthenticatedUserRoles).Any<string>((Func<string, bool>) (r => r == "Division Chief")))
          return this.Json((object) new{ success = false }, JsonRequestBehavior.AllowGet);
        this._documentService.Delete(id);
        this._documentByteService.Delete(byId.DocumentByteId);
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false }, JsonRequestBehavior.AllowGet);
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult Document()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Document, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual FileResult GetFile()
    {
      return (FileResult) new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.GetFile, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual FileResult GetFileData()
    {
      return (FileResult) new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.GetFileData, (string) null);
    }

    [DebuggerNonUserCode]
    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Upload()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Upload, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [NonAction]
    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public virtual JsonResult Delete()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string Document = "Document";
      public readonly string GetFile = "GetFile";
      public readonly string GetFileData = "GetFileData";
      public readonly string Upload = "Upload";
      public readonly string Data = "Data";
      public readonly string Delete = "Delete";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string Document = "Document";
      public const string GetFile = "GetFile";
      public const string GetFileData = "GetFileData";
      public const string Upload = "Upload";
      public const string Data = "Data";
      public const string Delete = "Delete";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Document
    {
      public readonly string refTable = "refTable";
      public readonly string refId = "refId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string includeRefIdDocs = "includeRefIdDocs";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetFile
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetFileData
    {
      public readonly string id = "id";
      public readonly string title = "title";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Upload
    {
      public readonly string doc = "doc";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Data
    {
      public readonly string id = "id";
      public readonly string refTableId = "refTableId";
      public readonly string permitStatusId = "permitStatusId";
      public readonly string includeRefIdDocs = "includeRefIdDocs";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Delete
    {
      public readonly string id = "id";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly UploadController.ViewsClass._ViewNamesClass s_ViewNames = new UploadController.ViewsClass._ViewNamesClass();
      public readonly string UploadDocument = "~/Views/Upload/UploadDocument.cshtml";

      public UploadController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return UploadController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string UploadDocument = "UploadDocument";
      }
    }
  }
}
