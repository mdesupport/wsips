﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers.CommentController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Filters;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using T4MVC;


namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers
{
  [Authorization]
  public class CommentController : BaseController
  {
    private static readonly CommentController.ActionNamesClass s_actions = new CommentController.ActionNamesClass();
    private static readonly CommentController.ActionParamsClass_LoadCommentsView s_params_LoadCommentsView = new CommentController.ActionParamsClass_LoadCommentsView();
    private static readonly CommentController.ActionParamsClass_Data s_params_Data = new CommentController.ActionParamsClass_Data();
    private static readonly CommentController.ActionParamsClass_Delete s_params_Delete = new CommentController.ActionParamsClass_Delete();
    private static readonly CommentController.ActionParamsClass_GetComment s_params_GetComment = new CommentController.ActionParamsClass_GetComment();
    private static readonly CommentController.ViewsClass s_views = new CommentController.ViewsClass();
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Area = "";
    [GeneratedCode("T4MVC", "2.0")]
    public new readonly string Name = "Comment";
    [GeneratedCode("T4MVC", "2.0")]
    public new const string NameConst = "Comment";
    private readonly IUpdatableService<Comment> _commentService;
    private readonly IService<LU_TableList> _tableListService;

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public CommentController Actions
    {
      get
      {
        return MVC.Comment;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public CommentController.ActionNamesClass ActionNames
    {
      get
      {
        return CommentController.s_actions;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public CommentController.ActionParamsClass_LoadCommentsView LoadCommentsViewParams
    {
      get
      {
        return CommentController.s_params_LoadCommentsView;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public CommentController.ActionParamsClass_Data DataParams
    {
      get
      {
        return CommentController.s_params_Data;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public CommentController.ActionParamsClass_Delete DeleteParams
    {
      get
      {
        return CommentController.s_params_Delete;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public CommentController.ActionParamsClass_GetComment GetCommentParams
    {
      get
      {
        return CommentController.s_params_GetComment;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public CommentController.ViewsClass Views
    {
      get
      {
        return CommentController.s_views;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected CommentController(Dummy d)
    {
    }

    public CommentController(IService<LU_TableList> tableListService, IUpdatableService<Comment> commentService)
    {
      this._commentService = commentService;
      this._tableListService = tableListService;
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    protected new RedirectToRouteResult RedirectToAction(ActionResult result)
    {
      return this.RedirectToRoute(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    protected new RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
    {
      return this.RedirectToRoutePermanent(((ActionResult) result).GetT4MVCResult().RouteValueDictionary);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual ActionResult LoadCommentsView()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadCommentsView, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Data()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
    }

    [NonAction]
    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public virtual JsonResult Delete()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    [NonAction]
    public virtual JsonResult GetComment()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetComment, (string) null);
    }

    public virtual ActionResult LoadCommentsView(string refTable, int refId, int permitStatusId)
    {
      var tableList = _tableListService.GetRange(0, 1, null, new[] { new DynamicFilter("Description==@0", refTable) });

      ViewBag.RefTableId = tableList.First<LU_TableList>().Id;
      ViewBag.RefId = refId;
      ViewBag.PermitStatusId = permitStatusId;

      return (ActionResult) this.PartialView(MVC.Comment.Views.Comment);
    }

    [HttpGet]
    public virtual JsonResult Data(int id, int permitStatus)
    {
      List<DynamicFilter> dynamicFilterList = new List<DynamicFilter>();
      if (permitStatus == -1)
        dynamicFilterList.Add(new DynamicFilter(string.Format("refId=={0}", (object) id), new object[0]));
      else
        dynamicFilterList.Add(new DynamicFilter(string.Format("refId=={0} && PermitStatusId=={1}", (object) id, (object) permitStatus), new object[0]));
      return this.Json((object) new
      {
        Data = this._commentService.GetRange(0, int.MaxValue, "PermitStatusId", (IEnumerable<DynamicFilter>) dynamicFilterList, "LU_PermitStatus").Select(x => new
        {
          Comment1 = x.Comment1.Length > 50 ? x.Comment1.Substring(0, 50) + "..." : x.Comment1,
          LastModifiedDate = x.LastModifiedDate,
          LastModifiedBy = x.LastModifiedBy,
          CreatedBy = x.CreatedBy,
          PermitStatusId = x.LU_PermitStatus != null ? x.LU_PermitStatus.Description : "",
          Id = x.Id,
          StatusId = x.PermitStatusId ?? -1
        })
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public virtual void SaveComment(Comment comment)
    {
      int? permitStatusId = comment.PermitStatusId;
      if ((permitStatusId.GetValueOrDefault() >= 1 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0)
        comment.PermitStatusId = new int?();
      this._commentService.Save(comment);
    }

    public virtual JsonResult Delete(int id)
    {
      try
      {
        this._commentService.Delete(id);
        return this.Json((object) new{ success = true }, JsonRequestBehavior.AllowGet);
      }
      catch (Exception ex)
      {
        this.HandleException(ex);
        return this.Json((object) new{ success = false });
      }
    }

    public virtual JsonResult GetComment(int id)
    {
      return this.Json((object) new
      {
        userName = System.Web.HttpContext.Current.User.Identity.Name,
        comment = this._commentService.GetById(id, (string) null)
      }, JsonRequestBehavior.AllowGet);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNamesClass
    {
      public readonly string LoadCommentsView = "LoadCommentsView";
      public readonly string Data = "Data";
      public readonly string Delete = "Delete";
      public readonly string GetComment = "GetComment";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionNameConstants
    {
      public const string LoadCommentsView = "LoadCommentsView";
      public const string Data = "Data";
      public const string Delete = "Delete";
      public const string GetComment = "GetComment";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_LoadCommentsView
    {
      public readonly string refTable = "refTable";
      public readonly string refId = "refId";
      public readonly string permitStatusId = "permitStatusId";
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ActionParamsClass_Data
    {
      public readonly string id = "id";
      public readonly string permitStatus = "permitStatus";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_Delete
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ActionParamsClass_GetComment
    {
      public readonly string id = "id";
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly CommentController.ViewsClass._ViewNamesClass s_ViewNames = new CommentController.ViewsClass._ViewNamesClass();
      public readonly string Comment = "~/Views/Comment/Comment.cshtml";

      public CommentController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return CommentController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string Comment = "Comment";
      }
    }
  }
}
