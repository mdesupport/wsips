﻿using MDGov.MDE.WSIPS.Portal.InternalWeb.App_Start;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using WebActivator;

[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: PreApplicationStartMethod(typeof (StructuremapMvc), "Start")]
[assembly: Guid("c15e72a8-932c-471c-8786-47790c8c6abf")]
[assembly: PreApplicationStartMethod(typeof (RegisterClientValidationExtensions), "Start")]
[assembly: AssemblyTitle("InternalWeb")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("JMT")]
[assembly: AssemblyProduct("InternalWeb")]
[assembly: AssemblyCopyright("Copyright © JMT 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("3.9.02.07")]
