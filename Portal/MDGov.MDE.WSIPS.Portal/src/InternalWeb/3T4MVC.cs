﻿// Decompiled with JetBrains decompiler
// Type: T4MVCHelpers
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;

[DebuggerNonUserCode]
[GeneratedCode("T4MVC", "2.0")]
internal static class T4MVCHelpers
{
  public static Func<string, string> ProcessVirtualPath = new Func<string, string>(T4MVCHelpers.ProcessVirtualPathDefault);
  public static Func<string, string> TimestampString = new Func<string, string>(T4Extensions.TimestampString);

  private static string ProcessVirtualPathDefault(string virtualPath)
  {
    return VirtualPathUtility.ToAbsolute(virtualPath);
  }

  public static bool IsProduction()
  {
    if (HttpContext.Current != null)
      return !HttpContext.Current.IsDebuggingEnabled;
    return false;
  }
}
