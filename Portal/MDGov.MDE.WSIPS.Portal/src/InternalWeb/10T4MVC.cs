﻿// Decompiled with JetBrains decompiler
// Type: T4MVC.PermittingClass
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers;
using System.CodeDom.Compiler;
using System.Diagnostics;

namespace T4MVC
{
  [GeneratedCode("T4MVC", "2.0")]
  [DebuggerNonUserCode]
  public class PermittingClass
  {
    public readonly string Name = "Permitting";
    public ChecklistController Checklist = (ChecklistController) new T4MVC_ChecklistController();
    public ConditionController Condition = (ConditionController) new T4MVC_ConditionController();
    public DashboardController Dashboard = (DashboardController) new T4MVC_DashboardController();
    public DocumentController Document = (DocumentController) new T4MVC_DocumentController();
    public EnforcementController Enforcement = (EnforcementController) new T4MVC_EnforcementController();
    public PermitController Permit = (PermitController) new T4MVC_PermitController();
    public WaterUseDetailController WaterUseDetail = (WaterUseDetailController) new T4MVC_WaterUseDetailController();
    public T4MVC.Permitting.SharedController Shared = new T4MVC.Permitting.SharedController();
  }
}
