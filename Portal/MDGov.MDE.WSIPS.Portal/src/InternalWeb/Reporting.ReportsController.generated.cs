﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Reporting.Controllers.T4MVC_ReportsController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Reporting.Controllers
{
  [GeneratedCode("T4MVC", "2.0")]
  [DebuggerNonUserCode]
  public class T4MVC_ReportsController : ReportsController
  {
    public T4MVC_ReportsController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult Index()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Index, (string) null);
    }

    [NonAction]
    public override JsonResult PumpageReport(int permitId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.PumpageReport, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      return (JsonResult) webMvcJsonResult;
    }
  }
}
