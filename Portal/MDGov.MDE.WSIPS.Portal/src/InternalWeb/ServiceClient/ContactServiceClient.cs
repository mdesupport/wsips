﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.ServiceClient.ContactServiceClient
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using System.Collections.Generic;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.ServiceClient
{
  public class ContactServiceClient : IContactServiceClient
  {
    private readonly IUpdatableService<PermitContact> _permitContactService;
    private readonly IUpdatableService<PermitContactInformation> _permitContactInformation;
    private readonly IUpdatableService<Contact> _contactService;
    private readonly IUpdatableService<ContactCommunicationMethod> _contactCommunicationMethodService;

    public ContactServiceClient(IUpdatableService<PermitContact> permitContactService, IUpdatableService<PermitContactInformation> permitContactInformation, IUpdatableService<Contact> contactService, IUpdatableService<ContactCommunicationMethod> contactCommunicationMethodService)
    {
      this._permitContactService = permitContactService;
      this._permitContactInformation = permitContactInformation;
      this._contactService = contactService;
      this._contactCommunicationMethodService = contactCommunicationMethodService;
    }

    public int SavePermitContact(int permitId, ContactForm contactForm)
    {
      PermitContact permitContact = this._permitContactService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("PermitId==@0 && ContactTypeId==@1 && ContactId==@2", new object[3]{ (object) permitId, (object) contactForm.ContactType, (object) contactForm.Id }) }, "PermitContactInformation").FirstOrDefault<PermitContact>();
      if (permitContact != null)
      {
        this.UpdateContact(permitContact, contactForm);
        return permitContact.ContactId;
      }
      if (contactForm.Id == 0)
      {
        Contact newContact = this.CreateNewContact(contactForm);
        contactForm.Id = this._contactService.Save(newContact);
      }
      int num = this._permitContactInformation.Save(this.CreatePermitContactInformation(contactForm));
      this._permitContactService.Save(new PermitContact()
      {
        PermitId = permitId,
        ContactId = contactForm.Id,
        ContactTypeId = new int?((int) contactForm.ContactType),
        IsPrimary = contactForm.IsPrimary,
        Active = true,
        PermitContactInformationId = new int?(num)
      });
      if (contactForm.EmailAddress != null)
        this.SavePrimaryEmail(contactForm.Id, contactForm.EmailAddress);
      if (contactForm.PrimaryTelephoneNumber != null)
        this.SavePrimaryTelephoneNumber(contactForm.Id, contactForm.PrimaryTelephoneNumber);
      if (contactForm.AltTelephoneNumber != null)
        this.SaveAlternateTelephoneNumber(contactForm.Id, contactForm.AltTelephoneNumber);
      return contactForm.Id;
    }

    private Contact CreateNewContact(ContactForm contactForm)
    {
      return new Contact() { ContactTypeId = (int) contactForm.ContactType, CountryId = new int?(1), CountyId = new int?(), DivisionId = new int?(), StateId = contactForm.StateId, TitleId = new int?(), UserId = new int?(), Salutation = "", FirstName = contactForm.FirstName, /*SecondaryName = contactForm.SecondaryName,*/ MiddleInitial = contactForm.MiddleInitial, LastName = contactForm.LastName, BusinessName = contactForm.BusinessName, Address1 = contactForm.Address1, Address2 = contactForm.Address2, City = contactForm.City, ZipCode = contactForm.ZipCode, Subdivision = (string) null, IsBusiness = contactForm.IsBusiness, Office = (string) null, Active = true };
    }

    private void UpdateContact(PermitContact permitContact, ContactForm contactForm)
    {
      permitContact.PermitContactInformation.StateId = contactForm.StateId;
      permitContact.PermitContactInformation.IsBusiness = contactForm.IsBusiness;
      permitContact.PermitContactInformation.BusinessName = contactForm.BusinessName;
      permitContact.PermitContactInformation.FirstName = contactForm.FirstName;
      /*permitContact.PermitContactInformation.SecondaryName = contactForm.SecondaryName;*/
      permitContact.PermitContactInformation.LastName = contactForm.LastName;
      permitContact.PermitContactInformation.MiddleInitial = contactForm.MiddleInitial;
      permitContact.PermitContactInformation.Address1 = contactForm.Address1;
      permitContact.PermitContactInformation.Address2 = contactForm.Address2;
      permitContact.PermitContactInformation.City = contactForm.City;
      permitContact.PermitContactInformation.ZipCode = contactForm.ZipCode;
      this._permitContactInformation.Save(permitContact.PermitContactInformation);
      if (contactForm.EmailAddress != null)
        this.SavePrimaryEmail(contactForm.Id, contactForm.EmailAddress);
      if (contactForm.PrimaryTelephoneNumber != null)
        this.SavePrimaryTelephoneNumber(contactForm.Id, contactForm.PrimaryTelephoneNumber);
      if (contactForm.AltTelephoneNumber != null)
        this.SaveAlternateTelephoneNumber(contactForm.Id, contactForm.AltTelephoneNumber);
      Contact byId = this._contactService.GetById(permitContact.ContactId, (string) null);
      Contact entity = Mapper.Map<ContactForm, Contact>(contactForm);
      entity.CreatedBy = byId.CreatedBy;
      entity.CreatedDate = byId.CreatedDate;
      entity.UserId = byId.UserId;
      entity.CountryId = byId.CountryId;
      entity.CountyId = byId.CountyId;
      entity.DivisionId = byId.DivisionId;
      entity.TitleId = byId.TitleId;
      entity.Salutation = byId.Salutation;
      entity.Subdivision = byId.Subdivision;
      entity.Active = byId.Active;
      entity.Office = byId.Office;
      entity.ApplicationNotification = byId.ApplicationNotification;
      entity.EmailNotification = byId.EmailNotification;
      this._contactService.Save(entity);
    }

    private void SavePrimaryEmail(int contactId, CommunicationMethodForm email)
    {
      ContactCommunicationMethod entity = this._contactCommunicationMethodService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ContactId==@0 && IsPrimaryEmail==@1", new object[2]{ (object) contactId, (object) true }) }, (string) null).FirstOrDefault<ContactCommunicationMethod>();
      if (entity != null)
      {
        entity.CommunicationValue = email.CommunicationValue;
        entity.CommunicationMethodId = 3;
        this._contactCommunicationMethodService.Save(entity);
      }
      else
        this._contactCommunicationMethodService.Save(new ContactCommunicationMethod()
        {
          IsPrimaryEmail = new bool?(true),
          ContactId = contactId,
          CommunicationValue = email.CommunicationValue,
          CommunicationMethodId = 3
        });
    }

    private void SavePrimaryTelephoneNumber(int contactId, CommunicationMethodForm phone)
    {
      ContactCommunicationMethod entity = this._contactCommunicationMethodService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ContactId==@0 && IsPrimaryPhone==@1", new object[2]{ (object) contactId, (object) true }) }, (string) null).FirstOrDefault<ContactCommunicationMethod>();
      if (entity != null)
      {
        entity.CommunicationValue = phone.CommunicationValue;
        entity.CommunicationMethodId = phone.SelectedCommunicationMethodId.GetValueOrDefault();
        this._contactCommunicationMethodService.Save(entity);
      }
      else
        this._contactCommunicationMethodService.Save(new ContactCommunicationMethod()
        {
          IsPrimaryPhone = new bool?(true),
          ContactId = contactId,
          CommunicationValue = phone.CommunicationValue,
          CommunicationMethodId = phone.SelectedCommunicationMethodId.GetValueOrDefault()
        });
    }

    private void SaveAlternateTelephoneNumber(int contactId, CommunicationMethodForm phone)
    {
      ContactCommunicationMethod entity = this._contactCommunicationMethodService.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]{ new DynamicFilter("ContactId==@0 && IsPrimaryPhone==@1", new object[2]{ (object) contactId, (object) false }) }, (string) null).FirstOrDefault<ContactCommunicationMethod>();
      if (entity != null)
      {
        entity.CommunicationValue = phone.CommunicationValue;
        entity.CommunicationMethodId = phone.SelectedCommunicationMethodId.GetValueOrDefault();
        this._contactCommunicationMethodService.Save(entity);
      }
      else
        this._contactCommunicationMethodService.Save(new ContactCommunicationMethod()
        {
          IsPrimaryPhone = new bool?(false),
          ContactId = contactId,
          CommunicationValue = phone.CommunicationValue,
          CommunicationMethodId = phone.SelectedCommunicationMethodId.GetValueOrDefault()
        });
    }

    private PermitContactInformation CreatePermitContactInformation(ContactForm contact)
    {
      if (contact == null)
        return (PermitContactInformation) null;
      return new PermitContactInformation() { CountryId = new int?(1), StateId = contact.StateId, IsBusiness = contact.IsBusiness, BusinessName = contact.BusinessName, Salutation = "", FirstName = contact.FirstName, /*SecondaryName = contact.SecondaryName,*/ LastName = contact.LastName, MiddleInitial = contact.MiddleInitial, Address1 = contact.Address1, Address2 = contact.Address2, City = contact.City, ZipCode = contact.ZipCode };
    }
  }
}
