﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.BundleConfig
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.Web.Optimization;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb
{
  public class BundleConfig
  {
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));
      bundles.Add(new ScriptBundle("~/bundles/jqgrid").Include("~/Scripts/jquery.jqGrid.js", "~/Scripts/i18n/grid.locale-en.js", "~/Scripts/jquery.jqGrid.defaults.js"));
      bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/jquery-ui-{version}.js"));
      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.unobtrusive*", "~/Scripts/jquery.validate*", "~/Scripts/CustomValidation.js"));
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));
      bundles.Add(new ScriptBundle("~/bundles/Library").Include("~/Scripts/Helper.js", "~/Scripts/underscore.js"));
      bundles.Add(new ScriptBundle("~/bundles/scrollbars").Include("~/Scripts/scrollbars/jquery.event.drag-2.2.js", "~/Scripts/scrollbars/jquery.ba-resize.js", "~/Scripts/scrollbars/mousehold.js", "~/Scripts/scrollbars/jquery.mousewheel.js", "~/Scripts/scrollbars/aplweb.scrollbars.js"));
      bundles.Add(new ScriptBundle("~/bundles/ViewScripts/Permit/Base").Include("~/Scripts/ViewScripts/Permit/Base/BaseApplicantIdentification.js", "~/Scripts/ViewScripts/Permit/Base/BaseContactSearchResult.js", "~/Scripts/ViewScripts/Permit/Base/BaseContactSearch.js", "~/Scripts/ViewScripts/Permit/Base/BaseWaterUseCategoryAndType.js", "~/Scripts/ViewScripts/Permit/Base/BaseWaterUseDescription.js", "~/Scripts/ViewScripts/Permit/Base/BaseWaterUseDetail.js", "~/Scripts/ViewScripts/Permit/Base/BaseWastewaterTreatmentAndDisposal.js", "~/Scripts/Helper.js"));
      bundles.Add(new ScriptBundle("~/bundles/ViewScripts/Permit/ApplicationWizard").Include("~/Scripts/ViewScripts/Permit/Base/BaseApplicantIdentification.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/ApplicantIdentification.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/InformationVerification.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/ContactSearchResult.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/ContactSearch.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/WaterUseCategoryAndType.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/WaterUseDescription.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/WaterUseDetail.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/WastewaterTreatmentAndDisposal.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/IdentifyLocation.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/ReviewApplication.js", "~/Scripts/ViewScripts/Permit/UploadDocument.js", "~/Scripts/ViewScripts/Permit/ApplicationWizard/Affirmation.js", "~/Scripts/Helper.js"));
      bundles.Add(new ScriptBundle("~/bundles/PermitDetails").Include("~/Scripts/ViewScripts/Permit/Details/Index.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Details.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Review.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Conditions.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/IPL.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/CCList.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/PumpageContact.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Documents.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Comments.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Compliance.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Enforcement.js", "~/Scripts/ViewScripts/Permit/Details/Tabs/Contacts.js"));
      bundles.Add(new ScriptBundle("~/bundles/ViewScripts/Permit/PermitDetail").Include("~/Scripts/ViewScripts/Permit/PermitDetail/ApplicantIdentification.js", "~/Scripts/ViewScripts/Permit/PermitDetail/ApprovedAllocation.js", "~/Scripts/ViewScripts/Permit/PermitDetail/ContactSearchResult.js", "~/Scripts/ViewScripts/Permit/PermitDetail/PermitInformation.js", "~/Scripts/ViewScripts/Permit/PermitDetail/WaterUseCategoryAndType.js", "~/Scripts/ViewScripts/Permit/PermitDetail/WastewaterTreatmentAndDisposal.js", "~/Scripts/ViewScripts/Permit/UploadDocument.js", "~/Scripts/jquery.maskMoney.js", "~/Scripts/Helper.js"));
      bundles.Add(new ScriptBundle("~/Permit/PendingApplication").Include("~/Scripts/Helper.js", "~/Scripts/ViewScripts/Permit/PendingApplication.js"));
      bundles.Add(new ScriptBundle("~/bundles/jmtCustom").Include("~/Scripts/jmtCustom/jquery.jmttg-custom-browser.js", "~/Scripts/jmtCustom/jquery.jmttg-custom-bindings.js", "~/Scripts/jmtCustom/jquery.jmttg-custom-combobox.js", "~/Scripts/jmtCustom/jquery.jmttg-custom-helpers.js", "~/Scripts/jmtCustom/jquery.jmttg-custom-modal.js", "~/Scripts/jmtCustom/jquery.jmttg-custom-zipcode.js", "~/Scripts/jmtExtensions/jquery.jmttg-extension-jqueryui.js", "~/Scripts/jmtCustom/jquery.jmttg-custom-startup.js"));
      bundles.Add(new ScriptBundle("~/bundles/esriarcgisapi").Include("~/Scripts/esri.arcgis.jsapi-3.4.js"));
      bundles.Add(new ScriptBundle("~/bundles/blockUI").Include("~/Scripts/jquery.blockUI.js"));
      bundles.Add(new ScriptBundle("~/bundles/jqHistory").Include("~/Scripts/history.js/json2.js", "~/Scripts/history.js/history.adapter.jquery.js", "~/Scripts/history.js/history.js", "~/Scripts/history.js/history.html4.js"));
      bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));
      bundles.Add(new StyleBundle("~/Content/scrollbars").Include("~/Scripts/scrollbars/scrollbars.css", "~/Scripts/scrollbars/scrollbars-square.css"));
      bundles.Add(new StyleBundle("~/Content/themes/base/bundle").Include("~/Content/themes/base/jquery.ui.core.css", "~/Content/themes/base/jquery.ui.resizable.css", "~/Content/themes/base/jquery.ui.selectable.css", "~/Content/themes/base/jquery.ui.accordion.css", "~/Content/themes/base/jquery.ui.autocomplete.css", "~/Content/themes/base/jquery.ui.button.css", "~/Content/themes/base/jquery.ui.dialog.css", "~/Content/themes/base/jquery.ui.slider.css", "~/Content/themes/base/jquery.ui.tabs.css", "~/Content/themes/base/jquery.ui.datepicker.css", "~/Content/themes/base/jquery.ui.progressbar.css", "~/Content/themes/base/jquery.ui.theme.css"));
      bundles.Add(new StyleBundle("~/Content/jqgrid").Include("~/Content/jquery.jqGrid/ui.jqgrid.css"));
      bundles.Add(new StyleBundle("~/Content/themes/gray/bundle").Include("~/Content/themes/gray/jquery-ui-1.9.1.custom.css"));
      bundles.Add(new StyleBundle("~/Content/ArcGISJSAPI/bundle").Include("~/Content/ArcGIS JS API/css/esri.css", "~/Content/ArcGIS JS API/css/infowindow.css"));
    }
  }
}
