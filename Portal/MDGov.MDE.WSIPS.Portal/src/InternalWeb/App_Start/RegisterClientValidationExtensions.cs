﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.App_Start.RegisterClientValidationExtensions
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using DataAnnotationsExtensions.ClientValidation;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.App_Start
{
  public static class RegisterClientValidationExtensions
  {
    public static void Start()
    {
      DataAnnotationsModelValidatorProviderExtensions.RegisterValidationExtensions();
    }
  }
}
