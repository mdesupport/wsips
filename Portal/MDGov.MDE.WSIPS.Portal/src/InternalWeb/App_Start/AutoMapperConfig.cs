﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.AutoMapperConfig
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using AutoMapper;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Helpers.Configuration;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Ecommerce.Helpers.Configuration;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Helpers.Configuration;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Configuration;
using StructureMap;
using System;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb
{
  public static class AutoMapperConfig
  {
    public static void RegisterProfiles()
    {
      Mapper.Initialize((Action<IConfiguration>) (x =>
      {
        x.ConstructServicesUsing(new Func<Type, object>(ObjectFactory.GetInstance));
        x.AddProfile<RootAutoMapperProfile>();
        x.AddProfile<AdministrationAreaAutoMapperProfile>();
        x.AddProfile<PermittingAreaAutoMapperProfile>();
        x.AddProfile<EcommerceAreaAutoMapperProfile>();
      }));
    }
  }
}
