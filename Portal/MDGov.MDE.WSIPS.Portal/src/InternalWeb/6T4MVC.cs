﻿// Decompiled with JetBrains decompiler
// Type: Links.Scripts
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;

namespace Links
{
  [GeneratedCode("T4MVC", "2.0")]
  [DebuggerNonUserCode]
  public static class Scripts
  {
    public static readonly string _references_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/_references.min.js") ? Scripts.Url("_references.js") : Scripts.Url("_references.min.js");
    public static readonly string backstretch_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/backstretch.min.js") ? Scripts.Url("backstretch.js") : Scripts.Url("backstretch.min.js");
    public static readonly string CustomValidation_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/CustomValidation.min.js") ? Scripts.Url("CustomValidation.js") : Scripts.Url("CustomValidation.min.js");
    public static readonly string DateFormatter_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/DateFormatter.min.js") ? Scripts.Url("DateFormatter.js") : Scripts.Url("DateFormatter.min.js");
    public static readonly string esri_arcgis_jsapi_3_4_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/esri.arcgis.min.jsapi-3.4.min.js") ? Scripts.Url("esri.arcgis.jsapi-3.4.js") : Scripts.Url("esri.arcgis.min.jsapi-3.4.min.js");
    public static readonly string Helper_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/Helper.min.js") ? Scripts.Url("Helper.js") : Scripts.Url("Helper.min.js");
    public static readonly string jquery_1_10_1_intellisense_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery-1.10.1.intellisense.min.js") ? Scripts.Url("jquery-1.10.1.intellisense.js") : Scripts.Url("jquery-1.10.1.intellisense.min.js");
    public static readonly string jquery_1_10_1_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery-1.10.1.min.js") ? Scripts.Url("jquery-1.10.1.js") : Scripts.Url("jquery-1.10.1.min.js");
    public static readonly string jquery_1_10_1_min_js = Scripts.Url("jquery-1.10.1.min.js");
    public static readonly string jquery_1_10_1_min_map = Scripts.Url("jquery-1.10.1.min.map");
    public static readonly string jquery_ui_1_10_3_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery-ui-1.10.3.min.js") ? Scripts.Url("jquery-ui-1.10.3.js") : Scripts.Url("jquery-ui-1.10.3.min.js");
    public static readonly string jquery_ui_1_10_3_min_js = Scripts.Url("jquery-ui-1.10.3.min.js");
    public static readonly string jquery_ui_timepicker_addon_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery-ui-timepicker-addon.min.js") ? Scripts.Url("jquery-ui-timepicker-addon.js") : Scripts.Url("jquery-ui-timepicker-addon.min.js");
    public static readonly string jquery_blockUI_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.blockUI.min.js") ? Scripts.Url("jquery.blockUI.js") : Scripts.Url("jquery.blockUI.min.js");
    public static readonly string jquery_blockUI_min_js = Scripts.Url("jquery.blockUI.min.js");
    public static readonly string jquery_formatCurrency_1_4_0_min_js = Scripts.Url("jquery.formatCurrency-1.4.0.min.js");
    public static readonly string jquery_jqGrid_defaults_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.jqGrid.defaults.min.js") ? Scripts.Url("jquery.jqGrid.defaults.js") : Scripts.Url("jquery.jqGrid.defaults.min.js");
    public static readonly string jquery_jqGrid_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.jqGrid.min.js") ? Scripts.Url("jquery.jqGrid.js") : Scripts.Url("jquery.jqGrid.min.js");
    public static readonly string jquery_jqGrid_min_js = Scripts.Url("jquery.jqGrid.min.js");
    public static readonly string jquery_labelify_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.labelify.min.js") ? Scripts.Url("jquery.labelify.js") : Scripts.Url("jquery.labelify.min.js");
    public static readonly string jquery_maskedinput_1_3_1_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.maskedinput-1.3.1.min.js") ? Scripts.Url("jquery.maskedinput-1.3.1.js") : Scripts.Url("jquery.maskedinput-1.3.1.min.js");
    public static readonly string jquery_maskedinput_1_3_1_min_js = Scripts.Url("jquery.maskedinput-1.3.1.min.js");
    public static readonly string jquery_maskMoney_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.maskMoney.min.js") ? Scripts.Url("jquery.maskMoney.js") : Scripts.Url("jquery.maskMoney.min.js");
    public static readonly string jquery_unobtrusive_ajax_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.unobtrusive-ajax.min.js") ? Scripts.Url("jquery.unobtrusive-ajax.js") : Scripts.Url("jquery.unobtrusive-ajax.min.js");
    public static readonly string jquery_unobtrusive_ajax_min_js = Scripts.Url("jquery.unobtrusive-ajax.min.js");
    public static readonly string jquery_validate_vsdoc_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.validate-vsdoc.min.js") ? Scripts.Url("jquery.validate-vsdoc.js") : Scripts.Url("jquery.validate-vsdoc.min.js");
    public static readonly string jquery_validate_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.validate.min.js") ? Scripts.Url("jquery.validate.js") : Scripts.Url("jquery.validate.min.js");
    public static readonly string jquery_validate_min_js = Scripts.Url("jquery.validate.min.js");
    public static readonly string jquery_validate_unobtrusive_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jquery.validate.unobtrusive.min.js") ? Scripts.Url("jquery.validate.unobtrusive.js") : Scripts.Url("jquery.validate.unobtrusive.min.js");
    public static readonly string jquery_validate_unobtrusive_min_js = Scripts.Url("jquery.validate.unobtrusive.min.js");
    public static readonly string knockout_2_1_0_debug_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/knockout-2.1.0.debug.min.js") ? Scripts.Url("knockout-2.1.0.debug.js") : Scripts.Url("knockout-2.1.0.debug.min.js");
    public static readonly string knockout_2_1_0_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/knockout-2.1.0.min.js") ? Scripts.Url("knockout-2.1.0.js") : Scripts.Url("knockout-2.1.0.min.js");
    public static readonly string modernizr_2_6_2_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/modernizr-2.6.2.min.js") ? Scripts.Url("modernizr-2.6.2.js") : Scripts.Url("modernizr-2.6.2.min.js");
    public static readonly string underscore_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/underscore.min.js") ? Scripts.Url("underscore.js") : Scripts.Url("underscore.min.js");
    private const string URLPATH = "~/Scripts";

    public static string Url()
    {
      return T4MVCHelpers.ProcessVirtualPath("~/Scripts");
    }

    public static string Url(string fileName)
    {
      return T4MVCHelpers.ProcessVirtualPath("~/Scripts/" + fileName);
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public static class CustomFileUpload
    {
      private const string URLPATH = "~/Scripts/CustomFileUpload";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload/" + fileName);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class css
      {
        public static readonly string enhanced_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/CustomFileUpload/css/enhanced.min.css") ? Scripts.CustomFileUpload.css.Url("enhanced.css") : Scripts.CustomFileUpload.css.Url("enhanced.min.css");
        private const string URLPATH = "~/Scripts/CustomFileUpload/css";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload/css");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload/css/" + fileName);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class images
      {
        public static readonly string bg_btn_png = Scripts.CustomFileUpload.images.Url("bg-btn.png");
        public static readonly string bg_submit_gif = Scripts.CustomFileUpload.images.Url("bg-submit.gif");
        public static readonly string icon_generic_gif = Scripts.CustomFileUpload.images.Url("icon-generic.gif");
        public static readonly string icon_image_gif = Scripts.CustomFileUpload.images.Url("icon-image.gif");
        public static readonly string icon_media_gif = Scripts.CustomFileUpload.images.Url("icon-media.gif");
        public static readonly string icon_zip_gif = Scripts.CustomFileUpload.images.Url("icon-zip.gif");
        private const string URLPATH = "~/Scripts/CustomFileUpload/images";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload/images");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload/images/" + fileName);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class js
      {
        public static readonly string jQuery_fileinput_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/CustomFileUpload/js/jQuery.fileinput.min.js") ? Scripts.CustomFileUpload.js.Url("jQuery.fileinput.js") : Scripts.CustomFileUpload.js.Url("jQuery.fileinput.min.js");
        private const string URLPATH = "~/Scripts/CustomFileUpload/js";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload/js");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/CustomFileUpload/js/" + fileName);
        }
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public static class history_js
    {
      public static readonly string amplify_store_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/amplify.store.min.js") ? Scripts.history_js.Url("amplify.store.js") : Scripts.history_js.Url("amplify.store.min.js");
      public static readonly string history_adapter_dojo_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.adapter.dojo.min.js") ? Scripts.history_js.Url("history.adapter.dojo.js") : Scripts.history_js.Url("history.adapter.dojo.min.js");
      public static readonly string history_adapter_jquery_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.adapter.jquery.min.js") ? Scripts.history_js.Url("history.adapter.jquery.js") : Scripts.history_js.Url("history.adapter.jquery.min.js");
      public static readonly string history_adapter_mootools_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.adapter.mootools.min.js") ? Scripts.history_js.Url("history.adapter.mootools.js") : Scripts.history_js.Url("history.adapter.mootools.min.js");
      public static readonly string history_adapter_prototype_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.adapter.prototype.min.js") ? Scripts.history_js.Url("history.adapter.prototype.js") : Scripts.history_js.Url("history.adapter.prototype.min.js");
      public static readonly string history_adapter_yui_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.adapter.yui.min.js") ? Scripts.history_js.Url("history.adapter.yui.js") : Scripts.history_js.Url("history.adapter.yui.min.js");
      public static readonly string history_adapter_zepto_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.adapter.zepto.min.js") ? Scripts.history_js.Url("history.adapter.zepto.js") : Scripts.history_js.Url("history.adapter.zepto.min.js");
      public static readonly string history_html4_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.html4.min.js") ? Scripts.history_js.Url("history.html4.js") : Scripts.history_js.Url("history.html4.min.js");
      public static readonly string history_js_ = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/history.min.js") ? Scripts.history_js.Url("history.js") : Scripts.history_js.Url("history.min.js");
      public static readonly string json2_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/history.js/json2.min.js") ? Scripts.history_js.Url("json2.js") : Scripts.history_js.Url("json2.min.js");
      private const string URLPATH = "~/Scripts/history.js";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/history.js");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/history.js/" + fileName);
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public static class i18n
    {
      public static readonly string grid_locale_ar_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-ar.min.js") ? Scripts.i18n.Url("grid.locale-ar.js") : Scripts.i18n.Url("grid.locale-ar.min.js");
      public static readonly string grid_locale_bg_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-bg.min.js") ? Scripts.i18n.Url("grid.locale-bg.js") : Scripts.i18n.Url("grid.locale-bg.min.js");
      public static readonly string grid_locale_bg1251_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-bg1251.min.js") ? Scripts.i18n.Url("grid.locale-bg1251.js") : Scripts.i18n.Url("grid.locale-bg1251.min.js");
      public static readonly string grid_locale_cat_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-cat.min.js") ? Scripts.i18n.Url("grid.locale-cat.js") : Scripts.i18n.Url("grid.locale-cat.min.js");
      public static readonly string grid_locale_cn_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-cn.min.js") ? Scripts.i18n.Url("grid.locale-cn.js") : Scripts.i18n.Url("grid.locale-cn.min.js");
      public static readonly string grid_locale_cs_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-cs.min.js") ? Scripts.i18n.Url("grid.locale-cs.js") : Scripts.i18n.Url("grid.locale-cs.min.js");
      public static readonly string grid_locale_da_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-da.min.js") ? Scripts.i18n.Url("grid.locale-da.js") : Scripts.i18n.Url("grid.locale-da.min.js");
      public static readonly string grid_locale_de_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-de.min.js") ? Scripts.i18n.Url("grid.locale-de.js") : Scripts.i18n.Url("grid.locale-de.min.js");
      public static readonly string grid_locale_dk_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-dk.min.js") ? Scripts.i18n.Url("grid.locale-dk.js") : Scripts.i18n.Url("grid.locale-dk.min.js");
      public static readonly string grid_locale_el_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-el.min.js") ? Scripts.i18n.Url("grid.locale-el.js") : Scripts.i18n.Url("grid.locale-el.min.js");
      public static readonly string grid_locale_en_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-en.min.js") ? Scripts.i18n.Url("grid.locale-en.js") : Scripts.i18n.Url("grid.locale-en.min.js");
      public static readonly string grid_locale_es_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-es.min.js") ? Scripts.i18n.Url("grid.locale-es.js") : Scripts.i18n.Url("grid.locale-es.min.js");
      public static readonly string grid_locale_fa_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-fa.min.js") ? Scripts.i18n.Url("grid.locale-fa.js") : Scripts.i18n.Url("grid.locale-fa.min.js");
      public static readonly string grid_locale_fi_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-fi.min.js") ? Scripts.i18n.Url("grid.locale-fi.js") : Scripts.i18n.Url("grid.locale-fi.min.js");
      public static readonly string grid_locale_fr_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-fr.min.js") ? Scripts.i18n.Url("grid.locale-fr.js") : Scripts.i18n.Url("grid.locale-fr.min.js");
      public static readonly string grid_locale_gl_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-gl.min.js") ? Scripts.i18n.Url("grid.locale-gl.js") : Scripts.i18n.Url("grid.locale-gl.min.js");
      public static readonly string grid_locale_he_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-he.min.js") ? Scripts.i18n.Url("grid.locale-he.js") : Scripts.i18n.Url("grid.locale-he.min.js");
      public static readonly string grid_locale_hr_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-hr.min.js") ? Scripts.i18n.Url("grid.locale-hr.js") : Scripts.i18n.Url("grid.locale-hr.min.js");
      public static readonly string grid_locale_hr1250_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-hr1250.min.js") ? Scripts.i18n.Url("grid.locale-hr1250.js") : Scripts.i18n.Url("grid.locale-hr1250.min.js");
      public static readonly string grid_locale_hu_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-hu.min.js") ? Scripts.i18n.Url("grid.locale-hu.js") : Scripts.i18n.Url("grid.locale-hu.min.js");
      public static readonly string grid_locale_id_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-id.min.js") ? Scripts.i18n.Url("grid.locale-id.js") : Scripts.i18n.Url("grid.locale-id.min.js");
      public static readonly string grid_locale_is_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-is.min.js") ? Scripts.i18n.Url("grid.locale-is.js") : Scripts.i18n.Url("grid.locale-is.min.js");
      public static readonly string grid_locale_it_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-it.min.js") ? Scripts.i18n.Url("grid.locale-it.js") : Scripts.i18n.Url("grid.locale-it.min.js");
      public static readonly string grid_locale_ja_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-ja.min.js") ? Scripts.i18n.Url("grid.locale-ja.js") : Scripts.i18n.Url("grid.locale-ja.min.js");
      public static readonly string grid_locale_kr_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-kr.min.js") ? Scripts.i18n.Url("grid.locale-kr.js") : Scripts.i18n.Url("grid.locale-kr.min.js");
      public static readonly string grid_locale_lt_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-lt.min.js") ? Scripts.i18n.Url("grid.locale-lt.js") : Scripts.i18n.Url("grid.locale-lt.min.js");
      public static readonly string grid_locale_mne_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-mne.min.js") ? Scripts.i18n.Url("grid.locale-mne.js") : Scripts.i18n.Url("grid.locale-mne.min.js");
      public static readonly string grid_locale_nl_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-nl.min.js") ? Scripts.i18n.Url("grid.locale-nl.js") : Scripts.i18n.Url("grid.locale-nl.min.js");
      public static readonly string grid_locale_no_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-no.min.js") ? Scripts.i18n.Url("grid.locale-no.js") : Scripts.i18n.Url("grid.locale-no.min.js");
      public static readonly string grid_locale_pl_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-pl.min.js") ? Scripts.i18n.Url("grid.locale-pl.js") : Scripts.i18n.Url("grid.locale-pl.min.js");
      public static readonly string grid_locale_pt_br_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-pt-br.min.js") ? Scripts.i18n.Url("grid.locale-pt-br.js") : Scripts.i18n.Url("grid.locale-pt-br.min.js");
      public static readonly string grid_locale_pt_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-pt.min.js") ? Scripts.i18n.Url("grid.locale-pt.js") : Scripts.i18n.Url("grid.locale-pt.min.js");
      public static readonly string grid_locale_ro_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-ro.min.js") ? Scripts.i18n.Url("grid.locale-ro.js") : Scripts.i18n.Url("grid.locale-ro.min.js");
      public static readonly string grid_locale_ru_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-ru.min.js") ? Scripts.i18n.Url("grid.locale-ru.js") : Scripts.i18n.Url("grid.locale-ru.min.js");
      public static readonly string grid_locale_sk_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-sk.min.js") ? Scripts.i18n.Url("grid.locale-sk.js") : Scripts.i18n.Url("grid.locale-sk.min.js");
      public static readonly string grid_locale_sr_latin_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-sr-latin.min.js") ? Scripts.i18n.Url("grid.locale-sr-latin.js") : Scripts.i18n.Url("grid.locale-sr-latin.min.js");
      public static readonly string grid_locale_sr_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-sr.min.js") ? Scripts.i18n.Url("grid.locale-sr.js") : Scripts.i18n.Url("grid.locale-sr.min.js");
      public static readonly string grid_locale_sv_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-sv.min.js") ? Scripts.i18n.Url("grid.locale-sv.js") : Scripts.i18n.Url("grid.locale-sv.min.js");
      public static readonly string grid_locale_th_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-th.min.js") ? Scripts.i18n.Url("grid.locale-th.js") : Scripts.i18n.Url("grid.locale-th.min.js");
      public static readonly string grid_locale_tr_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-tr.min.js") ? Scripts.i18n.Url("grid.locale-tr.js") : Scripts.i18n.Url("grid.locale-tr.min.js");
      public static readonly string grid_locale_tw_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-tw.min.js") ? Scripts.i18n.Url("grid.locale-tw.js") : Scripts.i18n.Url("grid.locale-tw.min.js");
      public static readonly string grid_locale_ua_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-ua.min.js") ? Scripts.i18n.Url("grid.locale-ua.js") : Scripts.i18n.Url("grid.locale-ua.min.js");
      public static readonly string grid_locale_vi_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/i18n/grid.locale-vi.min.js") ? Scripts.i18n.Url("grid.locale-vi.js") : Scripts.i18n.Url("grid.locale-vi.min.js");
      private const string URLPATH = "~/Scripts/i18n";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/i18n");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/i18n/" + fileName);
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public static class jmtCustom
    {
      public static readonly string jquery_jmttg_custom_bindings_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtCustom/jquery.jmttg-custom-bindings.min.js") ? Scripts.jmtCustom.Url("jquery.jmttg-custom-bindings.js") : Scripts.jmtCustom.Url("jquery.jmttg-custom-bindings.min.js");
      public static readonly string jquery_jmttg_custom_browser_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtCustom/jquery.jmttg-custom-browser.min.js") ? Scripts.jmtCustom.Url("jquery.jmttg-custom-browser.js") : Scripts.jmtCustom.Url("jquery.jmttg-custom-browser.min.js");
      public static readonly string jquery_jmttg_custom_combobox_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtCustom/jquery.jmttg-custom-combobox.min.js") ? Scripts.jmtCustom.Url("jquery.jmttg-custom-combobox.js") : Scripts.jmtCustom.Url("jquery.jmttg-custom-combobox.min.js");
      public static readonly string jquery_jmttg_custom_helpers_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtCustom/jquery.jmttg-custom-helpers.min.js") ? Scripts.jmtCustom.Url("jquery.jmttg-custom-helpers.js") : Scripts.jmtCustom.Url("jquery.jmttg-custom-helpers.min.js");
      public static readonly string jquery_jmttg_custom_modal_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtCustom/jquery.jmttg-custom-modal.min.js") ? Scripts.jmtCustom.Url("jquery.jmttg-custom-modal.js") : Scripts.jmtCustom.Url("jquery.jmttg-custom-modal.min.js");
      public static readonly string jquery_jmttg_custom_startup_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtCustom/jquery.jmttg-custom-startup.min.js") ? Scripts.jmtCustom.Url("jquery.jmttg-custom-startup.js") : Scripts.jmtCustom.Url("jquery.jmttg-custom-startup.min.js");
      public static readonly string jquery_jmttg_custom_zipcode_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtCustom/jquery.jmttg-custom-zipcode.min.js") ? Scripts.jmtCustom.Url("jquery.jmttg-custom-zipcode.js") : Scripts.jmtCustom.Url("jquery.jmttg-custom-zipcode.min.js");
      private const string URLPATH = "~/Scripts/jmtCustom";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/jmtCustom");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/jmtCustom/" + fileName);
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public static class jmtExtensions
    {
      public static readonly string jquery_jmttg_extension_jqGrid_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtExtensions/jquery.jmttg-extension-jqGrid.min.js") ? Scripts.jmtExtensions.Url("jquery.jmttg-extension-jqGrid.js") : Scripts.jmtExtensions.Url("jquery.jmttg-extension-jqGrid.min.js");
      public static readonly string jquery_jmttg_extension_jqueryui_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/jmtExtensions/jquery.jmttg-extension-jqueryui.min.js") ? Scripts.jmtExtensions.Url("jquery.jmttg-extension-jqueryui.js") : Scripts.jmtExtensions.Url("jquery.jmttg-extension-jqueryui.min.js");
      private const string URLPATH = "~/Scripts/jmtExtensions";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/jmtExtensions");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/jmtExtensions/" + fileName);
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public static class scrollbars
    {
      public static readonly string aplweb_scrollbars_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/scrollbars/aplweb.scrollbars.min.js") ? Scripts.scrollbars.Url("aplweb.scrollbars.js") : Scripts.scrollbars.Url("aplweb.scrollbars.min.js");
      public static readonly string jquery_ba_resize_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/scrollbars/jquery.ba-resize.min.js") ? Scripts.scrollbars.Url("jquery.ba-resize.js") : Scripts.scrollbars.Url("jquery.ba-resize.min.js");
      public static readonly string jquery_event_drag_2_2_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/scrollbars/jquery.event.drag-2.2.min.js") ? Scripts.scrollbars.Url("jquery.event.drag-2.2.js") : Scripts.scrollbars.Url("jquery.event.drag-2.2.min.js");
      public static readonly string jquery_mousewheel_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/scrollbars/jquery.mousewheel.min.js") ? Scripts.scrollbars.Url("jquery.mousewheel.js") : Scripts.scrollbars.Url("jquery.mousewheel.min.js");
      public static readonly string mousehold_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/scrollbars/mousehold.min.js") ? Scripts.scrollbars.Url("mousehold.js") : Scripts.scrollbars.Url("mousehold.min.js");
      public static readonly string scrollbars_square_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/scrollbars/scrollbars-square.min.css") ? Scripts.scrollbars.Url("scrollbars-square.css") : Scripts.scrollbars.Url("scrollbars-square.min.css");
      public static readonly string scrollbars_css = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/scrollbars/scrollbars.min.css") ? Scripts.scrollbars.Url("scrollbars.css") : Scripts.scrollbars.Url("scrollbars.min.css");
      private const string URLPATH = "~/Scripts/scrollbars";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/scrollbars");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/scrollbars/" + fileName);
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public static class ViewScripts
    {
      private const string URLPATH = "~/Scripts/ViewScripts";

      public static string Url()
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts");
      }

      public static string Url(string fileName)
      {
        return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/" + fileName);
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class Administration
      {
        public static readonly string ExternalUserValidation_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Administration/ExternalUserValidation.min.js") ? Scripts.ViewScripts.Administration.Url("ExternalUserValidation.js") : Scripts.ViewScripts.Administration.Url("ExternalUserValidation.min.js");
        public static readonly string UserPermissions_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Administration/UserPermissions.min.js") ? Scripts.ViewScripts.Administration.Url("UserPermissions.js") : Scripts.ViewScripts.Administration.Url("UserPermissions.min.js");
        private const string URLPATH = "~/Scripts/ViewScripts/Administration";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Administration");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Administration/" + fileName);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class Contact
      {
        public static readonly string MergeContacts_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Contact/MergeContacts.min.js") ? Scripts.ViewScripts.Contact.Url("MergeContacts.js") : Scripts.ViewScripts.Contact.Url("MergeContacts.min.js");
        private const string URLPATH = "~/Scripts/ViewScripts/Contact";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Contact");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Contact/" + fileName);
        }
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public static class Home
      {
        public static readonly string Index_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Home/Index.min.js") ? Scripts.ViewScripts.Home.Url("Index.js") : Scripts.ViewScripts.Home.Url("Index.min.js");
        private const string URLPATH = "~/Scripts/ViewScripts/Home";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Home");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Home/" + fileName);
        }
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public static class Permit
      {
        public static readonly string _permitReference_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/_permitReference.min.js") ? Scripts.ViewScripts.Permit.Url("_permitReference.js") : Scripts.ViewScripts.Permit.Url("_permitReference.min.js");
        public static readonly string AdvertisementFees_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/AdvertisementFees.min.js") ? Scripts.ViewScripts.Permit.Url("AdvertisementFees.js") : Scripts.ViewScripts.Permit.Url("AdvertisementFees.min.js");
        public static readonly string Checklist_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Checklist.min.js") ? Scripts.ViewScripts.Permit.Url("Checklist.js") : Scripts.ViewScripts.Permit.Url("Checklist.min.js");
        public static readonly string Comment_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Comment.min.js") ? Scripts.ViewScripts.Permit.Url("Comment.js") : Scripts.ViewScripts.Permit.Url("Comment.min.js");
        public static readonly string Dashboard_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Dashboard.min.js") ? Scripts.ViewScripts.Permit.Url("Dashboard.js") : Scripts.ViewScripts.Permit.Url("Dashboard.min.js");
        public static readonly string PendingApplication_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PendingApplication.min.js") ? Scripts.ViewScripts.Permit.Url("PendingApplication.js") : Scripts.ViewScripts.Permit.Url("PendingApplication.min.js");
        public static readonly string PermitHearing_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitHearing.min.js") ? Scripts.ViewScripts.Permit.Url("PermitHearing.js") : Scripts.ViewScripts.Permit.Url("PermitHearing.min.js");
        public static readonly string PublicComment_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PublicComment.min.js") ? Scripts.ViewScripts.Permit.Url("PublicComment.js") : Scripts.ViewScripts.Permit.Url("PublicComment.min.js");
        public static readonly string UploadDocument_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/UploadDocument.min.js") ? Scripts.ViewScripts.Permit.Url("UploadDocument.js") : Scripts.ViewScripts.Permit.Url("UploadDocument.min.js");
        private const string URLPATH = "~/Scripts/ViewScripts/Permit";

        public static string Url()
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit");
        }

        public static string Url(string fileName)
        {
          return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/" + fileName);
        }

        [GeneratedCode("T4MVC", "2.0")]
        [DebuggerNonUserCode]
        public static class ApplicationWizard
        {
          public static readonly string Affirmation_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/Affirmation.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("Affirmation.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("Affirmation.min.js");
          public static readonly string ApplicantIdentification_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/ApplicantIdentification.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("ApplicantIdentification.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("ApplicantIdentification.min.js");
          public static readonly string ApplicationWizard_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/ApplicationWizard.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("ApplicationWizard.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("ApplicationWizard.min.js");
          public static readonly string ContactSearch_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/ContactSearch.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("ContactSearch.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("ContactSearch.min.js");
          public static readonly string ContactSearchResult_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/ContactSearchResult.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("ContactSearchResult.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("ContactSearchResult.min.js");
          public static readonly string IdentifyLocation_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/IdentifyLocation.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("IdentifyLocation.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("IdentifyLocation.min.js");
          public static readonly string ReviewApplication_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/ReviewApplication.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("ReviewApplication.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("ReviewApplication.min.js");
          public static readonly string WastewaterTreatmentAndDisposal_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/WastewaterTreatmentAndDisposal.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("WastewaterTreatmentAndDisposal.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("WastewaterTreatmentAndDisposal.min.js");
          public static readonly string WaterUseCategoryAndType_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/WaterUseCategoryAndType.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("WaterUseCategoryAndType.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("WaterUseCategoryAndType.min.js");
          public static readonly string WaterUseDescription_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/WaterUseDescription.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("WaterUseDescription.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("WaterUseDescription.min.js");
          public static readonly string WaterUseDetail_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/ApplicationWizard/WaterUseDetail.min.js") ? Scripts.ViewScripts.Permit.ApplicationWizard.Url("WaterUseDetail.js") : Scripts.ViewScripts.Permit.ApplicationWizard.Url("WaterUseDetail.min.js");
          private const string URLPATH = "~/Scripts/ViewScripts/Permit/ApplicationWizard";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/ApplicationWizard");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/ApplicationWizard/" + fileName);
          }
        }

        [GeneratedCode("T4MVC", "2.0")]
        [DebuggerNonUserCode]
        public static class Base
        {
          public static readonly string BaseApplicantIdentification_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Base/BaseApplicantIdentification.min.js") ? Scripts.ViewScripts.Permit.Base.Url("BaseApplicantIdentification.js") : Scripts.ViewScripts.Permit.Base.Url("BaseApplicantIdentification.min.js");
          public static readonly string BaseContactSearch_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Base/BaseContactSearch.min.js") ? Scripts.ViewScripts.Permit.Base.Url("BaseContactSearch.js") : Scripts.ViewScripts.Permit.Base.Url("BaseContactSearch.min.js");
          public static readonly string BaseContactSearchResult_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Base/BaseContactSearchResult.min.js") ? Scripts.ViewScripts.Permit.Base.Url("BaseContactSearchResult.js") : Scripts.ViewScripts.Permit.Base.Url("BaseContactSearchResult.min.js");
          public static readonly string BaseWastewaterTreatmentAndDisposal_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Base/BaseWastewaterTreatmentAndDisposal.min.js") ? Scripts.ViewScripts.Permit.Base.Url("BaseWastewaterTreatmentAndDisposal.js") : Scripts.ViewScripts.Permit.Base.Url("BaseWastewaterTreatmentAndDisposal.min.js");
          public static readonly string BaseWaterUseCategoryAndType_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Base/BaseWaterUseCategoryAndType.min.js") ? Scripts.ViewScripts.Permit.Base.Url("BaseWaterUseCategoryAndType.js") : Scripts.ViewScripts.Permit.Base.Url("BaseWaterUseCategoryAndType.min.js");
          public static readonly string BaseWaterUseDescription_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Base/BaseWaterUseDescription.min.js") ? Scripts.ViewScripts.Permit.Base.Url("BaseWaterUseDescription.js") : Scripts.ViewScripts.Permit.Base.Url("BaseWaterUseDescription.min.js");
          public static readonly string BaseWaterUseDetail_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Base/BaseWaterUseDetail.min.js") ? Scripts.ViewScripts.Permit.Base.Url("BaseWaterUseDetail.js") : Scripts.ViewScripts.Permit.Base.Url("BaseWaterUseDetail.min.js");
          private const string URLPATH = "~/Scripts/ViewScripts/Permit/Base";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Base");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Base/" + fileName);
          }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("T4MVC", "2.0")]
        public static class Details
        {
          public static readonly string Index_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Index.min.js") ? Scripts.ViewScripts.Permit.Details.Url("Index.js") : Scripts.ViewScripts.Permit.Details.Url("Index.min.js");
          private const string URLPATH = "~/Scripts/ViewScripts/Permit/Details";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Details");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Details/" + fileName);
          }

          [DebuggerNonUserCode]
          [GeneratedCode("T4MVC", "2.0")]
          public static class Controls
          {
            public static readonly string Checklist_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Controls/Checklist.min.js") ? Scripts.ViewScripts.Permit.Details.Controls.Url("Checklist.js") : Scripts.ViewScripts.Permit.Details.Controls.Url("Checklist.min.js");
            public static readonly string Comments_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Controls/Comments.min.js") ? Scripts.ViewScripts.Permit.Details.Controls.Url("Comments.js") : Scripts.ViewScripts.Permit.Details.Controls.Url("Comments.min.js");
            public static readonly string Files_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Controls/Files.min.js") ? Scripts.ViewScripts.Permit.Details.Controls.Url("Files.js") : Scripts.ViewScripts.Permit.Details.Controls.Url("Files.min.js");
            private const string URLPATH = "~/Scripts/ViewScripts/Permit/Details/Controls";

            public static string Url()
            {
              return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Details/Controls");
            }

            public static string Url(string fileName)
            {
              return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Details/Controls/" + fileName);
            }
          }

          [DebuggerNonUserCode]
          [GeneratedCode("T4MVC", "2.0")]
          public static class Tabs
          {
            public static readonly string CCList_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/CCList.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("CCList.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("CCList.min.js");
            public static readonly string Comments_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Comments.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Comments.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Comments.min.js");
            public static readonly string Compliance_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Compliance.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Compliance.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Compliance.min.js");
            public static readonly string Conditions_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Conditions.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Conditions.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Conditions.min.js");
            public static readonly string Contacts_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Contacts.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Contacts.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Contacts.min.js");
            public static readonly string Details_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Details.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Details.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Details.min.js");
            public static readonly string Documents_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Documents.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Documents.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Documents.min.js");
            public static readonly string Enforcement_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Enforcement.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Enforcement.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Enforcement.min.js");
            public static readonly string IPL_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/IPL.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("IPL.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("IPL.min.js");
            public static readonly string PumpageContact_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/PumpageContact.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("PumpageContact.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("PumpageContact.min.js");
            public static readonly string Review_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/Details/Tabs/Review.min.js") ? Scripts.ViewScripts.Permit.Details.Tabs.Url("Review.js") : Scripts.ViewScripts.Permit.Details.Tabs.Url("Review.min.js");
            private const string URLPATH = "~/Scripts/ViewScripts/Permit/Details/Tabs";

            public static string Url()
            {
              return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Details/Tabs");
            }

            public static string Url(string fileName)
            {
              return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/Details/Tabs/" + fileName);
            }
          }
        }

        [DebuggerNonUserCode]
        [GeneratedCode("T4MVC", "2.0")]
        public static class PermitDetail
        {
          public static readonly string ApplicantIdentification_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/ApplicantIdentification.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("ApplicantIdentification.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("ApplicantIdentification.min.js");
          public static readonly string ApplicationSummary_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/ApplicationSummary.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("ApplicationSummary.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("ApplicationSummary.min.js");
          public static readonly string ApprovedAllocation_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/ApprovedAllocation.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("ApprovedAllocation.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("ApprovedAllocation.min.js");
          public static readonly string ContactSearchResult_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/ContactSearchResult.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("ContactSearchResult.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("ContactSearchResult.min.js");
          public static readonly string PermitInformation_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/PermitInformation.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("PermitInformation.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("PermitInformation.min.js");
          public static readonly string PumpageReporting_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/PumpageReporting.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("PumpageReporting.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("PumpageReporting.min.js");
          public static readonly string WastewaterTreatmentAndDisposal_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/WastewaterTreatmentAndDisposal.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("WastewaterTreatmentAndDisposal.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("WastewaterTreatmentAndDisposal.min.js");
          public static readonly string WaterUseCategoryAndType_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/WaterUseCategoryAndType.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("WaterUseCategoryAndType.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("WaterUseCategoryAndType.min.js");
          public static readonly string WithdrawalSourceInformation_js = !T4MVCHelpers.IsProduction() || !T4Extensions.FileExists("~/Scripts/ViewScripts/Permit/PermitDetail/WithdrawalSourceInformation.min.js") ? Scripts.ViewScripts.Permit.PermitDetail.Url("WithdrawalSourceInformation.js") : Scripts.ViewScripts.Permit.PermitDetail.Url("WithdrawalSourceInformation.min.js");
          private const string URLPATH = "~/Scripts/ViewScripts/Permit/PermitDetail";

          public static string Url()
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/PermitDetail");
          }

          public static string Url(string fileName)
          {
            return T4MVCHelpers.ProcessVirtualPath("~/Scripts/ViewScripts/Permit/PermitDetail/" + fileName);
          }
        }
      }
    }
  }
}
