﻿// Decompiled with JetBrains decompiler
// Type: T4MVC.Administration.SharedController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;

namespace T4MVC.Administration
{
  public class SharedController
  {
    private static readonly SharedController.ViewsClass s_views = new SharedController.ViewsClass();

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public SharedController.ViewsClass Views
    {
      get
      {
        return SharedController.s_views;
      }
    }

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public class ViewsClass
    {
      private static readonly SharedController.ViewsClass._ViewNamesClass s_ViewNames = new SharedController.ViewsClass._ViewNamesClass();
      public readonly string _BackToAdminLink = "~/Areas/Administration/Views/Shared/_BackToAdminLink.cshtml";
      public readonly string _BackToSearchLink = "~/Areas/Administration/Views/Shared/_BackToSearchLink.cshtml";
      public readonly string _CreateOrEditExternal = "~/Areas/Administration/Views/Shared/_CreateOrEditExternal.cshtml";
      public readonly string _CreateOrEditInternal = "~/Areas/Administration/Views/Shared/_CreateOrEditInternal.cshtml";
      public readonly string _CreateOrEditPublic = "~/Areas/Administration/Views/Shared/_CreateOrEditPublic.cshtml";
      public readonly string _CreateUserLinks = "~/Areas/Administration/Views/Shared/_CreateUserLinks.cshtml";
      public readonly string _LastLoggedInLeftNav = "~/Areas/Administration/Views/Shared/_LastLoggedInLeftNav.cshtml";
      public readonly string _UserPermissions = "~/Areas/Administration/Views/Shared/_UserPermissions.cshtml";

      public SharedController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return SharedController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string _BackToAdminLink = "_BackToAdminLink";
        public readonly string _BackToSearchLink = "_BackToSearchLink";
        public readonly string _CreateOrEditExternal = "_CreateOrEditExternal";
        public readonly string _CreateOrEditInternal = "_CreateOrEditInternal";
        public readonly string _CreateOrEditPublic = "_CreateOrEditPublic";
        public readonly string _CreateUserLinks = "_CreateUserLinks";
        public readonly string _LastLoggedInLeftNav = "_LastLoggedInLeftNav";
        public readonly string _UserPermissions = "_UserPermissions";
      }
    }
  }
}
