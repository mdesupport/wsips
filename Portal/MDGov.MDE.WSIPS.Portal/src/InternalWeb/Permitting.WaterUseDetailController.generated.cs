﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.T4MVC_WaterUseDetailController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [GeneratedCode("T4MVC", "2.0")]
  [DebuggerNonUserCode]
  public class T4MVC_WaterUseDetailController : WaterUseDetailController
  {
    public T4MVC_WaterUseDetailController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult LoadWaterUseCategoryAndType()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadWaterUseCategoryAndType, (string) null);
    }

    [NonAction]
    public override ActionResult LoadWaterUseDetails(int? permitId, string selectedWaterUserCategoryType)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadWaterUseDetails, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "selectedWaterUserCategoryType", (object) selectedWaterUserCategoryType);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddPoultryWateringEvaporative(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddPoultryWateringEvaporative, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddPoultryWateringFogger(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddPoultryWateringFogger, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddLivestockWatering(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddLivestockWatering, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddOtherLivestockWatering(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddOtherLivestockWatering, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddDairyAnimalWateing(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddDairyAnimalWateing, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddCrop(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddCrop, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddNurseryStock(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddNurseryStock, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddSod(int index)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddSod, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "index", (object) index);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddPrivateWaterSupply(string waterUseCategoryTypeids, int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddPrivateWaterSupply, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "waterUseCategoryTypeids", (object) waterUseCategoryTypeids);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AddWastewaterTreatmentDisposal(string waterUseCategoryTypeids, int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AddWastewaterTreatmentDisposal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "waterUseCategoryTypeids", (object) waterUseCategoryTypeids);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }
  }
}
