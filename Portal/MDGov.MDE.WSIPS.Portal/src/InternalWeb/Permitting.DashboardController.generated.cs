﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.T4MVC_DashboardController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [DebuggerNonUserCode]
  [GeneratedCode("T4MVC", "2.0")]
  public class T4MVC_DashboardController : DashboardController
  {
    public T4MVC_DashboardController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult Index(DashboardMode? mode, int? permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Index, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "mode", (object) mode);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult ExportToExcel()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ExportToExcel, (string) null);
    }

    [NonAction]
    public override ActionResult GenerateAveryLabels()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.GenerateAveryLabels, (string) null);
    }

    [NonAction]
    public override JsonResult Data(GridCommand command)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Filter(DashboardFilterForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Filter, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SpatialFilter(DashboardSpatialFilterForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SpatialFilter, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Search(string search)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Search, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "search", (object) search);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult SaveFilter(DashboardFilterForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SaveFilter, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override PartialViewResult GetFilterControl(FilterTemplate filterTemplate)
    {
      T4MVC_System_Web_Mvc_PartialViewResult partialViewResult = new T4MVC_System_Web_Mvc_PartialViewResult(this.Area, this.Name, this.ActionNames.GetFilterControl, (string) null);
      ModelUnbinderHelpers.AddRouteValues(partialViewResult.RouteValueDictionary, "filterTemplate", (object) filterTemplate);
      return (PartialViewResult) partialViewResult;
    }

    [NonAction]
    public override JsonResult BuildKmlFile(double[][][] geometry)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.BuildKmlFile, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "geometry", (object) geometry);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override FileResult RetrieveKmlFile()
    {
      return (FileResult) new T4MVC_System_Web_Mvc_FileResult(this.Area, this.Name, this.ActionNames.RetrieveKmlFile, (string) null);
    }
  }
}
