﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Controllers.T4MVC_UsersController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Administration.Controllers
{
  [DebuggerNonUserCode]
  [GeneratedCode("T4MVC", "2.0")]
  public class T4MVC_UsersController : UsersController
  {
    public T4MVC_UsersController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult Index()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Index, (string) null);
    }

    [NonAction]
    public override ActionResult SearchInternal()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.SearchInternal, (string) null);
    }

    [NonAction]
    public override ActionResult CreateInternal(string userName, string firstName, string lastName)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreateInternal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "userName", (object) userName);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "firstName", (object) firstName);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "lastName", (object) lastName);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult CreateInternal(InternalContact contact)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreateInternal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "contact", (object) contact);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult CreateExternal()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreateExternal, (string) null);
    }

    [NonAction]
    public override ActionResult CreateExternal(ExternalContact contact, string[] userRoles)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreateExternal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "contact", (object) contact);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "userRoles", (object) userRoles);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult CreatePublic()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreatePublic, (string) null);
    }

    [NonAction]
    public override ActionResult CreatePublic(PublicContact contact, string[] userRoles)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CreatePublic, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "contact", (object) contact);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "userRoles", (object) userRoles);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditInternal(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditInternal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditInternal(InternalContact contact)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditInternal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "contact", (object) contact);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditExternal(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditExternal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditExternal(ExternalContact contact)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditExternal, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "contact", (object) contact);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditPublic(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPublic, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditPublic(PublicContact contact)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPublic, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "contact", (object) contact);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult Activate(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Activate, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult Deactivate(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Deactivate, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AssignToCounty()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AssignToCounty, (string) null);
    }

    [NonAction]
    public override ActionResult AssignToCounty(List<ListProjectManagerCounty> data)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AssignToCounty, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "data", (object) data);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult AssignToSupervisor()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.AssignToSupervisor, (string) null);
    }

    [NonAction]
    public override JsonResult AssignToSupervisor(string role, int assignTo, int[] available, int[] assigned)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AssignToSupervisor, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "role", (object) role);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "assignTo", (object) assignTo);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "available", (object) available);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "assigned", (object) assigned);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetStaffForRoleData(string role)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetStaffForRoleData, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "role", (object) role);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetAvailableStaff(string role, int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetAvailableStaff, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "role", (object) role);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetAssignedStaff(string role, int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetAssignedStaff, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "role", (object) role);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Data(GridCommand command)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult ActiveDirData(GridCommand command, string filter)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ActiveDirData, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "filter", (object) filter);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult ClearGridSession()
    {
      return (JsonResult) new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.ClearGridSession, (string) null);
    }

    [NonAction]
    public override JsonResult SearchFilter(UsersSearchForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchFilter, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }
  }
}
