﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers.T4MVC_ConditionController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.InternalWeb.Models;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Areas.Permitting.Controllers
{
  [DebuggerNonUserCode]
  [GeneratedCode("T4MVC", "2.0")]
  public class T4MVC_ConditionController : ConditionController
  {
    public T4MVC_ConditionController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult Index()
    {
      return (ActionResult) new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Index, (string) null);
    }

    [NonAction]
    public override JsonResult Data(GridCommand command)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetAllSpecialConditions(GridCommand command)
    {
        T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetAllSpecialConditions, (string)null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult PermitData(GridCommand command, int permitId, bool complianceOnly)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.PermitData, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "complianceOnly", (object) complianceOnly);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Create(StandardCondition condition)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Create, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "condition", (object) condition);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult AddSpecialCondition(int permitId, int standardConditionId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.AddSpecialCondition, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitId", (object) permitId);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "standardConditionId", (object) standardConditionId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult Edit(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult Edit(StandardCondition condition)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Edit, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "condition", (object) condition);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult Activate(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Activate, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult Deactivate(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.Deactivate, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult SearchFilter(ConditionSearchForm form)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.SearchFilter, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "form", (object) form);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult UpdateSequence(int[] ids)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateSequence, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "ids", (object) ids);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult CustomDetails(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.CustomDetails, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult ComplianceCustomDetails(int permitConditionId, int conditionComplianceId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.ComplianceCustomDetails, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitConditionId", (object) permitConditionId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "conditionComplianceId", (object) conditionComplianceId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult GetReportHistory(GridCommand command, int permitConditionId)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetReportHistory, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitConditionId", (object) permitConditionId);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetPumpageReports(GridCommand command, int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetPumpageReports, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetStandardReports(GridCommand command, int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetStandardReports, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "command", (object) command);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult ViewPumpageReport(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.ViewPumpageReport, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditPumpageReport(int id, int conditionComplianceId, bool resubmit)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPumpageReport, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "conditionComplianceId", (object) conditionComplianceId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "resubmit", (object) resubmit);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditPumpageReport(PumpageReportDetailsForm report)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPumpageReport, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "report", (object) report);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditCustom(int id, int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditCustom, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult EditCustom(CustomConditionForm condition)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.EditCustom, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "condition", (object) condition);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult CreateCustom(CustomConditionForm condition)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.CreateCustom, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "condition", (object) condition);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult UpdateCustomSequence(int[] ids)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.UpdateCustomSequence, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "ids", (object) ids);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult DeleteCustom(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.DeleteCustom, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override ActionResult EditPermitComplianceStatus(int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPermitComplianceStatus, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditPermitComplianceStatus(int complianceStatusId, int permitId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditPermitComplianceStatus, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "complianceStatusId", (object) complianceStatusId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitId", (object) permitId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditConditionComplianceStatus(int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditConditionComplianceStatus, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override ActionResult EditConditionComplianceStatus(int complianceStatusId, int id)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.EditConditionComplianceStatus, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "complianceStatusId", (object) complianceStatusId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "id", (object) id);
      return (ActionResult) webMvcActionResult;
    }
  }
}
