﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers.T4MVC_CommentController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Web.Mvc;
using T4MVC;

namespace MDGov.MDE.WSIPS.Portal.InternalWeb.Controllers
{
  [DebuggerNonUserCode]
  [GeneratedCode("T4MVC", "2.0")]
  public class T4MVC_CommentController : CommentController
  {
    public T4MVC_CommentController()
      : base(Dummy.Instance)
    {
    }

    [NonAction]
    public override ActionResult LoadCommentsView(string refTable, int refId, int permitStatusId)
    {
      T4MVC_System_Web_Mvc_ActionResult webMvcActionResult = new T4MVC_System_Web_Mvc_ActionResult(this.Area, this.Name, this.ActionNames.LoadCommentsView, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "refTable", (object) refTable);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "refId", (object) refId);
      ModelUnbinderHelpers.AddRouteValues(webMvcActionResult.RouteValueDictionary, "permitStatusId", (object) permitStatusId);
      return (ActionResult) webMvcActionResult;
    }

    [NonAction]
    public override JsonResult Data(int id, int permitStatus)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Data, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "permitStatus", (object) permitStatus);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult Delete(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.Delete, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }

    [NonAction]
    public override JsonResult GetComment(int id)
    {
      T4MVC_System_Web_Mvc_JsonResult webMvcJsonResult = new T4MVC_System_Web_Mvc_JsonResult(this.Area, this.Name, this.ActionNames.GetComment, (string) null);
      ModelUnbinderHelpers.AddRouteValues(webMvcJsonResult.RouteValueDictionary, "id", (object) id);
      return (JsonResult) webMvcJsonResult;
    }
  }
}
