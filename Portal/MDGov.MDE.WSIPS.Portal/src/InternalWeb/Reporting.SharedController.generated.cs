﻿// Decompiled with JetBrains decompiler
// Type: T4MVC.Reporting.SharedController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;

namespace T4MVC.Reporting
{
  public class SharedController
  {
    private static readonly SharedController.ViewsClass s_views = new SharedController.ViewsClass();

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public SharedController.ViewsClass Views
    {
      get
      {
        return SharedController.s_views;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly SharedController.ViewsClass._ViewNamesClass s_ViewNames = new SharedController.ViewsClass._ViewNamesClass();

      public SharedController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return SharedController.ViewsClass.s_ViewNames;
        }
      }

      public class _ViewNamesClass
      {
      }
    }
  }
}
