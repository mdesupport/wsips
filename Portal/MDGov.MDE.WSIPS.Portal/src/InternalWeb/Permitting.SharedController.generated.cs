﻿// Decompiled with JetBrains decompiler
// Type: T4MVC.Permitting.SharedController
// Assembly: MDGov.MDE.WSIPS.Portal.InternalWeb, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EA07F2F4-D721-4851-B735-642F0407C37B
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Web\InternalWeb\bin\MDGov.MDE.WSIPS.Portal.InternalWeb.dll

using System.CodeDom.Compiler;
using System.Diagnostics;

namespace T4MVC.Permitting
{
  public class SharedController
  {
    private static readonly SharedController.ViewsClass s_views = new SharedController.ViewsClass();

    [GeneratedCode("T4MVC", "2.0")]
    [DebuggerNonUserCode]
    public SharedController.ViewsClass Views
    {
      get
      {
        return SharedController.s_views;
      }
    }

    [DebuggerNonUserCode]
    [GeneratedCode("T4MVC", "2.0")]
    public class ViewsClass
    {
      private static readonly SharedController.ViewsClass._ViewNamesClass s_ViewNames = new SharedController.ViewsClass._ViewNamesClass();
      private static readonly SharedController.ViewsClass._DisplayTemplatesClass s_DisplayTemplates = new SharedController.ViewsClass._DisplayTemplatesClass();
      private static readonly SharedController.ViewsClass._EditorTemplatesClass s_EditorTemplates = new SharedController.ViewsClass._EditorTemplatesClass();
      public readonly string AddWaterWithdrawalLocation = "~/Areas/Permitting/Views/Shared/AddWaterWithdrawalLocation.cshtml";
      public readonly string EditWaterWithdrawalLocation = "~/Areas/Permitting/Views/Shared/EditWaterWithdrawalLocation.cshtml";

      public SharedController.ViewsClass._ViewNamesClass ViewNames
      {
        get
        {
          return SharedController.ViewsClass.s_ViewNames;
        }
      }

      public SharedController.ViewsClass._DisplayTemplatesClass DisplayTemplates
      {
        get
        {
          return SharedController.ViewsClass.s_DisplayTemplates;
        }
      }

      public SharedController.ViewsClass._EditorTemplatesClass EditorTemplates
      {
        get
        {
          return SharedController.ViewsClass.s_EditorTemplates;
        }
      }

      public class _ViewNamesClass
      {
        public readonly string AddWaterWithdrawalLocation = "AddWaterWithdrawalLocation";
        public readonly string EditWaterWithdrawalLocation = "EditWaterWithdrawalLocation";
      }

      [GeneratedCode("T4MVC", "2.0")]
      [DebuggerNonUserCode]
      public class _DisplayTemplatesClass
      {
        public readonly string GroundwaterAllocationForm = "GroundwaterAllocationForm";
        public readonly string PermitParcelForm = "PermitParcelForm";
        public readonly string SurfacewaterAllocationForm = "SurfacewaterAllocationForm";
      }

      [DebuggerNonUserCode]
      [GeneratedCode("T4MVC", "2.0")]
      public class _EditorTemplatesClass
      {
        public readonly string GroundwaterAllocationForm = "GroundwaterAllocationForm";
        public readonly string PermitGeneralInformationForm = "PermitGeneralInformationForm";
        public readonly string PermitLocationInformationForm = "PermitLocationInformationForm";
        public readonly string PermitWwPrivateWaterSupplierForm = "PermitWwPrivateWaterSupplierForm";
        public readonly string SurfacewaterAllocationForm = "SurfacewaterAllocationForm";
        public readonly string WastewaterTreatmentAndDisposalForm = "WastewaterTreatmentAndDisposalForm";
        public readonly string WithdrawalLocationForm = "WithdrawalLocationForm";
      }
    }
  }
}
