﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.MapResolvers
{
    public class StateDropdownListResolver<S> : ValueResolver<S, IEnumerable<SelectListItem>>
        where S : class
    {
        private readonly IService<LU_State> _stateService;

        public StateDropdownListResolver(IService<LU_State> stateService)
        {
            _stateService = stateService;
        }

        protected override IEnumerable<SelectListItem> ResolveCore(S source)
        {
            return CreateSelectList(_stateService.GetRange(0, int.MaxValue, "Sequence", new[] { new DynamicFilter("Active == @0", true) }));
        }

        private IEnumerable<SelectListItem> CreateSelectList(IEnumerable<LU_State> source)
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Select", Value = "" });
            list.AddRange(source.Select(x => new SelectListItem { Text = x.Description, Value = x.Id.ToString() }));

            return list;
        }
    }
}