﻿using AutoMapper;
using MDGov.MDE.Permitting.Model;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.MapResolvers
{
    /// <summary>
    /// Email Resolver
    /// </summary>
    public class CommunicationMethodEmailResolver : ValueResolver<Contact, string>
    {
        protected override string ResolveCore(Contact source)
        {
            var contact = source.ContactCommunicationMethods.FirstOrDefault(c => c.IsPrimaryEmail == true);
            return (contact == null) ? string.Empty : contact.CommunicationValue;
        }
    }
}