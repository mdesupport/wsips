﻿using AutoMapper;
using MDGov.MDE.Permitting.Model;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.MapResolvers
{
    /// <summary>
    /// Phone Resolver
    /// </summary>
    public class CommunicationMethodPhoneResolver : ValueResolver<Contact, string>
    {
        protected override string ResolveCore(Contact source)
        {
            var contact = source.ContactCommunicationMethods.FirstOrDefault(c => c.IsPrimaryPhone == true);
            return (contact == null) ? string.Empty : contact.CommunicationValue;
        }
    }
}