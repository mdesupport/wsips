﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MDGov.MDE.Common.ServiceLayer;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.MapResolvers
{
    public class LookupDropdownListResolver<T, S> : ValueResolver<S, IEnumerable<SelectListItem>>
        where T : class
        where S : class
    {
        private readonly IService<T> _lookupService;

        public LookupDropdownListResolver(IService<T> lookupService)
        {
            _lookupService = lookupService;
        }

        protected override IEnumerable<SelectListItem> ResolveCore(S source)
        {
            return CreateSelectList(_lookupService.GetAll());
        }

        private IEnumerable<SelectListItem> CreateSelectList(IEnumerable<T> source)
        {
            var selectListitems = (new SelectList(source, "Id", "Description")).ToList();

            selectListitems.Insert(0, new SelectListItem { Text = "Select", Value = "" });

            return selectListitems;
        }
    }
}