﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.TypeConverters;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration
{
    public class RootAutoMapperProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "Root";
            }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Permit, DashboardPermit>();
            Mapper.CreateMap<IEnumerable<ILookup>, SelectList>().ConvertUsing<LookupSelectListTypeConverter>();
            Mapper.CreateMap<IEnumerable<LU_StateStream>, SelectList>().ConvertUsing<StateStreamSelectListTypeConverter>();
            Mapper.CreateMap<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>().ConvertUsing<WaterWithdrawalTypeTypeConverter>();
        }
    }
}