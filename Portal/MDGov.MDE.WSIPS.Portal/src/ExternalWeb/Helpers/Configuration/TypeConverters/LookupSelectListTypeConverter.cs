﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using System.Linq;
using MDGov.MDE.Common.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.TypeConverters
{
    public class LookupSelectListTypeConverter : ITypeConverter<IEnumerable<ILookup>, SelectList>
    {
        public SelectList Convert(ResolutionContext context)
        {
            if (context.SourceValue == null)
            {
                return null;
            }

            var list = ((IEnumerable<ILookup>)context.SourceValue).Select(t => new SelectListItem { Text = t.Description, Value = t.Id.ToString() }).ToList();

            list.Insert(0, new SelectListItem { Text = "Select" });

            return new SelectList(list, "Value", "Text");
        }
    }
}