﻿using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.TypeConverters
{
    public class StateStreamSelectListTypeConverter : ITypeConverter<IEnumerable<LU_StateStream>, SelectList>
    {
        public SelectList Convert(ResolutionContext context)
        {
            if (context.SourceValue == null)
            {
                return null;
            }

            var list = ((IEnumerable<LU_StateStream>)context.SourceValue).Select(t => new SelectListItem { Text = t.Key + " | " + t.Description, Value = t.Id.ToString() }).OrderBy(t => t.Text).ToList();

            list.Insert(0, new SelectListItem { Text = "Select" });

            return new SelectList(list, "Value", "Text");
        }
    }
}