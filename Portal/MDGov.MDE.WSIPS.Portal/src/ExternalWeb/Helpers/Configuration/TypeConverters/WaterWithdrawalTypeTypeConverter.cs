﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using System.Linq;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.TypeConverters
{
    public class WaterWithdrawalTypeTypeConverter : ITypeConverter<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>
    {
        public AttributedSelectList Convert(ResolutionContext context)
        {
            if (context.SourceValue == null)
            {
                return null;
            }

            var list = ((IEnumerable<LU_WaterWithdrawalType>)context.SourceValue).Select(i => new AttributedSelectListItem { Text = i.Description, Value = i.Id.ToString(), HtmlAttributes = new { WaterWithdrawalSourceId = i.WaterWithdrawalSourceId } }).ToList();

            list.Insert(0, new AttributedSelectListItem { Text = "Select" });

            return new AttributedSelectList(list, "Value", "Text");
        }
    }
}