﻿using System.Web;
using MDGov.MDE.Common.Configuration;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Service
{
    /// <summary>
    /// WSIPS Portal implementation of UpdatableService
    /// </summary>
    public class UpdatableService<T> : UpdatableServiceBase<T> where T : IUpdatableEntity
    {
        /// <summary>
        /// Saves an entity
        /// </summary>
        /// <param name="entity">Data entity</param>
        /// <returns>Id (integer)</returns>
        public override int Save(T entity)
        {
            entity.LastModifiedBy = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? Constants.SYSTEM : HttpContext.Current.User.Identity.Name;

            return base.Save(entity);
        }
    }
}