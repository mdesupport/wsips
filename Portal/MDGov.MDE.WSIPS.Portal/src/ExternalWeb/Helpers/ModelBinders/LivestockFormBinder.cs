﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders
{
    public class LivestockFormBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext,
                                         ModelBindingContext bindingContext,
                                         System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {

            switch (propertyDescriptor.Name)
            {
                case "NumberOfLivestock":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".NumberOfLivestock"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".NumberOfLivestock"].Replace(",", "")));
                    return;
                
            }

            base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
        }
    }
}