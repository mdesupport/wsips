﻿using System.Linq;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders
{
    public class GridCommandModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {
            var attr = propertyDescriptor.Attributes.OfType<BindingAttribute>().SingleOrDefault();

            if (attr == null)
            {
                // If we don't have a custom binding attribute, continue on with the default binding
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
            }
            else
            {
                // Custom set property value.
                var value = bindingContext.ValueProvider.GetValue(attr.Name).ConvertTo(propertyDescriptor.PropertyType);
                base.SetProperty(controllerContext, bindingContext, propertyDescriptor, value);
            }
        }
    }
}