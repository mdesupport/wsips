﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders
{
    public class PermitWwIrrigationFormBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext,
                                             ModelBindingContext bindingContext,
                                             System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {

            switch (propertyDescriptor.Name)
            {
                case "IrrigatedAcres":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".IrrigatedAcres"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".IrrigatedAcres"].Replace(",", "")));
                    return;

                case "CropYieldGoal":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".CropYieldGoal"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".CropYieldGoal"].Replace(",", "")));
                    return;
                case "AverageGallonPerDayFromGroundWater":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName+".AverageGallonPerDayFromGroundWater"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName+".AverageGallonPerDayFromGroundWater"].Replace(",", "")));
                    return;

                case "MaximumGallonPerDayFromGroundWater":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".MaximumGallonPerDayFromGroundWater"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".MaximumGallonPerDayFromGroundWater"].Replace(",", "")));
                            
                    return;

                case "AverageGallonPerDayFromSurfaceWater":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".AverageGallonPerDayFromSurfaceWater"]))
                                propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".AverageGallonPerDayFromSurfaceWater"].Replace(",", "")));
                    return;

                case "MaximumGallonPerDayFromSurfaceWater":

                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".MaximumGallonPerDayFromSurfaceWater"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".MaximumGallonPerDayFromSurfaceWater"].Replace(",", "")));
                    return;

                case "NoOfLotsOrResidentialUnits":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".NoOfLotsOrResidentialUnits"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".NoOfLotsOrResidentialUnits"].Replace(",", "")));
                    return;
            }
           
            base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
        }




    }
}