﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExnternalWeb.Helpers.ModelBinders
{
    public class PoultryWateringFormBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext,
                                           ModelBindingContext bindingContext,
                                           System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {

            switch (propertyDescriptor.Name)
            {
                case "NumberOfHouses":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".NumberOfHouses"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName+".NumberOfHouses"].Replace(",", "")));
                    return;
                case "NumberOfFlocksPerYear":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".NumberOfFlocksPerYear"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".NumberOfFlocksPerYear"].Replace(",", "")));
                    return;
                case "BirdsPerFock":
                    if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".BirdsPerFock"]))
                        propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + ".BirdsPerFock"].Replace(",", "")));
                    return;
               
            }

            base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
        }
    }
}