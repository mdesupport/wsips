﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders
{
    public class RemoveCommaModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext,
                                             ModelBindingContext bindingContext,
                                             System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {
            var propertyType = propertyDescriptor.PropertyType.FullName;

            if (propertyType.Contains("System.Int32"))
            {
                var name = propertyDescriptor.Name;

                if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + "." + propertyDescriptor.Name]))
                    propertyDescriptor.SetValue(bindingContext.Model, Convert.ToInt32(controllerContext.HttpContext.Request.Form[bindingContext.ModelName + "." + propertyDescriptor.Name].Replace(",", "")));
                return;
            }

            base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
        }




    }
}