﻿using System.Collections.Generic;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Models
{
    [ModelBinder(typeof(GridCommandModelBinder))]
    public class GridCommand
    {
        public int Page { get; set; }

        [Binding(Name = "rows")]
        public int PageSize { get; set; }

        [Binding(Name = "sidx")]
        public string SortField { get; set; }

        [Binding(Name = "sord")]
        public string SortOrder { get; set; }

        public int Skip()
        {
            return (Page - 1) * PageSize;
        }

        public string BuildSort()
        {
            if (string.IsNullOrEmpty(SortField)) return null;

            return propertyMap[SortField] + " " + SortOrder;
        }

        public string BuildSort(IDictionary<string, string> propertyMap)
        {
            if (string.IsNullOrEmpty(SortField)) return null;

            return propertyMap[SortField] + " " + SortOrder;
        }

        private Dictionary<string, string> propertyMap = new Dictionary<string, string>
        {
            { "permitnumber", "PermitNumber" },
            { "permittee", "Permittee" },
            { "effectivedate", "EffectiveDate" },
            { "expirationdate", "ExpirationDate" },
            { "permitstatus", "LU_PermitStatus.Description" },
            { "projectmanager", "ProjectManager" },
            { "usecode", "UseCode" }
        };
    }
}
