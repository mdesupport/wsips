﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Models
{
    public class GridData
    {
        public GridData()
        {

        }

        public GridData(IEnumerable data)
        {
            Data = data;
        }

        public int PageTotal { get; set; }

        public int CurrentPage { get; set; }

        public int RecordCount { get; set; }

        public IEnumerable Data { get; set; }
    }
}