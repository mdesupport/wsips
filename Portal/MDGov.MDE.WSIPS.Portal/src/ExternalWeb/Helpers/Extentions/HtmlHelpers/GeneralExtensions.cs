﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Extensions
{
    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString DatePicker(this HtmlHelper html, string name, DateTime? value = null)
        {
            return MvcHtmlString.Create(html.TextBox(name, value.HasValue ? value.Value.ToString("MM/dd/yyyy") : null, new { @class = "date" }).ToString());
        }

        public static MvcHtmlString DatePickerFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string fieldName = ExpressionHelper.GetExpressionText(expression);

            var date = (DateTime?)metadata.Model;

            return MvcHtmlString.Create(html.TextBox(fieldName, date.HasValue ? date.Value.ToString("MM/dd/yyyy") : null, new { @class = "date", @readonly = "readonly" }).ToString());
        }

        public static MvcHtmlString DatePickerFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string fieldName = ExpressionHelper.GetExpressionText(expression);




            var date = (DateTime?)metadata.Model;



            return MvcHtmlString.Create(html.TextBox(fieldName, date.HasValue ? date.Value.ToString("MM/dd/yyyy") : null, new { @class = "date", @readonly = "readonly" }).ToString());
        }

        public static MvcHtmlString RadioButtonList(this HtmlHelper helper, string id, Dictionary<int, string> RadioOptions, int selectedvalue, string className, bool horizontal = true)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var myOption in RadioOptions)
            {
                bool isSelected = selectedvalue == myOption.Key;
                if (horizontal)
                {
                    sb.Append("<span>" + helper.RadioButton(id, myOption.Key, isSelected, new { @class = className }) + myOption.Value + "</span>");
                }
                else
                {
                    sb.Append("<div>" + helper.RadioButton(id, myOption.Key, isSelected, new { @class = className }) + myOption.Value + "</div>");
                }
            }

            return new MvcHtmlString(sb.ToString());
        }

        public static MvcHtmlString RadioButtonList(this HtmlHelper helper, string id, Dictionary<bool, string> RadioOptions, bool selectedvalue, bool horizontal = true)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var myOption in RadioOptions)
            {
                bool isSelected = selectedvalue == myOption.Key;
                if (horizontal)
                {
                    sb.Append("<span>" + helper.RadioButton(id, myOption.Key, isSelected) + myOption.Value + "</span>");
                }
                else
                {
                    sb.Append("<div>" + helper.RadioButton(id, myOption.Key, isSelected) + myOption.Value + "</div>");
                }
            }

            return new MvcHtmlString(sb.ToString());
        }

        public static object GetModelStateValue(this HtmlHelper helper, string key, Type destinationType)
        {
            ModelState modelState;
            if (helper.ViewData.ModelState.TryGetValue(key, out modelState))
            {
                if (modelState.Value != null)
                {
                    return modelState.Value.ConvertTo(destinationType, null /* culture */);
                }
            }
            return null;
        }
    }
}