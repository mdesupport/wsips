﻿using System.Collections;

namespace System.Web.Mvc
{
    public class AttributedSelectList : AttributedMultiSelectList
    {
        public AttributedSelectList(IEnumerable items)
            : this(items, null /* selectedValue */)
        {
        }

        public AttributedSelectList(IEnumerable items, object selectedValue)
            : this(items, null /* dataValuefield */, null /* dataTextField */, selectedValue)
        {
        }

        public AttributedSelectList(IEnumerable items, string dataValueField, string dataTextField)
            : this(items, dataValueField, dataTextField, null /* selectedValue */)
        {
        }

        public AttributedSelectList(IEnumerable items, string dataValueField, string dataTextField, object selectedValue)
            : base(items, dataValueField, dataTextField, ToEnumerable(selectedValue))
        {
            SelectedValue = selectedValue;
        }

        public object SelectedValue { get; private set; }

        private static IEnumerable ToEnumerable(object selectedValue)
        {
            return (selectedValue != null) ? new object[] { selectedValue } : null;
        }
    }
}