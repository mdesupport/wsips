﻿
namespace System.Web.Mvc
{
    public class AttributedSelectListItem
    {
        public bool Selected { get; set; }

        public string Text { get; set; }

        public string Value { get; set; }

        public object HtmlAttributes { get; set; }
    }
}