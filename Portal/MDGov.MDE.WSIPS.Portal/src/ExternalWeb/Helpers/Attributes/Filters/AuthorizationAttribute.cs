﻿using System.Linq;
using StructureMap;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using System.Web.Security;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Filters
{
    /// <summary>
    /// Custom authorization attribute
    /// </summary>
    public class AuthorizationAttribute : AuthorizeAttribute
    {
        private string[] _authorizedRoles;
        public string[] AuthorizedRoles
        {
            get { return _authorizedRoles ?? new string[0]; }
            set { _authorizedRoles = value; }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new JsonResult
                    {
                        Data = new { IsAuthenticated = false, LoginUrl = FormsAuthentication.LoginUrl },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

        /// <summary>
        /// Main authorization logic
        /// </summary>
        /// <param name="httpContext">HttpContextBase</param>
        /// <returns>Boolean</returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            return true;

            //// Check that the user is active
            //var isActive = SessionHandler.GetSessionVar<bool>(SessionVariables.AuthenticatedUserIsActive);
            //if (!isActive)
            //{
            //    return false;
            //}

            //// If no roles are supplied to the attribute just check that the user is logged in and active.
            //if (AuthorizedRoles.Length == 0)
            //{
            //    return true;
            //}

            //// Check if the active logged in user is a member of the roles
            //var roles = SessionHandler.GetSessionVar<IEnumerable<string>>(SessionVariables.AuthenticatedUserRoles);
            //return AuthorizedRoles.Any(r => roles.Contains(r));
        }
    }
}