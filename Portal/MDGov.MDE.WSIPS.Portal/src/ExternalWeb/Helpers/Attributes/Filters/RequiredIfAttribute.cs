﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes.Filters
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredIfAttribute : RequiredAttribute, IClientValidatable
    {
        public string DependentProperty { get; set; }

        /// <summary>
        /// This can be one or more values to check against
        /// </summary>
        public object[] TargetValues { get; set; }

        public ComparisonType ComparisonType { get; set; }

        public RequiredIfAttribute(string dependentProperty, params object[] targetValues)
        {
            this.DependentProperty = dependentProperty;
            this.TargetValues = targetValues;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var containerType = validationContext.ObjectInstance.GetType();
            var field = containerType.GetProperty(this.DependentProperty);

            var returnValue = ValidationResult.Success;

            if (field != null)
            {
                var dependentvalue = field.GetValue(validationContext.ObjectInstance, null);

                switch (ComparisonType)
                {
                    case ComparisonType.NotEqual:
                        if (dependentvalue == null)
                        {
                            if (this.TargetValues != null)
                            {
                                if (!base.IsValid(value))
                                {
                                    returnValue = new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
                                }
                            }
                        }
                        else
                        {
                            if (this.TargetValues == null)
                            {
                                if (!base.IsValid(value))
                                {
                                    returnValue = new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
                                }
                            }
                            else
                            {
                                if (!this.TargetValues.Any(val => dependentvalue.Equals(val)))
                                {
                                    if (!base.IsValid(value))
                                    {
                                        returnValue = new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
                                    }
                                }
                            }
                        }

                        break;
                    default: // We do equal by default
                        if (dependentvalue == null)
                        {
                            if (this.TargetValues == null)
                            {
                                if (!base.IsValid(value))
                                {
                                    returnValue = new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
                                }
                            }
                        }
                        else
                        {
                            if (this.TargetValues != null)
                            {
                                if (this.TargetValues.Any(val => dependentvalue.Equals(val)))
                                {
                                    if (!base.IsValid(value))
                                    {
                                        returnValue = new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
                                    }
                                }
                            }
                        }

                        break;
                }
            }

            return returnValue;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule()
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "requiredif",
            };

            var depProp = BuildDependentPropertyId(metadata, context as ViewContext);

            if (this.TargetValues == null)
            {
                rule.ValidationParameters.Add("targetvalues", string.Empty);
            }
            else
            {
                List<string> targetValueParameters = new List<string>();
                foreach (var targetVal in this.TargetValues)
                {
                    if (targetVal.GetType() == typeof(bool))
                    {
                        targetValueParameters.Add(targetVal.ToString().ToLower());
                    }
                    else
                    {
                        targetValueParameters.Add(targetVal.ToString());
                    }
                }

                rule.ValidationParameters.Add("targetvalues", string.Join(",", targetValueParameters));
            }

            rule.ValidationParameters.Add("dependentproperty", depProp);
            rule.ValidationParameters.Add("comparisontype", ComparisonType.ToString());

            yield return rule;
        }

        private string BuildDependentPropertyId(ModelMetadata metadata, ViewContext viewContext)
        {
            var depProp = viewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(this.DependentProperty);
            var thisField = metadata.PropertyName + "_";

            if (depProp.StartsWith(thisField))
                depProp = depProp.Substring(thisField.Length);

            return depProp;
        }
    }

    public enum ComparisonType
    {
        Equal,
        NotEqual,
    }
}