﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class BindingAttribute : Attribute
    {
        public string Name { get; set; }
    }
}