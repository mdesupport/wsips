﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Attributes
{
    public class FilterSequenceAttribute : Attribute
    {
        public FilterSequenceAttribute(int sequence)
        {
            Sequence = sequence;
        }

        public int Sequence { get; set; }
    }
}