﻿using System;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SpatialFilterAttribute : Attribute
    {
    }
}