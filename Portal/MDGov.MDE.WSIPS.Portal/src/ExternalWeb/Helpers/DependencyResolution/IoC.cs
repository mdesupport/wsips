using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Logging;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Service;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.DependencyResolution
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });

                x.For<IRepository<ErrorLog>>().Use(ctx => new Repository<ErrorLog>(ctx.GetInstance<LoggingContext>()));
                x.For<IErrorLogging>().Use<ErrorLogging>();

                x.For(typeof(IService<>)).Use(typeof(ServiceBase<>));
                x.For(typeof(IUpdatableService<>)).Use(typeof(UpdatableService<>));
                
                x.For<IPermitWaterWithdrawalPurposeService>().Use<PermitWaterWithdrawalPurposeService>();
                x.For<IPermitLocationService>().Use<PermitLocationService>();
                x.For<IMembershipAuthenticationService>().Use<MembershipAuthenticationService>();
            });

            return ObjectFactory.Container;
        }
    }
}