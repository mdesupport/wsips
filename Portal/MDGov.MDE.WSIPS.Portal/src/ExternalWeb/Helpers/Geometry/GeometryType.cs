﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Geometry
{
    public class GeometryType
    {
        public const string Point = "POINT";
        public const string Polygon = "POLYGON";
        public const string Multipolygon = "MULTIPOLYGON";
        public const string Multipoint = "MULTIPOINT";
    }
}