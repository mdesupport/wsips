﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Geometry
{
    public class EsriJsonWktParser
    {
        public EsriJsonWktParser(string wellKnownText, int spatialReference)
        {
            WellKnownText = wellKnownText;
            SpatialReference = spatialReference;
        }

        public string WellKnownText { get; set; }

        public int SpatialReference { get; set; }

        public object Parse()
        {
            var geomType = WellKnownText.Substring(0, WellKnownText.IndexOf('(')).Trim();

            switch (geomType)
            {
                case GeometryType.Point:
                    throw new NotImplementedException();
                case GeometryType.Polygon:
                case GeometryType.Multipolygon:
                    return ParsePolygon(WellKnownText.Substring(WellKnownText.IndexOf('(')));
                case GeometryType.Multipoint:
                    throw new NotImplementedException();
                default:
                    throw new Exception("Invalid Well Known Text");
            }
        }

        private object ParsePolygon(string rings)
        {
            var ringArray = rings.Split(new[] { "(", "), (", ")" }, StringSplitOptions.RemoveEmptyEntries);
            var result = new string[ringArray.Length][][];

            for (int i = 0; i < ringArray.Length; i++)
            {
                var points = ringArray[i].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                result[i] = new string[points.Length][];

                for (int j = 0; j < points.Length; j++)
                {
                    var xy = points[j].Trim().Split(' ');
                    result[i][j] = xy;
                }
            }

            return new { rings = result, spatialReference = new { wkid = SpatialReference } };
        }
    }
}