﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum
{
    public enum FilterTemplate
    {
        [Display(Name = "Default")]
        DefaultAuthenticated,
        [Display(Name = "Public")]
        Public
    }

    public enum ContactPurpose
    {
        InformationVerification = 1,
        NewOrInformationVerfication = 2
    }

    public enum ContactType
    {
        WaterUser = 27,
        LandOwner = 16,
        Consultant = 8
    }

    public enum DashboardMode
    {
        Tabular,
        Map,
        Analysis
    }

    public enum WaterWithdrawalPurpose
    {
        LivestockWatering = 1,
        Poultry_Evaporative = 2,
        Poultry_Foggers = 3,
        DairyAnimalWatering = 4,
        OtherLivestockWatering = 5,
        FarmPotableSupplies = 6,
        AgriculturalIrrigation = 7,
        NurseryIrrigation = 8,
        SodFarmIrrigation = 9,
        FoodProcessing = 10,
        AquacultureAndAquarium = 11,
        Irrigation_Undefined = 12,
        GolfCourseIrrigation = 13,
        LawnAndParkIrrigation = 14,
        SmallIntermittentIrrigation = 15,
        IndustrialDrinkingSanitary = 16,
        MiningPotableSupplies = 17,
        IndustrialWashAndSeparationProcesses = 18,
        MineConstructionAndDewateringMiningOnly = 19,
        IndustrialHeatingAndcoolingWater = 20,
        SandAndGravelWashing = 21,
        ProductManufacturing = 22,
        Industrial_Undefined = 23,
        MiningOperations_Undefined = 24,
        WaterWellDrillingOperations = 25,
        OtherWellDrillingOperationsExcludingFracking = 26,
        FrackingOperationsForGasAndOil = 27,
        FossilFueledPowerGeneration = 28,
        FossilFueledPowerOnceThruCooling = 29,
        FossilFueledPowerCoolingTowerBlowby = 30,
        FossilFueledPowerBoilerBlowby = 31,
        FossilFueledPowerPollutionControl = 32,
        FossilFueledPowerOtherUses = 33,
        NuclearPowerGeneration = 34,
        HydroelectricPowerGeneration = 35,
        GeothermalPowerGeneration = 36,
        GovernmentRunWaterSupply = 37,
        PrivateWaterSupplier = 38,
        TrailerPark_ApartmentBldg_Condo = 39,
        CommercialDrinking_Sanitary = 40,
        InstitutionalDrinking_Sanitary = 41,
        EducationalDrinking_Sanitary = 42,
        SocialDrinking_Sanitary = 43,
        ReligiousDrinking_Sanitary = 44,
        FireAndRescueDrinking_Sanitary = 45,
        GovernmentDrinking_Sanitary = 46,
        UndefinedDrinking_Sanitary = 47,
        RecreationalDrinking_Sanitary = 48,
        SewageTreatmentPlantsOrAllUses = 49,
        CommercialHeatingAndCoolingWater = 50,
        CommercialWashingProcesses = 51,
        Laboratories = 52,
        CommercialOrUndefined = 53,
        SubdivisionswithIndividualWells = 54,
        ResidentialHeatPumps = 55,
        PermanentDewateringofBuildings = 56,
        WildlifePondsandRecreational = 57,
        EnvironmentalEnhancement = 58,
        GroundWaterCleanup = 59,
        HydrostaticTestingAndFireProtection = 60,
        ConstructionDewatering = 61
    }

    public enum IrrigationType
    {
        IrrigationUndefined = 1,
        GolfCourseIrrigation = 2,
        LawnAndParkIrrigation = 3,
        SmallIntermitentIrrigation = 4,
        NurseryIrrigation = 5,
        SodIrrigation = 6
    }
}