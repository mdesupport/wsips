﻿using System.Web.Security;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common
{
    // The FormsAuthentication type is sealed and contains static members, so it is difficult to
    // unit test code that calls its members. The interface and helper class below demonstrate
    // how to create an abstract wrapper around such a type in order to make the AccountController
    // code unit testable.
    public interface IFormsAuthenticationHelper
    {
        void SignIn(string userName);

        void SignOut();
    }

    /// <summary>
    /// FormsAuthenticationHelper used for sign in/out
    /// </summary>
    public partial class FormsAuthenticationHelper : IFormsAuthenticationHelper
    {
        public void SignIn(string userName)
        {
            FormsAuthentication.SetAuthCookie(userName, false);
        }

        public void SignOut()
        {
            SessionHandler.ClearAllSessionVars();
            FormsAuthentication.SignOut();
        }
    }
}
