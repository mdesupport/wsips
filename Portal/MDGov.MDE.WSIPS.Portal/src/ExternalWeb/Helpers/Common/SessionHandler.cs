﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service.Interface;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common
{
    public enum SessionVariables
    {
        AuthenticatedUserUserId,
        AuthenticatedUserContactId,
        AuthenticatedUserContactTypeId,
        CurrentlySelectedInvoiceId,
        CurrentlyLoggedinUserFullName,
        CurrentlyLoggedinUserId,
        DashboardFilter,
        DashboardNonSpatialFilteredPermitIds,
        DashboardSearch,
        DashboardSpatialFilter,
        DashboardSpatialFilteredPermitIds,
        KmlFileMemoryStream,
        PermitIds,
        SelectedDashboardFilterTemplate
    }

    public class SessionHandler
    {
        public static object GetSessionVar(SessionVariables sessionVariable)
        {
            return HttpContext.Current.Session[sessionVariable.ToString()];
        }

        public static T GetSessionVar<T>(SessionVariables sessionVariable)
        {
            T result;
            try
            {
                result = (T)HttpContext.Current.Session[sessionVariable.ToString()];
            }
            catch
            {
                result = default(T);
            }

            return result;
        }

        public static void SetSessionVar(SessionVariables sessionVariable, object value)
        {
            HttpContext.Current.Session[sessionVariable.ToString()] = value;
        }

        public static void ClearAllSessionVars()
        {
            HttpContext.Current.Session.RemoveAll();
        }

        internal static bool BuildSession(string userName)
        {
            var membershipService = ObjectFactory.GetInstance<IMembershipAuthenticationService>();
            var contactService = ObjectFactory.GetInstance<IService<Contact>>();

            var userId = membershipService.GetUserIdByName(userName);
            if (userId == 0)
            {
                // User no longer exists.
                return false;
            }

            var contact = contactService.GetRange(0, 1, null, new[] { new DynamicFilter("UserId == " + userId) }).FirstOrDefault();
            if (contact == null)
            {
                // No Contact Record.  Unlikely, but possible
                return false;
            }

            SessionHandler.SetSessionVar(SessionVariables.CurrentlyLoggedinUserId, userId);
            SessionHandler.SetSessionVar(SessionVariables.CurrentlyLoggedinUserFullName, (contact == null) ? "" : String.Format("{0} {1}", contact.FirstName, contact.LastName));
            SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserContactId, contact.Id);
            SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserContactTypeId, contact.ContactTypeId);

            return true;
        }
    }
}