﻿using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.ServiceClient;
using MDGov.MDE.Common.ServiceLayer;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Controllers
{
    public partial class UploadController : BaseController
    {
        private readonly IService<LU_TableList> _tableListService;
        private readonly IUpdatableService<Document> _documentService;
        private readonly IUpdatableService<DocumentByte> _documentByteService;
        private readonly IService<Permit> _permitService;
        private readonly IUpdatableService<ConditionCompliance> _conditionComplianceService;

        public UploadController(
            IService<LU_TableList> tableListService,
            IUpdatableService<Document> documentService,
            IUpdatableService<DocumentByte> documentByteService,
            IService<Permit> permitService,
            IUpdatableService<ConditionCompliance> conditionComplianceService)
        {
            _tableListService = tableListService;
            _documentService = documentService;
            _documentByteService = documentByteService;
            _permitService = permitService;
            _conditionComplianceService = conditionComplianceService;
        }

        public virtual ActionResult Document(string refTable, int refId, int? permitStatusId, bool includeRefIdDocs = false)
        {
            var tableList = _tableListService.GetRange(0, 1, null, new[] { new DynamicFilter(String.Format("Description==\"{0}\"", refTable)) });

            // populate the viewbag items
            ViewBag.RefTableId = tableList.First().Id;
            ViewBag.RefId = refId;
            ViewBag.PermitStatusId = permitStatusId;
            ViewBag.IncludeRefIdDocs = includeRefIdDocs;
            ViewBag.IsAuthenticated = Request.IsAuthenticated;

            // set permissions for deletion and upload
            // 1= View Only
            // 2= Upload and View
            // 3= upload, view, and delete
            ViewBag.PermissionLevel = 3;

            // get documents from database
            //var docs = _documentService.GetRange(0, 99999, "", new DynamicFilter[] { new DynamicFilter(String.Format("refId=={0} and refTableId=={1} and permitStatusId{2}", refId, ViewBag.RefTableId, permitStatusId == null ? " == " + System.Data.SqlTypes.SqlInt32.Null : " == " + permitStatusId)) });

            // pass documents to the view
            return PartialView(MVC.Upload.Views.UploadDocument);
        }

        private string GetDirectory(Document doc)
        {
            string path = WebConfigurationManager.AppSettings["UploadFolder"].ToString();

            if (doc.PermitStatusId == null)
            {

                if (!Directory.Exists(path + doc.RefTableId + "\\" + doc.RefId))
                    Directory.CreateDirectory(path + doc.RefTableId + "\\" + doc.RefId);
                return doc.RefTableId + "\\" + doc.RefId;
            }
            else
            {
                if (!Directory.Exists(path + doc.RefTableId + "\\" + doc.RefId + "\\" + doc.PermitStatusId))
                    Directory.CreateDirectory(path + doc.RefTableId + "\\" + doc.RefId + "\\" + doc.PermitStatusId);
                return doc.RefTableId + "\\" + doc.RefId + "\\" + doc.PermitStatusId;
            }
        }

        [HttpGet]
        public virtual FileResult GetFile(int id)
        {
            var file = _documentService.GetRange(0, 1, null, new[] { new DynamicFilter("Id == @0", id) }, "DocumentByte").First();

            return File(file.DocumentByte.Document, file.DocumentByte.MimeType, file.DocumentTitle);
        }

        [HttpPost]
        public virtual JsonResult Upload(Document doc)
        {
            if (doc.RefId == 0 || Request.Files[0].ContentLength == 0)
            {
                return Json(new { IsSuccess = false }, "text/html");
            }

            var dByte = new DocumentByte();

            if (doc.Id > 0)
            {
                // if this is an existing document, we need to update rather than create new
                var existingDoc = _documentService.GetById(doc.Id);
                if (existingDoc == null)
                {
                    return Json(new { IsSuccess = false }, "text/html");
                }

                dByte.Id = existingDoc.DocumentByteId;
            }

            // set the title
            doc.DocumentTitle = Uri.EscapeDataString(Path.GetFileName(Request.Files[0].FileName));

            if (!doc.DocumentTypeId.HasValue && doc.DocumentTitle.Length > 10 && doc.DocumentTitle.Substring(0, 11).ToLower() == "draftpermit")
            {
                doc.DocumentTypeId = 1; // 1 = Draft Permit
            }

            HttpPostedFileBase file = Request.Files[0];
            MemoryStream target = new MemoryStream();
            file.InputStream.CopyTo(target);

            dByte.Document = target.ToArray();
            dByte.MimeType = file.ContentType;

            doc.DocumentByteId = _documentByteService.Save(dByte);
            _documentService.Save(doc);

            // If upload is Condition Compliance Report, set Condition Compliance status to "Submitted"
            if (doc.RefTableId == 69 /*69 is ConditionCompliance*/)
            {
                var conditionCompliance = _conditionComplianceService.GetById(doc.RefId);
                conditionCompliance.PermitConditionComplianceStatusId = 5;
                _conditionComplianceService.Save(conditionCompliance);
            }

            return Json(new { IsSuccess = true }, "text/html");
        }

        [HttpGet]
        public virtual JsonResult Data(int id, int refTableId, int permitStatusId, bool includeRefIdDocs = false)
        {
            List<DynamicFilter> filter = new List<DynamicFilter>();
            List<int> combinedPermitStatuses = new List<int> { 22, 23, 32, 33, 41, 42, 59, 62, 65 };
            List<int> combinedRefIds = new List<int> { id };

            if (includeRefIdDocs)
            {
                var permit = _permitService.GetById(id);
                if (permit != null && permit.RefId.HasValue)
                {
                    var refPermit = _permitService.GetById(permit.RefId.Value);

                    if (refPermit != null && refPermit.PermitStatusId == 3 /*Completed Applications*/)
                    {
                        combinedRefIds.Add(permit.RefId.Value);
                    }
                }
            }

            filter.Add(new DynamicFilter("@0.Contains(outerIt.RefId)", combinedRefIds));

            if (!Request.IsAuthenticated)
            {
                filter.Add(new DynamicFilter("PermitStatusId == @0", 13));
            }
            else if (permitStatusId == 22 || permitStatusId == 23 || permitStatusId == 32 || permitStatusId == 33
                    || permitStatusId == 41 || permitStatusId == 42 || permitStatusId == 59 || permitStatusId == 62
                    || permitStatusId == 65)
            {
                filter.Add(new DynamicFilter("@0.Contains(outerIt.PermitStatusId)", combinedPermitStatuses));
            }
            else if (permitStatusId != -1)
            {
                filter.Add(new DynamicFilter("PermitStatusId == @0", permitStatusId));
            }

            var docs = _documentService.GetRange(0, int.MaxValue, "PermitStatusId", filter, "LU_PermitStatus").Select(x => new
            {
                Id = x.Id,
                DocumentTitle = Uri.UnescapeDataString(x.DocumentTitle),
                DocumentTypeId = x.DocumentTypeId,
                Description = x.Description,
                CreatedDate = x.CreatedDate,
                CreatedBy = x.CreatedBy,
                PermitStatusDescription = x.LU_PermitStatus != null ? x.LU_PermitStatus.Description : "",
                PermitStatusId = x.PermitStatusId ?? -1
            });

            return Json(new
            {
                Data = docs,
                DocumentGetPath = System.Web.Configuration.WebConfigurationManager.AppSettings["DocumentGetPath"].ToString()
            }, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult Delete(int id)
        {
            try
            {
                var documentByteId = _documentService.GetById(id).DocumentByteId;
                _documentService.Delete(id);
                _documentByteService.Delete(documentByteId);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public virtual string GetPermitDocument(int PermitId)
        {
            var temp = _documentService.GetRange(0, int.MaxValue, "", new[] { new DynamicFilter(String.Format("RefId=={0} and DocumentTypeId=={1}", PermitId, 3)) });
            if (temp != null && temp.Count() > 0)
                return System.Web.Configuration.WebConfigurationManager.AppSettings["DocumentGetPath"].ToString() + temp.Last().Id;
            else
                return "";
        }
    }
}