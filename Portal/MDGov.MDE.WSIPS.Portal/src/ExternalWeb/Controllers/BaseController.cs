﻿using System;
using System.Web.Mvc;
using MDGov.MDE.Common.Logging.Interface;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers
{
    public partial class BaseController : Controller
    {
        /// <summary>
        /// Log exception or message
        /// </summary>
        /// <param name="exc"></param>
        protected void HandleException(Exception exc)
        {
            if (exc != null)
            {
                ModelState.AddModelError(String.Empty, exc.Message);
                var logger = ObjectFactory.GetInstance<IErrorLogging>();
                logger.Log(exc);
            }
        }
    }
}
