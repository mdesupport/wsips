﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.Communication.Model;
using System.Configuration;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers
{
    public partial class CommentController : BaseController
    {
        private readonly IUpdatableService<Comment> _commentService;
        private readonly IService<LU_TableList> _tableListService;
        private readonly IUpdatableService<Message> _messageService;
        private readonly IUpdatableService<PermitContact> _permitContactService;

        public CommentController(
            IService<LU_TableList> tableListService,
            IUpdatableService<Comment> commentService,
            IUpdatableService<Message> messageService,
            IUpdatableService<PermitContact> permitContactService)
        {
            _commentService = commentService;
            _tableListService = tableListService;
            _messageService = messageService;
            _permitContactService = permitContactService;
        }

        public virtual ActionResult LoadCommentsView(string refTable, int refId, int permitStatusId, string callbackfunction)
        {
            var tableList = _tableListService.GetRange(0, 1, null, new [] { new DynamicFilter("Description==@0", refTable) });

            // populate the viewbag items
            ViewBag.RefTableId = tableList.First().Id;
            ViewBag.RefId = refId;
            ViewBag.PermitStatusId = permitStatusId;
            ViewBag.CallbackFunction = callbackfunction;

            return PartialView(MVC.Comment.Views.Comment);
        }

        [HttpGet]
        public virtual JsonResult Data(int id, int permitStatus)
        {
            List<DynamicFilter> filter = new List<DynamicFilter>();

            if (permitStatus == -1)
            {
                filter.Add(new DynamicFilter(String.Format("refId=={0}", id)));
            }
            else
            {
                filter.Add(new DynamicFilter(String.Format("refId=={0} && PermitStatusId=={1}", id, permitStatus)));
            }


            var comments = _commentService.GetRange(0, int.MaxValue, "PermitStatusId", filter, "LU_PermitStatus").Select(
                x => new
                {
                    Comment1 = x.Comment1.Length > 50 ? x.Comment1.Substring(0, 50) + "..." : x.Comment1,
                    LastModifiedDate = x.LastModifiedDate,
                    LastModifiedBy = x.LastModifiedBy,
                    CreatedBy = x.CreatedBy,
                    PermitStatusId = x.LU_PermitStatus != null ? x.LU_PermitStatus.Description : "",
                    Id = x.Id,
                    StatusId = x.PermitStatusId ?? -1
                }
            );


            var temp = Json(new
            {
                Data = comments
            }, JsonRequestBehavior.AllowGet);

            return temp;
        }

        [HttpGet]
        public virtual void SaveComment(Comment comment)
        {
            if (comment.PermitStatusId < 1)
                comment.PermitStatusId = null;

            _commentService.Save(comment);
        }

        public virtual JsonResult Delete(int id)
        {
            try
            {
                _commentService.Delete(id);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { success = false });
            }
        }

        public virtual JsonResult GetComment(int id)
        {
            string userName = User.Identity.Name;//System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            var comment = _commentService.GetById(id);

            return Json(new { userName = userName, comment = comment }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult SendNotificationEmail(int permitId)
        {
            var uri = new UriBuilder(Request.Url);
            uri.Query = null;
            uri.Path = Url.Action(MVC.Permitting.Permit.Details(permitId));
            var permitDetailsUrl = uri.Uri.ToString();

            //var manager = _permitContactService.GetRange(0, 1, null, new[] { new DynamicFilter("PermitId == " + permitId), new DynamicFilter("ContactTyeId == " + 20) }, "Contact");
            var manager = _permitContactService.GetRange(0, 1, null, new[] { new DynamicFilter("PermitId == " + permitId), new DynamicFilter("ContactTypeId == " + 20) });
            if (manager != null)
            {

                //Create a message object for the change
                var message = new Message();
                message.ContactIds = new List<int> { manager.First().ContactId };
                message.NotificationText = "A Comment Has Been Added By DNR";
                message.NotificationTypeId = 1;
                message.PermitId = permitId;
                message.StartDate = DateTime.MinValue;
                message.EndDate = DateTime.MaxValue;
                message.From = ConfigurationManager.AppSettings["SenderEmail"];
                message.Subject = "A Comment Has Been Added By DNR";
                message.Body = "A Comment Has Been Added By DNR. <p><a href=\"" + permitDetailsUrl + "\">" + permitDetailsUrl + "</a></p>";

                var saveid =_messageService.Save(message);
                return Json(new { saveid = saveid }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
    }
}
