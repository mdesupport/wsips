﻿using System;
using System.Linq;
using System.Web.Mvc;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Filters;
using MDGov.MDE.Communication.Model;
using System.Collections.Generic;
using System.Configuration;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers
{
    public partial class PublicCommentController : BaseController
    {
        private readonly IUpdatableService<PublicComment> _publicCommentService;
        private readonly IUpdatableService<Message> _messageService;
        private readonly IUpdatableService<PermitContact> _permitContactService;

        public PublicCommentController(
            IUpdatableService<PublicComment> publicCommentService,
            IUpdatableService<Message> messageService,
            IUpdatableService<PermitContact> permitContactService)
        {
            _publicCommentService = publicCommentService;
            _messageService = messageService;
            _permitContactService = permitContactService;
        }

        [HttpPost]
        public virtual void SaveComment(PublicComment publicComment)
        {
            _publicCommentService.Save(publicComment);

            if (publicComment.Id == 0)
            {
                // New comment added.  Send a notification to PM, Supervisor, and Division Chief
                var permitContacts = _permitContactService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitId == @0 && @1.Contains(outerIt.ContactTypeId) && Active", publicComment.PermitId, new [] { 20, 24, 32 }) });

                if (permitContacts != null && permitContacts.Any())
                {
                    var uri = new UriBuilder(Request.Url);
                    uri.Query = null;
                    uri.Path = Url.Action(MVC.Permitting.Permit.Details(publicComment.PermitId));
                    var permitDetailsUrl = uri.Uri.ToString();

                    var message = new Message
                    {
                        ContactIds = permitContacts.Select(pc => pc.ContactId).ToList(),
                        NotificationText = "Public Comment Submitted",
                        NotificationTypeId = 1,
                        PermitId = publicComment.PermitId,
                        StartDate = DateTime.MinValue,
                        EndDate = DateTime.MaxValue,
                        From = ConfigurationManager.AppSettings["SenderEmail"],
                        Subject = "Public Comment Submitted",
                        Body = "A citizen has submitted a comment regarding a permit " +
                        "currently under MDE review. Please click the link below and " +
                        "navigate to the \"Comments\" tab to view the new comment. " +
                        "<p><a href=\"" + permitDetailsUrl + "\">" + permitDetailsUrl + "</a></p>",
                    };

                    _messageService.Save(message);
                }
            }
        }
    }
}