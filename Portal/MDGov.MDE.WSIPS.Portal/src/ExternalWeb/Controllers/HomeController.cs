﻿using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers
{
    public partial class HomeController : BaseController
    {
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}
