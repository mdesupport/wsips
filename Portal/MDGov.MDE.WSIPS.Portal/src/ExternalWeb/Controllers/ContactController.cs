﻿using System.Linq;
using System.Web.Mvc;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers
{
    public partial class ContactController : BaseController
    {
        private readonly IService<LU_ZipCode> _zipCodeService;

        public ContactController(
            IService<LU_ZipCode> zipCodeService)
        {
            _zipCodeService = zipCodeService;
        }

        [HttpGet]
        public virtual JsonResult GetZipCodeAutoPopulateValues(string zipCode)
        {
            var zip = _zipCodeService.GetRange(0, 1, null, new[] { new DynamicFilter("ZipCode == @0", zipCode) }).SingleOrDefault();

            if (zip != null)
            {
                return Json(new { City = zip.City, StateId = zip.StateId }, JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
