// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments
#pragma warning disable 1591
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Controllers
{
    public partial class AccountController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected AccountController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Login()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Login);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult ConfirmAccount()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ConfirmAccount);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult ResetPassword()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ResetPassword);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult ValidateContact()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.ValidateContact);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Register()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Register);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public AccountController Actions { get { return MVC.Security.Account; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "Security";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Account";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Account";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Login = "Login";
            public readonly string ConfirmAccount = "ConfirmAccount";
            public readonly string LogOff = "LogOff";
            public readonly string ForgotPasswordRequest = "ForgotPasswordRequest";
            public readonly string PasswordChangeInstructions = "PasswordChangeInstructions";
            public readonly string PasswordResetInstructions = "PasswordResetInstructions";
            public readonly string PasswordChanged = "PasswordChanged";
            public readonly string ChangePassword = "ChangePassword";
            public readonly string ResetPassword = "ResetPassword";
            public readonly string CheckEmailToConfirm = "CheckEmailToConfirm";
            public readonly string ValidateContact = "ValidateContact";
            public readonly string Register = "Register";
            public readonly string EditAccount = "EditAccount";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Login = "Login";
            public const string ConfirmAccount = "ConfirmAccount";
            public const string LogOff = "LogOff";
            public const string ForgotPasswordRequest = "ForgotPasswordRequest";
            public const string PasswordChangeInstructions = "PasswordChangeInstructions";
            public const string PasswordResetInstructions = "PasswordResetInstructions";
            public const string PasswordChanged = "PasswordChanged";
            public const string ChangePassword = "ChangePassword";
            public const string ResetPassword = "ResetPassword";
            public const string CheckEmailToConfirm = "CheckEmailToConfirm";
            public const string ValidateContact = "ValidateContact";
            public const string Register = "Register";
            public const string EditAccount = "EditAccount";
        }


        static readonly ActionParamsClass_Login s_params_Login = new ActionParamsClass_Login();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Login LoginParams { get { return s_params_Login; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Login
        {
            public readonly string returnUrl = "returnUrl";
            public readonly string model = "model";
        }
        static readonly ActionParamsClass_ConfirmAccount s_params_ConfirmAccount = new ActionParamsClass_ConfirmAccount();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ConfirmAccount ConfirmAccountParams { get { return s_params_ConfirmAccount; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ConfirmAccount
        {
            public readonly string confirmation = "confirmation";
        }
        static readonly ActionParamsClass_ForgotPasswordRequest s_params_ForgotPasswordRequest = new ActionParamsClass_ForgotPasswordRequest();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ForgotPasswordRequest ForgotPasswordRequestParams { get { return s_params_ForgotPasswordRequest; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ForgotPasswordRequest
        {
            public readonly string UserName = "UserName";
        }
        static readonly ActionParamsClass_ChangePassword s_params_ChangePassword = new ActionParamsClass_ChangePassword();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ChangePassword ChangePasswordParams { get { return s_params_ChangePassword; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ChangePassword
        {
            public readonly string login = "login";
        }
        static readonly ActionParamsClass_ResetPassword s_params_ResetPassword = new ActionParamsClass_ResetPassword();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ResetPassword ResetPasswordParams { get { return s_params_ResetPassword; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ResetPassword
        {
            public readonly string token = "token";
            public readonly string login = "login";
        }
        static readonly ActionParamsClass_ValidateContact s_params_ValidateContact = new ActionParamsClass_ValidateContact();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ValidateContact ValidateContactParams { get { return s_params_ValidateContact; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ValidateContact
        {
            public readonly string contactId = "contactId";
        }
        static readonly ActionParamsClass_Register s_params_Register = new ActionParamsClass_Register();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Register RegisterParams { get { return s_params_Register; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Register
        {
            public readonly string contactId = "contactId";
            public readonly string form = "form";
        }
        static readonly ActionParamsClass_EditAccount s_params_EditAccount = new ActionParamsClass_EditAccount();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_EditAccount EditAccountParams { get { return s_params_EditAccount; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_EditAccount
        {
            public readonly string contact = "contact";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string ChangePassword = "ChangePassword";
                public readonly string CheckEmailToConfirm = "CheckEmailToConfirm";
                public readonly string ConfirmAccount = "ConfirmAccount";
                public readonly string EditAccount = "EditAccount";
                public readonly string ForgotPasswordRequest = "ForgotPasswordRequest";
                public readonly string Login = "Login";
                public readonly string PasswordChanged = "PasswordChanged";
                public readonly string PasswordResetInstructions = "PasswordResetInstructions";
                public readonly string Register = "Register";
                public readonly string ResetPassword = "ResetPassword";
            }
            public readonly string ChangePassword = "~/Areas/Security/Views/Account/ChangePassword.cshtml";
            public readonly string CheckEmailToConfirm = "~/Areas/Security/Views/Account/CheckEmailToConfirm.cshtml";
            public readonly string ConfirmAccount = "~/Areas/Security/Views/Account/ConfirmAccount.cshtml";
            public readonly string EditAccount = "~/Areas/Security/Views/Account/EditAccount.cshtml";
            public readonly string ForgotPasswordRequest = "~/Areas/Security/Views/Account/ForgotPasswordRequest.cshtml";
            public readonly string Login = "~/Areas/Security/Views/Account/Login.cshtml";
            public readonly string PasswordChanged = "~/Areas/Security/Views/Account/PasswordChanged.cshtml";
            public readonly string PasswordResetInstructions = "~/Areas/Security/Views/Account/PasswordResetInstructions.cshtml";
            public readonly string Register = "~/Areas/Security/Views/Account/Register.cshtml";
            public readonly string ResetPassword = "~/Areas/Security/Views/Account/ResetPassword.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_AccountController : MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Controllers.AccountController
    {
        public T4MVC_AccountController() : base(Dummy.Instance) { }

        [NonAction]
        partial void LoginOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string returnUrl);

        [NonAction]
        public override System.Web.Mvc.ActionResult Login(string returnUrl)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Login);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "returnUrl", returnUrl);
            LoginOverride(callInfo, returnUrl);
            return callInfo;
        }

        [NonAction]
        partial void ConfirmAccountOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string confirmation);

        [NonAction]
        public override System.Web.Mvc.ActionResult ConfirmAccount(string confirmation)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ConfirmAccount);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "confirmation", confirmation);
            ConfirmAccountOverride(callInfo, confirmation);
            return callInfo;
        }

        [NonAction]
        partial void LoginOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.LoginModel model, string returnUrl);

        [NonAction]
        public override System.Web.Mvc.ActionResult Login(MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.LoginModel model, string returnUrl)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Login);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "model", model);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "returnUrl", returnUrl);
            LoginOverride(callInfo, model, returnUrl);
            return callInfo;
        }

        [NonAction]
        partial void LogOffOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult LogOff()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.LogOff);
            LogOffOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void ForgotPasswordRequestOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult ForgotPasswordRequest()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ForgotPasswordRequest);
            ForgotPasswordRequestOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void ForgotPasswordRequestOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string UserName);

        [NonAction]
        public override System.Web.Mvc.ActionResult ForgotPasswordRequest(string UserName)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ForgotPasswordRequest);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "UserName", UserName);
            ForgotPasswordRequestOverride(callInfo, UserName);
            return callInfo;
        }

        [NonAction]
        partial void PasswordChangeInstructionsOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult PasswordChangeInstructions()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.PasswordChangeInstructions);
            PasswordChangeInstructionsOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void PasswordResetInstructionsOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult PasswordResetInstructions()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.PasswordResetInstructions);
            PasswordResetInstructionsOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void PasswordChangedOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult PasswordChanged()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.PasswordChanged);
            PasswordChangedOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void ChangePasswordOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult ChangePassword()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ChangePassword);
            ChangePasswordOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void ChangePasswordOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.ResetPasswordModel login);

        [NonAction]
        public override System.Web.Mvc.ActionResult ChangePassword(MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.ResetPasswordModel login)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ChangePassword);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "login", login);
            ChangePasswordOverride(callInfo, login);
            return callInfo;
        }

        [NonAction]
        partial void ResetPasswordOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string token);

        [NonAction]
        public override System.Web.Mvc.ActionResult ResetPassword(string token)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ResetPassword);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "token", token);
            ResetPasswordOverride(callInfo, token);
            return callInfo;
        }

        [NonAction]
        partial void ResetPasswordOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.ResetPasswordModel login);

        [NonAction]
        public override System.Web.Mvc.ActionResult ResetPassword(MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.ResetPasswordModel login)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ResetPassword);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "login", login);
            ResetPasswordOverride(callInfo, login);
            return callInfo;
        }

        [NonAction]
        partial void CheckEmailToConfirmOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult CheckEmailToConfirm()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CheckEmailToConfirm);
            CheckEmailToConfirmOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void ValidateContactOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int contactId);

        [NonAction]
        public override System.Web.Mvc.JsonResult ValidateContact(int contactId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.ValidateContact);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contactId", contactId);
            ValidateContactOverride(callInfo, contactId);
            return callInfo;
        }

        [NonAction]
        partial void RegisterOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int contactId);

        [NonAction]
        public override System.Web.Mvc.ActionResult Register(int contactId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Register);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contactId", contactId);
            RegisterOverride(callInfo, contactId);
            return callInfo;
        }

        [NonAction]
        partial void RegisterOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.RegisterModel form);

        [NonAction]
        public override System.Web.Mvc.ActionResult Register(MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.RegisterModel form)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Register);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "form", form);
            RegisterOverride(callInfo, form);
            return callInfo;
        }

        [NonAction]
        partial void EditAccountOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult EditAccount()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.EditAccount);
            EditAccountOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void EditAccountOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.RegisterModel contact);

        [NonAction]
        public override System.Web.Mvc.JsonResult EditAccount(MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models.RegisterModel contact)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.EditAccount);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contact", contact);
            EditAccountOverride(callInfo, contact);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591
