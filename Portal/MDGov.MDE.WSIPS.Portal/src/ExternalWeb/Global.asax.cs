﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.RegisterProfiles();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Already have an auth ticket.  Create new session vars.
                if (!SessionHandler.BuildSession(User.Identity.Name))
                {
                    //Something failed creating the new session, kill existing authentication ticket
                    FormsAuthentication.SignOut();
                    //Redirect
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
    }
}