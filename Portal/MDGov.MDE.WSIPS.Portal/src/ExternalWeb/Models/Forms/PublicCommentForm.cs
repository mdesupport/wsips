﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Models.Forms
{
    public class PublicCommentForm
    {
        public int publicComentDbId { get; set; }

        [DisplayName("Name")]
        [Required(ErrorMessage = "Name is required")]
        public string PublicCommentName { get; set; }

        [DisplayName("Contact Info")]
        [Required(ErrorMessage = "Contact Info is required")]
        public string PublicCommentContactInfo { get; set; }

        [DisplayName("Comment")]
        [Required(ErrorMessage = "Comment is required")]
        public string thePublicComment { get; set; }
    }
}