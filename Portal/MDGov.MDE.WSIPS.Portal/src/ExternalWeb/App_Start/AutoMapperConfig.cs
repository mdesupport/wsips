﻿using AutoMapper;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Ecommerce.Helpers.Configuration;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Configuration;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Configuration;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb
{
    public static class AutoMapperConfig
    {
        public static void RegisterProfiles()
        {
            Mapper.Initialize(x =>
            {
                x.ConstructServicesUsing(ObjectFactory.GetInstance);
                x.AddProfile<RootAutoMapperProfile>();
                x.AddProfile<PermittingAreaAutoMapperProfile>();
                x.AddProfile<SecurityAreaMapperProfile>();
                x.AddProfile<EcommerceAreaAutoMapperProfile>();
            });
        }
    }
}
