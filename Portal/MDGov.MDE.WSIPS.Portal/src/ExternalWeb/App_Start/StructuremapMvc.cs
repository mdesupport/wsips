using System.Web.Http;
using System.Web.Mvc;
using StructureMap;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.DependencyResolution;

[assembly: WebActivator.PreApplicationStartMethod(typeof(MDGov.MDE.WSIPS.Portal.ExternalWeb.App_Start.StructuremapMvc), "Start")]

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.App_Start
{
    public static class StructuremapMvc {
        public static void Start() {
			IContainer container = IoC.Initialize();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }
    }
}