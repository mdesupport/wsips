﻿/// <reference path="jquery-1.4.4-vsdoc.js" />
/// <reference path="jquery.validate.unobtrusive.js" />

// Conditional Validation
$.validator.addMethod('requiredif',
    function (value, element, parameters) {
        var id = '#' + parameters['dependentproperty'];

        // get the target value (as a string, 
        // as that's what actual value will be)
        var targetvalues = parameters['targetvalues'];
        targetvalues =
          (targetvalues == null ? '' : targetvalues).toString();

        // get the actual value of the target control
        // note - this probably needs to cater for more 
        // control types, e.g. radios
        var control = $(id);

        var actualvalue;
        if (control.is('input:checkbox')) {
            actualvalue = control.attr('checked').toString();
        }
        else {
            actualvalue = control.val();
        }

        var valuesArray = targetvalues.split(",");

        switch (parameters['comparisontype']) {
            case 'NotEqual':
                // if the condition is true, reuse the existing 
                // required field validator functionality
                var isMatch = false;
                for (i in valuesArray) {
                    if (valuesArray[i] == actualvalue)
                        isMatch = true;
                }
                if (isMatch == false) {
                    return $.validator.methods.required.call(this, value, element, parameters);
                }
                break;
            default:
                for (i in valuesArray) {
                    if (valuesArray[i] == actualvalue)
                        return $.validator.methods.required.call(this, value, element, parameters);
                }
                break;
        }

        return true;
    }
);

$.validator.unobtrusive.adapters.add(
    'requiredif',
    ['dependentproperty', 'targetvalues', 'comparisontype'],
    function (options) {
        options.rules['requiredif'] = {
            dependentproperty: options.params['dependentproperty'],
            targetvalues: options.params['targetvalues'],
            comparisontype: options.params['comparisontype']
        };
        options.messages['requiredif'] = options.message;
    });


// Date Range Validation
// The validator function
$.validator.addMethod('rangeDate',
      function (value, element, param) {
          if (!value) {
              return true; // not testing 'is required' here!
          }
          try {
              var dateValue = $.datepicker.parseDate("mm/dd/yy", value);
          }
          catch (e) {
              return false;
          }
          return param.min <= dateValue && dateValue <= param.max;
      });

// The adapter to support ASP.NET MVC unobtrusive validation
$.validator.unobtrusive.adapters.add('rangedate', ['min', 'max'],
     function (options) {
         var params = {
             min: $.datepicker.parseDate("mm/dd/yy", options.params.min),
             max: $.datepicker.parseDate("mm/dd/yy", options.params.max)
         };

         options.rules['rangeDate'] = params;
         if (options.message) {
             options.messages['rangeDate'] = options.message;
         }
     });