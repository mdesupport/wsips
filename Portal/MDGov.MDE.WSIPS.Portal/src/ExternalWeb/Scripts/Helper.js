﻿var Helper = {
    GetBaseURL: function (controllerName) {

        var currentUrl = document.location;
        var splitStringArray = currentUrl.href.split('/');
        var baseUrl = '';

        for (var i = 0; i < splitStringArray.length; i++) {
            if (splitStringArray[i] == controllerName) {
                break;
            }
            else {
                baseUrl += splitStringArray[i] + "/";
            }
        }
        return baseUrl;
    }
};