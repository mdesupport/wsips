﻿///// <reference path="../jquery-1.8.2.js" />
(function ($) {
    var jmttgModal = function () {
        var show = function () {
            // $(this) is still our link/button
            var $this = $(this),
                data = $this.data('modal'),
                $dialog = data.dialog;

            $dialog.load($this.attr("contenturl"), onModalDialogLoadComplete);
        };

        var onModalDialogLoadComplete = function () {
            // $(this) is now our dialog
            var $this = $(this);
            $.validator.unobtrusive.parse($this);
            $this.find('input[type=checkbox]').checkbox();
            $this.find("input[type=button],button").button();
            $this.find("input[type=submit]").button();

            if ($.isFunction($.unblockUI)) {
                $.unblockUI();
            }

            $this.dialog('open');
        };

        this.init = function (options) {
            return this.each(function () {
                // $(this) is our link/button
                var $this = $(this),
                    data = $this.data('modal');

                // If the plugin hasn't been initialized yet
                if (!data) {
                    $this.data('modal', {
                        dialog: $('<div />').dialog($.extend({
                            modal: true,
                            title: $this.attr('title'),
                            height: 'auto',
                            width: 'auto',
                            position: 'center',
                            draggable: false,
                            resizable: false,
                            autoOpen: false,
                            close: function() {
                                $(this).empty();
                            }
                        }, options))
                    });

                    $this.click(show);
                }
            });
        };
    };
    
    //return {
    //    ShowDialog: function (dialogOptions) {
    //        $('#Modal').load($(this).attr("contenturl"), options.OnModalDialogLoadComplete(dialogOptions));
    //        return false;
    //    },
    //    CloseDialog: function () {
    //        $('#Modal').dialog("close");
    //        return false;
    //    }
    //    //BindButton: function () {
    //    //    $(this).click($.jmttgCustomModal().ShowDialog());
    //    //}
    //};

    $.fn.modal = function (method) {
        var modal = new jmttgModal();
        if (modal[method]) {
            return modal[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return modal.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.modal');
        }
    };

})(jQuery);