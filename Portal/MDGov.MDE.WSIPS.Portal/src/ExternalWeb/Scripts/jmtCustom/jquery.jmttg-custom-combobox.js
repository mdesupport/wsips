﻿/// <reference path="../jquery-1.8.2.js" />
var selectLeft = -1;
var selectTop = -1;
var selectHeight = -1;                 //selectbox height
var selectWidth = -1;                 //selectbox width
var targetSelect;                        //actual html select to be replaced
var textPadding = 7;                    //padding around text within custom select (non-dropdown)
var arySelectOptions = new Array();     //array of select options
var maxScrollerHeight = 150;              //maximum scroller height
var scrollbarBarMin = -1;
var scrollbarBarMax = -1;

jQuery.extend({
    jmttgCustomComboBox: function () {
        return {

            //call this AFTER custom scrolling of a parent container
            // or the dropdown will be in the wrong location!
            init: function (htmlSelect) {
                //htmlselect must be a jquery object!

                //set global variables
                $.jmttgCustomComboBox().setSelectVariableValues(htmlSelect);
                //hide html select
                $.jmttgCustomComboBox().hideSelect();
                //insert after select
                $.jmttgCustomComboBox().insertAfter();
                //create the dropdown div
                $.jmttgCustomComboBox().createDropDown()
            },
            setSelectVariableValues: function (htmlSelect) {
                targetSelect = htmlSelect;
                selectHeight = htmlSelect.outerHeight();
                selectWidth = htmlSelect.outerWidth();
                var pos = htmlSelect.position();
                selectLeft = pos.left;
                selectTop = pos.top;

            },
            insertAfter: function () {
                targetSelect.after("<div id='selectTextArea' style='position:relative;min-height:" + $.jmttgCustomComboBox().getTextAreaheight() + "px; min-width:" + $.jmttgCustomComboBox().getTextAreaWidth() + "px;background:#ff0000;display:inline-block;line-height:" + $.jmttgCustomComboBox().getTextAreaheight() + "px;vertical-align:middle;padding:" + textPadding + "px;'>test<div style='position:absolute;right:3px;min-height:20px;min-width:20px;background-color:blue;text-align:center;vertical-align:middle;line-height:20px;display:inline-block;bottom:5px;cursor:pointer' onclick='$.jmttgCustomComboBox().showDDLClickEvent();'>+</div></div>");
            },
            showDDLClickEvent: function () {
                //can be updated to use an animation (slideDown/Up)
                if ($('#dropDownParentOuter').is(':visible')) {
                    $("#dropDownParentOuter").hide();
                }
                else {
                    $("#dropDownParentOuter").show();
                }
            },
            getTextAreaheight: function(){
                return eval(selectHeight - (2 * textPadding));
            },
            getTextAreaWidth: function (){
                return eval(selectWidth - (2 * textPadding));
            },
            hideSelect: function () {
                targetSelect.hide();
            },
            hideDropdown: function () {

            },
            createDropDown: function () {
                //Put all select option (val/text) into arySelectOptions
                $.jmttgCustomComboBox().populateOptionArray();
                //create dropdown parent div
                $.jmttgCustomComboBox().createDropDownParentDiv();
                //populate with html select options
                $.jmttgCustomComboBox().populateDropDownParentdiv();
                //create scrollbar top & bottom arrows
                $.jmttgCustomComboBox().createScrollbarHandles();


            },
            createScrollbarHandles: function(){
                var ddlParentOuter = $("#dropDownParentOuter");
                //create scroller click buttons
                ddlParentOuter.append($.jmttgCustomComboBox().createSingleScrollbarArrows());
                //create scroller background between arrows
                ddlParentOuter.append($.jmttgCustomComboBox().createSrollbarBackground());
                //add scroller background scroll bar
                $("#scrollerBackground").append($.jmttgCustomComboBox().createScrollbarBar());
                

            },
            createScrollbarBar: function(){
                var rtnHTML = "";
                var mWidth = 11;
                var bckgdWidth = $.jmttgCustomComboBox().getScrollerBackgroundWidth();
                //get differnece for indent
                var indent = eval(eval(bckgdWidth - mWidth)/2);
                //NEED TO CALCULATE HEIGHT!*/
                rtnHTML += "<div id='scrollbarBar' style='min-height:" + mWidth + "px;min-width:" + mWidth + "px;left:" + indent + "px;top:0px;position:absolute;background-color:#0000ff'></div>";
                return rtnHTML;
            },
            setScrollbarBarLocation: function (location) {
                var temp = location;
                /*
                if (location < scrollbarBarMin) { location = scrollbarBarMin; }
                if (location > scrollbarBarMax) { location = scrollbarBarMax; }
                */
                $("#scrollbarBar").css("top", location);

                //alert("\location: " + location);
            },

            calculateScrollbarBarLocationFromScrolltop: function(scrolltop){

                //percentage of hidden ddl items
                var percentHidden = eval(scrolltop / $("#holder").height());

                var percentAlwaysShowing =  $.jmttgCustomComboBox().getScrollBottom()/$("#holder").height();
                //alert(percentAlwaysShowing);
                //alert(percentHidden);
                //percentage as applied to scrollbarbackground height
                var scrollbarBarPercentageLocation = percentHidden * $.jmttgCustomComboBox().getScrollerBackgroundHeight();

                alert(percentHidden + " - " + percentAlwaysShowing);
                //percentage
                var scrollbarBarAsPercent = eval(scrollbarBarPercentageLocation * .01);
                
                scrollbarBarMin = 0;
                scrollbarBarMax = $.jmttgCustomComboBox().getScrollerBackgroundHeight() - 11;
                //GOOD! alert(scrollbarBarMax);
                var calcScrollPos = scrollbarBarMax * scrollbarBarAsPercent;

                //alert("percentage: " + scrollbarBarPercentageLocation + "\nmax value" + bottom);
                //set the location
                $.jmttgCustomComboBox().setScrollbarBarLocation(calcScrollPos);
            },
            createSrollbarBackground: function(){
                var rtnHTML = "";
                //get scrollbar arrow dimentions
                var scrollArrowHeight = $.jmttgCustomComboBox().getScrollbarArrowHeight();
                var scrollArrowWidth = $.jmttgCustomComboBox().getScollbarArrowWidth();
                //get css location values
                var top = eval($.jmttgCustomComboBox().getScrollTop() + scrollArrowHeight);
                var height =  eval($.jmttgCustomComboBox().getScrollBottom() - eval(2*scrollArrowHeight));
                var left = $.jmttgCustomComboBox().getScrollerLeftPosition();
                //update return var
                rtnHTML += "<div id='scrollerBackground' style='position:absolute;top:" + top + "px;height:" + height + "px;left:" + left + "px;width:" + scrollArrowWidth + "px;background-color:#ffffff;z-index:9999992'></div>";
                return rtnHTML;
            },
            getScrollerBackgroundHeight: function(){
                return parseInt($("#scrollerBackground").css("height"));
            },
            getScrollbarArrowHeight: function(){
                return parseInt($("#topScroll").css("min-height"));
            },
            getScollbarArrowWidth: function(){
                return parseInt($("#topScroll").css("min-width"));
            },
            getScrollerLeftPosition: function(){
                //**************************************************************
                //position over existing scroller
                //textarea width + text padding
                var left = eval($("#selectTextArea").width() + textPadding);
                //**************************************************************
                //var left = selectLeft;
                //**************************************************************

                return left;
            },
            getScrollbarBarHeight: function(){
                return parseInt($("#scrollbarBar").outerHeight());
            },
            getScrollerBackgroundHeight: function(){
                return parseInt($("#scrollerBackground").outerHeight());
            },
            getScrollerBackgroundWidth: function(){
                return parseInt($("#scrollerBackground").outerWidth());
            },
            getScrollBottom: function(){
                return parseInt($("#dropDownParentDiv").css("max-height"));
            },
            getScrollTop: function(){
                return parseInt(eval(selectTop + selectHeight));
            },
            createSingleScrollbarArrows: function () {
                var rtnHTML = "";
                //get top location
                var top = $.jmttgCustomComboBox().getScrollTop();
                //get left location
                var left = $.jmttgCustomComboBox().getScrollerLeftPosition();
                //scroller height minus the height of the scroller div
                var height = eval( top + eval(maxScrollerHeight - 18));
                //create div's
                //top div
                rtnHTML += "<div id='topScroll' onclick='$.jmttgCustomComboBox().scrollClickEvent(\"up\");' style='position:absolute;min-height:18px;min-width:17px;top:" + top + "px;left:" + left + "px;line-height:18px,vertical-align:middle,text-align:center;background-color:#ff0000;z-index:9999991'></div>";
                //bottom div
                rtnHTML += "<div id='bottomScroll' onclick='$.jmttgCustomComboBox().scrollClickEvent(\"down\");' style='position:absolute;min-height:18px;min-width:17px;top:" + height + "px;left:" + left + "px;line-height:18px,vertical-align:middle,text-align:center;background-color:#ff0000;z-index:9999991'></div>";
                //return HTML
                return rtnHTML;
            },
            scrollClickEvent: function (direction) {
                
                //get current scroll location
                var currentScrollPos = $("#dropDownParentDiv").scrollTop();
                //modify based on up/down click
                if (direction == "up") {
                    currentScrollPos -= 40;
                }
                else {
                    currentScrollPos += 40;
                }
                //send updated value to scroll
                $.jmttgCustomComboBox().setScrollPosition(currentScrollPos);
                
            },
            setScrollPosition: function(location){
                $("#dropDownParentDiv").scrollTop(location);
                //move the scrollbar bar to correct location
                $.jmttgCustomComboBox().calculateScrollbarBarLocationFromScrolltop(location);
            },
            populateDropDownParentdiv: function(){
                for (var i = 0; i < arySelectOptions.length; i++) {
                    //get array of option values & text
                    var singleOption = arySelectOptions[i];
                    //create div & append to dropdown parent
                    //$("#dropDownParentDiv").append($.jmttgCustomComboBox().createSingleDropDownListItem(singleOption));
                    $("#holder").append($.jmttgCustomComboBox().createSingleDropDownListItem(singleOption));
                }
            },
            createSingleDropDownListItem: function(singleOption){
                return "<div type='option' id='" + singleOption[0] + "'>" + singleOption[1] + "</div>";
            },
            createDropDownParentDiv: function () {
                var textAreaPos = $("#selectTextArea").position();
                //dropDownParentOuter - outer div that doesn't scroll....holds the custom srcollbar objects
                targetSelect.after("<div id='dropDownParentOuter' style='display:none;padding:none;margin:none;'><div id='dropDownParentDiv' style='min-width:" + selectWidth + "px;max-height:" + maxScrollerHeight + "px;overflow:hidden;position:absolute;top:" + eval(textAreaPos.top + selectHeight) + "px;left:" + textAreaPos.left + "px;background-color:#00ff00;z-index:999999'><div id='holder'></div></div></div>")
            },
            populateOptionArray: function () {
                targetSelect.children().each(function () {
                    var singleOption = new Array(2);
                    singleOption[0] = $(this).val();
                    singleOption[1] = $(this).html()
                    arySelectOptions.push(singleOption);
                });
                //alert(arySelectOptions.length);
            }
        };
    }
});
