﻿/// <reference path="../../Helper.js" />


(function ($) {
    var PendingPermitApplication = {
        grid: $("#PendingApplications"),

        ResumeFormatter: function (cellvalue, options, rowObject) {
            return '<a href="' + Helper.GetBaseURL("Permit") + 'Permit/ApplicationWizard/' + cellvalue + '" >Resume</a>';
        },

        // Grid Population method
        Load: function () {

            PendingPermitApplication.grid.jqGrid({
                ajaxGridOptions: { type: "GET" },
                serializeGridData: function (postdata) {
                    return postdata;
                },
                url: Helper.GetBaseURL("Permit") + 'Permit/LoadPendingPermitApplication/',
                datatype: "json",
                colNames: ['Permittee', 'Created Date', 'Use Description', ''],
                colModel: [
                    { name: 'Permittee', width: 15, index: 'Permittee' },
                    { name: 'CreatedDate', index: 'CreatedDate', width: 10, sortable: false, formatter: PendingPermitApplication.DateFormatter },
                    { name: 'UseDescription', width: 67, index: 'UseDescription' },
                    { name: 'Id', width: 8, sortable: false, formatter: PendingPermitApplication.ResumeFormatter }
                ],
                loadComplete: function (response) {
                    PendingPermitApplication.grid.jqGrid("fixGridWidthWithoutParentScroll");
                    PendingPermitApplication.grid.jqGrid("fixGridHeightWithoutParentScroll");
                },
                gridComplete: function () {
                    PendingPermitApplication.grid.jqGrid("fixGridWidthWithoutParentScroll");
                    PendingPermitApplication.grid.jqGrid("fixGridHeightWithoutParentScroll");
                },
                pager: $('#pager'),
                rowNum: 20,
                scrollOffset: 0,
                forceFit: true,
                autoWidth: true
            });
        },

        DateFormatter: function (cellvalue, options, rowObject) {
            if (cellvalue == null) return '';
            return (new Date(parseInt(cellvalue.replace("/Date(", "").replace(")/", ""), 10))).format('m/dd/yyyy');
        },
    };

    //Load data
    PendingPermitApplication.Load();

})(jQuery);