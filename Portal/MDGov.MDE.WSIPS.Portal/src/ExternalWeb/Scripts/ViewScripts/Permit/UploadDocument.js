﻿/// <reference path="../../Helper.js" />
var UploadDocument = {
    fileNames: new Array(),
    DeleteUrl: "/Upload/Delete",
    UploadUrl: "/Upload/upload/",
    GenerateDraftUrl: "/Document/GenerateDraftPermit/",
    PermitStatusId: "",

    InitializeVariables: function (controller) {
        var Url = Helper.GetBaseURL(controller);
        UploadDocument.DeleteUrl = Url + "Upload/Delete/";
        UploadDocument.UploadUrl = Url + "Upload/upload/";
        UploadDocument.GenerateDraftUrl = Url + "/Document/GenerateDraftPermit/";
    },

    Initialize: function (id, refTableId, permitStatusId, includeRefIdDocs) {
        UploadDocument.PermitStatusId = permitStatusId;
        UploadDocument.IncludeRefIdDocs = includeRefIdDocs == 'True';
        UploadDocument.uploadButtonClick();
        UploadDocument.generatePermitButtonClick(id, refTableId);
        UploadDocument.SetGrid(id, refTableId, permitStatusId, includeRefIdDocs);
    },

    RecreateUploadLink: function (id, refTableId, permitStatusId) {
        UploadDocument.PermitStatusId = permitStatusId
        $("#uploadLink").attr("onclick", "javascript:UploadDocument.upload(" + id + "," + refTableId + "," + permitStatusId + ")");
    },

    uploadButtonClick: function () {
        $("#uploadButton").click(function () {
            // Disable the message 
            window.onbeforeunload = null;

            $("#output").html("");

            $('[name=fileinfo]').detach();

            $('#uploadDialog').prepend("<form enctype='multipart/form-data' method='post' name='fileinfo' id='form1'><div id='fileSpan' class='divVerticalTopSpacer'><input id='fileUploadInput' type='file' size='50' name='file1' accept='.jpg,.pdf,.gif,.png,.doc,.docx,.xls,.xlsx,.txt' required><div class='divVerticalTopSpacer'>Provide a brief description of the uploaded file:</div><textarea style='resize:none' id='fileDesc' rows='4' cols='40'></textarea></div></form>")

            $("#uploadDialog").dialog({
                modal: true,
                position: "center",
                width: 540,
                resizable: false,
                height: 265,
                close: function () {
                    $(this).dialog('close');
                    //$("#dialog").detach()
                    $(this).dialog('destroy');
                    // Show a warning when the user presses the browser's back button or closes the browser
                    if (document.location.toString().indexOf("ApplicationWizard") != -1) {
                        window.onbeforeunload = function () { return "Warning - This will exit the application process and any unsaved data will be lost."; };
                    }
                }
            });
            return false;
        });
    },

    generatePermitButtonClick: function (id, refTableId) {
        $("#generatePermitButton").click(function () {
            var url = UploadDocument.GenerateDraftUrl;
            $.ajax({
                type: 'GET',
                data: { permitId: id, tableid: refTableId, permitStatusId: $('#SelectedStatusId').val() },
                url: url,
                success: function (response) {
                    if (response.IsSuccess) {
                        $('#uploadDashboardGrid').trigger("reloadGrid");
                    }
                },
                error: function () {
                    // Handle error
                },
            });
        });
    },

    generatePermit: function (id, refTableId) {
        var url = UploadDocument.GenerateDraftUrl;
        $.ajax({
            type: 'GET',
            data: { permitId: id, tableid: refTableId, permitStatusId: $('#SelectedStatusId').val() },
            url: url,
            async: false,
            cache: false,
            success: function (response) {
                if (response.IsSuccess) {
                    //$('#uploadDashboardGrid').trigger("reloadGrid");
                }
            },
            error: function (response) {
                // Handle error

                alert("got error");
            },
        });
    },

    SetGrid: function (id, refTableId, permitStatusId, includeRefIdDocs) {
        $("#uploadDashboardGrid").jqGrid({
            colModel: [
                { name: 'fileName', width: "25", jsonmap: "DocumentTitle" },
                { name: 'description', width: "25", jsonmap: "Description" },
                { name: 'uploadDate', width: "12", jsonmap: "CreatedDate", formatter: UploadDocument.DateFormatter },
                { name: 'uploaddBy', width: "12", jsonmap: "CreatedBy" },
                { name: 'permitStatus', width: "12", jsonmap: "PermitStatusDescription" },
                { name: 'viewColumn', width: "5", formatter: UploadDocument.ViewFormatter }
            ],
            url: Helper.GetBaseURL("Permitting") + "Upload/Data?id=" + id + "&refTableId=" + refTableId + "&permitStatusId=" + permitStatusId + "&includeRefIdDocs=" + includeRefIdDocs,
            colNames: ['File Name', 'Description', 'Upload Date', 'Uploaded By', 'Permit Status', ''],

            beforeProcessing: function (data, status, xhr) {
                UploadDocument.DocumentGetPath = data.DocumentGetPath;
                UploadDocument.fileNames = new Array();
            },
            afterInsertRow: function (rowid, aData, rowelem) {
                UploadDocument.CollectFileName("" + rowelem.DocumentTitle + "", "" + rowelem.PermitStatusId + "", "" + rowelem.Id + "");
            },
            gridComplete: function () {
                if (jQuery("#uploadDashboardGrid").jqGrid('getGridParam', 'records') == 0) {
                    $("#gbox_uploadDashboardGrid").hide()
                    $("#gbox_uploadDashboardGrid #pager").hide()
                    $("#MsgNoFileUploaded").detach()
                    //$("#uploadButton").parent().append('<p id="MsgNoFileUploaded">No Files Have Been Uploaded</p>')
                    $("#uploadButtonParent").append('<span id="MsgNoFileUploaded" class="smallItalicText">&nbsp;No Files Have Been Uploaded</span>')
                } else {
                    $("#MsgNoFileUploaded").detach()
                    $("#gbox_uploadDashboardGrid").show()
                    $("#gbox_uploadDashboardGrid #pager").show()
                }
                //add custom scrollbars - only in the dialog popup version - use 'gray' class to make custom scrollbar gray
                // do we already have a scrolling area?
                var len = $("#uploadDashboardGridContainer").find(".scrollcontent").length;
                //no
                if (len == 0) {
                    //add custom scrollbars
                    $('.dialogUploadGridContainer').scrollbars("gray");
                }
            },
            shrinkToFit: true,
            scrollOffset: 0,
            height: "auto"
        });

    },

    DateFormatter: function (cellvalue, options, rowObject) {
        if (cellvalue == null) return '';
        return (new Date(parseInt(cellvalue.replace("/Date(", "").replace(")/", ""), 10))).format('m/dd/yyyy h:MM:ss TT');
    },

    ViewFormatter: function (cellvalue, options, rowelem) {
        return '<div style="text-align:center"><a href="' + UploadDocument.DocumentGetPath + rowelem.Id + '" target="_blank">View</a></div>';
    },

    delFromGrid: function (cl, docTitle) {
        $("#uploadDialog2").dialog({
            modal: true,
            position: "center",
            width: 500,
            close: function () {
                $(this).dialog("destroy")
            },
            buttons: {
                "Yes": function () {
                    UploadDocument.deleteDoc(cl)
                    $('#uploadDashboardGrid').trigger("reloadGrid");

                    $("#uploadDialog2").dialog("close");
                },
                "No": function () {
                    $("#uploadDialog2").dialog("close");
                },
            }
        });
    },

    deleteDoc: function (cl) {
        var _url = UploadDocument.DeleteUrl;
        $.ajax({
            type: 'Delete',
            data: { id: cl },
            url: _url,
            success: function (response) {
                if (response.success) {
                } else {

                }
            }
        });
    },

    getFileNames: function () {
        var x = new Array();
        for (var i = 0; i < UploadDocument.fileNames.length; i++) {
            x[i] = UploadDocument.fileNames[i][1] + "_" + UploadDocument.fileNames[i][0];
        }
        return x;
    },

    fileUpload: function (form, action_url, div_id, refId, refTableId, permitStatusId) {
        var count = 0;
        // Create the iframe...
        var iframe = document.createElement("iframe");
        iframe.setAttribute("id", "upload_iframe");
        iframe.setAttribute("name", "upload_iframe");
        iframe.setAttribute("width", "0");
        iframe.setAttribute("height", "0");
        iframe.setAttribute("border", "0");
        iframe.setAttribute("style", "width: 0; height: 0; border: none;");

        // Add to document...
        form.parentNode.appendChild(iframe);
        window.frames['upload_iframe'].name = "upload_iframe";
        iframeId = document.getElementById("upload_iframe");

        var hiddenField1 = document.createElement("input");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("name", "RefId");
        hiddenField1.setAttribute("value", refId);
        form.appendChild(hiddenField1);

        var hiddenField2 = document.createElement("input");
        hiddenField2.setAttribute("type", "hidden");
        hiddenField2.setAttribute("name", "refTableId");
        hiddenField2.setAttribute("value", refTableId);
        form.appendChild(hiddenField2);

        var hiddenField3 = document.createElement("input");
        hiddenField3.setAttribute("type", "hidden");
        hiddenField3.setAttribute("name", "permitStatusId");
        hiddenField3.setAttribute("value", permitStatusId);
        form.appendChild(hiddenField3);

        var hiddenField4 = document.createElement("input");
        hiddenField4.setAttribute("type", "hidden");
        hiddenField4.setAttribute("name", "DocumentTitle");
        hiddenField4.setAttribute("value", escape($('input[type=file]').val().split('\\').pop()));
        form.appendChild(hiddenField4);

        var hiddenField5 = document.createElement("input");
        hiddenField5.setAttribute("type", "hidden");
        hiddenField5.setAttribute("name", "Description");
        hiddenField5.setAttribute("value", $('#fileDesc').val());
        form.appendChild(hiddenField5);

        //  event...
        var eventHandler = function () {
            // remove event handler if it exists
            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);

            // Message from server...
            if (iframeId.contentDocument) {
                var content = iframeId.contentDocument.body.innerHTML;
                var contentHead = iframeId.contentDocument.title;
            } else if (iframeId.contentWindow) {
                var content = iframeId.contentWindow.document.body.innerHTML;
                var contentHead = iframeId.contentWindow.document.title;
            } else if (iframeId.document) {
                var content = iframeId.document.body.innerHTML;
                var contentHead = iframeId.document.title;
            }

            //if server returns content, add it to the jqgrid
            try {
                //alert(iframeId.contentDocument.childNodes[0].textContent)
                //alert(iframeId.contentDocument.body.innerHTML)
                jQuery.parseJSON(content);

                if (content.length > 0) {
                    // add newly uploaded file info to jqgrid
                    //$("#uploadDashboardGrid").jqGrid("addRowData", 0, eval("(" + content + ")"));
                    $('#uploadDashboardGrid').trigger("reloadGrid");

                    // Del the iframe...
                    setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);

                    //append file info to Uploaddocument filenames array
                    //UploadDocument.fileNames[UploadDocument.fileNames.length] = new Array(escape($('input[type=file]').val().split('\\').pop()), refTableId + '/' + refId + '/' + (!Boolean(permitStatusId) ? '' : permitStatusId + '/') + escape($('input[type=file]').val().split('\\').pop()));
                } else {
                    //alert("An unknow error occured. File was not uploaded");
                }

                if ($("#uploadDialog").dialog("isOpen") === true) {
                    //destroy and close the jquery ui dialog
                    $("#uploadDialog").dialog("close");
                    return;
                    //$("#dialog").dialog("destroy");

                }
            } catch (e) {
                //must not be valid JSON
                if (contentHead == "Maximum request length exceeded." || contentHead == "404 - File or directory not found.") {
                    alert("Error: File was not uploaded. File size may be too big.");

                }
                if (contentHead.indexOf('Access to the path') != -1)
                    alert("Error: Unable to upload file due to server error");

                if ($("#uploadDialog").dialog("isOpen") === true) {
                    //destroy and close the jquery ui dialog
                    $("#uploadDialog").dialog("close");
                    $("#uploadDialog").dialog("destroy");
                }
            }

            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);
        };

        $("#upload_iframe").load(eventHandler);
        
        // Set properties of form...
        form.setAttribute("target", "upload_iframe");
        form.setAttribute("action", action_url);
        form.setAttribute("method", "post");
        form.setAttribute("enctype", "multipart/form-data");
        form.setAttribute("encoding", "multipart/form-data");
        
        // Submit the form...
        form.submit();
    },
    
    fileUploadPermit: function (action_url, div_id, refId, refTableId, permitStatusId) {
        var form = document.getElementById('finalPermitUpload')

        $("#uploadiframe").detach()
        $(form).append('<iframe id="uploadiframe" name="uploadiframe" src="" style="display: none;"></iframe>')

        $("#uploadiframe").unbind('load').bind('load', function () {
            if (document.getElementById("uploadiframe").contentDocument.body.innerHTML == '{"IsSuccess":true}') {
                var _url = Helper.GetBaseURL("Permit");

                _url += "Permit/ApprovePermit";

                $.ajax({
                    url: _url,
                    type: 'Get',
                    data: { PermitId: refId },
                    cache: false,
                    success: function (response) {
                        $("#approveDialog").dialog("close")

                        $("#approveMsgDialog").dialog({
                            modal: true,
                            position: "center",
                            width: 500,
                            close: function () {
                                window.location.reload()
                            }
                        });
                    }
                });
            }
        })

        var hiddenField1 = document.createElement("input");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("name", "RefId");
        hiddenField1.setAttribute("value", refId);
        form.appendChild(hiddenField1);

        var hiddenField2 = document.createElement("input");
        hiddenField2.setAttribute("type", "hidden");
        hiddenField2.setAttribute("name", "refTableId");
        hiddenField2.setAttribute("value", refTableId);
        form.appendChild(hiddenField2);

        var hiddenField3 = document.createElement("input");
        hiddenField3.setAttribute("type", "hidden");
        hiddenField3.setAttribute("name", "permitStatusId");
        hiddenField3.setAttribute("value", permitStatusId);
        form.appendChild(hiddenField3);


        var hiddenField4 = document.createElement("input");
        hiddenField4.setAttribute("type", "hidden");
        hiddenField4.setAttribute("name", "DocumentTypeId");
        hiddenField4.setAttribute("value", 3);
        form.appendChild(hiddenField4);

        document.getElementById('finalPermitUpload').setAttribute("action", action_url);

        document.getElementById('finalPermitUpload').target = 'uploadiframe';
        document.getElementById('finalPermitUpload').submit();
    },
    
    uploadStandardComplianceReport: function (url) {
        var form = $('#ComplianceReportUpload');
        form.submit();
        $('#uploadiframe').unbind('load').bind('load', function () {
            if (document.getElementById('uploadiframe').contentDocument.body.innerHTML == '{"IsSuccess":true}') {
                loadComplianceConditionDetails(url);
            }
        });
    },

    upload: function (refId, refTableId, permitStatusId) {
        //if permitStatus is null
        if (permitStatusId == "-1")
            permitStatusId = "";
        var oOutput = document.getElementById("uploadLink");
        //oOutput.innerHTML = "Uploading....";

        //get file name
        var fileName = $('#fileUploadInput').val().split('\\').pop();

        //if a file by the same name has not been uploaded already
        if (!_.contains(UploadDocument.getFileNames(), UploadDocument.PermitStatusId + "_" + fileName)) {

            //get filename extension
            fileName = $('#fileUploadInput').val().split('\\').pop().split('.').pop().toLowerCase();

            //if file extension is allowed, call fileupload method
            if (fileName == "jpg" || fileName == "pdf" || fileName == "gif" || fileName == "png"
                || fileName == "doc" || fileName == "docx" || fileName == "xls" || fileName == "xlsx"
                || fileName == "txt") {

                //if user has put in a description 
                if ($('#fileDesc').val().length != 0) {
                    //oOutput.innerHTML = "Uploading...."
                    UploadDocument.fileUpload(document.forms.namedItem("fileinfo"), UploadDocument.UploadUrl, "oOutput", refId, refTableId, permitStatusId)
                } else {
                    alert("Provide a brief description of the uploaded file");
                }
            } else {
                alert("You can only upload files with extension .jpg,.pdf,.gif,.png,.doc,.docx,.xls,.xlsx,.txt");
            }
        } else {
            $("#uploadReplaceDialog").dialog({
                width: 550,
                height: 280,
                buttons: {
                    "Yes": function () {
                        UploadDocument.deleteDoc(UploadDocument.GetDocIdFromArray(fileName, UploadDocument.PermitStatusId));
                        $(this).dialog("close");
                        UploadDocument.fileUpload(document.forms.namedItem("fileinfo"), UploadDocument.UploadUrl, "oOutput", refId, refTableId, permitStatusId);
                    },
                    "No": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    },

    GetDocIdFromArray: function (fileName, PermitStatusId) {
        for (var i = 0; i < UploadDocument.fileNames.length; i++) {
            if (UploadDocument.fileNames[i][1] == PermitStatusId && UploadDocument.fileNames[i][0] == fileName)
                return UploadDocument.fileNames[i][2];
        }
    },

    CollectFileName: function (fileName, PermitStatusId, Id) {
        UploadDocument.fileNames[UploadDocument.fileNames.length] = new Array(fileName, PermitStatusId, Id);
    }
};

