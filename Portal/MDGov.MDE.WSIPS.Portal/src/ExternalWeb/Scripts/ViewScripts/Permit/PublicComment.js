﻿var PublicComment = {
    PermitId: "",

    Initialize: function (permitId) {
        PublicComment.PermitId = permitId;

        $("#addPublicCommentButton").click(function (e) {
            PublicComment.AddNewComment();
        });
    },

    AddNewComment: function () {
        var commentContainer = $("#publicCommentContainer").clone().prop('title', 'Add New Public Comment');
        PublicComment.CreateDialog(commentContainer);
    },

    CreateDialog: function (container) {
        $.validator.unobtrusive.parse(container.find("form"));

        container.dialog({
            modal: true,
            title: container.prop('title'),
            position: "center",
            width: 450,
            resizable: false,
            draggable: false,
            open: function () {
                styleDialogInputs();

                container.find('#SavePublicCommentButton').click(function () {
                    if (container.find("form").valid()) {
                        $.ajax({
                            url: Helper.GetBaseURL("Permitting") + "PublicComment/SaveComment",
                            type: "POST",
                            data: { Comment: container.find('#thePublicComment').val(), Name: container.find('#PublicCommentName').val(), ContactInfo: container.find('#PublicCommentContactInfo').val(), PermitId: PublicComment.PermitId, Id: container.find('#publicComentDbId').val() },
                            success: function (response) {
                                container.find('#addPublicCommentForm').html("Comment Added");
                            }
                        });
                    }
                });

                container.find('#CancelPublicCommentButton').click(function () {
                    container.dialog("close");
                });
            },
            close: function () {
                container.dialog("destroy");
            }
        });
    }
}