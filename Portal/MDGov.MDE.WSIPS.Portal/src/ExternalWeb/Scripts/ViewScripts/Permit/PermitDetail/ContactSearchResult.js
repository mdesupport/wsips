﻿/// <reference path="ApplicationWizard.js" />
/// <reference path="../../underscore.js" />

//Bind on change of the radio button
$(document).on('change', '.contacts input:radio', (function (e) {
    var isBusiness = $(e.target).attr("value");

    $('#' + $(e.target).attr('id')).val(isBusiness);
    ContactSearchResult.ClearValidationError();                     // added by Mikyas (1/31/2013)
    ContactSearchResult.AdjustContactInformationLayout(isBusiness);
}));