﻿/// <reference path="ApplicationWizard.js" />
/// <reference path="InformationVerification.js" />

$('#ApplicantIdentification input:checkbox').change(function (e) {
    //Reset the value of the permittee when there is a change
    if (!global.ApplicantInformationChanged) {

        if (confirm("If you make changes to this information, the existing contact information for this permit will be deleted and must be re-entered prior to saving changes to the application. Make this change?")) {
            $('#PermitIssuedto').val('0');
            global.ApplicantInformationChanged = true;
            ApplicantIdentification.SetupOptions();
        }
        else {
            $(this).prop("checked") ? $(this).prop("checked", false) : $(this).prop("checked", true);
        }
    }
    else {
        ApplicantIdentification.SetupOptions();
    }
});

$("#PermitIssuedto").change(function () {
    ApplicantIdentification.DecideWhatToSearch();
});


$.extend(ApplicantIdentification, {

    Initialize: function () {

        ApplicationWizard.ClearSelectedWizardStep();
        $('#SaveButton').show();
        $('#ApplicantIdentification').show();
        global.currentStep = Steps.ApplicantIdentification;
        $('.ApplicantIdentification').addClass('wizardStepActive');

        this.SetupOptions();
    }
});