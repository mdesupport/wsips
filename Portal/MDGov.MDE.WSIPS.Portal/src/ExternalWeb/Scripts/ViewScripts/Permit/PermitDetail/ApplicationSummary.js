﻿var ContactType = {
    WaterUser: 1,
    LandOwner: 2,
    Consultant: 3
};

var Steps = {
    ApplicantIdentification: "ApplicantIdentification",
    ContactSearch: "ContactSearch",
    ContactSearchResult: "ContactSearchResult",
    WaterUseDescription: "WaterUseDescription",
    WaterUseCategoryAndType: "WaterUseCategoryAndType",
    WaterUseDetail: "WaterUseDetail",
    IdentifyLocation: "IdentifyLocation",
    WastewaterTreatmentAndDisposal: "WastewaterTreatmentAndDisposal",
    ReviewApplication: "ReviewApplication",
    Affirmation: "Affirmation"
};

var global = {
    currentStep: "",
    isWaterUser: 0,
    isLandOwner: 0,
    isConsultant: 0,
    SearchResult: null,
    waterUserContactFound: false,
    LandOwnerContactFound: false,
    ConsultantContactFound: false,
    CurrentlySearching: null,
    SelectedWaterUseCategoryTypeList: new Array(),
    isSaveAndResume: false,
    dataSaved: false,
    businessContactId: "",
    showWaterUseDetail: false,
    permittee: 0,
    ApplicantInformationChanged: false,
    ReloadPrivateWaterSupplierView: false,
    ReloadWasteWaterTreatmentAndDisposalPage: false
};

var ApplicationWizard = {
    Initialize: function () {
        $("[id$=TelephoneNumber_CommunicationValue]").each(function () {
            $(this).mask("999-999-9999");
            // if it has value, disable editing 
            if ($(this).val().length > 0) {
                $(this).attr('readonly', true);
            }
        });

        $("[id$=EmailAddress_CommunicationValue]").each(function () {
            // if it has value, disable editing 
            if ($(this).val().length > 0) {
                $(this).attr('readonly', true);
            }
        });

        ApplicationWizard.ClearSelectedWizardStep();

        global.dataSaved == true;
        //ApplicantIdentification.AssignData();
        //ApplicantIdentification.SetupOptions();
        $('.ApplicationSum').addClass('wizardStepActive');
        $("#reviewApplication").show();
        ApplicationWizard.LoadSummary();
        $("#SearchedContacts").append("<div id=newLandOwnersContainer></div>");
        //ApplicationWizard.LoadAdditionalLandOwners();
        ApplicationWizard.InitializeGlobalVariable();
    },

    HideEverything: function () {
        $('#MessageSummary').html('');
        $('#CreatePendingPermit').hide();
        $('#SearchedContacts').hide();
        $('#waterUseDescription').hide();
        $('#waterUseCatagoryAndType').hide();
        $('#waterUseDetails').hide();
        $("#reviewApplication").hide();
        $('#WastewaterTreatmentAndDisposal').hide();
        $('#ApplicantIdentification').hide();
        $('#SearchedContacts #WaterUser').hide();
        $('#SearchedContacts #LandOwner').hide();
        $('#SearchedContacts #Consultant').hide();
        $('#AddLandOwner').hide();
        $("#newLandOwnersContainer").hide();
        $("#WithdrawalSourceInformation").hide();
        $("#identifyLocation").css('z-index', '-9999');
    },

    LoadAdditionalLandOwners: function () {
        var _url = Helper.GetBaseURL("Permit") + "Permit/LoadAdditionalLandOwners";

        $.ajax({
            url: _url,
            type: 'Get',
            data: { 'PermitId': $("#PermitId").val() },
            async: false,
            success: function (response) {

                var items = $(response).children("div").length;
                if (items > 0) {
                    $("#newLandOwnersContainer").append(response);
                    for (i = 0; i < items; i++) {
                        //$("#newLandOwnersContainer").append("<hr />" );
                        $("#newLandOwnersContainer").children("div").eq(i).find("input")
                            .attr('id', function () {
                                return "ContactForm_" + i + "__" + $(this).attr('id');
                            })
                            .attr('name', function () {
                                return "ContactForm[" + i + "]." + $(this).attr('name');
                            })

                        $("#newLandOwnersContainer").children("div").eq(i).find("select")
                            .attr('id', function () {
                                return "ContactForm_" + i + "__" + $(this).attr('id');
                            })
                            .attr('name', function () {
                                return "ContactForm[" + i + "]." + $(this).attr('name');
                            })

                        $("<hr>").insertBefore($("#newLandOwnersContainer").children("div").eq(i))

                    }

                    $("#newLandOwnersContainer").attr("style", "display:none");
                    $("#newLandOwnersContainer").children("div").attr("style", "");
                }
            }
        });
    },

    AddLandOwner: function () {
        var _url = Helper.GetBaseURL("Permit") + "Permit/AddLandOwner";

        $.ajax({
            url: _url,
            type: 'Get',
            data: { 'PermitId': $("#PermitId").val() },
            async: false,
            success: function (response) {
                $("#newLandOwnersContainer").append("<hr />" + response);
                var index = $("#newLandOwnersContainer").children("div").length - 1;

                $("#newLandOwnersContainer").children("div:last-child").find("input")
                    .attr('id', function () {
                        return "ContactForm_" + index + "__" + $(this).attr('id')
                    })
                    .attr('name', function () {
                        return "ContactForm[" + index + "]." + $(this).attr('name')
                    })

                $("#newLandOwnersContainer").children("div:last-child").find("select")
                    .attr('id', function () {
                        return "ContactForm_" + index + "__" + $(this).attr('id')
                    })
                    .attr('name', function () {
                        return "ContactForm[" + index + "]." + $(this).attr('name')
                    })
                    .change(function () {
                        $(this).val($(this).val());
                    }
                    )

                $("#newLandOwnersContainer").children("div:last-child").show();
            }
        });
    },

    GetPermitDocument: function () {
        $.ajax({
            url: Helper.GetBaseURL("Permitting") + "Upload/GetPermitDocument",
            async: false,
            data: { PermitId: $('#PermitId').val() },
            success: function (response) {
                if (response.length > 0)
                    window.open(response)
            }
        });
    },

    Navigate: function (currentStep) {

        if (!global.ApplicantInformationChanged) {
            ApplicationWizard.InitializeGlobalVariable();
        }

        $('#SearchedContacts').show();
        //alert(global.currentStep)

        switch (currentStep) {
            case 1:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                ApplicantIdentification.Initialize();
                break;
            case 2:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUser.Initialize();
                $('#SaveButton').show();
                break;
            case 3:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                LandOwner.Initialize();
                $('#AddLandOwner').show();
                $('#SaveButton').show();
                $("#newLandOwnersContainer").show();
                break;
            case 4:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                Consultant.Initialize();
                $('#SaveButton').show();
                break;
            case 5:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUseDescription.Initialize();
                break;
            case 6:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUseCategoryAndType.Initialize();
                $('#SaveButton').show();
                break;
            case 7:
                var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
                if (selectedValues.length > 0) {
                    ApplicationWizard.ClearSelectedWizardStep();
                    ApplicationWizard.HideEverything();
                    WaterUseDetail.Initialize();
                }
                break;
            case 8:
                var selectedWaterUseCategoryType = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
                if (_.any(selectedWaterUseCategoryType, function (value) { return parseInt(value) > 11; })) {
                    ApplicationWizard.ClearSelectedWizardStep();
                    ApplicationWizard.HideEverything();
                    WastewaterTreatmentAndDisposal.Initialize();
                }
                break;
            case 9:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                $('.ApplicationSum').addClass('wizardStepActive');
                $("#reviewApplication").show();
                ApplicationWizard.LoadSummary();
                $('#SaveButton').hide();
                break;
            case 10:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                $('.CreatePendingPermit').addClass('wizardStepActive');
                $("#CreatePendingPermit").show();
                ApplicationWizard.CreatePendingPermit();
                break;
            case 11:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                $('.waterWithdrawalLocation').addClass('wizardStepActive');
                $("#identifyLocation").show();
                ApplicationWizard.IdentifyLocation.Initialize();
                break;
            default:
                ApplicationWizard.ClearSelectedWizardStep();
                ApplicationWizard.HideEverything();
                WaterUseDescription.Initialize();
                break;
        }
    },

    CollectSelectedWaterUseCategoryType: function () {
        // Get all selected values for water use category and type
        var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
        var commaSeparatedValues = _.reduce(selectedValues, function (memo, num) { return memo + num + ','; }, '');
        $('#SelectedWaterUsePurposeIds').val(commaSeparatedValues);
    },

    SaveAndResumeLater: function () {
        ApplicationWizard.CollectSelectedWaterUseCategoryType();
        //Save data -- submit form
        global.isSaveAndResume = true;
        ApplicationWizard.SaveData();
    },

    SaveData: function () {
        ApplicationWizard.CollectSelectedWaterUseCategoryType();
        var Message = "";

        if (global.ApplicantInformationChanged) {
            if (!global.waterUserContactFound) {
                Message = "Please add the water user contact";

                if (!global.LandOwnerContactFound) {
                    Message = "Please add the water user, and the land owner contact";
                }

                if (!global.ConsultantContactFound) {
                    Message = "Please add the water user, the land owner and the consultant contact";
                }
            } else if (!global.LandOwnerContactFound) {
                Message = "Please add the land owner contact";
                if (!global.ConsultantContactFound) {
                    Message = "Please add the land owner and consultant contact";
                }
            } else if (!global.ConsultantContactFound) {
                Message = "Please add the consultant contact";
            }

            if (Message.length > 0) {
                $('#WarningMessage').html(Message).dialog({
                    modal: true,
                    position: "center",
                    width: 500,
                    buttons: {
                        "OK": function () {
                            $("#WarningMessage").dialog('close');
                        }
                    }
                });
            }
        } else {
            $('#wizardForm').submit();
        }
    },

    SaveCallBack: function (response) {
        if (response.success) {
            $('#PermitId').val(response.permitId);

            //prepare the add additional land owners form
            $("#addLandOwnerForm").empty();
            $("#newLandOwnersContainer").clone().appendTo("#addLandOwnerForm");
            $("#addLandOwnerForm").append('<input type="hidden" id="PermitId" name="PermitId" value=' + $("#PermitId").val() + '>');

            //appending state ids (without doing this, stateIds will not be sent via form submit)
            var items = $("#newLandOwnersContainer").find("select").length;
            for (i = 0; i < items; i++) {
                $("select#ContactForm_" + i + "__StateId").val($("select#ContactForm_" + i + "__StateId").val());
            }

            $('#addLandOwnerForm').submit();
        } else {
            alert('There is a data quality issue that is preventing this permit from saving. Please verify that all required data has been provided for each permit detail section and then save.');
        }
    },

    SaveCallBackAdditionalLandOwners: function (response) {
        if (response.success) {
            for (var i = 0; i < response.Data.length; i++) {
                $("#newLandOwnersContainer").find('#ContactForm_' + i + '__Id').val(response.Data[i])
            }

            ContactSearchResult.ClearValidationError();

            alert('Saved successfully');
        } else {
            alert('There is a data quality issue that is preventing this permit from saving. Please verify that all required data has been provided for each permit detail section and then save.');
        }
    },

    SetPermitStatus: function (permitStatusId) {
        $.blockUI({ message: '<h4>Please wait, page loading...</h4>' });

        var _permitId = $('#PermitId').val();
        _url = Helper.GetBaseURL("Permit") + "Permit/SetPermitStatus/";

        $.ajax({
            url: _url,
            type: 'POST',
            data: { permitId: _permitId, permitStatusId: permitStatusId },
            cache: false,
            success: function (response) {
                if (response.success) {
                    window.location.href = Helper.GetBaseURL("Permit") + "Permit/Details/" + _permitId;
                } else {
                    alert("An error occured trying to change the permit status.");
                }
            }
        });
    },

    SetPreviousPermitStatus: function (permitStatusId) {
        $.blockUI({ message: '<h4>Please wait, page loading...</h4>' });

        var _permitId = $('#PermitId').val();
        _url = Helper.GetBaseURL("Permit") + "Permit/SetPreviousPermitStatus/";

        $.ajax({
            url: _url,
            type: 'POST',
            data: { permitId: _permitId, permitStatusId: permitStatusId },
            cache: false,
            success: function (response) {
                if (response.success) {
                    window.location.href = Helper.GetBaseURL("Permit") + "Permit/Details/" + _permitId;
                } else {
                    alert("An error occured trying to change the permit status.");
                }
            }
        });
    },

    DisplayErrorMessage: function (errorMessage) {
        $('#MessageSummary').html(errorMessage).show();
    },

    LoadwaterUseCatagoryAndTypeView: function () {
        WaterUseCategoryAndType.LoadwaterUseCatagoryAndTypeView();
    },

    LoadwaterUseDetailView: function () {
        var permitId = $('#PermitId').val();
        WaterUseDetail.LoadwaterUseDetailView(permitId);
    },

    RenderbodyRightWidth: function () {
        return $('.renderbodyRight').width() - 50;
    },

    HighlightCurrentWizardStep: function ($div) {
        $div.show();
        $div.addClass('wizardStepActive');
    },

    ClearSelectedWizardStep: function () {
        $("#leftSide .wizardStepActive").removeClass("wizardStepActive");
    },

    InitializeGlobalVariable: function () {
        // When a permit is resumed, we assume that all contact types are found. 
        global.ConsultantContactFound = true;
        global.waterUserContactFound = true;
        global.LandOwnerContactFound = true;
    },

    LoadSummary: function () {
        var url = Helper.GetBaseURL("Permit") + "Permit/summary/";
        var permitId = $("#PermitId").val();
        var ids = global.SelectedWaterUseCategoryTypeList.toString();

        $.ajax({
            url: url,
            type: 'Get',
            async: false,
            data: { id: permitId, waterDetailsSelected: ids },
            success: function (response) {
                $("#applicationSummary").html("").append(response);
                $("#applicationSummary").append('<div><a href="#" onclick="javascript:ApplicationWizard.Print();">Print Permit Summary</a></div>');
            }
        });
    },

    Print: function () {
        var url = Helper.GetBaseURL("Permit") + "Permit/CreatePdf/";
        var ids = global.SelectedWaterUseCategoryTypeList.toString();

        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", url);

        // setting form target to a window named 'formresult'
        form.setAttribute("target", "formresult");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("name", "waterDetailsSelected");
        hiddenField.setAttribute("value", ids);
        form.appendChild(hiddenField);

        var hiddenField2 = document.createElement("input");
        hiddenField2.setAttribute("name", "id");
        hiddenField2.setAttribute("value", $("#PermitId").val());
        form.appendChild(hiddenField2);

        document.body.appendChild(form);

        // creating the 'formresult' window with custom features prior to submitting the form
        window.open("", 'formresult', '');

        form.submit();
    }
};

function localResize() {
    //reset scrolwrap width to full width
    $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();

    //search results does not have custom scrollbars
    if (global.currentStep == Steps.ContactSearchResult) {
        $("#contactGrid").jqGrid("fixGridWidthWithoutParentScroll");
    }
}