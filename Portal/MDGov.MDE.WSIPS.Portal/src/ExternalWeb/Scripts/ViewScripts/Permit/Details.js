﻿$(document).ready(function () {

    //create the permit number, project manager & supervisor info
    applicationDetailsHelper.populateDivLocalNavigation();

    //position & scroll all pulldowns
    applicationDetailsHelper.handlePulldowns();

    //make sure renderbodyrght scrollwrap is 100% width
    $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
    //do the same for rbLEFT
    $.jmttgCustomBrowser.setRenderbodyLeftScrollWrap100Percent();
});

//array if pulldown div id's - Need to add to list if new menu is added
var pulldownIDAry = new Array("permitPulldown", "permitTypePulldown","projectManagerPulldown", "supervisorPulldown");

//called upon browser resize
function localResize() {
    //reposition permit pulldown
    applicationDetailsHelper.handlePulldownPositions();
    //make sure renderbodyrght scrollwrap is 100% width
    $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();

}
var applicationDetailsHelper = {

    selectedManagerId: 0,              //selected manager id
    previousManagerId:0,               //id before Save is clicked
    permitId: 0,                      //permit id
    managerFullName: "",              // selected manager name
    selectedSupervisorId: 0,           //selected supervisor ID
    previousSupervisorId: 0,            //id before save is clicked - does not change on highlight!
    selectedPermitTypeId: 0,              //selected permit type id
    previousPermitTypeId: 0,               //id before Save is clicked
    permitTypeKey:"",
    canEdit:"",
    //renderbody RIGHT
    handlePulldowns: function () {
        //add scrollbars to all pulldowns
        applicationDetailsHelper.handlePulldownScrollbars(null);
        //position all pulldowns
        //applicationDetailsHelper.handlePulldownPositions();
    },
    //adde custom scrollbars
    handlePulldownScrollbars: function ($targetMenu) {
        if ($targetMenu == null) {
            //add scrollcontainer class to pulldowns
            $(".LNPulldownScrollContent").addClass("scrollcontainer");
            //add custom scrollbars
            $(".LNPulldownScrollContent").scrollbars();
            //use green color class for scrollbar display
            $(".LNPulldownScrollContent").find(".scrollblock").addClass("LNPulldownScrollBarColor");
            $(".LNPulldownScrollContent").find(".scrollbtn").addClass("LNPulldownScrollBtnColor");
            //make sure pulldown menus are 100% width
            applicationDetailsHelper.setPulldownScrollsTo100Width(null);
        }
        else {
            $targetMenu.find(".LNPulldownScrollContent").addClass("scrollcontainer");
            $targetMenu.find(".LNPulldownScrollContent").scrollbars();
            $targetMenu.find(".LNPulldownScrollContent").find(".scrollblock").addClass("LNPulldownScrollBarColor");
            $targetMenu.find(".LNPulldownScrollContent").find(".scrollbtn").addClass("LNPulldownScrollBtnColor");
            //make sure pulldown menus are 100% width
            applicationDetailsHelper.setPulldownScrollsTo100Width($targetMenu);
        }
    },
    handlePulldownPositions:function(){
        //position all pulldowns
        applicationDetailsHelper.positionSupervisorMenu();
        applicationDetailsHelper.positionProjectManagerPulldown();
        applicationDetailsHelper.positionPermitPulldown();
        applicationDetailsHelper.positionPermitTypePulldown();
    },
    //hide all menus, skips exceptionMenuID if given
    hideAllMenus: function(exceptionMenuID){
        for (var i = 0; i < pulldownIDAry.length; i++) {
            var $targetMenu = $("#" + pulldownIDAry[i]);
            if (pulldownIDAry[i] != exceptionMenuID) {
                //hide menu
                applicationDetailsHelper.showHide($targetMenu, false);
            }
            else {
                //is the exception menu hidden?
                if (!applicationDetailsHelper.isVisible($targetMenu)) {
                    //show it
                    applicationDetailsHelper.showHide($targetMenu, true);
                }
            }
        }
        //make sure the "selected" highlights are correct
        //applicationDetailsHelper.setAllPulldownSelectedItems();
    },
    closeSupervisorPulldown: function () {
        //set proper select item in pulldown
        applicationDetailsHelper.setLNSupervisorPulldownSelectedItem(applicationDetailsHelper.previousSupervisorId);
        //close the menu
        applicationDetailsHelper.hideAllMenus(null);
    },
    closeManagerPulldown: function () {
        //set proper select item in pulldown
        applicationDetailsHelper.setLNmanagerPulldownSelectedItem(applicationDetailsHelper.previousManagerId);

        //close the menu
        applicationDetailsHelper.hideAllMenus(null);
    },
    closePermitTypePulldown: function () {
        //set proper select item in pulldown
        applicationDetailsHelper.setLNPermitTypePulldownSelectedItem(applicationDetailsHelper.previousPermitTypeId);

        //close the menu
        applicationDetailsHelper.hideAllMenus(null);
    },
    //content header nav click handler
    navClick: function ($jqObj) {
        //get id
        var navID = $jqObj.attr("id");
        //clear current active tab
        applicationDetailsHelper.clearActiveTab();
        //make the clicked tab active
        $jqObj.addClass("active");

        //handle click event
        switch (navID) {
            case "details":
                loadDetails();
                break;
            case "complianceReporting":
                loadComplianceTab();
                break;
            case "countyReviewAndApproval":
                loadCountyReviewAndApproval();
                break;
            case "documents":
                loadDocumentsTab();
                break;
            case "comments":
                loadCommentsTab();
                break;
            case "DNRReviewApproval":
                loadDNRReviewApprovaltab();
                break;
            case "compliance":
                loadComplianceTab();
                break;
            default:
                alert("missed navID ('" + navID + "') in navClick (Details.js), check for new switch value.");
                break;
        }
        //set custom scrollbars if necessary
        //applicationDetailsHelper.handleScrollingAfterDynamicLoad();
    },
    handleScrollingAfterDynamicLoad: function () {
        //checks to see if custom scrollbar exists before applying it
        if ($(".renderbodyLeft").find(".scrollwrap").length == 0) { applicationDetailsHelper.setLeftSideCustomScrollbar(); $.jmttgCustomBrowser.setRenderbodyLeftScrollWrap100Percent(); };
        if ($(".renderbodyRight").find(".scrollwrap").length == 0) { applicationDetailsHelper.setRightSideCustomScrollbar(); $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent(); }
    },
    clearActiveTab: function () {
        //loop through all tabs
        $(".grayTabLink").each(function () {
            //does it have the active class?
            if ($(this).hasClass("active")) {
                //remove it
                $(this).removeClass("active");
            }
        });
    },
    setLRSfcrollWrapWidth: function(){
        $.jmttgCustomBrowser.setRenderbodyLeftScrollWrap100Percent();
        $.jmttgCustomBrowser.setRenderbodyRightScrollWrap100Percent();
    },
    setLRCustomScrollbars: function(){
        applicationDetailsHelper.setLeftSideCustomScrollbar();
        applicationDetailsHelper.setRightSideCustomScrollbar();
    },
    setRightSideCustomScrollbar: function () {
        $('#rightSide .scrollcontainer').scrollbars();
    },
    setLeftSideCustomScrollbar:function(){
        $('#leftSide .scrollcontainer').scrollbars();
    },
    clearNavLinkUnderline: function () {
        $(".appDetailsNavContainer").find("span").removeClass("appDetailsActiveNavLink");
    },
    setNavLinkUnderline: function ($jqObj) {
        $jqObj.addClass("appDetailsActiveNavLink");
    },
    //add stuff to the right of the home, search & map buttons
    populateDivLocalNavigation: function () {
        var $outerDivRight = $(applicationDetailsHelper.getLNOuterDiv());                       //outer div float, right
        var $centerDiv = $(applicationDetailsHelper.getLNCenterDiv());                          //centered div in middle of outerDivRight
        var $permitNumControl = $(applicationDetailsHelper.getLNPermitNumberPulldownLink());    //permit number dropdown link
        var $permitTypeControl = $(applicationDetailsHelper.getPermitTypeSpan());               //permit type
        var $projectManagerControl = $(applicationDetailsHelper.getProjectManagerSpan());       //project manager 
        var $supervisorControl = $(applicationDetailsHelper.getSupervisorSpan());               //supervisor
        var $separator = (applicationDetailsHelper.getLNVerticalSeparator());                   // "|" between items
        //add spans to center div
        $centerDiv.append($permitNumControl, $separator, $permitTypeControl, $separator, $projectManagerControl, $separator, $supervisorControl/**/);
        //add center div to outer div
        $outerDivRight.append($centerDiv);
        var $localNavDiv = $("#localNavigation");
        $localNavDiv.append($outerDivRight);
    },
    //div floated right next to search
    getLNOuterDiv: function(){
        return "<div class='localNavContainer'></div>"
    },
    //centered 
    getLNCenterDiv: function () {
        return "<div></div>";
    },
    getLNPermitNumberPulldownLink: function(){
        return "<span class='localNavigationSpanHorizontalSpacer' id='permitNumber'><span id='permitNumberSpan' class='localNavigationBold'>&nbsp;</span><span id='permitNumberArrow' title='Show History' class='localNavigationEdit' style='padding-left:5px;' onclick='applicationDetailsHelper.permitNumberPulldownClick($(this))'>(Lineage)</span></span>";
    },
    getLNVerticalSeparator:function(){
        return "<span class='localNavigationSeparator'> </span>"; //return "<span class='localNavigationSeparator'>|</span>";
    },
    getPermitTypeSpan: function () {
        return "<span class='localNavigationSpanHorizontalSpacer' id='permitType'><span class='localNavigationBold'> </span>&nbsp;<span class='localNavigationValue'></span><span id='permitTypeEditSpan' class='localNavigationEdit' title='Edit Permit Type' onclick='applicationDetailsHelper.permitTypeEditClick($(this))'></span></span>";
    },
    getProjectManagerSpan: function () {
        return " ";//"<span class='localNavigationSpanHorizontalSpacer' id='projectManager'><span class='localNavigationBold'></span>&nbsp;<span class='localNavigationValue'></span><span id='managerEditSpan' class='localNavigationEdit' title='Edit Project Manager' onclick='applicationDetailsHelper.projectManagerEditClick($(this))'></span></span>";
    },
    getSupervisorSpan: function(){
        return " ";//"<span class='localNavigationSpanHorizontalSpacer' id='supervisor'><span class='localNavigationBold'></span>&nbsp;<span class='localNavigationValue'></span><span id='supervisorEditSpan' title='Edit Supervisor' onclick='applicationDetailsHelper.supervisorEditClick($(this))' class='localNavigationEdit'></span><span id='aftersupervisorEdit'>&nbsp;</span></span>";
    },
    //edit the supervisor click
    supervisorEditClick: function ($jqObj) {
//        alert(applicationDetailsHelper.canEdit);
        var $targetMenu = $("#" + "supervisorPulldown");
        //show/Hide menu
        if (applicationDetailsHelper.isVisible($targetMenu)) {
            //need to hide menu
            applicationDetailsHelper.showHide($targetMenu, false);
        }
        else {
            //need to show menu
            applicationDetailsHelper.showHide($targetMenu, true);
        }
        //hide other menus if open - exception supervisor
        applicationDetailsHelper.hideAllMenus("supervisorPulldown");
    },
    //edit the project manager click
    projectManagerEditClick: function($jqObj){
        var $targetMenu = $("#" + "projectManagerPulldown");
        //show/Hide menu
        if (applicationDetailsHelper.isVisible($targetMenu)) {
            //need to hide menu
            applicationDetailsHelper.showHide($targetMenu, false);
        }
        else {
            //need to show menu
            applicationDetailsHelper.showHide($targetMenu, true);
        }
        //hide other menus if open
        applicationDetailsHelper.hideAllMenus("projectManagerPulldown");
    },


    //edit the permit Type click
    permitTypeEditClick: function ($jqObj) {
        var $targetMenu = $("#" + "permitTypePulldown");
        //show/Hide menu
        if (applicationDetailsHelper.isVisible($targetMenu)) {
            //need to hide menu
            applicationDetailsHelper.showHide($targetMenu, false);
        }
        else {
            //need to show menu
            applicationDetailsHelper.showHide($targetMenu, true);
        }
        //hide other menus if open
        applicationDetailsHelper.hideAllMenus("permitTypePulldown");
        
    },





    permitNumberPulldownClick: function ($jqObj) {
        //TODO:
        //populate pulldown - use attributes from $jqObj if necessary

        var $targetMenu = $("#" + "permitPulldown");
        //show/Hide menu
        if (applicationDetailsHelper.isVisible($targetMenu)) {
            //need to hide menu
            applicationDetailsHelper.showHide($targetMenu, false);
        }
        else {
            //need to show menu
            applicationDetailsHelper.showHide($targetMenu, true);
        }
        //hide other menus if open
        applicationDetailsHelper.hideAllMenus("permitPulldown");
    },

    //click event for save link on supervisor pulldown
    supervisorSaveClick: function ($jqClickObj) {
        var fullName = applicationDetailsHelper.supervisorFullName;

        var _url = Helper.GetBaseURL("Permit");

        _url += "Permit/SaveSupervisor";
        $.ajax({
            url: _url,
            type: 'Get',
            async: false,
            cache: false,
            data: { supervisorId: applicationDetailsHelper.selectedSupervisorId, permitId: applicationDetailsHelper.permitId },
            success: function (response) {
            }
        });
        //set previous ID for close click event
        applicationDetailsHelper.previousSupervisorId = applicationDetailsHelper.selectedSupervisorId;
        //set supervisor name in GUI
        applicationDetailsHelper.setSupervisor(fullName);
        //close menu
        applicationDetailsHelper.hideAllMenus(null);

    },
   

    supervisorHighlightClick: function ($jqClickObj) {
        //remove previous highlight
        applicationDetailsHelper.removeAllPulldownHighlights($jqClickObj);
        //add selected class
        $jqClickObj.addClass("LNPulldownClickableItemSelected")
        //set global variables for save function
        applicationDetailsHelper.selectedSupervisorId = $jqClickObj.attr("supervisiorId");
        applicationDetailsHelper.permitId = $jqClickObj.attr("permitId");
        applicationDetailsHelper.supervisorFullName = $jqClickObj.html()

    },
    removeAllPulldownHighlights:function($jqObject){
        $jqObject.siblings().removeClass("LNPulldownClickableItemSelected");
    },
    setPulldownScrollsTo100Width: function (targetMenu) {
        if (targetMenu == null) {
            $('.LNPulldownScrollContentContainer').each(function () {
                //get the scrollwrap
                var scrollWrap = $(this).find('.scrollwrap');
                //get target width
                var targetWidth = $(this).closest(".LNPulldown").width() - 35;
                //set to target width
                scrollWrap.width(targetWidth);
            });
        }
        else {
            //set only the target menu scroll width to 100%
            targetMenu.find('.LNPulldownScrollContentContainer').find('.scrollwrap').width(targetMenu.closest(".LNPulldown").width() - 35);
        }
    },
    //set supervisor selected highlight
    setLNSupervisorPulldownSelectedItem: function (stargetID) {
        $("#supervisorPulldown").find(".LNPulldownClickableItem").each(function () {
            sid = $(this).attr("supervisiorid");
            if ($(this).attr("supervisiorid") == stargetID) {
                //make selected
                $(this).addClass("LNPulldownClickableItemSelected");
            }
            else {
                //remove(if selected but not "saved")
                $(this).removeClass("LNPulldownClickableItemSelected");
            }
        });
    },
    //set permit type selected highlight
    setLNPermitTypePulldownSelectedItem: function (stargetID) {
        $("#permitTypePulldown").find(".LNPulldownClickableItem").each(function () {
            sid = $(this).attr("permitTypeId");
            if ($(this).attr("permitTypeId") == stargetID) {
                //make selected
                $(this).addClass("LNPulldownClickableItemSelected");
            }
            else {
                //remove(if selected but not "saved")
                $(this).removeClass("LNPulldownClickableItemSelected");
            }
        });
    },
    setLNPulldownByPulldown: function ($pulldown, selectedID) {
        $pulldown.find(".LNPulldownClickableItem").each(function () {
            if ($(this).html() == selectedID) {
                //make selected
                $(this).addClass("LNPulldownClickableItemSelected");
            }
            else {
                //remove(if selected but not "saved")
                $(this).removeClass("LNPulldownClickableItemSelected");
            }
        });
    },
    //click event for save link on supervisor pulldown
    managerSaveClick: function ($jqClickObj) {
        var fullName = applicationDetailsHelper.managerFullName;

        var _url = Helper.GetBaseURL("Permit");
        
        _url += "Permit/SaveProjectManager";
        $.ajax({
            url: _url,
            type: 'Get',
            async: false,
            cache: false,
            data: { managerId: applicationDetailsHelper.selectedManagerId, permitId: applicationDetailsHelper.permitId },
            success: function (response) {
            }
        });
        //set previous ID for close click event
        applicationDetailsHelper.previousManagerId = applicationDetailsHelper.selectedManagerId;
        //set man name in GUI
        applicationDetailsHelper.setProjectManager(fullName);
        //close menu
        applicationDetailsHelper.hideAllMenus(null);

    },



    //click event for save link on permit type pulldown
    permitTypeSaveClick: function ($jqClickObj) {
        //var fullName = applicationDetailsHelper.managerFullName;

        var _url = Helper.GetBaseURL("Permit");

        _url += "Permit/SavePermitType";
        $.ajax({
            url: _url,
            type: 'Post',
            async: false,
            cache: false,
            data: { permitTypeId: applicationDetailsHelper.selectedPermitTypeId, permitId: applicationDetailsHelper.permitId },
            success: function (response) {
            }
        });
        //set previous ID for close click event
        applicationDetailsHelper.previousPermitTypeId = applicationDetailsHelper.selectedPermitTypeId;
        //set man name in GUI
        applicationDetailsHelper.setPermitType(applicationDetailsHelper.permitTypeKey)
        //close menu
        applicationDetailsHelper.hideAllMenus(null);

        window.location.reload();

    },




    managerHighlightClick: function ($jqClickObj) {
        //remove previous highlight
        applicationDetailsHelper.removeAllPulldownHighlights($jqClickObj);
        //add selected class
        $jqClickObj.addClass("LNPulldownClickableItemSelected");
        applicationDetailsHelper.selectedManagerId = $jqClickObj.attr("managerId");
        applicationDetailsHelper.permitId = $jqClickObj.attr("permitId");
        applicationDetailsHelper.managerFullName = $jqClickObj.html()

    },

    permitTypeHighlightClick: function ($jqClickObj) {
        //remove previous highlight
        applicationDetailsHelper.removeAllPulldownHighlights($jqClickObj);
        //add selected class
        $jqClickObj.addClass("LNPulldownClickableItemSelected");
        applicationDetailsHelper.selectedPermitTypeId = $jqClickObj.attr("permitTypeId");
        applicationDetailsHelper.permitId = $jqClickObj.attr("permitId");
        applicationDetailsHelper.permitTypeKey = $jqClickObj.html()

    },

    showHide: function ($jqObj, show) {
        if (show) {
            //hide
            $jqObj.slideDown(600, "easeOutQuad");
            return "showing";        }
        else {
            $jqObj.slideUp(400, "easeOutQuad");
        }
    },
    isVisible: function ($jqObj) {
        return $jqObj.is(":visible");
    },
    permitisVisible:function(){
        return $("#permitPulldown").is(":visible");
    },
    //set the project manager text value 
    setProjectManager: function (inputString) {
        //convert if empty string
        inputString = applicationDetailsHelper.handleBlankManagerSupervisor(inputString);
        //if first load selected automatically
        //if (applicationDetailsHelper.isInitalLoad()) { applicationDetailsHelper.selectedManagerId = inputString; }
        //set html
        $("#projectManager").find(".localNavigationValue").html(inputString);
        //make sure correct item is selected in the pulldown
        applicationDetailsHelper.setLNmanagerPulldownSelectedItem(applicationDetailsHelper.selectedManagerId);
        //reposition (name changes location slightly)
        applicationDetailsHelper.handlePulldownPositions();
    },
    isInitalLoad: function(){
        var rtnValue = false;
        if(applicationDetailsHelper.selectedManagerId==0 && applicationDetailsHelper.selectedSupervisorId==0){
            rtnValue = true;
        }
        return rtnValue;
    },
    //set the supervisor value
    setSupervisor: function (inputString) {
        //convert if empty string
        inputString = applicationDetailsHelper.handleBlankManagerSupervisor(inputString);
        //set html
        $("#supervisor").find(".localNavigationValue").html(inputString);
        //make sure correct item is selected in the pulldown
        applicationDetailsHelper.setLNSupervisorPulldownSelectedItem(applicationDetailsHelper.selectedSupervisorId);
        //reposition (name changes location slightly)
        applicationDetailsHelper.handlePulldownPositions();
    },
    //set manager selected highlight
    setLNmanagerPulldownSelectedItem: function (mtargetID) {
        $("#projectManagerPulldown").find(".LNPulldownClickableItem").each(function () {
            mid = $(this).attr("managerid");
            if ($(this).attr("managerid") == mtargetID) {
                //make selected
                $(this).addClass("LNPulldownClickableItemSelected");
            }
            else {
                //remove(if selected but not "saved")
                $(this).removeClass("LNPulldownClickableItemSelected");
            }
        });
    },
    handleBlankManagerSupervisor: function(inputString){
        if (inputString == "") {
            return "";//return "N/A";
        }
        else {
            return inputString;
        }
    },
    getSelectedProjectManager:function (){
        return $("#projectManager").find(".localNavigationValue").html();
    },
    getSelectedSupervisor: function () {
        return $("#supervisor").find(".localNavigationValue").html();
    },
    //set permit number
    setPermitNumber: function(inputString){
        $("#permitNumberSpan").html(inputString);
        //reposition menus
        applicationDetailsHelper.handlePulldownPositions();

    },
    //set permit type number
    setPermitType: function (inputString) {
        //convert if empty string
        inputString = applicationDetailsHelper.handleBlankManagerSupervisor(inputString);
        //set html
        $("#permitType").find(".localNavigationValue").html(inputString);
        //make sure correct item is selected in the pulldown
        applicationDetailsHelper.setLNPermitTypePulldownSelectedItem(applicationDetailsHelper.selectedPermitTypeId);
        //reposition (name changes location slightly)
        applicationDetailsHelper.handlePulldownPositions();

    },
    //sets the correct location for the permit pulldown menu
    positionPermitPulldown: function () {
        applicationDetailsHelper.positionPulldownBySep("permitPulldown", "permitNumber");
    },
    //sets the correct location for the permit type pulldown menu
    positionPermitTypePulldown: function () {
        applicationDetailsHelper.positionPulldownBySep("permitTypePulldown", "permitType");
    },
    //sets the correct location for the project manager pulldown menu
    positionProjectManagerPulldown: function () {
        applicationDetailsHelper.positionPulldownBySep("projectManagerPulldown", "projectManager");
    },
    positionSupervisorMenu: function(){
        $menu = $("#supervisorPulldown");
        //no sep on the item
        $tarSpan = $("#aftersupervisorEdit");
        //move the menu
        applicationDetailsHelper.moveMenu($tarSpan, $menu);
    },
    moveMenu: function (jsOffsetObj, jqMenuObj) {
        //get sep span position
        var $offset = jsOffsetObj.offset();

        if (typeof ($offset) === 'undefined') return;
        
        //get the left location
        var offLeft = $offset.left + 2;  //2 for pulldown border
        //get menu width
        var menuWidth = jqMenuObj.outerWidth(true);
        //place menu
        jqMenuObj.css("left", eval(offLeft - menuWidth)); 
    },
    //generic position based on menu separator
    positionPulldownBySep: function(menuID, spanID){
        //get pulldown
        var $menu = $("#" + menuID);
        //get pulldown span 
        var $pmSpan = $("#" + spanID);
        //get separator after permit span
        var $nextSep = $pmSpan.next(".localNavigationSeparator");
        //move menu
        applicationDetailsHelper.moveMenu($nextSep, $menu);
    },
    //renderbodyLEFT
    rBLHeaderClick: function (jqObj) {
        //get (1st) arrow
        $arrow = $(jqObj).find(".renderBLArrow").first();
        //get content div
        $content = jqObj.next("div");
        if (applicationDetailsHelper.isVisible($content)) {
            //hide
            applicationDetailsHelper.showHide($content, false);
            //set arrow dir
            applicationDetailsHelper.setArrowDirection($arrow, false);
            //set title
            jqObj.attr("title", "Show content");
        }
        else {
            //show content
            applicationDetailsHelper.showHide($content, true);
            //set arrow dir
            applicationDetailsHelper.setArrowDirection($arrow, true);
            //set title
            jqObj.attr("title", "Hide content");
        }
    },
    setArrowDirection:function(jqObj,up){
        if(up){
            jqObj.html("&#9650");
        }
        else{
            jqObj.html("&#9660");
        }
    },
    handleSeparatorClick: function ($jqObj) {
        //get arrow
        var $arrowSpan = $jqObj.find(".renderBLArrow");
        //get content
        var $contentDiv = $jqObj.next("div");
        //handle arrow & title
        if ($contentDiv.is(":visible")) {
            $arrowSpan.html("&#x25BC;");
            $jqObj.attr("title", "Open");
        }
        else {
            $arrowSpan.html("&#x25B2;");
            $jqObj.attr("title", "Close");
        }
        $contentDiv.slideToggle();
    },
    setScrollWrapWidth: function () {
            $('#rightSide').find('.scrollwrap').width(eval($('#rightSide').width() - 50));
    }

};