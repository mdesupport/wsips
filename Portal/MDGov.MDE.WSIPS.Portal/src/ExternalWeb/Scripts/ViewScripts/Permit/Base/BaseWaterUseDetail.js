﻿var WaterUseDetail = {

    Initialize: function () {

        //Collect selected water use category and type
        var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
        var commaSeparatedValues = _.reduce(selectedValues, function (memo, num) { return memo + num + ','; }, '');

        if (selectedValues.length > 0) {

            $(".commaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');

            global.currentStep = Steps.WaterUseDetail;
            $('#CurrentStep').val('7');
            $('#SaveButton').show();

            //while (!global.dataSaved) {
            //}

            if (this.SetUpPage(selectedValues)) {
                $('.waterUseDetails').addClass('wizardStepActive');
                $('.waterUseDetails').show(); // show the selected section on the left pannel
                $('#waterUseDetails').show(); // show the water use detail page
            }
            else {
                $('.waterUseDetails').hide();
                ApplicationWizard.IdentifyLocation.Initialize();
            }
        }
        else {
            // Redirect to next page (view)
            this.Next();
        }
    },

    ValidateData: function () {
        var totalUsePercentage = 0;
        $('[id$=UsePercentage]:visible').each(function () {
            if ($(this).val()) {
                totalUsePercentage += parseInt($(this).val(), 10);
            }
        });

        if (totalUsePercentage != 100) {
            $('#usePercentValidation').show();
            return false;
        }

        return $('#wizardForm').valid();
    },

    SetUpPage: function (selectedValues) {
        global.showWaterUseDetail = false;
        var _pageBlocked = false;

        if (_.contains(selectedValues, '2')) {
            $('#AddPoultryWateringEvaporativeParent').show();
            global.showWaterUseDetail = true;
            if ($.trim($('#_AddPoultryWateringEvaporativeParent').html()) == "") {
                $('#AddPoultryLink').click();
            }
        }
        else {
            $('#AddPoultryWateringEvaporativeParent').hide();
            $('#AddPoultryWateringEvaporativeParent .itemContainer').detach();
        }

        if (_.contains(selectedValues, '3')) {
            $('#AddPoultryWateringFoggerParent').show();
            global.showWaterUseDetail = true;
            if ($.trim($('#_AddPoultryWateringFoggerParent').html()).length == 0) {
                $('#AddPoultryFoggerLink').click();
            }
        }
        else {
            $('#AddPoultryWateringFoggerParent').hide();
            $('#AddPoultryWateringFoggerParent .itemContainer').detach();
        }

        if (_.contains(selectedValues, '4')) {
            $('#AddDairyAnimalWateingParent').show();
            global.showWaterUseDetail = true;
            if ($.trim($('#_AddDairyAnimalWateingParent').html()).length == 0) {
                $('#addDairyAnimalLink').click();
            }
        }
        else {
            $('#AddDairyAnimalWateingParent').hide();
            $('#AddDairyAnimalWateingParent .itemContainer').detach();
        }

        if (_.contains(selectedValues, '5')) {
            $('#AddOtherLivestockWateringParent').show();
            global.showWaterUseDetail = true;
            if ($.trim($('#_AddOtherLivestockWateringParent').html()).length == 0) {
                $('#addLivestockLink').click();
            }
        }
        else {
            $('#AddOtherLivestockWateringParent').hide();
            $('#AddOtherLivestockWateringParent .itemContainer').detach();
        }

        if (_.contains(selectedValues, '6')) {
            $('#FarmPotableSupplies').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#FarmPotableSupplies').hide();
        }

        if (_.contains(selectedValues, '7')) {
            $('#AddCropParent').show(); global.showWaterUseDetail = true;
            if ($.trim($('#_AddCropParent').html()).length == 0) {
                $('#addCropLink').click();
            }
        }
        else {
            $('#AddCropParent').hide();
            $('#AddCropParent .itemContainer').detach();
        }

        if (_.contains(selectedValues, '8')) {
            $('#AddNurseryStockParent').show();
            global.showWaterUseDetail = true;
            if ($.trim($('#_AddNurseryStockParent').html()).length == 0) {
                $('#NursaryAddLink').click();
            }
        }
        else {
            $('#AddNurseryStockParent').hide();
            $('#AddNurseryStockParent .itemContainer').detach();
        }

        if (_.contains(selectedValues, '9')) {
            $('#AddSodParent').show();
            global.showWaterUseDetail = true;
            if ($.trim($('#_AddSodParent').html()).length == 0) {
                $('#SodAddLink').click();
            }
        }
        else {
            $('#AddSodParent').hide(); $('#AddSodParent .itemContainer').detach();
        }

        if (_.contains(selectedValues, '10')) {
            $('#FoodProcessing').show(); global.showWaterUseDetail = true;
        }
        else {
            $('#FoodProcessing').hide();
        }

        if (_.contains(selectedValues, '11')) {
            $('#AquacultureAndAquarium').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#AquacultureAndAquarium').hide();
        }

        if (_.contains(selectedValues, '12')) {
            $('#IrrigationUndefined').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#IrrigationUndefined').hide();
        }

        if (_.contains(selectedValues, '13')) {
            $('#GolfCourseIrrigationForm').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#GolfCourseIrrigationForm').hide();
        }

        if (_.contains(selectedValues, '14')) {
            $('#LawnAndParkIrrigationForm').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#LawnAndParkIrrigationForm').hide();
        }

        if (_.contains(selectedValues, '15')) {
            $('#SmallIntermitentIrrigationForm').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#SmallIntermitentIrrigationForm').hide();
        }

        if (_.contains(selectedValues, '40')) {
            $('#CommercialDrinkingOrSanitary').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#CommercialDrinkingOrSanitary').hide();
        }

        if (_.contains(selectedValues, '42')) {
            $('#EducationalDrinkingOrSanitary').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#EducationalDrinkingOrSanitary').hide();
        }

        if (_.contains(selectedValues, '44')) {
            $('#ReligiousDrinkingOrSanitary').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#ReligiousDrinkingOrSanitary').hide();
        }

        if (_.contains(selectedValues, '57')) {
            $('#WildlifePonds').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#WildlifePonds').hide();
        }

        if (_.contains(selectedValues, '61')) {
            $('#ConstructionDewatering').show();
            global.showWaterUseDetail = true;
        }
        else {
            $('#ConstructionDewatering').hide();
        }

        var isWaterSupply = false;

        //Apply customer check boxes 
        $('input[type=checkbox]').checkbox();

        if (_.any(selectedValues, function (value) { return parseInt(value) > 11; })) {

            // If there is any changes to the water use category and type selection, then reload the wastewater treatment and disposal page 
            if (global.ReloadWasteWaterTreatmentAndDisposalPage) {

                //$.blockUI({ message: '<h4>Please wait, page loading...</h4>' });

                var _filteredValues = _.filter(selectedValues, function (value) { return value > 11; });

                var _url = Helper.GetBaseURL("Permit");

                _url += "WaterUseDetail/AddWastewaterTreatmentDisposal";

                $.ajax({
                    url: _url,
                    type: 'Get',
                    data: { waterUseCategoryTypeids: _filteredValues.join(","), permitId: $('#PermitId').val() },
                    cache: false,
                    async: false,
                    success: function (response) {
                        $('#WastewaterTreatmentAndDisposal').html(response);

                        $('input[type=checkbox]').checkbox();

                        $("input[id$='IsWastewaterDischarge']").change(function (e) {
                            WastewaterTreatmentAndDisposal.SetupView($(this));
                        });

                        $("select[id$='WaterDispersementTypeId']").change(function () {
                            if ($(this).val() == '4') {
                                $(this).parent().parent().find('.natureOfGroundwaterDischarge').show();
                            }
                            else {
                                $(this).parent().parent().find('.natureOfGroundwaterDischarge').hide();
                            }
                        });

                        // bind change events 
                        WaterUseDetail.BindChangeEvent();

                        if (_.any(selectedValues, function (value) { return parseInt(value) > 15; })) {
                        }
                        else {
                            //unblock the page 
                            //$.unblockUI();
                        }
                    }
                });
            }

            $('.wastewaterTreatmentDisposal').show();
        }
        else {
            $('.wastewaterTreatmentDisposal').hide();
        }

        if (_.any(selectedValues, function (value) { return parseInt(value) > 15; })) {

            if (_.any(selectedValues, function (value) { if (parseInt(value) == 37 || parseInt(value) == 38 || parseInt(value) == 39 || parseInt(value) == 54) return true; })) {
                isWaterSupply = true;
            }

            // If there is any changes to the water use category and type selection, then reload the wastewater treatment and disposal page 
            if (global.ReloadPrivateWaterSupplierView) {
                var _filteredValues = _.filter(selectedValues, function (value) { return value > 15; });

                var _url = Helper.GetBaseURL("Permit");

                _url += "WaterUseDetail/AddPrivateWaterSupply";

                $.ajax({
                    url: _url,
                    type: 'Get',
                    data: { waterUseCategoryTypeids: _filteredValues.join(","), permitId: $('#PermitId').val() },
                    cache: false,
                    async: false,
                    success: function (response) {
                        $('#PrivateWaterSuppiler').html(response);
                        //if (isWaterSupply) {
                        //    $("#PrivateWaterSuppiler").parent().find(".waterSupplyInfoOptional").show();
                        //}
                        //else {
                        //    $("#PrivateWaterSuppiler").parent().find(".waterSupplyInfoOptional").hide();
                        //}

                        //Apply customer check boxes 
                        $('input[type=checkbox]').checkbox();

                        WaterUseDetail.BindChangeEvent();

                        $(".commaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');
                    }
                });
            }

            $('#PrivateWaterSuppiler').show();

            if (isWaterSupply) {
                $("#PrivateWaterSuppiler").parent().find(".waterSupplyInfoOptional").show();
            }
            else {
                $("#PrivateWaterSuppiler").parent().find(".waterSupplyInfoOptional").hide();
            }

            global.showWaterUseDetail = true;

        } else {
            $('#PrivateWaterSuppiler').hide('');
        }

        // Reset global variable 
        global.ReloadPrivateWaterSupplierView = false;
        global.ReloadWasteWaterTreatmentAndDisposalPage = false;


        return global.showWaterUseDetail;
    },

    Add: function ($this) {
        var controller = $this.attr("data-controller");
        var action = $this.attr("data-action");

        var _url = Helper.GetBaseURL("Permit");
        var index = $('#' + action + 'Parent .itemContainer').length;

        _url += controller + '/' + action;

        $.ajax({
            url: _url,
            type: 'Get',
            async: false,
            data: { index: index },
            success: function (response) {
                if (!_.isNull(response)) {
                    $(response).hide().appendTo('#_' + action + 'Parent').slideDown("slow");
                    $("input[type=button]").button();

                    $('.wastewaterSelect').change(function () {
                        var id = $(this).attr('id');
                        var parent = $('#' + id).parent().parent();

                        if ($('#' + id + ' option:selected').text() == 'Other') {
                            parent.children("#otherDescripption").show();
                        }
                        else {
                            parent.children("#otherDescripption").hide();
                        }
                    });

                    $(".commaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');
                }
            }
        });
    },

    Delete: function (link) {

        var $container = $(link).closest(".itemContainer");
        $container.find('input:hidden:first').val('True');
        $container.slideUp("slow");
    },

    LoadwaterUseDetailView: function (permitId) {

        if ($.trim($('#waterUseDetails').html()).length == 0) {
            var _url = Helper.GetBaseURL("Permit");

            _url += "WaterUseDetail/LoadWaterUseDetails/";

            $.ajax({
                url: _url,
                type: 'Get',
                data: { permitId: permitId },
                cache: false,
                success: function (response) {
                    //Loading the application wizard finished
                    $.unblockUI();
                    $('#waterUseDetails').append(response);
                    //Apply customer check boxes 
                    $('input[type=checkbox]').checkbox();
                    WaterUseDetail.BindChangeEvent();
                }
            });

            return false;
        }
    },

    BindChangeEvent: function () {
        $(document).on('change', "input[id$='AverageGallonPerDayFromGroundWater']", function () {
            var _input1 = $(this).parent().parent().find("input[id$='AverageGallonPerDayFromSurfaceWater']");
            var _input2 = $(this).parent().parent().find("input[id$='MaximumGallonPerDayFromSurfaceWater']");

            if ($(this).val() != 0 || $(this).val() != "") {
                if (_input1.val() == "" || _.isUndefined(_input1.val())) {
                    _input1.val(0)
                }

                if (_input2.val() == "" || _.isUndefined(_input2.val())) {
                    _input2.val(0)
                }
            }
            else {
                if ((_input1.val() != 0 && _input1.val() != "" && !_.isUndefined(_input1.val())) || (_input2.val() != 0 && _input2.val() != "" && !_.isUndefined(_input2.val()))) {
                    $(this).val(0)
                }
            }

            $(this).closest('.divVerticalBottomSpacerDouble').find(".delayedCommaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');
        });

        $(document).on('change', "input[id$='MaximumGallonPerDayFromGroundWater']", function () {
            var _input1 = $(this).parent().parent().find("input[id$='AverageGallonPerDayFromSurfaceWater']");
            var _input2 = $(this).parent().parent().find("input[id$='MaximumGallonPerDayFromSurfaceWater']");

            if ($(this).val() != 0 || $(this).val() != "") {
                if (_input1.val() == "" || _.isUndefined(_input1.val())) {
                    _input1.val(0)
                }

                if (_input2.val() == "" || _.isUndefined(_input2.val())) {
                    _input2.val(0)
                }
            }
            else {
                if ((_input1.val() != 0 && _input1.val() != "" && !_.isUndefined(_input1.val())) || (_input2.val() != 0 && _input2.val() != "" && !_.isUndefined(_input2.val()))) {
                    $(this).val(0)
                }
            }

            $(this).closest('.divVerticalBottomSpacerDouble').find(".delayedCommaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');

        });

        $(document).on('change', "input[id$='AverageGallonPerDayFromSurfaceWater']", function () {
            var _input1 = $(this).parent().parent().find("input[id$='AverageGallonPerDayFromGroundWater']");
            var _input2 = $(this).parent().parent().find("input[id$='MaximumGallonPerDayFromGroundWater']");


            if ($(this).val() != 0 || $(this).val() != "") {
                if (_input1.val() == "" || _.isUndefined(_input1.val())) {
                    _input1.val(0)
                }

                if (_input2.val() == "" || _.isUndefined(_input2.val())) {
                    _input2.val(0)
                }
            }
            else {
                if ((_input1.val() != 0 && _input1.val() != "" && !_.isUndefined(_input1.val())) || (_input2.val() != 0 && _input2.val() != "" && !_.isUndefined(_input2.val()))) {
                    $(this).val(0)
                }
            }

            $(this).closest('.divVerticalBottomSpacerDouble').find(".delayedCommaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');

        });

        $(document).on('change', "input[id$='MaximumGallonPerDayFromSurfaceWater']", function () {
            var _input1 = $(this).parent().parent().find("input[id$='AverageGallonPerDayFromGroundWater']");
            var _input2 = $(this).parent().parent().find("input[id$='MaximumGallonPerDayFromGroundWater']");


            if ($(this).val() != 0 || $(this).val() != "") {
                if (_input1.val() == "" || _.isUndefined(_input1.val())) {
                    _input1.val(0)
                }

                if (_input2.val() == "" || _.isUndefined(_input2.val())) {
                    _input2.val(0)
                }
            }
            else {
                if ((_input1.val() != 0 && _input1.val() != "" && !_.isUndefined(_input1.val())) || (_input2.val() != 0 && _input2.val() != "" && !_.isUndefined(_input2.val()))) {
                    $(this).val(0)
                }
            }

            $(this).closest('.divVerticalBottomSpacerDouble').find(".delayedCommaInserted").maskMoney({ precision: 0, defaultZero: false, allowZero: true }).maskMoney('mask');

        });
    }
};