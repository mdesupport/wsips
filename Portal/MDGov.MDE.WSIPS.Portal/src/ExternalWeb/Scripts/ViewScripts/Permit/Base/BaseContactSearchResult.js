﻿var ContactSearchResult = {
    Initialize: function (recordCount) {
        global.currentStep = Steps.ContactSearchResult;

        $('#BackButton').show();

        // Create a proper title based on who is being searched currently.
        ContactSearchResult.FormatSearchResultTitle(recordCount);

        if (recordCount > 0) {
            $("#contactSearchResultContainer").show();
            ContactSearchResult.Show($('#contactSearchResult'), 600, 850, "Search result");
        } else {
            $("#contactSearchResultContainer").hide();
            ContactSearchResult.Show($('#contactSearchResult'), 180, 550, "Search result");
        }
    },

    DisplayContact: function (contactId) {
        $('#contactSearchResult').dialog('close');

        //Contact Found
        if (global.CurrentlySearching == "WaterUser") {
            global.waterUserContactFound = true;
        }
        else if (global.CurrentlySearching == "LandOwner") {
            global.LandOwnerContactFound = true;
        }

        //Populate data
        if (!_.isUndefined(contactId)) {
            var contact = _.find(global.SearchResult, function (contact) { return contact.Id == contactId });
            if (contact != null) {
                if (global.CurrentlySearching == "WaterUser") {
                    ContactSearchResult.PopulateContactValues(ContactType.WaterUser, contact);
                }
                else if (global.CurrentlySearching == "LandOwner") {
                    ContactSearchResult.PopulateContactValues(ContactType.LandOwner, contact);
                }
            }
        }
    },

    UserNotFound: function () {
        $('#contactSearchResult').dialog('close');
        ContactSearchResult.DisplayBlankContact();
    },

    DisplayBlankContact: function () {
        if ($('#contactSearchResult').is(":data(dialog)")) {
            $('#contactSearchResult').dialog('close');
        }

        //Contact not found , but at this point it is expected to be created. so it will be considered as if it is found.
        if (global.CurrentlySearching == "WaterUser") {
            global.waterUserContactFound = true;
            ContactSearchResult.PopulateContactValues(1, null);
        }
        else if (global.CurrentlySearching == "LandOwner") {
            global.LandOwnerContactFound = true;
            ContactSearchResult.PopulateContactValues(2, null);
        }
    },

    AdjustContactInformationLayout: function (isBusiness) {
        var showPermitteeName = false;
        var $contactContainer = null;
        if (global.CurrentlySearching == "WaterUser") {
            $contactContainer = $('#WaterUser');

            if (global.permittee == "2" || global.permittee == "4" || global.permittee == "0") {
                showPermitteeName = true;
            }
        }
        else if (global.CurrentlySearching == "LandOwner") {
            $contactContainer = $('#LandOwner');

            if (global.permittee == "3") {
                showPermitteeName = true;
            }
        }

        if (isBusiness == "True") {
            if (showPermitteeName) {
                $('.permitteeTitle').text($contactContainer.find('[id$=BusinessName]').val());
                $('.permitteeTitleContainer').show();
            }
            else {
                $('.permitteeTitleContainer').hide();
            }

            var $fmlNameContainer = $contactContainer.find("#FmlNameContainer").detach();
            var $secondaryNameContainer = $contactContainer.find("#SecondaryNameContainer").detach();

            if ($fmlNameContainer.length) {
                $contactContainer.find('#BusinessContactContainer').append($fmlNameContainer).show();

                $secondaryNameContainer.insertAfter($contactContainer.find("[id$=BusinessName]").closest('.divVerticalBottomSpacer'));
            }
            else {
                $contactContainer.find("#BusinessContactContainer").show();
            }

            $('#businessNameAsterisks').show();
        }
        else {
            if (showPermitteeName) {
                $('.permitteeTitle').text($contactContainer.find('[id$=FirstName]').val() + " " + $contactContainer.find('[id$=LastName]').val());
                $('.permitteeTitleContainer').show();
            }
            else {
                $('.permitteeTitleContainer').hide();
            }

            var $fmlNameContainer = $contactContainer.find("#FmlNameContainer").detach();
            var $secondaryNameContainer = $contactContainer.find("#SecondaryNameContainer").detach();

            if ($fmlNameContainer.length) {
                $contactContainer.find('#NameInformationContainer').append($fmlNameContainer).append($secondaryNameContainer);
                $contactContainer.find("#BusinessContactContainer").hide();
            }
            else {
                $contactContainer.find("#BusinessContactContainer").hide();
            }

            $('#businessNameAsterisks').hide();
        }
    },

    ValidateData: function () {
        //Reset validation error
        ContactSearchResult.ClearValidationError();

        //validate the form and if there is validation error don't pass to next step
        return $('#wizardForm').valid();
    },

    FormatSearchResultTitle: function (recordCount) {
        var title = "<div>";
        var searchCritrea = "";
        var searchFor = "";

        var _firstName = $('#SearchFirstName').val();
        var _lastName = $('#SearchLastName').val();
        var _businessName = $('#SearchBusinessName').val();
        var _emailAddress = $('#SearchEmailAddress').val();

        // identify the search critrea 
        if (!_.isUndefined(_firstName) && _firstName != "") {
            searchCritrea = 'First name: <b>"' + _firstName + '"</b>';
        }

        if (!_.isUndefined(_lastName) && _lastName != "") {

            if (searchCritrea.length > 0) {
                searchCritrea += ' and Last name: <b>"' + _lastName + '"</b>';
            }
            else {
                searchCritrea = 'Last name: <b>"' + _lastName + '"</b>';
            }
        }

        if (!_.isUndefined(_businessName) && _businessName != "") {

            if (searchCritrea.length > 0) {
                searchCritrea += ' or Business name: <b>"' + _businessName + '"</b>';
            }
            else {
                searchCritrea = '<b>Business name: <b>"' + _businessName + '"</b>';
            }
        }
        if (!_.isUndefined(_emailAddress) && _emailAddress != "") {

            if (searchCritrea.length > 0) {
                searchCritrea += '" or Email address: <b>"' + _emailAddress + '"</b>';
            }
            else {
                searchCritrea = 'Email address: <b>"' + _emailAddress + '"</b>';
            }
        }

        // Build search title
        if (recordCount == 0) {
            title += '<div class="divVerticalTopSpacer">You searched for ' + searchCritrea + '    <a href="#" onclick="javascript:ContactSearch.Modify();" style="color:blue;">Modify Search</a></div>';
            title += '<div style="padding:20px 0px 0px 20px;"><b>Your search returned no results</b></div>';
        }
        else {
            title += '<div style="font-weight:bold;">' + searchFor + ' Contact Information </div>';
            title += '<div class="divVerticalTopSpacer">You searched for ' + searchCritrea + '    <a href="#" onclick="javascript:ContactSearch.Modify();" style="color:blue;">Modify Search</a></div>';
            title += '<div style="padding:20px 0px 0px 0px;"> Select a ' + searchFor.toLowerCase() + ' from the list or create a new contact if the ' + searchFor.toLowerCase() + ' is not in the search result</div>';
        }

        title += "</div>";

        $('#searchTitle').html(title);
    },

    PopulateContactValues: function (contactTypeId, contact) {
        //Reset validation error 
        ContactSearchResult.ClearValidationError();

        //Reset selected step
        ApplicationWizard.ClearSelectedWizardStep();

        //show contact information
        $('#SearchedContacts').show();

        $('#BackButton').show();
        $('#SeachViewBackButton').hide();
        $('#NextButton').show();

        ContactSearchResult.HideSearchedContactView();

        var $contactContainer = null;
        if (contactTypeId == 1) {
            $contactContainer = $('#WaterUser').show();
            ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
        }
        else if (contactTypeId == 2) {
            $contactContainer = $('#LandOwner').show();
            ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
        }
        else if (contactTypeId == 3) {
            $contactContainer = $('#Consultant').show();
            ApplicationWizard.HighlightCurrentWizardStep($('.Consultant'));
        }

        if (contact != null) {
            $contactContainer.find('[id$=Id]').val(contact.Id);
            $contactContainer.find('[id$=UserId]').val(contact.UserId);
            $contactContainer.find('[id$=FirstName]').val(contact.FirstName);
            $contactContainer.find('[id$=MiddleInitial]').val(contact.MiddleInitial);
            $contactContainer.find('[id$=LastName]').val(contact.LastName);
            $contactContainer.find('[id$=BusinessName]').val(contact.BusinessName);
            /*$contactContainer.find('[id$=SecondaryName]').val(contact.SecondaryName);*/
            $contactContainer.find('[id$=Address1]').val(contact.Address1);
            $contactContainer.find('[id$=Address2]').val(contact.Address2);
            $contactContainer.find('[id$=City]').val(contact.City);
            $contactContainer.find('[id$=StateId]').val(contact.StateId).change();
            $contactContainer.find('[id$=ZipCode]').val(contact.ZipCode);
            $contactContainer.find('[id$=PrimaryTelephoneNumber_CommunicationValue]').val(contact.PrimaryTelephoneNumber.CommunicationValue);
            $contactContainer.find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val(contact.PrimaryTelephoneNumber.SelectedCommunicationMethodId).change();
            $contactContainer.find('[id$=AltTelephoneNumber_CommunicationValue]').val(contact.AltTelephoneNumber.CommunicationValue);
            $contactContainer.find('[id$=AltTelephoneNumber_SelectedCommunicationMethodId]').val(contact.AltTelephoneNumber.SelectedCommunicationMethodId).change();
            $contactContainer.find('[id$=EmailAddress_CommunicationValue]').val(contact.EmailAddress.CommunicationValue);

            // if there is already Telephone number, then don't allow editing it
            if (contact.PrimaryTelephoneNumber.length) {
                $contactContainer.find('[id$=PrimaryTelephoneNumber_CommunicationValue]').unmask().prop('readonly', true);
                $contactContainer.find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').prop('disabled', true).change();
            }
            else {
                $contactContainer.find('[id$=PrimaryTelephoneNumber_CommunicationValue]').prop('readonly', false).mask('999-999-9999');
                $contactContainer.find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').prop('disabled', false).change();
            }

            if (contact.AltTelephoneNumber.length) {
                $contactContainer.find('[id$=AltTelephoneNumber_CommunicationValue]').unmask().prop('readonly', true);
                $contactContainer.find('[id$=AltTelephoneNumber_SelectedCommunicationMethodId]').prop('disabled', true).change();
            }
            else {
                $contactContainer.find('[id$=AltTelephoneNumber_CommunicationValue]').prop('readonly', false).mask('999-999-9999');
                $contactContainer.find('[id$=AltTelephoneNumber_SelectedCommunicationMethodId]').prop('disabled', false).change();
            }

            // if there is already email address, then don't allow editing it
            if (contact.EmailAddress.length) {
                $contactContainer.find('[id$=EmailAddress_CommunicationValue]').prop('readonly', true);
            }
            else {
                $contactContainer.find('[id$=EmailAddress_CommunicationValue]').prop('readonly', false);
            }

            ContactSearchResult.AdjustContactInformationLayout($contactContainer.find('[id$=IsBusiness]:checked').val());
        }
        else {
            $contactContainer.find('[id$=Id]').val('');
            $contactContainer.find('[id$=UserId]').val('');
            $contactContainer.find('[id$=FirstName]').val($("#SearchFirstName").val());
            $contactContainer.find('[id$=MiddleInitial]').val('');
            $contactContainer.find('[id$=LastName]').val($("#SearchLastName").val());
            $contactContainer.find('[id$=BusinessName]').val($("#SearchBusinessName").val());
            /*$contactContainer.find('[id$=SecondaryName]').val('');*/
            $contactContainer.find('[id$=Address1]').val('');
            $contactContainer.find('[id$=Address2]').val('');
            $contactContainer.find('[id$=City]').val('');
            $contactContainer.find('[id$=StateId]').val('').change();
            $contactContainer.find('[id$=ZipCode]').val('');
            $contactContainer.find('[id$=PrimaryTelephoneNumber_CommunicationValue]').val('').prop('readonly', false).mask('999-999-9999');
            $contactContainer.find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val('').prop('disabled', false).change();
            $contactContainer.find('[id$=AltTelephoneNumber_CommunicationValue]').val('').prop('readonly', false).mask('999-999-9999');
            $contactContainer.find('[id$=AltTelephoneNumber_SelectedCommunicationMethodId]').val('').prop('disabled', false).change();
            $contactContainer.find('[id$=EmailAddress_CommunicationValue]').val($("#SearchEmailAddress").val()).prop('readonly', false);

            // By default the contact is for an individual , not business 
            ContactSearchResult.AdjustContactInformationLayout("False");
        }
    },

    ShowStateValidationMessage: function (container) {
        $(container + " .stateValidationMessage").html('<span style="color:#E80C4D;"><ul><li>State is required</li></ul></span>').show();
    },

    ShowPrimaryPhoneNumberValidationMessage: function (container) {
        $(container + " .primaryPhoneNumberValidationMessage").html('<span style="color:#E80C4D;"><ul><li>Primary Phone Number is required</li></ul></span>').show();
    },

    ClearValidationError: function () {
        //Removes validation summary 
        $('.validation-summary-errors').addClass('validation-summary-valid');
        $('.validation-summary-errors').removeClass('validation-summary-errors');

        //Removes validation from input-fields
        $('.input-validation-error').addClass('input-validation-valid');
        $('.input-validation-error').removeClass('input-validation-error');

        //Removes validation message after input-fields
        $('.field-validation-error').addClass('field-validation-valid');
        $('.field-validation-error').removeClass('field-validation-error');
    },

    ClearSearchCriteria: function () {
        $('#SearchFirstName').val('');
        $('#SearchLastName').val('');
        $('#SearchBusinessName').val('');
        $('#SearchEmailAddress').val('');
    },

    HideSearchedContactView: function () {
        //Reset contact view 
        $('#WaterUser').hide();
        $('#LandOwner').hide();
        $('#Consultant').hide();
    },

    Show: function ($this, $height, $width, $title) {
        $this.dialog({
            title: $title,
            autoOpen: false,
            bgiframe: true,
            modal: true,
            height: $height,
            width: $width,
            draggable: false,
            resizable: false,
            open: function () {
                $('#contactSearch').dialog('close');
                window.styleDialogInputs();
            },
            close: function () {
                $(this).dialog('destroy');
            }
        });

        $this.dialog('open');
    }
};

var WaterUser = {
    Initialize: function () {
        ApplicationWizard.ClearSelectedWizardStep();
        // Check if there is a need to search for water user contact, otherwise make the existing contact avialable for editing.
        global.currentStep = Steps.ContactSearchResult;
        ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
        $('#BackButton').show();
        $('#SaveButton').hide();
        ContactSearchResult.ClearValidationError();

        if (!global.waterUserContactFound) {
            global.CurrentlySearching = "WaterUser";
            $('.searchTitle').hide();
            $('.searchTitle.waterUser').show();

            //By default show a blank contact 
            ContactSearchResult.DisplayBlankContact();

            ContactSearchDialogBox.Show($('#contactSearch'), 350, 640, "Search for Water User");
        }
        else {

            global.CurrentlySearching = "WaterUser";
            // Display water user contact
            $('#SearchedContacts').show();
            ContactSearchResult.HideSearchedContactView();
            $('#WaterUser').show();

            ContactSearchResult.AdjustContactInformationLayout($('#WaterUser').find('[id$=IsBusiness]:checked').val());
        }
    }
};

var LandOwner = {
    Initialize: function (e) {  // e tells us whether the user clicked next or back.
        ApplicationWizard.ClearSelectedWizardStep();
        ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
        $('#SaveButton').hide();
        ContactSearchResult.ClearValidationError();

        if (!global.LandOwnerContactFound) {
            $('.LandOwner').show();
            global.CurrentlySearching = "LandOwner";
            $('.searchTitle').hide();
            $('.searchTitle.landOwner').show();

            //By default show a blank contact 
            ContactSearchResult.ClearSearchCriteria();
            ContactSearchResult.DisplayBlankContact();

            ContactSearchDialogBox.Show($('#contactSearch'), 350, 640, "Search for Land Owner");
        }
        else {
            if (global.isWaterUser && global.isLandOwner) {
                $('#SearchedContacts').hide();
                $('.LandOwner').hide();
                WaterUseDescription.Initialize();
            }
            else if (!global.isWaterUser && !global.isLandOwner && global.permittee == "4") {
                $('.LandOwner').hide();

                if (e == "next") {
                    Consultant.Initialize();
                }
                else if (e == "back") {
                    WaterUser.Initialize();
                }
            }
            else {
                global.currentStep = Steps.ContactSearchResult;
                global.CurrentlySearching = "LandOwner";
                // Display land owner contact
                $('#SearchedContacts').show();
                ContactSearchResult.HideSearchedContactView();

                $('.LandOwner').show(); // Show the left pannel link 
                $('#LandOwner').show(); // show thw land owner contact
                ContactSearchResult.AdjustContactInformationLayout($('#LandOwner').find('[id$=IsBusiness]:checked').val());
            }
        }
    }
};