﻿var WaterUseCategoryAndType = {
    OptionChanged: function ($this) {
        global.WaterUseCategoryAndTypeChanged = true;

        var purposeId = $this.prop('id');

        if ($this.is(':checked')) {
            global.SelectedWaterUseCategoryTypeList[purposeId] = purposeId;
        }
        else {
            global.SelectedWaterUseCategoryTypeList[purposeId] = 0;
        }

        // Check if you need to reload the private water supply and waste water treatment
        if (purposeId > 15) {
            global.ReloadPrivateWaterSupplierView = true;
            global.ReloadWasteWaterTreatmentAndDisposalPage = true;
        }
        else if (purposeId > 11) {
            global.ReloadWasteWaterTreatmentAndDisposalPage = true;
        }
    },

    NotSureWhatTheWaterUseType: function ($this) {
        if ($this.is(':checked')) {
            $('#waterUseCategoryTypeContainer input[type=checkbox]').prop('checked', false).prop('disabled', true).change();

            global.SelectedWaterUseCategoryTypeList = new Array();
        }
        else {
            $('#waterUseCategoryTypeContainer input[type=checkbox]').prop('disabled', false).change();
        }
    },

    LoadwaterUseCatagoryAndTypeView: function () {
        if ($.trim($('#waterUseCatagoryAndType').html()).length == 0) {

            var _url = Helper.GetBaseURL("Permit");

            _url += "WaterUseDetail/LoadWaterUseCategoryAndType";

            $.ajax({
                url: _url,
                type: 'Get',
                cache: false,
                success: function (response) {
                    $('#waterUseCatagoryAndType').append(response);

                    // Check the check boxes 
                    if (global.SelectedWaterUseCategoryTypeList.length > 0) {
                        //If we are editing existing permit, collect all ids of selected purposes
                        $('#waterUseCategoryTypeContainer input[type=checkbox]').each(function () {
                            var purposeId = $(this).prop('id');

                            // the value of global.SelectedWaterUseCategoryTypeList is set on document load.
                            if (_.contains(global.SelectedWaterUseCategoryTypeList, purposeId)) {
                                $(this).prop('checked', true).change();
                            }
                        });
                    }

                    //Reset values 
                    global.WaterUseCategoryAndTypeChanged = false;
                    global.ReloadPrivateWaterSupplierView = false;
                    global.ReloadWasteWaterTreatmentAndDisposalPage = false;

                    $('input[type=checkbox]').checkbox();

                    if (WaterUseDetail.SetUpPage(global.SelectedWaterUseCategoryTypeList)) {
                        $('.waterUseDetails').show();
                    }
                }
            });

            return false;
        }
    }
};