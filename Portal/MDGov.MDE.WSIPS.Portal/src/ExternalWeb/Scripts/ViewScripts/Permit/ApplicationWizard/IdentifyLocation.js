﻿/// <reference path="../../../esri.arcgis.jsapi-3.4.min.js" />
/// <reference path="_permitReference.js" />

(function ($) {
    dojo.require("esri.map");
    dojo.require("esri.layers.FeatureLayer");
    dojo.require("esri.tasks.geometry");
    dojo.require("esri.tasks.identify");
    dojo.require("esri.tasks.locator");
    dojo.require("esri.tasks.PrintTask");
    dojo.require("esri.toolbars.draw");

    var isLoaded = false;
    var popupShown = false;

    var MapPage = function () {
        var $this = $('#identifyLocation'),
            gisServicesHost = $('#GISServicesHost').val(),
            gisServicesDirectory = $('#GISServicesDirectory').val();

        this.init = function () {
            global.currentStep = Steps.IdentifyLocation;
            $('#CurrentStep').val('9');
            $('.IdentifyLocation').addClass('wizardStepActive');
            $('#SaveButton').show();
            $this.css('z-index', '');

            //if (!isLoaded) {
            // Load Map
            dojo.addOnLoad(function () {
                $this.find('#search').unbind('keypress');
                $this.find('#search').keypress(function (e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13) { //Enter keycode
                        $this.find('#searchbutton').click();
                    }
                });

                $this.find('#searchbutton').unbind('click');
                $this.find('#searchbutton').click(function (e) {
                    if ($this.find('#search').val() != '' && $this.find('#search').val() != $this.find('#search').attr('title')) {
                        if ($this.find("#searchtype").val() == "1") {
                            //Address search
                            LocateAddress();
                        }
                        else if ($this.find("#searchtype").val() == "2") {
                            //Lat/Long search
                            ZoomToLatLong();
                        }
                        else if ($this.find("#searchtype").val() == "3") {
                            //Parcel search
                            ZoomToParcel();
                        }
                    }
                });

                $this.find('#searchtype').unbind('change');
                $this.find("#searchtype").change(function () {
                    if ($this.find('#search').val() == $this.find('#search').attr('title')) {
                        $this.find('#search').val('');
                    }

                    if ($(this).val() == "1") {
                        $this.find('#search').attr('title', 'e.g. 2345 Glen Arm Rd. 21234').labelify({ labelledClass: "labelHighlight" });
                    }
                    else if ($(this).val() == "2") {
                        $this.find('#search').attr('title', 'e.g. 39.27138, -76.131995').labelify({ labelledClass: "labelHighlight" });
                    }
                    else if ($(this).val() == "3") {
                        $this.find('#search').attr('title', 'County Code,Tax Map,Grid,Parcel,Lot').labelify({ labelledClass: "labelHighlight" });
                    }
                });

                $this.find('#basemapswitcher').unbind('change');
                $this.find('#basemapswitcher').change(function () {
                    if ($('#basemaptype').val() == "1") {
                        basemapImagery.hide();
                        basemapTopo.hide();
                        basemap.show();
                    }
                    else if (($('#basemaptype').val() == "2")) {
                        basemapImagery.show();
                        basemapTopo.hide();
                        basemap.hide();
                    }
                    else if (($('#basemaptype').val() == "3")) {
                        basemapImagery.hide();
                        basemapTopo.show();
                        basemap.hide();
                    }
                });

                $this.find("#printbutton").unbind('click');
                $this.find("#printbutton").click(function (e) {
                    var template = $('#printtemplates').prop('value');
                    if (template == "Layout") {
                        alert("Please select a print layout.");
                    } else {
                        $('#printtemplates').prop('value', 'Printing...');
                        $('#printtemplates').prop('disabled', true);
                        if (template == "Map Only") {
                            PrintMap("MAP_ONLY");
                        } else {
                            PrintMap("A4 " + template);
                        }
                    }
                });

                $this.find("#printtemplates").unbind('click');
                $this.find("#printtemplates").click(function (e) {
                    $('.selectBox').slideToggle(200);
                    $('.selectBox li').click(function () {
                        $('.mehidden').val($(this).text());
                        $('.default').text($(this).text());
                        $('.selectBox').slideUp(200);
                    });
                });

                $this.find(".selectBox li").unbind('click');
                $this.find(".selectBox li").click(function (e) {
                    switch ($(this).prop('title')) {
                        case "Portrait":
                            $('#printtemplates').prop('value', 'Portrait');
                            break;

                        case "Landscape":
                            $('#printtemplates').prop('value', 'Landscape');
                            break;

                        case "Map Only":
                            $('#printtemplates').prop('value', 'Map Only');
                            break;
                    }
                });

                $this.find('#modalbutton').modal();

                //esri.config.defaults.io.proxyUrl = "//" + gisServicesProxyHost + "/ArcGIS%20Proxy/proxy.ashx";

                //code to create the map and add a basemap will go here
                var map = new esri.Map("map", {
                    logo: false
                });

                $('#map').unbind('resize');
                $('#map').resize(function () {
                    map.resize();
                });

                $('#map').on('click', '.editWithdrawalLocationLink', function () {
                    map.infoWindow.hide();
                    EditWithdrawalLocation($(this).attr('objectid'));
                });

                $('#map').on('click', '.removeWithdrawalLocationLink', function () {
                    if (confirm("Are you sure you want to remove this withdrawal location?")) {
                        RemoveWithdrawalLocation($(this).attr('objectid'));
                    }
                });

                map.infoWindow.resize(385, 300);

                var drawToolbar = new esri.toolbars.Draw(map);
                dojo.connect(drawToolbar, "onDrawEnd", onIdentifyLocationEnd);
                esri.bundle.toolbars.draw.addPoint = "Click on the map to define the location of a withdrawal or a parcel where water will be used";

                map.on("extent-change", function (evt) {
                    if (evt.lod.level > 5) {
                        // We're zoomed in, enable the adding of points
                        drawToolbar.activate(esri.toolbars.Draw.POINT);
                    }
                    else {
                        // Zoomed out, remove the ability to add a point
                        drawToolbar.deactivate();
                    }
                });

                geometrySvc = new esri.tasks.GeometryService(gisServicesHost + "/arcgis/rest/services/Utilities/Geometry/GeometryServer");

                var basemapURL = gisServicesHost + "/mdimap/ArcGIS/rest/services/ImageryBaseMapsEarthCover/MD.State.MDiMap_Gazetteer83M/MapServer/";
                var basemap = new esri.layers.ArcGISTiledMapServiceLayer(basemapURL);
                map.addLayer(basemap);

                var basemapImageryUrl = gisServicesHost + "/mdimap/ArcGIS/rest/services/ImageryBaseMapsEarthCover/MD.State.6InchImagery/MapServer";
                basemapImagery = new esri.layers.ArcGISTiledMapServiceLayer(basemapImageryUrl);
                map.addLayer(basemapImagery);
                basemapImagery.hide();

                var basemapTopoURL = "https://server.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer";
                var basemapTopo = new esri.layers.ArcGISTiledMapServiceLayer(basemapTopoURL);
                map.addLayer(basemapTopo);
                basemapTopo.hide();

                var parcelLayerURL = gisServicesHost + "/mdimap/ArcGIS/rest/services/PlanningCadastre/MD.State.ParcelBoundaries/MapServer/";
                var parcelLayer = new esri.layers.ArcGISTiledMapServiceLayer(parcelLayerURL);
                map.addLayer(parcelLayer);

                var taxMapLayerURL = "https://geodata.md.gov/imap/rest/services/PlanningCadastre/MD_PropertyData/MapServer";
                var taxMapLayer = new esri.layers.ArcGISTiledMapServiceLayer(taxMapLayerURL);
                map.addLayer(taxMapLayer);

                var permitLocationsURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsWaterWithdrawalAndPermitLocation/FeatureServer/2";
                var permitLocationsLayer = new esri.layers.FeatureLayer(permitLocationsURL, {
                    mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
                    outFields: ['*']
                });
                permitLocationsLayer.setDefinitionExpression("PermitId=" + $('#PermitId').val());
                dojo.connect(permitLocationsLayer, "onClick", onPermitLocationClick);
                var handle = dojo.connect(permitLocationsLayer, "onUpdateEnd", function () {
                    if (this.graphics.length == 0) {
                        // Pop up Modal Dialog
                        $('<div></div>')
                            .html("<p>Please use the search or navigation tools to locate your area of interest and click on the map to define the approximate withdrawal location or locations.</p><p>If you currently can't define the approximate location simply click near the center of the parcel on which the withdrawal will occur.</p>")
                            .dialog({
                                draggable: false,
                                modal: true,
                                resizable: false
                            });

                        dojo.disconnect(handle);
                        return;
                    }

                    var permitExtent = esri.graphicsExtent(this.graphics);
                    if (permitExtent) {
                        this._map.setExtent(permitExtent, true);
                        dojo.disconnect(handle);
                    }
                });
                map.addLayer(permitLocationsLayer);

                var groundwaterInfoWindowTemplate = new esri.InfoTemplate("Groundwater Withdrawal", getGroundwaterInfoWindowContent);
                var surfaceWaterInfoWindowTemplate = new esri.InfoTemplate("Surface Water Withdrawal", getSurfaceWaterInfoWindowContent);

                var groundwaterWithdrawalPointsURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsWaterWithdrawalAndPermitLocation/FeatureServer/1";
                var groundwaterWithdrawalPointsLayer = new esri.layers.FeatureLayer(groundwaterWithdrawalPointsURL, {
                    infoTemplate: groundwaterInfoWindowTemplate,
                    mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
                    outFields: ['*']
                });
                groundwaterWithdrawalPointsLayer.setDefinitionExpression("PermitId=" + $('#PermitId').val());
                map.addLayer(groundwaterWithdrawalPointsLayer);

                var surfaceWaterWithdrawalPointsURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsWaterWithdrawalAndPermitLocation/FeatureServer/0";
                var surfaceWaterWithdrawalPointsLayer = new esri.layers.FeatureLayer(surfaceWaterWithdrawalPointsURL, {
                    infoTemplate: surfaceWaterInfoWindowTemplate,
                    mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
                    outFields: ['*']
                });
                surfaceWaterWithdrawalPointsLayer.setDefinitionExpression("PermitId=" + $('#PermitId').val());
                map.addLayer(surfaceWaterWithdrawalPointsLayer);

                $this.on('mouseover', 'svg#map_gc image', function () {
                    $('.map > .tooltip').html('Click to view/edit withdrawal location');
                });

                $this.on('mouseout', 'svg#map_gc image', function () {
                    $('.map > .tooltip').html('Click on the map to define the location of a withdrawal or a parcel where water will be used');
                });

                var locatorService = new esri.tasks.Locator(gisServicesHost + "/mdimap/ArcGIS/rest/services/GeocodeServices/MD.State.MDCascadingLocatorWithZIPCodes/GeocodeServer");
                dojo.connect(locatorService, "onAddressToLocationsComplete", showResults);

                $this.data('pagedata', {
                    Map: map,
                    GroundwaterWithdrawalPointsLayer: groundwaterWithdrawalPointsLayer,
                    SurfaceWaterWithdrawalPointsLayer: surfaceWaterWithdrawalPointsLayer,
                    PermitLocationsLayer: permitLocationsLayer,
                    LocatorService: locatorService,
                    DrawToolbar: drawToolbar,
                    ClickModal: new ClickModal(),
                    ConservationEasementModal: new ConservationEasementModal()
                });

                isLoaded = true;
            });
            //}
        }

        this.HasAtLeastOnePoint = function () {
            var data = $this.data('pagedata');

            return data.GroundwaterWithdrawalPointsLayer.graphics.length > 0 ||
                data.SurfaceWaterWithdrawalPointsLayer.graphics.length > 0;
        }

        this.RefreshMap = function () {
            $this.data('pagedata').Map.infoWindow.hide();
            $this.data('pagedata').GroundwaterWithdrawalPointsLayer.refresh();
            $this.data('pagedata').SurfaceWaterWithdrawalPointsLayer.refresh();
            $this.data('pagedata').PermitLocationsLayer.refresh();
        }

        this.Destroy = function () {
            $this.css('z-index', '-9999');
        }

        var onPermitLocationClick = function (e) {
            $this.data('pagedata').ExistingPermitLocation = true;
            var feature = e.graphic;
            var objectId = feature.attributes.OBJECTID;

            var $modal = $('#clickfeaturemodal');
            var $deleteFeatureButton = $modal.find('#deletebutton');
            var $addPointButton = $modal.find('#addbutton');

            $deleteFeatureButton.unbind('click');
            $deleteFeatureButton.click(function () {
                $.ajax({
                    url: $modal.attr('data-submiturl'),
                    type: 'POST',
                    data: { id: objectId },
                    success: function (data) {
                        $this.data('pagedata').ExistingPermitLocation = null;
                        $modal.dialog('close');

                        if (data.Success) {
                            ApplicationWizard.IdentifyLocation.RefreshMap();
                        }
                    }
                });
            });

            $addPointButton.unbind('click');
            $addPointButton.click(function () {
                $modal.dialog('close');
                onIdentifyLocationEnd(e.mapPoint);
            });

            $modal.dialog({
                modal: true,
                height: 'auto',
                width: 'auto',
                position: 'center',
                draggable: false,
                resizable: false
            });

            e.stopPropagation();
        }

        var onIdentifyLocationEnd = function (point) {
            $.blockUI({ message: '<h4>Please wait, page loading...</h4>' });
            $this.data('pagedata', $.extend($this.data('pagedata'), { CurrentlySelectedWithdrawalLocation: point }));

            var map = $this.data('pagedata').Map;

            var identifyURL = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsSpatialQueries/MapServer";
            var identifyTask = new esri.tasks.IdentifyTask(identifyURL);

            var identifyParams = new esri.tasks.IdentifyParameters();
            identifyParams.geometry = point;
            identifyParams.returnGeometry = true;
            identifyParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;
            identifyParams.width = map.width;
            identifyParams.height = map.height;
            identifyParams.mapExtent = map.extent;
            identifyParams.tolerance = 0;

            identifyTask.execute(identifyParams, identifyTaskComplete);
        }

        var identifyTaskComplete = function (result) {
            // If we don't get both a county and a parcel, return
            if (result.length < 2) {
                $.unblockUI();
                return;
            }

            if ($this.data('pagedata').ExistingPermitLocation) {
                $this.data('pagedata').ExistingPermitLocation = null;

                //Get the X & Y values from the point and pass them along to the get request
                var loc = $this.data('pagedata').CurrentlySelectedWithdrawalLocation;
                var x = loc.x;
                var y = loc.y;
                var srid = loc.spatialReference.wkid;

                var $modalButton = $this.find('#modalbutton');
                $modalButton.attr('contenturl', $('#identifyLocation #AddWithdrawalLocationURL').val() + '?' + $.param({ permitId: $('#PermitId').val(), x: x, y: y, srid: srid }));
                $modalButton.click();
            }
            else {
                $.unblockUI();
                $this.data('pagedata').ClickModal.Open();
            }
        }

        var EditWithdrawalLocation = function (objectId) {
            var $modalButton = $this.find('#modalbutton');
            $modalButton.attr('contenturl', $('#EditWithdrawalLocationURL').val() + '/' + objectId);
            $modalButton.click();
        }

        var RemoveWithdrawalLocation = function (objectId) {
            $.ajax({
                url: $('#RemoveWithdrawalLocationURL').val(),
                type: 'POST',
                data: { id: objectId },
                success: function (data) {
                    if (data && data.Success) {
                        $this.data('pagedata').Map.infoWindow.hide();
                        $this.data('pagedata').GroundwaterWithdrawalPointsLayer.refresh();
                        $this.data('pagedata').SurfaceWaterWithdrawalPointsLayer.refresh();
                        $this.data('pagedata').PermitLocationsLayer.refresh();
                    }
                    else {
                        if (data && data.Message) {
                            alert("An error has occurred attempting to remove the withdrawal location: " + data.Message);
                        }
                        else {
                            alert("An error has occurred attempting to remove the withdrawal location");
                        }
                    }
                }
            });
        }

        var getGroundwaterInfoWindowContent = function (graphic) {
            var $container = $('<div></div>').css('height', 275).css('overflow', 'auto').css('margin-bottom', 15);

            var $table = $('<table></table>').css('width', 350)
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Type:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.GroundwaterType))));

            if (graphic.attributes.GroundwaterType == "Well") {
                $table
                    .append($('<tr></tr>')
                        .append($('<td class="infoWindowLeftLabel"></td>').text('Well Status:'))
                        .append($('<td></td>').text(nullOrEmpty(graphic.attributes.IsExisting))))
                    .append($('<tr></tr>')
                        .append($('<td class="infoWindowLeftLabel"></td>').text('Well Depth:'))
                        .append($('<td></td>').text(nullOrEmpty(graphic.attributes.WellDepthFeet + ' ft'))))
                    .append($('<tr></tr>')
                        .append($('<td class="infoWindowLeftLabel"></td>').text('Well Diameter:'))
                        .append($('<td></td>').text(nullOrEmpty(graphic.attributes.WellDiameterInches + ' in'))))
                    .append($('<tr></tr>')
                        .append($('<td class="infoWindowLeftLabel"></td>').text('Well Tag #:'))
                        .append($('<td></td>').text(nullOrEmpty(graphic.attributes.WellTagNumber))));
            }

            $table
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('County:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.County))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Street:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Street))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('City:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.City))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Zip:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Zip))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Tax Map:'))
                    .append($('<td></td>').html(getTaxMapCellText(graphic.attributes.TaxMap, graphic.attributes.ParcelId))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Watershed:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Watershed))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Comments:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Comment))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Water Use(s):'))
                    .append($('<td></td>').html(nullOrEmpty(graphic.attributes.WaterUse).replace('|', '<br/>'))));

            var $link = $('<div></div>').css('text-align', 'center').css('position', 'absolute').css('bottom', 0).width(350)
                            .append($('<span></span>')
                                .addClass('spanLink')
                                .addClass('editWithdrawalLocationLink')
                                .css('margin-right', 5)
                                .attr('objectid', graphic.attributes.OBJECTID)
                                .text('Edit'))
                            .append($('<span></span>')
                                .addClass('spanLink')
                                .addClass('removeWithdrawalLocationLink')
                                .attr('objectid', graphic.attributes.OBJECTID)
                                .text('Remove'));

            return $('<div></div>').append($container.append($table)).append($link).html();
        }

        var getSurfaceWaterInfoWindowContent = function (graphic) {
            var $container = $('<div></div>').css('height', 275).css('overflow', 'auto').css('margin-bottom', 15);

            var $table = $('<table></table>').css('width', 350)
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Type:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.SurfacewaterType))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Water Body Name:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.WaterSourceName))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Location of Intake:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.ExactLocationOfIntake))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('County:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.County))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Street:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Street))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('City:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.City))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Zip:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Zip))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Tax Map:'))
                    .append($('<td></td>').html(getTaxMapCellText(graphic.attributes.TaxMap, graphic.attributes.ParcelId))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Watershed:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Watershed))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Comments:'))
                    .append($('<td></td>').text(nullOrEmpty(graphic.attributes.Comment))))
                .append($('<tr></tr>')
                    .append($('<td class="infoWindowLeftLabel"></td>').text('Water Use(s):'))
                    .append($('<td></td>').html(nullOrEmpty(graphic.attributes.WaterUse).replace('|', '<br/>'))));

            var $link = $('<div></div>').css('text-align', 'center').css('position', 'absolute').css('bottom', 0).width(350)
                            .append($('<span></span>')
                                .addClass('spanLink')
                                .addClass('editWithdrawalLocationLink')
                                .css('margin-right', 5)
                                .attr('objectid', graphic.attributes.OBJECTID)
                                .text('Edit'))
                            .append($('<span></span>')
                                .addClass('spanLink')
                                .addClass('removeWithdrawalLocationLink')
                                .attr('objectid', graphic.attributes.OBJECTID)
                                .text('Remove'));

            return $('<div></div>').append($container.append($table)).append($link).html();
        }

        var nullOrEmpty = function (string) {
            return string && string != '' ? string : 'Not defined';
        }

        var getTaxMapCellText = function (taxMap, parcel) {
            if (taxMap && taxMap != '') {
                if (parcel && parcel != '') {
                    var baseUrl = 'http://sdatcert3.resiusa.org/rp_rewrite/details.aspx';

                    if (parcel.substring(0, 2) == '03') {
                        // Baltimore City has a different format for their ACCTID
                        var countyId = parcel.substring(0, 2);
                        var ward = parcel.substring(2, 4);
                        var section = parcel.substring(4, 6);
                        var block = parcel.substring(6, 11).replace(/\s+$/, ''); //Some blocks have an alpha character at the end, while others have a whitespace
                        var lot = parcel.substring(11, 14);

                        var queryString = '?County=' + countyId + '&SearchType=ACCT&Ward=' + ward + '&Section=' + section + '&Block=' + block + '&Lot=' + lot;

                        return taxMap + '<a href="' + baseUrl + queryString + '" target="_blank">SDAT</a>';
                    }
                    else {
                        var countyId = parcel.substring(0, 2);
                        var districtId = parcel.substring(2, 4);
                        var accountNumber = parcel.substring(4);

                        var queryString = '?County=' + countyId + '&SearchType=ACCT&District=' + districtId + '&AccountNumber=' + accountNumber;

                        return taxMap + '<a href="' + baseUrl + queryString + '" target="_blank">SDAT</a>';
                    }
                }
                else {
                    return taxMap;
                }
            }
            else {
                return 'Not Defined';
            }
        }

        var PrintMap = function (templateString) {
            var printUrl = gisServicesHost + "/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";
            var printTask = new esri.tasks.PrintTask(printUrl);
            var params = new esri.tasks.PrintParameters();
            var template = new esri.tasks.PrintTemplate();
            params.map = $this.data('pagedata').Map;
            template.layout = templateString;
            template.preserveScale = true;
            params.template = template;
            printTask.execute(params, printResult, printError);
        }

        var printResult = function (result) {
            $('#printtemplates').prop('value', 'Layout');
            $('#printtemplates').prop('disabled', false);
            window.open(result.url);
        }

        var printError = function (result) {
            alert(result);
            $('#printtemplates').prop('value', 'Layout');
            $('#printtemplates').prop('disabed', false);
        }

        var LocateAddress = function () {
            var data = $this.data('pagedata'),
                map = data.Map;

            map.graphics.clear();

            var address = { "SingleLine": $this.find('#search').val() };
            data.LocatorService.outSpatialReference = map.spatialReference;
            var options = {
                address: address,
                outFields: ["State"]
            }
            data.LocatorService.addressToLocations(options);
        }

        var ZoomToLatLong = function () {
            var data = $this.data('pagedata'),
                map = data.Map;

            map.graphics.clear();

            var latLong = $this.find('#search').val().split(',');
            if (latLong.length != 2) {
                return;
            }

            var lat = parseFloat(latLong[0]);
            var long = parseFloat(latLong[1]);

            var point = new esri.geometry.Point(long, lat);

            var params = new esri.tasks.ProjectParameters();
            params.geometries = [point];
            params.outSR = map.spatialReference;
            geometrySvc.project(params, onGeometryServiceProjectComplete);
        }

        var ZoomToParcel = function () {
            var data = $this.data('pagedata'),
                map = data.Map;

            map.graphics.clear();

            var params = $('#search').val().split(',');
            if (params.length < 4 || params.length > 5) {
                // Lot is optional, hence length < 4
                return;
            }

            var countyName = params[0].trim();

            var queryUrl = GetQueryTaskUrlByCountyName(countyName);
            if (queryUrl) {
                var queryTask = new esri.tasks.QueryTask(queryUrl);

                // build query filter
                var query = new esri.tasks.Query();
                query.returnGeometry = true;
                query.outFields = ["MAP", "GRID", "PARCEL", "LOT"];

                /* Parse out our query values */
                var parcelMap = ("0000" + params[1].trim()).slice(-4), // returns 00123params[1].replace(/^\s+|^[0]+|\s+$/g, ''),
                    grid = ("0000" + params[2].trim()).slice(-4), // returns 00123params[2].replace(/^\s+|^[0]+|\s+$/g, ''),
                    parcel = ("0000" + params[3].trim()).slice(-4); // returns 00123params[3].replace(/^\s+|^[0]+|\s+$/g, ''),

                query.where = "MAP = '" + parcelMap + "' AND GRID = '" + grid + "' AND PARCEL = '" + parcel + "'";

                if (params.length == 5) {
                    // Lot Included
                    var lot = params[4].trim();
                    query.where = query.where + " AND LOT = '" + lot + "'";
                }

                dojo.connect(queryTask, "onComplete", function (featureSet) {
                    if (featureSet.features.length == 0) {
                        alert("No results found");
                        return;
                    }

                    var feature = featureSet.features[0];

                    var symbol = new esri.symbol.SimpleMarkerSymbol();
                    var infoTemplate = new esri.InfoTemplate("Parcel", "Map: ${MAP}<br/>Grid: ${GRID}<br/>Parcel: ${PARCEL}<br/>Lot: ${LOT}");

                    symbol.setStyle(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE);
                    symbol.setColor(new dojo.Color([153, 0, 51, 0.75]));

                    //add a graphic to the map at the parcel location
                    var graphic = new esri.Graphic(feature.geometry, symbol, feature.attributes, infoTemplate);
                    map.graphics.add(graphic);

                    map.centerAndZoom(feature.geometry, 10);
                });

                queryTask.execute(query);
            }
        }

        var GetQueryTaskUrlByCountyName = function (countyName) {
            var baseUrl = gisServicesHost + "/arcgis/rest/services/" + gisServicesDirectory + "/MdeWsipsParcelPoints/MapServer/";

            // Append the Layer Id based upon the supplied county name
            switch (countyName.toLowerCase()) {
                case "allegany":
                case "al":
                    return baseUrl + "0";
                case "anne arundel":
                case "aa":
                    return baseUrl + "1";
                case "baltimore city":
                case "bc":
                    return baseUrl + "2";
                case "baltimore":
                case "ba":
                    return baseUrl + "3";
                case "calvert":
                case "ca":
                    return baseUrl + "4";
                case "caroline":
                case "co":
                    return baseUrl + "5";
                case "carroll":
                case "cl":
                    return baseUrl + "6";
                case "cecil":
                case "ce":
                    return baseUrl + "7";
                case "charles":
                case "ch":
                    return baseUrl + "8";
                case "dorchester":
                case "do":
                    return baseUrl + "9";
                case "frederick":
                case "fr":
                    return baseUrl + "10";
                case "garrett":
                case "ga":
                    return baseUrl + "11";
                case "harford":
                case "ha":
                    return baseUrl + "12";
                case "howard":
                case "ho":
                    return baseUrl + "13";
                case "kent":
                case "ke":
                    return baseUrl + "14";
                case "montgomery":
                case "mo":
                    return baseUrl + "15";
                case "prince george's":
                case "prince georges":
                case "pg":
                    return baseUrl + "16";
                case "queen anne's":
                case "queen annes":
                case "qa":
                    return baseUrl + "17";
                case "somerset":
                case "so":
                    return baseUrl + "18";
                case "st. mary's":
                case "st. marys":
                case "st mary's":
                case "st marys":
                case "sm":
                    return baseUrl + "19";
                case "talbot":
                case "ta":
                    return baseUrl + "20";
                case "washington":
                case "wa":
                    return baseUrl + "21";
                case "wicomico":
                case "wi":
                    return baseUrl + "22";
                case "worcester":
                case "wo":
                    return baseUrl + "23";
                default:
                    alert("Invalid County Name");
                    return;
            }
        }

        var showResults = function (candidates) {
            var candidate;
            var symbol = new esri.symbol.SimpleMarkerSymbol();
            var infoTemplate = new esri.InfoTemplate("Location", "Address: ${address}");

            symbol.setStyle(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE);
            symbol.setColor(new dojo.Color([153, 0, 51, 0.75]));

            var geom,
                data = $this.data('pagedata'),
                map = data.Map;

            $.each(candidates, function (idx, candidate) {
                if (candidate.score > 80) {
                    var attributes = { address: candidate.address, score: candidate.score, locatorName: candidate.attributes.Loc_name };
                    geom = candidate.location;

                    //add a graphic to the map at the geocoded location
                    var graphic = new esri.Graphic(geom, symbol, attributes, infoTemplate);
                    map.graphics.add(graphic);

                    //Add a text symbol to the map listing the location of the matched address.
                    var displayText = candidate.address;
                    var font = new esri.symbol.Font("16pt", esri.symbol.Font.STYLE_NORMAL, esri.symbol.Font.VARIANT_NORMAL, esri.symbol.Font.WEIGHT_BOLD, "Helvetica");

                    var textSymbol = new esri.symbol.TextSymbol(displayText, font, new dojo.Color("#666633"));
                    textSymbol.setOffset(0, 8);
                    map.graphics.add(new esri.Graphic(geom, textSymbol));

                    //break out of loop after one candidate with score greater than 80 is found.
                    return false;
                }
            });

            if (geom == undefined) {
                alert("No results found");
            }
            else {
                map.centerAndZoom(geom, 10);
            }
        }

        var onGeometryServiceProjectComplete = function (geometries) {
            var geom = geometries[0],
                data = $this.data('pagedata'),
                map = data.Map;

            if (geom == null || isNaN(geom.x) || isNaN(geom.y)) {
                alert("No results found");
                return;
            }

            var symbol = new esri.symbol.SimpleMarkerSymbol();
            symbol.setStyle(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE);
            symbol.setColor(new dojo.Color([153, 0, 51, 0.75]));

            //Add a graphic to the map at the x,y location
            var graphic = new esri.Graphic(geom, symbol);
            map.graphics.add(graphic);

            //Add a text symbol to the map listing the location of the matched address.
            //var displayText = candidate.address;
            //var font = new esri.symbol.Font("16pt", esri.symbol.Font.STYLE_NORMAL, esri.symbol.Font.VARIANT_NORMAL, esri.symbol.Font.WEIGHT_BOLD, "Helvetica");

            //var textSymbol = new esri.symbol.TextSymbol(displayText, font, new dojo.Color("#666633"));
            //textSymbol.setOffset(0, 8);
            //map.graphics.add(new esri.Graphic(geom, textSymbol));

            map.centerAndZoom(geom, 10);
        }

        var ConservationEasementModal = function () {
            // remove dropdown plugin
            $('#conservationeasementmodal').find($('[class="ui-combobox"]')).detach();
            $('#IsSubjectToEasement').show();
            $('#HaveNotifiedEasementHolder').show();

            //var $IsSubjectToEasement = $('#IsSubjectToEasement');

            $.ajax({
                url: $('#conservationeasementmodal').attr('data-contenturl'),
                type: 'GET',
                data: { permitId: $('#PermitId').val() },
                success: function (data) {
                    $('#IsSubjectToEasement').val(data.IsSubjectToEasement).change();
                    $('#HaveNotifiedEasementHolder').val(data.HaveNotifiedEasementHolder).change();
                }
            });

            if ($('#IsSubjectToEasement').val() == 'true') {
                $('#easementHolderContainer').show();
            }

            $('#IsSubjectToEasement').change(function () {
                if ($(this).val() == 'true') {
                    $('#easementHolderContainer').show();
                } else {
                    $('#easementHolderContainer').hide();
                }

                $(this).removeClass('input-validation-error');
                $('#HaveNotifiedEasementHolder').removeClass('input-validation-error').val("");
                $('#easementerror').hide();
            });
        }

        var ClickModal = function () {
            var $this = $('#clickmapmodal');

            var $withdrawalBtn = $this.find('#withdrawalbutton');
            var $parcelBtn = $this.find('#parcelbutton');
            var $cancelBtn = $this.find('#cancelbutton');

            $withdrawalBtn.click(function () {
                $.blockUI({ message: '<h4>Please wait, page loading...</h4>' });

                // Adding a withdrawal location
                $this.dialog('close');

                //Get the X & Y values from the point and pass them along to the get request
                var loc = $('#identifyLocation').data('pagedata').CurrentlySelectedWithdrawalLocation;
                var x = loc.x;
                var y = loc.y;
                var srid = loc.spatialReference.wkid;

                var $modalButton = $('#identifyLocation #modalbutton');
                $modalButton.attr('contenturl', $('#identifyLocation #AddWithdrawalLocationURL').val() + '?' + $.param({ permitId: $('#PermitId').val(), x: x, y: y, srid: srid }));
                $modalButton.click();
            });

            $parcelBtn.click(function () {
                // We're not adding a withdrawal location, however we still need to
                // submit our point to add a permit parcel
                $this.dialog('close');
                var withdrawalLocation = $('#identifyLocation').data('pagedata').CurrentlySelectedWithdrawalLocation;

                $.ajax({
                    url: $this.attr('data-submiturl'),
                    type: 'POST',
                    data: { permitId: $('#PermitId').val(), x: withdrawalLocation.x, y: withdrawalLocation.y, srid: withdrawalLocation.spatialReference.wkid },
                    success: function (data) {
                        $this.dialog('close');

                        if (data.Success) {
                            ApplicationWizard.IdentifyLocation.RefreshMap();
                        }
                    }
                });
            })

            $cancelBtn.click(function () {
                // User cancelled out of dialog
                $this.dialog('close');
            });

            $this.dialog({
                modal: true,
                height: 'auto',
                width: 'auto',
                position: 'center',
                draggable: false,
                resizable: false,
                autoOpen: false
            });

            this.Open = function () {
                $this.dialog('open');
            }
        }
    }

    $.extend(ApplicationWizard, {
        IdentifyLocation: {
            Initialize: function () {
                var page = new MapPage();
                page.init();
            },

            Next: function () {
                var page = new MapPage();
                if (!page.HasAtLeastOnePoint()) {
                    alert("Please add at least one withdrawal location before proceeding");
                    $('.IdentifyLocation').addClass('wizardStepActive');
                    return;
                }

                $('#conservationeasementmodal').dialog({
                    modal: true,
                    position: "center",
                    draggable: false,
                    resizable: false,
                    width: 500,
                    close: function () {
                        $(this).dialog("destroy");
                    },
                    buttons: {
                        "Submit": function () {
                            if ($('#IsSubjectToEasement').val() == "") {
                                $('#IsSubjectToEasement').addClass('input-validation-error')
                                $('#easementerror').show()
                                return
                            }

                            if (($('#IsSubjectToEasement').val() == 'true' && $('#HaveNotifiedEasementHolder').val() == "")) {
                                $('#HaveNotifiedEasementHolder').addClass('input-validation-error')
                                $('#easementerror').show()
                                return
                            }

                            $.ajax({
                                url: $(this).attr('data-posturl'),
                                type: 'POST',
                                data: { permitId: $('#PermitId').val(), isSubjectToEasement: $('#IsSubjectToEasement').val(), haveNotifiedEasementHolder: $('#HaveNotifiedEasementHolder').val() },
                                success: function (data) {
                                    $('#easementerror').hide()
                                    $('#IsSubjectToEasement').removeClass('input-validation-error')
                                    $('#HaveNotifiedEasementHolder').removeClass('input-validation-error')

                                    $('#conservationeasementmodal').dialog("close");
                                    page.Destroy();
                                    ReviewApplication.Initialize();
                                }
                            });
                        }
                    }
                });
            },

            Back: function () {
                var page = new MapPage();
                page.Destroy();
                var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });

                if (_.any(selectedValues, function (value) { return parseInt(value) > 11; })) {
                    WastewaterTreatmentAndDisposal.Initialize();
                }
                else if (global.showWaterUseDetail) {
                    // Redirect to next page (view)
                    WaterUseDetail.Initialize();
                }
                else {
                    WaterUseCategoryAndType.Initialize();
                }
            },

            Cancel: function () {
            },

            RefreshMap: function () {
                var page = new MapPage();
                page.RefreshMap();
            }
        }
    });
})(jQuery)