﻿/// <reference path="_permitReference.js" />

$.extend(WaterUseCategoryAndType, {
    Initialize: function () {
        global.currentStep = Steps.WaterUseCategoryAndType;
        $('#CurrentStep').val('6');
        $('.waterUseCategoryType').addClass('wizardStepActive');
        $('#waterUseCatagoryAndType').show();
        $('#SaveButton').hide();
    },

    Next: function () {
        WaterUseCategoryAndType.SaveWaterUseCategoryType();
    },

    Back: function () {
        $('#waterUseCatagoryAndType').hide();
        //$('#SaveButton').hide();
        WaterUseDescription.Initialize();
    },

    Cancel: function () {
    },

    SaveWaterUseCategoryType: function () {

        if (global.WaterUseCategoryAndTypeChanged) {
            $.blockUI({ message: '<h4>Please wait, page loading...</h4>' });

            var _form = $('#wizardForm');

            if (_form.valid()) {
                _form.attr('data-ajax-success', "WaterUseCategoryAndType.SaveCallBack");
                ApplicationWizard.CollectSelectedWaterUseCategoryType();
                _form.submit();
            }
        }
        else {
            WaterUseCategoryAndType.InitializeNextPage();
        }
    },

    SaveCallBack: function (response) {

        //Reset to the default save call back function
        $('#wizardForm').attr('data-ajax-success', "ApplicationWizard.SaveCallBack");
        //unblock the page 
        $.unblockUI();

        if (response.success) {
            global.WaterUseCategoryAndTypeChanged = false;
            WaterUseCategoryAndType.InitializeNextPage();
        }
        else {
            $("#saveMessage").html('An error occured while attempting to proceed to next step. Please contact the WSIPS system administrator.').dialog({
                modal: true,
                position: "center",
                width: 400
            });
        }
    },

    InitializeNextPage: function () {

        $('#waterUseCatagoryAndType').hide();
        // Redirect to next page (view)
        global.isSaveAndResume = false;

        if (global.WaterUseCategoryAndTypeChanged) {
            global.dataSaved = false;
            ApplicationWizard.SaveData();
        }
        else {
            global.dataSaved = true;
        }

        var selectedValues = _.filter(global.SelectedWaterUseCategoryTypeList, function (selected) { return selected != 0; });
        if (selectedValues.length > 0) {
            WaterUseDetail.Initialize();
        }
        else {
            global.showWaterUseDetail = false;
            ApplicationWizard.IdentifyLocation.Initialize();
        }
    }
});
