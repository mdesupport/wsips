﻿/// <reference path="_permitReference.js" />
/// <reference path="../../../Areas/Permitting/Views/Permit/WaterUseCategoryAndType.cshtml" />

$.extend(WaterUseDetail, {
    Next: function () {
        $('#usePercentValidation').hide();

        if (this.ValidateData()) {
            $('#waterUseDetails').hide();
            //Chekc if we need to go to this view
            WastewaterTreatmentAndDisposal.Initialize();
        }
        else {
            $('.waterUseDetails').addClass('wizardStepActive');
        }
    },

    Back: function () {
        $('#waterUseDetails').hide();
        $('#SaveButton').hide();
        WaterUseCategoryAndType.Initialize();
    },

    Cancel: function () {
    }
});