﻿/// <reference path="_permitReference.js" />

var WaterUseDescription = {
    PreviousView: "",
    Initialize: function () {
        ApplicationWizard.ClearSelectedWizardStep();
        this.PreviousView = global.currentStep;
        global.currentStep = Steps.WaterUseDescription;
        $('#CurrentStep').val('5');
        $('.waterUseDescription').addClass('wizardStepActive');
        $('#contactSearchResult').hide();
        $('#SearchedContacts').hide();
        $('#SaveButton').show();
        $('#waterUseDescription').show();
        //$('.scrollwrap').width(ApplicationWizard.RenderbodyRightWidth());

        ApplicationWizard.InitializeGlobalVariable();
    },

    Next: function () {
        if (this.ValidateData()) {

            //save and continue
            if ($('#PermitId').val() == "0") {
                ApplicationWizard.SaveData();
            }

            $('#waterUseDescription').hide();
            WaterUseCategoryAndType.Initialize();
        }
        else {
            $('.waterUseDescription').addClass('wizardStepActive');
        }
    },

    Back: function () {
        $('#waterUseDescription').hide();

        if ((!global.isLandOwner && !global.isWaterUser && global.permittee != "4") || !global.isLandOwner && global.isWaterUser) {
            LandOwner.Initialize();
        }
        else if ((global.isLandOwner && !global.isWaterUser) || !global.isLandOwner && !global.isWaterUser && global.permittee == "4") {
            WaterUser.Initialize();
        }
        else if (global.isLandOwner && global.isWaterUser) {
            InformationVerification.Initialize();
        }
        else if (global.permittee == "4" && !global.isLandOwner && !global.isWaterUser) {
            WaterUser.Initialize();
        }
    },

    Cancel: function () {
    },

    ValidateData: function () {
        //validate the form and if there is validation error don't pass to next step
        return $('#wizardForm').valid();
    }
};