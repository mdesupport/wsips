﻿/// <reference path="ApplicationWizard.js" />
/// <reference path="../../underscore.js" />

//Bind on change of the radio button
$('#SearchedContacts').on('change', 'input:radio[id$=IsBusiness]', function () {
    var isBusiness = $(this).val();

    ContactSearchResult.ClearValidationError();
    ContactSearchResult.AdjustContactInformationLayout(isBusiness);
});

// Extends BaseContactSearchResult 
$.extend(ContactSearchResult, {
    Next: function () {
        if (global.CurrentlySearching == "WaterUser") {
            WaterUser.Next();
        }
        else if (global.CurrentlySearching == "LandOwner") {
            LandOwner.Next();
        }
    },

    Back: function () {
        if (global.CurrentlySearching == "WaterUser") {
            WaterUser.Back();
        }
        else if (global.CurrentlySearching == "LandOwner") {
            LandOwner.Back();
        }
    },

    Cancel: function () {
    }
});

$.extend(WaterUser, {
    Next: function () {
        $('#WaterUser .stateValidationMessage').hide();
        $('#WaterUser .primaryPhoneNumberValidationMessage').hide();

        var selectedState = $('#WaterUser').find('[id$=StateId]').val();
        var primaryPhoneNumber = $('#WaterUser').find('[id$=PrimaryTelephoneNumber_CommunicationValue]').val();;
        var primaryPhoneType = $('#WaterUser').find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val();;

        if (ContactSearchResult.ValidateData()) {
            if (selectedState == '') {
                ContactSearchResult.ShowStateValidationMessage('#WaterUser');
                ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
            }
            else if (primaryPhoneNumber == '' || primaryPhoneType == '') {
                ContactSearchResult.ShowPrimaryPhoneNumberValidationMessage('#WaterUser');
                ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
            }
            else {
                LandOwner.Initialize();
            }
        }
        else {
            ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));
        }
    },
    Back: function () {
        // until they confirm, Stay in the same step
        ApplicationWizard.HighlightCurrentWizardStep($('.WaterUser'));

        $('#SearchedContacts').hide();
        ApplicationWizard.ClearSelectedWizardStep();
        InformationVerification.Initialize();
    }
});

$.extend(LandOwner, {
    Next: function () {
        $('#LandOwner .stateValidationMessage').hide();
        $('#LandOwner .primaryPhoneNumberValidationMessage').hide();

        var selectedState = $('#LandOwner').find('[id$=StateId]').val();
        var primaryPhoneNumber = $('#LandOwner').find('[id$=PrimaryTelephoneNumber_CommunicationValue]').val();;
        var primaryPhoneType = $('#LandOwner').find('[id$=PrimaryTelephoneNumber_SelectedCommunicationMethodId]').val();;

        if (ContactSearchResult.ValidateData()) {
            if (selectedState == '') {
                ContactSearchResult.ShowStateValidationMessage('#LandOwner');
                ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
            }
            else if (primaryPhoneNumber == '' || primaryPhoneType == '') {
                ContactSearchResult.ShowPrimaryPhoneNumberValidationMessage('#LandOwner');
                ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
            }
            else {
                WaterUseDescription.Initialize();
            }
        }
        else {
            ApplicationWizard.HighlightCurrentWizardStep($('.LandOwner'));
        }
    },
    Back: function () {
        if (global.isWaterUser == 0 && global.isLandOwner == 0) {
            WaterUser.Initialize();
        }
        else {
            InformationVerification.Initialize();
        }
    }
});