﻿/// <reference path="_permitReference.js" />

$('#InformationVerification').on('change', 'input:radio[id$=IsBusiness]', function () {
    var isBusiness = $(this).val();

    InformationVerification.AdjustContactInformationLayout(isBusiness);
});

var InformationVerification = {
    Initialize: function () {
        global.currentStep = Steps.InformationVerification;        
        $('.InformationVerification').addClass('wizardStepActive');
        $('[id$=TelephoneNumber_CommunicationValue]').mask("999-999-9999");
        $('#MessageSummary').html('');
        $('#BackButton').show();
        $('#SaveButton').hide();
        $('#NextButton').show();

        //Hide searched values 
        $('#LandOwner').hide();
        $('#WaterUser').hide();

        this.ShowView();
    },

    ShowView: function () {
        $('#InformationVerification').show();

        //if we are resuming permit application
        var _permitOwner = parseInt(global.permittee);

        if (global.isWaterUser == 0 && global.isLandOwner == 0 && global.isConsultant == 1) {
            $('#titleOne').show();
            $('#titleTwo').hide();
        }
            //if the applicant is either the water user or the land owner and the permit is issued to the applicant 
        else if (((global.isWaterUser == 1 && global.isLandOwner == 0) || (global.isWaterUser == 0 && global.isLandOwner == 1)) &&
            _permitOwner == 1) {
            $('#titleOne').hide();
            $('#titleTwo').show();
        }
            // if the applicant is a land owner and the permit is issued to the water user
        else if (global.isWaterUser == 0 && global.isLandOwner == 1 && _permitOwner == 2) {
            $('#titleOne').show();
            $('#titleTwo').hide();
        }
            // if the applicant is a water user and the permit is issued to the land owner
        else if (global.isWaterUser == 1 && global.isLandOwner == 0 && _permitOwner == 3) {
            $('#titleOne').show();
            $('#titleTwo').hide();
        }

        else if (global.isWaterUser == 1 && global.isLandOwner == 1) {
             // isBusiness -- False
            $('#titleOne').hide();
            $('#titleTwo').show();
        }

        InformationVerification.AdjustContactInformationLayout($('#Contact_IsBusiness:checked').val());
    },

    AdjustContactInformationLayout: function (isBusiness) {
        var $contactContainer = $('#InformationVerification');

        if (isBusiness == "True") {
            var $fmlNameContainer = $contactContainer.find("#FmlNameContainer").detach();
            var $secondaryNameContainer = $contactContainer.find("#SecondaryNameContainer").detach();

            if ($fmlNameContainer.length) {
                $contactContainer.find('#PersonalContactContainer').append($fmlNameContainer).show();

                $secondaryNameContainer.insertAfter($contactContainer.find("[id$=BusinessName]").closest('.divVerticalBottomSpacer'));
            }
            else {
                $contactContainer.find("#PersonalContactContainer").show();
            }

            $('#businessNameAsterisks').show();
        }
        else {
            var $fmlNameContainer = $contactContainer.find("#FmlNameContainer").detach();
            var $secondaryNameContainer = $contactContainer.find("#SecondaryNameContainer").detach();

            if ($fmlNameContainer.length) {
                $contactContainer.find('#NameInformationContainer').append($fmlNameContainer).append($secondaryNameContainer);
                $contactContainer.find("#PersonalContactContainer").hide();
            }
            else {
                $contactContainer.find("#PersonalContactContainer").hide();
            }

            $('#businessNameAsterisks').hide();
        }
    },

    Next: function () {
        if (this.ValidateData()) {
            $('#InformationVerification').hide();

            if (global.isWaterUser == 1 && global.isLandOwner == 1) {
                WaterUseDescription.Initialize();
            }
            else {
                //
                if (global.isWaterUser == 0) {
                    WaterUser.Initialize();
                }
                else if (global.isLandOwner == 0) {
                    LandOwner.Initialize();
                }
            }
        }
    },

    Back: function () {
        //keep the current step until they confrim
        $('.InformationVerification').addClass('wizardStepActive');

        if (confirm("All the contacts that you added for water user and/or land owner will be deleted. Are you sure you want to start over the application?")) {
            $('#InformationVerification').hide();
            ApplicantIdentification.Initialize();
        }
    },

    Cancel: function () {
    },

    ValidateData: function () {
        //validate the form and if there is validation error don't pass to next step
        return $('#wizardForm').valid();
    }
}