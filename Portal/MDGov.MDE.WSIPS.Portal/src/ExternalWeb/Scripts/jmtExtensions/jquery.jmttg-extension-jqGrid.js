﻿/// <reference path="../jquery-1.8.2.js" />

$.jgrid.extend({
    fixGridWidth: function () {
        return this.each(function () {
            var grid = this.grid;
            var $content = $(this).closest('.scrollcontent');
            var cntWdth = $content.innerWidth();
            if (cntWdth != null) {
                var $wrap = $content.children('.scrollwrap');
                var gviewScrollWidth = this.parentNode.parentNode.parentNode.scrollWidth;


                var pdngLeft = (typeof ($wrap.css('padding-left')) == 'undefined') ? 0 : parseInt($wrap.css('padding-left'));
                var pdngRight = (typeof ($wrap.css('padding-right')) == 'undefined') ? 0 : parseInt($wrap.css('padding-right'));
                //var posRight = (typeof ($wrap.css('right')) == 'undefined') ? 0 : parseInt($wrap.css('right'));

                var wrapPadding = pdngLeft + pdngRight + 2 // +2 is for Grid Border
                var newGridWidth = eval(cntWdth - wrapPadding);
                if (newGridWidth != gviewScrollWidth) {
                    $(this).jqGrid("setGridWidth", newGridWidth);
                }
            }
        });

    },
    fixGridWidthRBRLargePadding: function () {
        return this.each(function () {
            var grid = this.grid;
            var $content = $(this).closest('.scrollcontent');
            var cntWdth = $content.innerWidth();
            if (cntWdth != null) {
                cntWdth = $(this).closest('.renderbodyRight').innerWidth();
                var $wrap = $content.children('.scrollwrap');
                var gviewScrollWidth = this.parentNode.parentNode.parentNode.scrollWidth;
                var pdngLeft = (typeof ($wrap.css('padding-left')) == 'undefined') ? 0 : parseInt($wrap.css('padding-left'));
                var pdngRight = (typeof ($wrap.css('padding-right')) == 'undefined') ? 0 : parseInt($wrap.css('padding-right'));
                var wrapPadding = pdngLeft + pdngRight + 50;
                var newGridWidth = eval(cntWdth - wrapPadding);
                if (newGridWidth != gviewScrollWidth) {
                    $(this).jqGrid("setGridWidth", newGridWidth);
                }
            }
        });

    },

    fixGridWidthMainContent: function () {
        

        return this.each(function () {
            var grid = this.grid;
            var $content = $(this).closest('.scrollcontent');
            var cntWdth = $content.innerWidth();
            if (cntWdth != null) {
                cntWdth = $(this).closest('#mainContent').innerWidth();
                var $wrap = $content.children('.scrollwrap');
                var gviewScrollWidth = this.parentNode.parentNode.parentNode.scrollWidth;
                var pdngLeft = (typeof ($wrap.css('padding-left')) == 'undefined') ? 0 : parseInt($wrap.css('padding-left'));
                var pdngRight = (typeof ($wrap.css('padding-right')) == 'undefined') ? 0 : parseInt($wrap.css('padding-right'));
                var wrapPadding = pdngLeft + pdngRight + 50;
                var newGridWidth = eval(cntWdth - wrapPadding);
                if (newGridWidth != gviewScrollWidth) {
                    $(this).jqGrid("setGridWidth", newGridWidth);
                }
            }
        });
    },
    
    fixGridWidthWithoutParentScroll: function () {
        return this.each(function () {
            //get outermost injected div for jq Grid
            var outerGBox = $(this).hlprGetOuterGDiv(); //$(this).closest('div[id*="gbox_"]');
            //get it's parent
            var outerParent = outerGBox.parent();

            var pdngLeft = (typeof (outerParent.css('padding-left')) == 'undefined') ? 0 : parseInt(outerParent.css('padding-left'));
            var pdngRight = (typeof (outerParent.css('padding-right')) == 'undefined') ? 0 : parseInt(outerParent.css('padding-right'));

            //get outer parent width
            var outerParentWidth = outerParent.innerWidth();

            var wrapPadding = pdngLeft + pdngRight + 2 // +2 is for Grid Border
            var newGridWidth = eval(outerParentWidth - wrapPadding);
            $(this).jqGrid("setGridWidth", newGridWidth);
        });
    },
    fixGridHeightWithoutParentScroll: function () {
        return this.each(function () {
            //get outermost injected div for jq Grid
            var outerGBox = $(this).hlprGetOuterGDiv();
            //get it's parent (div outside of injected table div)
            var outerParent = outerGBox.parent();
            //get padding in parent div (innerHeight does NOT include padding)
            var pdngTop = (typeof (outerParent.css('padding-top')) == 'undefined') ? 0 : parseInt(outerParent.css('padding-top'));
            var pdngBottom = (typeof (outerParent.css('padding-bottom')) == 'undefined') ? 0 : parseInt(outerParent.css('padding-bottom'));
            //get outer parent available height
            var outerParentHeight = outerParent.innerHeight() - eval(pdngTop + pdngBottom);
            //loop through all children - get their total height
            var totalChildrenHeight = $(this).hlprGetChildrenHeightTotal(outerParent);
            //get difference
            var newGridHeight = eval(outerParentHeight - totalChildrenHeight);
            //subtract values - vertical sizing does NOT take into account header & pager height!
            var headerHeight = $(".ui-jqgrid-hdiv").outerHeight(true);
            var pagerHeight = $(".ui-jqgrid-pager").outerHeight(true);
            newGridHeight = newGridHeight - eval(headerHeight + pagerHeight + 2); // +2 is for the top and bottom border of the gbox div
            //set grid to new height
            $(this).jqGrid("setGridHeight", newGridHeight);
        });
    },
    hlprGetOuterGDiv: function () {
        //gets outer gdiv based on plugin standard naming
        return $(this).closest('div[id*="gbox_"]');
    },
    hlprGetChildrenHeightTotal: function (outerParent) {
        //get all _gbox siblings outerHeight
        var totalHeight = 0;
        outerParent.children().each(function () {
            if (!$(this).is('div[id ^= "gbox_"]')) {
                totalHeight += $(this).outerHeight(true);
            }
        });
        return totalHeight;
    }
});