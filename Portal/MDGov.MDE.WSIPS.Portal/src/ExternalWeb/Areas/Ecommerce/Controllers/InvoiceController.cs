﻿using iTextSharp.text.pdf;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Ecommerce.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Ecommerce.Controllers
{
    [Authorize]
    public partial class InvoiceController : Controller
    {
        private readonly IUpdatableService<ContactSale> _invoiceService;
        private readonly IService<ContactSaleGridModel> _invoiceGridModelService;
        private readonly IUpdatableService<Message> _messageService;
        private readonly IService<Contact> _contactService;

        public InvoiceController(
            IUpdatableService<ContactSale> invoiceService,
            IService<ContactSaleGridModel> invoiceGridModelService,
            IUpdatableService<Message> messageService,
            IService<Contact> contactService)
        {
            _invoiceService = invoiceService;
            _invoiceGridModelService = invoiceGridModelService;
            _messageService = messageService;
            _contactService = contactService;
        }

        [HttpGet]
        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public virtual JsonResult Data(GridCommand command, string permitNumber)
        {
            var filters = new List<DynamicFilter>();

            filters.Add(new DynamicFilter("ContactId == @0", SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId)));

            Dictionary<string, string> propertyMap = new Dictionary<string, string>
            {
                { "invoicenumber", "Id" },
                { "createdby", "CreatedBy" },
                { "invoiceamount", "InvoiceTotal" },
                { "invoicedate", "CreatedDate" }
                //{ "status", "LU_InvoiceStatus.Description" }
            };

            var count = _invoiceGridModelService.Count(filters);
            if (count == 0)
            {
                return Json(new GridData
                {
                    Data = null,
                    PageTotal = 0,
                    CurrentPage = 0,
                    RecordCount = 0
                }, JsonRequestBehavior.AllowGet);
            }

            var invoices = _invoiceGridModelService.GetRange(command.Skip(), command.PageSize, command.BuildSort(propertyMap) ?? "CreatedDate desc", filters);

            var pageTotal = (int)Math.Ceiling((float)count / (float)command.PageSize);

            return Json(new GridData
            {
                Data = invoices,
                PageTotal = pageTotal,
                CurrentPage = command.Page,
                RecordCount = count
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual FileResult Print(int id, string filename)
        {
            var contactId = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);

            var invoice = _invoiceService.GetById(id, "Product.LU_ProductType");
            if (invoice == null || invoice.ContactId != contactId)
            {
                return null;
            }

            var invoiceContact = _contactService.GetById(invoice.ContactId, "LU_State");
            if (invoiceContact == null)
            {
                return null;
            }

            var templatePath = ConfigurationManager.AppSettings["DocumentTemplateFolderPath"];
            var invoiceTemplate = Server.MapPath(Path.Combine(templatePath, "Invoice.pdf"));

            var responseStream = new MemoryStream();

            using (var invoiceFileStream = new FileStream(invoiceTemplate, FileMode.Open))
            {
                // Open Invoice Template
                var pdfReader = new PdfReader(invoiceFileStream);

                // PdfStamper, which will create
                var stamper = new PdfStamper(pdfReader, responseStream);

                var form = stamper.AcroFields;
                foreach (var fieldKey in form.Fields.Keys)
                {
                    if (fieldKey.Contains("{INVOICE_NUMBER}"))
                    {
                        form.SetField(fieldKey, invoice.Id.ToString());
                    }
                    else if (fieldKey.Contains("{INVOICE_DATE}"))
                    {
                        form.SetField(fieldKey, invoice.CreatedDate.ToShortDateString());
                    }
                    else if (fieldKey.Contains("{DUE_DATE}"))
                    {
                        form.SetField(fieldKey, invoice.CreatedDate.AddDays(30).ToShortDateString());
                    }
                    else if (fieldKey.Contains("{INVOICE_AMOUNT}"))
                    {
                        form.SetField(fieldKey, invoice.Amount.ToString("C"));
                    }
                    else if (fieldKey.Contains("{INVOICE_DESCRIPTION}"))
                    {
                        form.SetField(fieldKey, (invoice.Product.Description ?? "").ToUpper());
                    }
                    else if (fieldKey.Contains("{CONTACT_ID}"))
                    {
                        form.SetField(fieldKey, invoice.ContactId.ToString());
                    }
                    else if (fieldKey.Contains("{PERMIT_NUMBER}"))
                    {
                        form.SetField(fieldKey, invoice.PermitName);
                    }
                    else if (fieldKey.Contains("{MDE_CONTACT}"))
                    {
                        form.SetField(fieldKey, "Water Supply Program, 410-537-3714".ToUpper());
                    }
                    else if (fieldKey.Contains("{PCA_NUM}"))
                    {
                        form.SetField(fieldKey, invoice.Product.LU_ProductType.PcaCode.ToString());
                    }
                    else if (fieldKey.Contains("{AGY_OBJ}"))
                    {
                        form.SetField(fieldKey, invoice.Product.LU_ProductType.AobjCode.ToString());
                    }
                    else if (fieldKey.Contains("{COMPANY_NAME}"))
                    {
                        form.SetField(fieldKey, (invoiceContact.BusinessName ?? "").ToUpper());
                    }
                    else if (fieldKey.Contains("{CONTACT_NAME}"))
                    {
                        form.SetField(fieldKey, (invoiceContact.FirstName ?? "").ToUpper() + " " + (invoiceContact.LastName ?? "").ToUpper());
                    }
                    else if (fieldKey.Contains("{CONTACT_STREETADDRESS}"))
                    {
                        form.SetField(fieldKey, (invoiceContact.Address1 ?? "").ToUpper());
                    }
                    else if (fieldKey.Contains("{CONTACT_CITYSTATEZIP}"))
                    {
                        form.SetField(fieldKey, (invoiceContact.City ?? "").ToUpper() + ", " + (invoiceContact.LU_State.Key ?? "").ToUpper() + " " + (invoiceContact.ZipCode ?? "").ToUpper());
                    }
                }

                // "Flatten" the form so it wont be editable/usable anymore
                stamper.FormFlattening = true;

                stamper.Close();
                pdfReader.Close();
            }

            var cd = new ContentDisposition
            {
                FileName = "Invoice " + invoice.Id + ".pdf",
                Inline = true
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(responseStream.ToArray(), "application/pdf");
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ContentResult SessionVerification(SessionVerification obj)
        {
            var result = new StringBuilder();

            var invoice = _invoiceService.GetById(obj.remittance_id, "Product.LU_ProductType");

            if (invoice == null || invoice.PaidDate.HasValue)
            {
                result.Append("continue_processing=false");

                return Content(result.ToString(), "text/plain");
            }

            var contact = _contactService.GetById(invoice.ContactId, "LU_State");

            if (contact == null)
            {
                result.Append("continue_processing=false");

                return Content(result.ToString(), "text/plain");
            }

            result.Append("continue_processing=true");

            result.Append("&billing_firstname=" + contact.FirstName);
            result.Append("&billing_lastname=" + contact.LastName);
            result.Append("&billing_address=" + contact.Address1);
            result.Append("&billing_city=" + contact.City);
            result.Append("&billing_state=" + contact.LU_State.Description);
            result.Append("&billing_zip=" + contact.ZipCode);

            result.Append("&amount=" + invoice.Amount);

            /***************** V+Relay User defined fields *****************
             * Name          | WSIPS Field                   |  Required?  *
             * ----------------------------------------------------------- *
             * user_defined1 | Program Cost Account (PCA)    |     Yes     *
             * user_defined2 | Agency Object (AOBJ)          |     Yes     *
             * user_defined3 | Fee Name                      |     Yes     *
             * user_defined4 | Permit Number                 |     Yes     *
             * user_defined5 | Invoice Number                |     Yes     *
             ***************************************************************/

            result.Append("&user_defined1=" + invoice.Product.LU_ProductType.PcaCode.ToString("D5"));
            result.Append("&user_defined2=" + invoice.Product.LU_ProductType.AobjCode.ToString("D4"));
            result.Append("&user_defined3=" + invoice.Product.LU_ProductType.Description);
            result.Append("&user_defined4=" + invoice.PermitName);
            result.Append("&user_defined5=" + invoice.Id);

            return Content(result.ToString(), "text/plain");
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ContentResult SessionNotification(SessionNotification obj)
        {
            var invoice = _invoiceService.GetById(obj.remittance_id);

            if (invoice != null && !invoice.PaidDate.HasValue)
            {
                if (obj.transaction_status == 0) // Success
                {
                    // This could be the second time that they are completing a payment
                    // (e.g. it failed the first time, now they are trying again)
                    // so we need to reset the status code
                    invoice.PaymentFailCode = null;

                    // Finalize our invoice
                    invoice.ContactSaleStatusId = 2; // Paid
                    invoice.IsOnlinePayment = true;
                    invoice.PaidDate = DateTime.Now;
                }
                else
                {
                    invoice.PaymentFailCode = obj.transaction_status;
                }

                invoice.LastModifiedBy = "Payment Service";
                _invoiceService.Save(invoice);
            }

            return Content("success=true", "text/plain");
        }
    }
}
