﻿using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Payment.Model;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Ecommerce.Controllers
{
    [Authorize]
    public partial class PaymentConfirmationController : Controller
    {
        private readonly IUpdatableService<ContactSale> _invoiceService;

        public PaymentConfirmationController(
            IUpdatableService<ContactSale> invoiceService)
        {
            _invoiceService = invoiceService;
        }

        [HttpGet]
        public virtual ActionResult Index(
            string application_id,
            string remittance_id,
            string security_id)
        {
            int invoiceId;
            if (int.TryParse(remittance_id, out invoiceId))
            {
                var invoice = _invoiceService.GetById(invoiceId);

                if (invoice != null && invoice.PaidDate.HasValue)
                {
                    ViewBag.IsSuccess = true;

                    return View();
                }
            }

            ViewBag.IsSuccess = false;

            return View();
        }
    }
}
