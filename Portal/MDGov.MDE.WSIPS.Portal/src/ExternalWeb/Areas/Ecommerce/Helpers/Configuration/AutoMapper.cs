﻿using AutoMapper;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Ecommerce.Helpers.Configuration
{
    public class EcommerceAreaAutoMapperProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "Ecommerce";
            }
        }

        protected override void Configure()
        {
        }
    }
}