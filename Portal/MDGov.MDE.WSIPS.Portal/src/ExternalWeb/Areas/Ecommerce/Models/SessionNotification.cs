﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Ecommerce.Models
{
    public class SessionNotification
    {
        public string application_id { get; set; }
        public string message_version { get; set; }
        public int remittance_id { get; set; }
        public string security_id { get; set; }
        public short transaction_status { get; set; }
        public string payment_type { get; set; }
        public string card_type { get; set; }
        public string partial_card_number { get; set; }
        public string routing_transit_number { get; set; }
        public string partial_acct_number { get; set; }
        public string avs_response { get; set; }
        public string fail_code { get; set; }
        public string amount { get; set; }
        public string convenience_fee_collected { get; set; }
        public string convenience_fee_amount { get; set; }
        public string total_amount { get; set; }
        public string transaction_id { get; set; }
        public string user_defined1 { get; set; }
        public string user_defined2 { get; set; }
        public string user_defined3 { get; set; }
        public string user_defined4 { get; set; }
        public string user_defined5 { get; set; }
        public string email_address { get; set; }
    }
}