﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Ecommerce.Models
{
    public class SessionVerification
    {
        public string application_id { get; set; }
        public string message_version { get; set; }
        public int remittance_id { get; set; }
        public string security_id { get; set; }
    }
}