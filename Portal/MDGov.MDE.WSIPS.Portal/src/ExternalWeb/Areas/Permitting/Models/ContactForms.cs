﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models
{
    public class ContactForms
    {
        public List<ContactForm> ContactForm { get; set; }
    }
}