﻿using System.ComponentModel.DataAnnotations;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes.Filters;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models
{
    public class PumpageReportDetailsForm
    {
        /// <summary>
        /// Pumpage Report Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Permit Condition Id
        /// </summary>
        public int PermitConditionId { get; set; }

        /// <summary>
        /// Condition Compliance Id
        /// </summary>
        public int ConditionComplianceId { get; set; }

        /// <summary>
        /// Report Name
        /// </summary>
        [StringLength(255)]
        [Required(ErrorMessage = "Report Name is required")]
        public string Name { get; set; }

        /// <summary>
        /// Water Withdrawal Estimate Id
        /// </summary>
        [Required(ErrorMessage = "Withdrawal Estimate Method is required")]
        public int? WaterWithdrawalEstimateId { get; set; }

        /// <summary>
        /// Water Withdrawal Estimate Method (if other)
        /// </summary>
        [RequiredIf("WaterWithdrawalEstimateId", 9, ErrorMessage = "Please specify Other Withdrawal Estimate Method")]
        public string OtherWaterWithdrawalMethod { get; set; }

        /// <summary>
        /// Crop Type Id
        /// </summary>
        public int? CropTypeId { get; set; }

        /// <summary>
        /// Crop Type Description
        /// </summary>
        public string CropDescription { get; set; }

        [RequiredIf("CropTypeId", 12, ErrorMessage = "Please specify Other Field Crop")]
        public string OtherCropDescription { get; set; }

        /// <summary>
        /// Report Year
        /// </summary>
        public string ReportYear { get; set; }

        /// <summary>
        /// Pumpage Report Type Id
        /// </summary>
        public int? PumpageReportTypeId { get; set; }

        /// <summary>
        /// Submitted by Name
        /// </summary>
        [StringLength(100)]
        [Required(ErrorMessage = "Submitted By Name is required")]
        public string SubmittedBy { get; set; }

        /// <summary>
        /// Submitted by Phone
        /// </summary>
        [StringLength(100)]
        [Required(ErrorMessage = "Submitted By Phone is required")]
        public string Phone { get; set; }

        /// <summary>
        /// Received Date
        /// </summary>
        public DateTime? ReceivedDate { get; set; }

        /// <summary>
        /// Submitted Date
        /// </summary>
        public DateTime? SubmittedDate { get; set; }

        /// <summary>
        /// Report Details
        /// </summary>
        public virtual ICollection<PumpageReportDetail> PumpageReportDetails { get; set; }

        /// <summary>
        /// Is report a resubmission
        /// </summary>
        public bool IsResumbission { get; set; }
    }
}