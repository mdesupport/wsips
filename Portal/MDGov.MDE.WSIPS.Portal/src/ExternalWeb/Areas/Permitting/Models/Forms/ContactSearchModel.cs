﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class ContactSearchModel
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string EmailAddress { get; set; }
        public string BusinessName { get; set; }
        public int ContactId { get; set; }
        public int? UserId { get; set; }
    }
}