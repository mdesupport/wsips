﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwCommercialDrinkingOrSanitaryForm : StandardQuestionsForm
    {
        public int? UsePercentage { get; set; }

        public Nullable<int> NoOfLotsOrUnits { get; set; }

        public IEnumerable<SelectListItem> FacilityList { get; set; }
        public Nullable<int> TypeOfFacilityId { get; set; }

        public Nullable<int> AreaOrSquareFootage { get; set; }
        public Nullable<int> NoOfEmployees { get; set; }
        public Nullable<int> AvgNumberOfCustomersPerDay { get; set; }

        public IEnumerable<SelectListItem> RestaurantTypeList { get; set; }
        public Nullable<int> RestaurantTypeId { get; set; }

        public Nullable<int> NoOfSeats { get; set; }
        public Nullable<int> NoOfSlips { get; set; }
    }
}