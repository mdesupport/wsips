﻿using System.Data.Spatial;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Attributes;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [SpatialFilter]
    public class MapExtentFilterForm
    {
        public decimal? XMin { get; set; }

        public decimal? YMin { get; set; }

        public decimal? XMax { get; set; }

        public decimal? YMax { get; set; }

        public int? SRID { get; set; }

        public bool IsApplied { get; set; }

        public MapExtentFilter BuildFilter()
        {
            if (IsApplied)
            {
                var mapExtentFilter = new MapExtentFilter();

                mapExtentFilter.XMin = XMin.Value;
                mapExtentFilter.YMin = YMin.Value;
                mapExtentFilter.XMax = XMax.Value;
                mapExtentFilter.YMax = YMax.Value;
                mapExtentFilter.SRID = SRID.Value;

                return mapExtentFilter;
            }

            return null;
        }
    }
}