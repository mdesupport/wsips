﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using MDGov.MDE.Common.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Attributes;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class DashboardFilterForm
    {
        public DashboardFilterForm()
        {
        }

        public DashboardFilterForm(FilterTemplate filterTemplate)
        {
            SelectedFilterTemplate = filterTemplate;

            switch (filterTemplate)
            {
                case FilterTemplate.DefaultAuthenticated:
                    AssociatedWithMe = true;
                    PermitCategoryFilter = ObjectFactory.GetInstance<PermitCategoryFilterForm>();
                    PermitStatusFilter = ObjectFactory.GetInstance<PermitStatusFilterForm>();
                    WaterWithdrawalPurposeCategoryFilter = ObjectFactory.GetInstance<WaterWithdrawalPurposeCategoryFilterForm>();
                    PermitCountyFilter = ObjectFactory.GetInstance<PermitCountyFilterForm>();
                    AquiferFilter = ObjectFactory.GetInstance<AquiferFilterForm>();
                    WaterWithdrawalSourceFilter = ObjectFactory.GetInstance<WaterWithdrawalSourceFilterForm>();
                    PermitIssuanceDateFilter = ObjectFactory.GetInstance<PermitIssuanceDateFilterForm>();
                    PermitExpirationDateFilter = ObjectFactory.GetInstance<PermitExpirationDateFilterForm>();
                    break;
                case FilterTemplate.Public:
                    PermitCategoryFilter = ObjectFactory.GetInstance<PermitCategoryFilterForm>();
                    PermitStatusFilter = ObjectFactory.GetInstance<PermitStatusFilterForm>();
                    WaterWithdrawalPurposeCategoryFilter = ObjectFactory.GetInstance<WaterWithdrawalPurposeCategoryFilterForm>();
                    PermitCountyFilter = ObjectFactory.GetInstance<PermitCountyFilterForm>();
                    WaterWithdrawalSourceFilter = ObjectFactory.GetInstance<WaterWithdrawalSourceFilterForm>();
                    PermitIssuanceDateFilter = ObjectFactory.GetInstance<PermitIssuanceDateFilterForm>();
                    PermitExpirationDateFilter = ObjectFactory.GetInstance<PermitExpirationDateFilterForm>();
                    break;
                default:
                    break;
            }
        }

        public FilterTemplate SelectedFilterTemplate { get; set; }

        [Display(Name = "Only those associated with me")]
        public bool? AssociatedWithMe { get; set; }

        [Display(Name = "Only the most recent version")]
        public bool? MostRecentVersion { get; set; }

        public PermitCategoryFilterForm PermitCategoryFilter { get; set; }

        public PermitStatusFilterForm PermitStatusFilter { get; set; }

        public WaterWithdrawalPurposeCategoryFilterForm WaterWithdrawalPurposeCategoryFilter { get; set; }

        public PermitCountyFilterForm PermitCountyFilter { get; set; }

        public AquiferFilterForm AquiferFilter { get; set; }

        public WaterWithdrawalSourceFilterForm WaterWithdrawalSourceFilter { get; set; }

        public PermitIssuanceDateFilterForm PermitIssuanceDateFilter { get; set; }

        public PermitExpirationDateFilterForm PermitExpirationDateFilter { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();

            if (AssociatedWithMe.HasValue && AssociatedWithMe.Value)
            {
                var currentUserContactId = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);
                filters.Add(new DynamicFilter("PermitContacts.Any(ContactId==@0 && @1.Contains(outerIt.ContactTypeId) && IsPrimary)", currentUserContactId, new[] { 8, 9, 10, 16, 27 }));

                // if logged in user has contactypeid of 9, then set filter to category 2 and permit status of 4
                if (SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactTypeId) == 9)
                {
                    if (PermitCategoryFilter == null)
                    {
                        PermitCategoryFilter = new PermitCategoryFilterForm() { SelectedPermitCategoryIds = new List<int>() { 2 } };
                    }
                    else
                    {
                        PermitCategoryFilter.SelectedPermitCategoryIds = new List<int>() { 2 };
                    }

                    if (PermitStatusFilter == null)
                    {
                        PermitStatusFilter = new PermitStatusFilterForm() { SelectedPermitStatusId = 4 };
                    }
                    else
                    {
                        PermitStatusFilter.SelectedPermitStatusId = 4;
                    }
                }

                // if logged in user has contactypeid of 10, then set filter to category 2 and permit status of 14
                if (SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactTypeId) == 10)
                {
                    if (PermitCategoryFilter == null)
                    {
                        PermitCategoryFilter = new PermitCategoryFilterForm() { SelectedPermitCategoryIds = new List<int>() { 2 } };
                    }
                    else
                    {
                        PermitCategoryFilter.SelectedPermitCategoryIds = new List<int>() { 2 };
                    }

                    if (PermitStatusFilter == null)
                    {
                        PermitStatusFilter = new PermitStatusFilterForm() { SelectedPermitStatusId = 14 };
                    }
                    else
                    {
                        PermitStatusFilter.SelectedPermitStatusId = 14;
                    }
                }
            }
            else
            {
                filters.Add(new DynamicFilter("(PermitStatusId == 46) || ((LU_PermitStatus.PermitCategoryId == 2 || LU_PermitStatus.PermitCategoryId == 3 || LU_PermitStatus.PermitCategoryId == 4 ) && (PermitTypeId == 2 || PermitTypeId == 4))"));
            }

            foreach (var prop in this.GetType().GetProperties().Where(prop => prop.PropertyType.GetInterface(typeof(IDashboardFilter).Name, false) != null && !prop.PropertyType.GetCustomAttributes(typeof(SpatialFilterAttribute), false).Any()))
            {
                var filterClass = prop.GetValue(this);

                if (filterClass != null)
                {
                    var filter = (filterClass as IDashboardFilter).BuildFilter();

                    if (filter.Count() > 0)
                    {
                        filters.AddRange(filter);
                    }
                }
            }

            return filters;
        }

        public void Rehydrate()
        {
            foreach (var prop in this.GetType().GetProperties().Where(prop => prop.PropertyType.GetInterface(typeof(IDashboardFilter).Name, false) != null))
            {
                var filterClass = prop.GetValue(this);

                if (filterClass != null)
                {
                    (filterClass as IDashboardFilter).Rehydrate(SelectedFilterTemplate);
                }
            }
        }
    }
}