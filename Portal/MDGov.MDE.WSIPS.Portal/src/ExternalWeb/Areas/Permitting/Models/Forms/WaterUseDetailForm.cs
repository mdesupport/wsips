﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class WaterUseDetailForm
    {
        public WaterUseDetailForm()
        {
            PermitWwLivestockForm = new PermitWwLivestockUseDetailGroupForm();
            OtherLivestockWateringForm = new PermitWwLivestockUseDetailGroupForm();
            DairyAnimalWateringForm = new PermitWwLivestockUseDetailGroupForm();
            PoultryWateringFoggerForm = new PoultryWateringUseDetailGroupForm();
            PoultryWateringEvaporativeForm = new PoultryWateringUseDetailGroupForm();
            CropUseForm = new CropUseDetailGroupForm();
            NurseryStockIrrigationForm = new PermitWwIrrigationUseDetailGroupForm();
            SodIrrigationForm = new PermitWwIrrigationUseDetailGroupForm();
            PrivateWaterSuppilerForm = new PermitWwPrivateWaterSupplierGroupForm();
            IrrigationUndefinedForm = new PermitWwNonAgricultureIrrigationForm();
            LawnAndParkIrrigationForm = new PermitWwNonAgricultureIrrigationForm();
            SmallIntermitentIrrigationForm = new PermitWwNonAgricultureIrrigationForm();
            GolfCourseIrrigationForm = new PermitWwGolfCourseIrrigationForm();
            PermitWwCommercialDrinkingOrSanitaryForm = new PermitWwCommercialDrinkingOrSanitaryForm();
            PermitWwAquacultureAquariumForm = new PermitWwAquacultureAquariumForm();
            PermitWwConstructionDewateringForm = new PermitWwConstructionDewateringForm();
            PermitWwEducationalDrinkingOrsanitaryForm = new PermitWwEducationalDrinkingOrsanitaryForm();
            PermitWwFarmPotableSuppliesForm = new PermitWwFarmPotableSuppliesForm();
            PermitWwFoodProcessingForm = new PermitWwFoodProcessingForm();
            PermitWwReligiousDringkingOrSanitaryForm = new PermitWwReligiousDringkingOrSanitaryForm();
            PermitWwWildlifePondForm = new PermitWwWildlifePondForm();
        }

        public PermitWwLivestockUseDetailGroupForm PermitWwLivestockForm { get; set; }

        public PermitWwLivestockUseDetailGroupForm OtherLivestockWateringForm { get; set; }

        public PermitWwLivestockUseDetailGroupForm DairyAnimalWateringForm { get; set; }

        public PoultryWateringUseDetailGroupForm PoultryWateringFoggerForm { get; set; }

        public PoultryWateringUseDetailGroupForm PoultryWateringEvaporativeForm { get; set; }

        public CropUseDetailGroupForm CropUseForm { get; set; }

        public PermitWwIrrigationUseDetailGroupForm NurseryStockIrrigationForm { get; set; }

        public PermitWwIrrigationUseDetailGroupForm SodIrrigationForm { get; set; }

        public PermitWwPrivateWaterSupplierGroupForm PrivateWaterSuppilerForm { get; set; }

        public PermitWwNonAgricultureIrrigationForm IrrigationUndefinedForm { get; set; }

        public PermitWwNonAgricultureIrrigationForm LawnAndParkIrrigationForm { get; set; }

        public PermitWwNonAgricultureIrrigationForm SmallIntermitentIrrigationForm { get; set; }

        public PermitWwGolfCourseIrrigationForm GolfCourseIrrigationForm { get; set; }

        public PermitWwCommercialDrinkingOrSanitaryForm PermitWwCommercialDrinkingOrSanitaryForm { get; set; }

        public PermitWwAquacultureAquariumForm PermitWwAquacultureAquariumForm { get; set; }

        public PermitWwConstructionDewateringForm PermitWwConstructionDewateringForm { get; set; }

        public PermitWwEducationalDrinkingOrsanitaryForm PermitWwEducationalDrinkingOrsanitaryForm { get; set; }

        public PermitWwFarmPotableSuppliesForm PermitWwFarmPotableSuppliesForm { get; set; }

        public PermitWwFoodProcessingForm PermitWwFoodProcessingForm { get; set; }

        public PermitWwReligiousDringkingOrSanitaryForm PermitWwReligiousDringkingOrSanitaryForm { get; set; }

        public PermitWwWildlifePondForm PermitWwWildlifePondForm { get; set; }
    }
}