﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class CommunicationMethodForm
    {
        public string CommunicationValue { get; set; }

        public int? SelectedCommunicationMethodId { get; set; }
    }
}