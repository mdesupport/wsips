﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class CountyReviewAndApprovalForm
    {
        public int PermitId { get; set; }

        public bool IsChecked { get; set; }

        public string Comment { get; set; }

        public int CommentId { get; set; }

        public bool IsPastSubmission { get; set; }

        public string CountyOfficialSignOffBy { get; set; }

        public DateTime? CountyOfficialSignOffDate { get; set; }
    }
}