﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwNonAgricultureIrrigationForm : StandardQuestionsForm
    {
        public string Title { get; set; }

        public int? UsePercentage { get; set; }

        [StringLength(500)]
        public string PurposeDescription { get; set; }

        [Required]
        public Nullable<int> NoOfIrrigatedAcres { get; set; }

        public Nullable<int> NoOfDaysPerWeekIrrigated { get; set; }
    }
}