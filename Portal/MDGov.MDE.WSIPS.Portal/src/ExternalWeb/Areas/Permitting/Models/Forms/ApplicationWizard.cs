﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class ApplicationWizard
    {
        public ApplicationWizard()
        {
            this.PermitStatusDetailForm = new PermitStatusDetailForm();
        }

        public Nullable<int> ApplicationTypeId { get; set; }

        public Nullable<int> RefId { get; set; }

        public int PermitId { get; set; }

        public int PermitStatusId { get; set; }

        public string CurrentStep { get; set; }

        public int PermitCategoryId { get; set; }

        #region Application Identification

        public bool IsWaterUser { get; set; }

        public bool IsLandOwner { get; set; }

        public bool IsConsultant { get; set; }

        public int PermitIssuedTo { get; set; }

        #endregion

        #region Contact

        public ContactForm Contact { get; set; }

        public List<ContactForm> Contacts { get; set; }

        #endregion

        [Required(ErrorMessage = "Water use description is required.")]
        public string UseDescription { get; set; }

        public ContactSearchForm ContactSearchForm { get; set; }

        #region Water use Category and Type

        public bool NotSureWhatTheWaterUseTypeIs { get; set; }

        //Comma separated water use purpose ids
        public string SelectedWaterUsePurposeIds { get; set; }

        //Saved PermitWaterWithdrawal purposes Id
        public List<PermitWaterWithdrawalPurpose> SavedWaterWithdrawalPurpose { get; set; }

        #endregion

        #region Water Use Detail

        public WaterUseDetailForm WaterUseDetailForm { get; set; }

        #endregion

        #region waste water treatment and disposal
        public List<WastewaterTreatmentAndDisposalForm> WastewaterTreatmentDisposalForm { get; set; }

        #endregion

        #region Used by Permit Detail Page
        public PermitStatusDetailForm PermitStatusDetailForm { get; set; }

        public bool IsExternalApplicaion { get; set; }
        #endregion
    }
}