﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Spatial;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Attributes;
using Newtonsoft.Json.Linq;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [SpatialFilter]
    public class PermitDistanceFilterForm
    {
        public decimal? PointX { get; set; }

        public decimal? PointY { get; set; }

        [Display(Name = "SRID")]
        public int? SRID { get; set; }

        [Display(Name = "Distance")]
        public float? Distance { get; set; }

        public bool IsApplied { get; set; }

        public PermitDistanceFilter BuildFilter()
        {
            if (IsApplied)
            {
                var permitDistanceFilter = new PermitDistanceFilter();

                permitDistanceFilter.PointX = PointX.Value;
                permitDistanceFilter.PointY = PointY.Value;
                permitDistanceFilter.SRID = SRID.Value;
                permitDistanceFilter.Distance = Distance.Value;

                return permitDistanceFilter;
            }

            return null;
        }
    }
}