﻿using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class DashboardSearchForm
    {
        public DashboardMode DisplayMode { get; set; }

        public DashboardPermit SelectedPermit { get; set; }

        public DashboardFilterForm FilterForm { get; set; }

        public DashboardSpatialFilterForm SpatialFilterForm { get; set; }

        public void Rehydrate()
        {
            if (FilterForm != null)
            {
                FilterForm.Rehydrate();
            }
        }
    }
}