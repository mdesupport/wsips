﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitWwIrrigationUseDetailGroupForm
    {
        public int? UsePercentage { get; set; }

        public List<PermitWwIrrigationForm> IrrigationForms { get; set; }
    }
}