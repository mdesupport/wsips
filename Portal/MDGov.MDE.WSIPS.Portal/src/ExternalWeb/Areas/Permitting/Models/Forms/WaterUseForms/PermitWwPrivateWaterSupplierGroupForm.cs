﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitWwPrivateWaterSupplierGroupForm
    {
        public int? UsePercentage { get; set; }

        public List<PermitWwPrivateWaterSupplierForm> PrivateWaterSupplierForms { get; set; }
    }
}