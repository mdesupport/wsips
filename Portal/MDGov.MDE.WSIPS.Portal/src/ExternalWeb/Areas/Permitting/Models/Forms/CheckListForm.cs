﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class CheckListForm
    {
        public int RefId { get; set; }

        public int PermitStatusId { get; set; }

        public string CheckListCompleted { get; set; }

        public string CompletedByAndDate { get; set; }

        public IEnumerable<LU_CheckListAnswer> AnswerOptions { get; set; }

        public IEnumerable<CheckListQuestion> ChecklistQuestions { get; set; }

        public IEnumerable<CheckListAnswer> ChecklistAnswers { get; set; }
    }
}