﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitReviewForm
    {
        public PermitReviewForm()
        {
            this.PermitStatusDetail = new PermitStatusDetailForm();

            this.CheckListForm = new CheckListForm();

            this.HasPermissionToFinalizePermit = "false";

        }

        public PermitStatusDetailForm PermitStatusDetail { get; set; }

        public CheckListForm CheckListForm { get; set; }

        public int RefId { get; set; }

        public int PermitStatusId { get; set; }

        public string PermitType { get; set; }

        public string HasPermissionToFinalizePermit { get; set; }
    }
}