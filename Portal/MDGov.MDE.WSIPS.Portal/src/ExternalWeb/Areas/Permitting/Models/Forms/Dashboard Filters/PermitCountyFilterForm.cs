﻿using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using StructureMap;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitCountyFilterForm : IDashboardFilter
    {
        [Display(Name = "County")]
        public List<int> SelectedPermitCountyIds { get; set; }

        [XmlIgnore]
        public SelectList CountyList { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();
            if (SelectedPermitCountyIds != null && SelectedPermitCountyIds.Count() > 0 && SelectedPermitCountyIds.All(p => p != 0))
            {
                filters.Add(new DynamicFilter("PermitCounties.Any(@0.Contains(outerIt.CountyId))", SelectedPermitCountyIds));
            }

            return filters;
        }

        public void Rehydrate(FilterTemplate filterTemplate)
        {
            if (SelectedPermitCountyIds == null || SelectedPermitCountyIds.Count == 0)
            {
                SelectedPermitCountyIds = new List<int> { 0 };
            }

            CountyList = GetCountyList(ObjectFactory.GetInstance<IService<LU_County>>());
        }

        private SelectList GetCountyList(IService<LU_County> countyService)
        {
            var list = countyService.GetRange(0, int.MaxValue, "Sequence", new[] { new DynamicFilter("Active") }).Select(t => new SelectListItem { Text = t.Description, Value = t.Id.ToString() }).ToList();
            list.Insert(0, new SelectListItem { Text = "Display All", Value = "0" });

            return new SelectList(list, "Value", "Text");
        }
    }
}