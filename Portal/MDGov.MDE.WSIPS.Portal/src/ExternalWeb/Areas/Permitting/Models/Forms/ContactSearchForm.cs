﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class ContactSearchForm
    {
        public string SearchFirstName { get; set; }

        public string SearchLastName { get; set; }

        public string SearchBusinessName { get; set; }

        public string SearchEmailAddress { get; set; }
    }
}