﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitStatusDetailForm
    {
        public int RefId { get; set; }

        public int PermitStatusId { get; set; }

        public IEnumerable<LU_PermitStatus> PermitStatusNav { get; set; }

        public int PermitCategoryId { get; set; }

        public DateTime PermitStatusLastChangedDate { get; set; }

        public string PermitStatusDescription { get; set; }

    }
}