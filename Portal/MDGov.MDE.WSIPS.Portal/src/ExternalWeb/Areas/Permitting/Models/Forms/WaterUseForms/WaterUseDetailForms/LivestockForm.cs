﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class LivestockForm
    {
        public int Id { get; set; }

        public int PermitWaterWithdrawalPurposeId { get; set; }

        [StringLength(50)]
        [DataType(DataType.Text)]
        public string Livestock { get; set; }

        [RegularExpression(@"^\d+$", ErrorMessage = "Please enter a number.")]
        public Nullable<int> NumberOfLivestock { get; set; }

        public IEnumerable<SelectListItem> TypeOfLivestockList { get; set; }
        public bool HasDeleted { get; set; }
    }
}