﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class CropForm : PermitWwIrrigationForm
    {
        public IEnumerable<SelectListItem> CropTypeList { get; set; }
        public Nullable<int> CropId { get; set; }
        public IEnumerable<SelectListItem> CropYieldUnitList { get; set; }
        public Nullable<int> CropYieldUnitId { get; set; }
        public Nullable<int> CropYieldGoal { get; set; }
    }
}