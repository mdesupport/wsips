﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwGolfCourseIrrigationForm : StandardQuestionsForm
    {
        public int? UsePercentage { get; set; }

        public Nullable<int> NoOfIrrigatedAcres_teesOrGreens { get; set; }
        public Nullable<int> NoOfIrrigatedAcres_fairways { get; set; }
        public string TypeOfGrass_tees_greens { get; set; }
        public string TypeOfGrass_fairways { get; set; }

        public IEnumerable<SelectListItem> GolfIrrigationSystemTypeList { get; set; }
        public Nullable<int> GolfIrrigationSystemTypeId { get; set; }

        public IEnumerable<SelectListItem> IrrigationLayoutTypeList { get; set; }
        public Nullable<int> IrrigationLayoutTypeId { get; set; }

        public bool IsWellwaterPumpedIntoPond { get; set; }
    }
}