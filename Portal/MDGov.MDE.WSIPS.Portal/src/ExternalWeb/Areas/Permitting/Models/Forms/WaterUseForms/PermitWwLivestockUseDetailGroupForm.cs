﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitWwLivestockUseDetailGroupForm
    {
        public int? UsePercentage { get; set; }

        public List<LivestockForm> LivestockForms { get; set; }
    }
}