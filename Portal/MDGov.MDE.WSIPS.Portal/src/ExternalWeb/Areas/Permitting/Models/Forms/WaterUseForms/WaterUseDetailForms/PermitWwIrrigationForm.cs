﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwIrrigationForm
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int PermitWaterWithdrawalPurposeId { get; set; }

        [StringLength(500)]
        public string PurposeDescription { get; set; }

        [Required]
        public Nullable<int> IrrigatedAcres { get; set; }

        [Required]
        public Nullable<int> IrrigationSystemTypeId { get; set; }
        public IEnumerable<SelectListItem> IrrigationSystemTypeList { get; set; }

        [StringLength(500)]
        public string IrrigationSystemDescription { get; set; }

        public IrrigationType IrrigationType { get; set; }

        [Required]
        public Nullable<int> AverageGallonPerDayFromGroundWater { get; set; }
        [Required]
        public Nullable<int> MaximumGallonPerDayFromGroundWater { get; set; }
        [Required]
        public Nullable<int> AverageGallonPerDayFromSurfaceWater { get; set; }
        [Required]
        public Nullable<int> MaximumGallonPerDayFromSurfaceWater { get; set; }

        [StringLength(500)]
        public string EstimateDescription { get; set; }

        public bool HasDeleted { get; set; }

        public Nullable<int> NoOfLotsOrResidentialUnits { get; set; }
    }
}