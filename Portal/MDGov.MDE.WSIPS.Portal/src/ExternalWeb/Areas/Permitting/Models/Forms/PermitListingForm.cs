﻿
namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitListingForm
    {
        public int Id { get; set; }

        public string PermitNumber { get; set; }
        
    }
}