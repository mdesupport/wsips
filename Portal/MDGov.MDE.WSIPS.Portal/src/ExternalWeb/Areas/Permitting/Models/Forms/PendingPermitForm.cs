﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PendingPermitForm
    {
        public int PermitId { get; set; }

        [Required]
        public string PermitNumber { get; set; }

        public IEnumerable<SelectListItem> PermitTypeList { get; set; }

        public int PermitTypeId { get; set; }
    }
}