﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitStatusFilterForm : IDashboardFilter
    {
        [Display(Name = "Status")]
        public int SelectedPermitStatusId { get; set; }

        [XmlIgnore]
        public SelectList PermitStatusList { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();

            if (SelectedPermitStatusId > 0)
            {
                filters.Add(new DynamicFilter("PermitStatusId == " + SelectedPermitStatusId));
            }

            return filters;
        }

        public void Rehydrate(FilterTemplate filterTemplate)
        {
            PermitStatusList = GetPermitStatusList();
        }

        private SelectList GetPermitStatusList()
        {
            return new SelectList(new[] { new SelectListItem { Text = "Display All", Value = "0" } }, "Value", "Text");
        }
    }
}