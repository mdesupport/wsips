﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes.Filters;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class CustomConditionForm
    {

        /// <summary>
        /// Condition Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Permit Id
        /// </summary>
        public int PermitId { get; set; }

        /// <summary>
        /// Text
        /// </summary>
        [DisplayName("Condition Text")]
        [Required(ErrorMessage = "Condition Text is required")]
        public string ConditionText { get; set; }

        /// <summary>
        /// Sequence for sorting
        /// </summary>
        public string Sequence { get; set; }

        /// <summary>
        /// Self Reporting Required
        /// </summary>
        public bool RequiresSelfReporting { get; set; }

        /// <summary>
        /// Standard Condition Category
        /// </summary>
        [DisplayName("Condition Category")]
        [Required(ErrorMessage = "Condition Category is required")]
        public int StandardConditionTypeId { get; set; }

        /// <summary>
        /// Category
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Reporting Period Id
        /// </summary>
        [RequiredIf("RequiresSelfReporting", true, ErrorMessage = "Reporting Frequency is required")]
        public int? ConditionReportingPeriodId { get; set; }

        /// <summary>
        /// Number of Days (after Effective Date) for One Time Reporting Period
        /// </summary>
        public int? OneTimeReportDays { get; set; }

        /// <summary>
        /// Requires Report Validation
        /// </summary>
        public bool RequiresValidation { get; set; }

        /// <summary>
        /// Indicates that the condition is a standard condition, not editable view review screen
        /// </summary>
        public bool Standard { get; set; }

        /// <summary>
        /// Indicates that the condition requires Pumpage Reports. Another option would be Standard Reports.
        /// </summary>
        public bool PumpageReports { get; set; }

        /// <summary>
        /// Indicates Permit Condition Compliance Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// ComplianceReportingDueDate from ConditionCompliance
        /// </summary>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// Condition Compliance Id
        /// </summary>
        public int? ConditionComplianceId { get; set; }

    }
}