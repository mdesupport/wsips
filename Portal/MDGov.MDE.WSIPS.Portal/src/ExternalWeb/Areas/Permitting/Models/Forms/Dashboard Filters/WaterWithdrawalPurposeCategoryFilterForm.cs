﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class WaterWithdrawalPurposeCategoryFilterForm : IDashboardFilter
    {
        [Display(Name = "Use Code")]
        public List<int> SelectedWaterWithdrawalPurposeIds { get; set; }

        [XmlIgnore]
        public SelectList WaterWithdrawalPurposeCategoryList { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();
            if (SelectedWaterWithdrawalPurposeIds != null && SelectedWaterWithdrawalPurposeIds.Count() > 0 && SelectedWaterWithdrawalPurposeIds.All(p => p != 0))
            {
                filters.Add(new DynamicFilter("PermitWaterWithdrawalPurposes.Any(@0.Contains(outerIt.WaterWithdrawalPurposeId))", SelectedWaterWithdrawalPurposeIds));
            }

            return filters;
        }

        public void Rehydrate(FilterTemplate filterTemplate)
        {
            if (SelectedWaterWithdrawalPurposeIds == null || SelectedWaterWithdrawalPurposeIds.Count == 0)
            {
                SelectedWaterWithdrawalPurposeIds = new List<int> { 0 };
            }

            WaterWithdrawalPurposeCategoryList = GetWaterWithdrawalPurposeCategoryList(ObjectFactory.GetInstance<IService<LU_WaterWithdrawalPurposeCategory>>());
        }

        private SelectList GetWaterWithdrawalPurposeCategoryList(IService<LU_WaterWithdrawalPurposeCategory> waterWithdrawalPurposeCategoryService)
        {
            var list = waterWithdrawalPurposeCategoryService.GetAll("LU_WaterWithdrawalPurpose").SelectMany(t => t.LU_WaterWithdrawalPurpose).OrderBy(x => x.Description).Select(p => new SelectListItem { Text = p.Description, Value = p.Id.ToString() }).ToList();
            list.Insert(0, new SelectListItem { Text = "Display All", Value = "0" });

            return new SelectList(list, "Value", "Text");
        }
    }
}