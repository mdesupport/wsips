﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwEducationalDrinkingOrsanitaryForm
    {
        public int Id { get; set; }

        public int? UsePercentage { get; set; }

        public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }

        public string AverageGallonPerDayFromGroundWater { get; set; }
        public string MaximumGallonPerDayFromGroundWater { get; set; }
        public string AverageGallonPerDayFromSurfaceWater { get; set; }
        public string MaximumGallonPerDayFromSurfaceWater { get; set; }

        [StringLength(500)]
        public string EstimateDescription { get; set; }

        public IEnumerable<SelectListItem> EducationInstituteTypeList { get; set; }
        public Nullable<int> EducationInstituteTypeId { get; set; }

        public Nullable<int> NoOfStaff { get; set; }
        public Nullable<int> NoOfStudents { get; set; }
        public bool IsAthleticFieldIrrigation { get; set; }
        public Nullable<int> NoOfIrrigatedAcres { get; set; }
        public bool Chillers { get; set; }
        public Nullable<int> NoOfStaffWorkingMorethan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStaffWorkingLessthan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStudentsAttendingMorethan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStudentsAttendingLessthan20hrsPerWeek { get; set; }
    }
}