﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.Common.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public interface IDashboardFilter
    {
        IEnumerable<DynamicFilter> BuildFilter();

        void Rehydrate(FilterTemplate filterTemplate);
    }
}