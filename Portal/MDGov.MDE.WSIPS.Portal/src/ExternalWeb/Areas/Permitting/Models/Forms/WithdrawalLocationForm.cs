﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes.Filters;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class WithdrawalLocationForm
    {
        /// <summary>
        /// Withdrawal Location OBJECTID
        /// </summary>
        public int Id { get; set; }

        public int PermitId { get; set; }

        [Display(Name = "Withdrawal Source")]
        [Required(ErrorMessage = "Please Select a {0}")]
        public int WithdrawalSourceId { get; set; }

        public IEnumerable WithdrawalSourceList { get; set; }

        [Display(Name = "Water Withdrawal Type")]
        [Required(ErrorMessage = "Please Select a {0}")]
        public int WithdrawalTypeId { get; set; }

        public AttributedSelectList WithdrawalTypeList { get; set; }

        [Display(Name = "Water Body Name")]
        [RequiredIf("WithdrawalSourceId", 2, ErrorMessage = "Please Select a {0}")]
        public int? SelectedStateStreamId { get; set; }

        public SelectList StateStreamList { get; set; }

        [Display(Name = "Water Body Name")]
        [RequiredIf("SelectedStateStreamId", -1, ErrorMessage = "{0} is required")]
        public string OtherStreamName { get; set; }

        [Display(Name = "Location Of Intake")]
        public string IntakeLocation { get; set; }

        [Display(Name = "Is this an Existing Well?")]
        [RequiredIf("WaterWithdrawalTypeId", 1)]
        public bool IsExistingWell { get; set; }

        [Display(Name = "Well Depth (feet)")]
        public int? WellDepth { get; set; }

        [Display(Name = "Well Diameter (inches)")]
        public int? WellDiameter { get; set; }

        [Display(Name = "Well Tag No.")]
        public string WellTagNo { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }

        public double WithdrawalLocationX { get; set; }

        public double WithdrawalLocationY { get; set; }

        public int SpatialReferenceId { get; set; }

        [Display(Name = "Water Use Category and Type")]
        public IDictionary<PermitWaterWithdrawalPurposeForm, bool> PermitWaterWithdrawalPurposeList { get; set; }
    }
}