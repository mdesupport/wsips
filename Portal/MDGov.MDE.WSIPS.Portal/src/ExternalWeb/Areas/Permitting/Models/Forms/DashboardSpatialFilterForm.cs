﻿using System.Collections.Generic;
using System.Linq;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Attributes;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class DashboardSpatialFilterForm
    {
        public MapExtentFilterForm MapExtentFilter { get; set; }

        public PermitDistanceFilterForm PermitDistanceFilter { get; set; }

        public PermitFeatureFilterForm PermitFeatureFilter { get; set; }

        public DashboardSpatialFilters BuildFilter()
        {
            DashboardSpatialFilters filters = new DashboardSpatialFilters();

            if (MapExtentFilter != null && MapExtentFilter.IsApplied)
            {
                filters.MapExtentFilter = MapExtentFilter.BuildFilter();
            }

            if (PermitDistanceFilter != null && PermitDistanceFilter.IsApplied)
            {
                filters.PermitDistanceFilter = PermitDistanceFilter.BuildFilter();
            }

            if (PermitFeatureFilter != null && PermitFeatureFilter.IsApplied)
            {
                filters.PermitFeatureFilter = PermitFeatureFilter.BuildFilter();
            }

            return filters;
        }
    }
}