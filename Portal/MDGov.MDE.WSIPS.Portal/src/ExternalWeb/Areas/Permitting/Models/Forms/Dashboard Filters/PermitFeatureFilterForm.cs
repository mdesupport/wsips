﻿using System.ComponentModel.DataAnnotations;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Attributes;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [SpatialFilter]
    public class PermitFeatureFilterForm
    {
        public decimal? PointX { get; set; }

        public decimal? PointY { get; set; }

        [Display(Name = "SRID")]
        public int? SRID { get; set; }

        public FeatureTypes FeatureType { get; set; }

        public bool IsApplied { get; set; }

        public PermitFeatureFilter BuildFilter()
        {
            if (IsApplied)
            {
                var permitFeatureFilter = new PermitFeatureFilter();

                permitFeatureFilter.PointX = PointX.Value;
                permitFeatureFilter.PointY = PointY.Value;
                permitFeatureFilter.SRID = SRID.Value;
                permitFeatureFilter.FeatureType = FeatureType;

                return permitFeatureFilter;
            }

            return null;
        }
    }
}