﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class WaterWithdrawalSourceFilterForm : IDashboardFilter
    {
        [Display(Name = "Water Source")]
        public int WaterWithdrawalSourceId { get; set; }

        [XmlIgnore]
        public SelectList WaterWithdrawalSourceList { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();

            if (WaterWithdrawalSourceId == 1)
            {
                // Groundwater
                filters.Add(new DynamicFilter("PermitWithdrawalGroundwaters.Count > 0"));
            }
            else if (WaterWithdrawalSourceId == 2)
            {
                // Surface Water
                filters.Add(new DynamicFilter("PermitWithdrawalSurfaceWaters.Count > 0"));
            }

            return filters;
        }

        public void Rehydrate(FilterTemplate filterTemplate)
        {
            WaterWithdrawalSourceList = GetWaterWithdrawalSourceList(ObjectFactory.GetInstance<IService<LU_WaterWithdrawalSource>>());
        }

        private SelectList GetWaterWithdrawalSourceList(IService<LU_WaterWithdrawalSource> waterWithdrawalSourceService)
        {
            var list = waterWithdrawalSourceService.GetAll().Select(t => new SelectListItem { Text = t.Description, Value = t.Id.ToString() }).ToList();
            list.Insert(0, new SelectListItem { Text = "Display All", Value = "0" });

            return new SelectList(list, "Value", "Text", WaterWithdrawalSourceId);
        }
    }
}