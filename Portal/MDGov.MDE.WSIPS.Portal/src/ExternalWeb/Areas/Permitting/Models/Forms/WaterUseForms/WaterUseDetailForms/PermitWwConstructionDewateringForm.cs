﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwConstructionDewateringForm : StandardQuestionsForm
    {
        public int? UsePercentage { get; set; }

        public Nullable<int> AreaOfExcavation { get; set; }
        public Nullable<int> DepthOfExcavation { get; set; }
        public Nullable<int> NoOfPumps { get; set; }
        public Nullable<int> CapacityOfPumps { get; set; }
        public Nullable<int> HoursOfOperationPerDay { get; set; }

        public IEnumerable<SelectListItem> WaterRemovalExcavationTypeList { get; set; }
        public Nullable<int> WaterRemovalExcavationTypeId { get; set; }
    }
}