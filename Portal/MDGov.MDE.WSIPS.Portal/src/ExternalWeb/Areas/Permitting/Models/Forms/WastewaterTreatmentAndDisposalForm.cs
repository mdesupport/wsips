﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class WastewaterTreatmentAndDisposalForm
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsWastewaterDischarge { get; set; }
        public IEnumerable<SelectListItem> WaterDispersementTypeList { get; set; }
        public Nullable<int> WaterDispersementTypeId { get; set; }
        public string GroundwaterDischargeDiscription { get; set; }
        public string PermitDischargeNumber { get; set; }

        public int WaterWithdrawalPurposeId { get; set; } 
    }
}