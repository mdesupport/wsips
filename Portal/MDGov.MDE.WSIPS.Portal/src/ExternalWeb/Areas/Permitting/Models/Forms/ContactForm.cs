﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Attributes.Filters;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class ContactForm
    {
        public ContactForm()
        {
            PrimaryTelephoneNumber = new CommunicationMethodForm();
            AltTelephoneNumber = new CommunicationMethodForm();
            EmailAddress = new CommunicationMethodForm();
        }

        public int Id { get; set; }

        public int? UserId { get; set; }

        public bool Active { get; set; }

        public bool IsBusiness { get; set; }

        public ContactType ContactType { get; set; }
        
        [Display(Name = "First Name")]
        [StringLength(40)]
        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name or Initial ")]
        [StringLength(40)]
        public string MiddleInitial { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(50)]
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        /*[Display(Name = "Secondary Name")]
        [StringLength(100)]
        public string SecondaryName { get; set; }*/

        [Display(Name = "Business  Name")]
        [RequiredIf("IsBusiness", true, ErrorMessage = "Business Name is required")]
        [StringLength(120)]
        public string BusinessName { get; set; }

        [Display(Name = "Address (Line 1)")]
        [StringLength(100)]
        [Required(ErrorMessage = "Address Line1 is required")]
        public string Address1 { get; set; }

        [Display(Name = "Address (Line 2)")]
        [StringLength(100)]
        public string Address2 { get; set; }

        [Display(Name = "City")]
        [StringLength(50)]
        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "State is required")]
        public int? StateId { get; set; }

        public IEnumerable<SelectListItem> StateList { get; set; }

        [Display(Name = "Zip Code")]
        [StringLength(10)]
        [DataType(DataType.PostalCode)]
        [Required(ErrorMessage = "Zip Code is required")]
        public string ZipCode { get; set; }

        [Display(Name = "Primary Telephone No")]
        [RequiredIf("ContactType", 15, 6, ComparisonType = ComparisonType.NotEqual, ErrorMessage = "Primary telephone number and type are required")]
        public CommunicationMethodForm PrimaryTelephoneNumber { get; set; }

        [Display(Name = "Alt Telephone No")]
        public CommunicationMethodForm AltTelephoneNumber { get; set; }

        [Display(Name = "Email Address")]
        public CommunicationMethodForm EmailAddress { get; set; }

        public IEnumerable<SelectListItem> CommunicationMethodList { get; set; }
    }
}