﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class AquiferFilterForm : IDashboardFilter
    {
        [Display(Name = "Aquifer")]
        public List<int> SelectedAquiferIds { get; set; }

        [XmlIgnore]
        public SelectList AquiferList { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();
            if (SelectedAquiferIds != null && SelectedAquiferIds.Count() > 0 && SelectedAquiferIds.All(p => p != 0))
            {
                filters.Add(new DynamicFilter("@0.Contains(outerIt.AquiferId)", SelectedAquiferIds));
            }

            return filters;
        }

        public void Rehydrate(FilterTemplate filterTemplate)
        {
            if (SelectedAquiferIds == null || SelectedAquiferIds.Count == 0)
            {
                SelectedAquiferIds = new List<int> { 0 };
            }

            AquiferList = GetAquiferList(ObjectFactory.GetInstance<IService<LU_Aquifer>>());
        }

        private SelectList GetAquiferList(IService<LU_Aquifer> aquiferService)
        {
            var list = aquiferService.GetAll().OrderBy(x=> x.Description).Select(t => new SelectListItem { Text = t.Description, Value = t.Id.ToString() }).ToList();
            list.Insert(0, new SelectListItem { Text = "Display All", Value = "0" });

            return new SelectList(list, "Value", "Text");
        }
    }
}