﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class CropUseDetailGroupForm
    {
        public int? UsePercentage { get; set; }

        public int? TotalNumberOfAcres { get; set; }

        public List<CropForm> CropForms { get; set; }
    }
}