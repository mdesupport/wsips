﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MDGov.MDE.Common.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitExpirationDateFilterForm : IDashboardFilter
    {
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();
            if (StartDate.HasValue)
            {
                if (EndDate.HasValue)
                {
                    filters.Add(new DynamicFilter("@0 <= ExpirationDate && ExpirationDate <= @1", StartDate.Value, EndDate.Value));
                    
                }
                else
                {
                    filters.Add(new DynamicFilter("@0 <= ExpirationDate", StartDate.Value));
                }
            }
            else if (EndDate.HasValue)
            {
                filters.Add(new DynamicFilter("ExpirationDate <= @0", EndDate.Value));
            }

            return filters;
        }

        public void Rehydrate(FilterTemplate filterTemplate)
        {
        }
    }
}