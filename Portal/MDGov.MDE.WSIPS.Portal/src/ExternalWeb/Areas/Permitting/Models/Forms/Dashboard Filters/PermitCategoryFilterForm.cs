﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;
using System.Web;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    public class PermitCategoryFilterForm : IDashboardFilter
    {
        [Display(Name = "Type")]
        public List<int> SelectedPermitCategoryIds { get; set; }

        [XmlIgnore]
        public SelectList PermitCategoryList { get; set; }

        public IEnumerable<DynamicFilter> BuildFilter()
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();

            if (SelectedPermitCategoryIds != null && SelectedPermitCategoryIds.Count > 0 && SelectedPermitCategoryIds.All(p => p != 0))
            {
                filters.Add(new DynamicFilter("@0.Contains(outerIt.LU_PermitStatus.PermitCategoryId)", SelectedPermitCategoryIds));
            }

            return filters;
        }

        public void Rehydrate(FilterTemplate filterTemplate)
        {
            if (SelectedPermitCategoryIds == null || SelectedPermitCategoryIds.Count == 0)
            {
                SelectedPermitCategoryIds = new List<int> { 0 };
            }

            PermitCategoryList = GetPermitCategoryList(filterTemplate, ObjectFactory.GetInstance<IService<LU_PermitCategory>>());
        }

        private SelectList GetPermitCategoryList(FilterTemplate filterTemplate, IService<LU_PermitCategory> permitCategoryService)
        {
            IEnumerable<LU_PermitCategory> categories;
            if (filterTemplate == FilterTemplate.DefaultAuthenticated)
            {
                categories = permitCategoryService.GetRange(0, int.MaxValue, "Sequence", null);
            }
            else
            {
                categories = permitCategoryService.GetRange(0, int.MaxValue, "Sequence", new [] { new DynamicFilter("Id == 2 || Id == 4 || Id == 6") });
            }

            var list = categories.Select(t => new SelectListItem { Text = t.Description, Value = t.Id.ToString() }).ToList();
            list.Insert(0, new SelectListItem { Text = "Display All", Value = "0" });

            return new SelectList(list, "Value", "Text", SelectedPermitCategoryIds);
        }
    }
}