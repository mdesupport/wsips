﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class StandardQuestionsForm
    {
        public int Id { get; set; }
        public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }

        public Nullable<int> AverageGallonPerDayFromGroundWater { get; set; }
        public Nullable<int> MaximumGallonPerDayFromGroundWater { get; set; }
        public Nullable<int> AverageGallonPerDayFromSurfaceWater { get; set; }
        public Nullable<int> MaximumGallonPerDayFromSurfaceWater { get; set; }

        [StringLength(500)]
        public string EstimateDescription { get; set; }
    }
}