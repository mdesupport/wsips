﻿using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PoultryWateringForm
    {
        public int Id { get; set; }

        public int PermitWaterWithdrawalPurposeId { get; set; }

        public IEnumerable<SelectListItem> PoultryCoolingSystemList { get; set; }
        public Nullable<int> PoultryCoolingSystemId { get; set; }

        public IEnumerable<SelectListItem> PoultryTypeList { get; set; }
        public Nullable<int> PoultryId { get; set; }

        public Nullable<int> NumberOfHouses { get; set; }
        public Nullable<int> NumberOfFlocksPerYear { get; set; }
        public Nullable<int> BirdsPerFlock { get; set; }
        public int NumberOfDaysPerYearFoggersUsed { get; set; }
        public bool HasDeleted { get; set; }
    }
}