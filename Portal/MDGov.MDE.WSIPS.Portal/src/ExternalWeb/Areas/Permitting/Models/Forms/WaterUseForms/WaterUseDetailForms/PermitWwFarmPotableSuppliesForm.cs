﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwFarmPotableSuppliesForm
    {
        public int Id { get; set; }

        public int? UsePercentage { get; set; }

        [Required]
        public int NoOfEmployees { get; set; }

        public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    }
}