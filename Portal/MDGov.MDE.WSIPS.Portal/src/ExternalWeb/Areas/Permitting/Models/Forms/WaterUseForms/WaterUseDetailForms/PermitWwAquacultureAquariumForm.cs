﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwAquacultureAquariumForm
    {
        public int? UsePercentage { get; set; }

        public bool IsRecycledWater { get; set; }

        public int Id { get; set; }
        public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }

        public string AverageGallonPerDayFromGroundWater { get; set; }
        public string MaximumGallonPerDayFromGroundWater { get; set; }
        public string AverageGallonPerDayFromSurfaceWater { get; set; }
        public string MaximumGallonPerDayFromSurfaceWater { get; set; }

        [StringLength(500)]
        public string EstimateDescription { get; set; }
    }
}