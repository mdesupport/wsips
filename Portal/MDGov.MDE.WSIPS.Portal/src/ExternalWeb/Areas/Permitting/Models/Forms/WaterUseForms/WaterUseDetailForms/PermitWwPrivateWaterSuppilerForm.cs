﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwPrivateWaterSupplierForm : StandardQuestionsForm
    {
        public string Title { get; set; }

        //[StringLength(500)]
        //public string PurposeDescription { get; set; }

        //[Required]
        //public Nullable<int> IrrigatedAcres { get; set; }

        //[Required]
        //public Nullable<int> IrrigationSystemTypeId { get; set; }
        //public IEnumerable<SelectListItem> IrrigationSystemTypeList { get; set; }

        //[StringLength(500)]
        //public string IrrigationSystemDescription { get; set; }

        //public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
        //public IrrigationType IrrigationType { get; set; }
        
        public int? UsePercentage { get; set; }

        public int PermitWaterWithdrawalPurposeId { get; set; }

        public Nullable<int> NoOfLotsOrUnits { get; set; }
        public Nullable<int> NoOfEmployees { get; set; }
        public bool SwimmingPool { get; set; }
        public Nullable<int> NoOfRegularUsersPerDay { get; set; }
    }

    //public class PermitWwIndustrialDrinkingOrSanitaryForm : StandardQuestionsForm
    //{
    //    [Required]
    //    public int NoOfEmployees { get; set; }
    //    public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    //}

    //public class PermitWwMiningDrinkingOrSanitaryForm : StandardQuestionsForm
    //{
    //    [Required]
    //    public int NoOfEmployees { get; set; }
    //    public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    //}

    //public class PermitWwTrailerParkOrAptOrCondoForm : StandardQuestionsForm
    //{
    //    [Required]
    //    public int NoOfUnits { get; set; }
    //    public bool SwimmingPool { get; set; }
    //    public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    //}

    //public class PermitWwSubdivisionsForm : StandardQuestionsForm
    //{
    //    [Required]
    //    public int NoOflots { get; set; }
    //    public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    //}

    //public class PermitWwFireAndRescueDrinkingOrSanitaryForm : StandardQuestionsForm
    //{
    //    [Required]
    //    public int NoOfEmployees  { get; set; }
    //    public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    //}

    //public class PermitWwGovernmentDrinkingOrSanitaryForm : StandardQuestionsForm
    //{
    //    [Required]
    //    public int NoOfEmployees { get; set; }
    //    public int NoOfOtherRegularUsersPerDay { get; set; }
    //    public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    //}

    //public class PermitWwMiscellaneousDrinkingOrSanitaryForm : StandardQuestionsForm
    //{
    //    [Required]
    //    public int NoOfEmployees { get; set; }
    //    public int NoOfOtherRegularUsersPerDay { get; set; }
    //    public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
    //}
}