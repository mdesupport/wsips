﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwWildlifePondForm : StandardQuestionsForm
    {
        public int? UsePercentage { get; set; }

        public Nullable<int> NoOfPonds { get; set; }
        public Nullable<int> AreaOfPonds_inAcres { get; set; }
        public Nullable<int> DepthOfPonds_inFeet { get; set; }
        public Nullable<int> NoOfMonthsPerYearPondContainWater { get; set; }
    }
}