﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.ModelBinders;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms
{
    [ModelBinder(typeof(RemoveCommaModelBinder))]
    public class PermitWwReligiousDringkingOrSanitaryForm : StandardQuestionsForm
    {
        public int? UsePercentage { get; set; }

        public Nullable<int> NoOfParishioners { get; set; }
        public Nullable<int> NoOfServicesPerWeek { get; set; }
        public bool Daycare { get; set; }
        public Nullable<int> NoOfStaffWorkingMorethan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStaffWorkingLessthan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStudentsAttendingMorethan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStudentsAttendingLessthan20hrsPerWeek { get; set; }
    }
}