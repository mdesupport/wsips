﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models
{
    public class PendingPermit
    {
        public int Id { get; set; }

        public string UseDescription { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Permittee { get; set; }

        public string CreatedBy { get; set; }
    }
}