﻿using System;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models
{
    public class ConditionReport
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PermitConditionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ReceivedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? SubmittedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ConditionComplianceStatusId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? StandardConditionTypeId { get; set; }

        /// <summary>
        /// Total number of uploads related to this condition compliance Id
        /// </summary>
        public int Uploads { get; set; }

        public int? ExistingDocumentId { get; set; }

        public double? ReportAverageUsage { get; set; }
        public double? ReportTotalUsage { get; set; }
        public double AnnualAverageOverusePercentage { get; set; }
        public double AnnualAverageUsage { get; set; }
        public double? AnnualTotalUsage { get; set; }
    }
}