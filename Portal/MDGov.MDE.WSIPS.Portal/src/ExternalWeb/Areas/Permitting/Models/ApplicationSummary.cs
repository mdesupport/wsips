﻿using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models
{
    public class ApplicationSummary
    {
        public ApplicationSummary()
        {
            this.AdditionalLandOwners = new List<SummaryContact>();
        }

        public SummaryContact Applicant { get; set; }
        public SummaryContact WaterUser { get; set; }
        public SummaryContact PrimaryLandOwner { get; set; }
        public List<SummaryContact> AdditionalLandOwners { get; set; }
        public string Permittee { get; set; }
        public Permit Permit { get; set; }
        public List<string> SelectedUseDetailCategories { get; set; }
        public ApplicationWizard AppWizard { get; set; }
        public PermitWithdrawalGroundwater PermitWithdrawalGroundwater { get; set; }
        public PermitWithdrawalSurfacewater PermitWithdrawalSurfacewater { get; set; }
        public string PermitCounty { get; set; }
        public string ReceivedDate { get; set; }
        public int TotalIrrigationAcresForCrop { get; set; }
        public string RequiresWithdrawalReporting { get; set; }
    }
}