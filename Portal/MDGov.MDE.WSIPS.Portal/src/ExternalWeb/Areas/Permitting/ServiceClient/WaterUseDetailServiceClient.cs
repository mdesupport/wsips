﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.ServiceClient
{
    public interface IWaterUseDetailServiceClient
    {
        WaterUseDetailForm InitializeWaterUseDetailFrom(int permitId);
        void SaveWaterUseDetails(ApplicationWizard form);
        List<WastewaterTreatmentAndDisposalForm> GetWasteWaterTreatmentAndDisposal(string waterUseCategoryTypeids, int permitId);
        List<PermitWwPrivateWaterSupplierForm> GetPrivateWaterSupplierForms(string waterUseCategoryTypeids, int permitId);
    }

    public class WaterUseDetailServiceClient : IWaterUseDetailServiceClient
    {
        public List<PermitWaterWithdrawalPurpose> SavedWaterWithdrawalPurpose { get; set; }
        private readonly IUpdatableService<PermitWwLivestock> _permitWaterWithdrawalLivestockService;
        private readonly IUpdatableService<PermitWwAgricultural> _permitWwAgriculturalService;
        private readonly IUpdatableService<PermitWwNonAgricultureIrrigation> _permitWwNonAgricultureIrrigationService;
        private readonly IUpdatableService<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeService;
        private readonly IUpdatableService<PermitWwPoultry> _permitWwPoultryService;
        private readonly IUpdatableService<PermitWwCrop> _permitWwCropService;
        private readonly IUpdatableService<PermitWwPrivateWaterSupplier> _permitWwPrivateWaterSupplier;
        private readonly IUpdatableService<PermitWastewaterTreatmentDisposal> _permitWastewaterTreatmentDisposalService;
        private readonly IService<PermitWwPrivateWaterSupplier> _permitWaterWithdrawalPrivateWaterSupplierService;
        private readonly IUpdatableService<PermitWwFarmPortableSupply> _permitWwFarmPotableWaterSupplyService;
        private readonly IUpdatableService<PermitWwFoodProcessing> _permitWwFoodProcessing;
        private readonly IUpdatableService<PermitWwAquacultureOrAquarium> _permitWwAquacultureOrAquariumService;
        private readonly IUpdatableService<PermitWwCommercialDrinkingOrSanitary> _permitWwCommertialDrinkingOrSanitaryService;
        private readonly IUpdatableService<PermitWwInstitutionalEducational_drinkingOrsanitary> _permitWwEducationalDrinkingOrSanitaryService;
        private readonly IUpdatableService<PermitWwInstitutionalReligious_DringkingOrSanitary> _permitWwReligiousDrinkingOrSanitaryService;
        private readonly IUpdatableService<PermitWwWildlifePond> _permitWwWildlifePondService;
        private readonly IUpdatableService<PermitWwConstructionDewatering> _permitWwConstructionDewateringService;
        private readonly IUpdatableService<PermitWwGolfCourseIrrigation> _permitWwGolfCourseIrrigationService;

        public WaterUseDetailServiceClient(
            IUpdatableService<PermitWwLivestock> permitWaterWithdrawalLivestockService,
            IUpdatableService<PermitWwAgricultural> permitWwAgriculturalService,
            IUpdatableService<PermitWwNonAgricultureIrrigation> permitWwNonAgricultureIrrigationService,
            IUpdatableService<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeService,
            IUpdatableService<PermitWwPoultry> permitWwPoultryService,
            IUpdatableService<PermitWwPrivateWaterSupplier> permitWwPrivateWaterSupplier,
            IUpdatableService<PermitWwCrop> permitWwCropService,
            IUpdatableService<PermitWastewaterTreatmentDisposal> permitWastewaterTreatmentDisposalService,
            IService<PermitWwPrivateWaterSupplier> permitWaterWithdrawalPrivateWaterSupplierService,
            IUpdatableService<PermitWwFarmPortableSupply> permitWwFarmPotableWaterSupplyService,
            IUpdatableService<PermitWwFoodProcessing> permitWwFoodProcessing,
            IUpdatableService<PermitWwAquacultureOrAquarium> permitWwAquacultureOrAquariumService,
            IUpdatableService<PermitWwCommercialDrinkingOrSanitary> permitWwCommertialDrinkingOrSanitaryService,
            IUpdatableService<PermitWwInstitutionalEducational_drinkingOrsanitary> permitWwEducationalDrinkingOrSanitaryService,
            IUpdatableService<PermitWwInstitutionalReligious_DringkingOrSanitary> permitWwReligiousDrinkingOrSanitaryService,
            IUpdatableService<PermitWwWildlifePond> permitWwWildlifePondService,
            IUpdatableService<PermitWwConstructionDewatering> permitWwConstructionDewateringService,
            IUpdatableService<PermitWwGolfCourseIrrigation> permitWwGolfCourseIrrigationService)
        {
            _permitWaterWithdrawalLivestockService = permitWaterWithdrawalLivestockService;
            _permitWwAgriculturalService = permitWwAgriculturalService;
            _permitWwNonAgricultureIrrigationService = permitWwNonAgricultureIrrigationService;
            _permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
            _permitWwPoultryService = permitWwPoultryService;
            _permitWwCropService = permitWwCropService;
            _permitWwPrivateWaterSupplier = permitWwPrivateWaterSupplier;
            _permitWastewaterTreatmentDisposalService = permitWastewaterTreatmentDisposalService;
            _permitWaterWithdrawalPrivateWaterSupplierService = permitWaterWithdrawalPrivateWaterSupplierService;
            _permitWwFarmPotableWaterSupplyService = permitWwFarmPotableWaterSupplyService;
            _permitWwFoodProcessing = permitWwFoodProcessing;
            _permitWwAquacultureOrAquariumService = permitWwAquacultureOrAquariumService;
            _permitWwCommertialDrinkingOrSanitaryService = permitWwCommertialDrinkingOrSanitaryService;
            _permitWwEducationalDrinkingOrSanitaryService = permitWwEducationalDrinkingOrSanitaryService;
            _permitWwReligiousDrinkingOrSanitaryService = permitWwReligiousDrinkingOrSanitaryService;
            _permitWwWildlifePondService = permitWwWildlifePondService;
            _permitWwConstructionDewateringService = permitWwConstructionDewateringService;
            _permitWwGolfCourseIrrigationService = permitWwGolfCourseIrrigationService;
        }

        public void SaveWaterUseDetails(ApplicationWizard form)
        {
            if (form.WaterUseDetailForm != null)
            {
                SavePermitLivestockWatering(form);
                SaveOtherLivestockWatering(form);
                SaveDairyAnimalWatering(form);

                SaveFarmPotableSupplies(form);

                SavePoultryWatering(form);

                SaveCrops(form);

                SaveFoodProcessing(form);

                SaveAquacultureAndAquarium(form);

                SavePermitWwIrrigation(form);

                SavePrivateWaterSupplierVariants(form);

                SavePrivateWaterSuppiler(form);
            }
        }

        public List<WastewaterTreatmentAndDisposalForm> GetWasteWaterTreatmentAndDisposal(string waterUseCategoryTypeids, int permitId)
        {
            var forms = new List<WastewaterTreatmentAndDisposalForm>();

            var ids = waterUseCategoryTypeids.Split(new char[] { ',' }).ToList();
            ids.RemoveAll(x => x == "");

            // Get the water use category and type 
            var permitWaterWithdrawalPurpose = _permitWaterWithdrawalPurposeService.GetRange(0, 9999, null, new DynamicFilter[] { new DynamicFilter("@0.Contains(outerIt.WaterWithdrawalPurposeId) and PermitId=@1", ids, permitId) }, "LU_WaterWithdrawalPurpose");

            // Get Ids 
            var permitWaterWithdrawalPurposeIds = (permitWaterWithdrawalPurpose.Select(x => x.Id.ToString())).ToList();

            //Get Private Water Supply from database
            var permitWWWasteWaterTreatmentDisposalList = _permitWastewaterTreatmentDisposalService.GetRange(0, 9999, null, new DynamicFilter[] { new DynamicFilter("@0.Contains(outerIt.PermitWaterWithdrawalPurposeId)", permitWaterWithdrawalPurposeIds) }, "PermitWaterWithdrawalPurpose");
            var _savedIds = (permitWWWasteWaterTreatmentDisposalList.Select(x => x.PermitWaterWithdrawalPurpose)).ToList().Select(x => x.WaterWithdrawalPurposeId.ToString());

            ids.RemoveAll(x => _savedIds.Contains(x));

            var _list = permitWWWasteWaterTreatmentDisposalList.ToList();
            foreach (var id in ids)
            {
                var model = new PermitWastewaterTreatmentDisposal();
                model.PermitWaterWithdrawalPurpose = new PermitWaterWithdrawalPurpose();
                model.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId = Int32.Parse(id);
                _list.Add(model);
            }

            // Get the title 

            foreach (var model in _list)
            {
                var purpose = permitWaterWithdrawalPurpose.SingleOrDefault(x => x.LU_WaterWithdrawalPurpose.Id == model.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId);
                if (purpose != null)
                {
                    var _form = Mapper.Map<PermitWastewaterTreatmentDisposal, WastewaterTreatmentAndDisposalForm>(model);
                    _form.WaterWithdrawalPurposeId = (int)purpose.WaterWithdrawalPurposeId;
                    _form.Title = purpose.LU_WaterWithdrawalPurpose.Description;
                    if (_form.Id == 0)      // if form is newly clicked by user, set iswaterdischarge to true, else keep the data that was in db
                    {
                        _form.IsWastewaterDischarge = true;
                    }

                    forms.Add(_form);
                }
            }
            return forms;
        }

        public List<PermitWwPrivateWaterSupplierForm> GetPrivateWaterSupplierForms(string waterUseCategoryTypeids, int permitId)
        {
            var forms = new List<PermitWwPrivateWaterSupplierForm>();

            var ids = waterUseCategoryTypeids.Split(new char[] { ',' }).ToList();
            ids.RemoveAll(x => x == "" || x == "40" || x == "42" || x == "44" || x == "57" || x == "61"); // Remove all differnt variant of Private water supplies

            // Get the water use category and type 
            var permitWaterWithdrawalPurpose = _permitWaterWithdrawalPurposeService.GetRange(0, 9999, null, new DynamicFilter[] { new DynamicFilter("@0.Contains(outerIt.WaterWithdrawalPurposeId) and PermitId=@1", ids, permitId) }, "LU_WaterWithdrawalPurpose");

            // Get Ids 
            var permitWaterWithdrawalPurposeIds = (permitWaterWithdrawalPurpose.Select(x => x.Id.ToString())).ToList();

            //Get Private Water Supply from database
            var permitWaterWithdrawalPrivateWaterSupplierList = _permitWaterWithdrawalPrivateWaterSupplierService.GetRange(0, 9999, null, new DynamicFilter[] { new DynamicFilter("@0.Contains(outerIt.PermitWaterWithdrawalPurposeId)", permitWaterWithdrawalPurposeIds) }, "PermitWaterWithdrawalPurpose");
            var _savedIds = (permitWaterWithdrawalPrivateWaterSupplierList.Select(x => x.PermitWaterWithdrawalPurpose)).ToList().Select(x => x.WaterWithdrawalPurposeId.ToString());

            ids.RemoveAll(x => _savedIds.Contains(x));


            var _list = permitWaterWithdrawalPrivateWaterSupplierList.ToList();

            foreach (var id in ids)
            {
                var model = new PermitWwPrivateWaterSupplier();
                model.PermitWaterWithdrawalPurpose = new PermitWaterWithdrawalPurpose();
                model.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId = Int32.Parse(id);
                _list.Add(model);
            }

            // Get the title 
            foreach (var model in _list)
            {
                var purpose = permitWaterWithdrawalPurpose.SingleOrDefault(x => x.LU_WaterWithdrawalPurpose.Id == model.PermitWaterWithdrawalPurpose.WaterWithdrawalPurposeId);
                if (purpose != null)
                {
                    var _form = Mapper.Map<PermitWwPrivateWaterSupplier, PermitWwPrivateWaterSupplierForm>(model);
                    _form.PermitWaterWithdrawalPurposeId = (int)purpose.WaterWithdrawalPurposeId;
                    _form.UsePercentage = purpose.UsePercent;
                    _form.Title = purpose.LU_WaterWithdrawalPurpose.Description;

                    forms.Add(_form);
                }
            }
            return forms;
        }

        #region Load Water use Details

        public WaterUseDetailForm InitializeWaterUseDetailFrom(int permitId)
        {
            if (permitId == 0)
            {
                return CreateWaterUseDetailForm();
            }
            else
            {
                return LoadWaterUseDetailForm(permitId);
            }
        }

        private WaterUseDetailForm CreateWaterUseDetailForm()
        {
            var form = new WaterUseDetailForm();

            form.GolfCourseIrrigationForm = Mapper.Map<PermitWwGolfCourseIrrigation, PermitWwGolfCourseIrrigationForm>(new PermitWwGolfCourseIrrigation());
            form.PermitWwCommercialDrinkingOrSanitaryForm = Mapper.Map<PermitWwCommercialDrinkingOrSanitary, PermitWwCommercialDrinkingOrSanitaryForm>(new PermitWwCommercialDrinkingOrSanitary());
            form.PermitWwEducationalDrinkingOrsanitaryForm = Mapper.Map<PermitWwInstitutionalEducational_drinkingOrsanitary, PermitWwEducationalDrinkingOrsanitaryForm>(new PermitWwInstitutionalEducational_drinkingOrsanitary());
            form.PermitWwConstructionDewateringForm = Mapper.Map<PermitWwConstructionDewatering, PermitWwConstructionDewateringForm>(new PermitWwConstructionDewatering());

            return form;
        }

        private WaterUseDetailForm LoadWaterUseDetailForm(int permitId)
        {
            var waterUseDetailForm = CreateWaterUseDetailForm();

            var waterUsePurposes = _permitWaterWithdrawalPurposeService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitId=" + permitId) });

            if (waterUsePurposes != null && waterUsePurposes.Count() > 0)
            {
                var model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.Poultry_Evaporative);
                if (model != null)
                {
                    waterUseDetailForm.PoultryWateringEvaporativeForm.UsePercentage = model.UsePercent;
                    LoadPoultryWateringEvaporative(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.Poultry_Foggers);
                if (model != null)
                {
                    waterUseDetailForm.PoultryWateringFoggerForm.UsePercentage = model.UsePercent;
                    LoadPoultryWateringFogger(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.OtherLivestockWatering);
                if (model != null)
                {
                    waterUseDetailForm.OtherLivestockWateringForm.UsePercentage = model.UsePercent;
                    LoadOtherLivestockWatering(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.FarmPotableSupplies);
                if (model != null)
                {
                    LoadFarmPotableSupplies(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.DairyAnimalWatering);
                if (model != null)
                {
                    waterUseDetailForm.DairyAnimalWateringForm.UsePercentage = model.UsePercent;
                    LoadDairyAnimalWatering(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.AgriculturalIrrigation);
                if (model != null)
                {
                    waterUseDetailForm.CropUseForm.UsePercentage = model.UsePercent;
                    waterUseDetailForm.CropUseForm.TotalNumberOfAcres = model.TotalIrrigatedAcres.HasValue ? (int?)Math.Round(model.TotalIrrigatedAcres.Value) : 0;

                    LoadAgriculturalIrrigation(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.NurseryIrrigation);
                if (model != null)
                {
                    waterUseDetailForm.NurseryStockIrrigationForm.UsePercentage = model.UsePercent;
                    LoadNurseryIrrigation(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.SodFarmIrrigation);
                if (model != null)
                {
                    waterUseDetailForm.SodIrrigationForm.UsePercentage = model.UsePercent;
                    LoadSodFarmIrrigation(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.FoodProcessing);
                if (model != null)
                {
                    LoadFoodProcessing(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.AquacultureAndAquarium);
                if (model != null)
                {
                    LoadAquacultureOrAquarium(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.Irrigation_Undefined);
                if (model != null)
                {
                    LoadIrrigationUndefined(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.GolfCourseIrrigation);
                if (model != null)
                {
                    LoadGolfCourseIrrigation(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.LawnAndParkIrrigation);
                if (model != null)
                {
                    LoadLawnAndParkIrrigation(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.SmallIntermittentIrrigation);
                if (model != null)
                {
                    LoadSmallIntermitentIrrigation(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.CommercialDrinking_Sanitary);
                if (model != null)
                {
                    LoadCommercialDrinkingOrSanitary(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.EducationalDrinking_Sanitary);
                if (model != null)
                {
                    LoadEducationalDrinkingOrSanitary(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.ReligiousDrinking_Sanitary);
                if (model != null)
                {
                    LoadReligiousDrinkingOrSanitary(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.WildlifePondsandRecreational);
                if (model != null)
                {
                    LoadWildlifePonds(waterUseDetailForm, model.Id);
                }

                model = waterUsePurposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.ConstructionDewatering);
                if (model != null)
                {
                    LoadConstructionDewatering(waterUseDetailForm, model.Id);
                }

                IEnumerable<PermitWaterWithdrawalPurpose> _model = waterUsePurposes.Where(x => x.WaterWithdrawalPurposeId > (int)WaterWithdrawalPurpose.SmallIntermittentIrrigation);
                if (_model != null)
                {
                    var waterUseCategoryTypeids = String.Join(",", _model.Select(x => x.WaterWithdrawalPurposeId));
                    waterUseDetailForm.PrivateWaterSuppilerForm.UsePercentage = _model.Sum(x => x.UsePercent);
                    waterUseDetailForm.PrivateWaterSuppilerForm.PrivateWaterSupplierForms = GetPrivateWaterSupplierForms(waterUseCategoryTypeids, permitId);
                }

                return waterUseDetailForm;
            }
            else
            {
                return CreateWaterUseDetailForm();
            }
        }

        private void LoadPoultryWateringFogger(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
        {
            var poultryWateringFogger = _permitWwPoultryService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").ToList();

            waterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms = Mapper.Map<List<PermitWwPoultry>, List<PoultryWateringForm>>(poultryWateringFogger ?? new List<PermitWwPoultry>());
        }

        private void LoadPoultryWateringEvaporative(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
        {
            var poultryWateringEvaporative = _permitWwPoultryService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").ToList();

            waterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms = Mapper.Map<List<PermitWwPoultry>, List<PoultryWateringForm>>(poultryWateringEvaporative ?? new List<PermitWwPoultry>());
        }

        private void LoadOtherLivestockWatering(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
        {
            var permitWwLivestock = _permitWaterWithdrawalLivestockService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").ToList();

            waterUseDetailForm.OtherLivestockWateringForm.LivestockForms = Mapper.Map<List<PermitWwLivestock>, List<LivestockForm>>(permitWwLivestock ?? new List<PermitWwLivestock>());
        }

        private void LoadDairyAnimalWatering(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
        {
            var permitWwLivestocks = _permitWaterWithdrawalLivestockService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").ToList();

            waterUseDetailForm.DairyAnimalWateringForm.LivestockForms = Mapper.Map<List<PermitWwLivestock>, List<LivestockForm>>(permitWwLivestocks ?? new List<PermitWwLivestock>());
        }

        private void LoadFarmPotableSupplies(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
        {
            var farmPotableSupplies = _permitWwFarmPotableWaterSupplyService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailForm.PermitWwFarmPotableSuppliesForm = Mapper.Map<PermitWwFarmPortableSupply, PermitWwFarmPotableSuppliesForm>(farmPotableSupplies ?? new PermitWwFarmPortableSupply());
        }

        private void LoadAgriculturalIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var permitWwCrop = _permitWwCropService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").ToList();

            waterUseDetailform.CropUseForm.CropForms = Mapper.Map<List<PermitWwCrop>, List<CropForm>>(permitWwCrop ?? new List<PermitWwCrop>());
        }

        private void LoadNurseryIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var permitWwIrrigation = _permitWwAgriculturalService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").ToList();

            waterUseDetailform.NurseryStockIrrigationForm.IrrigationForms = Mapper.Map<List<PermitWwAgricultural>, List<PermitWwIrrigationForm>>(permitWwIrrigation ?? new List<PermitWwAgricultural>());
        }

        private void LoadSodFarmIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var permitWwIrrigation = _permitWwAgriculturalService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").ToList();

            waterUseDetailform.SodIrrigationForm.IrrigationForms = Mapper.Map<List<PermitWwAgricultural>, List<PermitWwIrrigationForm>>(permitWwIrrigation ?? new List<PermitWwAgricultural>());
        }

        private void LoadFoodProcessing(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
        {
            var foodProcessing = _permitWwFoodProcessing.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailForm.PermitWwFoodProcessingForm = Mapper.Map<PermitWwFoodProcessing, PermitWwFoodProcessingForm>(foodProcessing ?? new PermitWwFoodProcessing());
        }

        private void LoadAquacultureOrAquarium(WaterUseDetailForm waterUseDetailForm, int permitWaterWithdrawalPurposeId)
        {
            var aquacultureOrAquarium = _permitWwAquacultureOrAquariumService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailForm.PermitWwAquacultureAquariumForm = Mapper.Map<PermitWwAquacultureOrAquarium, PermitWwAquacultureAquariumForm>(aquacultureOrAquarium ?? new PermitWwAquacultureOrAquarium());
        }

        private void LoadIrrigationUndefined(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var permitWwIrrigation = _permitWwNonAgricultureIrrigationService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.IrrigationUndefinedForm = Mapper.Map<PermitWwNonAgricultureIrrigation, PermitWwNonAgricultureIrrigationForm>(permitWwIrrigation ?? new PermitWwNonAgricultureIrrigation());
        }

        private void LoadGolfCourseIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var golfIrrigation = _permitWwGolfCourseIrrigationService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.GolfCourseIrrigationForm = Mapper.Map<PermitWwGolfCourseIrrigation, PermitWwGolfCourseIrrigationForm>(golfIrrigation ?? new PermitWwGolfCourseIrrigation());
        }

        private void LoadLawnAndParkIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var permitWwIrrigation = _permitWwNonAgricultureIrrigationService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.LawnAndParkIrrigationForm = Mapper.Map<PermitWwNonAgricultureIrrigation, PermitWwNonAgricultureIrrigationForm>(permitWwIrrigation ?? new PermitWwNonAgricultureIrrigation());
        }

        private void LoadSmallIntermitentIrrigation(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var permitWwIrrigation = _permitWwNonAgricultureIrrigationService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.SmallIntermitentIrrigationForm = Mapper.Map<PermitWwNonAgricultureIrrigation, PermitWwNonAgricultureIrrigationForm>(permitWwIrrigation ?? new PermitWwNonAgricultureIrrigation());
        }

        private void LoadCommercialDrinkingOrSanitary(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var commercialDrinkingOrSanitary = _permitWwCommertialDrinkingOrSanitaryService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.PermitWwCommercialDrinkingOrSanitaryForm = Mapper.Map<PermitWwCommercialDrinkingOrSanitary, PermitWwCommercialDrinkingOrSanitaryForm>(commercialDrinkingOrSanitary ?? new PermitWwCommercialDrinkingOrSanitary());
        }

        private void LoadEducationalDrinkingOrSanitary(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var educationalDrinkingOrSanitary = _permitWwEducationalDrinkingOrSanitaryService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.PermitWwEducationalDrinkingOrsanitaryForm = Mapper.Map<PermitWwInstitutionalEducational_drinkingOrsanitary, PermitWwEducationalDrinkingOrsanitaryForm>(educationalDrinkingOrSanitary ?? new PermitWwInstitutionalEducational_drinkingOrsanitary());
        }

        private void LoadReligiousDrinkingOrSanitary(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var religiousDrinkingOrSanitary = _permitWwReligiousDrinkingOrSanitaryService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.PermitWwReligiousDringkingOrSanitaryForm = Mapper.Map<PermitWwInstitutionalReligious_DringkingOrSanitary, PermitWwReligiousDringkingOrSanitaryForm>(religiousDrinkingOrSanitary ?? new PermitWwInstitutionalReligious_DringkingOrSanitary());
        }

        private void LoadWildlifePonds(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var wildlifePonds = _permitWwWildlifePondService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, null).FirstOrDefault();

            waterUseDetailform.PermitWwWildlifePondForm = Mapper.Map<PermitWwWildlifePond, PermitWwWildlifePondForm>(wildlifePonds ?? new PermitWwWildlifePond());
        }

        private void LoadConstructionDewatering(WaterUseDetailForm waterUseDetailform, int permitWaterWithdrawalPurposeId)
        {
            var constructionDewatering = _permitWwConstructionDewateringService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitWaterWithdrawalPurposeId=" + permitWaterWithdrawalPurposeId) }, "PermitWaterWithdrawalPurpose").FirstOrDefault();

            waterUseDetailform.PermitWwConstructionDewateringForm = Mapper.Map<PermitWwConstructionDewatering, PermitWwConstructionDewateringForm>(constructionDewatering ?? new PermitWwConstructionDewatering());
        }

        #endregion

        #region  Save Water use Details

        private void SavePoultryWatering(ApplicationWizard form)
        {
            var purposeForm = form.WaterUseDetailForm.PoultryWateringEvaporativeForm;

            //Evaporative
            if (purposeForm.PoultryWateringForms != null && purposeForm.PoultryWateringForms.Count > 0)
            {
                var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.Poultry_Evaporative);
                if (purpose != null)
                {
                    purpose.UsePercent = purposeForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    purposeForm.PoultryWateringForms.RemoveAll(x => x.HasDeleted);
                    var _list = Mapper.Map<List<PoultryWateringForm>, List<PermitWwPoultry>>(purposeForm.PoultryWateringForms);
                    foreach (var item in _list)
                    {
                        item.PermitWaterWithdrawalPurposeId = purpose.Id;
                        _permitWwPoultryService.Save(item);
                    }
                }
            }

            purposeForm = form.WaterUseDetailForm.PoultryWateringFoggerForm;

            //Foggers
            if (purposeForm.PoultryWateringForms != null && purposeForm.PoultryWateringForms.Count > 0)
            {
                var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.Poultry_Foggers);
                if (purpose != null)
                {
                    purpose.UsePercent = purposeForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    purposeForm.PoultryWateringForms.RemoveAll(x => x.HasDeleted);
                    var _list = Mapper.Map<List<PoultryWateringForm>, List<PermitWwPoultry>>(purposeForm.PoultryWateringForms);
                    foreach (var item in _list)
                    {
                        item.PermitWaterWithdrawalPurposeId = purpose.Id;
                        _permitWwPoultryService.Save(item);
                    }
                }
            }
        }

        private void SavePermitLivestockWatering(ApplicationWizard form)
        {
            var purposeForm = form.WaterUseDetailForm.PermitWwLivestockForm;

            if (purposeForm.LivestockForms != null && purposeForm.LivestockForms.Count > 0)
            {
                var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.LivestockWatering);
                if (purpose != null)
                {
                    purpose.UsePercent = purposeForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    purposeForm.LivestockForms.RemoveAll(x => x.HasDeleted);
                    var _list = Mapper.Map<List<LivestockForm>, List<PermitWwLivestock>>(purposeForm.LivestockForms);
                    foreach (var item in _list)
                    {
                        item.PermitWaterWithdrawalPurposeId = purpose.Id;
                        _permitWaterWithdrawalLivestockService.Save(item);
                    }
                }
            }
        }

        private void SavePermitWwIrrigation(ApplicationWizard form)
        {
            // If the use selected waterusecategory and Type, save the details
            if (form.SavedWaterWithdrawalPurpose != null)
            {
                PermitWaterWithdrawalPurpose purpose = null;
                PermitWwNonAgricultureIrrigation _model = new PermitWwNonAgricultureIrrigation();

                // Save Irrigation (undefined)
                purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.Irrigation_Undefined);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.IrrigationUndefinedForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    _model = Mapper.Map<PermitWwNonAgricultureIrrigationForm, PermitWwNonAgricultureIrrigation>(form.WaterUseDetailForm.IrrigationUndefinedForm ?? new PermitWwNonAgricultureIrrigationForm());
                    _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                    _permitWwNonAgricultureIrrigationService.Save(_model);
                }

                // Save GolfCourse Irrigation
                purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.GolfCourseIrrigation);
                if (purpose != null && form.WaterUseDetailForm.GolfCourseIrrigationForm != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.GolfCourseIrrigationForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    var golfCourseModel = new PermitWwGolfCourseIrrigation();

                    golfCourseModel = Mapper.Map<PermitWwGolfCourseIrrigationForm, PermitWwGolfCourseIrrigation>(form.WaterUseDetailForm.GolfCourseIrrigationForm ?? new PermitWwGolfCourseIrrigationForm());
                    golfCourseModel.PermitWaterWithdrawalPurposeId = purpose.Id;
                    _permitWwGolfCourseIrrigationService.Save(golfCourseModel);
                }

                // Save Lawn And Park Irrigation
                purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.LawnAndParkIrrigation);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.LawnAndParkIrrigationForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    _model = Mapper.Map<PermitWwNonAgricultureIrrigationForm, PermitWwNonAgricultureIrrigation>(form.WaterUseDetailForm.LawnAndParkIrrigationForm ?? new PermitWwNonAgricultureIrrigationForm());
                    _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                    _permitWwNonAgricultureIrrigationService.Save(_model);
                }

                // Save Small Intermitent Irrigation
                purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.SmallIntermittentIrrigation);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.SmallIntermitentIrrigationForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    _model = Mapper.Map<PermitWwNonAgricultureIrrigationForm, PermitWwNonAgricultureIrrigation>(form.WaterUseDetailForm.SmallIntermitentIrrigationForm ?? new PermitWwNonAgricultureIrrigationForm());
                    _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                    _permitWwNonAgricultureIrrigationService.Save(_model);
                }

                //Save Nursery Irrigation 
                purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.NurseryIrrigation);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.NurseryStockIrrigationForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    if (form.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms != null)
                    {
                        form.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms.RemoveAll(x => x.HasDeleted);
                        var _list = Mapper.Map<List<PermitWwIrrigationForm>, List<PermitWwAgricultural>>(form.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms);
                        foreach (var item in _list)
                        {
                            item.PermitWaterWithdrawalPurposeId = purpose.Id;
                            _permitWwAgriculturalService.Save(item);
                        }
                    }
                }

                //Save Sod Irrigation 
                purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.SodFarmIrrigation);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.SodIrrigationForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    if (form.WaterUseDetailForm.SodIrrigationForm.IrrigationForms != null)
                    {
                        form.WaterUseDetailForm.SodIrrigationForm.IrrigationForms.RemoveAll(x => x.HasDeleted);
                        var _list = Mapper.Map<List<PermitWwIrrigationForm>, List<PermitWwAgricultural>>(form.WaterUseDetailForm.SodIrrigationForm.IrrigationForms);
                        foreach (var item in _list)
                        {
                            item.PermitWaterWithdrawalPurposeId = purpose.Id;
                            _permitWwAgriculturalService.Save(item);
                        }
                    }
                }
            }
        }

        private void SaveDairyAnimalWatering(ApplicationWizard form)
        {
            if (form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms != null && form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms.Count > 0)
            {
                var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.DairyAnimalWatering);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.DairyAnimalWateringForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms.RemoveAll(x => x.HasDeleted);
                    var _list = Mapper.Map<List<LivestockForm>, List<PermitWwLivestock>>(form.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms);

                    foreach (var item in _list)
                    {
                        item.PermitWaterWithdrawalPurposeId = purpose.Id;
                        _permitWaterWithdrawalLivestockService.Save(item);
                    }
                }
            }
        }

        private void SaveFarmPotableSupplies(ApplicationWizard form)
        {
            //Farm Portable Supplies
            var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.FarmPotableSupplies);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwFarmPotableSuppliesForm, PermitWwFarmPortableSupply>(form.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm);
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                _permitWwFarmPotableWaterSupplyService.Save(_model);
            }
        }

        private void SaveOtherLivestockWatering(ApplicationWizard form)
        {
            if (form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms != null && form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms.Count > 0)
            {
                var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.OtherLivestockWatering);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.OtherLivestockWateringForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms.RemoveAll(x => x.HasDeleted);
                    var _list = Mapper.Map<List<LivestockForm>, List<PermitWwLivestock>>(form.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms);

                    foreach (var item in _list)
                    {
                        item.PermitWaterWithdrawalPurposeId = purpose.Id;
                        _permitWaterWithdrawalLivestockService.Save(item);
                    }
                }
            }
        }

        private void SaveCrops(ApplicationWizard form)
        {
            //Crop irrigation
            if (form.WaterUseDetailForm.CropUseForm.CropForms != null && form.WaterUseDetailForm.CropUseForm.CropForms.Count > 0)
            {
                var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.AgriculturalIrrigation);
                if (purpose != null)
                {
                    purpose.UsePercent = form.WaterUseDetailForm.CropUseForm.UsePercentage;
                    _permitWaterWithdrawalPurposeService.Save(purpose);

                    form.WaterUseDetailForm.CropUseForm.CropForms.RemoveAll(x => x.HasDeleted);
                    var _list = Mapper.Map<List<CropForm>, List<PermitWwCrop>>(form.WaterUseDetailForm.CropUseForm.CropForms);
                    foreach (var item in _list)
                    {
                        item.PermitWaterWithdrawalPurposeId = purpose.Id;
                        _permitWwCropService.Save(item);
                    }
                }
            }
        }

        private void SaveFoodProcessing(ApplicationWizard form)
        {
            //Food Processsig
            var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.FoodProcessing);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwFoodProcessingForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwFoodProcessingForm, PermitWwFoodProcessing>(form.WaterUseDetailForm.PermitWwFoodProcessingForm ?? new PermitWwFoodProcessingForm());
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                _permitWwFoodProcessing.Save(_model);
            }
        }

        private void SaveAquacultureAndAquarium(ApplicationWizard form)
        {
            //Aquaculture and Aquarium
            var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.AquacultureAndAquarium);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwAquacultureAquariumForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwAquacultureAquariumForm, PermitWwAquacultureOrAquarium>(form.WaterUseDetailForm.PermitWwAquacultureAquariumForm);
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                _permitWwAquacultureOrAquariumService.Save(_model);
            }
        }

        private void SavePrivateWaterSupplierVariants(ApplicationWizard form)
        {
            int _permitWaterWithdrawalPurposeId;
            int id;

            //Commercial Dringking/Sanitary
            var purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.CommercialDrinking_Sanitary);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwCommercialDrinkingOrSanitaryForm, PermitWwCommercialDrinkingOrSanitary>(form.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm);
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                id = _permitWwCommertialDrinkingOrSanitaryService.Save(_model);
            }

            // Industrial Education Drinking/Sanitary
            purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.EducationalDrinking_Sanitary);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwEducationalDrinkingOrsanitaryForm, PermitWwInstitutionalEducational_drinkingOrsanitary>(form.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm);
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                _permitWwEducationalDrinkingOrSanitaryService.Save(_model);
            }

            //Institutional religious (drinking/sanitary)
            purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.ReligiousDrinking_Sanitary);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwReligiousDringkingOrSanitaryForm, PermitWwInstitutionalReligious_DringkingOrSanitary>(form.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm);
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                _permitWwReligiousDrinkingOrSanitaryService.Save(_model);
            }

            //Wildlife Ponds
            purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.WildlifePondsandRecreational);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwWildlifePondForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwWildlifePondForm, PermitWwWildlifePond>(form.WaterUseDetailForm.PermitWwWildlifePondForm);
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                _permitWwWildlifePondService.Save(_model);
            }

            //Construction Dewatering
            purpose = form.SavedWaterWithdrawalPurpose.SingleOrDefault(x => x.WaterWithdrawalPurposeId == (int)WaterWithdrawalPurpose.ConstructionDewatering);
            if (purpose != null)
            {
                purpose.UsePercent = form.WaterUseDetailForm.PermitWwConstructionDewateringForm.UsePercentage;
                _permitWaterWithdrawalPurposeService.Save(purpose);

                var _model = Mapper.Map<PermitWwConstructionDewateringForm, PermitWwConstructionDewatering>(form.WaterUseDetailForm.PermitWwConstructionDewateringForm);
                _model.PermitWaterWithdrawalPurposeId = purpose.Id;
                _permitWwConstructionDewateringService.Save(_model);
            }
        }

        private void SavePrivateWaterSuppiler(ApplicationWizard form)
        {
            var purposeForm = form.WaterUseDetailForm.PrivateWaterSuppilerForm;
            //Private water supply
            if (purposeForm.PrivateWaterSupplierForms != null && purposeForm.PrivateWaterSupplierForms.Count > 0)
            {
                var purposes = form.SavedWaterWithdrawalPurpose.Where(x => x.WaterWithdrawalPurposeId > (int)WaterWithdrawalPurpose.SmallIntermittentIrrigation);

                foreach (var _form in purposeForm.PrivateWaterSupplierForms)
                {
                    var model = Mapper.Map<PermitWwPrivateWaterSupplierForm, PermitWwPrivateWaterSupplier>(_form);
                    var purpose = purposes.SingleOrDefault(x => x.WaterWithdrawalPurposeId == _form.PermitWaterWithdrawalPurposeId);

                    if (purpose != null)
                    {
                        purpose.UsePercent = _form.UsePercentage;
                        _permitWaterWithdrawalPurposeService.Save(purpose);

                        model.PermitWaterWithdrawalPurposeId = purpose.Id;

                        _permitWwPrivateWaterSupplier.Save(model);
                    }
                }
            }
        }
        #endregion
    }
}