﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.ServiceClient
{
    public interface IPermitServiceClient
    {
        ApplicationWizard InitializeApplicationWizardFrom();
        ApplicationWizard GetApplicationWizardForExistingPermit(int permitId);
        int SaveApplicantInformation(ApplicationWizard form);
        IEnumerable<PendingPermit> GetPendingPermits();
        List<ContactSearchModel> BuildResult(IEnumerable<Contact> result);
        IEnumerable<DynamicFilter> BuildFilter(ContactSearchForm searchForm, IEnumerable<int> contactIds);
        List<ContactForm> BuildContactForm(IEnumerable<Contact> contacts);
    }

    public class PermitServiceClient : IPermitServiceClient
    {
        private readonly IUpdatableService<Permit> _permittingService;
        private readonly IUpdatableService<Contact> _contactService;
        private readonly IPermitWaterWithdrawalPurposeService _permitWaterWithdrawalPurposeService;
        private readonly IUpdatableService<PermitContact> _permitContactService;
        private readonly IUpdatableService<PermitContactInformation> _permitContactInformationService;
        private readonly IWaterUseDetailServiceClient _waterUseDetailServiceClient;
        private readonly IUpdatableService<PermitWastewaterTreatmentDisposal> _permitWastewaterTreatmentDisposalService;
        private readonly IUpdatableService<ContactCommunicationMethod> _contactCommunicationMethodService;

        public PermitServiceClient(
            IUpdatableService<Permit> permittingService,
            IUpdatableService<Contact> contactService,
            IPermitWaterWithdrawalPurposeService permitWaterWithdrawalPurposeService,
            IUpdatableService<PermitContact> permitContactService,
            IUpdatableService<PermitContactInformation> permitContactInformationService,
            IUpdatableService<PermitWastewaterTreatmentDisposal> permitWastewaterTreatmentDisposalService,
            IUpdatableService<ContactCommunicationMethod> contactCommunicationMethodService,
            IWaterUseDetailServiceClient waterUseDetailServiceClient)
        {
            _permittingService = permittingService;
            _contactService = contactService;
            _permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
            _permitContactService = permitContactService;
            _permitContactInformationService = permitContactInformationService;
            _waterUseDetailServiceClient = waterUseDetailServiceClient;
            _permitWastewaterTreatmentDisposalService = permitWastewaterTreatmentDisposalService;
            _contactCommunicationMethodService = contactCommunicationMethodService;
        }

        public ApplicationWizard InitializeApplicationWizardFrom()
        {
            var userId = SessionHandler.GetSessionVar<int>(SessionVariables.CurrentlyLoggedinUserId);

            var contact = _contactService.GetRange(0, 1, null, new[] { new DynamicFilter("UserId==" + userId) }, "ContactCommunicationMethods").FirstOrDefault();

            var form = new ApplicationWizard
            {
                Contacts = new List<ContactForm>(),
                Contact = Mapper.Map<Contact, ContactForm>(contact)
            };

            // The first contact is always the contact of the user who logged in.

            // And we will have place holders for at most two contacts (water user and land owner's contact if the applicant is a consultant)
            var contact1 = Mapper.Map<Contact, ContactForm>(new Contact());
            var contact2 = Mapper.Map<Contact, ContactForm>(new Contact());

            contact1.ContactType = ContactType.WaterUser;
            contact1.Active = true;

            contact2.ContactType = ContactType.LandOwner;
            contact2.Active = true;

            form.Contacts.Add(contact1);
            form.Contacts.Add(contact2);

            //For new permit application, assume the applicant is both a water user and land owner
            form.IsWaterUser = true; form.IsLandOwner = true; form.IsConsultant = false;

            return form;
        }

        public ApplicationWizard GetApplicationWizardForExistingPermit(int permitId)
        {
            var form = new ApplicationWizard
            {
                ContactSearchForm = new ContactSearchForm(),
                PermitId = permitId,
                Contacts = new List<ContactForm>()
            };

            var permit = _permittingService.GetById(permitId);

            //Applicant identification
            form.IsWaterUser = Convert.ToInt32(permit.ApplicantIdentification.Substring(1, 1)) != 0;
            form.IsLandOwner = Convert.ToInt32(permit.ApplicantIdentification.Substring(2, 1)) != 0;
            form.IsConsultant = Convert.ToInt32(permit.ApplicantIdentification.Substring(3, 1)) != 0;
            form.PermitIssuedTo = Convert.ToInt32(permit.ApplicantIdentification.Substring(4, 1));
            form.CurrentStep = "5"; // By defualt it starts from description page.
            form.UseDescription = permit.UseDescription;

            //Get Permit Contact information
            GetPermitContactInformation(form, permitId);

            //Get saved water use category and type
            var waterUsePurposes = _permitWaterWithdrawalPurposeService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitId=" + permitId) });

            if (waterUsePurposes != null && waterUsePurposes.Any())
            {
                form.SelectedWaterUsePurposeIds = String.Join(",", waterUsePurposes.Select(x => x.WaterWithdrawalPurposeId));
            }

            //load waste water treatment and disposal

            var _waterUseCategoryTypeids = String.Join(",", waterUsePurposes.Select(x => x.WaterWithdrawalPurposeId).Where(x => x > 11));
            form.WastewaterTreatmentDisposalForm = _waterUseDetailServiceClient.GetWasteWaterTreatmentAndDisposal(_waterUseCategoryTypeids, form.PermitId);

            return form;
        }

        public IEnumerable<PendingPermit> GetPendingPermits()
        {
            var userName = HttpContext.Current.User.Identity.Name;
            var permits = _permittingService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("CreatedBy == @0", userName), new DynamicFilter("PermitStatusId == @0", 1) }, "PermitContacts.PermitContactInformation");
            var permitees = permits.SelectMany(x => x.PermitContacts).Where(x => x.IsPermittee);

            return permitees.Select(x => new PendingPermit
            {
                Permittee = x.PermitContactInformation.IsBusiness ? x.PermitContactInformation.BusinessName : x.PermitContactInformation.FirstName + " " + x.PermitContactInformation.LastName,
                Id = x.Permit.Id,
                UseDescription = x.Permit.UseDescription,
                CreatedDate = x.Permit.CreatedDate,
                CreatedBy = x.Permit.CreatedBy
            });
        }

        public int SaveApplicantInformation(ApplicationWizard form)
        {
            Permit permit = null;

            if (form.PermitId > 0)
            {
                permit = _permittingService.GetById(form.PermitId);
            }
            else
            {
                permit = new Permit();
                permit.RefId = form.RefId ?? null;
                permit.ApplicationTypeId = form.ApplicationTypeId ?? 1; // new application 
                permit.PermitStatusId = 1; // in progress 
            }

            permit.UseDescription = form.UseDescription;

            if (form.IsWaterUser && form.IsLandOwner)
            {
                form.PermitIssuedTo = 2; // The permit will be issued to the water user (by default)
            }

            permit.ApplicantIdentification = String.Format("E{0}{1}{2}{3}{4}", form.IsWaterUser ? 1 : 0, form.IsLandOwner ? 1 : 0, form.IsConsultant ? 1 : 0, form.PermitIssuedTo, "5");

            int permitId = _permittingService.Save(permit);

            if (permitId != 0)
            {
                //save Water use purposes
                SaveWaterUsePurposes(form, permitId);

                //Save Waste water treatment and disposal
                SaveWastewaterTreatmentAndDisposal(form);

                // delete existing permit contact from database
                DeleteExistingPermitContacts(permitId);

                if (form.PermitIssuedTo == 1)
                {
                    if (form.IsWaterUser)
                    {
                        form.PermitIssuedTo = 2; // the permit is issued to the water user
                    }
                    else if (form.IsLandOwner)
                    {
                        form.PermitIssuedTo = 3; // the permit is to the land owner 
                    }

                }

                //Save permit contacts 
                SavePermitContacts(form, permitId);

                return permitId;
            }

            return 0;
        }

        public List<ContactSearchModel> BuildResult(IEnumerable<Contact> result)
        {
            var formattedResult = new List<ContactSearchModel>();

            if (result != null)
            {
                formattedResult = result.Select(x => new ContactSearchModel
                {
                    FirstName = x.FirstName,
                    MiddleName = x.MiddleInitial,
                    LastName = x.LastName,
                    Address1 = x.Address1 + " " + x.City + " MD",
                    EmailAddress = x.ContactCommunicationMethods.Any(y => y.IsPrimaryEmail.HasValue && y.IsPrimaryEmail.Value) ? x.ContactCommunicationMethods.Single(y => y.IsPrimaryEmail.HasValue && y.IsPrimaryEmail.Value).CommunicationValue : "",
                    BusinessName = x.BusinessName,
                    ContactId = x.Id,
                    UserId = x.UserId
                }).ToList();
            }

            return formattedResult;
        }

        public IEnumerable<DynamicFilter> BuildFilter(ContactSearchForm searchForm, IEnumerable<int> contactIds)
        {
            var filter = new List<DynamicFilter>();
            string filterFormatter = String.Empty;
            var values = new List<Object>();

            if (!string.IsNullOrEmpty(searchForm.SearchFirstName))
            {
                values.Add(searchForm.SearchFirstName);

                filterFormatter = "FirstName.Contains(@0)";
            }
            if (!string.IsNullOrEmpty(searchForm.SearchLastName))
            {
                values.Add(searchForm.SearchLastName);
                if (filterFormatter.Length > 0)
                {
                    filterFormatter += " and LastName.Contains(@1)";
                }
                else
                {
                    filterFormatter = "LastName.Contains(@0)";
                }
            }

            if (!string.IsNullOrEmpty(searchForm.SearchBusinessName))
            {
                values.Add(searchForm.SearchBusinessName);
                if (!string.IsNullOrEmpty(searchForm.SearchLastName) && !string.IsNullOrEmpty(searchForm.SearchFirstName))
                {
                    filterFormatter += "or BusinessName.Contains(@2)";
                }
                else if (filterFormatter.Length > 0)
                {
                    filterFormatter += "or BusinessName.Contains(@1)";
                }
                else
                {
                    filterFormatter += "BusinessName.Contains(@0)";
                }
            }

            if (contactIds.Count() > 0)
            {
                var index = values.Count;
                values.Add(contactIds);

                if (index > 0)
                {
                    filterFormatter += " or @" + index + ".Contains(outerIt.Id)";
                }
                else
                {
                    filterFormatter += "@" + index + ".Contains(outerIt.Id)";
                }
            }

            //Build filter  
            if (values.Count > 0)
            {
                filterFormatter += " and ContactTypeId != 9 and ContactTypeId != 10 and ContactTypeId != 30 and ContactTypeId != 20";
                filter.Add(new DynamicFilter(filterFormatter, values.ToArray()));
            }
            else
            {
                filter = null;
            }

            return filter;
        }

        public List<ContactForm> BuildContactForm(IEnumerable<Contact> contacts)
        {
            return Mapper.Map<IEnumerable<Contact>, IEnumerable<ContactForm>>(contacts).ToList();
        }

        #region Private Members

        private void SavePermitContacts(ApplicationWizard form, int permitId)
        {
            if (form.IsWaterUser && form.IsLandOwner)
            {
                // The Applicant only fills out the water user contact, so the following code tricks
                // the system into creating both a water user and land owner permit contact using
                // just the water user
                var waterUserAndLandOwnerContact = form.Contact;

                var contactId = SaveContact(waterUserAndLandOwnerContact);

                SavePermitContact(waterUserAndLandOwnerContact, contactId, permitId, ContactType.WaterUser, true, true);
                SavePermitContact(waterUserAndLandOwnerContact, contactId, permitId, ContactType.LandOwner, false, false);
            }
            else if (form.IsWaterUser && !form.IsLandOwner)
            {
                var waterUserContact = form.Contact;
                var landOwnerContact = form.Contacts.Single(x => x.ContactType == ContactType.LandOwner);

                var waterUserContactId = SaveContact(waterUserContact);
                var landOwnerContactId = SaveContact(landOwnerContact);

                SavePermitContact(waterUserContact, waterUserContactId, permitId, ContactType.WaterUser, true, form.PermitIssuedTo == 2 || form.PermitIssuedTo == 4);
                SavePermitContact(landOwnerContact, landOwnerContactId, permitId, ContactType.LandOwner, false, form.PermitIssuedTo == 3);
            }
            else if (!form.IsWaterUser && form.IsLandOwner)
            {
                var waterUserContact = form.Contacts.Single(x => x.ContactType == ContactType.WaterUser);
                var landOwnerContact = form.Contact;

                var waterUserContactId = SaveContact(waterUserContact);
                var landOwnerContactId = SaveContact(landOwnerContact);

                SavePermitContact(waterUserContact, waterUserContactId, permitId, ContactType.WaterUser, false, form.PermitIssuedTo == 2 || form.PermitIssuedTo == 4);
                SavePermitContact(landOwnerContact, landOwnerContactId, permitId, ContactType.LandOwner, true, form.PermitIssuedTo == 3);
            }
            else if (form.IsConsultant)
            {
                var waterUserContact = form.Contacts.Single(x => x.ContactType == ContactType.WaterUser);
                var landOwnerContact = form.Contacts.Single(x => x.ContactType == ContactType.LandOwner);

                if (form.PermitIssuedTo == 4)
                {
                    // If the user picks "Both", they only fill out the water user, so the land owner contact
                    // will be null so we override it to use the water user that they filled out.
                    landOwnerContact = form.Contacts.Single(x => x.ContactType == ContactType.WaterUser);
                }

                var consultantContact = form.Contact;

                var waterUserContactId = SaveContact(waterUserContact);
                var landOwnerContactId = SaveContact(landOwnerContact);
                var consultantContactId = SaveContact(consultantContact);

                SavePermitContact(waterUserContact, waterUserContactId, permitId, ContactType.WaterUser, false, form.PermitIssuedTo == 2 || form.PermitIssuedTo == 4);
                SavePermitContact(landOwnerContact, landOwnerContactId, permitId, ContactType.LandOwner, false, form.PermitIssuedTo == 3);
                SavePermitContact(consultantContact, consultantContactId, permitId, ContactType.Consultant, true, false);
            }
        }

        private int SaveContact(ContactForm form)
        {
            if (form == null) return 0;
            Contact contact;
            if (form.Id > 0)
            {
                //Existing
                contact = _contactService.GetById(form.Id);
                Mapper.Map(form, contact);
            }
            else
            {
                //New
                contact = Mapper.Map<ContactForm, Contact>(form);
            }

            form.Id = _contactService.Save(contact);

            //update communication methods 
            SaveCommunicationMethods(form);

            return form.Id;
        }

        private void SaveCommunicationMethods(ContactForm form)
        {
            var communicationMethods = _contactCommunicationMethodService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("ContactId == @0", form.Id) });

            var emailMethod = communicationMethods.FirstOrDefault(x => x.IsPrimaryEmail.HasValue && x.IsPrimaryEmail.Value);
            if (emailMethod != null)
            {
                emailMethod.CommunicationValue = form.EmailAddress.CommunicationValue;
                _contactCommunicationMethodService.Save(emailMethod);
            }
            else
            {
                var communicationMethod = new ContactCommunicationMethod
                {
                    IsPrimaryEmail = true,
                    ContactId = form.Id,
                    CommunicationValue = form.EmailAddress.CommunicationValue,
                    CommunicationMethodId = 3
                };
                _contactCommunicationMethodService.Save(communicationMethod);
            }

            var primaryPhoneMethod = communicationMethods.FirstOrDefault(x => x.IsPrimaryPhone.HasValue && x.IsPrimaryPhone.Value);
            if (primaryPhoneMethod != null)
            {
                primaryPhoneMethod.CommunicationValue = form.PrimaryTelephoneNumber.CommunicationValue;
                primaryPhoneMethod.CommunicationMethodId = form.PrimaryTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault();
                _contactCommunicationMethodService.Save(primaryPhoneMethod);
            }
            else
            {
                var communicationMethod = new ContactCommunicationMethod
                {
                    IsPrimaryPhone = true,
                    ContactId = form.Id,
                    CommunicationValue = form.PrimaryTelephoneNumber.CommunicationValue,
                    CommunicationMethodId = form.PrimaryTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault()
                };
                _contactCommunicationMethodService.Save(communicationMethod);
            }

            var alternatePhoneMethod = communicationMethods.FirstOrDefault(x => x.IsPrimaryPhone.HasValue && !x.IsPrimaryPhone.Value);
            if (alternatePhoneMethod != null)
            {
                alternatePhoneMethod.CommunicationValue = form.AltTelephoneNumber.CommunicationValue;
                alternatePhoneMethod.CommunicationMethodId = form.AltTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault();
                _contactCommunicationMethodService.Save(alternatePhoneMethod);
            }
            else
            {
                var communicationMethod = new ContactCommunicationMethod
                {
                    IsPrimaryPhone = false,
                    ContactId = form.Id,
                    CommunicationValue = form.AltTelephoneNumber.CommunicationValue,
                    CommunicationMethodId = form.AltTelephoneNumber.SelectedCommunicationMethodId.GetValueOrDefault()
                };
                _contactCommunicationMethodService.Save(communicationMethod);
            }
        }

        private void SavePermitContact(ContactForm form, int contactId, int permitId, ContactType contactType, bool isApplicant, bool isPermittee)
        {
            var permitContactInformationId = SavePermitContactInformation(form);
            var permitContact = CreatePermitContact(contactId, permitId, contactType, permitContactInformationId, isApplicant, isPermittee);
            _permitContactService.Save(permitContact);
        }

        private int SavePermitContactInformation(ContactForm form)
        {
            if (form == null) return 0;

            var contactInformation = Mapper.Map<ContactForm, PermitContactInformation>(form);

            return _permitContactInformationService.Save(contactInformation);
        }

        private void AddContactFromPermitContactInformation(ApplicationWizard form, PermitContact permitContact, ContactType contactType)
        {
            ContactForm contact;

            if (permitContact != null && permitContact.PermitContactInformation != null)
            {
                contact = Mapper.Map<PermitContact, ContactForm>(permitContact);
            }
            else
            {
                contact = Mapper.Map<PermitContact, ContactForm>(new PermitContact());
            }

            contact.ContactType = contactType;

            // Get the communication Method
            var communicationMethods = _contactCommunicationMethodService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("ContactId == @0", contact.Id) });
            var emailMethod = communicationMethods.FirstOrDefault(x => x.IsPrimaryEmail.HasValue && x.IsPrimaryEmail.Value);
            if (emailMethod != null)
            {
                contact.EmailAddress = new CommunicationMethodForm
                {
                    CommunicationValue = emailMethod.CommunicationValue,
                    SelectedCommunicationMethodId = emailMethod.CommunicationMethodId
                };
            }

            var primaryPhoneMethod = communicationMethods.FirstOrDefault(x => x.IsPrimaryPhone.HasValue && x.IsPrimaryPhone.Value);
            if (primaryPhoneMethod != null)
            {
                contact.PrimaryTelephoneNumber = new CommunicationMethodForm
                {
                    CommunicationValue = primaryPhoneMethod.CommunicationValue,
                    SelectedCommunicationMethodId = primaryPhoneMethod.CommunicationMethodId
                };
            }

            var alternatePhoneMethod = communicationMethods.FirstOrDefault(x => x.IsPrimaryPhone.HasValue && !x.IsPrimaryPhone.Value);
            if (alternatePhoneMethod != null)
            {
                contact.AltTelephoneNumber = new CommunicationMethodForm
                {
                    CommunicationValue = alternatePhoneMethod.CommunicationValue,
                    SelectedCommunicationMethodId = alternatePhoneMethod.CommunicationMethodId
                };
            }

            form.Contacts.Add(contact);
        }

        private void SaveWaterUsePurposes(ApplicationWizard form, int permitId)
        {
            form.SavedWaterWithdrawalPurpose = new List<PermitWaterWithdrawalPurpose>();

            //Delete saved PermitWaterWithdrawalPurposes if any
            _permitWaterWithdrawalPurposeService.DeleteAll(permitId);

            //Save PermitWaterWithdrawalPurposes if any
            if (form.SelectedWaterUsePurposeIds == null) return;
            var ids = form.SelectedWaterUsePurposeIds.Split(',');

            if (ids.Length <= 0) return;
            foreach (var id in ids)
            {
                if (string.IsNullOrEmpty(id) || id == "undefined") continue;
                var model = new PermitWaterWithdrawalPurpose
                {
                    PermitId = permitId,
                    WaterWithdrawalPurposeId = Convert.ToInt32(id)
                };

                if (model.WaterWithdrawalPurposeId == 7)
                {
                    model.TotalIrrigatedAcres = form.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres;
                }

                model.Id = _permitWaterWithdrawalPurposeService.Save(model);
                form.SavedWaterWithdrawalPurpose.Add(model);
            }
        }

        private void SaveWastewaterTreatmentAndDisposal(ApplicationWizard form)
        {
            //Waste water treatment and disposal
            if (form.WastewaterTreatmentDisposalForm != null && form.WastewaterTreatmentDisposalForm.Count > 0)
            {
                var entity = form.SavedWaterWithdrawalPurpose.Where(x => x.WaterWithdrawalPurposeId > (int)WaterWithdrawalPurpose.AquacultureAndAquarium);
                if (entity != null)
                {
                    if (form.WastewaterTreatmentDisposalForm != null)
                    {
                        foreach (var _form in form.WastewaterTreatmentDisposalForm)
                        {
                            var model = Mapper.Map<WastewaterTreatmentAndDisposalForm, PermitWastewaterTreatmentDisposal>(_form);
                            var _entity = entity.SingleOrDefault(x => x.WaterWithdrawalPurposeId == _form.WaterWithdrawalPurposeId);
                            if (_entity != null)
                            {
                                model.PermitWaterWithdrawalPurposeId = _entity.Id;
                                _permitWastewaterTreatmentDisposalService.Save(model);
                            }
                        }
                    }
                }
            }
        }

        private void GetPermitContactInformation(ApplicationWizard form, int permitId)
        {
            var permitContacts = _permitContactService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitId==" + permitId) }, "PermitContactInformation,Contact.ContactCommunicationMethods");

            // Get Applicant contact information
            var permitContact = permitContacts.FirstOrDefault(x => x.IsApplicant);
            if (permitContact != null)
            {
                form.Contact = Mapper.Map<PermitContact, ContactForm>(permitContact);
            }
            else
            {
                //this should not happen 
                form.Contact = Mapper.Map<PermitContact, ContactForm>(new PermitContact());
            }

            // Get land owner and water user contact if any
            var contacts = permitContacts.Where(x => !x.IsApplicant);
            if (form.IsWaterUser && form.IsLandOwner)
            {
                var waterUserContact = Mapper.Map<Contact, ContactForm>(new Contact());
                var landOwnerContact = Mapper.Map<Contact, ContactForm>(new Contact());

                waterUserContact.ContactType = ContactType.WaterUser;
                landOwnerContact.ContactType = ContactType.LandOwner;
                form.Contacts.Add(waterUserContact);
                form.Contacts.Add(landOwnerContact);
            }
            else if (form.IsWaterUser && !form.IsLandOwner)
            {
                var waterUserContact = Mapper.Map<Contact, ContactForm>(new Contact());
                waterUserContact.ContactType = ContactType.WaterUser;
                form.Contacts.Add(waterUserContact);

                var _permitContact = contacts.SingleOrDefault(x => x.ContactTypeId == (int)ContactType.LandOwner && x.IsPrimary);
                AddContactFromPermitContactInformation(form, _permitContact, ContactType.LandOwner);
            }

            else if (!form.IsWaterUser && form.IsLandOwner)
            {
                var _permitContact = contacts.SingleOrDefault(x => x.ContactTypeId == (int)ContactType.WaterUser && x.IsPrimary);
                AddContactFromPermitContactInformation(form, _permitContact, ContactType.WaterUser);

                var landOwnerContact = Mapper.Map<Contact, ContactForm>(new Contact());
                landOwnerContact.ContactType = ContactType.LandOwner;
                form.Contacts.Add(landOwnerContact);
            }
            else
            {
                var landownerPermitContact = contacts.SingleOrDefault(x => x.ContactTypeId == (int)ContactType.LandOwner && x.IsPrimary);
                var waterUserPermitContact = contacts.SingleOrDefault(x => x.ContactTypeId == (int)ContactType.WaterUser && x.IsPrimary);
                AddContactFromPermitContactInformation(form, waterUserPermitContact, ContactType.WaterUser);
                AddContactFromPermitContactInformation(form, landownerPermitContact, ContactType.LandOwner);
            }
        }

        private void DeleteExistingPermitContacts(int permitId)
        {
            var permitContacts = _permitContactService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitId == @0 && @1.Contains(outerIt.ContactTypeId)", permitId, new[] { 8, 16, 27 }) });

            if (permitContacts.Any())
            {
                foreach (var id in permitContacts.Select(x => x.Id))
                {
                    _permitContactService.Delete(id);
                }
            }
        }

        #region Factory Methods

        private static PermitContact CreatePermitContact(int contactId, int permitId, ContactType contactType, int permitContactInformationId, bool isApplicant, bool isPermittee)
        {
            return new PermitContact
            {
                ContactId = contactId,
                ContactTypeId = (int)contactType,
                PermitContactInformationId = permitContactInformationId,
                PermitId = permitId,
                IsApplicant = isApplicant,
                IsPrimary = true,
                IsPermittee = isPermittee,
                Active = true,
                Sequence = null,
            };
        }

        #endregion

        #endregion
    }
}