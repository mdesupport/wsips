﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.MapResolvers
{
    public class PermitWaterWithdrawalPurposeListResolver : ValueResolver<PermitWaterWithdrawalLocation, IDictionary<PermitWaterWithdrawalPurposeForm, bool>>
    {
        private readonly IService<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeService;

        public PermitWaterWithdrawalPurposeListResolver(IService<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeService)
        {
            _permitWaterWithdrawalPurposeService = permitWaterWithdrawalPurposeService;
        }

        protected override IDictionary<PermitWaterWithdrawalPurposeForm, bool> ResolveCore(PermitWaterWithdrawalLocation source)
        {
            IDictionary<PermitWaterWithdrawalPurposeForm, bool> result = new Dictionary<PermitWaterWithdrawalPurposeForm, bool>();

            var purposeList = _permitWaterWithdrawalPurposeService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("PermitId=" + source.PermitId) }, "LU_WaterWithdrawalPurpose");

            foreach (var purpose in purposeList)
            {
                var selected = source.SelectedPermitWaterWithdrawalPurposes == null ? false : source.SelectedPermitWaterWithdrawalPurposes.Contains(purpose.Id);

                result.Add(Mapper.Map<PermitWaterWithdrawalPurpose, PermitWaterWithdrawalPurposeForm>(purpose), selected);
            }

            return result;
        }
    }
}