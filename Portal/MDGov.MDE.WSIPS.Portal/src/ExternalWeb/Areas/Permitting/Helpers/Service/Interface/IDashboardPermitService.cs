﻿using System.Collections.Generic;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service
{
    public interface IDashboardPermitService
    {
        DashboardSearchResult GetRange(int skip, int take, string orderBy, DashboardFilterWrapper filterWrapper, string includeProperties = null);

        DashboardPermit GetById(int id);
    }
}