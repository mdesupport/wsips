﻿using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service
{
    public interface IPermitLocationService
    {
        int Save(PermitLocation entity);

        void Delete(int id);
    }
}