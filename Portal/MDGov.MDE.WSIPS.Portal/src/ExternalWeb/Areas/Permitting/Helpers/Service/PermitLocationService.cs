﻿using System.Net;
using System.Net.Http;
using System.Web;
using MDGov.MDE.Common;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service
{
    public class PermitLocationService : HttpClientBase<PermitLocation>, IPermitLocationService
    {
        public int Save(PermitLocation entity)
        {
            entity.LastModifiedBy = HttpContext.Current.User.Identity.Name;

            var response = Client.PostAsXmlAsync(UriTemplateHelper.SaveTemplate, entity).Result;

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                return response.Content.ReadAsAsync<int>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return 0;
            }
        }

        public void Delete(int id)
        {
            var response = Client.DeleteAsync(string.Format(UriTemplateHelper.DeleteTemplate, id)).Result;

            if (!response.IsSuccessStatusCode)
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);
            }
        }
        
        private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
        {
            if (statusCode == HttpStatusCode.NotFound)
            {
                exception.ExceptionType = "NotFound";
                exception.ExceptionMessage = exception.Message;
            }
            var logger = ObjectFactory.GetInstance<IErrorLogging>();
            logger.Log(exception);
        }
    }
}