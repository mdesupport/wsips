﻿using System;
using System.Net;
using System.Net.Http;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Service;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service
{
    public class PermitWaterWithdrawalPurposeService : UpdatableService<PermitWaterWithdrawalPurpose>, IPermitWaterWithdrawalPurposeService
    {
        public void DeleteAll(int permitId)
        {
            var response = Client.DeleteAsync(String.Format("DeleteAll?permitId={0}", permitId)).Result;

            if (!response.IsSuccessStatusCode)
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);
            }
        }

        private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
        {
            if (statusCode == HttpStatusCode.NotFound)
            {
                exception.ExceptionType = "NotFound";
                exception.ExceptionMessage = exception.Message;
            }

            var logger = ObjectFactory.GetInstance<IErrorLogging>();
            logger.Log(exception);
        }
    }
}