﻿using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service
{
    public class StateStreamService : ServiceBase<LU_StateStream>, IStateStreamService
    {
        public IEnumerable<LU_StateStream> GetByWatershed(double x, double y, int srid)
        {
            var response = Client.GetAsync(string.Format("GetByWatershed?x={0}&y={1}&srid={2}", x, y, srid)).Result;

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                return response.Content.ReadAsAsync<IEnumerable<LU_StateStream>>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                throw new Exception(exception.Message);
            }
        }
    }
}