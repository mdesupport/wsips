﻿using System;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface
{
    public interface IPermitService : IUpdatableService<Permit>
    {
        int ClonePermit(int PermitId, int PermitTypeId, string PermitNumber, string userName);

        string GeneratePermitNumber(int permitId);

        bool IsUniquePermitNumber(string PermitNumber);

        int CalculateStatus(int permitId);

        bool ChangeStatus(int permitId);

        DateTime? GetStatusChangedDate(int permitId, int permitStatusId);

        bool CancelPermitApplication(int permitId);
    }
}
