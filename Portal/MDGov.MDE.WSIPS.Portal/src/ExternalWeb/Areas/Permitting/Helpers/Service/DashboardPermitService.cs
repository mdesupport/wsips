﻿using System.Net;
using System.Net.Http;
using MDGov.MDE.Common;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service
{
    public class DashboardPermitService : HttpClientBase<DashboardPermit>, IDashboardPermitService
    {
        public DashboardSearchResult GetRange(int skip, int take, string orderBy, DashboardFilterWrapper filterWrapper, string includeProperties = null)
        {
            var response = Client.PostAsJsonAsync(string.Format(UriTemplateHelper.RangeTemplate, skip, take, orderBy) + string.Format("&includeProperties={0}", includeProperties), filterWrapper).Result;

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                return response.Content.ReadAsAsync<DashboardSearchResult>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                //var exception = new CustomException(new ApiException { ExceptionMessage = "Sample Exception" });
                HandleApiException(exception, response.StatusCode);

                return null;
            }
        }

        public DashboardPermit GetById(int id)
        {
            var response = Client.GetAsync(string.Format(UriTemplateHelper.IdTemplate, id)).Result;

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                return response.Content.ReadAsAsync<DashboardPermit>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return null;
            }
        }

        private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
        {
            if (statusCode == HttpStatusCode.NotFound)
            {
                exception.ExceptionType = "NotFound";
                exception.ExceptionMessage = exception.Message;
            }
            var logger = ObjectFactory.GetInstance<IErrorLogging>();
            logger.Log(exception);
        }
    }
}