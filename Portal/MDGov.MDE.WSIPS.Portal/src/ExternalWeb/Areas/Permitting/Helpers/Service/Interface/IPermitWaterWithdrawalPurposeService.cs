﻿using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface
{
    public interface IPermitWaterWithdrawalPurposeService : IUpdatableService<PermitWaterWithdrawalPurpose>
    {
        void DeleteAll(int permitId);
    }
}
