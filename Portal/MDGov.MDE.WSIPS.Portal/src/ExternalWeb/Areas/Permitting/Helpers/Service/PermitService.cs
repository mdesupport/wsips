﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using MDGov.MDE.Common.Configuration;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Service;
using StructureMap;
using System.Web.Http;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service
{
    public class PermitService : UpdatableService<Permit>, IPermitService
    {
        public int ClonePermit(int PermitId, int PermitTypeId, string PermitNumber, string userName)
        {
            var response = Client.GetAsync(String.Format("ClonePermit?PermitId={0}&PermitTypeId={1}&PermitNumber={2}&userName={3}", PermitId, PermitTypeId, PermitNumber, userName)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<int>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return 0;
            }
        }

        public string GeneratePermitNumber(int permitId)
        {
            var response = Client.GetAsync(String.Format("GeneratePermitNumber?PermitId={0}", permitId)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<string>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return string.Empty;
            }
        }

        public bool IsUniquePermitNumber(string PermitNumber)
        {
            var response = Client.GetAsync(String.Format("IsUniquePermitNumber?PermitNumber={0}", PermitNumber)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public int CalculateStatus(int permitId)
        {
            var response = Client.GetAsync(String.Format("CalculateStatus?permitId={0}", permitId)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<int>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return 0;
            }
        }

        public bool ChangeStatus(int permitId)
        {
            var response = Client.GetAsync(String.Format("ChangeStatus?permitId={0}", permitId)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public DateTime? GetStatusChangedDate(int permitId, int permitStatusId)
        {
            var response = Client.GetAsync(String.Format("GetStatusChangedDate?permitId={0}&permitStatusId={1}", permitId, permitStatusId)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<DateTime?>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return null;
            }
        }

        public bool CancelPermitApplication(int permitId)
        {
            var response = Client.PostAsync(String.Format("CancelPermitApplication?permitId={0}", permitId), new StringContent(String.Empty), GlobalConfiguration.Configuration.Formatters.JsonFormatter).Result;

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                return response.Content.ReadAsAsync<bool>().Result;
            }

            var exception = response.Content.ReadAsAsync<ApiException>().Result;
            HandleApiException(exception, response.StatusCode);

            return false;
        }

        private void HandleApiException(ApiException exception, HttpStatusCode statusCode)
        {
            if (statusCode == HttpStatusCode.NotFound)
            {
                exception.ExceptionType = "NotFound";
                exception.ExceptionMessage = exception.Message;
            }

            var logger = ObjectFactory.GetInstance<IErrorLogging>();
            logger.Log(exception);
        }
    }
}