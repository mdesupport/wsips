﻿using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface
{
    public interface IStateStreamService : IService<LU_StateStream>
    {
        IEnumerable<LU_StateStream> GetByWatershed(double x, double y, int srid);
    }
}
