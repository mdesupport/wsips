﻿using System;
using System.Linq;
using AutoMapper;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.MapResolvers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.MapResolvers;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Configuration
{
    public class PermittingAreaAutoMapperProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "Permitting";
            }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Contact, ContactForm>()
                .ForMember(dest => dest.ContactType, opt => opt.MapFrom(src => (ContactType)src.ContactTypeId))
                .ForMember(dest => dest.PrimaryTelephoneNumber, opt => opt.MapFrom(src => GetCommunicationMethod(src.ContactCommunicationMethods.FirstOrDefault(t => t.IsPrimaryPhone.HasValue && t.IsPrimaryPhone.Value))))
                .ForMember(dest => dest.AltTelephoneNumber, opt => opt.MapFrom(src => GetCommunicationMethod(src.ContactCommunicationMethods.FirstOrDefault(t => t.IsPrimaryPhone.HasValue && !t.IsPrimaryPhone.Value))))
                .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => GetCommunicationMethod(src.ContactCommunicationMethods.FirstOrDefault(t => t.IsPrimaryEmail.HasValue && t.IsPrimaryEmail.Value))))
                .ForMember(dest => dest.CommunicationMethodList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_CommunicationMethod, Contact>>())
                .ForMember(dest => dest.StateList, opt => opt.ResolveUsing<StateDropdownListResolver<Contact>>());
            Mapper.CreateMap<ContactForm, Contact>()
                .ForMember(dest => dest.ContactTypeId, opt => opt.MapFrom(src => (int)src.ContactType))
                .ForMember(dest => dest.LU_ContactType, opt => opt.Ignore());

            Mapper.CreateMap<PermitContact, ContactForm>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ContactId))
                .ForMember(dest => dest.ContactType, opt => opt.MapFrom(src => (ContactType)src.ContactTypeId.GetValueOrDefault()))
                .ForMember(dest => dest.PrimaryTelephoneNumber, opt => opt.MapFrom(src => GetCommunicationMethod(src.Contact.ContactCommunicationMethods.FirstOrDefault(t => t.IsPrimaryPhone.HasValue && t.IsPrimaryPhone.Value))))
                .ForMember(dest => dest.AltTelephoneNumber, opt => opt.MapFrom(src => GetCommunicationMethod(src.Contact.ContactCommunicationMethods.FirstOrDefault(t => t.IsPrimaryPhone.HasValue && !t.IsPrimaryPhone.Value))))
                .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => GetCommunicationMethod(src.Contact.ContactCommunicationMethods.FirstOrDefault(t => t.IsPrimaryEmail.HasValue && t.IsPrimaryEmail.Value))))
                .ForMember(dest => dest.CommunicationMethodList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_CommunicationMethod, PermitContact>>())
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Contact.UserId))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.PermitContactInformation.FirstName))
                .ForMember(dest => dest.MiddleInitial, opt => opt.MapFrom(src => src.PermitContactInformation.MiddleInitial))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.PermitContactInformation.LastName))
                /*.ForMember(dest => dest.SecondaryName, opt => opt.MapFrom(src => src.PermitContactInformation.SecondaryName))*/
                .ForMember(dest => dest.BusinessName, opt => opt.MapFrom(src => src.PermitContactInformation.BusinessName))
                .ForMember(dest => dest.Address1, opt => opt.MapFrom(src => src.PermitContactInformation.Address1))
                .ForMember(dest => dest.Address2, opt => opt.MapFrom(src => src.PermitContactInformation.Address2))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.PermitContactInformation.City))
                .ForMember(dest => dest.StateId, opt => opt.MapFrom(src => src.PermitContactInformation.StateId))
                .ForMember(dest => dest.ZipCode, opt => opt.MapFrom(src => src.PermitContactInformation.ZipCode))
                .ForMember(dest => dest.IsBusiness, opt => opt.MapFrom(src => src.PermitContactInformation.IsBusiness))
                .ForMember(dest => dest.StateList, opt => opt.ResolveUsing<StateDropdownListResolver<PermitContact>>());

            Mapper.CreateMap<ContactForm, PermitContactInformation>()
                .ForMember(dest => dest.Id, opt => opt.UseDestinationValue())
                .ForMember(dest => dest.CountryId, opt => opt.UseValue(1));

            Mapper.CreateMap<PermitWwPoultry, PoultryWateringForm>()
                .ForMember(dest => dest.PoultryCoolingSystemList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_PoultryCoolingSystem, PermitWwPoultry>>())
                .ForMember(dest => dest.PoultryTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_PoultryType, PermitWwPoultry>>());
            Mapper.CreateMap<PoultryWateringForm, PermitWwPoultry>();

            Mapper.CreateMap<PermitWwLivestock, LivestockForm>();
            Mapper.CreateMap<LivestockForm, PermitWwLivestock>();

            Mapper.CreateMap<PermitWwFarmPotableSuppliesForm, PermitWwFarmPortableSupply>();
            Mapper.CreateMap<PermitWwFarmPortableSupply, PermitWwFarmPotableSuppliesForm>()
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent));

            Mapper.CreateMap<PermitWwFoodProcessingForm, PermitWwFoodProcessing>()
                .ForMember(des => des.AverageGallonPerDayFromGroundWater, opt => opt.MapFrom(src => Int32.Parse(src.AverageGallonPerDayFromGroundWater.Replace(",", ""))))
                .ForMember(des => des.AverageGallonPerDayFromSurfaceWater, opt => opt.MapFrom(src => Int32.Parse(src.AverageGallonPerDayFromSurfaceWater.Replace(",", ""))))
                .ForMember(des => des.MaximumGallonPerDayFromGroundWater, opt => opt.MapFrom(src => Int32.Parse(src.MaximumGallonPerDayFromGroundWater.Replace(",", ""))))
                .ForMember(des => des.MaximumGallonPerDayFromSurfaceWater, opt => opt.MapFrom(src => Int32.Parse(src.MaximumGallonPerDayFromSurfaceWater.Replace(",", ""))));
            Mapper.CreateMap<PermitWwFoodProcessing, PermitWwFoodProcessingForm>()
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent));

            Mapper.CreateMap<PermitWwAquacultureAquariumForm, PermitWwAquacultureOrAquarium>()
                .ForMember(des => des.AverageGallonPerDayFromGroundWater, opt => opt.MapFrom(src => Int32.Parse(src.AverageGallonPerDayFromGroundWater.Replace(",", ""))))
                .ForMember(des => des.AverageGallonPerDayFromSurfaceWater, opt => opt.MapFrom(src => Int32.Parse(src.AverageGallonPerDayFromSurfaceWater.Replace(",", ""))))
                .ForMember(des => des.MaximumGallonPerDayFromGroundWater, opt => opt.MapFrom(src => Int32.Parse(src.MaximumGallonPerDayFromGroundWater.Replace(",", ""))))
                .ForMember(des => des.MaximumGallonPerDayFromSurfaceWater, opt => opt.MapFrom(src => Int32.Parse(src.MaximumGallonPerDayFromSurfaceWater.Replace(",", ""))));
            Mapper.CreateMap<PermitWwAquacultureOrAquarium, PermitWwAquacultureAquariumForm>()
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent));

            Mapper.CreateMap<PermitWwNonAgricultureIrrigationForm, PermitWwNonAgricultureIrrigation>();
            Mapper.CreateMap<PermitWwNonAgricultureIrrigation, PermitWwNonAgricultureIrrigationForm>()
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent));

            Mapper.CreateMap<PermitWwGolfCourseIrrigationForm, PermitWwGolfCourseIrrigation>();
            Mapper.CreateMap<PermitWwGolfCourseIrrigation, PermitWwGolfCourseIrrigationForm>()
                .ForMember(dest => dest.GolfIrrigationSystemTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_GolfIrrigationSystemType, PermitWwGolfCourseIrrigation>>())
                .ForMember(dest => dest.IrrigationLayoutTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_IrrigationLayoutType, PermitWwGolfCourseIrrigation>>())
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent));

            Mapper.CreateMap<PermitWwCommercialDrinkingOrSanitary, PermitWwCommercialDrinkingOrSanitaryForm>()
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent))
                .ForMember(dest => dest.RestaurantTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_RestaurantType, PermitWwCommercialDrinkingOrSanitary>>())
                .ForMember(dest => dest.FacilityList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_FacilityType, PermitWwCommercialDrinkingOrSanitary>>());
            Mapper.CreateMap<PermitWwCommercialDrinkingOrSanitaryForm, PermitWwCommercialDrinkingOrSanitary>();

            Mapper.CreateMap<PermitWwInstitutionalEducational_drinkingOrsanitary, PermitWwEducationalDrinkingOrsanitaryForm>()
                .ForMember(dest => dest.EducationInstituteTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_EducationInstituteType, PermitWwInstitutionalEducational_drinkingOrsanitary>>())
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent));

            Mapper.CreateMap<PermitWwEducationalDrinkingOrsanitaryForm, PermitWwInstitutionalEducational_drinkingOrsanitary>()
                .ForMember(des => des.AverageGallonPerDayFromGroundWater, opt => opt.MapFrom(src => Int32.Parse(src.AverageGallonPerDayFromGroundWater.Replace(",", ""))))
                .ForMember(des => des.AverageGallonPerDayFromSurfaceWater, opt => opt.MapFrom(src => Int32.Parse(src.AverageGallonPerDayFromSurfaceWater.Replace(",", ""))))
                .ForMember(des => des.MaximumGallonPerDayFromGroundWater, opt => opt.MapFrom(src => Int32.Parse(src.MaximumGallonPerDayFromGroundWater.Replace(",", ""))))
                .ForMember(des => des.MaximumGallonPerDayFromSurfaceWater, opt => opt.MapFrom(src => Int32.Parse(src.MaximumGallonPerDayFromSurfaceWater.Replace(",", ""))));

            Mapper.CreateMap<PermitWwInstitutionalReligious_DringkingOrSanitary, PermitWwReligiousDringkingOrSanitaryForm>()
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent));

            Mapper.CreateMap<PermitWwReligiousDringkingOrSanitaryForm, PermitWwInstitutionalReligious_DringkingOrSanitary>();

            Mapper.CreateMap<PermitWwInstitutionalReligious_DringkingOrSanitary, PermitWwWildlifePondForm>();
            Mapper.CreateMap<PermitWwWildlifePondForm, PermitWwInstitutionalReligious_DringkingOrSanitary>();

            Mapper.CreateMap<PermitWwConstructionDewatering, PermitWwConstructionDewateringForm>()
                .ForMember(dest => dest.UsePercentage, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurpose.UsePercent))
                .ForMember(dest => dest.WaterRemovalExcavationTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_WaterRemovalExcavationType, PermitWwConstructionDewatering>>());
            Mapper.CreateMap<PermitWwConstructionDewateringForm, PermitWwConstructionDewatering>();

            Mapper.CreateMap<PermitWwWildlifePond, PermitWwWildlifePondForm>();
            Mapper.CreateMap<PermitWwWildlifePondForm, PermitWwWildlifePond>();

            Mapper.CreateMap<PermitWwCrop, CropForm>()
                .ForMember(dest => dest.CropTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_CropType, PermitWwCrop>>())
                .ForMember(dest => dest.CropYieldUnitList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_CropYieldUnit, PermitWwCrop>>())
                .ForMember(dest => dest.IrrigationSystemTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_IrrigationSystemType, PermitWwCrop>>());
            Mapper.CreateMap<CropForm, PermitWwCrop>();

            Mapper.CreateMap<PermitWwAgricultural, PermitWwIrrigationForm>()
                .ForMember(dest => dest.IrrigationSystemTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_IrrigationSystemType, PermitWwAgricultural>>());
            Mapper.CreateMap<PermitWwIrrigationForm, PermitWwAgricultural>();

            Mapper.CreateMap<PermitWastewaterTreatmentDisposal, WastewaterTreatmentAndDisposalForm>()
                .ForMember(dest => dest.WaterDispersementTypeList, opt => opt.ResolveUsing<LookupDropdownListResolver<LU_WaterDispersementType, PermitWastewaterTreatmentDisposal>>());
            Mapper.CreateMap<WastewaterTreatmentAndDisposalForm, PermitWastewaterTreatmentDisposal>();

            Mapper.CreateMap<PermitWwPrivateWaterSupplier, PermitWwPrivateWaterSupplierForm>();
            Mapper.CreateMap<PermitWwPrivateWaterSupplierForm, PermitWwPrivateWaterSupplier>();

            Mapper.CreateMap<PermitWaterWithdrawalPurpose, PermitWaterWithdrawalPurposeForm>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.LU_WaterWithdrawalPurpose.Description));
            Mapper.CreateMap<PermitWaterWithdrawalLocation, WithdrawalLocationForm>()
                .ForMember(dest => dest.PermitWaterWithdrawalPurposeList, opt => opt.ResolveUsing<PermitWaterWithdrawalPurposeListResolver>().ConstructedBy(ObjectFactory.GetInstance<PermitWaterWithdrawalPurposeListResolver>))
                .ForMember(dest => dest.SelectedStateStreamId, opt => opt.MapFrom(src => src.StateStreamId));
            Mapper.CreateMap<WithdrawalLocationForm, PermitWaterWithdrawalLocation>()
                .ForMember(dest => dest.IsExistingWell, opt => opt.MapFrom(src => src.WithdrawalTypeId == 1 ? (bool?)src.IsExistingWell : null))
                .ForMember(dest => dest.StateStreamId, opt => opt.MapFrom(src => src.WithdrawalSourceId == 2 ? src.SelectedStateStreamId : null))
                .ForMember(dest => dest.OtherStreamName, opt => opt.MapFrom(src => src.SelectedStateStreamId == -1 ? src.OtherStreamName : null))
                .ForMember(dest => dest.SelectedPermitWaterWithdrawalPurposes, opt => opt.MapFrom(src => src.PermitWaterWithdrawalPurposeList == null ? null : src.PermitWaterWithdrawalPurposeList.Where(dict => dict.Value).Select(dict => dict.Key.Id)));

            Mapper.CreateMap<PermitContact, PermitListingForm>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PermitId))
                .ForMember(dest => dest.PermitNumber, opt => opt.MapFrom(src => src.Permit.PermitName));

            Mapper.CreateMap<PermitCondition, CustomConditionForm>();

            Mapper.CreateMap<ConditionCompliance, ConditionReport>()
                  .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.LU_PermitConditionComplianceStatus.Description))
                  .ForMember(dest => dest.ConditionComplianceStatusId, opt => opt.MapFrom(src => src.PermitConditionComplianceStatusId))
                  .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.ComplianceReportingEndDate))
                  .ForMember(dest => dest.DueDate, opt => opt.MapFrom(src => src.ComplianceReportingDueDate));

            Mapper.CreateMap<PumpageReport, PumpageReportDetailsForm>()
                .ForMember(dest => dest.SubmittedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.CropDescription, opt => opt.MapFrom(src => src.LU_CropType.Description));
            Mapper.CreateMap<PumpageReportDetailsForm, PumpageReport>()
                .ForMember(dest => dest.PumpageReportDetails, opt => opt.Ignore());
        }

        private CommunicationMethodForm GetCommunicationMethod(ContactCommunicationMethod communicationMethod)
        {
            var communicationMethodForm = new CommunicationMethodForm();

            if (communicationMethod != null)
            {
                communicationMethodForm.CommunicationValue = communicationMethod.CommunicationValue;
                communicationMethodForm.SelectedCommunicationMethodId = communicationMethod.CommunicationMethodId;
            }

            return communicationMethodForm;
        }
    }
}