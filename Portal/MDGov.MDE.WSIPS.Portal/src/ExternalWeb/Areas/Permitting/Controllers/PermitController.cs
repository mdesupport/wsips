﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.ServiceClient;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Controllers
{
    [Authorization]
    public partial class PermitController : BaseController
    {
        private readonly IMembershipAuthenticationService _membershipService;
        private readonly IPermitService _permittingService;
        private readonly IService<Contact> _contactService;
        private readonly IService<ContactCommunicationMethod> _contactCommunicationMethod;
        private readonly IPermitServiceClient _permitServiceClient;
        private readonly IService<LU_WaterWithdrawalSource> _waterWithdrawalSourceService;
        private readonly IService<LU_WaterWithdrawalType> _waterWithdrawalTypeService;
        private readonly IUpdatableService<PermitWithdrawalGroundwater> _permitWithdrawalGroundwaterService;
        private readonly IUpdatableService<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterService;
        private readonly IWaterUseDetailServiceClient _waterUseDetailsServiceClient;
        private readonly IUpdatableService<PermitContact> _permitContactService;
        private readonly IUpdatableService<PermitCounty> _permitCountyService;
        private readonly IUpdatableService<PermitParcel> _permitParcelService;
        private readonly IService<ProjectManagerCounty> _projectManagerCountyService;
        private readonly IService<LU_WaterWithdrawalPurposeCategory> _WaterWithdrawalPurposeCategory;
        private readonly IUpdatableService<PermitWaterWithdrawalLocation> _permitWaterWithdrawalLocationService;
        private readonly IService<LU_TableList> _tableListService;
        private readonly IService<Secretary> _secretaryService;
        private readonly IService<AdministrativeSpecialist> _administrativeSpecialistService;
        private readonly IPermitLocationService _permitLocationService;
        private readonly IStateStreamService _stateStreamService;
        private readonly IUpdatableService<Comment> _commentService;
        private readonly IUpdatableService<PermitCondition> _permitConditionService;
        private readonly IService<LU_PermitCategory> _permitCatagoryService;

        public PermitController(
            IMembershipAuthenticationService membershipService,
            IPermitService permittingService,
            IService<Contact> contactService,
            IService<ContactCommunicationMethod> contactCommunicationMethod,
            IPermitServiceClient permitServiceClient,
            IService<LU_WaterWithdrawalSource> waterWithdrawalSourceService,
            IService<LU_WaterWithdrawalType> waterWithdrawalTypeService,
            IUpdatableService<PermitWithdrawalGroundwater> permitWithdrawalGroundwaterService,
            IUpdatableService<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterService,
            IWaterUseDetailServiceClient waterUseDetailsServiceClient,
            IUpdatableService<PermitContact> permitContactService,
            IUpdatableService<PermitCounty> permitCountyService,
            IUpdatableService<PermitParcel> permitParcelService,
            IService<ProjectManagerCounty> projectManagerCountyService,
            IUpdatableService<PermitWaterWithdrawalLocation> permitWaterWithdrawalLocationService,
            IService<LU_WaterWithdrawalPurposeCategory> WaterWithdrawalPurposeCategory,
            IService<LU_TableList> tableListService,
            IService<Secretary> secretaryService,
            IService<AdministrativeSpecialist> administrativeSpecialistService,
            IPermitLocationService permitLocationService,
            IStateStreamService stateStreamService,
            IUpdatableService<Comment> commentService,
            IUpdatableService<PermitCondition> permitConditionService,
            IService<LU_PermitCategory> permitCatagoryService)
        {
            _membershipService = membershipService;
            _permittingService = permittingService;
            _contactService = contactService;
            _contactCommunicationMethod = contactCommunicationMethod;
            _permitServiceClient = permitServiceClient;
            _waterWithdrawalSourceService = waterWithdrawalSourceService;
            _waterWithdrawalTypeService = waterWithdrawalTypeService;
            _permitWithdrawalGroundwaterService = permitWithdrawalGroundwaterService;
            _permitWithdrawalSurfacewaterService = permitWithdrawalSurfacewaterService;
            _waterUseDetailsServiceClient = waterUseDetailsServiceClient;
            _permitContactService = permitContactService;
            _permitCountyService = permitCountyService;
            _permitParcelService = permitParcelService;
            _projectManagerCountyService = projectManagerCountyService;
            _WaterWithdrawalPurposeCategory = WaterWithdrawalPurposeCategory;
            _permitWaterWithdrawalLocationService = permitWaterWithdrawalLocationService;
            _tableListService = tableListService;
            _secretaryService = secretaryService;
            _administrativeSpecialistService = administrativeSpecialistService;
            _permitLocationService = permitLocationService;
            _stateStreamService = stateStreamService;
            _commentService = commentService;
            _permitConditionService = permitConditionService;
            _permitCatagoryService = permitCatagoryService;
        }

        #region Permit

        [HttpGet]
        public virtual ActionResult ApplicationWizard(int? id, int? applicationType)
        {
            int userId = SessionHandler.GetSessionVar<int>(SessionVariables.CurrentlyLoggedinUserId);
            if (userId != 0)
            {
                try
                {
                    ApplicationWizard form = null;

                    if (id.HasValue)
                    {
                        //Editing existing applicaiton 
                        form = _permitServiceClient.GetApplicationWizardForExistingPermit(id.Value);
                    }
                    else
                    {
                        form = _permitServiceClient.InitializeApplicationWizardFrom();
                        form.ApplicationTypeId = applicationType ?? 1;
                        if (TempData["RefId"] != null)
                        {
                            form.RefId = (int)TempData["RefId"];
                        }
                        else
                        {
                            form.RefId = null;
                        }
                    }

                    return View(form);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                    return View();
                }
            }
            else
            {
                return RedirectToAction(MVC.Security.Account.Login());
            }
        }

        [HttpPost]
        public virtual JsonResult ApplicationWizard(ApplicationWizard form, WaterUseDetailForm waterUserDetailForm)
        {
            try
            {
                form.PermitId = _permitServiceClient.SaveApplicantInformation(form);

                if (!form.NotSureWhatTheWaterUseTypeIs)
                {
                    form.WaterUseDetailForm.PrivateWaterSuppilerForm = waterUserDetailForm.PrivateWaterSuppilerForm;
                    _waterUseDetailsServiceClient.SaveWaterUseDetails(form);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { success = false });
            }

            return Json(new { success = true, permitId = form.PermitId, waterUserContactId = form.Contacts[0].Id, landOwnerContactId = form.Contacts[1].Id });
        }

        [HttpPost]
        public virtual JsonResult CancelPermitApplication(int permitId)
        {
            _permittingService.CancelPermitApplication(permitId);

            return Json(new { Success = true });
        }

        [HttpGet]
        public virtual ActionResult ListPermitApplication()
        {
            int userId = SessionHandler.GetSessionVar<int>(SessionVariables.CurrentlyLoggedinUserId);
            if (userId != 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction(MVC.Security.Account.Login());
            }
        }

        [HttpGet]
        public virtual JsonResult LoadPendingPermitApplication(GridCommand command)
        {
            try
            {
                var list = _permitServiceClient.GetPendingPermits();

                if (list != null && list.Count() > 0)
                {
                    int count = list.Count();
                    list = list.Skip(command.Skip()).Take(command.PageSize).ToList();
                    var pageTotal = (int)Math.Ceiling((float)count / (float)command.PageSize);

                    if (count > 0)
                    {
                        return Json(
                            new
                            {
                                Data = list,
                                PageTotal = pageTotal,
                                CurrentPage = command.Page,
                                RecordCount = count,
                            }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }

            return null;
        }

        [HttpGet]
        public virtual JsonResult GetPermitStatusesByCategory(int categoryId, bool filterStatuses = false)
        {
            if (Request.IsAuthenticated && !filterStatuses)
            {
                var statuses = _permitCatagoryService.GetById(categoryId, "LU_PermitStatus");

                return Json(statuses.LU_PermitStatus.Select(t => new { Id = t.Id, PermitCategoryId = t.PermitCategoryId, Description = t.Description }), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (categoryId == 2)
                {
                    return Json(new[] { new { Id = 19, PermitCategoryId = 2, Description = "Comment Period in Progress" } }, JsonRequestBehavior.AllowGet);
                }
                else if (categoryId == 4)
                {
                    return Json(new[] { new { Id = 79, PermitCategoryId = 4, Description = "Comment Period in Progress" } }, JsonRequestBehavior.AllowGet);
                }
            }

            return null;
        }

        [HttpGet]
        public virtual JsonResult SearchContact(GridCommand command, ContactSearchForm searchForm)
        {
            IEnumerable<DynamicFilter> filter = null;
            List<ContactSearchModel> formattedResult;
            List<ContactForm> contactForms;
            IEnumerable<int> _contactIds = new List<int>();
            IEnumerable<Contact> result = new List<Contact>();
            int count = 0;
            int pageTotal = 1;

            try
            {
                if (!string.IsNullOrEmpty(searchForm.SearchEmailAddress))
                {
                    var communicationMethod = _contactCommunicationMethod.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("CommunicationValue.Contains(@0)", searchForm.SearchEmailAddress) });
                    _contactIds = communicationMethod.Select(y => Convert.ToInt32(y.ContactId));
                }

                filter = _permitServiceClient.BuildFilter(searchForm, _contactIds);

                if (filter != null)
                {
                    count = _contactService.Count(filter);
                    result = _contactService.GetRange(command.Skip(), command.PageSize, command.SortField, filter, "ContactCommunicationMethods");
                }

                // Merg result 
                formattedResult = _permitServiceClient.BuildResult(result);
                contactForms = _permitServiceClient.BuildContactForm(result.ToList());

                pageTotal = (int)Math.Ceiling((float)count / (float)command.PageSize);

                return Json(
                           new
                           {
                               Data = formattedResult,
                               PageTotal = pageTotal,
                               CurrentPage = command.Page,
                               RecordCount = count,
                               ActualResult = contactForms
                           },
                           JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        #endregion

        #region Permit Detail

        [HttpGet]
        public virtual ActionResult Details(int id)
        {
            var permit = _permittingService.GetById(id, "PermitContacts.Contact,LU_PermitStatus,PermitSupplementalGroups.SupplementalGroup");

            if (Request.IsAuthenticated)
            {
                var currentUserContactId = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);

                if (!permit.PermitContacts.Any(x => x.ContactId == currentUserContactId))
                {
                    return View(MVC.Permitting.Permit.Views.AccessDenied);
                }
            }
            else
            {
                if (permit.PermitStatusId != 46 && ((permit.LU_PermitStatus.PermitCategoryId == 2 || permit.LU_PermitStatus.PermitCategoryId == 3 || permit.LU_PermitStatus.PermitCategoryId == 4) && (permit.PermitTypeId != 2 && permit.PermitTypeId != 4)))
                {
                    return View(MVC.Permitting.Permit.Views.AccessDenied);
                }
            }

            ViewBag.PermitStatusId = permit.PermitStatusId ?? 0;
            ViewBag.RefId = id;
            ViewBag.PermitTypeId = permit.PermitTypeId ?? 0;

            //external authority role required to view County Review and Approval tab
            if (_membershipService.IsInRole(User.Identity.Name.ToLower(), new String[] { "External Authority" })
                && SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactTypeId) == 9)
            {
                ViewBag.IsExternalAuthority = "true";
            }
            else
            {
                ViewBag.IsExternalAuthority = "false";
            }

            ViewBag.CurrentlyLoggedinContactTypeId = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactTypeId);

            ViewBag.CanEdit = "false";

            var permit2 = _permittingService.GetRange(0, 1, null, new DynamicFilter[] { new DynamicFilter("Id==" + id) }, "LU_PermitStatus");
            if (permit.LU_PermitStatus != null)
            {
                ViewBag.CategoryId = permit.LU_PermitStatus.PermitCategoryId;
                if (ViewBag.CategoryId == 1)
                {
                    ViewBag.PermitName = "Application";
                }
                else
                {
                    ViewBag.PermitName = permit.PermitName == null ? "No Permit #" : permit.PermitName;
                }
            }
            else
            {
                ViewBag.CategoryId = 0;
            }

            return View();
        }

        [HttpGet]
        public virtual ActionResult LoadDetails(int id)
        {
            var form = _permitServiceClient.GetApplicationWizardForExistingPermit(id);
            var permit = _permittingService.GetById(id, "LU_PermitStatus");

            form.ApplicationTypeId = permit.ApplicationTypeId;
            form.PermitId = permit.Id;
            form.PermitCategoryId = permit.LU_PermitStatus.PermitCategoryId ?? 0;
            form.RefId = permit.RefId;
            form.PermitStatusId = permit.PermitStatusId ?? 0;

            form.PermitStatusDetailForm.PermitStatusLastChangedDate = _permittingService.GetStatusChangedDate(permit.Id, permit.PermitStatusId.GetValueOrDefault()).GetValueOrDefault();
            form.PermitStatusDetailForm.PermitStatusDescription = permit.LU_PermitStatus.Description;
            form.PermitStatusDetailForm.RefId = permit.Id;
            form.PermitStatusDetailForm.PermitCategoryId = permit.LU_PermitStatus.PermitCategoryId ?? 0;
            form.PermitStatusDetailForm.PermitStatusId = permit.PermitStatusId ?? 0;

            form.IsExternalApplicaion = permit.ApplicantIdentification.Substring(0, 1) == "E";

            var relatedPermitCount = _permittingService.Count(new[] { new DynamicFilter("RefId == @0", permit.Id) });
            ViewBag.hasPendingPermit = relatedPermitCount > 0;
            ViewBag.RefId = permit.Id;

            return PartialView(MVC.Permitting.Permit.Views.DetailsPartials._Details, form);
        }

        [HttpGet]
        public virtual ActionResult LoadPermitHistory(int id)
        {
            //get all related permits
            var temp = GetRelatedPermits(id);
            ViewBag.RelatedPermits = temp;
            ViewBag.ActivePermit = id;


            // determine if this permit is part of supplemental group
            var permit = _permittingService.GetById(id, "PermitSupplementalGroups.SupplementalGroup");

            if (permit.PermitSupplementalGroups != null && permit.PermitSupplementalGroups.Count > 0)
            {
                ViewBag.SupplementalGroupId = permit.PermitSupplementalGroups.First().SupplementalGroupId;
                ViewBag.SupplementalGroupName = permit.PermitSupplementalGroups.First().SupplementalGroup.SupplementalGroupName;
            }
            else
                ViewBag.SupplementalGroupId = 0;


            return PartialView(MVC.Permitting.Permit.Views.DetailsPartials._DropDownPermitHistory);
        }

        [HttpGet]
        public virtual ActionResult LoadDocuments(int id, int permitStatus)
        {
            ViewBag.RefId = id;
            ViewBag.PermitStatusId = permitStatus;

            return PartialView(MVC.Permitting.Permit.Views.DetailsPartials._Documents);
        }

        [HttpGet]
        public virtual ActionResult LoadDNRReviewApproval(int permitid)
        {
            return PartialView(MVC.Permitting.Permit.Views.DetailsPartials._DNRReviewApproval);
        }

        [HttpGet]
        public virtual ActionResult LoadCountyReviewAndApprovalTab(int permitid)
        {
            var permit = _permittingService.GetById(permitid);

            if (permit == null)
            {
                throw new ArgumentException(permitid + " is not a valid permit Id", "permitid");
            }

            var item = new CountyReviewAndApprovalForm();
            item.PermitId = permit.Id;
            item.IsChecked = permit.CountyOfficialSignOffYesNo.HasValue && permit.CountyOfficialSignOffYesNo.Value;
            item.IsPastSubmission = permit.CountyOfficialSignOffDate.HasValue;
            item.CountyOfficialSignOffBy = permit.CountyOfficialSignOffBy;
            item.CountyOfficialSignOffDate = permit.CountyOfficialSignOffDate;

            var comment = _commentService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter(String.Format("CreatedBy==\"{0}\"", User.Identity.Name)), new DynamicFilter("RefId == " + permitid), new DynamicFilter("PermitStatusId == " + 4) });
            if (comment != null && comment.Any())
            {
                item.CommentId = comment.Last().Id;
                item.Comment = comment.Last().Comment1;
            }

            return PartialView(MVC.Permitting.Permit.Views.DetailsPartials._CountyReviewAndApproval, item);
        }

        [HttpGet]
        public virtual PartialViewResult LoadComplianceTab(int id)
        {
            return PartialView(MVC.Permitting.Permit.Views.DetailsPartials._Compliance);
        }

        #endregion

        #region application summary page

        private ApplicationSummary CreateApplicationSummary(int id, string waterDetailsSelected)
        {
            ApplicationSummary summary = new ApplicationSummary();

            var permit = _permittingService.GetById(
                id,
                string.Join(",", new[]
                {
                    "LU_Aquifer",
                    "LU_PermitStatus",
                    "PermitConditions",
                    "PermitCounties.LU_County",
                    "PermitContacts.PermitContactInformation.LU_State",
                    "PermitContacts.Contact.ContactCommunicationMethods",
                    "PermitWaterWithdrawalPurposes.LU_WaterWithdrawalPurpose",
                    "PermitWithdrawalGroundwaters.PermitWithdrawalGroundwaterDetails.LU_WaterWithdrawalType",
                    "PermitWithdrawalGroundwaters.PermitWithdrawalGroundwaterDetails.LU_Watershed",
                    "PermitWithdrawalSurfacewaters.PermitWithdrawalSurfacewaterDetails.LU_WaterWithdrawalType",
                    "PermitWithdrawalSurfacewaters.PermitWithdrawalSurfacewaterDetails.LU_Watershed",
                    "PermitWithdrawalSurfacewaters.PermitWithdrawalSurfacewaterDetails.LU_StateStream"
                }));

            if (permit != null)
            {
                summary.AppWizard = new ApplicationWizard();
                summary.Permit = permit;

                int[] waterDetailIndex = (from x in waterDetailsSelected.Split(',')
                                          where !string.IsNullOrEmpty(x) && Convert.ToInt32(x) != 0
                                          select Convert.ToInt32(x)).ToArray();

                // if permit category is 1, received date should be included in the summary
                if (permit.LU_PermitStatus != null && permit.LU_PermitStatus.PermitCategoryId.HasValue)
                {
                    summary.AppWizard.PermitCategoryId = permit.LU_PermitStatus.PermitCategoryId.Value;

                    if (permit.LU_PermitStatus.PermitCategoryId == 1 && permit.ReceivedDate.HasValue)
                    {
                        summary.ReceivedDate = permit.ReceivedDate.ToString();
                    }
                    else if (permit.LU_PermitStatus.PermitCategoryId == 1 && !permit.ReceivedDate.HasValue)
                    {
                        summary.ReceivedDate = "Unknown";
                    }
                }

                if (permit.PermitContacts != null)
                {
                    // get applicant, water user, and land owner
                    summary.PrimaryLandOwner = GetSummaryContact(permit.PermitContacts.FirstOrDefault(x => x.IsPrimary && x.ContactTypeId == (int)ContactType.LandOwner));
                    summary.WaterUser = GetSummaryContact(permit.PermitContacts.FirstOrDefault(x => x.IsPrimary && x.ContactTypeId == (int)ContactType.WaterUser));
                    summary.Applicant = GetSummaryContact(permit.PermitContacts.FirstOrDefault(x => x.IsPrimary && x.IsApplicant));

                    // get permittee
                    var permiteeContact = permit.PermitContacts.FirstOrDefault(x => x.IsPermittee && x.IsPrimary);
                    if (permiteeContact != null && permiteeContact.PermitContactInformation != null)
                    {
                        if (permiteeContact.PermitContactInformation.IsBusiness && !string.IsNullOrEmpty(permiteeContact.PermitContactInformation.BusinessName))
                        {
                            summary.Permittee = permiteeContact.PermitContactInformation.BusinessName;
                        }
                        else
                        {
                            summary.Permittee = permiteeContact.PermitContactInformation.FirstName + (string.IsNullOrEmpty(permiteeContact.PermitContactInformation.MiddleInitial) ? " " : " " + permiteeContact.PermitContactInformation.MiddleInitial + " ") + permiteeContact.PermitContactInformation.LastName;
                        }
                    }

                    // get additional land owners
                    foreach (var additionalLandOwner in permit.PermitContacts.Where(x => !x.IsPrimary && x.ContactTypeId == (int)ContactType.LandOwner))
                    {
                        summary.AdditionalLandOwners.Add(GetSummaryContact(additionalLandOwner));
                    }
                }

                summary.AppWizard.UseDescription = permit.UseDescription;
                summary.AppWizard.WaterUseDetailForm = _waterUseDetailsServiceClient.InitializeWaterUseDetailFrom(id);
                summary.AppWizard.WastewaterTreatmentDisposalForm = _waterUseDetailsServiceClient.GetWasteWaterTreatmentAndDisposal(string.Join(",", waterDetailIndex), id).Where(x => x.Id != 0).ToList();

                // determine selected use categories
                if (waterDetailIndex != null && waterDetailIndex.Any())
                {
                    var values = _WaterWithdrawalPurposeCategory.GetAll("LU_WaterWithdrawalPurpose");

                    summary.SelectedUseDetailCategories = new List<string>();

                    foreach (var item in values)
                    {
                        foreach (var subitem in item.LU_WaterWithdrawalPurpose)
                        {

                            if (waterDetailIndex.Contains(subitem.Id))
                                summary.SelectedUseDetailCategories.Add(subitem.Description);
                        }
                    }
                }

                summary.PermitWithdrawalGroundwater = permit.PermitWithdrawalGroundwaters.FirstOrDefault();
                summary.PermitWithdrawalSurfacewater = permit.PermitWithdrawalSurfacewaters.FirstOrDefault();
                summary.PermitCounty = permit.PermitCounties.Any() ? permit.PermitCounties.First().LU_County.Description : "Not Known";
                summary.RequiresWithdrawalReporting = permit.PermitConditions.Any(x => x.StandardConditionTypeId == 14) ? "Yes" : "No";
            }

            return summary;
        }

        private string FormatAddress(PermitContactInformation address)
        {
            return address == null ? "" : ((address.Address1 == null ? "" : address.Address1.Trim().Length > 0 ? address.Address1.Trim() + ", " : "") +
                               (address.Address2 == null ? "" : (address.Address2.Trim().Length > 0 ? address.Address2.Trim() + ", " : "")) +
                               (address.City == null ? "" : (address.City.Trim().Length > 0 ? address.City.Trim() + ", " : "")) +
                               (address.StateId == null ? "" : address.LU_State == null ? "" : address.LU_State.Description + ", ") +
                               (address.ZipCode ?? ""));
        }

        private SummaryContact GetSummaryContact(PermitContact contact)
        {
            SummaryContact summaryContact = null;

            if (contact != null && contact.PermitContactInformationId.HasValue)
            {
                summaryContact = new SummaryContact
                {
                    ContactId = contact.ContactId,
                    Name = contact.PermitContactInformation.FirstName + (string.IsNullOrEmpty(contact.PermitContactInformation.MiddleInitial) ? " " : " " + contact.PermitContactInformation.MiddleInitial + " ") + contact.PermitContactInformation.LastName,
                    /*SecondaryName = contact.PermitContactInformation.SecondaryName,*/
                    Address = FormatAddress(contact.PermitContactInformation),
                    BusinessName = contact.PermitContactInformation.BusinessName
                };

                var communicationMethods = contact.Contact.ContactCommunicationMethods;

                if (communicationMethods != null)
                {
                    if (communicationMethods.Any(x => x.IsPrimaryEmail.HasValue && x.IsPrimaryEmail.Value))
                    {
                        summaryContact.Email = communicationMethods.Single(x => x.IsPrimaryEmail.HasValue && x.IsPrimaryEmail.Value).CommunicationValue;
                    }

                    if (communicationMethods.Any(x => x.IsPrimaryPhone.HasValue && x.IsPrimaryPhone.Value))
                    {
                        summaryContact.Phone = communicationMethods.Single(x => x.IsPrimaryPhone.HasValue && x.IsPrimaryPhone.Value).CommunicationValue;
                    }
                }
            }

            return summaryContact;
        }

        [HttpGet]
        public virtual ActionResult Summary(int id, string waterDetailsSelected)
        {
            return PartialView(MVC.Permitting.Permit.Views.Summary, CreateApplicationSummary(id, waterDetailsSelected));
        }

        #endregion

        #region create pdf for summary

        [HttpPost]
        public virtual FileResult CreatePdf(int id, string waterDetailsSelected)
        {
            var summary = CreateApplicationSummary(id, waterDetailsSelected);

            var memStream = new MemoryStream();

            var doc1 = new iTextSharp.text.Document();
            doc1.SetMargins(doc1.LeftMargin, doc1.RightMargin, 180, doc1.BottomMargin);
            var bfTimes = BaseFont.CreateFont();
            var boldText = new Font(bfTimes, 12, Font.BOLD, BaseColor.BLACK);

            var writer = PdfWriter.GetInstance(doc1, memStream);
            writer.CloseStream = false;
            writer.PageEvent = new PdfHeader();

            doc1.Open();

            var emptyParagraph = new Paragraph(" ");

            doc1.Add(new Paragraph(DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString()));
            doc1.Add(emptyParagraph);

            doc1.Add(new Phrase("Permit Number: ", boldText));
            doc1.Add(new Phrase(summary.Permit.PermitName));
            doc1.Add(emptyParagraph);

            if (!string.IsNullOrEmpty(summary.Permit.SubmittedBy))
            {
                doc1.Add(new Phrase("Submitted By: ", boldText));
                doc1.Add(new Phrase(summary.Permit.SubmittedBy));
                doc1.Add(emptyParagraph);
            }

            if (summary.AppWizard.PermitCategoryId == 1 && summary.Permit.SubmittedDate.HasValue)
            {
                doc1.Add(new Phrase("Submitted On: ", boldText));
                doc1.Add(new Phrase(summary.Permit.SubmittedDate.Value.ToShortDateString()));
                doc1.Add(emptyParagraph);
            }
            else if (summary.AppWizard.PermitCategoryId == 2 || summary.AppWizard.PermitCategoryId == 3 || summary.AppWizard.PermitCategoryId == 4 || summary.AppWizard.PermitCategoryId == 5)
            {
                if (summary.Permit.ReceivedDate.HasValue)
                {
                    var receivedOnParagraph = new Paragraph();
                    receivedOnParagraph.Add(new Phrase("Received On: ", boldText));
                    receivedOnParagraph.Add(new Phrase(summary.Permit.ReceivedDate.Value.ToShortDateString()));
                    doc1.Add(receivedOnParagraph);
                }
            }
            else if (summary.AppWizard.PermitCategoryId == 6)
            {
                if (summary.Permit.ReceivedDate.HasValue)
                {
                    var receivedOnParagraph = new Paragraph();
                    receivedOnParagraph.Add(new Phrase("Received On: ", boldText));
                    receivedOnParagraph.Add(new Phrase(summary.Permit.ReceivedDate.Value.ToShortDateString()));
                    doc1.Add(receivedOnParagraph);
                }

                if (summary.Permit.DateOut.HasValue)
                {
                    var issuedDateParagraph = new Paragraph();
                    issuedDateParagraph.Add(new Phrase("Issued Date: ", boldText));
                    issuedDateParagraph.Add(new Phrase(summary.Permit.DateOut.Value.ToShortDateString()));
                    doc1.Add(issuedDateParagraph);
                }

                if (summary.Permit.AppropriationDate.HasValue)
                {
                    var appropriationDateParagraph = new Paragraph();
                    appropriationDateParagraph.Add(new Phrase("First Appropriation Date: ", boldText));
                    appropriationDateParagraph.Add(new Phrase(summary.Permit.AppropriationDate.Value.ToShortDateString()));
                    doc1.Add(appropriationDateParagraph);
                }

                if (summary.Permit.EffectiveDate.HasValue)
                {
                    var effectiveDateParagraph = new Paragraph();
                    effectiveDateParagraph.Add(new Phrase("Effective Date: ", boldText));
                    effectiveDateParagraph.Add(new Phrase(summary.Permit.EffectiveDate.Value.ToShortDateString()));
                    doc1.Add(effectiveDateParagraph);
                }

                if (summary.Permit.ExpirationDate.HasValue)
                {
                    var expirationDateParagraph = new Paragraph();
                    expirationDateParagraph.Add(new Phrase("Expiration Date: ", boldText));
                    expirationDateParagraph.Add(new Phrase(summary.Permit.ExpirationDate.Value.ToShortDateString()));
                    doc1.Add(expirationDateParagraph);
                }

                doc1.Add(new Phrase("Requires Withdrawal Reporting? ", boldText));
                doc1.Add(new Phrase(summary.RequiresWithdrawalReporting));
                doc1.Add(emptyParagraph);
            }
            else if (summary.AppWizard.PermitCategoryId == 7)
            {
                if (summary.Permit.DateOut.HasValue)
                {
                    var issuedDateParagraph = new Paragraph();
                    issuedDateParagraph.Add(new Phrase("Issued Date: ", boldText));
                    issuedDateParagraph.Add(new Phrase(summary.Permit.DateOut.Value.ToShortDateString()));
                    doc1.Add(issuedDateParagraph);
                }
            }

            #region contacts

            // Applicant information
            doc1.Add(new Paragraph("Applicant", boldText));
            if (summary.Applicant != null)
            {
                if (!String.IsNullOrEmpty(summary.Applicant.BusinessName))
                    doc1.Add(new Paragraph(" Business Name: " + summary.Applicant.BusinessName));
                doc1.Add(new Paragraph(" Name: " + summary.Applicant.Name));
                /*doc1.Add(new Paragraph(" Secondary Name: " + summary.Applicant.SecondaryName));*/
                doc1.Add(new Paragraph(" Address: " + summary.Applicant.Address));
                doc1.Add(new Paragraph(" Phone: " + summary.Applicant.Phone));
                doc1.Add(new Paragraph(" Email: " + summary.Applicant.Email));
            }
            else
            {
                doc1.Add(new Paragraph(" Applicant information could not be found"));
            }

            doc1.Add(new Paragraph(" "));

            // Water user information
            doc1.Add(new Paragraph("Water User", boldText));
            if (summary.WaterUser != null)
            {
                if (summary.Applicant != null &&
                    (summary.Applicant.ContactId == summary.WaterUser.ContactId))
                {
                    doc1.Add(new Paragraph(" Same as Applicant"));
                }
                else
                {
                    if (!String.IsNullOrEmpty(summary.WaterUser.BusinessName))
                        doc1.Add(new Paragraph(" Business Name: " + summary.WaterUser.BusinessName));
                    doc1.Add(new Paragraph(" Name: " + summary.WaterUser.Name));
                    /*doc1.Add(new Paragraph(" Secondary Name: " + summary.WaterUser.SecondaryName));*/
                    doc1.Add(new Paragraph(" Address: " + summary.WaterUser.Address));
                    doc1.Add(new Paragraph(" Phone: " + summary.WaterUser.Phone));
                    doc1.Add(new Paragraph(" Email: " + summary.WaterUser.Email));
                }
            }
            else
            {
                doc1.Add(new Paragraph(" Water User information could not be found"));
            }

            doc1.Add(new Paragraph(" "));

            // Primary land owner information
            doc1.Add(new Paragraph("Primary Land Owner", boldText));
            if (summary.PrimaryLandOwner != null)
            {
                if (summary.Applicant != null &&
                    (summary.Applicant.ContactId == summary.PrimaryLandOwner.ContactId))
                {
                    doc1.Add(new Paragraph(" Same as Applicant"));
                }
                else
                {
                    if (!String.IsNullOrEmpty(summary.PrimaryLandOwner.BusinessName))
                        doc1.Add(new Paragraph(" Business Name: " + summary.PrimaryLandOwner.BusinessName));
                    doc1.Add(new Paragraph(" Name: " + summary.PrimaryLandOwner.Name));
                    /*doc1.Add(new Paragraph(" Secondary Name: " + summary.PrimaryLandOwner.SecondaryName));*/
                    doc1.Add(new Paragraph(" Address: " + summary.PrimaryLandOwner.Address));
                    doc1.Add(new Paragraph(" Phone: " + summary.PrimaryLandOwner.Phone));
                    doc1.Add(new Paragraph(" Email: " + summary.PrimaryLandOwner.Email));
                }
            }
            else
            {
                doc1.Add(new Paragraph(" Land Owner information could not be found"));
            }

            doc1.Add(new Paragraph(" "));

            // Additional land owner information
            if (summary.AdditionalLandOwners != null && summary.AdditionalLandOwners.Count > 0)
            {
                doc1.Add(new Paragraph("Additional Land Owners", boldText));
                foreach (var item in summary.AdditionalLandOwners)
                {
                    if (!String.IsNullOrEmpty(item.BusinessName))
                        doc1.Add(new Paragraph(" Business Name: " + item.BusinessName));
                    doc1.Add(new Paragraph(" Name: " + item.Name));
                    /*doc1.Add(new Paragraph(" Secondary Name: " + item.SecondaryName));*/
                    doc1.Add(new Paragraph(" Address: " + item.Address));
                    doc1.Add(new Paragraph(" Phone: " + item.Phone));
                    doc1.Add(new Paragraph(" Email: " + item.Email));
                    doc1.Add(new Paragraph(" "));
                }
            }

            // Permitee informaion
            doc1.Add(new Paragraph("Permittee", boldText));
            if (!String.IsNullOrEmpty(summary.Permittee) && !String.IsNullOrEmpty(summary.Permittee.Trim()))
            {
                doc1.Add(new Paragraph(" " + summary.Permittee));
            }
            else
            {
                doc1.Add(new Paragraph(" Permittee information could not be found"));
            }

            #endregion

            doc1.Add(emptyParagraph);
            doc1.Add(new Paragraph("Water-Use Description", boldText));
            doc1.Add(new Paragraph(" " + summary.AppWizard.UseDescription));
            doc1.Add(emptyParagraph);

            if (summary.SelectedUseDetailCategories != null)
            {
                doc1.Add(new Paragraph("Selected Water Use Details", boldText));
                foreach (var item in summary.SelectedUseDetailCategories.Where(cat => !string.IsNullOrEmpty(cat)))
                {
                    doc1.Add(new Paragraph(" " + item));
                }

                doc1.Add(emptyParagraph);
            }

            #region Poultry Evaporative

            if (summary.AppWizard.WaterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms != null &&
                summary.AppWizard.WaterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms.Count > 0)
            {
                doc1.Add(new Paragraph("Poultry Evaporative", boldText));
                foreach (var poultryWateringEvaporativeForm in summary.AppWizard.WaterUseDetailForm.PoultryWateringEvaporativeForm.PoultryWateringForms)
                {
                    if (poultryWateringEvaporativeForm.PoultryId.HasValue)
                    {
                        doc1.Add(new Paragraph(" Poultry Type: " + poultryWateringEvaporativeForm.PoultryTypeList.FirstOrDefault(x => x.Value == poultryWateringEvaporativeForm.PoultryId.Value.ToString()).Text));
                    }

                    if (poultryWateringEvaporativeForm.NumberOfHouses.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Houses: " + poultryWateringEvaporativeForm.NumberOfHouses.Value));
                    }

                    if (poultryWateringEvaporativeForm.NumberOfFlocksPerYear.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Flocks per Year: " + poultryWateringEvaporativeForm.NumberOfFlocksPerYear.Value));
                    }

                    if (poultryWateringEvaporativeForm.BirdsPerFlock.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Birds per Flock: " + poultryWateringEvaporativeForm.BirdsPerFlock.Value));
                    }

                    doc1.Add(new Paragraph(" No. of days per year foggers used: " + poultryWateringEvaporativeForm.NumberOfDaysPerYearFoggersUsed));

                    doc1.Add(new Paragraph(" "));
                }
            }

            #endregion

            #region Poultry Fogger

            if (summary.AppWizard.WaterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms != null &&
                summary.AppWizard.WaterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms.Count > 0)
            {
                doc1.Add(new Paragraph("Poultry Fogger", boldText));
                foreach (var poultryWateringFoggerForm in summary.AppWizard.WaterUseDetailForm.PoultryWateringFoggerForm.PoultryWateringForms)
                {
                    if (poultryWateringFoggerForm.PoultryId.HasValue)
                    {
                        doc1.Add(new Paragraph(" Poultry Type: " + poultryWateringFoggerForm.PoultryTypeList.Single(x => x.Value == poultryWateringFoggerForm.PoultryId.Value.ToString()).Text));
                    }

                    if (poultryWateringFoggerForm.NumberOfHouses.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Houses: " + poultryWateringFoggerForm.NumberOfHouses.Value));
                    }

                    if (poultryWateringFoggerForm.NumberOfFlocksPerYear.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Flocks per Year: " + poultryWateringFoggerForm.NumberOfFlocksPerYear.Value));
                    }

                    if (poultryWateringFoggerForm.BirdsPerFlock.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Birds per Flock: " + poultryWateringFoggerForm.BirdsPerFlock.Value));
                    }

                    doc1.Add(new Paragraph(" "));
                }
            }

            #endregion

            #region Dairy Animal Watering

            if (summary.AppWizard.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms != null &&
                summary.AppWizard.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms.Count > 0)
            {
                doc1.Add(new Paragraph("Dairy Animal Watering", boldText));
                foreach (var dairyAnimalWateringForm in summary.AppWizard.WaterUseDetailForm.DairyAnimalWateringForm.LivestockForms)
                {
                    if (!string.IsNullOrEmpty(dairyAnimalWateringForm.Livestock))
                    {
                        doc1.Add(new Paragraph(" Type of Livestock: " + dairyAnimalWateringForm.Livestock));
                    }

                    if (dairyAnimalWateringForm.NumberOfLivestock.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Animals: " + dairyAnimalWateringForm.NumberOfLivestock.Value));
                    }

                    doc1.Add(new Paragraph(" "));
                }
            }

            #endregion

            #region Other Livestock Watering

            if (summary.AppWizard.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms != null &&
                summary.AppWizard.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms.Count > 0)
            {
                doc1.Add(new Paragraph("Other Livestock Watering", boldText));
                foreach (var otherLivestockWateringForm in summary.AppWizard.WaterUseDetailForm.OtherLivestockWateringForm.LivestockForms)
                {
                    if (!string.IsNullOrEmpty(otherLivestockWateringForm.Livestock))
                    {
                        doc1.Add(new Paragraph(" Type of Livestock: " + otherLivestockWateringForm.Livestock));
                    }

                    if (otherLivestockWateringForm.NumberOfLivestock.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Animals: " + otherLivestockWateringForm.NumberOfLivestock.Value));
                    }

                    doc1.Add(new Paragraph(" "));
                }
            }

            #endregion

            #region Farm Potable Supplies

            if (summary.AppWizard.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm.Id != 0)
            {
                doc1.Add(new Paragraph("Farm Potable Supplies", boldText));
                doc1.Add(new Paragraph(" No. of employees: " + summary.AppWizard.WaterUseDetailForm.PermitWwFarmPotableSuppliesForm.NoOfEmployees.ToString()));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Crop Irrigation

            if (summary.AppWizard.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres.HasValue && summary.AppWizard.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres.Value > 0)
            {
                doc1.Add(new Paragraph("Crop Irrigation", boldText));
                doc1.Add(new Paragraph(" Total Irrigated Acres: " + summary.AppWizard.WaterUseDetailForm.CropUseForm.TotalNumberOfAcres.Value.ToString()));
                doc1.Add(new Paragraph(" "));

                if (summary.AppWizard.WaterUseDetailForm.CropUseForm.CropForms != null &&
                    summary.AppWizard.WaterUseDetailForm.CropUseForm.CropForms.Count > 0)
                {
                    foreach (var cropForm in summary.AppWizard.WaterUseDetailForm.CropUseForm.CropForms)
                    {
                        if (cropForm.CropId.HasValue && cropForm.CropTypeList != null)
                        {
                            doc1.Add(new Paragraph(" Type of Crop: " + cropForm.CropTypeList.FirstOrDefault(x => x.Value == cropForm.CropId.Value.ToString()).Text));
                        }

                        if (cropForm.IrrigatedAcres.HasValue)
                        {
                            doc1.Add(new Paragraph(" No. of Irrigated Acres: " + cropForm.IrrigatedAcres.Value));
                        }

                        if (cropForm.IrrigationSystemTypeId.HasValue && cropForm.IrrigationSystemTypeList != null)
                        {
                            var other = cropForm.IrrigationSystemTypeList.FirstOrDefault(x => x.Text == "Other");
                            int otherId = -1;
                            if (other != null)
                            {
                                otherId = Convert.ToInt32(other.Value);
                            }

                            if (cropForm.IrrigationSystemTypeId.Value == otherId)
                            {
                                doc1.Add(new Paragraph(" Irrigation System Used: " + cropForm.IrrigationSystemDescription));
                            }
                            else
                            {
                                doc1.Add(new Paragraph(" Irrigation System Used: " + cropForm.IrrigationSystemTypeList.FirstOrDefault(x => x.Value == cropForm.IrrigationSystemTypeId.Value.ToString()).Text));
                            }
                        }

                        if (cropForm.CropYieldUnitId.HasValue != null && cropForm.CropYieldUnitList != null)
                        {
                            doc1.Add(new Paragraph(" Crop Yield Goal: " + cropForm.CropYieldGoal + " " + cropForm.CropYieldUnitList.FirstOrDefault(x => x.Value == cropForm.CropYieldUnitId.Value.ToString()).Text));
                        }

                        doc1.Add(new Paragraph(" "));
                    }
                }
            }

            #endregion

            #region Nursery Irrigation

            if (summary.AppWizard.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms != null &&
                summary.AppWizard.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms.Count > 0)
            {
                doc1.Add(new Paragraph("Nursery Irrigation", boldText));
                foreach (var nurseryStockItem in summary.AppWizard.WaterUseDetailForm.NurseryStockIrrigationForm.IrrigationForms)
                {
                    if (!string.IsNullOrEmpty(nurseryStockItem.PurposeDescription))
                    {
                        doc1.Add(new Paragraph(" Describe What is Being Irrigated: " + nurseryStockItem.PurposeDescription));
                    }

                    if (nurseryStockItem.IrrigatedAcres.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Irrigated Acres: " + nurseryStockItem.IrrigatedAcres.Value));
                    }

                    if (nurseryStockItem.IrrigationSystemTypeId.HasValue && nurseryStockItem.IrrigationSystemTypeList != null)
                    {
                        var other = nurseryStockItem.IrrigationSystemTypeList.FirstOrDefault(x => x.Text == "Other");
                        int otherId = -1;
                        if (other != null)
                        {
                            otherId = Convert.ToInt32(other.Value);
                        }

                        if (nurseryStockItem.IrrigationSystemTypeId.Value == otherId)
                        {
                            doc1.Add(new Paragraph(" Irrigation System Used: " + nurseryStockItem.IrrigationSystemDescription));
                        }
                        else
                        {
                            doc1.Add(new Paragraph(" Irrigation System Used: " + nurseryStockItem.IrrigationSystemTypeList.FirstOrDefault(x => x.Value == nurseryStockItem.IrrigationSystemTypeId.Value.ToString()).Text));
                        }
                    }

                    doc1.Add(new Paragraph(" "));
                }
            }

            #endregion

            #region Sod Farm Irrigation

            if (summary.AppWizard.WaterUseDetailForm.SodIrrigationForm.IrrigationForms != null &&
                summary.AppWizard.WaterUseDetailForm.SodIrrigationForm.IrrigationForms.Count > 0)
            {
                doc1.Add(new Paragraph("Sod Farm Irrigation", boldText));

                foreach (var sodItem in summary.AppWizard.WaterUseDetailForm.SodIrrigationForm.IrrigationForms)
                {
                    if (!string.IsNullOrEmpty(sodItem.PurposeDescription))
                    {
                        doc1.Add(new Paragraph(" Describe What is Being Irrigated: " + sodItem.PurposeDescription));
                    }

                    if (sodItem.IrrigatedAcres.HasValue)
                    {
                        doc1.Add(new Paragraph(" No. of Irrigated Acres: " + sodItem.IrrigatedAcres.Value));
                    }

                    if (sodItem.IrrigationSystemTypeId.HasValue && sodItem.IrrigationSystemTypeList != null)
                    {
                        var other = sodItem.IrrigationSystemTypeList.FirstOrDefault(x => x.Text == "Other");
                        int otherId = -1;
                        if (other != null)
                        {
                            otherId = Convert.ToInt32(other.Value);
                        }

                        if (sodItem.IrrigationSystemTypeId.Value == otherId)
                        {
                            doc1.Add(new Paragraph(" Irrigation System Used: " + sodItem.IrrigationSystemDescription));
                        }
                        else
                        {
                            doc1.Add(new Paragraph(" Irrigation System Used: " + sodItem.IrrigationSystemTypeList.FirstOrDefault(x => x.Value == sodItem.IrrigationSystemTypeId.Value.ToString()).Text));
                        }
                    }

                    doc1.Add(new Paragraph(" "));
                }
            }

            #endregion

            #region Food Processing (Onsite)

            if (summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.Id != 0)
            {
                doc1.Add(new Paragraph("Food Processing (Onsite)", boldText));
                doc1.Add(new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.AverageGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.MaximumGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.MaximumGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwFoodProcessingForm.EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Aquaculture/Aquariums

            if (summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.Id != 0)
            {
                doc1.Add(new Paragraph("Aquaculture/Aquariums", boldText));
                doc1.Add(new Paragraph(" Is the water recycled?: " + (summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.IsRecycledWater ? "Yes" : "No")));
                doc1.Add(new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.AverageGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.MaximumGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.MaximumGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.PermitWwAquacultureAquariumForm.EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Irrigation Undefined

            if (summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm != null &&
                summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.Id != 0)
            {
                doc1.Add(new Paragraph("Irrigation (Undefined)", boldText));
                doc1.Add(new Paragraph(" Describe What is Being Irrigated: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.PurposeDescription));
                doc1.Add(new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.AverageGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.MaximumGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.IrrigationUndefinedForm.EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Golf Course Irrigation

            if (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm != null &&
                summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.Id != 0)
            {
                doc1.Add(new Paragraph("Golf Course Irrigation", boldText));

                if (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.GolfIrrigationSystemTypeId.HasValue)
                {
                    doc1.Add(new Paragraph(" Irrigation system: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.GolfIrrigationSystemTypeList.FirstOrDefault(x => x.Value == summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.GolfIrrigationSystemTypeId.Value.ToString()).Text));
                }

                if (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IrrigationLayoutTypeId.HasValue)
                {
                    doc1.Add(new Paragraph(" Irrigation layout: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IrrigationLayoutTypeList.FirstOrDefault(x => x.Value == summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IrrigationLayoutTypeId.Value.ToString()).Text));
                }

                doc1.Add(new Paragraph(" Is well water pumped into pond: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.IsWellwaterPumpedIntoPond ? "Yes" : "No")));
                doc1.Add(new Paragraph(" No. of irrigated acres – tees/greens: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_teesOrGreens.HasValue ? summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_teesOrGreens.Value.ToString() : "")));
                doc1.Add(new Paragraph(" No. of irrigated acres – fairways: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_fairways.HasValue ? summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.NoOfIrrigatedAcres_fairways.Value.ToString() : "")));
                doc1.Add(new Paragraph(" Type of grass – tees/greens: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_tees_greens == null ? summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_tees_greens : "")));
                doc1.Add(new Paragraph(" Type of grass – fairways: " + (summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_fairways == null ? summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.TypeOfGrass_fairways : "")));

                doc1.Add(new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.AverageGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.MaximumGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.GolfCourseIrrigationForm.EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Lawn and Park Irrigation

            if (summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm != null &&
                summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.Id != 0)
            {
                doc1.Add(new Paragraph("Lawn and Park Irrigation", boldText));

                if (summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfIrrigatedAcres.HasValue)
                {
                    doc1.Add(new Paragraph(" No of irrigated acres: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfIrrigatedAcres.Value));
                }

                if (summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfDaysPerWeekIrrigated.HasValue)
                {
                    doc1.Add(new Paragraph(" No of days per week irrigated : " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.NoOfDaysPerWeekIrrigated.Value));
                }

                doc1.Add(new Paragraph(" Describe What is Being Irrigated: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.PurposeDescription));
                doc1.Add(new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.AverageGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.MaximumGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.LawnAndParkIrrigationForm.EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Small Intermitent Irrigation

            if (summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm != null &&
                summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.Id != 0)
            {
                doc1.Add(new Paragraph("Small Intermitent Irrigation", boldText));

                if (summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfIrrigatedAcres.HasValue)
                {
                    doc1.Add(new Paragraph(" No of irrigated acres: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfIrrigatedAcres.Value));
                }

                if (summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfDaysPerWeekIrrigated.HasValue)
                {
                    doc1.Add(new Paragraph(" No of days per week irrigated : " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.NoOfDaysPerWeekIrrigated.Value));
                }

                doc1.Add(new Paragraph(" Describe What is Being Irrigated: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.PurposeDescription));
                doc1.Add(new Paragraph(" For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.AverageGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.MaximumGallonPerDayFromGroundWater));
                doc1.Add(new Paragraph(" For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.AverageGallonPerDayFromSurfaceWater));
                doc1.Add(new Paragraph(" Describe how estimate was derived: " + summary.AppWizard.WaterUseDetailForm.SmallIntermitentIrrigationForm.EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Commercial drinking/sanitary

            if (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.Id != 0)
            {
                doc1.Add(new Paragraph("Commercial drinking/sanitary", boldText));
                var optionId =
                    summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.TypeOfFacilityId;

                doc1.Add(
                    new Paragraph(" Type of facility: " +
                                  summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                      .FacilityList.FirstOrDefault(x => x.Value == optionId.ToString()).Text));

                doc1.Add(
                    new Paragraph(" Area (Sq footage) : " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                      .AreaOrSquareFootage.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                          .AreaOrSquareFootage.ToString()
                                      : "")));

                doc1.Add(
                    new Paragraph(" No of employees : " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                      .NoOfEmployees.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                          .NoOfEmployees.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Avg number of customers per day : " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                      .AvgNumberOfCustomersPerDay.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                          .AvgNumberOfCustomersPerDay.ToString()
                                      : "")));
                if (optionId == 1)
                {
                    var optionId2 =
                        summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm.RestaurantTypeId;
                    doc1.Add(
                        new Paragraph(" Restaurant Type: " +
                                      summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                          .RestaurantTypeList.FirstOrDefault(x => x.Value == optionId.ToString()).Text));
                    doc1.Add(
                        new Paragraph(" No. of seats: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                          .NoOfSeats.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwCommercialDrinkingOrSanitaryForm.NoOfSeats.ToString()
                                          : "")));
                }
                else if (optionId == 5)
                {
                    doc1.Add(
                        new Paragraph(" No. of slips: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                          .NoOfSlips.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwCommercialDrinkingOrSanitaryForm.NoOfSlips.ToString()
                                          : "")));
                }

                if (
                    summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                        .AverageGallonPerDayFromGroundWater.HasValue)
                    doc1.Add(
                        new Paragraph(
                            " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                            (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                .AverageGallonPerDayFromGroundWater.HasValue
                                ? summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                    .AverageGallonPerDayFromGroundWater.ToString()
                                : "")));
                if (
                    summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                        .MaximumGallonPerDayFromGroundWater.HasValue)
                    doc1.Add(
                        new Paragraph(
                            " For Groundwater, Gallons per day to be withdrawn during month of maximum use : " +
                            (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                .MaximumGallonPerDayFromGroundWater.HasValue
                                ? summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                    .MaximumGallonPerDayFromGroundWater.ToString()
                                : "")));
                if (
                    summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                        .AverageGallonPerDayFromSurfaceWater.HasValue)
                    doc1.Add(
                        new Paragraph(
                            " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                            (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                .AverageGallonPerDayFromSurfaceWater.HasValue
                                ? summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                    .AverageGallonPerDayFromSurfaceWater.ToString()
                                : "")));
                if (
                    summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                        .MaximumGallonPerDayFromSurfaceWater.HasValue)
                    doc1.Add(
                        new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn : " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                          .MaximumGallonPerDayFromSurfaceWater.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwCommercialDrinkingOrSanitaryForm
                                              .MaximumGallonPerDayFromSurfaceWater.ToString()
                                          : "")));
                doc1.Add(
                    new Paragraph(" Describe how estimate was derived: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwCommercialDrinkingOrSanitaryForm
                                      .EstimateDescription)));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Institutional Educational (drinking/sanitary)

            if (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm.Id != 0)
            {
                doc1.Add(new Paragraph("Institutional Educational (drinking/sanitary)", boldText));
                var facilityoptionId =
                    summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                        .EducationInstituteTypeId;
                doc1.Add(
                    new Paragraph(" Type of facility: " +
                                  summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                      .EducationInstituteTypeList.FirstOrDefault(
                                          x => x.Value == facilityoptionId.ToString()).Text));

                if (facilityoptionId == 1) //School
                {
                    doc1.Add(
                        new Paragraph(" No. of staff: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .NoOfStaff.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwEducationalDrinkingOrsanitaryForm.NoOfStaff.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" No. of students: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .NoOfStudents.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwEducationalDrinkingOrsanitaryForm.NoOfStudents.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" Athletic field irrigation: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .IsAthleticFieldIrrigation
                                          ? "Yes"
                                          : "No")));
                    if (
                        summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                            .IsAthleticFieldIrrigation)
                    {
                        doc1.Add(
                            new Paragraph(" No. of irrigated acres: " +
                                          (summary.AppWizard.WaterUseDetailForm
                                              .PermitWwEducationalDrinkingOrsanitaryForm.NoOfIrrigatedAcres.HasValue
                                              ? summary.AppWizard.WaterUseDetailForm
                                                  .PermitWwEducationalDrinkingOrsanitaryForm.NoOfIrrigatedAcres.ToString
                                                  ()
                                              : "")));
                    }
                    doc1.Add(
                        new Paragraph(" Chillers: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .Chillers
                                          ? "Yes"
                                          : "No")));
                }
                else if (facilityoptionId == 2) //Daycare
                {
                    doc1.Add(
                        new Paragraph(" No. staff working more than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .NoOfStaffWorkingMorethan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwEducationalDrinkingOrsanitaryForm
                                              .NoOfStaffWorkingMorethan20hrsPerWeek.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" No. staff working less than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .NoOfStaffWorkingLessthan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwEducationalDrinkingOrsanitaryForm
                                              .NoOfStaffWorkingLessthan20hrsPerWeek.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" No. students attending more than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .NoOfStudentsAttendingMorethan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwEducationalDrinkingOrsanitaryForm
                                              .NoOfStudentsAttendingMorethan20hrsPerWeek.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" No. students attending less than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .NoOfStudentsAttendingLessthan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwEducationalDrinkingOrsanitaryForm
                                              .NoOfStudentsAttendingLessthan20hrsPerWeek.ToString()
                                          : "")));
                }

                doc1.Add(
                    new Paragraph(
                        " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                            .AverageGallonPerDayFromGroundWater != null
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                .AverageGallonPerDayFromGroundWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                      .MaximumGallonPerDayFromGroundWater != null
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .MaximumGallonPerDayFromGroundWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(
                        " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                            .AverageGallonPerDayFromSurfaceWater != null
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                .AverageGallonPerDayFromSurfaceWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                      .MaximumGallonPerDayFromSurfaceWater != null
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                          .MaximumGallonPerDayFromSurfaceWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Describe how estimate was derived: " +
                                  summary.AppWizard.WaterUseDetailForm.PermitWwEducationalDrinkingOrsanitaryForm
                                      .EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Institutional religious (drinking/sanitary)

            if (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.Id != 0)
            {
                doc1.Add(new Paragraph("Institutional religious (drinking/sanitary)", boldText));

                doc1.Add(
                    new Paragraph(" No. of parishioners: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                      .NoOfParishioners.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .NoOfParishioners.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" No. of services per week: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                      .NoOfServicesPerWeek.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .NoOfServicesPerWeek.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Athletic field irrigation: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.Daycare
                                      ? "Yes"
                                      : "No")));
                if (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm.Daycare)
                {
                    doc1.Add(
                        new Paragraph(" No. staff working more than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .NoOfStaffWorkingMorethan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwReligiousDringkingOrSanitaryForm
                                              .NoOfStaffWorkingMorethan20hrsPerWeek.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" No. staff working less than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .NoOfStaffWorkingLessthan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwReligiousDringkingOrSanitaryForm
                                              .NoOfStaffWorkingLessthan20hrsPerWeek.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" No. students attending more than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .NoOfStudentsAttendingMorethan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwReligiousDringkingOrSanitaryForm
                                              .NoOfStudentsAttendingMorethan20hrsPerWeek.ToString()
                                          : "")));
                    doc1.Add(
                        new Paragraph(" No. students attending less than 20 hrs per week: " +
                                      (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .NoOfStudentsAttendingLessthan20hrsPerWeek.HasValue
                                          ? summary.AppWizard.WaterUseDetailForm
                                              .PermitWwReligiousDringkingOrSanitaryForm
                                              .NoOfStudentsAttendingLessthan20hrsPerWeek.ToString()
                                          : "")));
                }

                doc1.Add(
                    new Paragraph(
                        " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                            .AverageGallonPerDayFromGroundWater.HasValue
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                .AverageGallonPerDayFromGroundWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                      .MaximumGallonPerDayFromGroundWater.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .MaximumGallonPerDayFromGroundWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(
                        " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                            .AverageGallonPerDayFromSurfaceWater.HasValue
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                .AverageGallonPerDayFromSurfaceWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                      .MaximumGallonPerDayFromSurfaceWater.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                          .MaximumGallonPerDayFromSurfaceWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Describe how estimate was derived: " +
                                  summary.AppWizard.WaterUseDetailForm.PermitWwReligiousDringkingOrSanitaryForm
                                      .EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Wildlife Ponds

            if (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.Id != 0)
            {
                doc1.Add(new Paragraph("Wildlife Ponds", boldText));

                doc1.Add(
                    new Paragraph(" No. of ponds: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.NoOfPonds.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.NoOfPonds.ToString
                                          ()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Area of pond(s) (in acres): " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.AreaOfPonds_inAcres
                                      .HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                          .AreaOfPonds_inAcres.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Depth of pond(s) (in feet): " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.DepthOfPonds_inFeet
                                      .HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                          .DepthOfPonds_inFeet.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" No. of months per year that pond(s) contain water: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                      .NoOfMonthsPerYearPondContainWater.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                          .NoOfMonthsPerYearPondContainWater.ToString()
                                      : "")));

                doc1.Add(
                    new Paragraph(
                        " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                            .AverageGallonPerDayFromGroundWater.HasValue
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                .AverageGallonPerDayFromGroundWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                      .MaximumGallonPerDayFromGroundWater.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                          .MaximumGallonPerDayFromGroundWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(
                        " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                            .AverageGallonPerDayFromSurfaceWater.HasValue
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                .AverageGallonPerDayFromSurfaceWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                      .MaximumGallonPerDayFromSurfaceWater.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm
                                          .MaximumGallonPerDayFromSurfaceWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Describe how estimate was derived: " +
                                  summary.AppWizard.WaterUseDetailForm.PermitWwWildlifePondForm.EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Construction Dewatering

            if (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm != null &&
                summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.Id != 0)
            {

                doc1.Add(new Paragraph("Construction Dewatering", boldText));

                doc1.Add(
                    new Paragraph(" Area of excavation (in sq. ft): " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .AreaOfExcavation.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                          .AreaOfExcavation.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Depth of excavation (in feet): " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .DepthOfExcavation.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                          .DepthOfExcavation.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" No. of pumps: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.NoOfPumps
                                      .HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                          .NoOfPumps.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Capacity of pumps: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .CapacityOfPumps.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                          .CapacityOfPumps.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Hours of operation per day: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .HoursOfOperationPerDay.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                          .HoursOfOperationPerDay.ToString()
                                      : "")));

                var removalExcavationTypeId =
                    summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm.WaterRemovalExcavationTypeId;
                doc1.Add(
                    new Paragraph(" How is water removed from excavation? " +
                                  summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .WaterRemovalExcavationTypeList.FirstOrDefault(
                                          x => x.Value == removalExcavationTypeId.ToString()).Text));

                doc1.Add(
                    new Paragraph(
                        " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                            .AverageGallonPerDayFromGroundWater.HasValue
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                .AverageGallonPerDayFromGroundWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Groundwater, Gallons per day to be withdrawn during month of maximum use: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .MaximumGallonPerDayFromGroundWater.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                          .MaximumGallonPerDayFromGroundWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(
                        " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                        (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                            .AverageGallonPerDayFromSurfaceWater.HasValue
                            ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                .AverageGallonPerDayFromSurfaceWater.ToString()
                            : "")));
                doc1.Add(
                    new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " +
                                  (summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .MaximumGallonPerDayFromSurfaceWater.HasValue
                                      ? summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                          .MaximumGallonPerDayFromSurfaceWater.ToString()
                                      : "")));
                doc1.Add(
                    new Paragraph(" Describe how estimate was derived: " +
                                  summary.AppWizard.WaterUseDetailForm.PermitWwConstructionDewateringForm
                                      .EstimateDescription));
                doc1.Add(new Paragraph(" "));
            }

            #endregion

            #region Private Water Supplier

            if (summary.AppWizard.WaterUseDetailForm.PrivateWaterSuppilerForm.PrivateWaterSupplierForms != null)
            {
                foreach (var privateWaterSupplier in summary.AppWizard.WaterUseDetailForm.PrivateWaterSuppilerForm.PrivateWaterSupplierForms)
                {
                    if (privateWaterSupplier.AverageGallonPerDayFromGroundWater.HasValue ||
                        privateWaterSupplier.MaximumGallonPerDayFromGroundWater.HasValue
                        || privateWaterSupplier.AverageGallonPerDayFromSurfaceWater.HasValue ||
                        privateWaterSupplier.MaximumGallonPerDayFromSurfaceWater.HasValue ||
                        !string.IsNullOrEmpty(privateWaterSupplier.EstimateDescription))
                    {
                        doc1.Add(new Paragraph(privateWaterSupplier.Title, boldText));

                        if (privateWaterSupplier.PermitWaterWithdrawalPurposeId == 16 ||
                            privateWaterSupplier.PermitWaterWithdrawalPurposeId == 17 ||
                            privateWaterSupplier.PermitWaterWithdrawalPurposeId == 23 ||
                            privateWaterSupplier.PermitWaterWithdrawalPurposeId == 24 ||
                            privateWaterSupplier.PermitWaterWithdrawalPurposeId == 45 ||
                            privateWaterSupplier.PermitWaterWithdrawalPurposeId == 46)
                        {
                            doc1.Add(
                                new Paragraph(" No. of employees: " +
                                              (privateWaterSupplier.NoOfEmployees.HasValue
                                                  ? privateWaterSupplier.NoOfEmployees.ToString()
                                                  : "")));
                        }

                        if (privateWaterSupplier.PermitWaterWithdrawalPurposeId == 46)
                        {
                            doc1.Add(
                                new Paragraph(" No. of other regular users per day: " +
                                              (privateWaterSupplier.NoOfRegularUsersPerDay.HasValue
                                                  ? privateWaterSupplier.NoOfRegularUsersPerDay.ToString()
                                                  : "")));
                        }

                        if (privateWaterSupplier.PermitWaterWithdrawalPurposeId == 39 ||
                            privateWaterSupplier.PermitWaterWithdrawalPurposeId == 54)
                        {
                            doc1.Add(
                                new Paragraph(" No. of lots/Units: " +
                                              (privateWaterSupplier.NoOfLotsOrUnits.HasValue
                                                  ? privateWaterSupplier.NoOfLotsOrUnits.ToString()
                                                  : "")));
                        }

                        if (privateWaterSupplier.PermitWaterWithdrawalPurposeId == 39)
                        {
                            doc1.Add(new Paragraph(" Swimming pool? " + (privateWaterSupplier.SwimmingPool ? "Yes" : "No")));
                        }

                        doc1.Add(
                            new Paragraph(
                                " For Groundwater, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                                privateWaterSupplier.AverageGallonPerDayFromGroundWater));
                        doc1.Add(
                            new Paragraph(
                                " For Groundwater, Gallons per day to be withdrawn during month of maximum use: " +
                                privateWaterSupplier.MaximumGallonPerDayFromGroundWater));
                        doc1.Add(
                            new Paragraph(
                                " For Surface Water, Average No. of Gallons per day to be withdrawn on an annual basis: " +
                                privateWaterSupplier.AverageGallonPerDayFromSurfaceWater));
                        doc1.Add(
                            new Paragraph(" For Surface Water, Maximum gallons per day to be withdrawn: " +
                                          privateWaterSupplier.AverageGallonPerDayFromSurfaceWater));
                        doc1.Add(
                            new Paragraph(" Describe how estimate was derived: " +
                                          privateWaterSupplier.EstimateDescription));
                        doc1.Add(new Paragraph(" "));
                    }
                }
            }

            #endregion

            if (summary.AppWizard != null && summary.AppWizard.WastewaterTreatmentDisposalForm != null)
            {
                foreach (var wasteWaterTreatmentDisposal in summary.AppWizard.WastewaterTreatmentDisposalForm)
                {
                    if (wasteWaterTreatmentDisposal != null && wasteWaterTreatmentDisposal.Id > 0)
                    {
                        doc1.Add(new Paragraph(wasteWaterTreatmentDisposal.Title + " Water Treatment Disposal", boldText));

                        doc1.Add(
                            new Paragraph(" Will this water withdrawal result in any wastewater discharge: " +
                                          ((wasteWaterTreatmentDisposal.IsWastewaterDischarge) ? "Yes" : "No")));
                        if (wasteWaterTreatmentDisposal.IsWastewaterDischarge)
                        {
                            if (wasteWaterTreatmentDisposal.WaterDispersementTypeId != null)
                            {
                                string tempID = wasteWaterTreatmentDisposal.WaterDispersementTypeId.ToString();
                                doc1.Add(
                                    new Paragraph(
                                        " Select the kind of wastewater treatment and disposal system that will be used: " +
                                        wasteWaterTreatmentDisposal.WaterDispersementTypeList.FirstOrDefault(
                                            x => x.Value == tempID).Text));
                            }

                            if (!String.IsNullOrEmpty(wasteWaterTreatmentDisposal.GroundwaterDischargeDiscription))
                                doc1.Add(
                                    new Paragraph(" Describe the nature of the groundwater discharge: " +
                                                  wasteWaterTreatmentDisposal.GroundwaterDischargeDiscription));
                            if (!String.IsNullOrEmpty(wasteWaterTreatmentDisposal.PermitDischargeNumber))
                                doc1.Add(
                                    new Paragraph(" Enter the permit discharge number: " +
                                                  wasteWaterTreatmentDisposal.PermitDischargeNumber));
                        }

                        doc1.Add(emptyParagraph);
                    }
                }
            }

            if (summary.PermitWithdrawalGroundwater != null)
            {
                doc1.Add(new Paragraph("Groundwater Details", boldText));
                doc1.Add(
                    new Paragraph(" Average Gallon Per Day: " +
                                  (summary.PermitWithdrawalGroundwater.AvgGalPerDay == 0
                                      ? "To Be Determined"
                                      : String.Format("{0:n0}", summary.PermitWithdrawalGroundwater.AvgGalPerDay))));
                doc1.Add(
                    new Paragraph(" Maximum Gallon Per Day: " +
                                  (summary.PermitWithdrawalGroundwater.MaxGalPerDay == 0
                                      ? "To Be Determined"
                                      : String.Format("{0:n0}", summary.PermitWithdrawalGroundwater.MaxGalPerDay))));

                doc1.Add(emptyParagraph);

                foreach (var item in summary.PermitWithdrawalGroundwater.PermitWithdrawalGroundwaterDetails)
                {
                    if (!string.IsNullOrEmpty(item.LU_WaterWithdrawalType.Description))
                        doc1.Add(new Paragraph(" Groundwater Type: " + item.LU_WaterWithdrawalType.Description));
                    if (item.IsExisting.HasValue)
                        doc1.Add(new Paragraph(" Is this an Existing Well? " + (item.IsExisting.HasValue ? item.IsExisting.Value ? "Yes" : "No" : "")));
                    if (item.WellDepthFeet.HasValue)
                        doc1.Add(new Paragraph(" Well Depth (feet): " + item.WellDepthFeet.ToString()));
                    if (item.WellDiameterInches.HasValue)
                        doc1.Add(new Paragraph(" Well Diameter (inches): " + item.WellDiameterInches ?? ""));
                    if (!string.IsNullOrEmpty(item.WellTagNumber))
                        doc1.Add(new Paragraph(" Well Tag No.: " + item.WellTagNumber));
                    if (!string.IsNullOrEmpty(item.TaxMapNumber))
                        doc1.Add(new Paragraph(" Tax Map: " + item.TaxMapNumber));
                    if (!string.IsNullOrEmpty(item.TaxMapBlockNumber))
                        doc1.Add(new Paragraph(" Grid: " + item.TaxMapBlockNumber));
                    if (!string.IsNullOrEmpty(item.TaxMapParcel))
                        doc1.Add(new Paragraph(" Parcel: " + item.TaxMapParcel));
                    if (!string.IsNullOrEmpty(item.TaxMapSubBlockNumber))
                        doc1.Add(new Paragraph(" Block: " + item.TaxMapSubBlockNumber));
                    if (!string.IsNullOrEmpty(item.LotNumber)) doc1.Add(new Paragraph(" Lot: " + item.LotNumber));
                    if (!string.IsNullOrEmpty(item.Street)) doc1.Add(new Paragraph(" Street: " + item.Street));
                    if (!string.IsNullOrEmpty(item.City)) doc1.Add(new Paragraph(" City: " + item.City));
                    if (!string.IsNullOrEmpty(item.State)) doc1.Add(new Paragraph(" State: " + item.State));
                    if (!string.IsNullOrEmpty(item.Zip)) doc1.Add(new Paragraph(" Zip: " + item.Zip));
                    if (item.LU_Watershed != null && !string.IsNullOrEmpty(item.LU_Watershed.Description))
                        doc1.Add(new Paragraph(" Watershed: " + item.LU_Watershed.Description));
                    doc1.Add(new Paragraph(" Comment: " + item.Comment));

                    if (summary.Permit.WaterWithdrawalLocationPurposes.Any(x => x.PermitWithdrawalId == item.Id))
                    {
                        var items = String.Join(", ",
                            summary.Permit.WaterWithdrawalLocationPurposes.Where(x => x.PermitWithdrawalId == item.Id)
                                .Select(x => x.PermitWaterWithdrawalPurpose.LU_WaterWithdrawalPurpose.Description));
                        doc1.Add(new Paragraph(" Associated Withdrawal Purposes: " + items));
                    }

                    doc1.Add(emptyParagraph);
                }
            }

            if (summary.PermitWithdrawalSurfacewater != null)
            {
                doc1.Add(new Paragraph("Surface Water Details", boldText));

                doc1.Add(
                    new Paragraph(" Average Gallon Per Day: " +
                                  (summary.PermitWithdrawalSurfacewater.AvgGalPerDay == 0
                                      ? "To Be Determined"
                                      : String.Format("{0:n0}", summary.PermitWithdrawalSurfacewater.AvgGalPerDay))));
                doc1.Add(
                    new Paragraph(" Maximum Gallon Per Day: " +
                                  (summary.PermitWithdrawalSurfacewater.MaxGalPerDay == 0
                                      ? "To Be Determined"
                                      : String.Format("{0:n0}", summary.PermitWithdrawalSurfacewater.MaxGalPerDay))));

                doc1.Add(emptyParagraph);

                foreach (var item in summary.PermitWithdrawalSurfacewater.PermitWithdrawalSurfacewaterDetails)
                {
                    if (!string.IsNullOrEmpty(item.LU_WaterWithdrawalType.Description))
                        doc1.Add(new Paragraph(" Surface Water Type:" + item.LU_WaterWithdrawalType.Description));
                    if (!string.IsNullOrEmpty(item.ExactLocationOfIntake))
                        doc1.Add(new Paragraph(" Exact Location Of Intake:" + item.ExactLocationOfIntake));
                    if (!string.IsNullOrEmpty(item.LU_StateStream.Description))
                        doc1.Add(new Paragraph(" Water Source Name:" + (item.LU_StateStream != null ? item.LU_StateStream.Description + " | " + item.LU_StateStream.Key : "")));
                    if (!string.IsNullOrEmpty(item.TaxMapNumber))
                        doc1.Add(new Paragraph(" Tax Map: " + item.TaxMapNumber));
                    if (!string.IsNullOrEmpty(item.TaxMapBlockNumber))
                        doc1.Add(new Paragraph(" Grid: " + item.TaxMapBlockNumber));
                    if (!string.IsNullOrEmpty(item.TaxMapParcel))
                        doc1.Add(new Paragraph(" Parcel: " + item.TaxMapParcel));
                    if (!string.IsNullOrEmpty(item.TaxMapSubBlockNumber))
                        doc1.Add(new Paragraph(" Block: " + item.TaxMapSubBlockNumber));
                    if (!string.IsNullOrEmpty(item.LotNumber)) doc1.Add(new Paragraph(" Lot: " + item.LotNumber));
                    if (!string.IsNullOrEmpty(item.Street)) doc1.Add(new Paragraph(" Street: " + item.Street));
                    if (!string.IsNullOrEmpty(item.City)) doc1.Add(new Paragraph(" City: " + item.City));
                    if (!string.IsNullOrEmpty(item.State)) doc1.Add(new Paragraph(" State: " + item.State));
                    if (!string.IsNullOrEmpty(item.Zip)) doc1.Add(new Paragraph(" Zip: " + item.Zip));
                    if (item.LU_Watershed != null && !string.IsNullOrEmpty(item.LU_Watershed.Description))
                        doc1.Add(new Paragraph(" Watershed: " + item.LU_Watershed.Description));
                    doc1.Add(new Paragraph(" Comment: " + item.Comment));

                    if (summary.Permit.WaterWithdrawalLocationPurposes.Any(x => x.PermitWithdrawalId == item.Id))
                    {
                        var items = String.Join(", ",
                            summary.Permit.WaterWithdrawalLocationPurposes.Where(x => x.PermitWithdrawalId == item.Id)
                                .Select(x => x.PermitWaterWithdrawalPurpose.LU_WaterWithdrawalPurpose.Description));
                        doc1.Add(new Paragraph(" Associated Withdrawal Purposes: " + items));
                    }

                    doc1.Add(emptyParagraph);
                }
            }

            if (summary.Permit.IsInWMStrategyArea.HasValue || summary.Permit.IsInTier2Watershed.HasValue ||
                summary.Permit.AquiferId.HasValue
                || summary.Permit.AquiferTest.HasValue || !string.IsNullOrEmpty(summary.Permit.SourceDescription) ||
                !string.IsNullOrEmpty(summary.Permit.Location))
            {
                doc1.Add(new Paragraph("Location Information", boldText));
                if (summary.Permit.IsInWMStrategyArea != null)
                    doc1.Add(
                        new Paragraph(" Water Management Strategy Area: " +
                                      (summary.Permit.IsInWMStrategyArea.Value ? "Yes" : "No")));
                if (summary.Permit.IsInTier2Watershed != null)
                    doc1.Add(
                        new Paragraph(" Tier II Watershed: " +
                                      (summary.Permit.IsInTier2Watershed.Value ? "Yes" : "No")));
                if (summary.Permit.AquiferId != null && summary.Permit.LU_Aquifer != null)
                    doc1.Add(
                        new Paragraph(" Aquifer: " + summary.Permit.LU_Aquifer.Description + " | " +
                                      summary.Permit.LU_Aquifer.Key));
                if (summary.Permit.AquiferTest != null)
                    doc1.Add(
                        new Paragraph(" Aquifer Test Required: " +
                                      (summary.Permit.AquiferTest.Value ? "Yes" : "No")));
                if (!string.IsNullOrEmpty(summary.Permit.SourceDescription))
                    doc1.Add(new Paragraph(" Source Description: " + summary.Permit.SourceDescription));
                if (!string.IsNullOrEmpty(summary.Permit.Location))
                    doc1.Add(new Paragraph(" Location: " + summary.Permit.Location));
                doc1.Add(new Paragraph(" ADC Map Page: " + summary.Permit.ADCPageLetterGrid + " Row: " + summary.Permit.ADCXCoord + " Column: " + summary.Permit.ADCYCoord));
                doc1.Add(new Paragraph(" County hydro map: " + summary.Permit.CountyHydroGeoMap));
                doc1.Add(new Paragraph(" USGS Topo map: " + summary.Permit.USGSTopoMap));
                doc1.Add(new Paragraph(" "));
            }

            doc1.Add(new Paragraph("Easement Information", boldText));
            doc1.Add(new Paragraph(" Conservation Easement Value: " + (summary.Permit.IsSubjectToEasement.HasValue ? summary.Permit.IsSubjectToEasement.Value ? "Yes" : "No" : "")));
            doc1.Add(new Paragraph(" Notified Holder Value: " + (summary.Permit.HaveNotifiedEasementHolder.HasValue ? summary.Permit.HaveNotifiedEasementHolder.Value ? "Yes" : "No" : "")));
            doc1.Add(emptyParagraph);

            doc1.Close();

            // Seek the stream back to the beginning
            memStream.Seek(0, SeekOrigin.Begin);

            return File(memStream, "application/pdf");
        }

        public class PdfHeader : PdfPageEventHelper
        {
            public override void OnStartPage(PdfWriter writer, iTextSharp.text.Document doc)
            {
                var bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                var boldText = new Font(bfTimes, 16, Font.BOLD, BaseColor.BLACK);
                var smalText = new Font(bfTimes, 10);

                var footer = new Paragraph("THANK YOU", FontFactory.GetFont(FontFactory.TIMES, 10, iTextSharp.text.Font.NORMAL));

                var line1 = new Paragraph("MARYLAND DEPARTMENT OF THE ENVIRONMENT", boldText);
                var line2 = new Paragraph("WATER MANAGEMENT ADMINISTRATION - WATER SUPPLY PROGRAM");
                var line3 = new Paragraph("1800 Washington Boulevard, Baltimore, Maryland 21230", smalText);
                var line4 = new Paragraph("(410) 537-3714   1-800-633-6101   fax (410)537-3157   http://www.mde.state.md.us");
                var line5 = new Paragraph("APPLICATION FOR A PERMIT TO APPROPRIATE AND USE WATERS OF THE STATE");

                line1.Alignment = 1;
                line2.Alignment = 1;
                line3.Alignment = 1;
                line4.Alignment = 1;
                line5.Alignment = 1;

                footer.Alignment = Element.ALIGN_RIGHT;

                var footerTbl = new PdfPTable(1);
                footerTbl.WidthPercentage = 100;
                footerTbl.TotalWidth = 550;
                footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

                var cell = new PdfPCell();
                cell.AddElement(line1);
                cell.AddElement(line2);
                cell.AddElement(line3);
                cell.AddElement(line4);
                cell.AddElement(line5);

                cell.Border = 0;

                cell.PaddingLeft = 10;

                footerTbl.AddCell(cell);

                int rowStart = 0;
                int rowEnd = -1;
                float x = 10;
                float y = 800;

                footerTbl.WriteSelectedRows(rowStart, rowEnd, x, y, writer.DirectContent);
            }
        }

        #endregion

        #region Map
        [HttpGet]
        public virtual PartialViewResult AddWaterWithdrawalLocation(int permitId, double x, double y, int srid)
        {
            try
            {
                var model = new PermitWaterWithdrawalLocation
                {
                    PermitId = permitId,
                    WithdrawalSourceId = 1, //Groundwater
                    IsExistingWell = true,
                    WithdrawalLocationX = x,
                    WithdrawalLocationY = y,
                    SpatialReferenceId = srid
                };

                var form = Mapper.Map<PermitWaterWithdrawalLocation, WithdrawalLocationForm>(model);
                form.WithdrawalSourceList = _waterWithdrawalSourceService.GetAll();
                form.WithdrawalTypeList = Mapper.Map<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>(_waterWithdrawalTypeService.GetAll());

                // Populate State Stream List with only those which are within
                // watersheds that contain the x/y of the withdrawal location
                var streamsByWatershed = Mapper.Map<IEnumerable<LU_StateStream>, SelectList>(_stateStreamService.GetByWatershed(x, y, srid)).ToList();
                streamsByWatershed.Add(new SelectListItem { Text = "Other", Value = "-1" });
                form.StateStreamList = new SelectList(streamsByWatershed, "Value", "Text");

                return PartialView(form);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return PartialView(MVC.Shared.Views.Error);
            }
        }

        [HttpPost]
        public virtual JsonResult AddWaterWithdrawalLocation(WithdrawalLocationForm form)
        {
            try
            {
                var model = Mapper.Map<WithdrawalLocationForm, PermitWaterWithdrawalLocation>(form);
                _permitWaterWithdrawalLocationService.Save(model);

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { Success = false });
            }
        }

        [HttpGet]
        public virtual JsonResult GetConservationEasement(int permitId)
        {
            var permit = _permittingService.GetById(permitId);

            return Json(new
            {
                IsSubjectToEasement = permit.IsSubjectToEasement,
                HaveNotifiedEasementHolder = permit.HaveNotifiedEasementHolder
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult SetConservationEasement(int permitId, bool? isSubjectToEasement, bool? haveNotifiedEasementHolder)
        {
            try
            {
                var permit = _permittingService.GetById(permitId);
                permit.IsSubjectToEasement = isSubjectToEasement;
                permit.HaveNotifiedEasementHolder = haveNotifiedEasementHolder;
                _permittingService.Save(permit);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public virtual PartialViewResult EditWaterWithdrawalLocation(int id)
        {
            try
            {
                if (id > 0)
                {
                    var model = _permitWaterWithdrawalLocationService.GetById(id);

                    if (model != null)
                    {
                        var form = Mapper.Map<PermitWaterWithdrawalLocation, WithdrawalLocationForm>(model);
                        form.WithdrawalSourceList = _waterWithdrawalSourceService.GetAll();
                        form.WithdrawalTypeList = Mapper.Map<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>(_waterWithdrawalTypeService.GetAll());

                        // Populate State Stream List with only those which are within
                        // watersheds that contain the x/y of the withdrawal location
                        var streamsByWatershed = Mapper.Map<IEnumerable<LU_StateStream>, SelectList>(_stateStreamService.GetByWatershed(form.WithdrawalLocationX, form.WithdrawalLocationY, form.SpatialReferenceId)).ToList();
                        streamsByWatershed.Add(new SelectListItem { Text = "Other", Value = "-1" });
                        form.StateStreamList = new SelectList(streamsByWatershed, "Value", "Text");

                        return PartialView(form);
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return PartialView(MVC.Shared.Views.Error);
            }
        }

        [HttpPost]
        public virtual JsonResult EditWaterWithdrawalLocation(WithdrawalLocationForm form)
        {
            try
            {
                var model = Mapper.Map<WithdrawalLocationForm, PermitWaterWithdrawalLocation>(form);
                _permitWaterWithdrawalLocationService.Save(model);

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { Success = false });
            }
        }

        [HttpPost]
        public virtual JsonResult RemoveWaterWithdrawalLocation(int id)
        {
            try
            {
                _permitWaterWithdrawalLocationService.Delete(id);
            }
            catch (Exception)
            {
                return Json(new { Success = false });
            }

            return Json(new { Success = true });
        }

        [HttpPost]
        public virtual JsonResult RemovePermitLocation(int id)
        {
            try
            {
                _permitLocationService.Delete(id);
            }
            catch (Exception)
            {
                return Json(new { Success = false });
            }

            return Json(new { Success = true });
        }

        [HttpPost]
        public virtual JsonResult AddPermitLocation(int permitId, double x, double y, int srid)
        {
            try
            {
                var model = new PermitLocation
                {
                    PermitId = permitId,
                    WithdrawalLocationX = x,
                    WithdrawalLocationY = y,
                    SpatialReferenceId = srid
                };

                int permitLocationId = _permitLocationService.Save(model);

                return Json(new { Success = true });
            }
            catch (Exception)
            {
                return Json(new { Success = false });
            }
        }

        #endregion

        #region Affirmation
        [HttpPost]
        public virtual JsonResult Affirmation(int id, string submittedBy, string submittedDate)
        {
            var transactionalDeleteActions = new List<Action>();
            string returnMessage;

            try
            {
                // The following is a hack to get the water usage totals to calculate
                _permittingService.Save(_permittingService.GetById(id));

                var application = _permittingService.GetById(id, "PermitContacts.Contact.ContactCommunicationMethods,PermitCounties,PermitWithdrawalGroundwaters,PermitWithdrawalSurfacewaters,PermitWaterWithdrawalPurposes.LU_WaterWithdrawalPurpose");

                if (application == null)
                {
                    throw new Exception("Permit " + id + " does not exist");
                }

                var applicant = application.PermitContacts.FirstOrDefault(pc => pc.IsApplicant && pc.IsPrimary);

                if (applicant == null)
                {
                    throw new Exception("Permit " + id + " does not contain an applicant contact");
                }

                var county = application.PermitCounties.FirstOrDefault();
                if (county == null)
                {
                    throw new Exception("Permit " + id + " does not contain any counties");
                }

                if (IsExemption(application))
                {
                    // If this application is elegible for an exemption, we have a slightly different workflow
                    // Write the first active Administrative Specialist to the PermitContacts table
                    var administrativeSpecialist = _administrativeSpecialistService.GetRange(0, 1, "CreatedDate desc", new[] { new DynamicFilter("Active") }).SingleOrDefault();

                    if (administrativeSpecialist != null)
                    {
                        var administrativeSpecialistContact = new PermitContact
                        {
                            PermitId = application.Id,
                            ContactId = administrativeSpecialist.ContactId,
                            ContactTypeId = 36, // Administrative Specialist
                            IsApplicant = false,
                            IsPrimary = true,
                            Active = true
                        };

                        var asPermitContactId = _permitContactService.Save(administrativeSpecialistContact);
                        if (asPermitContactId > 0)
                        {
                            // Save successful, add to our transaction
                            transactionalDeleteActions.Add(() => _permitContactService.Delete(asPermitContactId));
                        }
                        else
                        {
                            // Save failed
                            throw new Exception("Error saving Administrative Specialist Permit Contact");
                        }
                    }

                    // Set Application Status to "Complete"
                    application.PermitStatusId = 3; // PermitStatusId of 3 == "Complete"
                    application.SubmittedBy = submittedBy;
                    application.SubmittedDate = Convert.ToDateTime(submittedDate);

                    // Remove navigation properties (they will cause an "already exists"
                    // EF exception if they are persisted).
                    application.PermitContacts = null;
                    application.PermitCounties = null;
                    application.PermitWaterWithdrawalPurposes = null;
                    application.PermitWithdrawalGroundwaters = null;
                    application.PermitWithdrawalSurfacewaters = null;

                    returnMessage = "Based on the information provided in this application, the requested water appropriation may be eligible for an exemption. MDE will review the application and make a final determination.";

                    // Save Application and return the newly created Id
                    return Json(new { Success = true, Message = returnMessage, PermitId = _permittingService.Save(application) }, JsonRequestBehavior.AllowGet);
                }

                // Get the Project Manager based on the County
                var pmCounty = _projectManagerCountyService.GetRange(0, 1, null, new[] { new DynamicFilter("CountyId==@0", county.CountyId) }, "ProjectManager.Supervisor.DivisionChief").FirstOrDefault();
                if (pmCounty == null)
                {
                    throw new Exception("No project manager assigned to county " + county.CountyId);
                }

                var pm = pmCounty.ProjectManager;
                if (pm == null)
                {
                    throw new Exception("Project manager " + pmCounty.ProjectManagerId + " does not exist");
                }

                // Create PermitContact record for the Project Manager
                var pmContact = new PermitContact
                {
                    PermitId = application.Id,
                    ContactId = pm.ContactId,
                    ContactTypeId = 20, // Project Manager
                    IsApplicant = false,
                    IsPrimary = true,
                    Active = true
                };

                var pmContactId = _permitContactService.Save(pmContact);
                if (pmContactId > 0)
                {
                    // Save successful, add to our transaction
                    transactionalDeleteActions.Add(delegate { _permitContactService.Delete(pmContactId); });
                }
                else
                {
                    // Save failed
                    throw new Exception("Error saving Project Manager Contact");
                }

                // If this project manager has a supervisor, create a PermitContact record for the Supervisor
                // Get the Supervisor based on the Project Manager
                var sup = pm.Supervisor;
                if (sup != null)
                {
                    var supContact = new PermitContact
                    {
                        PermitId = application.Id,
                        ContactId = sup.ContactId,
                        ContactTypeId = 24, // Supervisor
                        IsApplicant = false,
                        IsPrimary = true,
                        Active = true
                    };

                    var supContactId = _permitContactService.Save(supContact);
                    if (supContactId > 0)
                    {
                        // Save successful, add to our transaction
                        transactionalDeleteActions.Add(delegate { _permitContactService.Delete(supContactId); });
                    }
                    else
                    {
                        // Save failed
                        throw new Exception("Error saving Supervisor Contact");
                    }

                    // Get the Division Chief based on the Supervisor
                    var dc = sup.DivisionChief;
                    if (dc != null)
                    {
                        var dcContact = new PermitContact
                        {
                            PermitId = application.Id,
                            ContactId = dc.ContactId,
                            ContactTypeId = 32, // Division Chief
                            IsApplicant = false,
                            IsPrimary = true,
                            Active = true
                        };

                        var dcContactId = _permitContactService.Save(dcContact);
                        if (dcContactId > 0)
                        {
                            // Save successful, add to our transaction
                            transactionalDeleteActions.Add(delegate { _permitContactService.Delete(dcContactId); });
                        }
                        else
                        {
                            // Save failed
                            throw new Exception("Error saving Division Chief Contact");
                        }
                    }
                }

                // Write the first active secretary to the PermitContacts table
                var secretary = _secretaryService.GetRange(0, 1, "CreatedDate desc", new[] { new DynamicFilter("Active") }).SingleOrDefault();

                if (secretary != null)
                {
                    var secContact = new PermitContact
                    {
                        PermitId = application.Id,
                        ContactId = secretary.ContactId,
                        ContactTypeId = 31, // Secretary
                        IsApplicant = false,
                        IsPrimary = true,
                        Active = true
                    };

                    var secPermitContactId = _permitContactService.Save(secContact);
                    if (secPermitContactId > 0)
                    {
                        // Save successful, add to our transaction
                        transactionalDeleteActions.Add(() => _permitContactService.Delete(secPermitContactId));
                    }
                    else
                    {
                        // Save failed
                        throw new Exception("Error saving Secretary Permit Contact");
                    }
                }


                // Get the DNR contact and create a PermitContact for them
                var dnrContact = _contactService.GetRange(0, 1, null, new[] { new DynamicFilter("CountyId==@0&&ContactTypeId==@1", county.CountyId, 10) }, "ContactCommunicationMethods").FirstOrDefault(); // 10 is the ContactTypeId for Department of Natural Resources
                if (dnrContact != null)
                {
                    var dnrContactContact = new PermitContact
                    {
                        PermitId = application.Id,
                        ContactId = dnrContact.Id,
                        ContactTypeId = 10, // Department of Natural Resources
                        IsApplicant = false,
                        IsPrimary = true,
                        Active = true
                    };

                    var dnrContactContactId = _permitContactService.Save(dnrContactContact);
                    if (dnrContactContactId > 0)
                    {
                        // Save successful, add to our transaction
                        transactionalDeleteActions.Add(delegate { _permitContactService.Delete(dnrContactContactId); });
                    }
                    else
                    {
                        // Save failed
                        throw new Exception("Error saving Department or Natural Resources Contact");
                    }
                }

                // Get the Environmental Health Director and create a PermitContact for them
                var environmentalHealthDirector = _contactService.GetRange(0, 1, null, new[] { new DynamicFilter("CountyId == @0 && ContactTypeId == @1", county.CountyId, 33) }, "ContactCommunicationMethods").FirstOrDefault(); // 33 is the ContactTypeId for Environmental Health Director
                if (environmentalHealthDirector != null)
                {
                    var environmentalHealthDirectorContact = new PermitContact
                    {
                        PermitId = application.Id,
                        ContactId = environmentalHealthDirector.Id,
                        ContactTypeId = 33, // Environmental Health Director
                        IsApplicant = false,
                        IsPrimary = true,
                        Active = true
                    };

                    var environmentalHealthDirectorContactId = _permitContactService.Save(environmentalHealthDirectorContact);
                    if (environmentalHealthDirectorContactId > 0)
                    {
                        // Save successful, add to our transaction
                        transactionalDeleteActions.Add(delegate { _permitContactService.Delete(environmentalHealthDirectorContactId); });
                    }
                    else
                    {
                        // Save failed
                        throw new Exception("Error saving Environmental Health Director Contact");
                    }
                }

                // If the application contains at least one use code that is NOT agricultural,
                // we need to create a county official contact for the affected county
                if (application.PermitWaterWithdrawalPurposes.Any(pwp => pwp.LU_WaterWithdrawalPurpose.WaterWithdrawalPurposeCategoryId > 1))
                {
                    // Get the External Authority and create a PermitContact for them
                    var externalAuthority = _contactService.GetRange(0, 1, null, new[] { new DynamicFilter("CountyId == @0 && ContactTypeId == @1", county.CountyId, 9) }, "ContactCommunicationMethods").FirstOrDefault(); // 9 is the ContactTypeId for County Official
                    if (externalAuthority != null)
                    {
                        var externalAuthorityContact = new PermitContact
                        {
                            PermitId = application.Id,
                            ContactId = externalAuthority.Id,
                            ContactTypeId = 9, // External Authority (County Official)
                            IsApplicant = false,
                            IsPrimary = true,
                            Active = true
                        };

                        var externalAuthorityContactId = _permitContactService.Save(externalAuthorityContact);
                        if (externalAuthorityContactId > 0)
                        {
                            // Save successful, add to our transaction
                            transactionalDeleteActions.Add(delegate { _permitContactService.Delete(externalAuthorityContactId); });
                        }
                        else
                        {
                            // Save failed
                            throw new Exception("Error saving External Authority Contact");
                        }
                    }
                }

                // Set Application Status to "Complete"
                application.PermitStatusId = 3; // PermitStatusId of 3 == "Complete"
                application.SubmittedBy = submittedBy;
                application.SubmittedDate = Convert.ToDateTime(submittedDate);

                // Remove navigation properties (they will cause an "already exists"
                // EF exception if they are persisted).
                application.PermitContacts = null;
                application.PermitCounties = null;
                application.PermitWaterWithdrawalPurposes = null;
                application.PermitWithdrawalGroundwaters = null;
                application.PermitWithdrawalSurfacewaters = null;

                returnMessage = "Based on the information provided in this application, the requested water appropriation is not eligible for an exemption. This application will enter the water appropriation permitting process.";

                // Save Application and return the newly created Id
                return Json(new { Success = true, Message = returnMessage, PermitId = _permittingService.Save(application) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Delete all records created before the error
                transactionalDeleteActions.ForEach(t => t.Invoke());

                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Private Members
        private bool IsExemption(Permit application)
        {
            // An exemption is one that is non-agricultural, has less that 5,000 gallons per day
            // of total water use, is not for a community water system or groundwater heat pump,
            // and is not within a Water Management Strategy Area            
            var isAgricultural = application.PermitWaterWithdrawalPurposes.Any(pwp => pwp.LU_WaterWithdrawalPurpose.WaterWithdrawalPurposeCategoryId == 1);

            long galPerDay = 0;
            var groundwater = application.PermitWithdrawalGroundwaters.FirstOrDefault();
            if (groundwater != null)
            {
                galPerDay += groundwater.AvgGalPerDay;
            }

            var isSurfacewater = application.PermitWithdrawalSurfacewaters.Any();

            var communityWaterSystem = application.PermitWaterWithdrawalPurposes.Any(pwp => pwp.LU_WaterWithdrawalPurpose.WaterWithdrawalPurposeCategoryId == 5);
            var groundwaterHeatPump = application.PermitWaterWithdrawalPurposes.Any(pwp => pwp.WaterWithdrawalPurposeId == 55);

            var isInWmStrategyArea = application.IsInWMStrategyArea.GetValueOrDefault();

            return !isAgricultural && !isSurfacewater && galPerDay > 0 && galPerDay <= 5000 && !communityWaterSystem && !groundwaterHeatPump && !isInWmStrategyArea;
        }

        private List<string> getParcels(int PermitId)
        {
            List<string> temp = new List<string>();
            var parcels = _permitParcelService.GetRange(0, int.MaxValue, null, new DynamicFilter[] { new DynamicFilter("PermitId==@0", PermitId) });
            if (parcels.Count() == 0)
            {
                return null;
            }

            foreach (var item in parcels)
            {
                temp.Add(item.ParcelId);
            }
            return temp;
        }

        private IEnumerable<PermitWithdrawalGroundwaterDetail> getPermitWithdrawalGroundwaterDetail(int PermitId)
        {
            var permitWithdrawalGroundwater = _permitWithdrawalGroundwaterService.GetRange(0, 1, null, new DynamicFilter[] { new DynamicFilter("PermitId==@0", PermitId) }, "PermitWithdrawalGroundwaterDetails.LU_WaterWithdrawalType").SingleOrDefault();

            if (permitWithdrawalGroundwater != null)
            {
                return permitWithdrawalGroundwater.PermitWithdrawalGroundwaterDetails;
            }

            return null;
        }

        private IEnumerable<PermitWithdrawalSurfacewaterDetail> getPermitWithdrawalSurfacewaterDetail(int PermitId)
        {
            var permitWithdrawalSurfacewater = _permitWithdrawalSurfacewaterService.GetRange(0, 1, null, new DynamicFilter[] { new DynamicFilter("PermitId==@0", PermitId) }, "PermitWithdrawalSurfacewaterDetails.LU_WaterWithdrawalType").SingleOrDefault();

            if (permitWithdrawalSurfacewater != null)
            {
                return permitWithdrawalSurfacewater.PermitWithdrawalSurfacewaterDetails;
            }

            return null;
        }

        private List<Permit> GetRelatedPermits(int id)
        {
            var permit = _permittingService.GetById(id, "LU_PermitStatus");
            int application;

            if (permit.LU_PermitStatus.PermitCategoryId == 1)
            {
                application = id;
            }
            else
            {
                if (permit.RefId != null)
                {
                    application = _permittingService.GetById(permit.RefId ?? 0).Id;
                }
                else
                {
                    if (permit.PermitNumber == null)
                    {
                        return null;
                    }
                    return _permittingService.GetRange(0, int.MaxValue, "RevisionNumber", new DynamicFilter[] { new DynamicFilter("PermitNumber==@0", permit.PermitNumber) }, null).ToList();
                }
            }

            var applications = new List<int>();
            applications.Add(application);

            var applicationsDone = new List<int>();

            var permits = new List<Permit>();
            var permits2 = new List<Permit>();

            var result2 = applications.Except(applicationsDone);

            while (applications.Except(applicationsDone).Any())
            {
                var result = applications.Except(applicationsDone);
                application = result.First();

                permits.Add(_permittingService.GetById(application, "LU_PermitStatus"));

                var relatedPermits = _permittingService.GetRange(0, int.MaxValue, "", new DynamicFilter[] { new DynamicFilter("RefId==@0", application) }, null);

                foreach (var item in relatedPermits)
                {
                    permits.Add(item);

                    if (item.PermitNumber != null)
                    {
                        var relatedPermits2 = _permittingService.GetRange(0, int.MaxValue, "", new DynamicFilter[] { new DynamicFilter("PermitNumber==@0 && Id!=@1", item.PermitNumber, item.Id) }, null);
                        foreach (var item2 in relatedPermits2)
                        {
                            if (item2.RefId != null)
                            {
                                applications.Add(item2.RefId ?? 0);
                            }
                        }
                    }
                }

                applicationsDone.Add(application);
            }

            applicationsDone = applicationsDone.OrderBy(x => x).ToList();

            var sortedPermits = new List<Permit>();
            permits = permits.GroupBy(x => x.Id).Select(y => y.First()).ToList();

            foreach (var item in applicationsDone)
            {
                if (!sortedPermits.Any(x => x.Id == item))
                {
                    sortedPermits.Add(permits.First(x => x.Id == item));
                }

                sortedPermits.AddRange(permits.Where(x => x.RefId == item));
                var temp = permits.Where(x => x.RefId == item);
                foreach (var itemInner in temp)
                {
                    if (!sortedPermits.Any(x => x.Id == itemInner.Id))
                    {
                        sortedPermits.Add(itemInner);
                    }
                }
            }

            return sortedPermits.OrderBy(t => t.PermitName).ToList();
        }

        #endregion
    }
}
