﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.Communication.Model;
using System.Configuration;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Controllers
{
    public partial class ChecklistController : BaseController
    {
        private readonly IUpdatableService<Comment> _commentService;
        private readonly IUpdatableService<Permit> _permitService;
        private readonly IUpdatableService<Message> _messageService;
        private readonly IService<LU_TableList> _tableListService;

        public ChecklistController(
            IUpdatableService<Comment> commentService,
            IUpdatableService<Permit> permitService,
            IUpdatableService<Message> messageService,
            IService<LU_TableList> tableListService)
        {
            _commentService = commentService;
            _permitService = permitService;
            _messageService = messageService;
            _tableListService = tableListService;
        }

        [HttpPost]
        public virtual ActionResult SubmitCountyReview(CountyReviewAndApprovalForm form)
        {
            var permit = _permitService.GetById(form.PermitId, "PermitContacts.PermitContactInformation");

            //send notification to project manager
            var manager = permit.PermitContacts.FirstOrDefault(x => x.ContactTypeId == 20 && x.IsPrimary);
            if (manager != null)
            {
                SendNotificationEmail(form.PermitId, manager.ContactId);
            }

            // save comment
            if (!string.IsNullOrEmpty(form.Comment))
            {
                if (form.CommentId == 0) // if adding a new comment
                {
                    Comment comment = new Comment();
                    comment.PermitStatusId = 4;
                    comment.Comment1 = form.Comment;
                    comment.RefId = form.PermitId;
                    comment.RefTableId = _tableListService.GetRange(0, 1, null, new[] { new DynamicFilter("Description == \"Permit\"") }).First().Id;
                    _commentService.Save(comment);
                }
                else // if updating a comment
                {
                    var comment = _commentService.GetById(form.CommentId);
                    comment.Comment1 = form.Comment;
                    _commentService.Save(comment);
                }
            }

            // Set the appropriate fields on the permit record
            permit.CountyOfficialSignOffBy = User.Identity.Name;
            permit.CountyOfficialSignOffDate = DateTime.Now;
            permit.CountyOfficialSignOffYesNo = form.IsChecked;
            permit.PermitContacts = null;
            _permitService.Save(permit);

            return Json(new { IsSuccess = true });
        }

        private void SendNotificationEmail(int permitId, int managerId)
        {
            var uri = new UriBuilder(Request.Url);
            uri.Query = null;
            uri.Path = Url.Action(MVC.Permitting.Permit.Details(permitId));
            var permitDetailsUrl = uri.Uri.ToString();
            
            //Create a message object for the change
            var message = new Message();
            message.ContactIds = new List<int> { managerId };
            message.NotificationText = "County Review and Approval is complete";
            message.NotificationTypeId = 1;
            message.PermitId = permitId;
            message.StartDate = DateTime.MinValue;
            message.EndDate = DateTime.MaxValue;
            message.From = ConfigurationManager.AppSettings["SenderEmail"];
            message.Subject = "County Review and Approval is complete";
            message.Body = "County Review and Approval is complete. <p><a href=\"" + permitDetailsUrl + "\">" + permitDetailsUrl + "</a></p>";

            _messageService.Save(message);
        }
    }
}
