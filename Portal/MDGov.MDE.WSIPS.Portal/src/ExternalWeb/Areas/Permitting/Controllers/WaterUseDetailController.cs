﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.ServiceClient;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Controllers
{
    public partial class WaterUseDetailController : BaseController
    {
        private readonly IUpdatableService<PermitWwLivestock> _permitWwLivestockService;
        private readonly IUpdatableService<PermitWwAgricultural> _permitWwAgriculturalService;
        private readonly IService<LU_WaterWithdrawalPurposeCategory> _WaterWithdrawalPurposeCategory;
        private readonly IUpdatableService<PermitWwPoultry> _permitWaterWithdrawalPoultryService;
        private readonly IUpdatableService<PermitWwCrop> _permitWwCropService;
        private readonly IWaterUseDetailServiceClient _waterUseDetailsServiceClient;
        private readonly IUpdatableService<LU_CropYieldUnit> _cropYieldUnitService;

        public WaterUseDetailController(
            IUpdatableService<PermitWwLivestock> permitWwLivestockService,
            IUpdatableService<PermitWwAgricultural> permitWwAgriculturalService,
            IService<LU_WaterWithdrawalPurposeCategory> WaterWithdrawalPurposeCategory,
            IUpdatableService<PermitWwPoultry> permitWaterWithdrawalPoultryService,
            IUpdatableService<PermitWwCrop> permitWwCropService,
            IWaterUseDetailServiceClient waterUseDetailsServiceClient,
            IUpdatableService<LU_CropYieldUnit> cropYieldUnitService)
        {
            _permitWwLivestockService = permitWwLivestockService;
            _permitWwAgriculturalService = permitWwAgriculturalService;
            _WaterWithdrawalPurposeCategory = WaterWithdrawalPurposeCategory;
            _waterUseDetailsServiceClient = waterUseDetailsServiceClient;
            _permitWaterWithdrawalPoultryService = permitWaterWithdrawalPoultryService;
            _permitWwCropService = permitWwCropService;
            _cropYieldUnitService = cropYieldUnitService;
        }

        [HttpGet]
        public virtual ActionResult LoadWaterUseCategoryAndType()
        {
            IEnumerable<LU_WaterWithdrawalPurposeCategory> values = _WaterWithdrawalPurposeCategory.GetAll("LU_WaterWithdrawalPurpose");

            return PartialView(MVC.Permitting.Permit.Views.WaterUseCategoryAndType, values);
        }

        [HttpGet]
        public virtual ActionResult LoadWaterUseDetails(int? permitId, string selectedWaterUserCategoryType)
        {
            var form = new ApplicationWizard();

            form.WaterUseDetailForm = _waterUseDetailsServiceClient.InitializeWaterUseDetailFrom(Convert.ToInt32(permitId));

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetails, form);
        }

        #region Poultry Watering

        [HttpGet]
        public virtual ActionResult AddPoultryWateringEvaporative(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwPoultry, PoultryWateringForm>(new PermitWwPoultry());

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.PoultryWateringEvaporative, form);
        }

        [HttpGet]
        public virtual ActionResult AddPoultryWateringFogger(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwPoultry, PoultryWateringForm>(new PermitWwPoultry());

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.PoultryWateringFogger, form);
        }

        [HttpDelete]
        public virtual JsonResult DeletePoultryWatering(int id)
        {
            try
            {
                _permitWaterWithdrawalPoultryService.Delete(id);

                return Json(new { sucess = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { sucess = false });
            }
        }
        
        #endregion 

        #region LivestockWatering

        [HttpGet]
        public virtual ActionResult AddLivestockWatering(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwLivestock, LivestockForm>(new PermitWwLivestock());

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.LivestockWatering, form);
        }

        [HttpDelete]
        public virtual JsonResult Delete(int id)
        {
            try
            {
                _permitWwLivestockService.Delete(id);

                return Json(new { sucess = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { sucess = false });
            }
        }

        #endregion

        #region Other LivestockWatering

        [HttpGet]
        public virtual ActionResult AddOtherLivestockWatering(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwLivestock, LivestockForm>(new PermitWwLivestock());

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.OtherLivestockWatering, form);
        }

        [HttpDelete]
        public virtual JsonResult DeleteOtherLivestockWatering(int id)
        {
            try
            {
                _permitWwLivestockService.Delete(id);

                return Json(new { sucess = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { sucess = false });
            }
        }

        #endregion

        #region Dairy Animal Watering

        [HttpGet]
        public virtual ActionResult AddDairyAnimalWateing(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwLivestock, LivestockForm>(new PermitWwLivestock());
            //form.PermitWaterWithdrawalPurposeId = permitWaterWithdrawalPurposeId;

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.DairyAnimalWatering, form);
        }

        [HttpDelete]
        public virtual JsonResult DeleteDairyAnimalWateing(int id)
        {
            try
            {
                _permitWwLivestockService.Delete(id);

                return Json(new { sucess = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { sucess = false });
            }
        }

        #endregion

        [HttpGet]
        public virtual ActionResult AddCrop(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwCrop, CropForm>(new PermitWwCrop());
            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.Crop, form);
        }

        [HttpDelete]
        public virtual JsonResult DeleteCrop(int id)
        {
            try
            {
                _permitWwCropService.Delete(id);

                return Json(new { sucess = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { sucess = false });
            }
        }

        #region Nursery Irrigation

        [HttpGet]
        public virtual ActionResult AddNurseryStock(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwAgricultural, PermitWwIrrigationForm>(new PermitWwAgricultural());

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.NurseryStock, form);
        }

        [HttpDelete]
        public virtual JsonResult DeleteNurseryStock(int id)
        {
            try
            {
                _permitWwAgriculturalService.Delete(id);

                return Json(new { sucess = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { sucess = false });
            }
        }

        #endregion

        #region Sod Irrigation

        [HttpGet]
        public virtual ActionResult AddSod(int index)
        {
            ViewData["index"] = index;
            var form = Mapper.Map<PermitWwAgricultural, PermitWwIrrigationForm>(new PermitWwAgricultural());

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.Sod, form);
        }

        [HttpDelete]
        public virtual JsonResult DeleteSod(int id)
        {
            try
            {
                _permitWwAgriculturalService.Delete(id);

                return Json(new { sucess = true });
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return Json(new { sucess = false });
            }
        }

        #endregion

        #region PrivateWater Supply

        [HttpGet]
        public virtual ActionResult AddPrivateWaterSupply(string waterUseCategoryTypeids, int permitId)
        {
            var forms = _waterUseDetailsServiceClient.GetPrivateWaterSupplierForms(waterUseCategoryTypeids, permitId);

            var waterUserDetails = new WaterUseDetailForm();
            waterUserDetails.PrivateWaterSuppilerForm.PrivateWaterSupplierForms = forms;

            return PartialView(MVC.Permitting.Permit.Views.WaterUseDetailTemplates.PrivateWaterSuppliers, waterUserDetails);
        }

        #endregion

        #region Waste Water Treatment and disposal

        [HttpGet]
        public virtual ActionResult AddWastewaterTreatmentDisposal(string waterUseCategoryTypeids, int permitId)
        {
            //WastewaterTreatmentAndDisposalForm
            var forms = _waterUseDetailsServiceClient.GetWasteWaterTreatmentAndDisposal(waterUseCategoryTypeids, permitId);

            var wizard = new ApplicationWizard();
            wizard.WastewaterTreatmentDisposalForm = forms;

            return PartialView(MVC.Permitting.Permit.Views.WastewaterTreatmentAndDisposal, wizard);
        }

        #endregion
    }
}
