﻿using AutoMapper;
using MDGov.MDE.Common.Helpers;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using CustomCondition = MDGov.MDE.Common.Model.CustomCondition;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Controllers
{
    /// <summary>
    /// Contains methods for managing both standard and custom conditions
    /// </summary>
    public partial class ConditionController : BaseController
    {
        private readonly IService<LU_StandardConditionType> _standardConditionTypeService;
        private readonly IService<LU_PermitType> _permitTypeService;
        private readonly IService<LU_PermitTemplate> _permitTemplateService;
        private readonly IService<LU_ConditionReportingPeriod> _reportingPeriodService;
        private readonly IUpdatableService<PermitCondition> _permitConditionService;
        private readonly IUpdatableService<Permit> _permitService;
        private readonly IService<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterService;
        private readonly IService<PermitWithdrawalGroundwater> _permitWithdrawalGroundwaterService;
        private readonly IUpdatableService<ConditionCompliance> _conditionComplianceService;
        private readonly IUpdatableService<PumpageReport> _pumpageReportService;
        private readonly IService<LU_PermitComplianceStatus> _permitComplianceStatusService;
        private readonly IService<LU_PermitConditionComplianceStatus> _conditionComplianceStatusService;
        private readonly IService<LU_WaterWithdrawalEstimate> _waterWithdrawalEstimateService;
        private readonly IService<LU_Month> _monthService;
        private readonly IService<Document> _documentService;
        private readonly IService<LU_CropType> _cropTypeService;
        
        public ConditionController(IService<LU_StandardConditionType> standardConditionTypeService,
            IService<LU_PermitType> permitTypeService,
            IService<LU_PermitTemplate> permitTemplateService,
            IService<LU_ConditionReportingPeriod> reportingPeriodService,
            IUpdatableService<PermitCondition> permitConditionService,
            IUpdatableService<Permit> permitService,
            IService<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterService,
            IService<PermitWithdrawalGroundwater> permitWithdrawalGroundwaterService,
            IUpdatableService<ConditionCompliance> conditionComplianceService,
            IUpdatableService<PumpageReport> pumpageReportService,
            IService<LU_PermitComplianceStatus> permitComplianceStatusService,
            IService<LU_PermitConditionComplianceStatus> conditionComplianceStatus,
            IService<LU_WaterWithdrawalEstimate> waterWithdrawalEstimateService,
            IService<LU_Month> monthService,
            IService<Document> documentService,
            IService<LU_CropType> cropTypeService)
        {
            _standardConditionTypeService = standardConditionTypeService;
            _permitTypeService = permitTypeService;
            _permitTemplateService = permitTemplateService;
            _reportingPeriodService = reportingPeriodService;
            _permitConditionService = permitConditionService;
            _permitService = permitService;
            _permitWithdrawalSurfacewaterService = permitWithdrawalSurfacewaterService;
            _permitWithdrawalGroundwaterService = permitWithdrawalGroundwaterService;
            _conditionComplianceService = conditionComplianceService;
            _pumpageReportService = pumpageReportService;
            _permitComplianceStatusService = permitComplianceStatusService;
            _conditionComplianceStatusService = conditionComplianceStatus;
            _waterWithdrawalEstimateService = waterWithdrawalEstimateService;
            _monthService = monthService;
            _documentService = documentService;
            _cropTypeService = cropTypeService;
        }

        //
        // GET: /Permitting/Condition/PermitData

        [HttpGet]
        public virtual JsonResult PermitData(GridCommand command, int permitId, bool complianceOnly = false)
        {
            var permit = _permitService.GetById(permitId, "PermitConditions,PermitWithdrawalGroundwaters,PermitWithdrawalSurfacewaters");

            var surfaceAvg = permit.PermitWithdrawalSurfacewaters.FirstOrDefault();
            var groundAvg = permit.PermitWithdrawalGroundwaters.FirstOrDefault();
            IEnumerable<PermitCondition> permitConditions = permit.PermitConditions.OrderBy(pc => pc.Sequence);

            if (complianceOnly)
            {
                permitConditions = permitConditions.Where(pc => pc.RequiresSelfReporting.GetValueOrDefault());
            }

            var customConditions = Mapper.Map<IEnumerable<PermitCondition>, IEnumerable<CustomConditionForm>>(permitConditions);

            if (complianceOnly)
            {
                foreach (var item in customConditions)
                {
                    var nextConditionCompliance = _conditionComplianceService.GetRange(0, Int32.MaxValue, "ComplianceReportingDueDate", new[] { new DynamicFilter("PermitConditionId == @0", item.Id) }, "LU_PermitConditionComplianceStatus").FirstOrDefault(c => c.PermitConditionComplianceStatusId == 4 /*Pending*/);

                    if (nextConditionCompliance != null)
                    {
                        item.Status = nextConditionCompliance.LU_PermitConditionComplianceStatus.Description;
                        item.DueDate = nextConditionCompliance.ComplianceReportingDueDate;
                    }
                }

                customConditions = customConditions.OrderBy(c => c.DueDate);
            }

            var list = (from item in customConditions
                        select new CustomCondition
                        {
                            Id = item.Id,
                            PermitId = item.PermitId,
                            ConditionComplianceId = item.ConditionComplianceId,
                            ConditionText = item.ConditionText,
                            DueDate = item.DueDate,
                            OneTimeReportDays = item.OneTimeReportDays
                        }).ToList();

            var modifiedConditions = Formatter.ReplaceVariables(list, permit, surfaceAvg, groundAvg).ToList();
            var count = modifiedConditions.Count();

            return Json(new GridData
            {
                Data = modifiedConditions,
                PageTotal = 1,
                CurrentPage = command.Page,
                RecordCount = count
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Permitting/Condition/CustomDetails

        [HttpGet]
        public virtual ActionResult CustomDetails(int id)
        {
            var condition = _permitConditionService.GetById(id);
            var conditions = new List<CustomConditionForm>();

            if (condition.PermitId != null)
            {
                var permitCondition = Mapper.Map<PermitCondition, CustomConditionForm>(condition);
                var permit = _permitService.GetById((int)condition.PermitId);
                var surfaceAvg = _permitWithdrawalSurfacewaterService.GetRange(0, 1, null, new[] { new DynamicFilter("PermitId == " + permit.Id) }).FirstOrDefault();
                var groundAvg = _permitWithdrawalGroundwaterService.GetRange(0, 1, null, new[] { new DynamicFilter("PermitId == " + permit.Id) }).FirstOrDefault();

                conditions.Add(permitCondition);

                var list = (from item in conditions
                            select new CustomCondition
                            {
                                ConditionText = item.ConditionText,
                                DueDate = item.DueDate
                            }).ToList();

                var firstOrDefault = Formatter.ReplaceVariables(list, permit, surfaceAvg, groundAvg).FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var conditionText = firstOrDefault.ConditionText;
                    permitCondition.ConditionText = conditionText;
                }

                var compliance = _conditionComplianceService.GetRange(0, Int32.MaxValue, null,
                                                                  new[] { new DynamicFilter("PermitConditionId == " + id) },
                                                                  "LU_PermitConditionComplianceStatus").FirstOrDefault(c => (c.ComplianceReportingDueDate <= DateTime.Now) && (c.ComplianceReportingStartDate <= DateTime.Now));

                if (compliance != null)
                {
                    permitCondition.Status = compliance.LU_PermitConditionComplianceStatus.Description;
                    permitCondition.DueDate = compliance.ComplianceReportingDueDate;
                }
                else
                {
                    var pending = _conditionComplianceService.GetRange(0, Int32.MaxValue, null, new[] { new DynamicFilter("PermitConditionId == " + permitCondition.Id) }, "LU_PermitConditionComplianceStatus").FirstOrDefault(c => (c.ComplianceReportingDueDate > DateTime.Now));
                    if (pending != null) permitCondition.Status = pending.LU_PermitConditionComplianceStatus.Description;
                }

                return PartialView(MVC.Permitting.Condition.Views._ConditionDetails, permitCondition);
            }

            var customCondition = Mapper.Map<PermitCondition, CustomCondition>(condition);

            return PartialView(MVC.Permitting.Condition.Views._ConditionDetails, customCondition);
        }

        //
        // GET: /Permitting/Condition/GetReportHistory

        [HttpGet]
        public virtual JsonResult GetReportHistory(GridCommand command, int permitConditionId)
        {
            var conditionTypeId = _permitConditionService.GetById(permitConditionId).StandardConditionTypeId;
            var reports = _conditionComplianceService.GetRange(0, Int32.MaxValue, null, new[]
            {
                new DynamicFilter("PermitConditionId == " + permitConditionId)
            }, "LU_PermitConditionComplianceStatus");

            var conditionReports = Mapper.Map<IEnumerable<ConditionCompliance>, IEnumerable<ConditionReport>>(reports).OrderBy(r => r.DueDate).ToList();

            foreach (var item in conditionReports)
            {
                var pumpageReport = _pumpageReportService.GetRange(0, int.MaxValue, "ReceivedDate desc", new[] { new DynamicFilter("Active && ConditionComplianceId == @0", item.Id) }).FirstOrDefault();
                if (pumpageReport != null)
                {
                    item.ReceivedDate = pumpageReport.ReceivedDate;
                    item.SubmittedDate = pumpageReport.CreatedDate;
                }

                item.StandardConditionTypeId = conditionTypeId;
                var upload = _documentService.GetRange(0, 1, null, new[] { new DynamicFilter("RefId == @0 && RefTableId == @1", item.Id, 69) }).SingleOrDefault();
                if (upload != null)
                {
                    item.ExistingDocumentId = upload.Id;
                    item.ReceivedDate = upload.CreatedDate;
                    item.SubmittedDate = upload.CreatedDate;
                }
            }

            var count = conditionReports.Count();
            var pageTotal = (int)Math.Ceiling(count / (float)command.PageSize);

            return Json(new GridData
            {
                Data = conditionReports,
                PageTotal = pageTotal,
                CurrentPage = command.Page,
                RecordCount = count
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult ComplianceCustomDetails(int permitConditionId, int conditionComplianceId)
        {
            var condition = _permitConditionService.GetById(permitConditionId);
            var conditions = new List<CustomConditionForm>();

            ViewBag.ViewUploadBaseUrl = WebConfigurationManager.AppSettings["DocumentGetPath"];

            if (condition.PermitId != null)
            {
                var permitCondition = Mapper.Map<PermitCondition, CustomConditionForm>(condition);
                var permit = _permitService.GetById((int)condition.PermitId);

                if (condition.StandardConditionTypeId == 14) permitCondition.PumpageReports = true;

                conditions.Add(permitCondition);

                var list = (from item in conditions
                            select new CustomCondition
                            {
                                ConditionText = item.ConditionText,
                                DueDate = item.DueDate
                            }).ToList();

                var firstOrDefault =
                    Formatter.ReplaceVariables(list, permit, permit.PermitWithdrawalSurfacewaters.FirstOrDefault(),
                                               permit.PermitWithdrawalGroundwaters.FirstOrDefault()).FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var conditionText = firstOrDefault.ConditionText;
                    permitCondition.ConditionText = conditionText;
                }

                var compliance = _conditionComplianceService.GetById(conditionComplianceId,
                                                                     "LU_PermitConditionComplianceStatus");
                if (compliance != null)
                {
                    permitCondition.Status = compliance.LU_PermitConditionComplianceStatus.Description;
                    permitCondition.ConditionComplianceId = compliance.Id;
                }

                return PartialView(MVC.Permitting.Condition.Views._ComplianceConditionDetails, permitCondition);
            }
            var customCondition = Mapper.Map<PermitCondition, CustomCondition>(condition);
            return PartialView(MVC.Permitting.Condition.Views._ComplianceConditionDetails, customCondition);
        }

        //
        // GET: /Permitting/Condition/ViewPumpageReport

        [HttpGet]
        public virtual ActionResult ViewPumpageReport(int id)
        {
            var report = _pumpageReportService.GetById(id, "PumpageReportDetails, LU_WaterWithdrawalEstimate, LU_CropType");
            var details = Mapper.Map<PumpageReport, PumpageReportDetailsForm>(report);

            var permitConditionId = _conditionComplianceService.GetById((int)report.ConditionComplianceId).PermitConditionId;
            details.PermitConditionId = permitConditionId;
            details.PumpageReportTypeId = _permitConditionService.GetById(permitConditionId).PumpageReportTypeId;

            GetWaterWithdrawalEstimateTypes();
            GetCropTypes();
            GetReportMonths();

            return PartialView(MVC.Permitting.Condition.Views._ViewPumpageReport, details);
        }

        //
        // GET: /Permitting/Condition/EditPumpageReport

        [HttpGet]
        public virtual ActionResult EditPumpageReport(int id, int conditionComplianceId = 0, bool resubmit = false)
        {
            var report = _pumpageReportService.GetById(id, "PumpageReportDetails, LU_WaterWithdrawalEstimate");
            var details = Mapper.Map<PumpageReport, PumpageReportDetailsForm>(report);
            if (id == 0)
            {
                details = new PumpageReportDetailsForm();
                var compliance = _conditionComplianceService.GetById(conditionComplianceId);
                if (compliance != null)
                {
                    details.PermitConditionId = compliance.PermitConditionId;
                    details.ReportYear = Convert.ToDateTime(compliance.ComplianceReportingStartDate).Year.ToString(CultureInfo.InvariantCulture);
                    details.PumpageReportTypeId = _permitConditionService.GetById(compliance.PermitConditionId).PumpageReportTypeId;
                }
                details.Name = "Water Withdrawal Report";
                details.IsResumbission = resubmit;
            }
            else
            {
                if (report.ConditionComplianceId != null)
                {
                    var permitConditionId = _conditionComplianceService.GetById((int)report.ConditionComplianceId).PermitConditionId;
                    var compliance = _conditionComplianceService.GetById((int)report.ConditionComplianceId);
                    if (compliance != null)
                    {
                        details.PumpageReportTypeId = _permitConditionService.GetById(compliance.PermitConditionId).PumpageReportTypeId;
                    }
                    details.PermitConditionId = permitConditionId;
                }
            }

            GetWaterWithdrawalEstimateTypes();
            GetCropTypes();
            GetReportMonths();
            return PartialView(MVC.Permitting.Condition.Views._CreateOrEditPumpageReport, details);
        }

        //
        // POST: /Permitting/Condition/EditPumpageReport

        [HttpPost]
        public virtual ActionResult EditPumpageReport(PumpageReportDetailsForm report)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (report.WaterWithdrawalEstimateId == 4 && report.OtherWaterWithdrawalMethod == null) // Other
                    {
                        ModelState.AddModelError(String.Empty, "Please specify Other Withdrawal Estimate Method");
                    }
                    else
                    {
                        var pumpageReport = Mapper.Map<PumpageReportDetailsForm, PumpageReport>(report);

                        pumpageReport.Active = true;

                        if (pumpageReport.Id == 0) // New submission
                        {
                            pumpageReport.ReceivedDate = DateTime.Now;
                        }

                        if (pumpageReport.ConditionComplianceId != null)
                        {
                            if (report.IsResumbission)
                            {
                                // Get existing reports for this condition compliance id and set them to be inactive
                                var existing = _pumpageReportService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("ConditionComplianceId == @0 && Active", pumpageReport.ConditionComplianceId) });
                                foreach (var existingReport in existing)
                                {
                                    existingReport.Active = false;
                                    _pumpageReportService.Save(existingReport);
                                }
                            }
                        }

                        pumpageReport.PumpageReportDetails = report.PumpageReportDetails;

                        foreach (var item in pumpageReport.PumpageReportDetails)
                        {
                            item.CreatedBy = User.Identity.Name;
                            item.LastModifiedBy = User.Identity.Name;
                        }

                        var id = _pumpageReportService.Save(pumpageReport);

                        // Update Condition Compliance status to "Submitted"
                        if (pumpageReport.ConditionComplianceId != null)
                        {
                            var conditionCompliance = _conditionComplianceService.GetById((int)pumpageReport.ConditionComplianceId);
                            conditionCompliance.PermitConditionComplianceStatusId = 5;
                            _conditionComplianceService.Save(conditionCompliance);
                        }

                        return Json(new { IsSuccess = true }, JsonRequestBehavior.AllowGet);
                    }
                }

                var jsonErrors = ModelState.Values.Where(v => v.Errors.Any()).SelectMany(t => t.Errors.Select(e => e.ErrorMessage));

                return Json(new { IsSuccess = false, Errors = jsonErrors }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                HandleException(exc);
                return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Permitting/Condition/GetPumpageReports

        [HttpGet]
        public virtual JsonResult GetPumpageReports(GridCommand command, int id)
        {
            var reports = _pumpageReportService.GetRange(0, 1, "CreatedDate desc",
                new[] {new DynamicFilter("ConditionComplianceId == @0", id)}).ToList();

            var count = reports.Count;
            var pageTotal = (int)Math.Ceiling(count / (float)command.PageSize);

            return Json(new GridData
            {
                Data = reports,
                PageTotal = pageTotal,
                CurrentPage = command.Page,
                RecordCount = count
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Permitting/Condition/GetStandardReports

        [HttpGet]
        public virtual JsonResult GetStandardReports(GridCommand command, int id)
        {
            var reports = _documentService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("RefId == @0 && RefTableId == @1", id, 69) }).ToList();

            var count = reports.Count();
            var pageTotal = (int)Math.Ceiling(count / (float)command.PageSize);

            return Json(new GridData
            {
                Data = reports,
                PageTotal = pageTotal,
                CurrentPage = command.Page,
                RecordCount = count
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Permitting/Condition/EditConditionComplianceStatus

        [HttpGet]
        public virtual ActionResult EditConditionComplianceStatus(int id)
        {
            var statusId = _conditionComplianceService.GetById(id).PermitConditionComplianceStatusId;
            ViewBag.ConditionCompliance = id;
            ViewBag.PermitConditionComplianceStatusId = statusId;
            GetConditionComplianceStatuses();
            return PartialView(MVC.Permitting.Condition.Views._EditPermitConditionComplianceStatus);
        }

        //
        // POST: /Permitting/Condition/EditConditionComplianceStatus

        [HttpPost]
        public virtual ActionResult EditConditionComplianceStatus(int complianceStatusId, int id)
        {
            try
            {
                var compliance = _conditionComplianceService.GetById(id);
                if (compliance != null)
                {
                    compliance.PermitConditionComplianceStatusId = complianceStatusId;
                    _conditionComplianceService.Save(compliance);
                }
                return Json(new { IsSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exc)
            {
                HandleException(exc);
                return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets a list of standard condition types. Used for populating a drop-down list.
        /// </summary>
        private void GetConditionTypes()
        {
            ViewBag.ConditionTypes = _standardConditionTypeService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        /// <summary>
        /// Gets a list of permit types. Used for populating a drop-down list.
        /// </summary>
        private void GetPermitTypes()
        {
            ViewBag.PermitTypes = _permitTypeService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        /// <summary>
        /// Gets a list of permit types. Used for populating a drop-down list.
        /// </summary>
        private void GetPermitTemplates()
        {
            ViewBag.PermitTemplates = _permitTemplateService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        /// <summary>
        /// Gets a list of Condition Report Types. Used for populating a drop-down list.
        /// </summary>
        private void GetReportingPeriods()
        {
            ViewBag.ReportingPeriods = _reportingPeriodService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        /// <summary>
        /// 
        /// </summary>
        private void GetComplianceStatuses()
        {
            ViewBag.PermitComplianceStatuses = _permitComplianceStatusService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        /// <summary>
        /// 
        /// </summary>
        private void GetConditionComplianceStatuses()
        {
            ViewBag.ConditionComplianceStatuses = _conditionComplianceStatusService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        private void GetWaterWithdrawalEstimateTypes()
        {
            ViewBag.WaterWithdrawalEstimateTypes = _waterWithdrawalEstimateService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        private void GetReportMonths()
        {
            ViewBag.Months = _monthService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

        /// <summary>
        /// Gets a list of Crop Types. Used for populating a drop-down list (set of checkboxes).
        /// </summary>
        private void GetCropTypes()
        {
            ViewBag.CropTypes = _cropTypeService.GetRange(0, Int32.MaxValue, "Sequence", new[] { new DynamicFilter("Active == True") });
        }

    }
}
