﻿using AutoMapper;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Models.Forms;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Controllers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Enum;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Filters;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Geometry;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using DynamicFilter = MDGov.MDE.Common.Model.DynamicFilter;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting.Controllers
{
    [Authorization]
    public partial class DashboardController : BaseController
    {
        private readonly IService<LU_County> _countyService;
        private readonly IService<LU_WaterWithdrawalSource> _waterWithdrawalSourceService;
        private readonly IService<LU_PermitType> _permitTypeService;
        private readonly IService<LU_PermitStatus> _permitStatusService;
        private readonly IService<Permit> _permitService;
        private readonly IDashboardPermitService _dashboardPermitService;
        private readonly IService<PermitContact> _permitContactService;

        public DashboardController(
            IService<LU_County> countyService,
            IService<LU_WaterWithdrawalSource> waterWithdrawalSourceService,
            IService<LU_PermitType> permitTypeService,
            IService<LU_PermitStatus> permitStatusService,
            IService<Permit> permitService,
            IDashboardPermitService dashboardPermitService,
            IService<PermitContact> permitContactService)
        {
            _countyService = countyService;
            _waterWithdrawalSourceService = waterWithdrawalSourceService;
            _permitTypeService = permitTypeService;
            _permitStatusService = permitStatusService;
            _permitService = permitService;
            _dashboardPermitService = dashboardPermitService;
            _permitContactService = permitContactService;
        }

        [HttpGet]
        public virtual ActionResult Index(DashboardMode? mode = null, int? permitId = null)
        {
            var form = new DashboardSearchForm
            {
                DisplayMode = mode ?? DashboardMode.Tabular,
                SpatialFilterForm = new DashboardSpatialFilterForm()
            };

            switch (form.DisplayMode)
            {
                case DashboardMode.Tabular:
                case DashboardMode.Map:
                case DashboardMode.Analysis:
                    if (form.DisplayMode == DashboardMode.Analysis)
                    {
                        if (!permitId.HasValue)
                        {
                            return RedirectToAction(MVC.Permitting.Dashboard.Index(DashboardMode.Tabular));
                        }

                        form.SelectedPermit = _dashboardPermitService.GetById(permitId.Value);
                    }

                    var dashboardFilterFromSession = SessionHandler.GetSessionVar(SessionVariables.DashboardFilter);
                    if (dashboardFilterFromSession != null)
                    {
                        form.FilterForm = SessionHandler.GetSessionVar<DashboardFilterForm>(SessionVariables.DashboardFilter);
                        form.SpatialFilterForm = SessionHandler.GetSessionVar<DashboardSpatialFilterForm>(SessionVariables.DashboardSpatialFilter);
                    }
                    else
                    {
                        form.FilterForm = new DashboardFilterForm(FilterTemplate.DefaultAuthenticated);
                        form.SpatialFilterForm = new DashboardSpatialFilterForm();
                    }

                    SessionHandler.SetSessionVar(SessionVariables.DashboardFilter, form.FilterForm);
                    SessionHandler.SetSessionVar(SessionVariables.DashboardSpatialFilter, form.SpatialFilterForm);

                    form.Rehydrate();

                    return View(form);
                default:
                    return RedirectToAction(MVC.Home.Index());
            }
        }

        [HttpGet]
        public virtual ActionResult PublicSearch(DashboardMode? mode = null, int? permitId = null)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction(MVC.Permitting.Dashboard.Index(mode, permitId));
            }

            var form = new DashboardSearchForm
            {
                DisplayMode = mode ?? DashboardMode.Tabular,
                SpatialFilterForm = new DashboardSpatialFilterForm()
            };

            switch (form.DisplayMode)
            {
                case DashboardMode.Tabular:
                case DashboardMode.Map:
                case DashboardMode.Analysis:
                    if (form.DisplayMode == DashboardMode.Analysis)
                    {
                        if (!permitId.HasValue)
                        {
                            return RedirectToAction(MVC.Permitting.Dashboard.Index(DashboardMode.Tabular));
                        }

                        form.SelectedPermit = _dashboardPermitService.GetById(permitId.Value);
                    }

                    var dashboardFilterFromSession = SessionHandler.GetSessionVar(SessionVariables.DashboardFilter);
                    if (dashboardFilterFromSession != null)
                    {
                        form.FilterForm = SessionHandler.GetSessionVar<DashboardFilterForm>(SessionVariables.DashboardFilter);
                        form.SpatialFilterForm = SessionHandler.GetSessionVar<DashboardSpatialFilterForm>(SessionVariables.DashboardSpatialFilter);
                    }
                    else
                    {
                        form.FilterForm = new DashboardFilterForm(FilterTemplate.Public);
                        form.SpatialFilterForm = new DashboardSpatialFilterForm();
                    }

                    SessionHandler.SetSessionVar(SessionVariables.DashboardFilter, form.FilterForm);
                    SessionHandler.SetSessionVar(SessionVariables.DashboardSpatialFilter, form.SpatialFilterForm);

                    form.Rehydrate();

                    return View(form);
                default:
                    return RedirectToAction(MVC.Home.Index());
            }
        }

        [HttpGet]
        public virtual JsonResult Data(GridCommand command)
        {
            var filterForm = SessionHandler.GetSessionVar<DashboardFilterForm>(SessionVariables.DashboardFilter);

            if (filterForm == null)
            {
                return Json(new
                {
                    GridModel = new GridData
                    {
                        Data = null,
                        PageTotal = 0,
                        CurrentPage = 0,
                        RecordCount = 0
                    }
                }, JsonRequestBehavior.AllowGet);
            }

            var propertyMap = new Dictionary<string, string>
            {
                { "permitname", "PermitName" },
                { "county", "CountyCode" },
                { "permittee", "PermitteeName" },
                { "permitcategory", "PermitCategory" },
                { "permitstatus", "PermitStatus" },
                { "islarge", "IsLargePermit" },
                { "reqadvertising", "RequiresAdvertising" },
                { "dayspending", "DaysPending" },
                { "projectmanager", "ProjectManagerName" },
                { "date", "Date" }
            };

            var searchText = SessionHandler.GetSessionVar<string>(SessionVariables.DashboardSearch);

            var filterWrapper = new DashboardFilterWrapper();

            filterWrapper.Filters = filterForm.BuildFilter().Concat(BuildSearchFilter(searchText));

            var spatialFilterForm = SessionHandler.GetSessionVar<DashboardSpatialFilterForm>(SessionVariables.DashboardSpatialFilter);
            if (spatialFilterForm != null)
            {
                filterWrapper.SpatialFilters = spatialFilterForm.BuildFilter();
            }

            var permitSearchResult = _dashboardPermitService.GetRange(command.Skip(), command.PageSize, command.BuildSort(propertyMap) ?? "PermitName", filterWrapper);

            object featureJson = null;
            if (permitSearchResult.Feature != null)
            {
                var parser = new EsriJsonWktParser(permitSearchResult.Feature.WellKnownText, permitSearchResult.Feature.CoordinateSystemId);
                featureJson = parser.Parse();
            }

            if (!permitSearchResult.PermitIds.Any())
            {
                return Json(new
                {
                    SpatialFilterFeature = featureJson,
                    GridModel = new GridData
                    {
                        Data = null,
                        PageTotal = 0,
                        CurrentPage = 0,
                        RecordCount = 0
                    }
                }, JsonRequestBehavior.AllowGet);
            }

            var count = permitSearchResult.PermitIds.Count();

            //Keep the permit ids in session -- will be used for export to excel functionality
            SessionHandler.SetSessionVar(SessionVariables.PermitIds, permitSearchResult.PermitIds);

            var pageTotal = (int)Math.Ceiling((float)count / (float)command.PageSize);

            return Json(new
            {
                PermitIds = permitSearchResult.PermitIds,
                SpatialFilterFeature = featureJson,
                GridModel = new GridData
                {
                    Data = permitSearchResult.DashboardPermits,
                    PageTotal = pageTotal,
                    CurrentPage = command.Page,
                    RecordCount = count
                }
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult Filter(DashboardFilterForm form)
        {
            if (form != null)
            {
                SessionHandler.SetSessionVar(SessionVariables.DashboardFilter, form);
            }

            return Json(new { IsSaveFilterResult = false, IsSuccess = true });
        }

        [HttpPost]
        public virtual JsonResult PublicFilter(DashboardFilterForm form)
        {
            if (form != null)
            {
                SessionHandler.SetSessionVar(SessionVariables.DashboardFilter, form);
            }

            return Json(new { IsSaveFilterResult = false, IsSuccess = true });
        }

        [HttpPost]
        public virtual JsonResult SpatialFilter(DashboardSpatialFilterForm form)
        {
            if (form != null)
            {
                SessionHandler.SetSessionVar(SessionVariables.DashboardSpatialFilter, form);
            }

            return Json(new { IsSuccess = true });
        }

        [HttpPost]
        public virtual JsonResult PublicSpatialFilter(DashboardSpatialFilterForm form)
        {
            if (form != null)
            {
                SessionHandler.SetSessionVar(SessionVariables.DashboardSpatialFilter, form);
            }

            return Json(new { IsSuccess = true });
        }

        [HttpPost]
        public virtual JsonResult Search(string search)
        {
            SessionHandler.SetSessionVar(SessionVariables.DashboardSearch, search);

            return Json(new { IsSuccess = true });
        }

        [HttpGet]
        public virtual PartialViewResult GetFilterControl(FilterTemplate filterTemplate)
        {
            SessionHandler.SetSessionVar(SessionVariables.SelectedDashboardFilterTemplate, filterTemplate);

            DashboardFilterForm form = new DashboardFilterForm(filterTemplate);

            form.Rehydrate();

            return PartialView(typeof(DashboardFilterForm).Name, form);
        }

        [HttpGet]
        public virtual JsonResult GetActivePermitsForContact()
        {
            try
            {
                var contactId = SessionHandler.GetSessionVar<int>(SessionVariables.AuthenticatedUserContactId);

                if (contactId == 0) return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);

                var permits = _permitContactService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("ContactId == " + contactId) }, "Permit").Where(p => p.Permit.PermitStatusId == 46 && p.IsPermittee).ToList();
                var permitListings = Mapper.Map<IEnumerable<PermitContact>, IEnumerable<PermitListingForm>>(permits);

                return Json(new { Permits = permitListings, IsSuccess = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public virtual JsonResult LinksModal(int permitId, string linkType)
        {
            switch (linkType)
            {
                case "renewalLink":
                    TempData["RefId"] = permitId;
                    return Json(new { success = true, url = Url.Action(MVC.Permitting.Permit.ApplicationWizard(null, 3)) }, JsonRequestBehavior.AllowGet);

                case "modificationLink":
                    TempData["RefId"] = permitId;
                    return Json(new { success = true, url = Url.Action(MVC.Permitting.Permit.ApplicationWizard(null, 2)) }, JsonRequestBehavior.AllowGet);

                case "complianceLink":
                    return Json(new { success = true, url = Url.Action(MVC.Permitting.Permit.Details(permitId)) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<int> GetFilteredPermitIds()
        {
            var nonSpatialFilteredPermitIds = SessionHandler.GetSessionVar<IEnumerable<int>>(SessionVariables.DashboardNonSpatialFilteredPermitIds);
            var spatialFilteredPermitIds = SessionHandler.GetSessionVar<IEnumerable<int>>(SessionVariables.DashboardSpatialFilteredPermitIds);

            IEnumerable<int> resultingPermitIds = null;

            if (nonSpatialFilteredPermitIds != null && nonSpatialFilteredPermitIds.Any())
            {
                if (spatialFilteredPermitIds != null && spatialFilteredPermitIds.Any())
                {
                    resultingPermitIds = nonSpatialFilteredPermitIds.Intersect(spatialFilteredPermitIds);
                }
                else
                {
                    resultingPermitIds = nonSpatialFilteredPermitIds;
                }
            }
            else
            {
                if (spatialFilteredPermitIds != null && spatialFilteredPermitIds.Any())
                {
                    resultingPermitIds = spatialFilteredPermitIds;
                }
            }

            return resultingPermitIds;
        }

        private IEnumerable<DynamicFilter> BuildSearchFilter(string searchText)
        {
            List<DynamicFilter> filters = new List<DynamicFilter>();

            if (!string.IsNullOrEmpty(searchText))
            {
                /*filters.Add(new DynamicFilter("PermitName.Contains(@0) || PermitContacts.Any(((ContactTypeId == 29 || IsPermittee) && (PermitContactInformation.FirstName.Contains(@0) || PermitContactInformation.LastName.Contains(@0) || PermitContactInformation.SecondaryName.Contains(@0) || PermitContactInformation.BusinessName.Contains(@0))) || ((ContactTypeId == 24) && (Contact.FirstName.Contains(@0) || Contact.LastName.Contains(@0) || Contact.SecondaryName.Contains(@0) || Contact.BusinessName.Contains(@0))))", searchText));*/
                filters.Add(new DynamicFilter("PermitName.Contains(@0) || PermitContacts.Any(((ContactTypeId == 29 || IsPermittee) && (PermitContactInformation.FirstName.Contains(@0) || PermitContactInformation.LastName.Contains(@0) || PermitContactInformation.SecondaryName.Contains(@0) || PermitContactInformation.BusinessName.Contains(@0))) || ((ContactTypeId == 24) && (Contact.FirstName.Contains(@0) || Contact.LastName.Contains(@0) || Contact.BusinessName.Contains(@0))))", searchText));
            }

            return filters;
        }

        [HttpPost]
        public virtual JsonResult BuildKmlFile(double[][][] geometry)
        {
            var output = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(output))
            {
                writer.WriteStartDocument();                                    //<?xml version="1.0" encoding="UTF-8"?>

                writer.WriteStartElement("kml", "http://www.opengis.net/kml/2.2"); //<kml xmlns="http://www.opengis.net/kml/2.2">                               

                writer.WriteStartElement("Document");                          //<Document>
                writer.WriteStartElement("name");
                writer.WriteString("WSIPSKml.kml");
                writer.WriteEndElement();                                      //  <name>TestTemplate.kml</name>

                writer.WriteStartElement("Style");
                writer.WriteStartAttribute("id");
                writer.WriteString("polyStyle");
                writer.WriteEndAttribute();                                    //  <Style id="polyStyle">

                writer.WriteStartElement("LineStyle");                         //  <LineStyle>
                writer.WriteStartElement("width");
                writer.WriteString("3");
                writer.WriteEndElement();
                writer.WriteStartElement("color");
                writer.WriteString("FF00FFFF");
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.WriteStartElement("PolyStyle");                         //    <PolyStyle>
                writer.WriteStartElement("color");
                writer.WriteString("00000000");
                writer.WriteEndElement();                                      //      <color>00000000</color>

                writer.WriteEndElement();                                      //    </PolyStyle>
                writer.WriteEndElement();                                      //  </Style>

                writer.WriteStartElement("Placemark");                         //  <Placemark>
                writer.WriteStartElement("name");
                writer.WriteString("WSIPS Parcel");
                writer.WriteEndElement();                                      //    <name>Test Template</name>

                writer.WriteStartElement("styleUrl");
                writer.WriteString("#polyStyle");
                writer.WriteEndElement();                                      //    <styleUrl>#polyStyle</styleUrl>

                writer.WriteStartElement("Polygon");                           //  <Polygon>
                writer.WriteStartElement("outerBoundaryIs");                   //    <outerBoundaryIs>
                writer.WriteStartElement("LinearRing");                        //      <LinearRing>
                writer.WriteStartElement("coordinates");                       //        <coordinates>

                foreach (double[][] ring in geometry)
                {
                    foreach (double[] pair in ring)
                    {
                        writer.WriteString(pair[0] + "," + pair[1] + ",1" + Environment.NewLine);
                    }
                }

                writer.WriteEndElement();                                      //        </coordinates>
                writer.WriteEndElement();                                      //      </LinearRing>
                writer.WriteEndElement();                                      //    </outerBoundaryIs>
                writer.WriteEndElement();                                      //  </Polygon>
                writer.WriteEndElement();                                      // </Placemark>

                writer.WriteEndElement();                                      //</Document>
                writer.WriteEndElement();                                      //</kml>

                writer.WriteEndDocument();
            }

            output.Seek(0, SeekOrigin.Begin);
            SessionHandler.SetSessionVar(SessionVariables.KmlFileMemoryStream, output);
            return Json(new { Url = Url.Action(MVC.Permitting.Dashboard.RetrieveKmlFile()) });
        }

        [HttpGet]
        public virtual FileResult RetrieveKmlFile()
        {
            var output = SessionHandler.GetSessionVar<MemoryStream>(SessionVariables.KmlFileMemoryStream);
            return File(output, "application/vnd.google-earth.kml+xml", "WSIPSKml.kml");
        }
    }
}