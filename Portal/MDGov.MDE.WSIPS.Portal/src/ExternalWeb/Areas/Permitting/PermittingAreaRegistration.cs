﻿using System.Web.Mvc;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Permitting
{
    public class PermittingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Permitting";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Permitting_Files",
                "Permitting/{controller}/{action}/{id}/{filename}");

            context.MapRoute(
                "Permitting_default",
                "Permitting/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
