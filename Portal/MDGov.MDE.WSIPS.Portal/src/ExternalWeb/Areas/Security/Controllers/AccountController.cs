﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Common;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Controllers
{
    [Authorize]
    public partial class AccountController : Controller
    {
        private readonly IMembershipAuthenticationService _membershipService;
        private readonly IUpdatableService<Contact> _contactService;
        private readonly IUpdatableService<ContactCommunicationMethod> _contactCommunicationMethodService;
        private readonly IService<LU_CommunicationMethod> _communicationMethodService;
        private readonly IService<LU_State> _stateService;

        public AccountController(
            IMembershipAuthenticationService membershipService,
            IUpdatableService<Contact> contactService,
            IUpdatableService<ContactCommunicationMethod> contactCommunicationMethodService,
            IService<LU_CommunicationMethod> communicationMethodService,
            IService<LU_State> stateService)
        {
            _membershipService = membershipService;
            _contactService = contactService;
            _contactCommunicationMethodService = contactCommunicationMethodService;
            _communicationMethodService = communicationMethodService;
            _stateService = stateService;
        }

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public virtual ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // GET: /Account/Confirm

        [AllowAnonymous]
        public virtual ActionResult ConfirmAccount(string confirmation)
        {
            _membershipService.Confirm(confirmation);
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && _membershipService.Login(model.UserName, model.Password))
            {
                var userId = _membershipService.GetUserIdByName(model.UserName);
                var contact = _contactService.GetRange(0, 1, null, new[] { new DynamicFilter("UserId == " + userId) }).FirstOrDefault();

                // No Contact Record, unlikely, but possible
                if (contact == null)
                {
                    ModelState.AddModelError(String.Empty, ErrorReasonToString(ErrorReason.Unknown));
                    return View(model);
                }

                // Check if Contact account is inactive
                bool active = contact.Active;
                if (!active)
                {
                    ModelState.AddModelError(String.Empty, ErrorReasonToString(ErrorReason.Inactive));
                    return View(model);
                }

                // Check if the user has 3 failed login attempts
                if (_membershipService.TotalPasswordFailures(model.UserName) > 2)
                {
                    ModelState.AddModelError(String.Empty, ErrorReasonToString(ErrorReason.Locked));
                    return View(model);
                }

                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                SessionHandler.ClearAllSessionVars();

                SessionHandler.SetSessionVar(SessionVariables.CurrentlyLoggedinUserId, userId);
                SessionHandler.SetSessionVar(SessionVariables.CurrentlyLoggedinUserFullName, (contact == null) ? "" : String.Format("{0} {1}", contact.FirstName, contact.LastName));
                SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserContactId, contact.Id);
                SessionHandler.SetSessionVar(SessionVariables.AuthenticatedUserContactTypeId, contact.ContactTypeId);

                return RedirectToLocal(returnUrl);
            }

            if (_membershipService.GetUserIdByName(model.UserName) == 0)
            {
                ModelState.AddModelError("", ErrorReasonToString(ErrorReason.InvalidCredentials));
                return View(model);
            }

            if (!_membershipService.IsConfirmed(model.UserName))
            {
                ModelState.AddModelError("", ErrorReasonToString(ErrorReason.NotConfirmed));
                return View(model);
            }

            // Invalid credentials
            ModelState.AddModelError("", ErrorReasonToString(ErrorReason.InvalidCredentials));
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            SessionHandler.ClearAllSessionVars();

            return RedirectToAction(MVC.Security.Account.Login());
        }

        //
        // GET: /Account/ForgotPasswordRequest

        [AllowAnonymous]
        public virtual ActionResult ForgotPasswordRequest()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPasswordRequest

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult ForgotPasswordRequest(string UserName)
        {
            if (ModelState.IsValid && (!String.IsNullOrWhiteSpace(UserName)))
            {
                if (_membershipService.GetUserIdByName(UserName) == 0)
                {
                    ModelState.AddModelError(String.Empty, "This account does not exist.");
                    return View();
                }
                if (!_membershipService.IsConfirmed(UserName))
                {
                    _membershipService.ResendConfirmationEmail(UserName, UserName);
                    ModelState.AddModelError(String.Empty, "This account was never confirmed. A confirmation email has been sent to your account.");
                    return View();
                }
                var success = _membershipService.SendPasswordResetEmail(UserName, Environment.UserName);
                if (!success)
                {
                    ModelState.AddModelError(String.Empty, "Email could not be sent. Please check email address.");
                    return View();
                }
                return RedirectToAction(MVC.Security.Account.ActionNames.PasswordResetInstructions, MVC.Security.Account.Name, new { area = MVC.Security.Name });
            }

            ModelState.AddModelError(String.Empty, "Email Address cannot be empty.");
            return View();
        }

        //
        // GET: /Account/PasswordChangeInstructions

        [AllowAnonymous]
        public virtual ActionResult PasswordChangeInstructions()
        {
            return View();
        }

        //
        // GET: /Account/PasswordResetInstructions

        [AllowAnonymous]
        public virtual ActionResult PasswordResetInstructions()
        {
            return View();
        }

        //
        // GET: /Account/PasswordChanged

        [AllowAnonymous]
        public virtual ActionResult PasswordChanged()
        {
            return View();
        }

        //
        // GET: /Account/ChangePassword

        [AllowAnonymous]
        public virtual ActionResult ChangePassword()
        {
            var login = new ResetPasswordModel();
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult ChangePassword(ResetPasswordModel login)
        {
            if (ModelState.IsValid)
            {
                if (!_membershipService.ChangePassword(login.UserName, login.OldPassword, login.NewPassword))
                {
                    ModelState.AddModelError(String.Empty, "The password could not be changed.");

                    return View(login);
                }

                return RedirectToAction(MVC.Security.Account.ActionNames.PasswordChanged, MVC.Security.Account.Name, new { area = MVC.Security.Name });
            }

            return View(login);
        }

        //
        // GET: /Account/ResetPassword

        [AllowAnonymous]
        public virtual ActionResult ResetPassword(string token)
        {
            var login = new ResetPasswordModel { ResetToken = token, OldPassword = "password" };
            return View(login);
        }

        //
        // POST: /Account/ChangePassword

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult ResetPassword(ResetPasswordModel login)
        {
            if (ModelState.IsValid)
            {
                if (!_membershipService.ResetPassword(login.UserName, login.ResetToken, login.NewPassword))
                {
                    ModelState.AddModelError(String.Empty, "The password could not be changed.");

                    return View(login);
                }

                return RedirectToAction(MVC.Security.Account.ActionNames.PasswordChanged, MVC.Security.Account.Name, new { area = MVC.Security.Name });
            }

            return View(login);
        }

        [AllowAnonymous]
        public virtual ActionResult CheckEmailToConfirm()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public virtual JsonResult ValidateContact(int contactId)
        {
            Contact contact = null;

            if (contactId > 0)
            {
                contact = _contactService.GetById(contactId);
                if (contact != null) return Json(new { isValid = contact.Active }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { isValid = false}, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public virtual ActionResult Register(int contactId)
        {
            var contact = new Contact();

            if (contactId > 0)
            {
                contact = _contactService.GetById(contactId);
            }

            var form = Mapper.Map<Contact, RegisterModel>(contact);

            form.ExistingContact = contactId > 0;

            return View(form);
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Register(RegisterModel form)
        {
            ActionResult returnAction;
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    // Check for possible duplicates
                    var existingUserId = _membershipService.GetUserIdByName(form.EmailAddress);
                    if (existingUserId > 0)
                    {
                        ModelState.AddModelError(String.Empty, "A user with this email address already exists.");
                        form.StateList = _stateService.GetRange(0, int.MaxValue, "Sequence", new[] { new DynamicFilter("Active == @0", true) }).Select(x =>
                            new SelectListItem
                            {
                                Selected = x.Id == form.StateId,
                                Text = x.Description,
                                Value = x.Id.ToString()
                            });

                        return View(form);
                    }

                    // Create membership Record
                    _membershipService.CreateAccountWithPassword(form.EmailAddress, form.EmailAddress, form.Password, "Public", ViewBag.UserAccount);
                    form.UserId = _membershipService.GetUserIdByName(form.EmailAddress);
                    _membershipService.UpdateUserInfo(form.UserId, null, form.LastName);

                    // Create User Permissions
                    _membershipService.AssignRoles(form.EmailAddress, new String[] { "Applicant / Permittee" });


                    // Create public user record
                    var pubContact = Mapper.Map<RegisterModel, Contact>(form);
                    pubContact.ApplicationNotification = true;
                    pubContact.EmailNotification = true;
                    pubContact.Active = true;
                    pubContact.ContactTypeId = 2;

                    if (!String.IsNullOrWhiteSpace(pubContact.BusinessName)) pubContact.IsBusiness = true;
                    
                    var id = 0;
                    
                    if (form.ExistingContact)
                    {
                        id = form.Id;
                        _contactService.Save(pubContact);
                    }
                    else
                    {
                        id = _contactService.Save(pubContact);

                    }

                    DeleteExistingCommunicationMethods(id);
                    CreateCommunicationMethods(id, form.TelephoneNumber, form.EmailAddress);

                    returnAction = RedirectToAction(MVC.Security.Account.CheckEmailToConfirm());
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError(String.Empty, ErrorCodeToString(e.StatusCode));
                    returnAction = View(form);
                }
            }
            else
            {
                returnAction = View(form);
            }

            // If we got this far, something failed, redisplay form
            form.StateList = _stateService.GetRange(0, int.MaxValue, "Sequence", new[] { new DynamicFilter("Active == @0", true) }).Select(x =>
                new SelectListItem
                {
                    Selected = x.Id == form.StateId,
                    Text = x.Description,
                    Value = x.Id.ToString()
                });

            return returnAction;
        }

        [HttpGet]
        public virtual ActionResult EditAccount()
        {
            var currentUserId = SessionHandler.GetSessionVar<int>(SessionVariables.CurrentlyLoggedinUserId);
            var contact = _contactService.GetRange(0, 1, null, new DynamicFilter[] { new DynamicFilter("UserId == " + currentUserId) }, "ContactCommunicationMethods").FirstOrDefault();

            return View(Mapper.Map<Contact, RegisterModel>(contact));
        }

        [HttpPost]
        public virtual JsonResult EditAccount(RegisterModel contact)
        {
            try
            {
                var currentUserId = SessionHandler.GetSessionVar<int>(SessionVariables.CurrentlyLoggedinUserId);

                // Check for possible duplicates
                var existingUserId = _membershipService.GetUserIdByName(contact.EmailAddress);
                if (existingUserId > 0 && existingUserId != currentUserId)
                {
                    return Json(new { success = false, message = "A user with this email address already exists." });
                }

                var userContact = _contactService.GetRange(0, 1, null, new[] { new DynamicFilter("UserId == " + currentUserId) }).SingleOrDefault();

                userContact.FirstName = contact.FirstName;
                userContact.LastName = contact.LastName;
                userContact.BusinessName = contact.BusinessName;
                userContact.Address1 = contact.Address1;
                userContact.Address2 = contact.Address2;
                userContact.City = contact.City;
                userContact.StateId = contact.StateId;
                userContact.ZipCode = contact.ZipCode;
                userContact.MiddleInitial = contact.MiddleInitial;
                /*userContact.SecondaryName = contact.SecondaryName;*/

                UpdateCommunicationMethods(contact.ContactId, contact.TelephoneNumber, contact.EmailAddress);

                int saveContact = _contactService.Save(userContact);

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        private void CreateCommunicationMethods(int contactId, string dayPhone, string email)
        {
            if (contactId == 0)
            {
                throw new ArgumentException("Invalid Value", "contactId");
            }

            if (!String.IsNullOrEmpty(email))
            {
                var method = _communicationMethodService.GetRange(0, 1, null, new[] { new DynamicFilter("Description == \"Email\"") }).FirstOrDefault();
                if (method != null)
                {
                    var emailMethod = new ContactCommunicationMethod
                    {
                        CommunicationMethodId = method.Id,
                        ContactId = contactId,
                        IsPrimaryEmail = true,
                        CommunicationValue = email
                    };

                    _contactCommunicationMethodService.Save(emailMethod);
                }
            }

            if (!String.IsNullOrEmpty(dayPhone))
            {
                var method = _communicationMethodService.GetRange(0, 1, null, new[] { new DynamicFilter("Description == \"Mobile\"") }).FirstOrDefault();
                if (method != null)
                {
                    var phone = new ContactCommunicationMethod
                    {
                        CommunicationMethodId = method.Id,
                        ContactId = contactId,
                        IsPrimaryPhone = true,
                        CommunicationValue = dayPhone
                    };

                    _contactCommunicationMethodService.Save(phone);
                }
            }
        }

        private void UpdateCommunicationMethods(int contactId, string dayPhone, string email)
        {
            var methods = _contactCommunicationMethodService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("ContactId == " + contactId) });

            if (!String.IsNullOrEmpty(dayPhone))
            {
                var phoneMethod = methods.SingleOrDefault(m => m.IsPrimaryPhone.GetValueOrDefault());
                if (phoneMethod != null)
                {
                    phoneMethod.CommunicationValue = dayPhone;
                    _contactCommunicationMethodService.Save(phoneMethod);
                }
            }

            if (!String.IsNullOrEmpty(email))
            {
                var emailContact = methods.SingleOrDefault(m => m.IsPrimaryEmail.GetValueOrDefault());
                if (emailContact.CommunicationValue != email)
                {
                    // Update user account 
                    var userID = SessionHandler.GetSessionVar<int>(SessionVariables.CurrentlyLoggedinUserId);
                    if (_membershipService.UpdateUserInfo(userID, email, ""))
                    {
                        FormsAuthentication.SetAuthCookie(email, false); // Default to more secure in-memory cookie
                    }

                    emailContact.CommunicationValue = email;
                    _contactCommunicationMethodService.Save(emailContact);
                }
            }
        }

        private void DeleteExistingCommunicationMethods(int contactId)
        {
            var contactCommunicationMethods = _contactCommunicationMethodService.GetRange(0, int.MaxValue, null, new[] { new DynamicFilter("ContactId == @0", contactId) });

            if (contactCommunicationMethods != null)
            {
                foreach (var method in contactCommunicationMethods)
                {
                    _contactCommunicationMethodService.Delete(method.Id);
                }
            }
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
                //return RedirectToAction("Index", "Home", new { area = "" });
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        public enum ErrorReason
        {
            InvalidCredentials,
            Locked,
            NotConfirmed,
            Inactive,
            Unknown
        }

        private string ErrorReasonToString(ErrorReason reason)
        {
            switch (reason)
            {
                case ErrorReason.InvalidCredentials:
                    return "The username or password submitted is incorrect.";
                case ErrorReason.NotConfirmed:
                    return "This account has not yet been approved. Please click on the link in the registration confirmation email.";
                case ErrorReason.Inactive:
                    return "This account has been disabled. Please contact the Water Supply Program at 410-537-3714.";
                case ErrorReason.Locked:
                    return "This user account has been locked. Please contact the Water Supply Program at 410-537-3714.";
                default:
                    return "Unknown Error. Please contact the Water Supply Program at 410-537-3714.";
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
