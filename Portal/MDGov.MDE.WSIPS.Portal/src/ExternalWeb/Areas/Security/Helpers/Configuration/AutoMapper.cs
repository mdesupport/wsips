﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.TypeConverters;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.MapResolvers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Models;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Helpers.Configuration.MapResolvers;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Configuration
{
    public class SecurityAreaMapperProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "Security";
            }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Permit, DashboardPermit>();
            Mapper.CreateMap<IEnumerable<ILookup>, SelectList>().ConvertUsing<LookupSelectListTypeConverter>();
            Mapper.CreateMap<IEnumerable<LU_WaterWithdrawalType>, AttributedSelectList>().ConvertUsing<WaterWithdrawalTypeTypeConverter>();

            Mapper.CreateMap<Contact, RegisterModel>()
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.Id))
                //.ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.Address1))
                .ForMember(dest => dest.EmailAddress, opt => opt.ResolveUsing<CommunicationMethodEmailResolver>())
                .ForMember(dest => dest.TelephoneNumber, opt => opt.ResolveUsing<CommunicationMethodPhoneResolver>())
                .ForMember(dest => dest.StateList, opt => opt.ResolveUsing<StateDropdownListResolver<Contact>>());


            Mapper.CreateMap<RegisterModel, Contact>()
                //.ForMember(dest => dest.Address1, opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.ContactTypeId, opt => opt.UseValue(2)); // Applicant
        }
    }
}