﻿using System.Collections.Generic;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service.Interface
{
    public interface IMembershipAuthenticationService
    {
        /// <summary>
        /// Validates user using membership service
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <param name="password">Password (string)</param>
        /// <returns>Boolean</returns>
        bool Login(string userName, string password);

        /// <summary>
        /// Clears session and auth cookies
        /// </summary>
        void Logout();

        /// <summary>
        /// Gets membership API user id based on the username
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <returns>Id (integer)</returns>
        int GetUserIdByName(string userName);

        /// <summary>
        /// Confirms user account
        /// </summary>
        /// <param name="confirmationToken">Membership API confirmation token (string)</param>
        /// <returns></returns>
        bool Confirm(string confirmationToken);

        /// <summary>
        /// Creates/registers a user
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <param name="password">Password (string)</param>
        /// <returns>Membership API confirmation token</returns>
        string Create(string userName, string password);

        /// <summary>
        /// Changes user password
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <param name="currentPassword">Old Password (string)</param>
        /// <param name="newPassword">New Password (string)</param>
        /// <returns>Boolean</returns>
        bool ChangePassword(string userName, string currentPassword, string newPassword);

        /// <summary>
        /// Resets user's forgotten password
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <param name="resetToken">API generated reset token (string)</param>
        /// <param name="newPassword">New Password (string)</param>
        /// <returns>Boolean</returns>
        bool ResetPassword(string userName, string resetToken, string newPassword);

        /// <summary>
        /// Gets a list of users from Membership API database
        /// </summary>
        /// <param name="ids">Arrays of ids (Integers)</param>
        /// <param name="name">UserName (string)</param>
        /// <param name="typeId">User Type Id (integer)</param>
        /// <param name="role">Role (String)</param>
        /// <returns>Collection of UserListing models</returns>
        IEnumerable<UserListing> GetRange(int[] ids, string name, int typeId, string role);

        /// <summary>
        /// Gets user listing model by User Id
        /// </summary>
        /// <param name="id">User Id (integer)</param>
        /// <returns>UserListing instance</returns>
        UserListing GetUserById(int id);

        /// <summary>
        /// Checks if a user with a given username is confirmed
        /// </summary>
        /// <param name="username">UserName (string)</param>
        /// <returns>Boolean</returns>
        bool IsConfirmed(string username);

        /// <summary>
        /// Gets the number of password failures for a given user since the last successful login
        /// </summary>
        /// <param name="username">UserName (string)</param>
        /// <returns>Integer</returns>
        int TotalPasswordFailures(string username);

        /// <summary>
        /// Sends an email to the email specified with the link to change user's password
        /// </summary>
        /// <param name="username">User name (email address)</param>
        /// <param name="principalUser">User who modified the record</param>
        /// <returns>Boolean</returns>
        bool SendPasswordChangeEmail(string username, string principalUser);

        bool ResendConfirmationEmail(string username, string principalUser);

        /// <summary>
        /// Sends an email to the email specified with the link to reset user's password
        /// </summary>
        /// <param name="username">User name (email address)</param>
        /// <param name="principalUser">User who modified the record</param>
        /// <returns>Boolean</returns>
        bool SendPasswordResetEmail(string username, string principalUser);

        /// <summary>
        /// Determines whether a user's password is system generated or changed by a user
        /// </summary>
        /// <param name="userName">User name (email address)</param>
        /// <returns>Boolean</returns>
        bool IsPasswordSystemGenerated(string userName);

        /// <summary>
        /// Creates a new user account with system generated password
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <param name="email">Email address (string)</param>
        /// <param name="userType">User type (string)</param>
        /// <param name="principalUser">Principal (Logged in) user (string)</param>
        /// <returns>string</returns>
        string CreateAccount(string userName, string email, string userType, string principalUser);

        /// <summary>
        /// Creates a new user account with user provided password
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <param name="email">Email address (string)</param>
        /// <param name="userType">User type (string)</param>
        /// <param name="principalUser">Principal (Logged in) user (string)</param>
        /// <returns>string</returns>
        string CreateAccountWithPassword(string userName, string email, string password , string userType, string principalUser);

        /// <summary>
        /// Updates user name and last name (the only information shared between contacts and members)
        /// </summary>
        /// <param name="id">UserId (integer)</param>
        /// <param name="newUserName">New user name (email)</param>
        /// <param name="newLastName">New last name</param>
        /// <returns>Boolean</returns>
        bool UpdateUserInfo(int id, string newUserName, string newLastName);

        /// <summary>
        /// Assigns roles to the user
        /// </summary>
        /// <param name="userName">Username (string)</param>
        /// <param name="roles">Collection of roles (strings)</param>
        /// <returns>Boolean</returns>
        bool AssignRoles(string userName, string[] roles);

        /// <summary>
        /// Checks if a user belongs in the specified list of roles
        /// </summary>
        /// <param name="userName">User Name (String)</param>
        /// <param name="roles">Roles (List of Strings)</param>
        /// <returns>Boolean</returns>
        bool IsInRole(string userName, string[] roles);
    }
}