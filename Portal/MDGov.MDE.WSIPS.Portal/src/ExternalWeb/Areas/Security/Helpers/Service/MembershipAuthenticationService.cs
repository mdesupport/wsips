﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using MDGov.MDE.Common.Helpers.Common;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service.Interface;
using MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models;
using StructureMap;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Helpers.Service
{
    public class MembershipAuthenticationService : HttpClientBase, IMembershipAuthenticationService
    {
        public MembershipAuthenticationService()
            : base(ConfigurationManager.AppSettings["MembershipUri"])
        { }

        public string CreateAccount(string userName, string email, string userType, string principalUser)
        {
            var approved = (userType == "Public");
            var response = Client.PostAsJsonAsync("CreateAccount", new { UserName = userName, UserType = userType, Email = email, Approved = true, PrincipalUser = userName }).Result;

            if (response.IsSuccessStatusCode)
            {
                var token = response.Content.ReadAsAsync<string>().Result;
                return token;
            }
            var exception = response.Content.ReadAsAsync<ApiException>().Result;
            HandleApiException(exception, response.StatusCode);

            return string.Empty;
        }

        public string CreateAccountWithPassword(string userName, string email, string password, string userType, string principalUser)
        {
            var response = Client.PostAsJsonAsync("CreateAccountWithPassword", new { UserName = userName, UserType = userType, Password = password, Email = email, Approved = false, PrincipalUser = userName }).Result;

            if (response.IsSuccessStatusCode)
            {
                var token = response.Content.ReadAsAsync<string>().Result;
                return token;
            }
            var exception = response.Content.ReadAsAsync<ApiException>().Result;
            HandleApiException(exception, response.StatusCode);

            return string.Empty;
        }

        public bool UpdateUserInfo(int id, string newUserName, string newLastName)
        {
            var response = Client.PostAsJsonAsync("UpdateUserInfo", new { UserId = id, Username = newUserName, LastName = newLastName }).Result;

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            var exception = response.Content.ReadAsAsync<ApiException>().Result;
            HandleApiException(exception, response.StatusCode);

            return false;
        }


        public bool AssignRoles(string userName, string[] roles)
        {
            var response = Client.PostAsJsonAsync("AssignRoles", new { UserName = userName, Roles = roles }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            var exception = response.Content.ReadAsAsync<ApiException>().Result;
            HandleApiException(exception, response.StatusCode);

            return false;
        }

        public bool Login(string userName, string password)
        {
            var response = Client.PostAsJsonAsync("Validate", new { UserName = userName, Password = password }).Result;
            //var response = Client.PostAsync("Validate?userName=" + userName + "&password=" + password, null).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public int GetUserIdByName(string userName)
        {

            var response = Client.GetAsync(String.Format("GetUserIdByName?userName={0}", userName)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<int>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return 0;
            }
        }

        public bool IsPasswordSystemGenerated(string userName)
        {

            var response = Client.GetAsync(String.Format("HasSystemGeneratedPassword?userName={0}", userName)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        public string Create(string userName, string password)
        {
            throw new NotImplementedException();
        }

        public bool ChangePassword(string userName, string currentPassword, string newPassword)
        {
            var response = Client.PostAsJsonAsync("ChangePassword", new { UserName = userName, OldPassword = currentPassword, NewPassword = newPassword }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public bool ResetPassword(string userName, string resetToken, string newPassword)
        {
            var response = Client.PostAsJsonAsync("ResetPassword", new { UserName = userName, ResetToken = resetToken, NewPassword = newPassword }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public IEnumerable<UserListing> GetRange(int[] ids, string name, int typeId, string role)
        {
            var response = Client.PostAsJsonAsync("GetUserRange", new { Ids = ids, Name = name, TypeId = typeId, Role = role }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<IEnumerable<UserListing>>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return new List<UserListing>();
            }
        }

        public UserListing GetUserById(int id)
        {
            var response = Client.GetAsync(String.Format("GetUserById/{0}", id)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<UserListing>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return null;
            }
        }

        public bool Confirm(string confirmationToken)
        {
            var response = Client.GetAsync(String.Format("ConfirmAccount?token={0}", confirmationToken)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public bool IsConfirmed(string username)
        {
            var response = Client.GetAsync(String.Format("IsConfirmed?userName={0}", username)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public int TotalPasswordFailures(string username)
        {
            var response = Client.GetAsync(String.Format("PasswordFailures?userName={0}", username)).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<int>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return 0;
            }
        }

        public bool SendPasswordChangeEmail(string username, string principalUser)
        {
            var response = Client.PostAsJsonAsync("ChangePasswordEmail", new { UserName = username, PrincipalUser = principalUser }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }


        public bool SendPasswordResetEmail(string username, string principalUser)
        {
            var response = Client.PostAsJsonAsync("ResetPasswordEmail", new { UserName = username, PrincipalUser = principalUser }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public bool ResendConfirmationEmail(string username, string principalUser)
        {
            var response = Client.PostAsJsonAsync("ReSendConfirmationEmail", new { UserName = username, PrincipalUser = principalUser }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsAsync<ApiException>().Result;
                HandleApiException(exception, response.StatusCode);

                return false;
            }
        }

        public bool IsInRole(string userName, string[] roles)
        {
            var response = Client.PostAsJsonAsync("IsInRole", new { UserName = userName, Roles = roles }).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<bool>().Result;
            }
            var exception = response.Content.ReadAsAsync<ApiException>().Result;
            HandleApiException(exception, response.StatusCode);

            return false;
        }

        private static void HandleApiException(ApiException exception, HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.NotFound:
                    exception.ExceptionType = "NotFound";
                    exception.ExceptionMessage = exception.Message;
                    break;
                case HttpStatusCode.MethodNotAllowed:
                    exception.ExceptionType = "MethodNotAllowed";
                    exception.ExceptionMessage = exception.Message;
                    break;
            }
            var logger = ObjectFactory.GetInstance<IErrorLogging>();
            logger.Log(exception);
        }
    }
}