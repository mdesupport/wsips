﻿using System.Collections.Generic;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models
{
    /// <summary>
    /// User Listing View model for membership users. Used for populating the grid.
    /// </summary>
    public class UserListing
    {
        /// <summary>
        /// User Id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// User type id
        /// </summary>
        public int UserTypeId { get; set; }

        /// <summary>
        /// User Type (Public, Internal, etc.)
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// Collection of Strings
        /// </summary>
        public virtual IEnumerable<string> Roles { get; set; }
    }
}