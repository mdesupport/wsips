﻿namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models
{
    /// <summary>
    /// User Type View Model for membership user types. Used for populating drop-down lists 
    /// </summary>
    public class UserTypeListing
    {
        /// <summary>
        /// User Type Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// User Type Name
        /// </summary>
        public string Description { get; set; }
    }
}