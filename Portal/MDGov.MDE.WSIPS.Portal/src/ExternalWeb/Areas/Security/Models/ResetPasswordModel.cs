﻿using MDGov.MDE.Common.Helpers.RegexPatterns;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models
{
    public class ResetPasswordModel
    {
        [DisplayName("Email Address")]
        [StringLength(50)]
        [Required(ErrorMessage = "Email Address is required")]
        [RegularExpression(Patterns.EMAIL_ADDRESS, ErrorMessage = "Enter a valid email address.")]
        public string UserName { get; set; }

        [DisplayName("Old Password")]
        [Required(ErrorMessage = "Old Password is required")]
        public string OldPassword { get; set; }

        [DisplayName("New Password")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "New password is required")]
        [RegularExpression(@".*[^a-zA-Z]+.*", ErrorMessage = "{0} must contain at least one non-alphabetic character.")]
        public string NewPassword { get; set; }

        [DisplayName("Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        [Required(ErrorMessage = "Confirmed new password is required")]
        public string ConfirmPassword { get; set; }

        public string ResetToken { get; set; }
    }
}