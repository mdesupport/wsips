﻿using System.Collections.Generic;
using System.Web.Mvc;
using MDGov.MDE.Common.Helpers.RegexPatterns;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MDGov.MDE.WSIPS.Portal.ExternalWeb.Areas.Security.Models
{
    public class RegisterModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int ContactId { get; set; }

        [DisplayName("Email Address")]
        [StringLength(50)]
        [Required(ErrorMessage = "Email Address is required")]
        [RegularExpression(Patterns.EMAIL_ADDRESS, ErrorMessage = "Enter a valid email address.")]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        [RegularExpression(@".*[^a-zA-Z]+.*", ErrorMessage = "{0} must contain at least one non-alphabetic character.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [DisplayName("First Name")]
        [StringLength(40)]
        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [StringLength(40)]
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        [DisplayName("Telephone No")]
        [StringLength(50)]
        [Required(ErrorMessage = "Telephone No Phone is required")]
        public string TelephoneNumber { get; set; }

        [DisplayName("Business Name")]
        [StringLength(100)]
        public string BusinessName { get; set; }

        [DisplayName("City")]
        [StringLength(50)]
        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [DisplayName("State")]
        [Required(ErrorMessage = "State is required")]
        public int? StateId { get; set; }

        public IEnumerable<SelectListItem> StateList { get; set; }

        [DisplayName("Zip Code")]
        [StringLength(10)]
        [Required(ErrorMessage = "Zip Code is required")]
        public string ZipCode { get; set; }

        /*[DisplayName("Secondary Name")]
        [StringLength(100)]
        public string SecondaryName { get; set; }*/

        [DisplayName("Address (Line 1)")]
        [StringLength(100)]
        [Required(ErrorMessage = "Address Line1 is required")]
        public string Address1 { get; set; }

        [DisplayName("Address (Line 2)")]
        [StringLength(100)]
        public string Address2 { get; set; }

        [DisplayName("Middle Name or Initial")]
        [StringLength(40)]
        public string MiddleInitial { get; set; }

        public bool ExistingContact { get; set; }
    }
}
