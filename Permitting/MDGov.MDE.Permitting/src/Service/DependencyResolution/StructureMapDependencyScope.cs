﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.DependencyResolution.StructureMapDependencyScope
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using Microsoft.Practices.ServiceLocation;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;

namespace MDGov.MDE.Permitting.Service.DependencyResolution
{
  public class StructureMapDependencyScope : ServiceLocatorImplBase, IDependencyScope, IDisposable
  {
    protected readonly IContainer Container;

    public StructureMapDependencyScope(IContainer container)
    {
      if (container == null)
        throw new ArgumentNullException("container");
      this.Container = container;
    }

    public void Dispose()
    {
      this.Container.Dispose();
    }

    public new object GetService(Type serviceType)
    {
      if (serviceType == (Type) null)
        return (object) null;
      try
      {
        return serviceType.IsAbstract || serviceType.IsInterface ? this.Container.TryGetInstance(serviceType) : this.Container.GetInstance(serviceType);
      }
      catch
      {
        return (object) null;
      }
    }

    public IEnumerable<object> GetServices(Type serviceType)
    {
      return this.Container.GetAllInstances(serviceType).Cast<object>();
    }

    protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
    {
      return this.Container.GetAllInstances(serviceType).Cast<object>();
    }

    protected override object DoGetInstance(Type serviceType, string key)
    {
      if (!string.IsNullOrEmpty(key))
        return this.Container.GetInstance(serviceType, key);
      if (!serviceType.IsAbstract && !serviceType.IsInterface)
        return this.Container.GetInstance(serviceType);
      return this.Container.TryGetInstance(serviceType);
    }
  }
}
