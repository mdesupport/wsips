﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.DependencyResolution.ServiceLayerRegistry
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Logging;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.DataLayer.Interface;
using MDGov.MDE.Permitting.DataLayer.Repository;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.Permitting.Service.Controllers;
using StructureMap;
using StructureMap.Configuration.DSL;
using System;
using System.Data.Entity;

namespace MDGov.MDE.Permitting.Service.DependencyResolution
{
  public class ServiceLayerRegistry : Registry
  {
    public ServiceLayerRegistry()
    {
      this.For(typeof (IRepository<>)).Use(typeof (MDGov.MDE.Common.DataLayer.Repository<>));
      this.For<DbContext>().Use<ModelContext>();
      this.For<IRepository<ErrorLog>>().Use((Func<IContext, IRepository<ErrorLog>>) (ctx => (IRepository<ErrorLog>) new MDGov.MDE.Common.DataLayer.Repository<ErrorLog>(ctx.GetInstance<LoggingContext>())));
      this.For<IDashboardPermittingRepository>().Use<DashboardPermittingRepository>();
      this.For<IPermitRepository>().Use<PermitRepository>();
      this.For(typeof (IService<>)).Use(typeof (ServiceBase<>));
      this.For(typeof (IUpdatableService<>)).Use(typeof (UpdatableServiceBase<>));
      this.For<IUpdatableService<ConditionCompliance>>().Use<ConditionComplianceController>();
      this.For<IErrorLogging>().Use<ErrorLogging>();
    }
  }
}
