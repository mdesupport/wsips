﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.DependencyResolution.IoC
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Permitting.BusinessLayer.DependencyResolution;
using StructureMap;
using StructureMap.Graph;
using System;

namespace MDGov.MDE.Permitting.Service.DependencyResolution
{
  public static class IoC
  {
    public static IContainer Initialize()
    {
      ObjectFactory.Initialize((Action<IInitializationExpression>) (x =>
      {
        x.Scan((Action<IAssemblyScanner>) (scan =>
        {
          scan.TheCallingAssembly();
          scan.WithDefaultConventions();
        }));
        x.AddRegistry<BusinessLayerRegistry>();
        x.AddRegistry<ServiceLayerRegistry>();
      }));
      return ObjectFactory.Container;
    }
  }
}
