﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.DependencyResolution.StructureMapDependencyResolver
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using StructureMap;
using System;
using System.Web.Http.Dependencies;

namespace MDGov.MDE.Permitting.Service.DependencyResolution
{
  public class StructureMapDependencyResolver : StructureMapDependencyScope, IDependencyResolver, IDependencyScope, IDisposable
  {
    public StructureMapDependencyResolver(IContainer container)
      : base(container)
    {
    }

    public IDependencyScope BeginScope()
    {
      return (IDependencyScope) new StructureMapDependencyResolver(this.Container.GetNestedContainer());
    }
  }
}
