﻿using MDGov.MDE.Permitting.Service.App_Start;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using WebActivator;

[assembly: AssemblyCopyright("Copyright © JMT 2012")]
[assembly: ComVisible(false)]
[assembly: AssemblyProduct("Service")]
[assembly: AssemblyTrademark("")]
[assembly: Guid("1c8ecc1c-08c5-43b6-8650-c0e04ccf8d00")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyCompany("JMT")]
[assembly: PreApplicationStartMethod(typeof (StructuremapMvc), "Start")]
[assembly: AssemblyTitle("Service")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyVersion("1.0.0.0")]
