﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.AutoMapperConfig
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using AutoMapper;
using MDGov.MDE.Permitting.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Permitting.Service
{
  public class AutoMapperConfig
  {
    public static void Configure()
    {
      Mapper.CreateMap<sp_GetWaterWithdrawalLocation_Result, PermitWaterWithdrawalLocation>().ForMember((Expression<Func<PermitWaterWithdrawalLocation, object>>) (dest => dest.SelectedPermitWaterWithdrawalPurposes), (Action<IMemberConfigurationExpression<sp_GetWaterWithdrawalLocation_Result>>) (opt => opt.MapFrom<string[]>((Expression<Func<sp_GetWaterWithdrawalLocation_Result, string[]>>) (src => src.AssociatedPermitWaterWithdrawalPurposes.Split(new char[]
      {
        ','
      }))))).ForMember((Expression<Func<PermitWaterWithdrawalLocation, object>>) (dest => (object) dest.WithdrawalSourceId), (Action<IMemberConfigurationExpression<sp_GetWaterWithdrawalLocation_Result>>) (opt => opt.MapFrom<int?>((Expression<Func<sp_GetWaterWithdrawalLocation_Result, int?>>) (src => src.WaterWithdrawalSourceId)))).ForMember((Expression<Func<PermitWaterWithdrawalLocation, object>>) (dest => dest.IntakeLocation), (Action<IMemberConfigurationExpression<sp_GetWaterWithdrawalLocation_Result>>) (opt => opt.MapFrom<string>((Expression<Func<sp_GetWaterWithdrawalLocation_Result, string>>) (src => src.ExactLocationOfIntake))));
    }
  }
}
