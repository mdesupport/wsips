﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.App_Start.StructuremapMvc
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Permitting.Service.DependencyResolution;
using StructureMap;
using System.Web.Http;
using System.Web.Mvc;

namespace MDGov.MDE.Permitting.Service.App_Start
{
  public static class StructuremapMvc
  {
    public static void Start()
    {
      IContainer container = IoC.Initialize();
      DependencyResolver.SetResolver((object) new StructureMapDependencyResolver(container));
      GlobalConfiguration.Configuration.DependencyResolver = (System.Web.Http.Dependencies.IDependencyResolver) new StructureMapDependencyResolver(container);
    }
  }
}
