﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.WebApiConfig
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using System.Web.Http;

namespace MDGov.MDE.Permitting.Service
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{action}/{id}", (object) new
      {
        id = RouteParameter.Optional
      });
    }
  }
}
