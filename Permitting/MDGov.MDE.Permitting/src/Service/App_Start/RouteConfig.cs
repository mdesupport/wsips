﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.RouteConfig
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using System.Web.Mvc;
using System.Web.Routing;

namespace MDGov.MDE.Permitting.Service
{
  public class RouteConfig
  {
    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
      routes.MapRoute("Default", "{controller}/{id}", (object) new
      {
        controller = "Home",
        action = "Index",
        id = UrlParameter.Optional
      });
    }
  }
}
