﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.FilterConfig
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.WSIPS.Permitting.Service.Helpers.Filters;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace MDGov.MDE.Permitting.Service
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add((object) new HandleErrorAttribute());
    }

    public static void RegisterHttpFilters(HttpFilterCollection filters)
    {
      filters.Add((IFilter) new HandleExceptionAttribute());
    }
  }
}
