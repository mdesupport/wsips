﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwGolfCourseIrrigationController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwGolfCourseIrrigationController : ApiController, IUpdatableService<PermitWwGolfCourseIrrigation>, IService<PermitWwGolfCourseIrrigation>
  {
    private readonly IRepository<PermitWwGolfCourseIrrigation> _permitWwGolfCourseIrrigationRepository;

    public PermitWwGolfCourseIrrigationController(IRepository<PermitWwGolfCourseIrrigation> permitWwGolfCourseIrrigationRepository)
    {
      this._permitWwGolfCourseIrrigationRepository = permitWwGolfCourseIrrigationRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwGolfCourseIrrigation> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwGolfCourseIrrigation>) this._permitWwGolfCourseIrrigationRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwGolfCourseIrrigation> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwGolfCourseIrrigation>) this._permitWwGolfCourseIrrigationRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwGolfCourseIrrigation entity)
    {
      return this._permitWwGolfCourseIrrigationRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwGolfCourseIrrigationRepository.Delete(Convert.ToInt32(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwGolfCourseIrrigation GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
