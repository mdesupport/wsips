﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Communication.Model;
using MDGov.MDE.Payment.Model;
using MDGov.MDE.Permitting.DataLayer.Interface;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.Permitting.Service.Helpers;
using MDGov.MDE.Permitting.Service.Helpers.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http.Formatting;
using System.Text;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitController : ApiController, IUpdatableService<Permit>, IService<Permit>
  {
    private readonly MDGov.MDE.Permitting.Model.ModelContext _context;
    private readonly IPermitRepository _permitRepository;
    private readonly IRepository<PermitCounty> _permitCountyRepository;
    private readonly IRepository<PermitWithdrawalGroundwater> _permitWwGroundwater;
    private readonly IRepository<PermitWithdrawalSurfacewater> _permitWwSurfacewater;
    private readonly IRepository<PermitStatusState> _permitStatusStateRepository;
    private readonly IRepository<LU_PermitStatus> _permitStatusRepository;
    private readonly IRepository<PermitStatusWorkflow> _permitStatusWorkflowRepository;
    private readonly IRepository<LU_PermitStatusCommunication> _permitStatusCommunicationRepository;
    private readonly IRepository<PermitContact> _permitContactRepository;
    private readonly IRepository<PermitStatusHistory> _permitHistoryStatusRepository;
    private readonly IRepository<ConditionCompliance> _conditionComplianceRepository;
    private readonly IUpdatableService<Message> _messageService;
    private readonly IService<Product> _productService;
    private readonly IUpdatableService<ContactSale> _contactSaleService;

    public PermitController(MDGov.MDE.Permitting.Model.ModelContext context, IPermitRepository permitRepository, IRepository<PermitCounty> permitCountyRepository, IRepository<PermitWithdrawalGroundwater> permitWwGroundwater, IRepository<PermitWithdrawalSurfacewater> permitWwSurfacewater, IRepository<PermitStatusState> permitStatusStateRepository, IRepository<LU_PermitStatus> permitStatusRepository, IRepository<PermitStatusWorkflow> permitStatusWorkflowRepository, IRepository<LU_PermitStatusCommunication> permitStatusCommunicationRepository, IRepository<PermitContact> permitContactRepository, IRepository<PermitStatusHistory> permitHistoryStatusRepository, IRepository<ConditionCompliance> conditionComplianceRepository, IUpdatableService<Message> messageService, IService<Product> productService, IUpdatableService<ContactSale> contactSaleService)
    {
      this._context = context;
      this._permitRepository = permitRepository;
      this._permitCountyRepository = permitCountyRepository;
      this._permitWwGroundwater = permitWwGroundwater;
      this._permitWwSurfacewater = permitWwSurfacewater;
      this._permitStatusWorkflowRepository = permitStatusWorkflowRepository;
      this._permitStatusStateRepository = permitStatusStateRepository;
      this._permitStatusRepository = permitStatusRepository;
      this._permitStatusCommunicationRepository = permitStatusCommunicationRepository;
      this._permitContactRepository = permitContactRepository;
      this._permitHistoryStatusRepository = permitHistoryStatusRepository;
      this._conditionComplianceRepository = conditionComplianceRepository;
      this._messageService = messageService;
      this._productService = productService;
      this._contactSaleService = contactSaleService;
    }

    [HttpGet]
    public IEnumerable<Permit> GetAll(string includeProperties)
    {
      return (IEnumerable<Permit>) this._permitRepository.All(includeProperties);
    }

    [HttpGet]
    public Permit GetById(int id, string includeProperties)
    {
      return this._permitRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public IEnumerable<Permit> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Permit>) this._permitRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public IEnumerable<Permit> GetPermitsForPumpageLetters()
    {
      JsonMediaTypeFormatter jsonFormatter = this.Configuration.Formatters.JsonFormatter;
      jsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
      jsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
      return this._permitRepository.GetPermitsForPumpageLetters();
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._permitRepository.Count(filters);
    }

    [HttpPost]
    public int Save(Permit entity)
    {
      if (entity.PermitNumber != null)
        entity.PermitNumber = entity.PermitNumber.ToUpper();
      int num;
      if (entity.Id > 0)
      {
        Permit byId = this._permitRepository.GetById(entity.Id, "LU_PermitStatus");
        int? permitStatusId1 = byId.PermitStatusId;
        int? permitStatusId2 = entity.PermitStatusId;
        if ((permitStatusId1.GetValueOrDefault() != permitStatusId2.GetValueOrDefault() ? 1 : (permitStatusId1.HasValue != permitStatusId2.HasValue ? 1 : 0)) != 0)
        {
          int? permitStatusId3 = byId.PermitStatusId;
          if ((permitStatusId3.GetValueOrDefault() != 46 ? 0 : (permitStatusId3.HasValue ? 1 : 0)) != 0)
          {
            if (((IEnumerable<int>) new int[3]
            {
              59,
              62,
              65
            }).Contains<int>(entity.PermitStatusId.GetValueOrDefault()))
            {
              entity.DateOut = new DateTime?();
              this.RemoveAllConditionComplianceRecords(entity.Id);
            }
          }
          num = this._permitRepository.Save(entity);
          this.ChangePermitStatus(entity.Id);
        }
        else
          num = this._permitRepository.Save(entity);
      }
      else
        num = this._permitRepository.Save(entity);
      if (entity.PermitStatusId.HasValue && entity.PermitStatusId.Value > 0)
      {
        LU_PermitStatus byId = this._permitStatusRepository.GetById(entity.PermitStatusId.Value, (string) null);
        if (byId != null)
        {
          int? permitCategoryId = byId.PermitCategoryId;
          if ((permitCategoryId.GetValueOrDefault() != 1 ? 0 : (permitCategoryId.HasValue ? 1 : 0)) != 0)
            this._context.sp_AggregateWaterUsageAmounts(new int?(num), entity.LastModifiedBy);
        }
      }
      return num;
    }

    private void RemoveAllConditionComplianceRecords(int permitId)
    {
      List<ConditionCompliance> list = this._conditionComplianceRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitCondition.PermitId == @0", new object[1]
        {
          (object) permitId
        })
      }, (string) null).ToList<ConditionCompliance>();
      if (list == null || list.Count <= 0)
        return;
      foreach (ConditionCompliance conditionCompliance in list)
        this._conditionComplianceRepository.Delete(conditionCompliance.Id);
    }

    [HttpGet]
    public int ClonePermit(int PermitId, int PermitTypeId, string PermitNumber, string userName)
    {
      Permit permit = this._permitRepository.GetRange(0, 1, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Id == @0", new object[1]
        {
          (object) PermitId
        })
      }, "PermitContacts").FirstOrDefault<Permit>();
      if (permit == null)
        return 0;
      string source = permit.ApplicantIdentification ?? "";
      int num = !permit.ApplicationTypeId.HasValue ? 0 : permit.ApplicationTypeId.Value;
      if (source.StartsWith("E"))
      {
        PermitContact permitContact = permit.PermitContacts.SingleOrDefault<PermitContact>((Func<PermitContact, bool>) (x => x.IsPermittee));
        StringBuilder stringBuilder = new StringBuilder(source);
        stringBuilder[0] = 'I';
        if ((int) source.ElementAt<char>(4) == 49)
        {
          int? contactTypeId1 = permitContact.ContactTypeId;
          if ((contactTypeId1.GetValueOrDefault() != 16 ? 0 : (contactTypeId1.HasValue ? 1 : 0)) != 0)
          {
            stringBuilder[4] = '3';
          }
          else
          {
            int? contactTypeId2 = permitContact.ContactTypeId;
            if ((contactTypeId2.GetValueOrDefault() != 27 ? 0 : (contactTypeId2.HasValue ? 1 : 0)) != 0)
              stringBuilder[4] = '2';
          }
          source = stringBuilder.ToString();
        }
      }
      int permitId = this._context.Database.SqlQuery<int>("exec sp_ClonePermit @PermitId, @ApplicationTypeId, @ApplicantIdentification, @PermitTypeId, @PermitNumber, @CreatedBy, @LastModifiedBy", (object[]) new SqlParameter[7]
      {
        new SqlParameter("PermitId", (object) PermitId),
        new SqlParameter("ApplicationTypeId", (object) num),
        new SqlParameter("ApplicantIdentification", (object) source),
        new SqlParameter("PermitTypeId", (object) PermitTypeId),
        PermitNumber == null ? new SqlParameter("PermitNumber", (object) DBNull.Value) : new SqlParameter("PermitNumber", (object) PermitNumber),
        new SqlParameter("CreatedBy", (object) userName),
        new SqlParameter("LastModifiedBy", (object) userName)
      }).SingleOrDefault<int>();
      this.ChangePermitStatus(permitId);
      return permitId;
    }

    [HttpGet]
    public string GeneratePermitNumber(int permitId)
    {
      string str1 = string.Empty;
      string empty1 = string.Empty;
      string empty2 = string.Empty;
      string str2 = DateTime.Now.Year.ToString();
      this._permitRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("Id == @0", new object[1]
        {
          (object) permitId
        })
      }, "LU_PermitStatus");
      IQueryable<PermitCounty> range1 = this._permitCountyRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == @0", new object[1]
        {
          (object) permitId
        })
      }, "LU_County");
      if (range1.Any<PermitCounty>())
        str1 = range1.First<PermitCounty>().LU_County.Key;
      string str3 = !this._permitWwGroundwater.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == @0", new object[1]
        {
          (object) permitId
        })
      }, (string) null).Any<PermitWithdrawalGroundwater>() ? "S" : "G";
      IQueryable<Permit> range2 = this._permitRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitNumber.Contains(@0)", new object[1]
        {
          (object) (str1 + str2 + str3)
        })
      }, (string) null);
      string str4 = "001";
      if (range2.Any<Permit>())
        str4 = range2.Select<Permit, int>((Expression<Func<Permit, int>>) (p => EdmFunctions.ToInt32(p.PermitNumber.Substring(p.PermitNumber.Length - 3)))).FirstMissing().ToString("D3");
      return string.Format("{0}{1}{2}{3}", (object) str1, (object) str2, (object) str3, (object) str4);
    }

    [HttpGet]
    public bool IsUniquePermitNumber(string PermitNumber)
    {
      return this._permitRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitNumber.Contains(@0)", new object[1]
        {
          (object) PermitNumber
        })
      }, (string) null).Count<Permit>() <= 0;
    }

    [HttpGet]
    public int CalculateStatus(int permitId)
    {
      Permit byId = this._permitRepository.GetById(permitId, (string) null);
      int currentPermitStatus = !byId.PermitStatusId.HasValue ? 0 : byId.PermitStatusId.Value;
      int permitTypeId = !byId.PermitTypeId.HasValue ? 0 : byId.PermitTypeId.Value;
      int currentPertCategory = 0;
      IQueryable<PermitStatusWorkflow> range1 = this._permitStatusWorkflowRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitTypeId=" + (object) permitTypeId, new object[0])
      }, (string) null);
      IQueryable<LU_PermitStatus> source1 = this._permitStatusRepository.All((string) null);
      IQueryable<LU_PermitStatus> source2 = source1.Where<LU_PermitStatus>((Expression<Func<LU_PermitStatus, bool>>) (x => x.Id == currentPermitStatus));
      if (source2 != null && source2.Count<LU_PermitStatus>() > 0)
        currentPertCategory = !source2.First<LU_PermitStatus>().PermitCategoryId.HasValue ? 0 : source2.First<LU_PermitStatus>().PermitCategoryId.Value;
      IQueryable<PermitStatusWorkflow> source3 = range1.Join((IEnumerable<LU_PermitStatus>) source1, (Expression<Func<PermitStatusWorkflow, int>>) (workFlow => workFlow.PermitStatusId), (Expression<Func<LU_PermitStatus, int>>) (pStatus => pStatus.Id), (workFlow, pStatus) => new
      {
        workFlow = workFlow,
        pStatus = pStatus
      }).Where(data => data.workFlow.PermitTypeId == permitTypeId && data.pStatus.PermitCategoryId == (int?) currentPertCategory).OrderBy(data => data.workFlow.Sequence).Select(data => data.workFlow);
      IQueryable<PermitStatusState> range2 = this._permitStatusStateRepository.GetRange(0, 9999, "", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId=" + (object) permitId, new object[0])
      }, (string) null);
      if (range2 != null && range2.Count<PermitStatusState>() > 0)
      {
        foreach (PermitStatusWorkflow permitStatusWorkflow in (IEnumerable<PermitStatusWorkflow>) source3)
        {
          PermitStatusWorkflow workFlow = permitStatusWorkflow;
          PermitStatusState permitStatusState = range2.Where<PermitStatusState>((Expression<Func<PermitStatusState, bool>>) (x => x.PermitStatusId == (int?) workFlow.PermitStatusId)).FirstOrDefault<PermitStatusState>();
          if (permitStatusState == null)
            return workFlow.PermitStatusId;
          if (!permitStatusState.Completed)
            return permitStatusState.PermitStatusId.Value;
        }
      }
      else if (source3.Count<PermitStatusWorkflow>() > 0)
        return source3.OrderBy<PermitStatusWorkflow, int>((Expression<Func<PermitStatusWorkflow, int>>) (x => x.Sequence)).First<PermitStatusWorkflow>().PermitStatusId;
      return 0;
    }

    [HttpGet]
    public bool ChangeStatus(int permitId)
    {
      return this.ChangePermitStatus(permitId);
    }

    [HttpGet]
    public StatusChange GetLastStatusChange(int permitId, int permitStatusId)
    {
      List<PermitStatusHistory> list = this._permitHistoryStatusRepository.GetRange(0, int.MaxValue, "CreatedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId == @0", new object[1]
        {
          (object) permitId
        })
      }, (string) null).ToList<PermitStatusHistory>();
      if (list == null)
        return (StatusChange) null;
      StatusChange statusChange = new StatusChange();
      for (int index = 0; index < list.Count; ++index)
      {
        PermitStatusHistory permitStatusHistory = list[index];
        int? permitStatusId1 = permitStatusHistory.PermitStatusId;
        int num = permitStatusId;
        if ((permitStatusId1.GetValueOrDefault() != num ? 0 : (permitStatusId1.HasValue ? 1 : 0)) != 0)
        {
          if (index + 1 < list.Count)
            statusChange.PreviousPermitStatusId = list[index + 1].PermitStatusId;
          statusChange.PermitStatusId = permitStatusHistory.PermitStatusId.GetValueOrDefault();
          statusChange.ChangedBy = permitStatusHistory.CreatedBy;
          statusChange.ChangedDate = permitStatusHistory.CreatedDate;
          break;
        }
      }
      return statusChange;
    }

    [HttpPost]
    public bool CancelPermitApplication(int permitId)
    {
      return this._permitRepository.CancelPermitApplication(permitId);
    }

    public bool ChangePermitStatus(int permitId)
    {
      try
      {
        Permit byId = this._permitRepository.GetById(permitId, "PermitContacts,PermitWithdrawalGroundwaters,PermitWithdrawalSurfacewaters");
        this._permitHistoryStatusRepository.Save(new PermitStatusHistory()
        {
          PermitStatusId = byId.PermitStatusId,
          PermitId = new int?(byId.Id),
          LastModifiedBy = byId.LastModifiedBy
        });
        IQueryable<LU_PermitStatusCommunication> range1 = this._permitStatusCommunicationRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("PermitStatusId == @0", new object[1]
          {
            (object) byId.PermitStatusId
          })
        }, (string) null);
        if (range1 != null && range1.Any<LU_PermitStatusCommunication>())
        {
          foreach (LU_PermitStatusCommunication statusCommunication in (IEnumerable<LU_PermitStatusCommunication>) range1)
          {
            if (statusCommunication.ContactTypeId.HasValue)
            {
              IQueryable<PermitContact> range2 = this._permitContactRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
              {
                new DynamicFilter("PermitId == @0 && ContactTypeId == @1", new object[2]
                {
                  (object) permitId,
                  (object) statusCommunication.ContactTypeId.Value
                })
              }, (string) null);
              if (range2.Any<PermitContact>())
              {
                List<int> intList1 = new List<int>()
                {
                  9,
                  10
                };
                List<int> intList2 = new List<int>()
                {
                  2,
                  8,
                  16,
                  27
                };
                string str;
                if (intList1.Contains(statusCommunication.ContactTypeId.Value))
                  str = ConfigurationManager.AppSettings["ExternalPortalUrl"].TrimEnd('/') + "/Security/Account/Login?ReturnUrl=%2fPermitting%2fPermit%2fDetails%2f" + (object) permitId;
                else if (intList2.Contains(statusCommunication.ContactTypeId.Value))
                  str = ConfigurationManager.AppSettings["ExternalPortalUrl"].TrimEnd('/') + "/Permitting/Permit/Details/" + (object) permitId;
                else
                  str = ConfigurationManager.AppSettings["InternalPortalUrl"].TrimEnd('/') + "/Permitting/Permit?id=" + (object) permitId;
                string appSetting = ConfigurationManager.AppSettings["SenderEmail"];
                Message entity = new Message();
                entity.Subject = statusCommunication.EmailSubject.ToString();
                entity.Body = statusCommunication.EmailBody + "<p>" + str + "</p>";
                entity.From = appSetting;
                entity.EndDate = new DateTime?(DateTime.MaxValue);
                entity.StartDate = new DateTime?(DateTime.MinValue);
                entity.PermitId = permitId;
                entity.NotificationText = statusCommunication.NotificationBody;
                entity.NotificationTypeId = 1;
                entity.ContactIds = range2.Select<PermitContact, int>((Expression<Func<PermitContact, int>>) (x => x.ContactId)).ToList<int>();
                entity.LastModifiedBy = byId.LastModifiedBy;
                this._messageService.Save(entity);
              }
            }
          }
        }
        int? permitStatusId = byId.PermitStatusId;
        if ((permitStatusId.GetValueOrDefault() != 8 ? 0 : (permitStatusId.HasValue ? 1 : 0)) != 0 && !byId.IsExemptFromFees.GetValueOrDefault())
        {
          Product product = this._productService.GetRange(0, 1, "LastModifiedDate desc", (IEnumerable<DynamicFilter>) new DynamicFilter[1]
          {
            new DynamicFilter("ProductTypeId == @0 && EffectiveDate <= @1 && ApplicationTypeId == @2 && Active", new object[3]
            {
              (object) 1,
              (object) DateTime.Today,
              (object) byId.ApplicationTypeId
            })
          }, "GraduatedProductRules.LU_ProductRuleOperator").SingleOrDefault<Product>();
          if (product != null)
          {
            int contactId = byId.PermitContacts.Single<PermitContact>((Func<PermitContact, bool>) (pc =>
            {
              if (pc.IsPermittee)
                return pc.IsPrimary;
              return false;
            })).ContactId;
            if (product.IsGraduated)
            {
              long left = 0;
              switch (byId.PermitNumber.ToUpper()[6])
              {
                case 'G':
                  PermitWithdrawalGroundwater withdrawalGroundwater = byId.PermitWithdrawalGroundwaters.SingleOrDefault<PermitWithdrawalGroundwater>();
                  if (withdrawalGroundwater != null)
                  {
                    left = withdrawalGroundwater.AvgGalPerDay;
                    break;
                  }
                  break;
                case 'S':
                  PermitWithdrawalSurfacewater withdrawalSurfacewater = byId.PermitWithdrawalSurfacewaters.SingleOrDefault<PermitWithdrawalSurfacewater>();
                  if (withdrawalSurfacewater != null)
                  {
                    left = withdrawalSurfacewater.AvgGalPerDay;
                    break;
                  }
                  break;
                default:
                  throw new Exception("Error parsing groundwater/surfacewater from PermitNumber");
              }
              foreach (GraduatedProductRule graduatedProductRule in (IEnumerable<GraduatedProductRule>) product.GraduatedProductRules)
              {
                if (graduatedProductRule.LU_ProductRuleOperator.Description.ToBooleanOperation(left, graduatedProductRule.ProductRuleOperand))
                  this._contactSaleService.Save(new ContactSale()
                  {
                    ContactId = contactId,
                    PermitId = new int?(byId.Id),
                    PermitName = byId.PermitName,
                    ProductId = new int?(product.Id),
                    ContactSaleStatusId = 1,
                    Amount = graduatedProductRule.Amount,
                    LastModifiedBy = byId.LastModifiedBy
                  });
              }
            }
            else
              this._contactSaleService.Save(new ContactSale()
              {
                ContactId = contactId,
                PermitId = new int?(byId.Id),
                PermitName = byId.PermitName,
                ProductId = new int?(product.Id),
                ContactSaleStatusId = 1,
                Amount = product.Amount.GetValueOrDefault(),
                LastModifiedBy = byId.LastModifiedBy
              });
          }
        }
        return true;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    public void Delete(int id)
    {
      throw new NotImplementedException();
    }
  }
}
