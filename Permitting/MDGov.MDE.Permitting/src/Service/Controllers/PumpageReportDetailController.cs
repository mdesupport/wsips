﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PumpageReportDetailController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PumpageReportDetailController : ApiController, IUpdatableService<PumpageReportDetail>, IService<PumpageReportDetail>
  {
    private readonly IRepository<PumpageReportDetail> _pumpageReportDetailsRepository;

    public PumpageReportDetailController(IRepository<PumpageReportDetail> pumpageReportDetailsRepository)
    {
      this._pumpageReportDetailsRepository = pumpageReportDetailsRepository;
    }

    [HttpGet]
    public IEnumerable<PumpageReportDetail> GetAll(string includeProperties)
    {
      return (IEnumerable<PumpageReportDetail>) this._pumpageReportDetailsRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PumpageReportDetail> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PumpageReportDetail>) this._pumpageReportDetailsRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PumpageReportDetail GetById(int id, string includeProperties)
    {
      return this._pumpageReportDetailsRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._pumpageReportDetailsRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PumpageReportDetail entity)
    {
      return this._pumpageReportDetailsRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._pumpageReportDetailsRepository.Delete(id);
    }
  }
}
