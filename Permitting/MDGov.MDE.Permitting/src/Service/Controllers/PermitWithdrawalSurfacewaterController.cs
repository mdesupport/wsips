﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWithdrawalSurfacewaterController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWithdrawalSurfacewaterController : ApiController, IUpdatableService<PermitWithdrawalSurfacewater>, IService<PermitWithdrawalSurfacewater>
  {
    private readonly IRepository<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterRepository;

    public PermitWithdrawalSurfacewaterController(IRepository<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterRepository)
    {
      this._permitWithdrawalSurfacewaterRepository = permitWithdrawalSurfacewaterRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWithdrawalSurfacewater> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalSurfacewater>) this._permitWithdrawalSurfacewaterRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWithdrawalSurfacewater> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalSurfacewater>) this._permitWithdrawalSurfacewaterRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitWithdrawalSurfacewater GetById(int id, string includeProperties = null)
    {
      return this._permitWithdrawalSurfacewaterRepository.GetById(id, includeProperties);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWithdrawalSurfacewaterRepository.Delete(id);
    }

    [HttpPost]
    public int Save(PermitWithdrawalSurfacewater entity)
    {
      return this._permitWithdrawalSurfacewaterRepository.Save(entity);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
