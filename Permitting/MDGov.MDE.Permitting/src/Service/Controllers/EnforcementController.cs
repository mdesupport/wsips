﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.EnforcementController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class EnforcementController : ApiController, IUpdatableService<Enforcement>, IService<Enforcement>
  {
    private readonly IRepository<Enforcement> _enforcementRepository;

    public EnforcementController(IRepository<Enforcement> enforcementRepository)
    {
      this._enforcementRepository = enforcementRepository;
    }

    [HttpGet]
    public IEnumerable<Enforcement> GetAll(string includeProperties)
    {
      return (IEnumerable<Enforcement>) this._enforcementRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Enforcement> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Enforcement>) this._enforcementRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public Enforcement GetById(int id, string includeProperties)
    {
      return this._enforcementRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._enforcementRepository.Count(filters);
    }

    [HttpPost]
    public int Save(Enforcement entity)
    {
      return this._enforcementRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._enforcementRepository.Delete(id);
    }
  }
}
