﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.AnswerController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class AnswerController : ApiController, IService<LU_CheckListAnswer>
  {
    private readonly IRepository<LU_CheckListAnswer> _answerRepository;

    public AnswerController(IRepository<LU_CheckListAnswer> answerRepository)
    {
      this._answerRepository = answerRepository;
    }

    [HttpGet]
    public IEnumerable<LU_CheckListAnswer> GetAll(string includeProperties)
    {
      return (IEnumerable<LU_CheckListAnswer>) this._answerRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_CheckListAnswer> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_CheckListAnswer>) this._answerRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public LU_CheckListAnswer GetById(int id, string includeProperties)
    {
      return this._answerRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._answerRepository.Count(filters);
    }
  }
}
