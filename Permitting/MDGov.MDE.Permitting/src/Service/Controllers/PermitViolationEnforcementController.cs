﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitViolationEnforcementController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitViolationEnforcementController : ApiController, IUpdatableService<PermitViolationEnforcement>, IService<PermitViolationEnforcement>
  {
    private readonly IRepository<PermitViolationEnforcement> _permitViolationEnforcementRepository;

    public PermitViolationEnforcementController(IRepository<PermitViolationEnforcement> permitViolationEnforcementRepository)
    {
      this._permitViolationEnforcementRepository = permitViolationEnforcementRepository;
    }

    [HttpGet]
    public IEnumerable<PermitViolationEnforcement> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitViolationEnforcement>) this._permitViolationEnforcementRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitViolationEnforcement> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitViolationEnforcement>) this._permitViolationEnforcementRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitViolationEnforcement GetById(int id, string includeProperties)
    {
      return this._permitViolationEnforcementRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._permitViolationEnforcementRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PermitViolationEnforcement entity)
    {
      return this._permitViolationEnforcementRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitViolationEnforcementRepository.Delete(id);
    }
  }
}
