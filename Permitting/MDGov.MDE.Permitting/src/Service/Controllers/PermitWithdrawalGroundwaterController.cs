﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWithdrawalGroundwaterController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWithdrawalGroundwaterController : ApiController, IUpdatableService<PermitWithdrawalGroundwater>, IService<PermitWithdrawalGroundwater>
  {
    private readonly IRepository<PermitWithdrawalGroundwater> _permitWithdrawalGroundwaterRepository;

    public PermitWithdrawalGroundwaterController(IRepository<PermitWithdrawalGroundwater> permitWithdrawalGroundwaterRepository)
    {
      this._permitWithdrawalGroundwaterRepository = permitWithdrawalGroundwaterRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWithdrawalGroundwater> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalGroundwater>) this._permitWithdrawalGroundwaterRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWithdrawalGroundwater> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalGroundwater>) this._permitWithdrawalGroundwaterRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitWithdrawalGroundwater GetById(int id, string includeProperties = null)
    {
      return this._permitWithdrawalGroundwaterRepository.GetById(Convert.ToInt32(id), includeProperties);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWithdrawalGroundwaterRepository.Delete(id);
    }

    [HttpPost]
    public int Save(PermitWithdrawalGroundwater entity)
    {
      return this._permitWithdrawalGroundwaterRepository.Save(entity);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
