﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWaterWithdrawalPurposeController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWaterWithdrawalPurposeController : ApiController, IUpdatableService<PermitWaterWithdrawalPurpose>, IService<PermitWaterWithdrawalPurpose>
  {
    private readonly IRepository<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeRepository;

    public PermitWaterWithdrawalPurposeController(IRepository<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeRepository)
    {
      this._permitWaterWithdrawalPurposeRepository = permitWaterWithdrawalPurposeRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWaterWithdrawalPurpose> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWaterWithdrawalPurpose>) this._permitWaterWithdrawalPurposeRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWaterWithdrawalPurpose> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWaterWithdrawalPurpose>) this._permitWaterWithdrawalPurposeRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitWaterWithdrawalPurpose GetById(int id, string includeProperties = null)
    {
      return this._permitWaterWithdrawalPurposeRepository.GetById(id, includeProperties);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWaterWithdrawalPurposeRepository.Delete(id);
    }

    [HttpPost]
    public int Save(PermitWaterWithdrawalPurpose entity)
    {
      return this._permitWaterWithdrawalPurposeRepository.Save(entity);
    }

    [HttpDelete]
    public void DeleteAll(int permitId)
    {
      List<PermitWaterWithdrawalPurpose> list = this._permitWaterWithdrawalPurposeRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("PermitId==" + (object) permitId, new object[0])
      }, (string) null).ToList<PermitWaterWithdrawalPurpose>();
      if (list == null || list.Count <= 0)
        return;
      foreach (PermitWaterWithdrawalPurpose withdrawalPurpose in list)
        this._permitWaterWithdrawalPurposeRepository.Delete(withdrawalPurpose.Id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
