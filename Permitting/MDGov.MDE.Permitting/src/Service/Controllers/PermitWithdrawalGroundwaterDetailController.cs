﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWithdrawalGroundwaterDetailController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWithdrawalGroundwaterDetailController : ApiController, IUpdatableService<PermitWithdrawalGroundwaterDetail>, IService<PermitWithdrawalGroundwaterDetail>
  {
    private readonly IRepository<PermitWithdrawalGroundwater> _permitWithdrawalGroundwaterRepository;
    private readonly IRepository<PermitWithdrawalGroundwaterDetail> _permitWithdrawalGroundwaterDetailRepository;

    public PermitWithdrawalGroundwaterDetailController(IRepository<PermitWithdrawalGroundwater> permitWithdrawalGroundwaterRepository, IRepository<PermitWithdrawalGroundwaterDetail> permitWithdrawalGroundwaterDetailRepository)
    {
      this._permitWithdrawalGroundwaterRepository = permitWithdrawalGroundwaterRepository;
      this._permitWithdrawalGroundwaterDetailRepository = permitWithdrawalGroundwaterDetailRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWithdrawalGroundwaterDetail> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalGroundwaterDetail>) this._permitWithdrawalGroundwaterDetailRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWithdrawalGroundwaterDetail> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalGroundwaterDetail>) this._permitWithdrawalGroundwaterDetailRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitWithdrawalGroundwaterDetail GetById(int id, string includeProperties = null)
    {
      return this._permitWithdrawalGroundwaterDetailRepository.GetById(id, includeProperties);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      PermitWithdrawalGroundwaterDetail byId = this._permitWithdrawalGroundwaterDetailRepository.GetById(id, (string) null);
      if (byId == null)
        return;
      if (byId.PermitWithdrawalGroundwater.PermitWithdrawalGroundwaterDetails.Count == 1)
        this._permitWithdrawalGroundwaterRepository.Delete(byId.PermitWithdrawalGroundwaterId);
      else
        this._permitWithdrawalGroundwaterDetailRepository.Delete(id);
    }

    [HttpPost]
    public int Save(PermitWithdrawalGroundwaterDetail entity)
    {
      return this._permitWithdrawalGroundwaterDetailRepository.Save(entity);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
