﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwInstitutionalEducational_drinkingOrsanitaryController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwInstitutionalEducational_drinkingOrsanitaryController : ApiController, IUpdatableService<PermitWwInstitutionalEducational_drinkingOrsanitary>, IService<PermitWwInstitutionalEducational_drinkingOrsanitary>
  {
    private readonly IRepository<PermitWwInstitutionalEducational_drinkingOrsanitary> _permitWwInstitutionalEducational_drinkingOrsanitaryRepository;

    public PermitWwInstitutionalEducational_drinkingOrsanitaryController(IRepository<PermitWwInstitutionalEducational_drinkingOrsanitary> permitWwInstitutionalEducational_drinkingOrsanitaryRepository)
    {
      this._permitWwInstitutionalEducational_drinkingOrsanitaryRepository = permitWwInstitutionalEducational_drinkingOrsanitaryRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwInstitutionalEducational_drinkingOrsanitary> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwInstitutionalEducational_drinkingOrsanitary>) this._permitWwInstitutionalEducational_drinkingOrsanitaryRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwInstitutionalEducational_drinkingOrsanitary> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwInstitutionalEducational_drinkingOrsanitary>) this._permitWwInstitutionalEducational_drinkingOrsanitaryRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwInstitutionalEducational_drinkingOrsanitary entity)
    {
      return this._permitWwInstitutionalEducational_drinkingOrsanitaryRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwInstitutionalEducational_drinkingOrsanitaryRepository.Delete(Convert.ToInt32(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwInstitutionalEducational_drinkingOrsanitary GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
