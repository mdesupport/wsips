﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ContactController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ContactController : ApiController, IUpdatableService<Contact>, IService<Contact>
  {
    private readonly IRepository<Contact> _contactRepository;
    private readonly IValidator<Contact> _contactValidator;
    private readonly ModelContext _context;

    public ContactController(IRepository<Contact> contactRepository, IValidator<Contact> contactValidator, ModelContext context)
    {
      this._contactRepository = contactRepository;
      this._contactValidator = contactValidator;
      this._context = context;
    }

    [HttpGet]
    public IEnumerable<Contact> GetAll(string includeProperties)
    {
      return (IEnumerable<Contact>) this._contactRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Contact> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Contact>) this._contactRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public Contact GetById(int id, string includeProperties)
    {
      return this._contactRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._contactRepository.Count(filters);
    }

    [HttpPost]
    public int Save(Contact entity)
    {
      this._contactValidator.ValidateAndThrow<Contact>(entity);
      if (entity.Id > 0)
      {
        Contact byId = this._contactRepository.GetById(entity.Id, (string) null);
        entity.ContactTypeId = !entity.UserId.HasValue || byId.UserId.HasValue ? byId.ContactTypeId : 2;
      }
      return this._contactRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._contactRepository.Delete(id);
    }

    [HttpGet]
    public int MergeContacts(int fromContactId, int toContactId)
    {
      return this._context.sp_MergeContacts(new int?(fromContactId), new int?(toContactId));
    }
  }
}
