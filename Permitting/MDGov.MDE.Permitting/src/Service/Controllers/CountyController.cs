﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.CountyController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class CountyController : ApiController, IService<LU_County>
  {
    private readonly IRepository<LU_County> _countyRepository;

    public CountyController(IRepository<LU_County> countyRepository)
    {
      this._countyRepository = countyRepository;
    }

    [HttpGet]
    public IEnumerable<LU_County> GetAll(string includeProperties)
    {
      return (IEnumerable<LU_County>) this._countyRepository.All(includeProperties);
    }

    [HttpGet]
    public LU_County GetById(int id, string includeProperties)
    {
      return this._countyRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_County> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_County>) this._countyRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
