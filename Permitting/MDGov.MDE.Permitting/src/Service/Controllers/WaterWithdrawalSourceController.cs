﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.WaterWithdrawalSourceController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class WaterWithdrawalSourceController : ApiController, IService<LU_WaterWithdrawalSource>
  {
    private readonly IRepository<LU_WaterWithdrawalSource> _waterWithdrawalSourceRepository;

    public WaterWithdrawalSourceController(IRepository<LU_WaterWithdrawalSource> waterWithdrawalSourceRepository)
    {
      this._waterWithdrawalSourceRepository = waterWithdrawalSourceRepository;
    }

    [HttpGet]
    public IEnumerable<LU_WaterWithdrawalSource> GetAll(string includeProperties)
    {
      return (IEnumerable<LU_WaterWithdrawalSource>) this._waterWithdrawalSourceRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_WaterWithdrawalSource> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_WaterWithdrawalSource>) this._waterWithdrawalSourceRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_WaterWithdrawalSource GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
