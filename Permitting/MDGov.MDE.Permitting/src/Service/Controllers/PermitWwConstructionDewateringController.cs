﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwConstructionDewateringController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwConstructionDewateringController : ApiController, IUpdatableService<PermitWwConstructionDewatering>, IService<PermitWwConstructionDewatering>
  {
    private readonly IRepository<PermitWwConstructionDewatering> _permitWwConstructionDewateringRepository;

    public PermitWwConstructionDewateringController(IRepository<PermitWwConstructionDewatering> permitWwConstructionDewateringRepository)
    {
      this._permitWwConstructionDewateringRepository = permitWwConstructionDewateringRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwConstructionDewatering> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwConstructionDewatering>) this._permitWwConstructionDewateringRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwConstructionDewatering> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwConstructionDewatering>) this._permitWwConstructionDewateringRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwConstructionDewatering entity)
    {
      return this._permitWwConstructionDewateringRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwConstructionDewateringRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwConstructionDewatering GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
