﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitContactInformationController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitContactInformationController : ApiController, IUpdatableService<PermitContactInformation>, IService<PermitContactInformation>
  {
    private readonly IRepository<PermitContactInformation> _permitContactInformationRepository;
    private readonly IValidator<PermitContactInformation> _permitContactInformationValidator;

    public PermitContactInformationController(IRepository<PermitContactInformation> permitContactInformationRepository, IValidator<PermitContactInformation> permitContactInformationValidator)
    {
      this._permitContactInformationRepository = permitContactInformationRepository;
      this._permitContactInformationValidator = permitContactInformationValidator;
    }

    [HttpGet]
    public IEnumerable<PermitContactInformation> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitContactInformation>) this._permitContactInformationRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitContactInformation> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitContactInformation>) this._permitContactInformationRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitContactInformation GetById(int id, string includeProperties)
    {
      return this._permitContactInformationRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._permitContactInformationRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PermitContactInformation entity)
    {
      this._permitContactInformationValidator.ValidateAndThrow<PermitContactInformation>(entity);
      return this._permitContactInformationRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitContactInformationRepository.Delete(id);
    }
  }
}
