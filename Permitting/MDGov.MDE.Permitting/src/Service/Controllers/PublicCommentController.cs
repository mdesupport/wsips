﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PublicCommentController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PublicCommentController : ApiController, IUpdatableService<PublicComment>, IService<PublicComment>
  {
    private readonly IRepository<PublicComment> _publicCommentRepository;

    public PublicCommentController(IRepository<PublicComment> publicCommentRepository)
    {
      this._publicCommentRepository = publicCommentRepository;
    }

    public int Save(PublicComment comment)
    {
      return this._publicCommentRepository.Save(comment);
    }

    [HttpPost]
    public IEnumerable<PublicComment> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PublicComment>) this._publicCommentRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public void Delete(int id)
    {
      this._publicCommentRepository.Delete(id);
    }

    [HttpGet]
    public PublicComment GetById(int id, string includeProperties)
    {
      return this._publicCommentRepository.GetById(id, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<PublicComment> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
