﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.DocumentController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class DocumentController : ApiController, IUpdatableService<Document>, IService<Document>
  {
    private readonly IRepository<Document> _documentRepository;

    public DocumentController(IRepository<Document> documentRepository)
    {
      this._documentRepository = documentRepository;
    }

    public int Save(Document doc)
    {
      return this._documentRepository.Save(doc);
    }

    public IEnumerable<Document> GetAll()
    {
      return (IEnumerable<Document>) this._documentRepository.All((string) null);
    }

    [HttpGet]
    public Document GetById(int id, string includeProperties)
    {
      return this._documentRepository.GetById(id, includeProperties);
    }

    public void Delete(int id)
    {
      this._documentRepository.Delete(id);
    }

    [HttpPost]
    public IEnumerable<Document> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Document>) this._documentRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._documentRepository.Count(filters);
    }

    public IEnumerable<Document> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
