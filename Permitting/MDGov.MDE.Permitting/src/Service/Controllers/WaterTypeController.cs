﻿using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
    public class WaterTypeController : ApiController, IService<LU_WaterType>
    {
        private readonly IRepository<LU_WaterType> _waterTypeRepository;

        public WaterTypeController(IRepository<LU_WaterType> waterTypeRepository)
        {
            this._waterTypeRepository = waterTypeRepository;
        }

        [HttpGet]
        public IEnumerable<LU_WaterType> GetAll(string includeProperties = null)
        {
            return (IEnumerable<LU_WaterType>)this._waterTypeRepository.All((string)null);
        }

        [HttpGet]
        public LU_WaterType GetById(int id, string includeProperties)
        {
            return this._waterTypeRepository.GetById(id, includeProperties);
        }

        public int Count(IEnumerable<DynamicFilter> filters)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LU_WaterType> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
        {
            throw new NotImplementedException();
        }
    }
}
