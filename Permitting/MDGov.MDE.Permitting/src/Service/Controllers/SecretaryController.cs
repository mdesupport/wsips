﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.SecretaryController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class SecretaryController : ApiController, IUpdatableService<Secretary>, IService<Secretary>
  {
    private readonly IRepository<Secretary> _secretaryRepository;
    private readonly IValidator<Secretary> _secretaryValidator;

    public SecretaryController(IRepository<Secretary> secretaryRepository, IValidator<Secretary> secretaryValidator)
    {
      this._secretaryRepository = secretaryRepository;
      this._secretaryValidator = secretaryValidator;
    }

    [HttpGet]
    public IEnumerable<Secretary> GetAll(string includeProperties = null)
    {
      return (IEnumerable<Secretary>) this._secretaryRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Secretary> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Secretary>) this._secretaryRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(Secretary entity)
    {
      return this._secretaryRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._secretaryRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public Secretary GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
