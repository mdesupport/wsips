﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.WatershedController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Permitting.Model;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class WatershedController : ApiController
  {
    private readonly IRepository<LU_Watershed> _watershedRepository;

    public WatershedController(IRepository<LU_Watershed> watershedRepository)
    {
      this._watershedRepository = watershedRepository;
    }

    [HttpGet]
    public LU_Watershed GetById(int id, string includeProperties)
    {
      return this._watershedRepository.GetById(id, includeProperties);
    }
  }
}
