﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitConditionController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitConditionController : ApiController, IUpdatableService<PermitCondition>, IService<PermitCondition>
  {
    private readonly IRepository<PermitCondition> _permitConditionRepository;
    private readonly IValidator<PermitCondition> _permitConditionValidator;

    public PermitConditionController(IRepository<PermitCondition> permitConditionRepository, IValidator<PermitCondition> permitConditionValidator)
    {
      this._permitConditionRepository = permitConditionRepository;
      this._permitConditionValidator = permitConditionValidator;
    }

    [HttpGet]
    public IEnumerable<PermitCondition> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitCondition>) this._permitConditionRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitCondition> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitCondition>) this._permitConditionRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitCondition GetById(int id, string includeProperties)
    {
      return this._permitConditionRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(PermitCondition entity)
    {
      this._permitConditionValidator.ValidateAndThrow<PermitCondition>(entity);
      return this._permitConditionRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitConditionRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
