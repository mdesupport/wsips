﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitStatusHistoryController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitStatusHistoryController : ApiController, IUpdatableService<PermitStatusHistory>, IService<PermitStatusHistory>
  {
    private readonly IRepository<PermitStatusHistory> _permitStatusHistoryRepository;

    public PermitStatusHistoryController(IRepository<PermitStatusHistory> permitStatusHistoryRepository)
    {
      this._permitStatusHistoryRepository = permitStatusHistoryRepository;
    }

    [HttpPost]
    public IEnumerable<PermitStatusHistory> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitStatusHistory>) this._permitStatusHistoryRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitStatusHistory entity)
    {
      return this._permitStatusHistoryRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitStatusHistoryRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<PermitStatusHistory> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }

    public PermitStatusHistory GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
