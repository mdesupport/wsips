﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitCategoryController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitCategoryController : ApiController, IService<LU_PermitCategory>
  {
    private readonly IRepository<LU_PermitCategory> _permitCategoryRepository;

    public PermitCategoryController(IRepository<LU_PermitCategory> permitCategoryRepository)
    {
      this._permitCategoryRepository = permitCategoryRepository;
    }

    [HttpGet]
    public IEnumerable<LU_PermitCategory> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_PermitCategory>) this._permitCategoryRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_PermitCategory> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      return (IEnumerable<LU_PermitCategory>) this._permitCategoryRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_PermitCategory GetById(int id, string includeProperties = null)
    {
      return this._permitCategoryRepository.GetById(id, includeProperties);
    }
  }
}
