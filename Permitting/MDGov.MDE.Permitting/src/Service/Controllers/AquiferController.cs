﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.AquiferController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class AquiferController : ApiController, IService<LU_Aquifer>
  {
    private readonly IRepository<LU_Aquifer> _aquiferRepository;

    public AquiferController(IRepository<LU_Aquifer> aquiferRepository)
    {
      this._aquiferRepository = aquiferRepository;
    }

    [HttpGet]
    public IEnumerable<LU_Aquifer> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_Aquifer>) this._aquiferRepository.All((string) null);
    }

    [HttpGet]
    public LU_Aquifer GetById(int id, string includeProperties)
    {
      return this._aquiferRepository.GetById(id, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<LU_Aquifer> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
