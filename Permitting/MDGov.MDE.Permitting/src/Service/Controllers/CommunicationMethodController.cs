﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.CommunicationMethodController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class CommunicationMethodController : ApiController, IService<LU_CommunicationMethod>
  {
    private readonly IRepository<LU_CommunicationMethod> _communicationMethodRepository;

    public CommunicationMethodController(IRepository<LU_CommunicationMethod> communicationMethodRepository)
    {
      this._communicationMethodRepository = communicationMethodRepository;
    }

    public IEnumerable<LU_CommunicationMethod> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_CommunicationMethod>) this._communicationMethodRepository.All(includeProperties);
    }

    public LU_CommunicationMethod GetById(int id, string includeProperties = null)
    {
      return this._communicationMethodRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_CommunicationMethod> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      return (IEnumerable<LU_CommunicationMethod>) this._communicationMethodRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
