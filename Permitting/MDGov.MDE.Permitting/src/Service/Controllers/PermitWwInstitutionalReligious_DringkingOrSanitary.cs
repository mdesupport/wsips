﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwInstitutionalReligious_DringkingOrSanitaryController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwInstitutionalReligious_DringkingOrSanitaryController : ApiController, IUpdatableService<PermitWwInstitutionalReligious_DringkingOrSanitary>, IService<PermitWwInstitutionalReligious_DringkingOrSanitary>
  {
    private readonly IRepository<PermitWwInstitutionalReligious_DringkingOrSanitary> _permitWwInstitutionalReligious_DringkingOrSanitaryRepository;

    public PermitWwInstitutionalReligious_DringkingOrSanitaryController(IRepository<PermitWwInstitutionalReligious_DringkingOrSanitary> permitWwInstitutionalReligious_DringkingOrSanitaryRepository)
    {
      this._permitWwInstitutionalReligious_DringkingOrSanitaryRepository = permitWwInstitutionalReligious_DringkingOrSanitaryRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwInstitutionalReligious_DringkingOrSanitary> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwInstitutionalReligious_DringkingOrSanitary>) this._permitWwInstitutionalReligious_DringkingOrSanitaryRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwInstitutionalReligious_DringkingOrSanitary> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwInstitutionalReligious_DringkingOrSanitary>) this._permitWwInstitutionalReligious_DringkingOrSanitaryRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwInstitutionalReligious_DringkingOrSanitary entity)
    {
      return this._permitWwInstitutionalReligious_DringkingOrSanitaryRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwInstitutionalReligious_DringkingOrSanitaryRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwInstitutionalReligious_DringkingOrSanitary GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
