﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwCommercialDrinkingOrSanitaryController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwCommercialDrinkingOrSanitaryController : ApiController, IUpdatableService<PermitWwCommercialDrinkingOrSanitary>, IService<PermitWwCommercialDrinkingOrSanitary>
  {
    private readonly IRepository<PermitWwCommercialDrinkingOrSanitary> _permitWwCommercialDrinkingOrSanitaryRepository;

    public PermitWwCommercialDrinkingOrSanitaryController(IRepository<PermitWwCommercialDrinkingOrSanitary> permitWwCommercialDrinkingOrSanitaryRepository)
    {
      this._permitWwCommercialDrinkingOrSanitaryRepository = permitWwCommercialDrinkingOrSanitaryRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwCommercialDrinkingOrSanitary> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwCommercialDrinkingOrSanitary>) this._permitWwCommercialDrinkingOrSanitaryRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwCommercialDrinkingOrSanitary> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwCommercialDrinkingOrSanitary>) this._permitWwCommercialDrinkingOrSanitaryRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwCommercialDrinkingOrSanitary entity)
    {
      return this._permitWwCommercialDrinkingOrSanitaryRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwCommercialDrinkingOrSanitaryRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwCommercialDrinkingOrSanitary GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
