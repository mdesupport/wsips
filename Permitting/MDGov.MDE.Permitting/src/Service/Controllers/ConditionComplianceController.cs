﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ConditionComplianceController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ConditionComplianceController : ApiController, IUpdatableService<ConditionCompliance>, IService<ConditionCompliance>
  {
    private readonly IRepository<ConditionCompliance> _conditionComplianceRepository;
    private readonly IValidator<ConditionCompliance> _conditionComplianceValidator;

    public ConditionComplianceController(IRepository<ConditionCompliance> conditionComplianceRepository, IValidator<ConditionCompliance> conditionComplianceValidator)
    {
      this._conditionComplianceRepository = conditionComplianceRepository;
      this._conditionComplianceValidator = conditionComplianceValidator;
    }

    [HttpGet]
    public IEnumerable<ConditionCompliance> GetAll(string includeProperties)
    {
      return (IEnumerable<ConditionCompliance>) this._conditionComplianceRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<ConditionCompliance> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<ConditionCompliance>) this._conditionComplianceRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public ConditionCompliance GetById(int id, string includeProperties)
    {
      return this._conditionComplianceRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._conditionComplianceRepository.Count(filters);
    }

    [HttpPost]
    public int Save(ConditionCompliance entity)
    {
      this._conditionComplianceValidator.ValidateAndThrow<ConditionCompliance>(entity);
      return this._conditionComplianceRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._conditionComplianceRepository.Delete(id);
    }
  }
}
