﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitParcelController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitParcelController : ApiController, IUpdatableService<PermitParcel>, IService<PermitParcel>
  {
    private readonly IRepository<PermitParcel> _permitParcelRepository;

    public PermitParcelController(IRepository<PermitParcel> permitParcelRepository)
    {
      this._permitParcelRepository = permitParcelRepository;
    }

    [HttpGet]
    public IEnumerable<PermitParcel> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitParcel>) this._permitParcelRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitParcel> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitParcel>) this._permitParcelRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitParcel GetById(int id, string includeProperties)
    {
      return this._permitParcelRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._permitParcelRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PermitParcel entity)
    {
      return this._permitParcelRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitParcelRepository.Delete(id);
    }
  }
}
