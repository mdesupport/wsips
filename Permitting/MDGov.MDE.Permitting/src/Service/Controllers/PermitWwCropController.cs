﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwCropController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwCropController : ApiController, IUpdatableService<PermitWwCrop>, IService<PermitWwCrop>
  {
    private readonly IRepository<PermitWwCrop> _permitWwCropRepository;

    public PermitWwCropController(IRepository<PermitWwCrop> permitWwCropRepository)
    {
      this._permitWwCropRepository = permitWwCropRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwCrop> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwCrop>) this._permitWwCropRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwCrop> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwCrop>) this._permitWwCropRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwCrop entity)
    {
      return this._permitWwCropRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwCropRepository.Delete(Convert.ToInt32(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwCrop GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
