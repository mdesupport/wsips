﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ExportDashboardController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ExportDashboardController : ApiController, IService<ExportDashboardPermit>
  {
    private readonly IRepository<Permit> _permitRepository;
    private readonly IRepository<LU_PermitCategory> _permitCategoryRepository;
    private readonly IRepository<LU_PermitStatus> _permitStatusRepository;
    private readonly IRepository<LU_County> _countyRepository;
    private readonly IRepository<LU_Aquifer> _aquiferRepository;
    private readonly IRepository<LU_WaterWithdrawalPurpose> _waterWithdrawalPurposeRepository;
    private readonly DbContext _dbContext;

    public ExportDashboardController(IRepository<Permit> permitRepository, IRepository<LU_PermitCategory> permitCategoryRepository, IRepository<LU_PermitStatus> permitStatusRepository, IRepository<LU_County> countyRepository, IRepository<LU_Aquifer> aquiferRepository, IRepository<LU_WaterWithdrawalPurpose> waterWithdrawalPurposeRepository, DbContext dbContext)
    {
      this._permitRepository = permitRepository;
      this._permitCategoryRepository = permitCategoryRepository;
      this._permitStatusRepository = permitStatusRepository;
      this._countyRepository = countyRepository;
      this._aquiferRepository = aquiferRepository;
      this._waterWithdrawalPurposeRepository = waterWithdrawalPurposeRepository;
      this._dbContext = dbContext;
    }

    [HttpPost]
    public IEnumerable<ExportDashboardPermit> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      IQueryable<LU_PermitCategory> source1 = this._permitCategoryRepository.All((string) null);
      IQueryable<LU_PermitStatus> source2 = this._permitStatusRepository.All((string) null);
      IQueryable<LU_County> source3 = this._countyRepository.All((string) null);
      IQueryable<LU_Aquifer> source4 = this._aquiferRepository.All((string) null);
      IQueryable<LU_WaterWithdrawalPurpose> source5 = this._waterWithdrawalPurposeRepository.All((string) null);
      string str1 = "";
      string str2 = "";
      string str3 = "";
      string str4 = "";
      string str5 = "";
      string str6 = "";
      string str7 = "";
      string str8 = "";
      string str9 = "";
      bool flag = false;
      foreach (DynamicFilter filter in filters)
      {
        if (filter.Predicate.StartsWith("LU_PermitStatus.PermitCategoryId"))
        {
          int permitCatId = Convert.ToInt32(filter.Values[0]);
          str1 = source1.FirstOrDefault<LU_PermitCategory>((Expression<Func<LU_PermitCategory, bool>>) (x => x.Id == permitCatId)).Description;
        }
        else if (filter.Predicate.StartsWith("PermitStatusId"))
        {
          int statusID = Convert.ToInt32(Regex.Match(filter.Predicate, "\\d+").Value);
          str2 = source2.FirstOrDefault<LU_PermitStatus>((Expression<Func<LU_PermitStatus, bool>>) (pc => pc.Id == statusID)).Description;
        }
        else if (filter.Predicate.StartsWith("PermitCounties"))
        {
          foreach (JToken jtoken in (IEnumerable<JToken>) filter.Values[0])
          {
            JToken item1 = jtoken;
            str3 = str3 + source3.FirstOrDefault<LU_County>((Expression<Func<LU_County, bool>>) (x => x.Id == (int) item1)).Description + ", ";
          }
        }
        else if (filter.Predicate.StartsWith("PermitWithdrawalGroundwaters.Any"))
        {
          foreach (JToken jtoken in (IEnumerable<JToken>) filter.Values[0])
          {
            JToken item1 = jtoken;
            str5 = str5 + source4.FirstOrDefault<LU_Aquifer>((Expression<Func<LU_Aquifer, bool>>) (x => x.Id == (int) item1)).Description + ", ";
          }
        }
        else if (filter.Predicate.StartsWith("PermitWaterWithdrawalPurposes"))
        {
          foreach (JToken jtoken in (IEnumerable<JToken>) filter.Values[0])
          {
            JToken item1 = jtoken;
            str4 = str4 + source5.FirstOrDefault<LU_WaterWithdrawalPurpose>((Expression<Func<LU_WaterWithdrawalPurpose, bool>>) (x => x.Id == (int) item1)).Description + ", ";
          }
        }
        else if (filter.Predicate.StartsWith("PermitWithdrawalSurfaceWaters.Count > 0"))
          str6 = "Surface Water";
        else if (filter.Predicate.StartsWith("PermitWithdrawalGroundwaters.Count > 0"))
          str6 = "Groundwater";
        else if (filter.Predicate.Contains("EffectiveDate"))
          str7 = ((IEnumerable<object>) filter.Values).Count<object>() != 1 ? Convert.ToDateTime(filter.Values[0]).ToShortDateString() + " to " + Convert.ToDateTime(filter.Values[1]).ToShortDateString() : Convert.ToDateTime(filter.Values[0]).ToShortDateString();
        else if (filter.Predicate.Contains("ExpirationDate"))
          str8 = ((IEnumerable<object>) filter.Values).Count<object>() != 1 ? Convert.ToDateTime(filter.Values[0]).ToShortDateString() + " to " + Convert.ToDateTime(filter.Values[1]).ToShortDateString() : Convert.ToDateTime(filter.Values[0]).ToShortDateString();
        else if (filter.Predicate.StartsWith("PermitContacts.Any"))
          flag = true;
        else if (filter.Predicate.StartsWith("PermitNumber.Contains"))
          str9 = filter.Values[0].ToString();
      }
      List<ExportDashboardPermit> list = this._dbContext.Database.SqlQuery<ExportDashboardPermit>("exec sp_PermitReport @permitId", (object) new SqlParameter("permitId", (object) string.Join(",", this._permitRepository.GetRange(skip, take, orderBy, filters, includeProperties).Select<Permit, int>((Expression<Func<Permit, int>>) (x => x.Id)).ToList<int>().Select<int, string>((Func<int, string>) (p => p.ToString())).ToArray<string>()))).ToList<ExportDashboardPermit>();
      foreach (ExportDashboardPermit exportDashboardPermit in list)
      {
        exportDashboardPermit.PermitCategory = str1;
        exportDashboardPermit.SelectedPermitStatus = str2;
        exportDashboardPermit.Counties = str3;
        exportDashboardPermit.Aquifers = str5;
        exportDashboardPermit.WaterSource = str6;
        exportDashboardPermit.EffectiveDate = str7;
        exportDashboardPermit.ExpirationDate = str8;
        exportDashboardPermit.WithdrawlPurposes = str4;
        exportDashboardPermit.AssociatedWithMe = flag;
        exportDashboardPermit.SearchText = str9;
      }
      return (IEnumerable<ExportDashboardPermit>) list;
    }

    [HttpPost]
    public int GetReportParameterId(IEnumerable<int> PermitIds)
    {
      return this._dbContext.Database.SqlQuery<int>("exec sp_InsertReportParameter @PermitIds", (object) new SqlParameter("PermitIds", (object) string.Join<int>(",", PermitIds))).SingleOrDefault<int>();
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<ExportDashboardPermit> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }

    public ExportDashboardPermit GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
