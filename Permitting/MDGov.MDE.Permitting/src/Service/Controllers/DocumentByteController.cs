﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.DocumentByteController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class DocumentByteController : ApiController, IUpdatableService<DocumentByte>, IService<DocumentByte>
  {
    private readonly IRepository<DocumentByte> _documentBytetRepository;

    public DocumentByteController(IRepository<DocumentByte> documentBytetRepository)
    {
      this._documentBytetRepository = documentBytetRepository;
    }

    public int Save(DocumentByte doc)
    {
      return this._documentBytetRepository.Save(doc);
    }

    [HttpPost]
    public IEnumerable<DocumentByte> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<DocumentByte>) this._documentBytetRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public DocumentByte GetById(int id, string includeProperties)
    {
      return this._documentBytetRepository.GetById(id, includeProperties);
    }

    public void Delete(int id)
    {
      this._documentBytetRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<DocumentByte> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
