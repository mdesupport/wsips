﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ChecklistQuestionController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ChecklistQuestionController : ApiController, IUpdatableService<CheckListQuestion>, IService<CheckListQuestion>
  {
    private readonly IRepository<CheckListQuestion> _checklistQuestionRepository;

    public ChecklistQuestionController(IRepository<CheckListQuestion> checklistQuestionRepository)
    {
      this._checklistQuestionRepository = checklistQuestionRepository;
    }

    [HttpGet]
    public IEnumerable<CheckListQuestion> GetAll(string includeProperties)
    {
      return (IEnumerable<CheckListQuestion>) this._checklistQuestionRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<CheckListQuestion> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<CheckListQuestion>) this._checklistQuestionRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public CheckListQuestion GetById(int id, string includeProperties)
    {
      return this._checklistQuestionRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._checklistQuestionRepository.Count(filters);
    }

    [HttpPost]
    public int Save(CheckListQuestion entity)
    {
      return this._checklistQuestionRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._checklistQuestionRepository.Delete(id);
    }
  }
}
