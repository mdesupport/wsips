﻿using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
    public class TidalTypeController : ApiController, IService<LU_TidalType>
    {
        private readonly IRepository<LU_TidalType> _tidalTypeRepository;

        public TidalTypeController(IRepository<LU_TidalType> tidalTypeRepository)
        {
            this._tidalTypeRepository = tidalTypeRepository;
        }

        [HttpGet]
        public IEnumerable<LU_TidalType> GetAll(string includeProperties = null)
        {
            return (IEnumerable<LU_TidalType>)this._tidalTypeRepository.All((string)null);
        }

        [HttpGet]
        public LU_TidalType GetById(int id, string includeProperties)
        {
            return this._tidalTypeRepository.GetById(id, includeProperties);
        }

        public int Count(IEnumerable<DynamicFilter> filters)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LU_TidalType> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
        {
            throw new NotImplementedException();
        }
    }
}
