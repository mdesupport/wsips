﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ContactStateController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ContactStateController : ApiController, IService<LU_State>
  {
    private readonly IRepository<LU_State> _contactStateRepository;

    public ContactStateController(IRepository<LU_State> contactStateRepository)
    {
      this._contactStateRepository = contactStateRepository;
    }

    [HttpGet]
    public IEnumerable<LU_State> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_State>) this._contactStateRepository.All(includeProperties).OrderBy<LU_State, string>((Expression<Func<LU_State, string>>) (x => x.Description));
    }

    [HttpPost]
    public IEnumerable<LU_State> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_State>) this._contactStateRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_State GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
