﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWastewaterTreatmentDisposalController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWastewaterTreatmentDisposalController : ApiController, IUpdatableService<PermitWastewaterTreatmentDisposal>, IService<PermitWastewaterTreatmentDisposal>
  {
    private readonly IRepository<PermitWastewaterTreatmentDisposal> _permitWastewaterTreatmentDisposalRepository;

    public PermitWastewaterTreatmentDisposalController(IRepository<PermitWastewaterTreatmentDisposal> permitWastewaterTreatmentDisposalRepository)
    {
      this._permitWastewaterTreatmentDisposalRepository = permitWastewaterTreatmentDisposalRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWastewaterTreatmentDisposal> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWastewaterTreatmentDisposal>) this._permitWastewaterTreatmentDisposalRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWastewaterTreatmentDisposal> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWastewaterTreatmentDisposal>) this._permitWastewaterTreatmentDisposalRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWastewaterTreatmentDisposal entity)
    {
      return this._permitWastewaterTreatmentDisposalRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWastewaterTreatmentDisposalRepository.Delete(Convert.ToInt32(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWastewaterTreatmentDisposal GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
