﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ProjectManagerCountyController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ProjectManagerCountyController : ApiController, IUpdatableService<ProjectManagerCounty>, IService<ProjectManagerCounty>
  {
    private readonly IRepository<ProjectManagerCounty> _projectManagerCountyRepository;

    public ProjectManagerCountyController(IRepository<ProjectManagerCounty> projectManagerCountyRepository)
    {
      this._projectManagerCountyRepository = projectManagerCountyRepository;
    }

    [HttpGet]
    public IEnumerable<ProjectManagerCounty> GetAll(string includeProperties = null)
    {
      return (IEnumerable<ProjectManagerCounty>) this._projectManagerCountyRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<ProjectManagerCounty> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<ProjectManagerCounty>) this._projectManagerCountyRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(ProjectManagerCounty entity)
    {
      return this._projectManagerCountyRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._projectManagerCountyRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public ProjectManagerCounty GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
