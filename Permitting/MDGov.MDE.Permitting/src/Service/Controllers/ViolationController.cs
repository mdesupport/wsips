﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ViolationController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ViolationController : ApiController, IUpdatableService<Violation>, IService<Violation>
  {
    private readonly IRepository<Violation> _violationRepository;
    private readonly IRepository<PermitCounty> _permitCountyRepository;
    private readonly IValidator<Violation> _violationValidator;

    public ViolationController(IRepository<Violation> violationRepository, IRepository<PermitCounty> permitCountyRepository, IValidator<Violation> violationValidator)
    {
      this._violationRepository = violationRepository;
      this._permitCountyRepository = permitCountyRepository;
      this._violationValidator = violationValidator;
    }

    [HttpGet]
    public IEnumerable<Violation> GetAll(string includeProperties)
    {
      return (IEnumerable<Violation>) this._violationRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Violation> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Violation>) this._violationRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public Violation GetById(int id, string includeProperties)
    {
      return this._violationRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(Violation entity)
    {
      if (entity.Id == 0)
        entity.FileNumber = this.GenerateFileNumber(entity.PermitId);
      this._violationValidator.ValidateAndThrow<Violation>(entity);
      return this._violationRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._violationRepository.Delete(id);
    }

    [HttpGet]
    private string GenerateFileNumber(int permitId)
    {
      string str1 = DateTime.Today.Year.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      List<Violation> list = this._violationRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
      {
        new DynamicFilter("FileNumber.StartsWith(@0)", new object[1]
        {
          (object) str1
        })
      }, (string) null).ToList<Violation>();
      int num = 0;
      if (list.Count > 0)
        num = list.Max<Violation>((Func<Violation, int>) (t =>
        {
          if (string.IsNullOrEmpty(t.FileNumber))
            return 0;
          return int.Parse(t.FileNumber.Split('-')[1]);
        }));
      string str2 = (num + 1).ToString("D3");
      return string.Format("{0}-{1}", (object) str1, (object) str2);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
