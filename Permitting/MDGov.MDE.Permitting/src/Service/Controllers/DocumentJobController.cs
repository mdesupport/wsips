﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.DocumentJobController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class DocumentJobController : ApiController, IUpdatableService<DocumentJob>, IService<DocumentJob>
  {
    private readonly IRepository<DocumentJob> _documentJobRepository;

    public DocumentJobController(IRepository<DocumentJob> documentJobRepository)
    {
      this._documentJobRepository = documentJobRepository;
    }

    [HttpPost]
    public int Save(DocumentJob job)
    {
      return this._documentJobRepository.Save(job);
    }

    [HttpPost]
    public IEnumerable<DocumentJob> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<DocumentJob>) this._documentJobRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public DocumentJob GetById(int id, string includeProperties)
    {
      return this._documentJobRepository.GetById(id, includeProperties);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._documentJobRepository.Delete(id);
    }

    [HttpGet]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    [HttpGet]
    public IEnumerable<DocumentJob> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
