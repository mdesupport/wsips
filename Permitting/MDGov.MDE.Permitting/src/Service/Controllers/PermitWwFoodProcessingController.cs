﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwFoodProcessingController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwFoodProcessingController : ApiController, IUpdatableService<PermitWwFoodProcessing>, IService<PermitWwFoodProcessing>
  {
    private readonly IRepository<PermitWwFoodProcessing> _permitWwFoodProcessing;

    public PermitWwFoodProcessingController(IRepository<PermitWwFoodProcessing> permitWwFoodProcessing)
    {
      this._permitWwFoodProcessing = permitWwFoodProcessing;
    }

    [HttpGet]
    public IEnumerable<PermitWwFoodProcessing> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwFoodProcessing>) this._permitWwFoodProcessing.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwFoodProcessing> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwFoodProcessing>) this._permitWwFoodProcessing.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwFoodProcessing entity)
    {
      return this._permitWwFoodProcessing.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwFoodProcessing.Delete(Convert.ToInt32(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwFoodProcessing GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
