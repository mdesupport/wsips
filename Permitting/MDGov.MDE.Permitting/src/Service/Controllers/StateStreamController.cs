﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.StateStreamController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class StateStreamController : ApiController, IService<LU_StateStream>
  {
    private readonly IRepository<LU_StateStream> _stateStreamRepository;

    public StateStreamController(IRepository<LU_StateStream> stateStreamRepository)
    {
      this._stateStreamRepository = stateStreamRepository;
    }

    [HttpGet]
    public IEnumerable<LU_StateStream> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_StateStream>) this._stateStreamRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_StateStream> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_StateStream>) this._stateStreamRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public IEnumerable<LU_StateStream> GetByWatershed(double x, double y, int srid)
    {
        var geom = DbGeometry.PointFromText(string.Format("POINT({0} {1})", x, y), srid);

        var streams = _stateStreamRepository.All().Where(ss => ss.LU_Watershed.Shape.Intersects(geom) && !ss.LU_Watershed.Active);
        var relatedStreams = _stateStreamRepository.All().Where(ss => streams.Any(st => st.Id == ss.ReceivingTributaryId || st.ReceivingTributaryId == ss.Id));

        return streams.Union(relatedStreams);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_StateStream GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
