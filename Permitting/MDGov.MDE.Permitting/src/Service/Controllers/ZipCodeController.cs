﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ZipCodeController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ZipCodeController : ApiController, IUpdatableService<LU_ZipCode>, IService<LU_ZipCode>
  {
    private readonly ModelContext _context;
    private readonly IRepository<LU_ZipCode> _zipCodeRepository;

    public ZipCodeController(ModelContext context, IRepository<LU_ZipCode> zipCodeRepository)
    {
      this._context = context;
      this._zipCodeRepository = zipCodeRepository;
    }

    [HttpGet]
    public IEnumerable<LU_ZipCode> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_ZipCode>) this._zipCodeRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_ZipCode> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      return (IEnumerable<LU_ZipCode>) this._zipCodeRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public LU_ZipCode GetById(int id, string includeProperties = null)
    {
      return this._zipCodeRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(LU_ZipCode entity)
    {
      throw new NotImplementedException();
    }

    [HttpDelete]
    public void Delete(int id)
    {
      throw new NotImplementedException();
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
