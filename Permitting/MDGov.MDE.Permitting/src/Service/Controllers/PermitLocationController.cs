﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitLocationController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Permitting.Model;
using System;
using System.Data.Objects;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitLocationController : ApiController
  {
    private readonly ModelContext _context;

    public PermitLocationController(ModelContext context)
    {
      this._context = context;
    }

    [HttpPost]
    public int Save(PermitLocation entity)
    {
      return this._context.sp_AddPermitLocation(new int?(entity.PermitId), new double?(entity.WithdrawalLocationX), new double?(entity.WithdrawalLocationY), new int?(entity.SpatialReferenceId), entity.LastModifiedBy, new bool?(false), new ObjectParameter("countyId", (object) DBNull.Value), new ObjectParameter("permitLocationOID", (object) DBNull.Value), new ObjectParameter("taxMapNumber", (object) DBNull.Value), new ObjectParameter("taxMapBlockNumber", (object) DBNull.Value), new ObjectParameter("taxMapSubBlockNumber", (object) DBNull.Value), new ObjectParameter("taxMapParcel", (object) DBNull.Value), new ObjectParameter("lotNumber", (object) DBNull.Value), new ObjectParameter("parcelStreet", (object) DBNull.Value), new ObjectParameter("parcelCity", (object) DBNull.Value), new ObjectParameter("parcelState", (object) DBNull.Value), new ObjectParameter("parcelZip", (object) DBNull.Value)).FirstOrDefault<int?>().GetValueOrDefault();
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._context.sp_RemovePermitLocation(new int?(id));
    }
  }
}
