﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PumpageReportTypeController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PumpageReportTypeController : ApiController, IService<LU_PumpageReportType>
  {
    private readonly IRepository<LU_PumpageReportType> _pumpageReportTypeRepository;

    public PumpageReportTypeController(IRepository<LU_PumpageReportType> pumpageReportTypeRepository)
    {
      this._pumpageReportTypeRepository = pumpageReportTypeRepository;
    }

    [HttpGet]
    public IEnumerable<LU_PumpageReportType> GetAll(string includeProperties)
    {
      return (IEnumerable<LU_PumpageReportType>) this._pumpageReportTypeRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_PumpageReportType> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_PumpageReportType>) this._pumpageReportTypeRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_PumpageReportType GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
