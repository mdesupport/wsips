﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwPrivateWaterSupplierController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwPrivateWaterSupplierController : ApiController, IUpdatableService<PermitWwPrivateWaterSupplier>, IService<PermitWwPrivateWaterSupplier>
  {
    private readonly IRepository<PermitWwPrivateWaterSupplier> _permitWaterWithdrawalPrivateWaterSupplierRepository;

    public PermitWwPrivateWaterSupplierController(IRepository<PermitWwPrivateWaterSupplier> permitWaterWithdrawalPrivateWaterSupplierRepository)
    {
      this._permitWaterWithdrawalPrivateWaterSupplierRepository = permitWaterWithdrawalPrivateWaterSupplierRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwPrivateWaterSupplier> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwPrivateWaterSupplier>) this._permitWaterWithdrawalPrivateWaterSupplierRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwPrivateWaterSupplier> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwPrivateWaterSupplier>) this._permitWaterWithdrawalPrivateWaterSupplierRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwPrivateWaterSupplier entity)
    {
      return this._permitWaterWithdrawalPrivateWaterSupplierRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWaterWithdrawalPrivateWaterSupplierRepository.Delete(Convert.ToInt32(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwPrivateWaterSupplier GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
