﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.SupplementalGroupController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class SupplementalGroupController : ApiController, IUpdatableService<SupplementalGroup>, IService<SupplementalGroup>
  {
    private readonly IRepository<SupplementalGroup> _supplementalGroupRepository;

    public SupplementalGroupController(IRepository<SupplementalGroup> supplementalGroupRepository)
    {
      this._supplementalGroupRepository = supplementalGroupRepository;
    }

    [HttpGet]
    public SupplementalGroup GetById(int id, string includeProperties)
    {
      return this._supplementalGroupRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public IEnumerable<SupplementalGroup> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<SupplementalGroup>) this._supplementalGroupRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(SupplementalGroup entity)
    {
      return this._supplementalGroupRepository.Save(entity);
    }

    public void Delete(int id)
    {
      this._supplementalGroupRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<SupplementalGroup> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
