﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitContactController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitContactController : ApiController, IUpdatableService<PermitContact>, IService<PermitContact>
  {
    private readonly IRepository<PermitContact> _permitContactRepository;
    private readonly IValidator<PermitContact> _permitContactValidator;

    public PermitContactController(IRepository<PermitContact> permitContactRepository, IValidator<PermitContact> permitContactValidator)
    {
      this._permitContactRepository = permitContactRepository;
      this._permitContactValidator = permitContactValidator;
    }

    [HttpGet]
    public IEnumerable<PermitContact> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitContact>) this._permitContactRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitContact> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitContact>) this._permitContactRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitContact GetById(int id, string includeProperties)
    {
      return this._permitContactRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._permitContactRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PermitContact entity)
    {
      this._permitContactValidator.ValidateAndThrow<PermitContact>(entity);
      return this._permitContactRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitContactRepository.Delete(id);
    }
  }
}
