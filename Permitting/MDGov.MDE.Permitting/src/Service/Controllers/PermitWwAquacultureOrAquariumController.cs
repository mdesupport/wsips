﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwAquacultureOrAquariumController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwAquacultureOrAquariumController : ApiController, IUpdatableService<PermitWwAquacultureOrAquarium>, IService<PermitWwAquacultureOrAquarium>
  {
    private readonly IRepository<PermitWwAquacultureOrAquarium> _permitWwAquacultureOrAquarium;

    public PermitWwAquacultureOrAquariumController(IRepository<PermitWwAquacultureOrAquarium> permitWwAquacultureOrAquarium)
    {
      this._permitWwAquacultureOrAquarium = permitWwAquacultureOrAquarium;
    }

    [HttpGet]
    public IEnumerable<PermitWwAquacultureOrAquarium> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwAquacultureOrAquarium>) this._permitWwAquacultureOrAquarium.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwAquacultureOrAquarium> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwAquacultureOrAquarium>) this._permitWwAquacultureOrAquarium.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwAquacultureOrAquarium entity)
    {
      return this._permitWwAquacultureOrAquarium.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwAquacultureOrAquarium.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwAquacultureOrAquarium GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
