﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.DivisionChiefController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class DivisionChiefController : ApiController, IUpdatableService<DivisionChief>, IService<DivisionChief>
  {
    private readonly IRepository<DivisionChief> _divisionChiefRepository;

    public DivisionChiefController(IRepository<DivisionChief> divisionChiefRepository)
    {
      this._divisionChiefRepository = divisionChiefRepository;
    }

    [HttpGet]
    public IEnumerable<DivisionChief> GetAll()
    {
      return (IEnumerable<DivisionChief>) this._divisionChiefRepository.All((string) null);
    }

    [HttpPost]
    public IEnumerable<DivisionChief> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<DivisionChief>) this._divisionChiefRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(DivisionChief entity)
    {
      return this._divisionChiefRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._divisionChiefRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<DivisionChief> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }

    public DivisionChief GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
