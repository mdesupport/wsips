﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitHearingController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitHearingController : ApiController, IUpdatableService<PermitHearing>, IService<PermitHearing>
  {
    private readonly IRepository<PermitHearing> _permitHearingRepository;

    public PermitHearingController(IRepository<PermitHearing> permitHearingRepository)
    {
      this._permitHearingRepository = permitHearingRepository;
    }

    [HttpGet]
    public PermitHearing GetById(int id, string includeProperties)
    {
      return this._permitHearingRepository.GetById(id, includeProperties);
    }

    public int Save(PermitHearing permitHearing)
    {
      return this._permitHearingRepository.Save(permitHearing);
    }

    public void Delete(int id)
    {
      this._permitHearingRepository.Delete(id);
    }

    [HttpPost]
    public IEnumerable<PermitHearing> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitHearing>) this._permitHearingRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<PermitHearing> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
