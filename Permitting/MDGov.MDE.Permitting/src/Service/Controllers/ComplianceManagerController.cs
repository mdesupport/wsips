﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ComplianceManagerController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ComplianceManagerController : ApiController, IUpdatableService<ComplianceManager>, IService<ComplianceManager>
  {
    private readonly IRepository<ComplianceManager> _complianceManagerRepository;

    public ComplianceManagerController(IRepository<ComplianceManager> complianceManagerRepository)
    {
      this._complianceManagerRepository = complianceManagerRepository;
    }

    [HttpGet]
    public IEnumerable<ComplianceManager> GetAll(string includeProperties)
    {
      return (IEnumerable<ComplianceManager>) this._complianceManagerRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<ComplianceManager> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<ComplianceManager>) this._complianceManagerRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public ComplianceManager GetById(int id, string includeProperties)
    {
      return this._complianceManagerRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(ComplianceManager entity)
    {
      return this._complianceManagerRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._complianceManagerRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
