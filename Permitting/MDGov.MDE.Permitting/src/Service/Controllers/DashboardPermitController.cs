﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.DashboardPermitController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class DashboardPermitController : ApiController
  {
    private ModelContext _context;

    public DashboardPermitController()
    {
      this._context = new ModelContext();
    }

    [HttpPost]
    public DashboardSearchResult GetRange(int skip, int take, string orderBy, DashboardFilterWrapper filterWrapper)
    {
        IQueryable<Permit> queryable1 = this._context.Permits.AsQueryable<Permit>();

        if (filterWrapper.FailedToReportTwoPeriods)
            queryable1 = queryable1.Join<Permit, PermitsFailedToReportTwoPeriod, int, Permit>((IEnumerable<PermitsFailedToReportTwoPeriod>) this._context.PermitsFailedToReportTwoPeriods, (Expression<Func<Permit, int>>) (p => p.Id), (Expression<Func<PermitsFailedToReportTwoPeriod, int>>) (p2 => p2.Id), (Expression<Func<Permit, PermitsFailedToReportTwoPeriod, Permit>>) ((p, p2) => p));

        if (filterWrapper.Filters != null)
        {
            foreach (DynamicFilter dynamicFilter in filterWrapper.Filters.Where<DynamicFilter>((Func<DynamicFilter, bool>) (f => !string.IsNullOrEmpty(f.Predicate))))
                queryable1 = queryable1.Where<Permit>(dynamicFilter.Predicate, dynamicFilter.Values);
        }

        if (filterWrapper.SpatialFilters != null)
            queryable1 = filterWrapper.SpatialFilters.Apply(queryable1);

        IQueryable<DashboardPermit> source1 = queryable1.Join<Permit, DashboardPermit, int, DashboardPermit>((IEnumerable<DashboardPermit>)this._context.DashboardPermits, (Expression<Func<Permit, int>>)(p => p.Id), (Expression<Func<DashboardPermit, int>>)(dp => dp.Id), (Expression<Func<Permit, DashboardPermit, DashboardPermit>>)((p, dp) => dp));
        IQueryable<int> queryable2 = source1.Select(p => p.Id);

        if (string.IsNullOrEmpty(orderBy))
            orderBy = "Id";
        IQueryable<DashboardPermit> source2 = source1.OrderBy(orderBy);

        if (skip > 0)
            source2 = source2.Skip(skip);
        if (take < int.MaxValue)
            source2 = source2.Take<DashboardPermit>(take);

        return new DashboardSearchResult()
        {
            PermitIds = queryable2.AsEnumerable(),
            Feature = this.GetSpatialFilterFeature(filterWrapper.SpatialFilters),
            DashboardPermits = source2.AsEnumerable()
        };
    }

    [HttpGet]
    public DashboardPermit GetById(int id)
    {
      return this._context.DashboardPermits.SingleOrDefault<DashboardPermit>((Expression<Func<DashboardPermit, bool>>) (p => p.Id == id));
    }

    private DbGeometryWellKnownValue GetSpatialFilterFeature(DashboardSpatialFilters dashboardSpatialFilters)
    {
      if (dashboardSpatialFilters == null)
          return (DbGeometryWellKnownValue)null;

      DbGeometry dbGeometry = (DbGeometry) null;
      PermitDistanceFilter permitDistanceFilter = dashboardSpatialFilters.PermitDistanceFilter;

      if (permitDistanceFilter != null)
        dbGeometry = permitDistanceFilter.GetGeometry();

      PermitFeatureFilter permitFeatureFilter = dashboardSpatialFilters.PermitFeatureFilter;
      if (permitFeatureFilter != null)
        dbGeometry = dbGeometry != null ? dbGeometry.Union(permitFeatureFilter.GetGeometry()) : permitFeatureFilter.GetGeometry();

      if (dbGeometry != null)
        return dbGeometry.WellKnownValue;

        return (DbGeometryWellKnownValue)null;
    }
  }
}
