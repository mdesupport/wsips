﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWithdrawalSurfacewaterDetailController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWithdrawalSurfacewaterDetailController : ApiController, IUpdatableService<PermitWithdrawalSurfacewaterDetail>, IService<PermitWithdrawalSurfacewaterDetail>
  {
    private readonly IRepository<PermitWithdrawalSurfacewater> _permitWithdrawalSurfacewaterRepository;
    private readonly IRepository<PermitWithdrawalSurfacewaterDetail> _permitWithdrawalSurfacewaterDetailRepository;

    public PermitWithdrawalSurfacewaterDetailController(IRepository<PermitWithdrawalSurfacewater> permitWithdrawalSurfacewaterRepository, IRepository<PermitWithdrawalSurfacewaterDetail> permitWithdrawalSurfacewaterDetailRepository)
    {
      this._permitWithdrawalSurfacewaterRepository = permitWithdrawalSurfacewaterRepository;
      this._permitWithdrawalSurfacewaterDetailRepository = permitWithdrawalSurfacewaterDetailRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWithdrawalSurfacewaterDetail> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalSurfacewaterDetail>) this._permitWithdrawalSurfacewaterDetailRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWithdrawalSurfacewaterDetail> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWithdrawalSurfacewaterDetail>) this._permitWithdrawalSurfacewaterDetailRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitWithdrawalSurfacewaterDetail GetById(int id, string includeProperties = null)
    {
      return this._permitWithdrawalSurfacewaterDetailRepository.GetById(id, includeProperties);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      PermitWithdrawalSurfacewaterDetail byId = this._permitWithdrawalSurfacewaterDetailRepository.GetById(id, (string) null);
      if (byId == null)
        return;
      if (byId.PermitWithdrawalSurfacewater.PermitWithdrawalSurfacewaterDetails.Count == 1)
        this._permitWithdrawalSurfacewaterRepository.Delete(byId.PermitWithdrawalSurfacewaterId);
      else
        this._permitWithdrawalSurfacewaterDetailRepository.Delete(id);
    }

    [HttpPost]
    public int Save(PermitWithdrawalSurfacewaterDetail entity)
    {
      return this._permitWithdrawalSurfacewaterDetailRepository.Save(entity);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
