﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.AdministrativeSpecialistController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class AdministrativeSpecialistController : ApiController, IUpdatableService<AdministrativeSpecialist>, IService<AdministrativeSpecialist>
  {
    private readonly IRepository<AdministrativeSpecialist> _administrativeSpecialistRepository;

    public AdministrativeSpecialistController(IRepository<AdministrativeSpecialist> administrativeSpecialistRepository)
    {
      this._administrativeSpecialistRepository = administrativeSpecialistRepository;
    }

    [HttpGet]
    public IEnumerable<AdministrativeSpecialist> GetAll(string includeProperties)
    {
      return (IEnumerable<AdministrativeSpecialist>) this._administrativeSpecialistRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<AdministrativeSpecialist> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<AdministrativeSpecialist>) this._administrativeSpecialistRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public AdministrativeSpecialist GetById(int id, string includeProperties)
    {
      return this._administrativeSpecialistRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(AdministrativeSpecialist entity)
    {
      return this._administrativeSpecialistRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._administrativeSpecialistRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
