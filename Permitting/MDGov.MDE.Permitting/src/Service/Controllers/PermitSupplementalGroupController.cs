﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitSupplementalGroupController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitSupplementalGroupController : ApiController, IUpdatableService<PermitSupplementalGroup>, IService<PermitSupplementalGroup>
  {
    private readonly IRepository<PermitSupplementalGroup> _permitSupplementalGroupRepository;

    public PermitSupplementalGroupController(IRepository<PermitSupplementalGroup> permitSupplementalGroupRepository)
    {
      this._permitSupplementalGroupRepository = permitSupplementalGroupRepository;
    }

    [HttpGet]
    public PermitSupplementalGroup GetById(int id, string includeProperties)
    {
      return this._permitSupplementalGroupRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitSupplementalGroup> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitSupplementalGroup>) this._permitSupplementalGroupRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitSupplementalGroup entity)
    {
      return this._permitSupplementalGroupRepository.Save(entity);
    }

    public void Delete(int id)
    {
      this._permitSupplementalGroupRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<PermitSupplementalGroup> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
