﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.CommentController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class CommentController : ApiController, IUpdatableService<Comment>, IService<Comment>
  {
    private readonly IRepository<Comment> _commentRepository;

    public CommentController(IRepository<Comment> commentRepository)
    {
      this._commentRepository = commentRepository;
    }

    public int Save(Comment comment)
    {
      return this._commentRepository.Save(comment);
    }

    [HttpPost]
    public IEnumerable<Comment> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Comment>) this._commentRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public void Delete(int id)
    {
      this._commentRepository.Delete(id);
    }

    [HttpGet]
    public Comment GetById(int id, string includeProperties)
    {
      return this._commentRepository.GetById(id, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<Comment> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
