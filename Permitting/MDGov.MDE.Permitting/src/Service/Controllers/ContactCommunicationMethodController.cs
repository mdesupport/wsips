﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ContactCommunicationMethodController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ContactCommunicationMethodController : ApiController, IUpdatableService<ContactCommunicationMethod>, IService<ContactCommunicationMethod>
  {
    private readonly IRepository<ContactCommunicationMethod> _contactCommunicationMethodRepository;
    private readonly IValidator<ContactCommunicationMethod> _contactCommunicationMethodValidator;

    public ContactCommunicationMethodController(IRepository<ContactCommunicationMethod> contactCommunicationMethodRepository, IValidator<ContactCommunicationMethod> contactCommunicationMethodValidator)
    {
      this._contactCommunicationMethodRepository = contactCommunicationMethodRepository;
      this._contactCommunicationMethodValidator = contactCommunicationMethodValidator;
    }

    [HttpGet]
    public IEnumerable<ContactCommunicationMethod> GetAll(string includeProperties = null)
    {
      return (IEnumerable<ContactCommunicationMethod>) this._contactCommunicationMethodRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<ContactCommunicationMethod> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      return (IEnumerable<ContactCommunicationMethod>) this._contactCommunicationMethodRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(ContactCommunicationMethod entity)
    {
      this._contactCommunicationMethodValidator.ValidateAndThrow<ContactCommunicationMethod>(entity);
      if (entity.IsPrimaryEmail.HasValue)
      {
        int num = 3;
        IQueryable<ContactCommunicationMethod> range = this._contactCommunicationMethodRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("ContactId == @0 && CommunicationMethodId == @1", new object[2]
          {
            (object) entity.ContactId,
            (object) num
          })
        }, (string) null);
        if (range.Any<ContactCommunicationMethod>())
        {
          if (entity.IsPrimaryEmail.Value)
          {
            foreach (ContactCommunicationMethod entity1 in range.ToList<ContactCommunicationMethod>())
            {
              entity1.IsPrimaryEmail = new bool?(false);
              this._contactCommunicationMethodRepository.Save(entity1);
            }
          }
        }
        else
          entity.IsPrimaryEmail = new bool?(true);
      }
      else if (entity.IsPrimaryPhone.HasValue)
      {
        List<int> intList = new List<int>()
        {
          1,
          2,
          4,
          5
        };
        IQueryable<ContactCommunicationMethod> range = this._contactCommunicationMethodRepository.GetRange(0, int.MaxValue, (string) null, (IEnumerable<DynamicFilter>) new DynamicFilter[1]
        {
          new DynamicFilter("ContactId == @0 && @1.Contains(outerIt.CommunicationMethodId)", new object[2]
          {
            (object) entity.ContactId,
            (object) intList
          })
        }, (string) null);
        if (range.Any<ContactCommunicationMethod>())
        {
          if (entity.IsPrimaryPhone.Value)
          {
            foreach (ContactCommunicationMethod entity1 in range.ToList<ContactCommunicationMethod>())
            {
              entity1.IsPrimaryPhone = new bool?(false);
              this._contactCommunicationMethodRepository.Save(entity1);
            }
          }
        }
        else
          entity.IsPrimaryPhone = new bool?(true);
      }
      return this._contactCommunicationMethodRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._contactCommunicationMethodRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public ContactCommunicationMethod GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
