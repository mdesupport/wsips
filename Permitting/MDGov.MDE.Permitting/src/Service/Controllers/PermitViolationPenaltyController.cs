﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitViolationPenaltyController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitViolationPenaltyController : ApiController, IUpdatableService<PermitViolationPenalty>, IService<PermitViolationPenalty>
  {
    private readonly IRepository<PermitViolationPenalty> _permitViolationPenaltyRepository;

    public PermitViolationPenaltyController(IRepository<PermitViolationPenalty> permitViolationPenaltyRepository)
    {
      this._permitViolationPenaltyRepository = permitViolationPenaltyRepository;
    }

    [HttpGet]
    public IEnumerable<PermitViolationPenalty> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitViolationPenalty>) this._permitViolationPenaltyRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitViolationPenalty> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitViolationPenalty>) this._permitViolationPenaltyRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitViolationPenalty GetById(int id, string includeProperties)
    {
      return this._permitViolationPenaltyRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._permitViolationPenaltyRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PermitViolationPenalty entity)
    {
      return this._permitViolationPenaltyRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitViolationPenaltyRepository.Delete(id);
    }
  }
}
