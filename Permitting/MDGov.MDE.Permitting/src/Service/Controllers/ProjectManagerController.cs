﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ProjectManagerController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ProjectManagerController : ApiController, IUpdatableService<ProjectManager>, IService<ProjectManager>
  {
    private readonly IRepository<ProjectManager> _projectManagerRepository;

    public ProjectManagerController(IRepository<ProjectManager> projectManagerRepository)
    {
      this._projectManagerRepository = projectManagerRepository;
    }

    [HttpGet]
    public IEnumerable<ProjectManager> GetAll(string includeProperties)
    {
      return (IEnumerable<ProjectManager>) this._projectManagerRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<ProjectManager> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<ProjectManager>) this._projectManagerRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public ProjectManager GetById(int id, string includeProperties)
    {
      return this._projectManagerRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(ProjectManager entity)
    {
      return this._projectManagerRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._projectManagerRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
