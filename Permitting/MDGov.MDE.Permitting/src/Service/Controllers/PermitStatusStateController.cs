﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitStatusStateController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitStatusStateController : ApiController, IUpdatableService<PermitStatusState>, IService<PermitStatusState>
  {
    private readonly IRepository<PermitStatusState> _permitStatusStateService;

    public PermitStatusStateController(IRepository<PermitStatusState> permitStatusStateService)
    {
      this._permitStatusStateService = permitStatusStateService;
    }

    [HttpGet]
    public PermitStatusState GetById(int id, string includeProperties)
    {
      return this._permitStatusStateService.GetById(id, includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitStatusState> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitStatusState>) this._permitStatusStateService.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitStatusState entity)
    {
      return this._permitStatusStateService.Save(entity);
    }

    public void Delete(int id)
    {
      throw new NotImplementedException();
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<PermitStatusState> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
