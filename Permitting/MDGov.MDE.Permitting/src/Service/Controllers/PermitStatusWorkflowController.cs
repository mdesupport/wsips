﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitStatusWorkflowController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitStatusWorkflowController : ApiController, IUpdatableService<PermitStatusWorkflow>, IService<PermitStatusWorkflow>
  {
    private readonly IRepository<PermitStatusWorkflow> _permitStatusWorkflowRepository;

    public PermitStatusWorkflowController(IRepository<PermitStatusWorkflow> permitStatusWorkflowRepository)
    {
      this._permitStatusWorkflowRepository = permitStatusWorkflowRepository;
    }

    [HttpGet]
    public IEnumerable<PermitStatusWorkflow> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitStatusWorkflow>) this._permitStatusWorkflowRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitStatusWorkflow> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitStatusWorkflow>) this._permitStatusWorkflowRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PermitStatusWorkflow GetById(int id, string includeProperties)
    {
      return this._permitStatusWorkflowRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._permitStatusWorkflowRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PermitStatusWorkflow entity)
    {
      return this._permitStatusWorkflowRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitStatusWorkflowRepository.Delete(id);
    }
  }
}
