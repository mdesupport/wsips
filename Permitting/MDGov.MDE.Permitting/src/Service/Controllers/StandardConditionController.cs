﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.StandardConditionController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using FluentValidation;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class StandardConditionController : ApiController, IUpdatableService<LU_StandardCondition>, IService<LU_StandardCondition>
  {
    private readonly IRepository<LU_StandardCondition> _standardConditionRepository;
    private readonly IValidator<LU_StandardCondition> _standardConditionValidator;

    public StandardConditionController(IRepository<LU_StandardCondition> standardConditionRepository, IValidator<LU_StandardCondition> standardConditionValidator)
    {
      this._standardConditionRepository = standardConditionRepository;
      this._standardConditionValidator = standardConditionValidator;
    }

    [HttpGet]
    public IEnumerable<LU_StandardCondition> GetAll(string includeProperties)
    {
      return (IEnumerable<LU_StandardCondition>) this._standardConditionRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_StandardCondition> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_StandardCondition>) this._standardConditionRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public LU_StandardCondition GetById(int id, string includeProperties)
    {
      return this._standardConditionRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(LU_StandardCondition entity)
    {
      this._standardConditionValidator.ValidateAndThrow<LU_StandardCondition>(entity);
      return this._standardConditionRepository.Save(entity);
    }

    public void Delete(int id)
    {
      throw new NotImplementedException();
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._standardConditionRepository.GetRange(0, int.MaxValue, (string) null, filters, (string) null).Count<LU_StandardCondition>();
    }
  }
}
