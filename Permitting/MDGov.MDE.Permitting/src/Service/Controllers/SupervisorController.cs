﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.SupervisorController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class SupervisorController : ApiController, IUpdatableService<Supervisor>, IService<Supervisor>
  {
    private readonly IRepository<Supervisor> _supervisorRepository;

    public SupervisorController(IRepository<Supervisor> supervisorRepository)
    {
      this._supervisorRepository = supervisorRepository;
    }

    [HttpGet]
    public IEnumerable<Supervisor> GetAll(string includeProperties = null)
    {
      return (IEnumerable<Supervisor>) this._supervisorRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Supervisor> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Supervisor>) this._supervisorRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(Supervisor entity)
    {
      return this._supervisorRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._supervisorRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public Supervisor GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
