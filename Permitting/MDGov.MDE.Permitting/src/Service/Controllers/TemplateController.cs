﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.TemplateController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class TemplateController : ApiController, IService<LU_Template>
  {
    private readonly IRepository<LU_Template> _contactTypeRepository;

    public TemplateController(IRepository<LU_Template> contactRepository)
    {
      this._contactTypeRepository = contactRepository;
    }

    [HttpGet]
    public IEnumerable<LU_Template> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_Template>) this._contactTypeRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_Template> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_Template>) this._contactTypeRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_Template GetById(int id, string includeProperties = null)
    {
      return this._contactTypeRepository.GetById(id, includeProperties);
    }
  }
}
