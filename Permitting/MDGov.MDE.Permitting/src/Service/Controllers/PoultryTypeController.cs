﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PoultryTypeController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PoultryTypeController : ApiController, IService<LU_PoultryType>
  {
    private readonly IRepository<LU_PoultryType> _poultryTypeRepository;

    public PoultryTypeController(IRepository<LU_PoultryType> poultryTypeRepository)
    {
      this._poultryTypeRepository = poultryTypeRepository;
    }

    [HttpGet]
    public IEnumerable<LU_PoultryType> GetAll(string includeProperties = null)
    {
      return (IEnumerable<LU_PoultryType>) this._poultryTypeRepository.All(includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_PoultryType GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<LU_PoultryType> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
