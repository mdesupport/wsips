﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwFarmPortableSupplyController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwFarmPortableSupplyController : ApiController, IUpdatableService<PermitWwFarmPortableSupply>, IService<PermitWwFarmPortableSupply>
  {
    private readonly IRepository<PermitWwFarmPortableSupply> _permitWwFarmPortableSupply;

    public PermitWwFarmPortableSupplyController(IRepository<PermitWwFarmPortableSupply> permitWwFarmPortableSupply)
    {
      this._permitWwFarmPortableSupply = permitWwFarmPortableSupply;
    }

    [HttpGet]
    public IEnumerable<PermitWwFarmPortableSupply> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwFarmPortableSupply>) this._permitWwFarmPortableSupply.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwFarmPortableSupply> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwFarmPortableSupply>) this._permitWwFarmPortableSupply.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwFarmPortableSupply entity)
    {
      return this._permitWwFarmPortableSupply.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwFarmPortableSupply.Delete(Convert.ToInt32(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwFarmPortableSupply GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
