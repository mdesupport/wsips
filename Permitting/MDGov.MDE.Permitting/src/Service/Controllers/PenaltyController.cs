﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PenaltyController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PenaltyController : ApiController, IUpdatableService<Penalty>, IService<Penalty>
  {
    private readonly IRepository<Penalty> _penaltyRepository;

    public PenaltyController(IRepository<Penalty> penaltyRepository)
    {
      this._penaltyRepository = penaltyRepository;
    }

    [HttpGet]
    public IEnumerable<Penalty> GetAll(string includeProperties)
    {
      return (IEnumerable<Penalty>) this._penaltyRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<Penalty> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<Penalty>) this._penaltyRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public Penalty GetById(int id, string includeProperties)
    {
      return this._penaltyRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._penaltyRepository.Count(filters);
    }

    [HttpPost]
    public int Save(Penalty entity)
    {
      return this._penaltyRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._penaltyRepository.Delete(id);
    }
  }
}
