﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PumpageReportController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PumpageReportController : ApiController, IUpdatableService<PumpageReport>, IService<PumpageReport>
  {
    private readonly IRepository<PumpageReport> _pumpageReportRepository;

    public PumpageReportController(IRepository<PumpageReport> pumpageReportRepository)
    {
      this._pumpageReportRepository = pumpageReportRepository;
    }

    [HttpGet]
    public IEnumerable<PumpageReport> GetAll(string includeProperties)
    {
      return (IEnumerable<PumpageReport>) this._pumpageReportRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PumpageReport> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PumpageReport>) this._pumpageReportRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public PumpageReport GetById(int id, string includeProperties)
    {
      return this._pumpageReportRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._pumpageReportRepository.Count(filters);
    }

    [HttpPost]
    public int Save(PumpageReport entity)
    {
      int id = this._pumpageReportRepository.Save(entity);
      entity = this._pumpageReportRepository.GetById(id, "ConditionCompliance.PermitCondition");
      this.CalculateConditionComplianceOverages(entity.ConditionCompliance.PermitCondition.PermitId, new int?(entity.ConditionCompliance.ComplianceReportingStartDate.Year));
      return id;
    }

    private void CalculateConditionComplianceOverages(int? permitId, int? year)
    {
      new ModelContext().sp_CalculateConditionComplianceValues(permitId, year);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._pumpageReportRepository.Delete(id);
    }
  }
}
