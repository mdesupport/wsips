﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ComplianceStaffController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ComplianceStaffController : ApiController, IUpdatableService<ComplianceStaff>, IService<ComplianceStaff>
  {
    private readonly IRepository<ComplianceStaff> _complianceStaffRepository;

    public ComplianceStaffController(IRepository<ComplianceStaff> complianceStaffRepository)
    {
      this._complianceStaffRepository = complianceStaffRepository;
    }

    [HttpGet]
    public IEnumerable<ComplianceStaff> GetAll(string includeProperties)
    {
      return (IEnumerable<ComplianceStaff>) this._complianceStaffRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<ComplianceStaff> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<ComplianceStaff>) this._complianceStaffRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public ComplianceStaff GetById(int id, string includeProperties)
    {
      return this._complianceStaffRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Save(ComplianceStaff entity)
    {
      return this._complianceStaffRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._complianceStaffRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }
  }
}
