﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWaterWithdrawalLocationController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using AutoMapper;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWaterWithdrawalLocationController : ApiController, IUpdatableService<PermitWaterWithdrawalLocation>, IService<PermitWaterWithdrawalLocation>
  {
    private readonly ModelContext _context;
    private readonly IRepository<PermitWaterWithdrawalPurpose> _permitWaterWithdrawalPurposeRepository;

    public PermitWaterWithdrawalLocationController(ModelContext context, IRepository<PermitWaterWithdrawalPurpose> permitWaterWithdrawalPurposeRepository)
    {
      this._context = context;
      this._permitWaterWithdrawalPurposeRepository = permitWaterWithdrawalPurposeRepository;
    }

    [HttpGet]
    public PermitWaterWithdrawalLocation GetById(int id, string includeProperties = null)
    {
      PermitWaterWithdrawalLocation withdrawalLocation = (PermitWaterWithdrawalLocation) null;
      if (id > 0)
      {
        sp_GetWaterWithdrawalLocation_Result source = this._context.sp_GetWaterWithdrawalLocation(new int?(id)).FirstOrDefault<sp_GetWaterWithdrawalLocation_Result>();
        if (source != null)
          withdrawalLocation = Mapper.Map<sp_GetWaterWithdrawalLocation_Result, PermitWaterWithdrawalLocation>(source);
      }
      return withdrawalLocation;
    }

    [HttpPost]
    public int Save(PermitWaterWithdrawalLocation entity)
    {
      return entity.Id != 0 ? this._context.sp_EditWaterWithdrawalLocation(new int?(entity.Id), new int?(entity.WithdrawalSourceId), new int?(entity.WithdrawalTypeId), string.Join<int>(",", entity.SelectedPermitWaterWithdrawalPurposes), entity.StateStreamId, entity.OtherStreamName, entity.IntakeLocation, entity.IsExistingWell, entity.WellDepth, entity.WellDiameter, entity.WellTagNo, entity.Comments, entity.LastModifiedBy).First<int?>().GetValueOrDefault() : this._context.sp_AddWaterWithdrawalLocation(new int?(entity.PermitId), new double?(entity.WithdrawalLocationX), new double?(entity.WithdrawalLocationY), new int?(entity.SpatialReferenceId), new int?(entity.WithdrawalSourceId), new int?(entity.WithdrawalTypeId), string.Join<int>(",", entity.SelectedPermitWaterWithdrawalPurposes), entity.StateStreamId, entity.OtherStreamName, entity.IntakeLocation, entity.IsExistingWell, entity.WellDepth, entity.WellDiameter, entity.WellTagNo, entity.Comments, entity.LastModifiedBy).First<int?>().GetValueOrDefault();
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._context.sp_RemoveWaterWithdrawalLocation(new int?(id));
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<PermitWaterWithdrawalLocation> GetAll(string includeProperties = null)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<PermitWaterWithdrawalLocation> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
