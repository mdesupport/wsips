﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.ChecklistController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class ChecklistController : ApiController, IUpdatableService<CheckList>, IService<CheckList>
  {
    private readonly IRepository<CheckList> _checklistRepository;

    public ChecklistController(IRepository<CheckList> checklistRepository)
    {
      this._checklistRepository = checklistRepository;
    }

    [HttpGet]
    public IEnumerable<CheckList> GetAll(string includeProperties)
    {
      return (IEnumerable<CheckList>) this._checklistRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<CheckList> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<CheckList>) this._checklistRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpGet]
    public CheckList GetById(int id, string includeProperties)
    {
      return this._checklistRepository.GetById(id, includeProperties);
    }

    [HttpPost]
    public int Count(IEnumerable<DynamicFilter> filters)
    {
      return this._checklistRepository.Count(filters);
    }

    [HttpPost]
    public int Save(CheckList entity)
    {
      return this._checklistRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._checklistRepository.Delete(id);
    }
  }
}
