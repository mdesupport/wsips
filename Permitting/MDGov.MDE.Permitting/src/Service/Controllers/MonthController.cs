﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.MonthController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class MonthController : ApiController, IService<LU_Month>
  {
    private readonly IRepository<LU_Month> _monthRepository;

    public MonthController(IRepository<LU_Month> monthRepository)
    {
      this._monthRepository = monthRepository;
    }

    [HttpGet]
    public IEnumerable<LU_Month> GetAll(string includeProperties)
    {
      return (IEnumerable<LU_Month>) this._monthRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<LU_Month> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<LU_Month>) this._monthRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public LU_Month GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
