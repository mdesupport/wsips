﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Controllers.PermitWwNonAgricultureIrrigationController
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Model;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MDGov.MDE.Permitting.Service.Controllers
{
  public class PermitWwNonAgricultureIrrigationController : ApiController, IUpdatableService<PermitWwNonAgricultureIrrigation>, IService<PermitWwNonAgricultureIrrigation>
  {
    private readonly IRepository<PermitWwNonAgricultureIrrigation> _permitWwNonAgriculturalRepository;

    public PermitWwNonAgricultureIrrigationController(IRepository<PermitWwNonAgricultureIrrigation> permitWwNonAgriculturalRepository)
    {
      this._permitWwNonAgriculturalRepository = permitWwNonAgriculturalRepository;
    }

    [HttpGet]
    public IEnumerable<PermitWwNonAgricultureIrrigation> GetAll(string includeProperties)
    {
      return (IEnumerable<PermitWwNonAgricultureIrrigation>) this._permitWwNonAgriculturalRepository.All(includeProperties);
    }

    [HttpPost]
    public IEnumerable<PermitWwNonAgricultureIrrigation> GetRange(int skip, int take, string orderBy, IEnumerable<DynamicFilter> filters, string includeProperties)
    {
      return (IEnumerable<PermitWwNonAgricultureIrrigation>) this._permitWwNonAgriculturalRepository.GetRange(skip, take, orderBy, filters, includeProperties);
    }

    [HttpPost]
    public int Save(PermitWwNonAgricultureIrrigation entity)
    {
      return this._permitWwNonAgriculturalRepository.Save(entity);
    }

    [HttpDelete]
    public void Delete(int id)
    {
      this._permitWwNonAgriculturalRepository.Delete(id);
    }

    public int Count(IEnumerable<DynamicFilter> filters)
    {
      throw new NotImplementedException();
    }

    public PermitWwNonAgricultureIrrigation GetById(int id, string includeProperties = null)
    {
      throw new NotImplementedException();
    }
  }
}
