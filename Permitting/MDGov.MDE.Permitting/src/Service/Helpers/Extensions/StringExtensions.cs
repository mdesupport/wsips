﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Helpers.Extensions.StringExtensions
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using System;

namespace MDGov.MDE.Permitting.Service.Helpers.Extensions
{
  public static class StringExtensions
  {
    public static bool ToBooleanOperation(this string oper, long left, long right)
    {
      switch (oper)
      {
        case ">":
          return left > right;
        case "<":
          return left < right;
        case ">=":
          return left >= right;
        case "<=":
          return left <= right;
        case "=":
        case "==":
          return left == right;
        default:
          throw new Exception("Invalid operator");
      }
    }
  }
}
