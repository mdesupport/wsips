﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Service.Helpers.LinqExtensions
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace MDGov.MDE.Permitting.Service.Helpers
{
  public static class LinqExtensions
  {
    public static int FirstMissing(this IEnumerable<int> source)
    {
      int num1 = 0;
      foreach (int num2 in (IEnumerable<int>) source.OrderBy<int, int>((Func<int, int>) (n => n)))
      {
        if (num2 - num1 > 1)
          return num1 + 1;
        num1 = num2;
      }
      return num1 + 1;
    }
  }
}
