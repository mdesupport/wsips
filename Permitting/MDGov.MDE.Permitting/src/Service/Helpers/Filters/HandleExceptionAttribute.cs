﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.WSIPS.Permitting.Service.Helpers.Filters.HandleExceptionAttribute
// Assembly: MDGov.MDE.Permitting.Service, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2BB51A5F-D545-4EBB-BA8F-03542EE6C5DF
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Service.dll

using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MDGov.MDE.WSIPS.Permitting.Service.Helpers.Filters
{
  public class HandleExceptionAttribute : ExceptionFilterAttribute
  {
    public override void OnException(HttpActionExecutedContext actionExecutedContext)
    {
      throw new HttpResponseException(actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An exception has been caught", actionExecutedContext.Exception));
    }
  }
}
