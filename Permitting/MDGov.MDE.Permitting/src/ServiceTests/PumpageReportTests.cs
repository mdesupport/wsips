﻿using System;
using System.Collections.Generic;
using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.ServiceLayer;
using MDGov.MDE.Permitting.Model;
using MDGov.MDE.Permitting.Service.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StructureMap.AutoMocking;

namespace MDGov.MDE.Permitting.ServiceTests
{
    [TestClass]
    public class PumpageReportTests
    {
        [TestMethod]
        public void Save()
        {
            var pumpageReportEntity = new PumpageReport
            {
                Id = 5,
                ConditionCompliance = new ConditionCompliance
                {
                    ComplianceReportingStartDate = DateTime.Today,
                    PermitCondition = new PermitCondition
                    {
                        PermitId = 1234
                    }
                }
            };

            var mocker = new MoqAutoMocker<PumpageReportController>();
            mocker.MockObjectFactory();

            var pumpageReportRepositoryDependency = Mock.Get(mocker.Get<IRepository<PumpageReport>>());
            pumpageReportRepositoryDependency.Setup(handler => handler.Save(It.IsAny<PumpageReport>())).Returns(5);
            pumpageReportRepositoryDependency.Setup(handler => handler.GetById(It.IsAny<int>(), It.IsAny<string>())).Returns(pumpageReportEntity);

            var controller = mocker.ClassUnderTest;
            controller.Save(pumpageReportEntity);
        }
    }
}
