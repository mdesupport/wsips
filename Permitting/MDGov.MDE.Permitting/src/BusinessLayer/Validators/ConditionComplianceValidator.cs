﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.BusinessLayer.Validation.ConditionComplianceValidator
// Assembly: MDGov.MDE.Permitting.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 30EE2A6C-19E9-4DA4-AA4A-F3AA18244789
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Permitting.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Permitting.BusinessLayer.Validation
{
  internal class ConditionComplianceValidator : AbstractValidator<ConditionCompliance>
  {
    public ConditionComplianceValidator()
    {
      this.RuleFor<DateTime>((Expression<Func<ConditionCompliance, DateTime>>) (t => t.ComplianceReportingDueDate)).NotNull<ConditionCompliance, DateTime>().WithMessage<ConditionCompliance, DateTime>("Compliance Reporting Due Date cannot be null.");
      this.RuleFor<DateTime>((Expression<Func<ConditionCompliance, DateTime>>) (t => t.ComplianceReportingStartDate)).NotNull<ConditionCompliance, DateTime>().WithMessage<ConditionCompliance, DateTime>("Compliance Reporting Start Date cannot be null.");
    }
  }
}
