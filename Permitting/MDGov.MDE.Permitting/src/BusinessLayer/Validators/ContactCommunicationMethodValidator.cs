﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.BusinessLayer.Validation.ContactCommunicationMethodValidator
// Assembly: MDGov.MDE.Permitting.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 30EE2A6C-19E9-4DA4-AA4A-F3AA18244789
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MDGov.MDE.Permitting.BusinessLayer.Validation
{
  internal class ContactCommunicationMethodValidator : AbstractValidator<ContactCommunicationMethod>
  {
    public ContactCommunicationMethodValidator()
    {
      this.RuleFor<int>((Expression<Func<ContactCommunicationMethod, int>>) (t => t.ContactId)).NotEmpty<ContactCommunicationMethod, int>().WithMessage<ContactCommunicationMethod, int>("Contact Id cannot be empty.");
      this.RuleFor<string>((Expression<Func<ContactCommunicationMethod, string>>) (t => t.CommunicationValue)).NotEmpty<ContactCommunicationMethod, string>().WithMessage<ContactCommunicationMethod, string>("Communication Value cannot be empty.");
      this.RuleFor<bool?>((Expression<Func<ContactCommunicationMethod, bool?>>) (t => t.IsPrimaryEmail)).NotNull<ContactCommunicationMethod, bool?>().When<ContactCommunicationMethod, bool?>((Func<ContactCommunicationMethod, bool>) (t => this.IsEmailCommunicationMethod(t.CommunicationMethodId)), ApplyConditionTo.AllValidators).WithMessage<ContactCommunicationMethod, bool?>("IsPrimaryEmail must have a value (true or false) for an email communication method.");
      this.RuleFor<bool?>((Expression<Func<ContactCommunicationMethod, bool?>>) (t => t.IsPrimaryPhone)).NotNull<ContactCommunicationMethod, bool?>().When<ContactCommunicationMethod, bool?>((Func<ContactCommunicationMethod, bool>) (t => this.IsPhoneCommunicationMethod(t.CommunicationMethodId)), ApplyConditionTo.AllValidators).WithMessage<ContactCommunicationMethod, bool?>("IsPrimaryPhone must have a value (true or false) for a phone communication method.");
      this.RuleFor<bool?>((Expression<Func<ContactCommunicationMethod, bool?>>) (t => t.IsPrimaryEmail)).Must<ContactCommunicationMethod, bool?>(new Func<bool?, bool>(this.BeNull)).When<ContactCommunicationMethod, bool?>((Func<ContactCommunicationMethod, bool>) (t => t.IsPrimaryPhone.HasValue), ApplyConditionTo.AllValidators).WithMessage<ContactCommunicationMethod, bool?>("IsPrimaryEmail cannot be set at the same time as IsPrimaryPhone.");
      this.RuleFor<bool?>((Expression<Func<ContactCommunicationMethod, bool?>>) (t => t.IsPrimaryPhone)).Must<ContactCommunicationMethod, bool?>(new Func<bool?, bool>(this.BeNull)).When<ContactCommunicationMethod, bool?>((Func<ContactCommunicationMethod, bool>) (t => t.IsPrimaryEmail.HasValue), ApplyConditionTo.AllValidators).WithMessage<ContactCommunicationMethod, bool?>("IsPrimaryPhone cannot be set at the same time as IsPrimaryEmail.");
    }

    private bool IsEmailCommunicationMethod(int communicationMethodId)
    {
      return 3 == communicationMethodId;
    }

    private bool IsPhoneCommunicationMethod(int communicationMethodId)
    {
      return new List<int>() { 1, 2, 4, 5 }.Contains(communicationMethodId);
    }

    private bool BeNull(bool? value)
    {
      return !value.HasValue;
    }
  }
}
