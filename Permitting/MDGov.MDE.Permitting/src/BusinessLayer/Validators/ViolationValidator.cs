﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.BusinessLayer.Validation.ViolationValidator
// Assembly: MDGov.MDE.Permitting.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 30EE2A6C-19E9-4DA4-AA4A-F3AA18244789
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Permitting.Model;
using System;
using System.Linq.Expressions;

namespace MDGov.MDE.Permitting.BusinessLayer.Validation
{
  internal class ViolationValidator : AbstractValidator<Violation>
  {
    public ViolationValidator()
    {
      this.RuleFor<int>((Expression<Func<Violation, int>>) (t => t.PermitId)).NotEmpty<Violation, int>().WithMessage<Violation, int>("Permit Id cannot be empty.");
      this.RuleFor<int>((Expression<Func<Violation, int>>) (t => t.ViolationTypeId)).NotEmpty<Violation, int>().WithMessage<Violation, int>("Violation Type cannot be empty.");
    }
  }
}
