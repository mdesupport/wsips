﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.BusinessLayer.DependencyResolution.BusinessLayerRegistry
// Assembly: MDGov.MDE.Permitting.BusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 30EE2A6C-19E9-4DA4-AA4A-F3AA18244789
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.BusinessLayer.dll

using FluentValidation;
using MDGov.MDE.Permitting.BusinessLayer.Validation;
using MDGov.MDE.Permitting.Model;
using StructureMap.Configuration.DSL;

namespace MDGov.MDE.Permitting.BusinessLayer.DependencyResolution
{
  public class BusinessLayerRegistry : Registry
  {
    public BusinessLayerRegistry()
    {
      this.For<IValidator<Contact>>().Use<ContactValidator>();
      this.For<IValidator<PermitContactInformation>>().Use<PermitContactInformationValidator>();
      this.For<IValidator<PermitContact>>().Use<PermitContactValidator>();
      this.For<IValidator<LU_StandardCondition>>().Use<StandardConditionValidator>();
      this.For<IValidator<PermitCondition>>().Use<PermitConditionValidator>();
      this.For<IValidator<PumpageReport>>().Use<PumpageReportValidator>();
      this.For<IValidator<Secretary>>().Use<SecretaryValidator>();
      this.For<IValidator<Violation>>().Use<ViolationValidator>();
      this.For<IValidator<ConditionCompliance>>().Use<ConditionComplianceValidator>();
      this.For<IValidator<ContactCommunicationMethod>>().Use<ContactCommunicationMethodValidator>();
    }
  }
}
