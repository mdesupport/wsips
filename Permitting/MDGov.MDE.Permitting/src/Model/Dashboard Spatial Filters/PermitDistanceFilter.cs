﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitDistanceFilter
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Model.dll

using System;
using System.Data.Spatial;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitDistanceFilter : IDashboardSpatialFilter
  {
    private DbGeometry _geometry;

    public Decimal PointX { get; set; }

    public Decimal PointY { get; set; }

    public int SRID { get; set; }

    public float Distance { get; set; }

    public DbGeometry GetGeometry()
    {
      if (this._geometry == null)
        this._geometry = DbGeometry.PointFromText(string.Format("POINT({0} {1})", (object) this.PointX, (object) this.PointY), this.SRID).Buffer(new double?((double) this.Distance * 1609.344));
      return this._geometry;
    }
  }
}
