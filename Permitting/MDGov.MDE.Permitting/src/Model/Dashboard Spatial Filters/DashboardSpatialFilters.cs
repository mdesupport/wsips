﻿using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace MDGov.MDE.Permitting.Model
{
  public class DashboardSpatialFilters
  {
    public MapExtentFilter MapExtentFilter { get; set; }

    public PermitDistanceFilter PermitDistanceFilter { get; set; }

    public PermitFeatureFilter PermitFeatureFilter { get; set; }

    public IQueryable<Permit> Apply(IQueryable<Permit> model)
    {
        DbGeometry geom = null;

        foreach (var prop in this.GetType().GetProperties().Where(prop => prop.PropertyType.GetInterface(typeof(IDashboardSpatialFilter).Name, false) != null))
        {
            var filterClass = prop.GetValue(this);

            if (filterClass != null)
            {
                if (geom == null)
                {
                    geom = (filterClass as IDashboardSpatialFilter).GetGeometry();
                }
                else
                {
                    var filterGeom = (filterClass as IDashboardSpatialFilter).GetGeometry();

                    if (filterGeom != null)
                    {
                        geom = geom.Union(filterGeom);
                    }
                }
            }
        }

        if (geom == null && MapExtentFilter != null)
        {
            geom = MapExtentFilter.GetGeometry();
        }

        if (geom != null)
        {
            model = model.Where(t => t.PermitParcels.Any(pp => pp.Shape.Intersects(geom)));
        }

        return model;
    }
  }
}
