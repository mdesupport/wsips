﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitFeatureFilter
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Model.dll

using System;
using System.Data.Spatial;
using System.Linq;
using System.Linq.Expressions;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitFeatureFilter : IDashboardSpatialFilter
  {
    private DbGeometry _geometry;

    public Decimal PointX { get; set; }

    public Decimal PointY { get; set; }

    public int SRID { get; set; }

    public FeatureTypes FeatureType { get; set; }

    public DbGeometry GetGeometry()
    {
      if (this._geometry == null)
      {
        ModelContext modelContext = new ModelContext();
        DbGeometry point = DbGeometry.PointFromText(string.Format("POINT({0} {1})", (object) this.PointX, (object) this.PointY), this.SRID);
        switch (this.FeatureType)
        {
          case FeatureTypes.RechargeEasement:
            LU_RechargeEasement rechargeEasement = modelContext.LU_RechargeEasement.FirstOrDefault<LU_RechargeEasement>((Expression<Func<LU_RechargeEasement, bool>>) (feature => point.Intersects(feature.Shape) && feature.Active));
            this._geometry = rechargeEasement == null ? (DbGeometry) null : rechargeEasement.Shape;
            break;
          case FeatureTypes.WmStrategyArea:
            LU_WaterManagementStrategyArea managementStrategyArea = modelContext.LU_WaterManagementStrategyArea.FirstOrDefault<LU_WaterManagementStrategyArea>((Expression<Func<LU_WaterManagementStrategyArea, bool>>) (feature => point.Intersects(feature.Shape) && feature.Active));
            this._geometry = managementStrategyArea == null ? (DbGeometry) null : managementStrategyArea.Shape;
            break;
          case FeatureTypes.Watershed:
            LU_Watershed luWatershed = modelContext.LU_Watershed.FirstOrDefault<LU_Watershed>((Expression<Func<LU_Watershed, bool>>) (feature => point.Intersects(feature.Shape) && feature.Active));
            this._geometry = luWatershed == null ? (DbGeometry) null : luWatershed.Shape;
            break;
        }
      }
      return this._geometry;
    }
  }
}
