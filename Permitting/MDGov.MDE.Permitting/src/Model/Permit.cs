//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDGov.MDE.Permitting.Model
{
    using System;
    using System.Collections.Generic;
    using MDGov.MDE.Common.Model;
    using Newtonsoft.Json;
    
    public partial class Permit : IUpdatableEntity
    {
        public Permit()
        {
            this.CheckListAnswers = new HashSet<CheckListAnswer>();
            this.GeneratedLetters = new HashSet<GeneratedLetter>();
            this.PermitSupplementalGroups = new HashSet<PermitSupplementalGroup>();
            this.PublicComments = new HashSet<PublicComment>();
            this.WaterWithdrawalLocationPurposes = new HashSet<WaterWithdrawalLocationPurpose>();
            this.PermitConditions = new HashSet<PermitCondition>();
            this.PermitSoilTypes = new HashSet<PermitSoilType>();
            this.PermitHearings = new HashSet<PermitHearing>();
            this.PermitApplicationPackets = new HashSet<PermitApplicationPacket>();
            this.PermitWithdrawalSurfacewaters = new HashSet<PermitWithdrawalSurfacewater>();
            this.PermitCounties = new HashSet<PermitCounty>();
            this.Permit1 = new HashSet<Permit>();
            this.PermitStatusHistories = new HashSet<PermitStatusHistory>();
            this.Violations = new HashSet<Violation>();
            this.PermitHearingNotifications = new HashSet<PermitHearingNotification>();
            this.PermitWaterDispersements = new HashSet<PermitWaterDispersement>();
            this.PermitWaterWithdrawalPurposes = new HashSet<PermitWaterWithdrawalPurpose>();
            this.PermitWithdrawalGroundwaters = new HashSet<PermitWithdrawalGroundwater>();
            this.PermitStatusStates = new HashSet<PermitStatusState>();
            this.PermitParcels = new HashSet<PermitParcel>();
            this.PermitContacts = new HashSet<PermitContact>();
        }
    
        public int Id { get; set; }
        public Nullable<int> RefId { get; set; }
        public Nullable<int> ApplicationTypeId { get; set; }
        public Nullable<int> Compliance_ContactId { get; set; }
        public Nullable<int> ComplianceStatusId { get; set; }
        public Nullable<int> PermitStatusId { get; set; }
        public Nullable<int> PermitTypeId { get; set; }
        public Nullable<int> ReportCodeId { get; set; }
        public Nullable<int> WaterManagementStrategyAreaId { get; set; }
        public Nullable<int> WaterTypeId { get; set; }
        public Nullable<int> TidalTypeId { get; set; }
        public Nullable<int> AquiferId { get; set; }
        public string ApplicantIdentification { get; set; }
        public Nullable<int> RamsWanActid { get; set; }
        public string PermitNumber { get; set; }
        public string RevisionNumber { get; set; }
        public string PermitName { get; set; }
        public Nullable<bool> HasNtwWarning { get; set; }
        public Nullable<bool> RequiresAdvertising { get; set; }
        public string PWSID { get; set; }
        public Nullable<bool> IsExemptFromFees { get; set; }
        public Nullable<bool> HasWtrAuditWarning { get; set; }
        public string ProjectName { get; set; }
        public Nullable<bool> AreAbandonedWellsPresent { get; set; }
        public Nullable<bool> IsVoluntaryPermit { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public Nullable<System.DateTime> AppropriationDate { get; set; }
        public Nullable<System.DateTime> DateOut { get; set; }
        public Nullable<bool> IsInRechargeEasement { get; set; }
        public Nullable<bool> IsInTier2Watershed { get; set; }
        public Nullable<bool> IsInWMStrategyArea { get; set; }
        public string HB935VerifiedBy { get; set; }
        public Nullable<System.DateTime> HB935VerifiedDate { get; set; }
        public Nullable<bool> HardRockSubsurface { get; set; }
        public Nullable<bool> SbdnWarning { get; set; }
        public Nullable<bool> AquiferTest { get; set; }
        public string Location { get; set; }
        public string SourceDescription { get; set; }
        public string UseDescription { get; set; }
        public Nullable<bool> WaterQualityAnalysis { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public string City { get; set; }
        public Nullable<bool> StateWide { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public string CountyOfficialSignOffBy { get; set; }
        public Nullable<System.DateTime> CountyOfficialSignOffDate { get; set; }
        public Nullable<bool> CountyOfficialSignOffYesNo { get; set; }
        public string FormCode { get; set; }
        public string ReportCode { get; set; }
        public Nullable<bool> IsSubjectToEasement { get; set; }
        public Nullable<bool> HaveNotifiedEasementHolder { get; set; }
        public string ADCXCoord { get; set; }
        public string ADCYCoord { get; set; }
        public string ADCPageLetterGrid { get; set; }
        public string CountyHydroGeoMap { get; set; }
        public string USGSTopoMap { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public System.DateTime LastModifiedDate { get; set; }
    
        public virtual ICollection<CheckListAnswer> CheckListAnswers { get; set; }
        public virtual ICollection<GeneratedLetter> GeneratedLetters { get; set; }
        public virtual LU_ApplicationType LU_ApplicationType { get; set; }
        public virtual LU_Aquifer LU_Aquifer { get; set; }
        public virtual LU_PermitComplianceStatus LU_PermitComplianceStatus { get; set; }
        public virtual LU_PermitStatus LU_PermitStatus { get; set; }
        public virtual LU_PermitType LU_PermitType { get; set; }
        public virtual LU_ReportCode LU_ReportCode { get; set; }
        public virtual LU_WaterType LU_WaterType { get; set; }
        public virtual LU_TidalType LU_TidalType { get; set; }
        public virtual ICollection<PermitSupplementalGroup> PermitSupplementalGroups { get; set; }
        public virtual ICollection<PublicComment> PublicComments { get; set; }
        public virtual ICollection<WaterWithdrawalLocationPurpose> WaterWithdrawalLocationPurposes { get; set; }
        public virtual ICollection<PermitCondition> PermitConditions { get; set; }
        public virtual ICollection<PermitSoilType> PermitSoilTypes { get; set; }
        public virtual ICollection<PermitHearing> PermitHearings { get; set; }
        public virtual ICollection<PermitApplicationPacket> PermitApplicationPackets { get; set; }
        public virtual ICollection<PermitWithdrawalSurfacewater> PermitWithdrawalSurfacewaters { get; set; }
        public virtual ICollection<PermitCounty> PermitCounties { get; set; }
        public virtual ICollection<Permit> Permit1 { get; set; }
        public virtual Permit Permit2 { get; set; }
        public virtual ICollection<PermitStatusHistory> PermitStatusHistories { get; set; }
        public virtual ICollection<Violation> Violations { get; set; }
        public virtual ICollection<PermitHearingNotification> PermitHearingNotifications { get; set; }
        public virtual ICollection<PermitWaterDispersement> PermitWaterDispersements { get; set; }
        public virtual ICollection<PermitWaterWithdrawalPurpose> PermitWaterWithdrawalPurposes { get; set; }
        public virtual ICollection<PermitWithdrawalGroundwater> PermitWithdrawalGroundwaters { get; set; }
        public virtual ICollection<PermitStatusState> PermitStatusStates { get; set; }
        public virtual ICollection<PermitParcel> PermitParcels { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual ICollection<PermitContact> PermitContacts { get; set; }
    }
}
