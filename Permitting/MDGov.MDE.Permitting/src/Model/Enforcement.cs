//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDGov.MDE.Permitting.Model
{
    using System;
    using System.Collections.Generic;
    using MDGov.MDE.Common.Model;
    using Newtonsoft.Json;
    
    public partial class Enforcement : IUpdatableEntity
    {
        public Enforcement()
        {
            this.PermitViolationEnforcements = new HashSet<PermitViolationEnforcement>();
        }
    
        public int Id { get; set; }
        public int EnforcementActionId { get; set; }
        public string Comments { get; set; }
        public Nullable<System.DateTime> ActionDate { get; set; }
        public Nullable<System.DateTime> ResponseDueDate { get; set; }
        public Nullable<System.DateTime> ResponseReceivedDate { get; set; }
        public int ContactId { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public System.DateTime LastModifiedDate { get; set; }
    
        public virtual LU_EnforcementAction LU_EnforcementAction { get; set; }
        public virtual ICollection<PermitViolationEnforcement> PermitViolationEnforcements { get; set; }
    }
}
