﻿CREATE TABLE [dbo].[LU_County] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [StateId]          INT           NULL,
    [Key]              VARCHAR (10)  NULL,
    [Description]      VARCHAR (255) NULL,
    [Sequence]         INT           NULL,
    [Active]           BIT           NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME      NOT NULL,
    CONSTRAINT [XPKLU_County] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_164] FOREIGN KEY ([StateId]) REFERENCES [dbo].[LU_State] ([Id])
);

