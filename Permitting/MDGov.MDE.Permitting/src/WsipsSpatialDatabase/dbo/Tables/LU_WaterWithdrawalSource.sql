﻿CREATE TABLE [dbo].[LU_WaterWithdrawalSource] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Key]              VARCHAR (10)  NULL,
    [Description]      VARCHAR (255) NULL,
    [Sequence]         INT           NOT NULL,
    [Active]           BIT           NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME      NOT NULL,
    CONSTRAINT [XPKLU_WaterWithdrawalSource] PRIMARY KEY CLUSTERED ([Id] ASC)
);

