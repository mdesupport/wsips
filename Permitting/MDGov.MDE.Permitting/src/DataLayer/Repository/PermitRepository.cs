﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.DataLayer.Repository.PermitRepository
// Assembly: MDGov.MDE.Permitting.DataLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 21F4BA11-7DA7-4D4C-8E28-37E06D3A8472
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.DataLayer.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Common.Logging.Interface;
using MDGov.MDE.Permitting.DataLayer.Interface;
using MDGov.MDE.Permitting.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace MDGov.MDE.Permitting.DataLayer.Repository
{
  public class PermitRepository : MDGov.MDE.Common.DataLayer.Repository<Permit>, IPermitRepository, IRepository<Permit>
  {
    public PermitRepository(DbContext context, IErrorLogging loggingContext)
      : base(context, loggingContext)
    {
    }

    public bool CancelPermitApplication(int permitId)
    {
      (this._context as ModelContext).sp_CancelPermitApplication(new int?(permitId));
      return true;
    }

    public IEnumerable<Permit> GetPermitsForPumpageLetters()
    {
      return (IEnumerable<Permit>) this._context.Set<Permit>().Include<Permit, IEnumerable<ICollection<ConditionCompliance>>>((Expression<Func<Permit, IEnumerable<ICollection<ConditionCompliance>>>>) (t => t.PermitConditions.Select<PermitCondition, ICollection<ConditionCompliance>>((Func<PermitCondition, ICollection<ConditionCompliance>>) (x => x.ConditionCompliances)))).Include<Permit, IEnumerable<Contact>>((Expression<Func<Permit, IEnumerable<Contact>>>) (x => x.PermitContacts.Select<PermitContact, Contact>((Func<PermitContact, Contact>) (pc => pc.Contact)))).Where<Permit>((Expression<Func<Permit, bool>>) (p => p.PermitStatusId == (int?) 46)).OrderBy<Permit, object>(new Func<Permit, object>(this.SortPermitsByPumpageContact));
    }

    private object SortPermitsByPumpageContact(Permit permit)
    {
      PermitContact permitContact = permit.PermitContacts.SingleOrDefault<PermitContact>((Func<PermitContact, bool>) (pc =>
      {
        int? contactTypeId = pc.ContactTypeId;
        if (contactTypeId.GetValueOrDefault() == 22)
          return contactTypeId.HasValue;
        return false;
      }));
      if (permitContact != null)
      {
        if (string.IsNullOrEmpty(permitContact.Contact.LastName) && !string.IsNullOrWhiteSpace(permitContact.Contact.BusinessName))
          return (object) permitContact.Contact.BusinessName.Trim();
        string str = (string.IsNullOrEmpty(permitContact.Contact.LastName) ? string.Empty : permitContact.Contact.LastName.Trim()) + (string.IsNullOrEmpty(permitContact.Contact.FirstName) ? string.Empty : permitContact.Contact.FirstName.Trim());
        if (!string.IsNullOrWhiteSpace(str))
          return (object) str;
      }
      return (object) permit.PermitNumber;
    }
  }
}
