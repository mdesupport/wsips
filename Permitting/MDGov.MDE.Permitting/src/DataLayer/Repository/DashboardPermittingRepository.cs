﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.DataLayer.Repository.DashboardPermittingRepository
// Assembly: MDGov.MDE.Permitting.DataLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 21F4BA11-7DA7-4D4C-8E28-37E06D3A8472
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.DataLayer.dll

using MDGov.MDE.Common.Model;
using MDGov.MDE.Permitting.DataLayer.Interface;
using MDGov.MDE.Permitting.Model;
using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;

namespace MDGov.MDE.Permitting.DataLayer.Repository
{
  public class DashboardPermittingRepository : IDashboardPermittingRepository
  {
    private readonly ModelContext _context;

    public DashboardPermittingRepository(ModelContext context)
    {
      this._context = context;
    }

    public IQueryable<Permit> All(string includeProperties)
    {
      return this.PermittingEntitySet(includeProperties);
    }

    public IQueryable<Permit> GetRange(int skip, int take, string orderBy, DashboardFilterWrapper filterWrapper, string includeProperties)
    {
      IQueryable<Permit> source = this.PermittingEntitySet(includeProperties);
      if (filterWrapper.Filters != null)
      {
        foreach (DynamicFilter dynamicFilter in filterWrapper.Filters.Where<DynamicFilter>((Func<DynamicFilter, bool>) (f => !string.IsNullOrEmpty(f.Predicate))))
          source = source.Where<Permit>(dynamicFilter.Predicate, dynamicFilter.Values);
      }
      if (string.IsNullOrEmpty(orderBy))
        orderBy = "Id";
      return source.OrderBy<Permit>(orderBy).Skip<Permit>(skip).Take<Permit>(take);
    }

    private IQueryable<Permit> PermittingEntitySet(string includeProperties)
    {
      DbQuery<Permit> dbQuery = (DbQuery<Permit>) this._context.Permits;
      if (string.IsNullOrEmpty(includeProperties))
        return (IQueryable<Permit>) dbQuery;
      string str = includeProperties;
      char[] chArray = new char[1]{ ',' };
      foreach (string path in str.Split(chArray))
        dbQuery = dbQuery.Include(path);
      return (IQueryable<Permit>) dbQuery;
    }
  }
}
