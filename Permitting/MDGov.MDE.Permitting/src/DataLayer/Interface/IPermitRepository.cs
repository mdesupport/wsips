﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.DataLayer.Interface.IPermitRepository
// Assembly: MDGov.MDE.Permitting.DataLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 21F4BA11-7DA7-4D4C-8E28-37E06D3A8472
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.DataLayer.dll

using MDGov.MDE.Common.DataLayer;
using MDGov.MDE.Permitting.Model;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.DataLayer.Interface
{
  public interface IPermitRepository : IRepository<Permit>
  {
    bool CancelPermitApplication(int permitId);

    IEnumerable<Permit> GetPermitsForPumpageLetters();
  }
}
