﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.DataLayer.Interface.IDashboardPermittingRepository
// Assembly: MDGov.MDE.Permitting.DataLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 21F4BA11-7DA7-4D4C-8E28-37E06D3A8472
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.DataLayer.dll

using MDGov.MDE.Permitting.Model;
using System.Linq;

namespace MDGov.MDE.Permitting.DataLayer.Interface
{
  public interface IDashboardPermittingRepository
  {
    IQueryable<Permit> All(string includeProperties);

    IQueryable<Permit> GetRange(int skip, int take, string orderBy, DashboardFilterWrapper filterWrapper, string includeProperties);
  }
}
