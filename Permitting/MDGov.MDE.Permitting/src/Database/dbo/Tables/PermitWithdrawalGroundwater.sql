﻿CREATE TABLE [dbo].[PermitWithdrawalGroundwater] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [PermitId]          INT            NOT NULL,
    [AvgGalPerDay]      BIGINT         NOT NULL DEFAULT 0,
    [MaxGalPerDay]      BIGINT         NOT NULL DEFAULT 0,
    [CreatedBy]         VARCHAR (100)  NOT NULL,
    [CreatedDate]       DATETIME2      NOT NULL,
    [LastModifiedBy]    VARCHAR (100)  NOT NULL,
    [LastModifiedDate]  DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitWithdrawalGroundwater] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_65] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id])
);

