﻿CREATE TABLE [dbo].[PermitWwInstitutionalEducational_drinkingOrsanitary] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
	[PermitWaterWithdrawalPurposeId] INT           NULL,
	[AverageGallonPerDayFromGroundWater]            INT    NULL,
	[MaximumGallonPerDayFromGroundWater]                   INT    NULL,
	[AverageGallonPerDayFromSurfaceWater]            INT    NULL,
	[MaximumGallonPerDayFromSurfaceWater]                   INT    NULL,
	[EstimateDescription]            VARCHAR (500)  NULL,
	[EducationInstituteTypeId]					INT NULL,
	[NoOfStaff]     INT NULL,
	[NoOfStudents]   INT NULL,
	[IsAthleticFieldIrrigation] BIT NULL,
	[NoOfIrrigatedAcres] INT NULL,
	[Chillers]   BIT NULL, 
	[NoOfStaffWorkingMorethan20hrsPerWeek]      INT NULL,
	[NoOfStaffWorkingLessthan20hrsPerWeek]      INT NULL,
	[NoOfStudentsAttendingMorethan20hrsPerWeek]      INT NULL,
	[NoOfStudentsAttendingLessthan20hrsPerWeek]      INT NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
	CONSTRAINT [XPKPermitWwInstitutionalEducational_drinkingOrsanitary] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [PWWIE_FK_39] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [PWW_IE_DS_FK_39] FOREIGN KEY ([EducationInstituteTypeId]) REFERENCES [dbo].[LU_EducationInstituteType] ([Id])
	
);

