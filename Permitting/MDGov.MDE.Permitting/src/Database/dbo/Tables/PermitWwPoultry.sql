﻿CREATE TABLE [dbo].[PermitWwPoultry] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
    [PoultryCoolingSystemId]         INT           NULL,
    [PoultryId]                      INT           NULL,
    [PermitWaterWithdrawalPurposeId] INT           NULL,
    [NumberOfHouses]                 INT           NULL,
    [NumberOfFlocksPerYear]          INT           NULL,
    [BirdsPerFlock]                  INT           NULL,
	[NoOfDaysPerYearFoggersUsed]     INT           NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermit_Water_Withdrawal_Poultry] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_118] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [R_48] FOREIGN KEY ([PoultryId]) REFERENCES [dbo].[LU_PoultryType] ([Id]),
    CONSTRAINT [R_49] FOREIGN KEY ([PoultryCoolingSystemId]) REFERENCES [dbo].[LU_PoultryCoolingSystem] ([Id])
);

