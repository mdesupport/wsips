﻿CREATE TABLE [dbo].[PermitWwFarmPortableSupplies] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
	[PermitWaterWithdrawalPurposeId] INT           NULL,
	[NoOfEmployees]					 INT NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
	CONSTRAINT [XPKPermitWwFarmPortableSupplies] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [PWwFarmFK_39] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
);

