﻿CREATE TABLE [dbo].[LU_Template] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Key]              VARCHAR (10)  NULL,
    [Description]      VARCHAR (255) NULL,
	[TemplateTypeId]   INT           NULL,
    [FileName]         VARCHAR (255) NOT NULL,
    [Sequence]         INT           NULL,
    [Active]           BIT           NOT NULL,
	[CreatedBy]        VARCHAR (100) NOT NULL,
	[CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKLU_Template] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [R_301] FOREIGN KEY ([TemplateTypeId]) REFERENCES [dbo].[LU_TemplateType] ([Id])
);