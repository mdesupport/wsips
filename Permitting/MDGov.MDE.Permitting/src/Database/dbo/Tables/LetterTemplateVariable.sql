﻿CREATE TABLE [dbo].[LetterTemplateVariable] (
    [Id]                     INT           IDENTITY (1, 1) NOT NULL,
    [LetterTemplateVariable] VARCHAR (20)  NULL,
    [CreatedBy]              VARCHAR (100) NOT NULL,
    [CreatedDate]            DATETIME2      NOT NULL,
    [LastModifiedBy]         VARCHAR (100) NOT NULL,
    [LastModifiedDate]       DATETIME2      NOT NULL,
    CONSTRAINT [XPKLetterTemplateVariable] PRIMARY KEY CLUSTERED ([Id] ASC)
);

