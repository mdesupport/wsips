﻿CREATE TABLE [dbo].[PermitApplicationPacketItem] (
    [Id]                        INT           IDENTITY (1, 1) NOT NULL,
    [ApplicationPacketItemId]   INT           NULL,
    [PermitApplicationPacketId] INT           NULL,
    [ReceivedDate]              DATETIME2      NULL,
    [AcceptedDate]              DATETIME2      NULL,
    [Sequence]                  INT           NULL,
    [CreatedBy]                 VARCHAR (100) NOT NULL,
    [CreatedDate]               DATETIME2      NOT NULL,
    [LastModifiedBy]            VARCHAR (100) NOT NULL,
    [LastModifiedDate]          DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitApplicationPacketItem] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_104] FOREIGN KEY ([ApplicationPacketItemId]) REFERENCES [dbo].[LU_ApplicationPacketItem] ([Id]),
    CONSTRAINT [R_105] FOREIGN KEY ([PermitApplicationPacketId]) REFERENCES [dbo].[PermitApplicationPacket] ([Id])
);

