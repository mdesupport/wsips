﻿CREATE TABLE [dbo].[PumpageReport] (
    [Id]								INT           IDENTITY (1, 1) NOT NULL,
	[Name]								VARCHAR (255) NULL,
    [ConditionComplianceId]				INT           NULL,
    [ContactId]							INT           NULL,
    [WaterWithdrawalEstimateId]			INT           NULL,
    [ReceivedDate]						DATETIME2     NULL,
    [OtherWaterWithdrawalMethod]		VARCHAR (255) NULL,
	[CropTypeId]						INT			  NULL,
	[OtherCropDescription]				VARCHAR (255) NULL,
    [ReportYear]						VARCHAR (4)   NULL,
	[SubmittedBy]						VARCHAR (100) NULL,
	[Phone]								VARCHAR (100) NULL,
	[ReportPeriod]					    VARCHAR (1) NULL,
	[Active]                            BIT           NOT NULL,
    [CreatedBy]							VARCHAR (100) NOT NULL,
    [CreatedDate]						DATETIME2     NOT NULL,
    [LastModifiedBy]					VARCHAR (100) NOT NULL,
    [LastModifiedDate]					DATETIME2     NOT NULL,
    CONSTRAINT [XPKPumpageReport] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_126] FOREIGN KEY ([WaterWithdrawalEstimateId]) REFERENCES [dbo].[LU_WaterWithdrawalEstimate] ([Id]),
    CONSTRAINT [FK_PumpageReport_ConditionCompliance] FOREIGN KEY ([ConditionComplianceId]) REFERENCES [dbo].[ConditionCompliance] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [R_214] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]), 
	CONSTRAINT [R_315] FOREIGN KEY ([CropTypeId]) REFERENCES [dbo].[LU_CropType] ([Id])
);

