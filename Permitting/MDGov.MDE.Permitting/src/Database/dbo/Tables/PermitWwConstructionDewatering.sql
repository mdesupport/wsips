﻿CREATE TABLE [dbo].[PermitWwConstructionDewatering] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
	[PermitWaterWithdrawalPurposeId] INT           NULL,
	[AverageGallonPerDayFromGroundWater]            INT    NULL,
	[MaximumGallonPerDayFromGroundWater]                   INT    NULL,
	[AverageGallonPerDayFromSurfaceWater]            INT    NULL,
	[MaximumGallonPerDayFromSurfaceWater]                   INT    NULL,
	[EstimateDescription]            VARCHAR (500)  NULL,
	[AreaOfExcavation]   INT NULL,
	[DepthOfExcavation] INT Null,
	[NoOfPumps]      INT Null,
	[CapacityOfPumps] INT NULL,
	[HoursOfOperationPerDay]  INT NULL,
	[WaterRemovalExcavationTypeId]  INT NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
	CONSTRAINT [XPK_PermitWwConstructionDewatering] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [PWW_CD_PWWP_39] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [PWW_CD_FK_39] FOREIGN KEY ([WaterRemovalExcavationTypeId]) REFERENCES [dbo].[LU_WaterRemovalExcavationType] ([Id])
);

