﻿CREATE TABLE [dbo].[HearingNotification] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [HearingNotificationTypeId]     INT     NULL,
    [Subject]                VARCHAR (100)  NULL,
    [Message]                VARCHAR (8000) NULL,
    [StartDate]              DATETIME2      NULL,
    [EndDate]                DATETIME2      NULL,
    [Active]                 BIT            NOT NULL,
    [PublicHearingRequested] BIT            NULL,
    [ExtendUntillDate]       DATETIME2      NULL,
    [CreatedBy]              VARCHAR (100)  NOT NULL,
    [CreatedDate]            DATETIME2      NOT NULL,
    [LastModifiedBy]         VARCHAR (100)  NOT NULL,
    [LastModifiedDate]       DATETIME2      NOT NULL,
    [IsExtended] BIT NULL, 
    CONSTRAINT [XPKHearingNotification] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_68] FOREIGN KEY ([HearingNotificationTypeId]) REFERENCES [dbo].[LU_HearingNotificationType] ([Id])
);

