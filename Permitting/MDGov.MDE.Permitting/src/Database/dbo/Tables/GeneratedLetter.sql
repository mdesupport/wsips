﻿CREATE TABLE [dbo].[GeneratedLetter] (
    [Id]                         INT           IDENTITY (1, 1) NOT NULL,
    [ApprovedBy_DivisionChiefId] INT           NULL,
    [LetterTemplateId]           INT           NULL,
    [PermitId]                   INT           NULL,
    [GeneratedLetter]            VARCHAR (MAX) NULL,
    [ApprovedDate_Supervisor]    DATETIME2      NULL,
    [ApprovedBy_SupervisorId]    INT           NULL,
    [ApprovedDate_DivisionChief] DATETIME2      NULL,
    [CreatedBy]                  VARCHAR (100) NOT NULL,
    [CreatedDate]                DATETIME2      NOT NULL,
    [LastModifiedBy]             VARCHAR (100) NOT NULL,
    [LastModifiedDate]           DATETIME2      NOT NULL,
    CONSTRAINT [XPKGeneratedLetter] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_102] FOREIGN KEY ([LetterTemplateId]) REFERENCES [dbo].[LetterTemplate] ([Id]),
    CONSTRAINT [R_103] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
    CONSTRAINT [R_168] FOREIGN KEY ([ApprovedBy_DivisionChiefId]) REFERENCES [dbo].[DivisionChief] ([Id]),
    CONSTRAINT [R_169] FOREIGN KEY ([ApprovedBy_SupervisorId]) REFERENCES [dbo].[Supervisor] ([Id])
);

