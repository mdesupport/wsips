﻿CREATE TABLE [dbo].[ConditionCompliance] (
    [Id]                                    INT            IDENTITY (1, 1) NOT NULL,
    [PermitConditionComplianceStatusId]     INT            NOT NULL,
    [PermitConditionId]                     INT            NOT NULL,
    [ComplianceReportingStartDate]          DATETIME2      NOT NULL,
    [ComplianceReportingEndDate]            DATETIME2      NOT NULL,
    [ComplianceReportingDueDate]            DATETIME2      NOT NULL,
	[AnnualAverageUsage]                    FLOAT          NOT NULL DEFAULT 0,
	[AnnualAverageOverusePercentage]        FLOAT          NOT NULL DEFAULT 0,
	[MonthlyMaximumOverusePercentage]       FLOAT          NOT NULL DEFAULT 0,
	[AnnualTotalUsage]                      FLOAT          NULL,
	[ReportAverageUsage]                    FLOAT          NULL,
	[ReportTotalUsage]                      FLOAT          NULL,
	[HighMonth]                             INT            NULL,
	[HighMonthAvgGalPerDay]                 FLOAT          NULL,
    [CreatedBy]                             VARCHAR (100)  NOT NULL,
    [CreatedDate]                           DATETIME2      NOT NULL,
    [LastModifiedBy]                        VARCHAR (100)  NOT NULL,
    [LastModifiedDate]                      DATETIME2      NOT NULL,
    CONSTRAINT [XPKConditionCompliance] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_117] FOREIGN KEY ([PermitConditionComplianceStatusId]) REFERENCES [dbo].[LU_PermitConditionComplianceStatus] ([Id]),
    CONSTRAINT [R_18] FOREIGN KEY ([PermitConditionId]) REFERENCES [dbo].[PermitCondition] ([Id]) ON DELETE CASCADE
);

