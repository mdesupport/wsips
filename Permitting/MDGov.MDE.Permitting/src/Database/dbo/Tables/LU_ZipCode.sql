﻿CREATE TABLE [dbo].[LU_ZipCode](
	[Id]				INT				IDENTITY(1,1) NOT NULL,
	[StateId]			INT				NOT NULL,
	[ZipCode]			VARCHAR(10)		NOT NULL,
	[City]				VARCHAR(100)	NOT NULL,
	[Key]				VARCHAR(10)		NULL,
	[Description]		VARCHAR(255)	NULL,
	[Sequence]			INT				NULL,
	[Active]			BIT				NOT NULL,
	[CreatedBy]			VARCHAR(100)	NOT NULL,
	[CreatedDate]		DATETIME2		NOT NULL,
	[LastModifiedBy]	VARCHAR(100)	NOT NULL,
	[LastModifiedDate]	DATETIME2		NOT NULL, 
    CONSTRAINT [PK_LU_ZipCode] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_LU_ZipCode_LU_State] FOREIGN KEY (StateId) REFERENCES [dbo].[LU_State] (Id)
)