﻿CREATE TABLE [dbo].[PermitStatusState] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [PermitId]            INT           NULL,
	[PermitStatusId] INT           NULL,
    [Completed]            Bit			NOT NULL,
    [CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2      NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2      NOT NULL,
	CONSTRAINT [XPKPermitStatusState] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [PSFK_31] FOREIGN KEY ([PermitStatusId]) REFERENCES [dbo].[LU_PermitStatus] ([Id]),
    CONSTRAINT [RRT_46] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
);

