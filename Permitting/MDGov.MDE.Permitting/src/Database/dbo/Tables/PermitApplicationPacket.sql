﻿CREATE TABLE [dbo].[PermitApplicationPacket] (
    [Id]                      INT           IDENTITY (1, 1) NOT NULL,
    [ApprovedBy_SupervisorId] INT           NULL,
    [PermitId]                INT           NULL,
    [ApprovedDate_Supervisor] DATETIME2      NULL,
    [SentToPermitteeDate]     DATETIME2      NULL,
    [PacketCompleteDate]      DATETIME2      NULL,
    [CreatedBy]               VARCHAR (100) NOT NULL,
    [CreatedDate]             DATETIME2      NOT NULL,
    [LastModifiedBy]          VARCHAR (100) NOT NULL,
    [LastModifiedDate]        DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitApplicationPacket] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_170] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
    CONSTRAINT [R_172] FOREIGN KEY ([ApprovedBy_SupervisorId]) REFERENCES [dbo].[Supervisor] ([Id])
);

