﻿CREATE TABLE [dbo].[ContactMergeHistory]
(
	[Id] INT NOT NULL IDENTITY,
	[TableName] VARCHAR(255) NOT NULL,
	[PrimaryId] INT NOT NULL,
	[OldContactId] INT NOT NULL,
	[NewContactId] INT NOT NULL,
	CONSTRAINT [PK_ContactMergeHistory] PRIMARY KEY CLUSTERED ([Id] ASC)
)
