﻿CREATE TABLE [dbo].[PermitWwLivestock] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
    [Livestock]                    VARCHAR (50)          NULL,
    [PermitWaterWithdrawalPurposeId] INT           NULL,
    [NumberOfLivestock]              INT           NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermit_Water_Withdrawal_Livestock] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_121] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
);

