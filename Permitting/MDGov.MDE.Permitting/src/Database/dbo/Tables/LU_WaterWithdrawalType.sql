﻿CREATE TABLE [dbo].[LU_WaterWithdrawalType] (
    [Id]                      INT           IDENTITY (1, 1) NOT NULL,
    [WaterWithdrawalSourceId] INT           NULL,
    [Key]                     VARCHAR (10)  NULL,
    [Description]             VARCHAR (255) NULL,
    [Sequence]                INT           NOT NULL,
    [Active]                  BIT           NOT NULL,
    [CreatedBy]               VARCHAR (100) NOT NULL,
    [CreatedDate]             DATETIME2      NOT NULL,
    [LastModifiedBy]          VARCHAR (100) NOT NULL,
    [LastModifiedDate]        DATETIME2      NOT NULL,
    CONSTRAINT [XPKWaterWithdrawalType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_192] FOREIGN KEY ([WaterWithdrawalSourceId]) REFERENCES [dbo].[LU_WaterWithdrawalSource] ([Id])
);

