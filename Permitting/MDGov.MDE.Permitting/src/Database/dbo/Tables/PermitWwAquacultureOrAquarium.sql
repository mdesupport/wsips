﻿CREATE TABLE [dbo].[PermitWwAquacultureOrAquarium] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
	[PermitWaterWithdrawalPurposeId] INT           NULL,
	[AverageGallonPerDayFromGroundWater]            INT    NULL,
	[MaximumGallonPerDayFromGroundWater]                   INT    NULL,
	[AverageGallonPerDayFromSurfaceWater]            INT    NULL,
	[MaximumGallonPerDayFromSurfaceWater]                   INT    NULL,
	[EstimateDescription]            VARCHAR (500)  NULL,
	[IsRecycledWater]          bit NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
	CONSTRAINT [XPKPermitWwAA] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [PWwAAFK_39] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
);

