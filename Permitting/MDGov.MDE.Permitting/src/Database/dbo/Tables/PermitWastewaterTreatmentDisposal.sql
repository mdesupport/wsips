﻿CREATE TABLE [dbo].[PermitWastewaterTreatmentDisposal] (
    [Id]                              INT           IDENTITY (1, 1) NOT NULL,
	[PermitWaterWithdrawalPurposeId] INT           NULL,
	[IsWastewaterDischarge]			  BIT     NULL,
    [WaterDispersementTypeId]         INT           NULL,
	[GroundwaterDischargeDiscription] VARCHAR (500)  NULL,
	[PermitDischargeNumber]			  VARCHAR(20) NULL,
    [CreatedBy]                       VARCHAR (100) NOT NULL,
    [CreatedDate]                     DATETIME2      NOT NULL,
    [LastModifiedBy]                  VARCHAR (100) NOT NULL,
    [LastModifiedDate]                DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitWastewaterTreatmentDisposal] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [PWRD_61] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [PWWDT_62] FOREIGN KEY ([WaterDispersementTypeId]) REFERENCES [dbo].[LU_WaterDispersementType] ([Id])
);

