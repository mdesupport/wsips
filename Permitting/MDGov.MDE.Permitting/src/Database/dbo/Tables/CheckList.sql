﻿CREATE TABLE [dbo].[CheckList] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [PermitStatusId] INT           NULL,
	[Active]        BIT           NOT NULL,
    [CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2      NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2      NOT NULL,
    CONSTRAINT [XPKCheckList] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_43] FOREIGN KEY ([PermitStatusId]) REFERENCES [dbo].[LU_PermitStatus] ([Id])
);

