﻿CREATE TABLE [dbo].[ContactCommunicationMethod] (
    [Id]                    INT           IDENTITY (1, 1) NOT NULL,
    [CommunicationMethodId] INT           NOT NULL,
    [ContactId]             INT           NOT NULL,
    [IsPrimaryEmail]        BIT           NULL,
    [IsPrimaryPhone]        BIT           NULL,
    [CommunicationValue]    VARCHAR (50)  NOT NULL,
    [CreatedBy]             VARCHAR (100) NOT NULL,
    [CreatedDate]           DATETIME2     NOT NULL,
    [LastModifiedBy]        VARCHAR (100) NOT NULL,
    [LastModifiedDate]      DATETIME2     NOT NULL,
	[SourceId]				INT			  NULL,
    [SourceTable]			VARCHAR(50)   NULL,
    CONSTRAINT [XPKContactCommunicationMethod] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_184] FOREIGN KEY ([CommunicationMethodId]) REFERENCES [dbo].[LU_CommunicationMethod] ([Id]),
    CONSTRAINT [R_185] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]) ON DELETE CASCADE
);

