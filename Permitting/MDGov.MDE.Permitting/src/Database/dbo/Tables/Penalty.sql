﻿CREATE TABLE [dbo].[Penalty]
(
	[Id] INT IDENTITY (1, 1) NOT NULL,
	[ProductId] int NOT NULL, 
    [ContactSaleId] INT NOT NULL, 
    [CreatedBy] VARCHAR(100) NOT NULL, 
    [CreatedDate] DATETIME2 NOT NULL, 
    [LastModifiedBy] VARCHAR(100) NOT NULL, 
    [LastModifiedDate] DATETIME2 NOT NULL,
	CONSTRAINT [XPKPenalty] PRIMARY KEY CLUSTERED ([Id] ASC)
)
