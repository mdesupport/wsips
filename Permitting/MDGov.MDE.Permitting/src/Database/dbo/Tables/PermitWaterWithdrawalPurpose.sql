﻿CREATE TABLE [dbo].[PermitWaterWithdrawalPurpose] (
    [Id]                       INT           IDENTITY (1, 1) NOT NULL,
    [PermitId]                 INT           NULL,
    [WaterWithdrawalPurposeId] INT           NULL,
    [UsePercent]               INT           NULL,
	[TotalIrrigatedAcres]      INT           NULL,
	[Sequence]                 INT           NULL, 
    [CreatedBy]                VARCHAR (100) NOT NULL,
    [CreatedDate]              DATETIME2     NOT NULL,
    [LastModifiedBy]           VARCHAR (100) NOT NULL,
    [LastModifiedDate]         DATETIME2     NOT NULL,

    CONSTRAINT [XPKPermitWaterWithdrawalPurpose] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_63] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
    CONSTRAINT [R_96] FOREIGN KEY ([WaterWithdrawalPurposeId]) REFERENCES [dbo].[LU_WaterWithdrawalPurpose] ([Id])
);

