﻿CREATE TABLE [dbo].[LU_Aquifer] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [AquiferTypeId]    INT           NULL,
    [Key]              VARCHAR (10)  NULL,
    [Description]      VARCHAR (255) NULL,
    [Sequence]         INT           NULL,
    [Active]           BIT           NOT NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKLU_Aquifer] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_205] FOREIGN KEY ([AquiferTypeId]) REFERENCES [dbo].[LU_AquiferType] ([Id])
);

