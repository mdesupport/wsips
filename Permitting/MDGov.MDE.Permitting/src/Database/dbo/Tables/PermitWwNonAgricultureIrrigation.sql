﻿CREATE TABLE [dbo].[PermitWwNonAgricultureIrrigation] (
   [Id]                             INT           IDENTITY (1, 1) NOT NULL,
	[PurposeDescription]			 VARCHAR (500)  NULL,
	[NoOfIrrigatedAcres]                 INT           NULL,
	[NoOfDaysPerWeekIrrigated]           INT  NULL,
	[AverageGallonPerDayFromGroundWater]            INT    NULL,
	[MaximumGallonPerDayFromGroundWater]                   INT    NULL,
	[AverageGallonPerDayFromSurfaceWater]            INT    NULL,
	[MaximumGallonPerDayFromSurfaceWater]                   INT    NULL,
    [PermitWaterWithdrawalPurposeId] INT           NULL,
	[EstimateDescription]            VARCHAR (500)  NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
    CONSTRAINT [XPK_PermitWwNonAgricultureIrrigation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [PwwP_FK_119] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
);

