﻿CREATE TABLE [dbo].[PermitCounty] (
    [Id]                        INT           IDENTITY (1, 1) NOT NULL,
    [CountyId]                  INT           NOT NULL,
    [PermitId]                  INT           NOT NULL,
    [RequiresSignOff]           BIT           NULL,
    [CountySignOff]             BIT           NULL,
    [CountySignOffExplanation]  VARCHAR (255) NULL,
    [CountySignOffContactTitle] INT           NULL,
    [CountySignOffContact]      VARCHAR (20)  NULL,
    [CountySignOffDate]         DATETIME2      NULL,
    [UsePercent]                INT           NULL,
    [CreatedBy]                 VARCHAR (100) NOT NULL,
    [CreatedDate]               DATETIME2      NOT NULL,
    [LastModifiedBy]            VARCHAR (100) NOT NULL,
    [LastModifiedDate]          DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitCounty] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_197] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[LU_County] ([Id]),
    CONSTRAINT [R_203] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id])
);

