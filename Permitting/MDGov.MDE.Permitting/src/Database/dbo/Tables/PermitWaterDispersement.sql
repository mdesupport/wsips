﻿CREATE TABLE [dbo].[PermitWaterDispersement] (
    [Id]                       INT           IDENTITY (1, 1) NOT NULL,
    [AquiferId]                INT           NULL,
    [PermitId]                 INT           NULL,
    [StateStreamId]            INT           NULL,
    [WaterDispersementTypeId]  INT           NULL,
    [WasteWaterTreatmentOther] VARCHAR (255) NULL,
    [DischargePermitNumber]    VARCHAR (20)  NULL,
    [StreamName]               VARCHAR (20)  NULL,
    [CreatedBy]                VARCHAR (100) NOT NULL,
    [CreatedDate]              DATETIME2      NOT NULL,
    [LastModifiedBy]           VARCHAR (100) NOT NULL,
    [LastModifiedDate]         DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitWaterDispersement] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_225] FOREIGN KEY ([AquiferId]) REFERENCES [dbo].[LU_Aquifer] ([Id]),
    CONSTRAINT [R_39] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [R_45] FOREIGN KEY ([WaterDispersementTypeId]) REFERENCES [dbo].[LU_WaterDispersementType] ([Id]),
    CONSTRAINT [R_76] FOREIGN KEY ([StateStreamId]) REFERENCES [dbo].[LU_StateStream] ([Id])
);

