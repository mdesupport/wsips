﻿CREATE TABLE [dbo].[PermitCondition] (
    [Id]                         INT            IDENTITY (1, 1) NOT NULL,
    [ConditionReportingPeriodId] INT            NULL,
    [PermitId]                   INT            NULL,
	[StandardConditionTypeId]    INT            NULL,
    [ConditionText]              VARCHAR (MAX)  NULL,
    [RequiresSelfReporting]      BIT            NULL,
    [OneTimeReportDays]			 INT	        NULL,
	[RequiresValidation]		 BIT			NULL,
	[ComplianceReportingDueDate] VARCHAR(5)     NULL,
	[PumpageReportTypeId]		 INT			NULL,
    [Sequence]                   INT            NULL,
    [CreatedBy]                  VARCHAR (100)  NOT NULL,
    [CreatedDate]                DATETIME2       NOT NULL,
    [LastModifiedBy]             VARCHAR (100)  NOT NULL,
    [LastModifiedDate]           DATETIME2       NOT NULL,
    CONSTRAINT [XPKPermitCondition] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_1] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
	CONSTRAINT [R_313] FOREIGN KEY ([StandardConditionTypeId]) REFERENCES [dbo].[LU_StandardConditionType] ([Id]),
    CONSTRAINT [R_116] FOREIGN KEY ([ConditionReportingPeriodId]) REFERENCES [dbo].[LU_ConditionReportingPeriod] ([Id]),
	CONSTRAINT [R_316] FOREIGN KEY ([PumpageReportTypeId]) REFERENCES [dbo].[LU_PumpageReportType] ([Id])
);

