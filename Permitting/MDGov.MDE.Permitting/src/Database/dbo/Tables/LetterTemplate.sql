﻿CREATE TABLE [dbo].[LetterTemplate] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [LetterText]       VARCHAR (MAX) NULL,
    [TemplateName]     VARCHAR (20)  NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKLetterTemplate] PRIMARY KEY CLUSTERED ([Id] ASC)
);

