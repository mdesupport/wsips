﻿CREATE TABLE [dbo].[Enforcement] (
    [Id]                                   INT           IDENTITY (1, 1) NOT NULL,
	[EnforcementActionId]				   INT           NOT NULL,
	[Comments]							   VARCHAR(MAX) NULL, 
	[ActionDate]                      DATETIME2      NULL,
    [ResponseDueDate]                      DATETIME2      NULL,
    [ResponseReceivedDate]                 DATETIME2      NULL,
    [ContactId]                       INT      NOT NULL,
    [CreatedBy]                            VARCHAR (100) NOT NULL,
    [CreatedDate]                          DATETIME2      NOT NULL,
    [LastModifiedBy]                       VARCHAR (100) NOT NULL,
    [LastModifiedDate]                     DATETIME2      NOT NULL,
    CONSTRAINT [XPKEnforcement] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_307] FOREIGN KEY ([EnforcementActionId]) REFERENCES [dbo].[LU_EnforcementAction] ([Id])
);

