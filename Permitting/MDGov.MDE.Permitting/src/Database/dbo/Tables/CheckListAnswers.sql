﻿CREATE TABLE [dbo].[CheckListAnswers] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [CheckListQuestionId] INT           NULL,
    [PermitId]            INT           NULL,
    [AnswerId]            INT			NOT NULL,
    [CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2      NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2      NOT NULL,
    CONSTRAINT [XPKCheckListAnswers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_31] FOREIGN KEY ([CheckListQuestionId]) REFERENCES [dbo].[CheckListQuestions] ([Id]),
    CONSTRAINT [R_46] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
	CONSTRAINT [R_300] FOREIGN KEY ([AnswerId]) REFERENCES [dbo].[LU_CheckListAnswer] ([Id])
);

