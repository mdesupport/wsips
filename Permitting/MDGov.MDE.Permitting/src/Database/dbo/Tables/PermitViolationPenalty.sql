﻿CREATE TABLE [dbo].[PermitViolationPenalty]
(
	[Id] INT		            IDENTITY (1, 1) NOT NULL, 
    [ViolationId] INT NOT NULL,
	[PenaltyId] INT NOT NULL,
	[CreatedBy]                 VARCHAR (100) NOT NULL,
    [CreatedDate]               DATETIME2      NOT NULL,
    [LastModifiedBy]            VARCHAR (100) NOT NULL,
    [LastModifiedDate]          DATETIME2      NOT NULL,
	CONSTRAINT [XPKPermitViolationPenalty] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [R_310] FOREIGN KEY ([ViolationId]) REFERENCES [dbo].[Violation] ([Id]),
	CONSTRAINT [R_311] FOREIGN KEY ([PenaltyId]) REFERENCES [dbo].[Penalty] ([Id])
)
