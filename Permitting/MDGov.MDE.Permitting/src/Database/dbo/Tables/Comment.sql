﻿CREATE TABLE [dbo].[Comment] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [PermitStatusId] INT           NULL,
    [RefId]               INT           NULL,
    [RefTableId]          INT           NULL,
    [Comment]             VARCHAR (MAX) NULL,
    [CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2      NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2      NOT NULL,
    CONSTRAINT [XPKComment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_198] FOREIGN KEY ([RefTableId]) REFERENCES [dbo].[LU_TableList] ([Id]),
    CONSTRAINT [R_78] FOREIGN KEY ([PermitStatusId]) REFERENCES [dbo].[LU_PermitStatus] ([Id])
);

