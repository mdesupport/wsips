﻿CREATE TABLE [dbo].[DocumentByte]
(
	[Id] INT             IDENTITY (1, 1) NOT NULL,
    [Document] VARBINARY(MAX) NULL,
	[MimeType]            VARCHAR (100)   NULL,
	[CreatedBy]           VARCHAR (100)   NOT NULL,
    [CreatedDate]         DATETIME2        NOT NULL,
    [LastModifiedBy]      VARCHAR (100)   NOT NULL,
    [LastModifiedDate]    DATETIME2        NOT NULL,
	CONSTRAINT [XPKDocumentByte] PRIMARY KEY CLUSTERED ([Id] ASC),
)
