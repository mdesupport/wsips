﻿CREATE TABLE [dbo].[PermitSoilType] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [PermitId]         INT           NULL,
    [SoilTypeId]       INT           NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitSoilType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_100] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
    CONSTRAINT [R_101] FOREIGN KEY ([SoilTypeId]) REFERENCES [dbo].[LU_SoilType] ([Id])
);

