﻿CREATE TABLE [dbo].[PermitWaterWithdrawalMisc] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
    [PermitWaterWithdrawalPurposeId] INT           NULL,
    [MiscReason]                     VARCHAR (255) NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitWaterWithdrawalMisc] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_97] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id])
);

