﻿CREATE TABLE [dbo].[PermitWwFoodProcessing] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
	[PermitWaterWithdrawalPurposeId] INT           NULL,
	[AverageGallonPerDayFromGroundWater]            INT    NULL,
	[MaximumGallonPerDayFromGroundWater]                   INT    NULL,
	[AverageGallonPerDayFromSurfaceWater]            INT    NULL,
	[MaximumGallonPerDayFromSurfaceWater]                   INT    NULL,
	[NoOfLotsOrUnits]     INT    NULL,
	[EstimateDescription]            VARCHAR (500)  NULL,
	[NoOfEmployees]					INT NULL,
	[SwimmingPool] bit Null,
	[NoOfRegularUsersPerDay] Int Null,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
	CONSTRAINT [XPK_PermitWwFoodProcessing] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [PWWFP_FK_39] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
);

