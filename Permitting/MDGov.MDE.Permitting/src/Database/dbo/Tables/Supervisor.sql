﻿CREATE TABLE [dbo].[Supervisor] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [ContactId]        INT           NOT NULL,
    [DivisionChiefId]  INT           NULL,
    [Active]           BIT           NOT NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKSupervisor] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_174] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [R_92] FOREIGN KEY ([DivisionChiefId]) REFERENCES [dbo].[DivisionChief] ([Id])
);

