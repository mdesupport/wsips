﻿CREATE TABLE [dbo].[LU_StateStream] (
    [Id]                   INT           IDENTITY (1, 1) NOT NULL,
    [WatershedId]          INT           NULL,
    [Key]                  VARCHAR (10)  NULL,
    [Description]          VARCHAR (255) NULL,
    [Sequence]             INT           NULL,
    [Active]               BIT           NOT NULL,
    [SecondaryName]        VARCHAR (50)  NULL,
    [Level]                INT           NULL,
    [McFlag]               CHAR (1)      NULL,
    [ReceivingTributaryId] INT           NULL,
    [RiverMile]            INT           NULL,
    [VolumeNumber]         INT           NULL,
    [SequenceNumber]       INT           NULL,
    [CreatedBy]            VARCHAR (100) NOT NULL,
    [CreatedDate]          DATETIME2      NOT NULL,
    [LastModifiedBy]       VARCHAR (100) NOT NULL,
    [LastModifiedDate]     DATETIME2      NOT NULL,
    CONSTRAINT [XPKLU_StateStream] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_266] FOREIGN KEY ([ReceivingTributaryId]) REFERENCES [dbo].[LU_StateStream] ([Id]),
    CONSTRAINT [R_66] FOREIGN KEY ([WatershedId]) REFERENCES [dbo].[LU_Watershed] ([Id])
);

