﻿CREATE TABLE [dbo].[WaterWithdrawalLocationPurpose] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
	[PermitId]                       INT           NOT NULL,
	[PermitWaterWithdrawalPurposeId] INT           NOT NULL,
    [WaterWithdrawalSourceId]        INT           NOT NULL,
    [PermitWithdrawalId]             INT           NOT NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2     NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2     NOT NULL,
    CONSTRAINT [XPKWaterWithdrwalLocationPurpose] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_WaterWithdrawalLocationPurpose_Permit] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit]([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_WaterWithdrawalLocationPurpose_PermitWaterWithdrawalPurpose] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose]([Id]) ON DELETE CASCADE
);

