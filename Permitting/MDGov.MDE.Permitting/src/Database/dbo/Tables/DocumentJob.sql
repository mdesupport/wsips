﻿CREATE TABLE [dbo].[DocumentJob]
(
	[Id]               INT           IDENTITY (1, 1) NOT NULL, 
    [TemplateId] INT NULL, 
    [ContactId] INT NULL, 
	[DocumentByteId] INT NULL, 
    [Status] INT NULL,
	[Active] BIT NULL,
	[CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2      NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2      NOT NULL,
	CONSTRAINT [XPKDocumentJob] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_318] FOREIGN KEY ([TemplateId]) REFERENCES [dbo].[LU_Template] ([Id]),
	CONSTRAINT [R_319] FOREIGN KEY ([DocumentByteId]) REFERENCES [dbo].[DocumentByte] ([Id])
)
