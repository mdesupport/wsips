﻿CREATE TABLE [dbo].[SupplementalGroup] (
    [Id]                    INT           IDENTITY (1, 1) NOT NULL,
	[SupplementalGroupName]  AS ('Group_'+CONVERT([varchar],[Id])),
    [CreatedBy]             VARCHAR (100) NOT NULL,
    [CreatedDate]           DATETIME2     NOT NULL,
    [LastModifiedBy]        VARCHAR (100) NOT NULL,
    [LastModifiedDate]      DATETIME2     NOT NULL,
    CONSTRAINT [XPKSupplementalGroup] PRIMARY KEY CLUSTERED ([Id] ASC)
);