﻿CREATE TABLE [dbo].[ExistingPermitOrApplication] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [UserId]           INT           NULL,
    [PermitName]       VARCHAR (20)  NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKExistingPermitOrApplication] PRIMARY KEY CLUSTERED ([Id] ASC)
);

