﻿CREATE TABLE [dbo].[LU_PermitStatusCommunication] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [PermitStatusId]      INT           NULL,
	[ContactTypeId]       INT           NULL,
	[EmailSubject]        VARCHAR (200) NULL,
	[EmailBody]           VARCHAR (max) NULL,
	[NotificationBody]    VARCHAR (max) NULL,
	[NotificationLink]    VARCHAR (400) NULL,
	[Description]         VARCHAR (255) NULL,
    [CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2     NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2     NOT NULL,
    CONSTRAINT [XPKLU_PermitStatusCommunication] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [RR_43] FOREIGN KEY ([PermitStatusId]) REFERENCES [dbo].[LU_PermitStatus] ([Id]),
	CONSTRAINT [FKCT_43] FOREIGN KEY ([ContactTypeId]) REFERENCES [dbo].[LU_ContactType] ([Id])
);

