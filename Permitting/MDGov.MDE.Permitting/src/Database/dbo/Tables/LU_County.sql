﻿CREATE TABLE [dbo].[LU_County] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [Enf_StateRegionId] INT           NULL,
    [Ntw_StateRegionId] INT           NULL,
    [StateId]           INT           NULL,
    [Wa_Region]         INT           NULL,
    [Newspaper]         VARCHAR (50)  NULL,
    [Key]               VARCHAR (10)  NULL,
    [Description]       VARCHAR (255) NULL,
    [Sequence]          INT           NULL,
    [Active]            BIT           NOT NULL,
    [CreatedBy]         VARCHAR (100) NOT NULL,
    [CreatedDate]       DATETIME2      NOT NULL,
    [LastModifiedBy]    VARCHAR (100) NOT NULL,
    [LastModifiedDate]  DATETIME2      NOT NULL,
    CONSTRAINT [XPKLU_County] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_163] FOREIGN KEY ([Ntw_StateRegionId]) REFERENCES [dbo].[LU_StateRegion] ([Id]),
    CONSTRAINT [R_164] FOREIGN KEY ([StateId]) REFERENCES [dbo].[LU_State] ([Id]),
    CONSTRAINT [R_165] FOREIGN KEY ([Enf_StateRegionId]) REFERENCES [dbo].[LU_StateRegion] ([Id])
);

