﻿CREATE TABLE [dbo].[Document] (
    [Id]                  INT             IDENTITY (1, 1) NOT NULL,
    [PermitStatusId]      INT             NULL,
    [RefId]               INT             NOT NULL,
    [RefTableId]          INT             NOT NULL,
    [DocumentTitle]       VARCHAR (1000)  NOT NULL,
    [DocumentByteId]      INT             NOT NULL,
	[DocumentTypeId]      INT             NULL,
	[Description]         VARCHAR (1000)  NULL,
    [CreatedBy]           VARCHAR (100)   NOT NULL,
    [CreatedDate]         DATETIME2       NOT NULL,
    [LastModifiedBy]      VARCHAR (100)   NOT NULL,
    [LastModifiedDate]    DATETIME2       NOT NULL,
    CONSTRAINT [XPKDocument] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_199] FOREIGN KEY ([RefTableId]) REFERENCES [dbo].[LU_TableList] ([Id]),
    CONSTRAINT [R_77] FOREIGN KEY ([PermitStatusId]) REFERENCES [dbo].[LU_PermitStatus] ([Id]), 
    CONSTRAINT [FK_Document_ToTable] FOREIGN KEY ([DocumentByteId]) REFERENCES [DocumentByte]([Id]),
	CONSTRAINT [R_303] FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[LU_DocumentType] ([Id])
);

