﻿CREATE TABLE [dbo].[PermitWwHorticulture] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
    [HorticultureId]                 INT           NULL,
    [PermitWaterWithdrawalPurposeId] INT           NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitWwHorticulture] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_120] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]),
    CONSTRAINT [R_57] FOREIGN KEY ([HorticultureId]) REFERENCES [dbo].[LU_HorticultureType] ([Id])
);

