﻿CREATE TABLE [dbo].[PermitViolationEnforcement]
(
	[Id] INT		            IDENTITY (1, 1) NOT NULL, 
    [ViolationId] INT NOT NULL,
	[EnforcementId] INT NOT NULL,
	[CreatedBy]                 VARCHAR (100) NOT NULL,
    [CreatedDate]               DATETIME2      NOT NULL,
    [LastModifiedBy]            VARCHAR (100) NOT NULL,
    [LastModifiedDate]          DATETIME2      NOT NULL,
	CONSTRAINT [XPKPermitViolationEnforcement] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [R_308] FOREIGN KEY ([ViolationId]) REFERENCES [dbo].[Violation] ([Id]),
	CONSTRAINT [R_309] FOREIGN KEY ([EnforcementId]) REFERENCES [dbo].[Enforcement] ([Id])
)
