﻿CREATE TABLE [dbo].[Report] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [URL]              VARCHAR (100) NULL,
    [ReportName]       VARCHAR (100) NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKReport] PRIMARY KEY CLUSTERED ([Id] ASC)
);

