﻿CREATE TABLE [dbo].[PermitContact] (
    [Id]                         INT           IDENTITY (1, 1) NOT NULL,
    [ContactId]                  INT           NOT NULL,
    [ContactTypeId]              INT           NULL,
    [PermitContactInformationId] INT           NULL,
    [PermitId]                   INT           NOT NULL,
    [IsApplicant]                BIT           NOT NULL,
    [IsPermittee]                BIT           NOT NULL,
    [IsPrimary]                  BIT           NOT NULL,
    [Active]                     BIT           NOT NULL,
    [Sequence]                   INT           NULL,
    [CreatedBy]                  VARCHAR (100) NOT NULL,
    [CreatedDate]                DATETIME2      NOT NULL,
    [LastModifiedBy]             VARCHAR (100) NOT NULL,
    [LastModifiedDate]           DATETIME2      NOT NULL,
	[SourceId]					 INT			NULL,
    [SourceTable]				 VARCHAR(50)	NULL,
    CONSTRAINT [XPKPermitContact] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PermitContact_PermitContactInformation] FOREIGN KEY ([PermitContactInformationId]) REFERENCES [dbo].[PermitContactInformation] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [R_217] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]),
    CONSTRAINT [R_218] FOREIGN KEY ([ContactTypeId]) REFERENCES [dbo].[LU_ContactType] ([Id]),
    CONSTRAINT [R_219] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id])
);

