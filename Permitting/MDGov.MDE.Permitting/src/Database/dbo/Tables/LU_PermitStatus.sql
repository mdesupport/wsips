﻿CREATE TABLE [dbo].[LU_PermitStatus] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
	[PermitExternalStatusId] INT     NULL,
	[PermitCategoryId] INT           NULL,
    [Key]              VARCHAR (10)  NULL,
    [Description]      VARCHAR (255) NULL,
    [Sequence]         INT           NULL,
    [Active]           BIT           NOT NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKLU_PermitStatus] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_LU_PermitStatus_LU_PermitExternalStatus] FOREIGN KEY ([PermitExternalStatusId]) REFERENCES [LU_PermitExternalStatus]([Id]),
    CONSTRAINT [FK_LU_PermitStatus_LU_PermitCategory] FOREIGN KEY ([PermitCategoryId]) REFERENCES [LU_PermitCategory]([Id])
);