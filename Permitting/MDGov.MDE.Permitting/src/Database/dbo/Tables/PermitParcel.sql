﻿CREATE TABLE [dbo].[PermitParcel] (
    [Id]					INT           IDENTITY (1, 1) NOT NULL,
    [PermitId]				INT           NULL,
	[CountyId]				INT           NULL,
    [IsAdjacent]			BIT           NULL,
    [ParcelId]				VARCHAR (50)  NULL,
	[TaxMapNumber]			NVARCHAR(12)  NULL,
	[TaxMapBlockNumber]		NVARCHAR(12)  NULL,
	[TaxMapSubBlockNumber]	NVARCHAR(12)  NULL,
	[TaxMapParcel]			NVARCHAR(12)  NULL,
	[LotNumber]				NVARCHAR(12)  NULL,
	[Street]				VARCHAR(500)  NULL,
	[City]					VARCHAR(500)  NULL,
	[State]					VARCHAR(10)   NULL,
	[Zip]					VARCHAR(10)   NULL,
	[Shape]					GEOMETRY      NULL,
    [CreatedBy]				VARCHAR (100) NOT NULL,
    [CreatedDate]			DATETIME2     NOT NULL,
    [LastModifiedBy]		VARCHAR (100) NOT NULL,
    [LastModifiedDate]		DATETIME2     NOT NULL,
    CONSTRAINT [XPKPermitParcels] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_69] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
	CONSTRAINT [R_237] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[LU_County] ([Id])
);

