﻿CREATE TABLE [dbo].[PermitWwCrops] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
    [CropId]                         INT           NULL,
    [CropYieldUnitId]                INT           NULL,
    [IrrigationSystemTypeId]         INT           NULL,
	[IrrigationSystemDescription]    VARCHAR(5000)  NULL,
    [PermitWaterWithdrawalPurposeId] INT           NULL,
    [IrrigatedAcres]                 INT           NULL,
    [CropYieldGoal]                  INT           NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermit_Water_Withdrawal_Crops] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_119] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [R_60] FOREIGN KEY ([CropId]) REFERENCES [dbo].[LU_CropType] ([Id]),
    CONSTRAINT [R_61] FOREIGN KEY ([IrrigationSystemTypeId]) REFERENCES [dbo].[LU_IrrigationSystemType] ([Id]),
    CONSTRAINT [R_62] FOREIGN KEY ([CropYieldUnitId]) REFERENCES [dbo].[LU_CropYieldUnit] ([Id])
);

