﻿CREATE TABLE [dbo].[PermitStatusHistory] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [PermitStatusId] INT           NULL,
    [PermitId]            INT           NULL,
    [CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2      NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2      NOT NULL,
    CONSTRAINT [XPKApplicationStatusHistory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_28] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
    CONSTRAINT [R_88] FOREIGN KEY ([PermitStatusId]) REFERENCES [dbo].[LU_PermitStatus] ([Id])
);

