﻿CREATE TABLE [dbo].[PermitWwVegetables] (
    [Id]                             INT           IDENTITY (1, 1) NOT NULL,
    [CropYieldUnitId]                INT           NULL,
    [IrrigationSystemTypeId]         INT           NULL,
    [PermitWaterWithdrawalPurposeId] INT           NULL,
    [VegetableId]                    INT           NULL,
    [IrrigatedAcres]                 INT           NULL,
    [CropYieldGoal]                  INT           NULL,
    [CreatedBy]                      VARCHAR (100) NOT NULL,
    [CreatedDate]                    DATETIME2      NOT NULL,
    [LastModifiedBy]                 VARCHAR (100) NOT NULL,
    [LastModifiedDate]               DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitWwVegetables] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_122] FOREIGN KEY ([PermitWaterWithdrawalPurposeId]) REFERENCES [dbo].[PermitWaterWithdrawalPurpose] ([Id]),
    CONSTRAINT [R_53] FOREIGN KEY ([VegetableId]) REFERENCES [dbo].[LU_VegetableType] ([Id]),
    CONSTRAINT [R_54] FOREIGN KEY ([IrrigationSystemTypeId]) REFERENCES [dbo].[LU_IrrigationSystemType] ([Id]),
    CONSTRAINT [R_55] FOREIGN KEY ([CropYieldUnitId]) REFERENCES [dbo].[LU_CropYieldUnit] ([Id])
);

