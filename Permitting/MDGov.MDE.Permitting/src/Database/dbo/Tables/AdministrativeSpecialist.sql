﻿CREATE TABLE [dbo].[AdministrativeSpecialist] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [ContactId]        INT           NOT NULL,
    [Active]           BIT           NOT NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2     NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2     NOT NULL,
    CONSTRAINT [XPKAdministrativeSpecialist] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AdministrativeSpecialist_Contact] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]) ON DELETE CASCADE,
);