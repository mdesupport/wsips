﻿CREATE TABLE [dbo].[PermitHearingNotification] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [NotificationId]   INT           NULL,
    [PermitId]         INT           NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitHearingNotification] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_34] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
    CONSTRAINT [R_35] FOREIGN KEY ([NotificationId]) REFERENCES [dbo].[HearingNotification] ([Id])
);

