﻿CREATE TABLE [dbo].[CheckListQuestions] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [CheckListId]      INT           NULL,
    [Question]         VARCHAR (255) NULL,
    [QuestionNumber]   INT           NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKCheckListQuestions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_37] FOREIGN KEY ([CheckListId]) REFERENCES [dbo].[CheckList] ([Id])
);

