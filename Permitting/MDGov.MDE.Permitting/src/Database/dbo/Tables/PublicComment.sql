﻿CREATE TABLE [dbo].[PublicComment] (
    [Id]				INT					IDENTITY (1, 1) NOT NULL,
    [PermitId]			INT					NOT NULL,
	[Comment]             VARCHAR (MAX) NOT NULL,
    [Name]				VARCHAR (125)		NULL,
    [ContactInfo]		VARCHAR (300)		NULL,
    [CreatedBy]			VARCHAR (100)		NOT NULL,
    [CreatedDate]		DATETIME2			NOT NULL,
    [LastModifiedBy]	VARCHAR (100)		NOT NULL,
    [LastModifiedDate]	DATETIME2			NOT NULL
    CONSTRAINT [XPKPublicComment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PublicComment_Permit] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
);
