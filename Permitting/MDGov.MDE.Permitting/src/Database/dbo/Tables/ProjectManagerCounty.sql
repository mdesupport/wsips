﻿CREATE TABLE [dbo].[ProjectManagerCounty] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [ProjectManagerId] INT           NOT NULL,
	[CountyId]         INT           NULL,
    [Active]           BIT           NOT NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKProjectManagerCounty] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_294] FOREIGN KEY ([ProjectManagerId]) REFERENCES [dbo].[ProjectManager] ([Id]),
    CONSTRAINT [R_295] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[LU_County] ([Id])
);


