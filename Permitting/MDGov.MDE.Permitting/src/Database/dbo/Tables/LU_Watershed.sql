﻿CREATE TABLE [dbo].[LU_Watershed] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Key]              VARCHAR (17)  NULL,
    [Description]      VARCHAR (255) NULL,
    [Sequence]         INT           NULL,
    [Active]           BIT           NOT NULL,
	[Shape]            GEOMETRY      NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100) NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKWatershed] PRIMARY KEY CLUSTERED ([Id] ASC)
);

