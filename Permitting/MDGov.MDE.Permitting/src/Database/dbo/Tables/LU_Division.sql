﻿CREATE TABLE [dbo].[LU_Division] (
    [Id]                   INT           IDENTITY (1, 1) NOT NULL,
    [CountyId]             INT           NULL,
    [DivisionChiefId]      INT           NULL,
    [StateId]              INT           NULL,
    [Name]                 VARCHAR (100) NULL,
    [Address1]             VARCHAR (50)  NULL,
    [Address2]             VARCHAR (50)  NULL,
    [City]                 VARCHAR (50)  NULL,
    [ZipCode]              VARCHAR (10)  NULL,
    [PrimaryContactNumber] VARCHAR (10)  NULL,
	[Description]          VARCHAR (255) NULL,
    [Sequence]             INT           NULL,
    [Active]               BIT           NOT NULL,
    [CreatedBy]            VARCHAR (100) NOT NULL,
    [CreatedDate]          DATETIME2      NOT NULL,
    [LastModifiedBy]       VARCHAR (100) NOT NULL,
    [LastModifiedDate]     DATETIME2      NOT NULL,
    CONSTRAINT [XPKDivision] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_188] FOREIGN KEY ([DivisionChiefId]) REFERENCES [dbo].[DivisionChief] ([Id]),
    CONSTRAINT [R_288] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[LU_County] ([Id]),
    CONSTRAINT [R_296] FOREIGN KEY ([StateId]) REFERENCES [dbo].[LU_State] ([Id])
);

