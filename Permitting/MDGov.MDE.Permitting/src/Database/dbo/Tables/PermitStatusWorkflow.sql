﻿CREATE TABLE [dbo].[PermitStatusWorkflow]
(
	[Id] INT IDENTITY (1, 1) NOT NULL, 
    [PermitTypeId] INT NOT NULL, 
    [PermitStatusId] INT NOT NULL, 
    [Sequence] INT NOT NULL, 
    [CreatedBy] VARCHAR(100) NOT NULL, 
    [CreatedDate] DATETIME2 NOT NULL, 
    [LastModifiedBy] VARCHAR(100) NOT NULL, 
    [LastModifiedDate] DATETIME2 NOT NULL,
	CONSTRAINT [XPKPermitStatusWorkflow] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_298] FOREIGN KEY ([PermitTypeId]) REFERENCES [dbo].[LU_PermitType] ([Id]),
    CONSTRAINT [R_299] FOREIGN KEY ([PermitStatusId]) REFERENCES [dbo].[LU_PermitStatus] ([Id])
)
