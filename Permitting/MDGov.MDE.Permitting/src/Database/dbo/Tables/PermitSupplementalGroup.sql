﻿CREATE TABLE [dbo].[PermitSupplementalGroup] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [PermitId]            INT           NOT NULL,
    [SupplementalGroupId] INT           NOT NULL,
	[SupplementalTypeId]  INT           NOT NULL,
    [CreatedBy]           VARCHAR (100) NOT NULL,
    [CreatedDate]         DATETIME2     NOT NULL,
    [LastModifiedBy]      VARCHAR (100) NOT NULL,
    [LastModifiedDate]    DATETIME2     NOT NULL,
    CONSTRAINT [XPKPermitSupplementalGroup] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PermitSupplementalGroup_SupplementalGroup] FOREIGN KEY ([SupplementalGroupId]) REFERENCES [dbo].[SupplementalGroup] ([Id]),
    CONSTRAINT [FK_PermitSupplementalGroup_Permit] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
	CONSTRAINT [FK_PermitSupplementalGroup_SupplementalType] FOREIGN KEY ([SupplementalTypeId]) REFERENCES LU_SupplementalType (Id)
);

