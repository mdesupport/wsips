﻿CREATE TABLE [dbo].[PumpageReportDetail] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [MonthId]          INT            NOT NULL,
    [PumpageReportId]  INT            NOT NULL,
    [Gallons]          BIGINT         NOT NULL DEFAULT 0,
	[InchesApplied]    INT            NULL DEFAULT 0,
	[Acres]			   INT            NULL DEFAULT 0,
    [CreatedBy]        VARCHAR (100)  NOT NULL,
    [CreatedDate]      DATETIME2      NOT NULL,
    [LastModifiedBy]   VARCHAR (100)  NOT NULL,
    [LastModifiedDate] DATETIME2      NOT NULL,
    CONSTRAINT [XPKPumpageReportDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_123] FOREIGN KEY ([MonthId]) REFERENCES [dbo].[LU_Month] ([Id]),
    CONSTRAINT [R_4] FOREIGN KEY ([PumpageReportId]) REFERENCES [dbo].[PumpageReport] ([Id]) ON DELETE CASCADE
);