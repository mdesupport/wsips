﻿CREATE TABLE [dbo].[Violation]
(
	[Id] INT IDENTITY (1, 1) NOT NULL,
	[PermitId] int NOT NULL, 
    [FileNumber] VARCHAR(20) NOT NULL, 
    [ViolationTypeId] INT NOT NULL, 
    [ViolationDate] DATETIME2 NOT NULL, 
    [Description] VARCHAR(MAX) NULL,
    [Status] VARCHAR(MAX) NULL, 
    [ResolutionDate] DATETIME2 NULL, 
    [CreatedBy] VARCHAR(100) NOT NULL, 
    [CreatedDate] DATETIME2 NOT NULL, 
    [LastModifiedBy] VARCHAR(100) NOT NULL, 
    [LastModifiedDate] DATETIME2 NOT NULL,
	CONSTRAINT [XPKViolation] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [R_304] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
	CONSTRAINT [R_305] FOREIGN KEY ([ViolationTypeId]) REFERENCES [dbo].[LU_ViolationType] ([Id])
)
