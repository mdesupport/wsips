﻿CREATE TABLE [dbo].[PermitHearing] (
    [Id]                    INT           IDENTITY (1, 1) NOT NULL,
    [PermitHearingStatusId] INT           NULL,
    [PermitId]              INT           NULL,
    [StateId]               INT           NULL,
    [PermitHearingDateTime] DATETIME2      NULL,
    [Address1]              VARCHAR (50)  NULL,
    [Address2]              VARCHAR (50)  NULL,
    [City]                  VARCHAR (50)  NULL,
    [ZipCode]               VARCHAR (10)  NULL,
	[HearingOfficer]        VARCHAR (100) NULL,
    [CreatedBy]             VARCHAR (100) NOT NULL,
    [CreatedDate]           DATETIME2      NOT NULL,
    [LastModifiedBy]        VARCHAR (100) NOT NULL,
    [LastModifiedDate]      DATETIME2      NOT NULL,
    CONSTRAINT [XPKPermitHearing] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [R_107] FOREIGN KEY ([PermitHearingStatusId]) REFERENCES [dbo].[LU_PermitHearingStatus] ([Id]),
    CONSTRAINT [R_108] FOREIGN KEY ([PermitId]) REFERENCES [dbo].[Permit] ([Id]),
    CONSTRAINT [R_114] FOREIGN KEY ([StateId]) REFERENCES [dbo].[LU_State] ([Id])
);

