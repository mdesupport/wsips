﻿CREATE PROCEDURE [dbo].[sp_GetWaterWithdrawalLocation]
(
	@id int
)
AS

	SELECT	p.OBJECTID AS Id, p.PermitId, p.Shape.STX AS WithdrawalLocationX, p.Shape.STY AS WithdrawalLocationY, p.Shape.STSrid as SpatialReferenceId, p.WaterWithdrawalSourceId,
			CASE WHEN p.WaterWithdrawalSourceId = 1 THEN pwgwd.GroundwaterTypeId ELSE pwswd.SurfaceWaterTypeId END AS WithdrawalTypeId,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN NULL ELSE pwswd.StateStreamId END AS StateStreamId,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN NULL ELSE pwswd.OtherStreamName END AS OtherStreamName,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN NULL ELSE pwswd.ExactLocationOfIntake END AS ExactLocationOfIntake,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN pwgwd.IsExisting ELSE NULL END AS IsExistingWell,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN pwgwd.WellDepthFeet ELSE NULL END AS WellDepth,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN pwgwd.WellDiameterInches ELSE NULL END AS WellDiameter,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN pwgwd.WellTagNumber ELSE NULL END AS WellTagNo,
			CASE WHEN p.WaterWithdrawalSourceId = 1	THEN pwgwd.Comment ELSE pwswd.Comment END AS Comments,
			wu.WaterUses AS AssociatedPermitWaterWithdrawalPurposes, p.LastModifiedBy
	FROM	MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT p LEFT OUTER JOIN
			MdeWaterPermitting..PermitWithdrawalGroundwaterDetail pwgwd ON p.PermitWithdrawalId = pwgwd.Id LEFT OUTER JOIN
			MdeWaterPermitting..PermitWithdrawalSurfaceWaterDetail pwswd ON p.PermitWithdrawalId = pwswd.Id LEFT OUTER JOIN
			(SELECT wwlp.WaterWithdrawalSourceId, wwlp.PermitWithdrawalId, STUFF((SELECT ',' + CAST(wwlp2.PermitWaterWithdrawalPurposeId AS VARCHAR)
			                             FROM MdeWaterPermitting..WaterWithdrawalLocationPurpose wwlp2
			                             WHERE wwlp.WaterWithdrawalSourceId = wwlp2.WaterWithdrawalSourceId AND wwlp.PermitWithdrawalId = wwlp2.PermitWithdrawalId
										 FOR XML PATH('')), 1, 1, '') AS WaterUses
             FROM MdeWaterPermitting..WaterWithdrawalLocationPurpose wwlp
             GROUP BY wwlp.WaterWithdrawalSourceId, wwlp.PermitWithdrawalId) AS wu on p.WaterWithdrawalSourceId = wu.WaterWithdrawalSourceId AND p.PermitWithdrawalId = wu.PermitWithdrawalId
			
	WHERE	OBJECTID = @id