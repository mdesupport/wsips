﻿CREATE PROCEDURE [dbo].[sp_RemovePermitLocation]
(
	@objectId int
)
AS
BEGIN TRANSACTION

	IF (@objectId IS NULL)
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	declare @permitId int
	declare @parcelId nvarchar(50)
	declare @location geometry

	-- Get Information about the location
	SELECT	@permitId = PermitId,
			@parcelId = ISNULL(ParcelId, 0),
			@location = SHAPE
	FROM MdeWsipsSpatial.gis_adm.PERMITLOCATION
	WHERE OBJECTID = @objectId


	-- Find all points for this permit which fall within the permit location and remove them
	DECLARE @withdrawalLocationOID int
	DECLARE PointCursor CURSOR FAST_FORWARD FOR
	SELECT OBJECTID FROM MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT
	WHERE PermitId = @permitId AND Shape.STIntersects(@location) = 1
	
	OPEN PointCursor

	FETCH NEXT FROM PointCursor INTO @withdrawalLocationOID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC MdeWaterPermitting..sp_RemoveWaterWithdrawalLocation @withdrawalLocationOID

		FETCH NEXT FROM PointCursor INTO @withdrawalLocationOID
	END

	CLOSE PointCursor
	DEALLOCATE PointCursor


	-- Finally, remove the location and PermitParcel Records
	DELETE FROM MdeWaterPermitting..PermitParcel
	WHERE PermitId = @permitId AND ISNULL(ParcelId, 0) = @parcelId

	DELETE FROM MdeWsipsSpatial.gis_adm.PERMITLOCATION
	WHERE PermitId = @permitId AND ISNULL(ParcelId, 0) = @parcelId


IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
END
ELSE
BEGIN
	COMMIT TRANSACTION
END