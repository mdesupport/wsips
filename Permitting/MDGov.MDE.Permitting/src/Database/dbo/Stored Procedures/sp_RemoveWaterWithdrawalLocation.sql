﻿CREATE PROCEDURE [dbo].[sp_RemoveWaterWithdrawalLocation]
(
	@objectId int
)
AS
BEGIN TRANSACTION

	IF (@objectId IS NULL)
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END

	declare @permitId int
	declare @waterWithdrawalSourceId int
	declare @waterWithdrawalId int
	declare @point geometry
	declare @county geometry

	-- Get Information about the point
	SELECT	@permitId = PermitId,
			@waterWithdrawalSourceId = WaterWithdrawalSourceId,
			@waterWithdrawalId = PermitWithdrawalId,
			@point = SHAPE
	FROM MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT
	WHERE OBJECTID = @objectId


	-- Remove the point record
	DELETE FROM MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT
	WHERE OBJECTID = @objectId
	

	-- Remove the purposes for this point
	DELETE FROM MdeWaterPermitting..WaterWithdrawalLocationPurpose
	WHERE WaterWithdrawalSourceId = @waterWithdrawalSourceId AND PermitWithdrawalId = @waterWithdrawalId


	-- Then remove the groundwater/surfacewater records as necessary
	declare @waterWithdrawalCount int
	IF @waterWithdrawalSourceId = 1 -- Groundwater
	BEGIN
		declare @groundwaterWithdrawalId int

		SELECT	@permitId = PermitId,
				@groundwaterWithdrawalId = PermitWithdrawalGroundwaterId
		FROM MdeWaterPermitting..PermitWithdrawalGroundwaterDetail PWGD
		INNER JOIN MdeWaterPermitting..PermitWithdrawalGroundwater PWG
			ON PWGD.PermitWithdrawalGroundwaterId = PWG.Id
		WHERE PWGD.Id = @waterWithdrawalId
		
		DELETE FROM MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
		WHERE Id = @waterWithdrawalId

		SELECT @waterWithdrawalCount = COUNT(*)
		FROM MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
		WHERE PermitWithdrawalGroundwaterId = @groundwaterWithdrawalId

		IF @waterWithdrawalCount = 0 -- No more detail records exist, so delete the parent record
		BEGIN
			DELETE FROM MdeWaterPermitting..PermitWithdrawalGroundwater
			WHERE Id = @groundwaterWithdrawalId
		END
	END
	ELSE IF @waterWithdrawalSourceId = 2 -- Surface Water
	BEGIN
		declare @surfacewaterWithdrawalId int

		SELECT	@permitId = PermitId,
				@surfacewaterWithdrawalId = PermitWithdrawalSurfacewaterId
		FROM MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail PWSD
		INNER JOIN MdeWaterPermitting..PermitWithdrawalSurfacewater PWS
			ON PWSD.PermitWithdrawalSurfacewaterId = PWS.Id
		WHERE PWSD.Id = @waterWithdrawalId
		
		DELETE FROM MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail
		WHERE Id = @waterWithdrawalId

		SELECT @waterWithdrawalCount = COUNT(*)
		FROM MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail
		WHERE PermitWithdrawalSurfacewaterId = @surfacewaterWithdrawalId

		IF @waterWithdrawalCount = 0 -- No more detail records exist, so delete the parent record
		BEGIN
			DELETE FROM MdeWaterPermitting..PermitWithdrawalSurfacewater
			WHERE Id = @surfacewaterWithdrawalId
		END
	END


	-- Then, determine the county from the point
	declare @countyId int
	SELECT	@county = Shape,
			@countyId = WSIPS_ID
	FROM MdeWsipsSpatial.gis_adm.MIDATLNCNTYS
	WHERE Shape.STIntersects(@point) = 1

	declare @remainingPointsInCounty int
	SELECT @remainingPointsInCounty = COUNT(*)
	FROM MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT
	WHERE PermitId = @permitId AND SHAPE.STIntersects(@county) = 1

	-- If no more points exist in this county, we need to remove the PermitCounty record for this permit
	IF @remainingPointsInCounty = 0
	BEGIN
		DELETE FROM MdeWaterPermitting..PermitCounty
		WHERE PermitId = @permitId AND CountyId = @countyId
	END


	-- If this is the last point within a Tier II Catchment or Water Management
	-- Strategy Area, we need to flip the associated bit on the permit record
	--
	-- First, Tier II Catchments
	declare @remainingPointsInCatchment int
	SELECT @remainingPointsInCatchment = COUNT(*)
	FROM MdeWsipsSpatial.gis_adm.TIERII_CATCHMENTS_2011_FINAL c
	INNER JOIN MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT p ON c.Shape.STIntersects(p.Shape) = 1
	WHERE p.PermitId = @permitId

	IF @remainingPointsInCatchment = 0
	BEGIN
		UPDATE MdeWaterPermitting..Permit SET IsInTier2Watershed = 0 WHERE Id = @permitId
	END

	-- And then Water Management Strategy Areas
	declare @remainingPointsInStrategyArea int
	SELECT @remainingPointsInStrategyArea = COUNT(*)
	FROM MdeWsipsSpatial.gis_adm.WMSTRATEGYAREAS a
	INNER JOIN MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT p ON a.Shape.STIntersects(p.Shape) = 1
	WHERE p.PermitId = @permitId

	IF @remainingPointsInStrategyArea = 0
	BEGIN
		UPDATE MdeWaterPermitting..Permit SET IsInWMStrategyArea = 0 WHERE Id = @permitId
	END

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
END
ELSE
BEGIN
	COMMIT TRANSACTION
END