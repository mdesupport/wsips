﻿

-- =============================================
-- Author:		JMT
-- Create date: 1/6/2014
-- Description:	This stored procedure is used to unmerge a previously merged contact. 
--	The ContactId parameter represents the id of the contact that was previously merged from
-- =============================================
CREATE PROCEDURE [dbo].[sp_UnmergeContacts]
	@ContactId  int
AS
BEGIN

	-- exec [dbo].[sp_UnmergeContacts] 122407

    BEGIN TRY
        BEGIN TRANSACTION MergeContact
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
		
		--declare @ContactId int
		--set @ContactId = 122407

		SET IDENTITY_INSERT MdeWaterPermitting..Contact ON

		-- we need to first put back the old contact
		INSERT INTO MdeWaterPermitting..Contact
			([Id],[ContactTypeId] ,[CountryId] ,[CountyId] ,[DivisionId] ,[StateId] ,[TitleId]
				,[UserId] ,[Salutation] ,[FirstName] ,[MiddleInitial] ,[LastName] ,[SecondaryName] ,[BusinessName]
				,[Address1] ,[Address2] ,[City] ,[ZipCode] ,[Subdivision] ,[IsBusiness] ,[Office] ,[ApplicationNotification]
				,[EmailNotification] ,[Active] ,[CreatedBy] ,[CreatedDate] ,[LastModifiedBy] ,[LastModifiedDate])
        SELECT TOP 1 @ContactId,[ContactTypeId] ,[CountryId] ,[CountyId] ,[DivisionId] ,[StateId] ,[TitleId]
				,[UserId] ,[Salutation] ,[FirstName] ,[MiddleInitial] ,[LastName] ,[SecondaryName] ,[BusinessName]
				,[Address1] ,[Address2] ,[City] ,[ZipCode] ,[Subdivision] ,[IsBusiness] ,[Office] ,[ApplicationNotification]
				,[EmailNotification] ,[Active] ,[CreatedBy] ,[CreatedDate] ,[LastModifiedBy] ,[LastModifiedDate] 
        FROM MdeWaterPermitting..ContactHistory
        WHERE Id = @ContactId
		ORDER BY LastModifiedDate DESC

		SET IDENTITY_INSERT MdeWaterPermitting..Contact OFF

		-- get all of the records that need to be unmerged
        SELECT	TableName, PrimaryId, @ContactId OldContactId
		into #tempRecords
        FROM	ContactMergeHistory
        WHERE	OldContactId = @ContactId

		--drop table #tempRecords
		
		-- Unmerge Supervisor
		UPDATE	MdeWaterPermitting..Supervisor 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..Supervisor'
			and Id = PrimaryId	

		-- Unmerge Enforcement
		UPDATE	MdeWaterPermitting..Enforcement 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..Enforcement'
			and Id = PrimaryId		

		-- Unmerge DivisionChief
		UPDATE	MdeWaterPermitting..DivisionChief 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..DivisionChief'
			and Id = PrimaryId	

		-- Unmerge ProjectManager
		UPDATE	MdeWaterPermitting..ProjectManager 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..ProjectManager'
			and Id = PrimaryId	

		-- Unmerge PumpageReport
		UPDATE	MdeWaterPermitting..PumpageReport 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..PumpageReport'
			and Id = PrimaryId	

		-- Unmerge Secretary
		UPDATE	MdeWaterPermitting..Secretary 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..Secretary'
			and Id = PrimaryId

		-- Unmerge ContactCommunicationMethod
		UPDATE	MdeWaterPermitting..ContactCommunicationMethod 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..ContactCommunicationMethod'
			and Id = PrimaryId

		-- Unmerge ComplianceManager
		UPDATE	MdeWaterPermitting..ComplianceManager 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..ComplianceManager'
			and Id = PrimaryId

		-- Unmerge PermitContact
		UPDATE	MdeWaterPermitting..PermitContact 
		SET		ContactId = t.OldContactId 
		FROM	#tempRecords t
        WHERE	t.TableName = 'MdeWaterPermitting..PermitContact'
			and Id = PrimaryId

        COMMIT
    END TRY
    BEGIN CATCH
            -- An error has occurred
            IF @@TRANCOUNT > 0
                    ROLLBACK
                     
            -- Raise an error with the details of the exception
            DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
            SELECT @ErrMsg = ERROR_MESSAGE(),
                    @ErrSeverity = ERROR_SEVERITY()
                     
            RAISERROR(@ErrMsg, @ErrSeverity, 1)
    END CATCH

END