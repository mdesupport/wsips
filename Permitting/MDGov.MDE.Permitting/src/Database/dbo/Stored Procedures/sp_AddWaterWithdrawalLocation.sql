﻿CREATE PROCEDURE [dbo].[sp_AddWaterWithdrawalLocation]
(
	@permitId int,
	@withdrawalLocationX float,
	@withdrawalLocationY float,
	@spatialReferenceId int,
	@withdrawalSourceId int,
	@withdrawalTypeId int,
	@selectedPermitWaterWithdrawalPurposeIds varchar(max),
	@stateStreamId int,
	@otherStreamName varchar(500),
	@intakeLocation varchar(max),
	@isExistingWell bit,
	@wellDepth int,
	@wellDiameter int,
	@wellTagNo varchar(20),
	@comment varchar(max),
	@user varchar(100)
)
AS

BEGIN TRANSACTION

	IF (@withdrawalLocationX IS NULL OR @withdrawalLocationY IS NULL OR @spatialReferenceId IS NULL OR @withdrawalSourceId IS NULL OR @withdrawalTypeId IS NULL OR @user IS NULL OR @user = '')
	BEGIN
		ROLLBACK TRANSACTION
		SELECT 0 AS WithdrawalLocationObjectId
		RETURN
	END


	-- Call sp_AddPermitLocation to add PermitLocation, PermitParcel, and PermitCounty entities, if necessary
	declare @permitLocationOID int
	declare @countyId int
	declare @taxMapNumber nvarchar(12)
	declare @taxMapBlockNumber nvarchar(12)
	declare @taxMapSubBlockNumber nvarchar(12)
	declare @taxMapParcel nvarchar(12)
	declare @lotNumber nvarchar(12)
	declare @parcelStreet nvarchar(500)
	declare @parcelCity nvarchar(500)
	declare @parcelState nvarchar(10)
	declare @parcelZip nvarchar(10)
	EXEC MdeWaterPermitting..sp_AddPermitLocation
	@permitId = @permitId,
	@withdrawalLocationX = @withdrawalLocationX,
	@withdrawalLocationY = @withdrawalLocationY,
	@spatialReferenceId = @spatialReferenceId,
	@user = @user,
	@suppressOutput = 1,
	@permitLocationOID = @permitLocationOID OUTPUT,
	@countyId = @countyId OUTPUT,
	@taxMapNumber = @taxMapNumber OUTPUT,
	@taxMapBlockNumber = @taxMapBlockNumber OUTPUT,
	@taxMapSubBlockNumber = @taxMapSubBlockNumber OUTPUT,
	@taxMapParcel = @taxMapParcel OUTPUT,
	@lotNumber = @lotNumber OUTPUT,
	@parcelStreet = @parcelStreet OUTPUT,
	@parcelCity = @parcelCity OUTPUT,
	@parcelState = @parcelState OUTPUT,
	@parcelZip = @parcelZip OUTPUT

	IF @permitLocationOID IS NULL -- Error creating PERMITLOCATION/PermitParcel record
	BEGIN
		ROLLBACK TRANSACTION
		SELECT 0 AS WithdrawalLocationObjectId
		RETURN
	END


	-- Add PermitCounty record, if necessary
	declare @permitCountyCount int;
	SELECT @permitCountyCount = COUNT(*) FROM MdeWaterPermitting..PermitCounty
	WHERE PermitId = @permitId AND CountyId = @countyId

	IF @permitCountyCount = 0
	BEGIN
		INSERT INTO MdeWaterPermitting..PermitCounty
			([PermitId], [CountyId], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
			VALUES (@permitId, @countyId, @user, GETDATE(), @user, GETDATE())
	END


	-- Create point geometry
	declare @withdrawalLocation geometry
	SET @withdrawalLocation = geometry::Point(@withdrawalLocationX, @withdrawalLocationY, @spatialReferenceId);


	-- Update Permit record to represent whether this point lies in a Tier II Catchment
	-- or Water Management Strategy Area
	-- 
	-- Start with Recharge Easements
	IF (SELECT COUNT(*) FROM MdeWsipsSpatial.gis_adm.RECHARGEEASEMENTS WHERE Shape.STIntersects(@withdrawalLocation) = 1) > 0
	BEGIN
		UPDATE MdeWaterPermitting..Permit
		SET IsInRechargeEasement = 1
		WHERE Id = @permitId
	END

	-- Then check for Tier II Catchments
	IF (SELECT COUNT(*) FROM MdeWsipsSpatial.gis_adm.TIERII_CATCHMENTS_2011_FINAL WHERE Shape.STIntersects(@withdrawalLocation) = 1) > 0
	BEGIN
		UPDATE MdeWaterPermitting..Permit
		SET IsInTier2Watershed = 1
		WHERE Id = @permitId
	END

	-- Now check for Water Management Strategy Areas
	IF (SELECT COUNT(*) FROM MdeWsipsSpatial.gis_adm.WMSTRATEGYAREAS WHERE Shape.STIntersects(@withdrawalLocation) = 1) > 0
	BEGIN
		UPDATE MdeWaterPermitting..Permit
		SET IsInWMStrategyArea = 1
		WHERE Id = @permitId
	END


	-- Get the intersecting Watershed
	declare @watershedId int = NULL
	SELECT @watershedId = Id FROM MdeWaterPermitting..LU_Watershed WHERE Shape.STIntersects(@withdrawalLocation) = 1 and Active = 1


	-- Add Groundwater/SurfaceWater locations
	declare @permitWithdrawalId int
	IF @withdrawalSourceId = 1 -- Groundwater
	BEGIN
		declare @permitWithdrawalGroundwaterCount int;
		SELECT @permitWithdrawalGroundwaterCount = COUNT(*) FROM MdeWaterPermitting..PermitWithdrawalGroundwater
		WHERE PermitId = @permitId

		IF @permitWithdrawalGroundwaterCount = 0
		BEGIN
			INSERT INTO MdeWaterPermitting..PermitWithdrawalGroundwater
				([PermitId], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
				VALUES (@permitId, @user, GETDATE(), @user, GETDATE())
		END

		DECLARE @permitWithdrawalGroundwaterId int
		SELECT TOP 1 @permitWithdrawalGroundwaterId = Id FROM MdeWaterPermitting..PermitWithdrawalGroundwater
		WHERE PermitId = @permitId

		INSERT INTO MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
		(
			[GroundwaterTypeId],
			[PermitWithdrawalGroundwaterId],
			[WatershedId],
			[CountyId],
			[IsExisting],
			[WellTagNumber],
			[WellDepthFeet],
			[WellDiameterInches],
			[Comment],
			[TaxMapNumber],
			[TaxMapBlockNumber],
			[TaxMapSubBlockNumber],
			[TaxMapParcel],
			[LotNumber],
			[Street],
			[City],
			[State],
			[Zip],
			[CreatedBy],
			[CreatedDate],
			[LastModifiedBy],
			[LastModifiedDate]
		)
		VALUES (@withdrawalTypeId, @permitWithdrawalGroundwaterId, @watershedId, @countyId, @isExistingWell, @wellTagNo, @wellDepth, @wellDiameter, @comment, @taxMapNumber, @taxMapBlockNumber, @taxMapSubBlockNumber, @taxMapParcel, @lotNumber, @parcelStreet, @parcelCity, @parcelState, @parcelZip, @user, GETDATE(), @user, GETDATE())
		SET @permitWithdrawalId = SCOPE_IDENTITY()
	END
	ELSE IF @withdrawalSourceId = 2 -- Surface Water
	BEGIN
		declare @permitWithdrawalSurfacewaterCount int
		SELECT @permitWithdrawalSurfacewaterCount = COUNT(*) FROM MdeWaterPermitting..PermitWithdrawalSurfacewater
		WHERE PermitId = @permitId

		IF @permitWithdrawalSurfacewaterCount = 0
		BEGIN
			INSERT INTO MdeWaterPermitting..PermitWithdrawalSurfacewater
				([PermitId], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
				VALUES (@permitId, @user, GETDATE(), @user, GETDATE())
		END

		DECLARE @permitWithdrawalSurfacewaterId int
		SELECT TOP 1 @permitWithdrawalSurfacewaterId = Id FROM MdeWaterPermitting..PermitWithdrawalSurfacewater
		WHERE PermitId = @permitId

		INSERT INTO MdeWaterPermitting..PermitWithdrawalSurfaceWaterDetail
		(
			[SurfaceWaterTypeId],
			[PermitWithdrawalSurfacewaterId],
			[WatershedId],
			[StateStreamId],
			[OtherStreamName],
			[CountyId],
			[ExactLocationOfIntake],
			[Comment],
			[TaxMapNumber],
			[TaxMapBlockNumber],
			[TaxMapSubBlockNumber],
			[TaxMapParcel],
			[LotNumber],
			[Street],
			[City],
			[State],
			[Zip],
			[CreatedBy],
			[CreatedDate],
			[LastModifiedBy],
			[LastModifiedDate]
		)
		VALUES (@withdrawalTypeId, @permitWithdrawalSurfacewaterId, @watershedId, @stateStreamId, @otherStreamName, @countyId, @intakeLocation, @comment, @taxMapNumber, @taxMapBlockNumber, @taxMapSubBlockNumber, @taxMapParcel, @lotNumber, @parcelStreet, @parcelCity, @parcelState, @parcelZip, @user, GETDATE(), @user, GETDATE())
		SET @permitWithdrawalId = SCOPE_IDENTITY()
	END


	-- If necessary, associate our use codes (withdrawal purposes) with the appropriate surface water or groundwater record
	IF ISNULL(@selectedPermitWaterWithdrawalPurposeIds, '') != ''
	BEGIN
		INSERT INTO MdeWaterPermitting..WaterWithdrawalLocationPurpose
			SELECT @permitId, Id, @withdrawalSourceId, @permitWithdrawalId, @user, GETDATE(), @user, GETDATE()
			FROM dbo.udfSplitIDs(@selectedPermitWaterWithdrawalPurposeIds, ',')
	END


	-- Finally, we need to add our actual point to the spatial table
	-- Get the next sequential ObjectId
	declare @newWithdrawalLocationPointOID int
	EXEC	MdeWsipsSpatial.[gis_adm].[sp_getNextObjectID]
			@TableName = N'WITHDRAWALLOCATIONPOINT',
			@newOID = @newWithdrawalLocationPointOID OUTPUT

	-- Perform the insert
	INSERT INTO MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT
		([OBJECTID], [PermitId], [WaterWithdrawalSourceId], [PermitWithdrawalId], [SHAPE], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
		VALUES (@newWithdrawalLocationPointOID, @permitId, @withdrawalSourceId, @permitWithdrawalId, @withdrawalLocation, @user, GETDATE(), @user, GETDATE())
	
	
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	SELECT 0 AS WithdrawalLocationObjectId
	RETURN
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT @newWithdrawalLocationPointOID AS WithdrawalLocationObjectId
	RETURN
END