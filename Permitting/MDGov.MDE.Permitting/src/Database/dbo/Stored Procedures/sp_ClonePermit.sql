﻿CREATE PROCEDURE [dbo].[sp_ClonePermit]
     @PermitId  int,
	 @ApplicationTypeId int,  
	 @ApplicantIdentification Varchar(6),
	 @PermitTypeId int,
	 @PermitNumber VarChar(50), 
	 @CreatedBy VarChar(50),
	 @LastModifiedBy varchar(50)
	 
AS
BEGIN
BEGIN TRANSACTION

Declare @NewPermitId Int;
Declare @permitStatusId int;
Declare @RevisionNumber varchar(20);
Declare @CurrentRevisionNo Int;

-- Get the permit status Id of the permit to be cloned
select @permitStatusId = PermitStatusId
from Permit where Id = @PermitId

-- Get the revison number 
if @PermitTypeId <> 5
begin
	if (select COUNT(*) from Permit where PermitNumber = @PermitNumber) = 0
	begin
		set @RevisionNumber = '01'
	end
	else 
	begin  
		select @CurrentRevisionNo = MAX(CAST(ISNULL(RevisionNumber, '0') as INT))
		from Permit where [PermitNumber] = @PermitNumber
					  
		set @RevisionNumber = @CurrentRevisionNo + 1; 
		set @RevisionNumber = right ('00' + ltrim(str(@RevisionNumber)), 2 )
	end
end
	
Insert Into Permit ([RefId],[ApplicationTypeId],[Compliance_ContactId],[ComplianceStatusId],[PermitStatusId],
      [PermitTypeId],[ReportCodeId],[WaterManagementStrategyAreaId],[WaterTypeId],[AquiferId],[ApplicantIdentification],[RamsWanActid]
      ,[PermitNumber],[RevisionNumber],[HasNtwWarning], [RequiresAdvertising], [HasWtrAuditWarning],[ProjectName]
      ,[AreAbandonedWellsPresent],[IsVoluntaryPermit],[ReceivedDate],[EffectiveDate],[ExpirationDate],[AppropriationDate]
      ,[DateOut],[IsInTier2Watershed],[IsInWMStrategyArea],[HB935VerifiedBy],[HB935VerifiedDate],[HardRockSubsurface]
      ,[SbdnWarning],[AquiferTest],[Location],[SourceDescription],[UseDescription],[WaterQualityAnalysis],[Description]
      ,[City],[StateWide],[SubmittedBy],[SubmittedDate],[FormCode] ,[ReportCode] ,[IsSubjectToEasement],[HaveNotifiedEasementHolder]
      ,[ADCXCoord],[ADCYCoord],[ADCPageLetterGrid],[CountyHydroGeoMap] ,[USGSTopoMap],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
       select @PermitId, [ApplicationTypeId], [Compliance_ContactId],[ComplianceStatusId],
	          case 
			     when @permitStatusId = 46 then 26
			     When @ApplicationTypeId = 1 or @ApplicationTypeId = 4 Then
					  case when @PermitTypeId = 3 or @PermitTypeId = 4 
						   then 4 
						   when @PermitTypeId = 1 or @PermitTypeId = 2 
						   then 5
						   when @PermitTypeId = 6
						   then 68
						   else 44
					  end 
				 When @ApplicationTypeId = 2 Then
					  case when @PermitTypeId in (1,2,3,4)
						   then 26
						   else 44
					  end 
				 
				 When @ApplicationTypeId = 3 Then
					  case when @PermitTypeId in (1,2,3,4)
						   then 35
						   else 44
					  end 
			  end,
			  @PermitTypeId,[ReportCodeId],[WaterManagementStrategyAreaId],[WaterTypeId] ,[AquiferId], @ApplicantIdentification ,[RamsWanActid],
			  @PermitNumber,@RevisionNumber,[HasNtwWarning],[RequiresAdvertising],[HasWtrAuditWarning],[ProjectName],[AreAbandonedWellsPresent],[IsVoluntaryPermit],GETDATE(),[EffectiveDate],
			  [ExpirationDate],[AppropriationDate],[DateOut],[IsInTier2Watershed],[IsInWMStrategyArea], [HB935VerifiedBy],[HB935VerifiedDate],[HardRockSubsurface],
			  [SbdnWarning] ,[AquiferTest],[Location] ,[SourceDescription],[UseDescription],[WaterQualityAnalysis],[Description],
			  [City],[StateWide],[SubmittedBy],[SubmittedDate],[FormCode] ,[ReportCode] ,[IsSubjectToEasement],[HaveNotifiedEasementHolder],
			  [ADCXCoord],[ADCYCoord],[ADCPageLetterGrid],[CountyHydroGeoMap] ,[USGSTopoMap],@CreatedBy, GetDate(),@LastModifiedBy ,GetDate()
      from Permit
      where Id = @PermitId

 -- Get the newly added permit Id
  SELECT @NewPermitId = SCOPE_IDENTITY()

 -- Add the new permit to Permit Status History table 

 insert into PermitStatusHistory Values ( 
			  case    When @ApplicationTypeId = 1 Then
					  case when @PermitTypeId = 3 or @PermitTypeId = 4 
						   then 4 
						   else 5 
					  end 
				 When @ApplicationTypeId = 2 Then 26
				 
				 When @ApplicationTypeId = 3 Then 35
			  end, @NewPermitId, @CreatedBy, GetDate(),@LastModifiedBy ,GetDate())

 -- Clone Permit Water Withdrawal Purpose and water use detail 
 declare @id int 
 declare @NewPermitWaterWithdrawalPurposeId int 
 
 -- Keep the old and new Permitwaterwithdrawal purpose ids in temp table 
 CREATE TABLE #Mapper(
   OldPermitWaterWithdrawalPurposeId int NOT NULL,
   NewPermitWaterWithdrawalPurposeId int NULL,
   OldPermitWwGroundWaterDetailId int NULL,
   NewPermitWwGroundWaterDetailId int NULL,
   OldPermitWwSurfaceWaterDetailId int NULL,
   NewPermitWwSurfaceWaterDetailId int NULL
 );

 INSERT INTO #Mapper (OldPermitWaterWithdrawalPurposeId, OldPermitWwGroundWaterDetailId, OldPermitWwSurfaceWaterDetailId)
 SELECT PermitWaterWithdrawalPurposeId AS OldPermitWaterWithdrawalPurposeId,
        CASE WHEN WaterWithdrawalSourceId = 1 THEN PermitWithdrawalId ELSE NULL END AS OldPermitWwGroundWaterDetailId,
		CASE WHEN WaterWithdrawalSourceId = 2 THEN PermitWithdrawalId ELSE NULL END AS OldPermitWwSurfaceWaterDetailId
 FROM MdeWaterPermitting..WaterWithdrawalLocationPurpose
 WHERE Permitid = @PermitId

 declare WaterUsepurposeCursor Cursor 
 for (select DISTINCT  id
     from PermitWaterWithdrawalPurpose where PermitId= @PermitId)
 
 Open  WaterUsepurposeCursor

 Fetch next From WaterUsepurposeCursor Into @id
 while @@FETCH_STATUS = 0
 BEGIN 
	   insert into PermitWaterWithdrawalPurpose ([PermitId],[WaterWithdrawalPurposeId],[UsePercent],
				  [CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate],[Sequence],[TotalIrrigatedAcres])
	   select @NewPermitId ,[WaterWithdrawalPurposeId] ,[UsePercent] ,@CreatedBy,
		   GetDate() ,@LastModifiedBy ,GetDate() , [Sequence],[TotalIrrigatedAcres]
		  from PermitWaterWithdrawalPurpose
		  where Id = @id

		SELECT @NewPermitWaterWithdrawalPurposeId = SCOPE_IDENTITY()
	  
		UPDATE #Mapper SET NewPermitWaterWithdrawalPurposeId = @NewPermitWaterWithdrawalPurposeId
		WHERE OldPermitWaterWithdrawalPurposeId = @id
   
	   -- CLONE AGRICULTURAL
	   insert into [dbo].[PermitWwAgricultural] ([PurposeDescription],[IrrigatedAcres],[IrrigationSystemTypeId],[IrrigationSystemDescription],[AverageGallonPerDayFromGroundWater],
												[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
												[PermitWaterWithdrawalPurposeId],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select [PurposeDescription],[IrrigatedAcres] ,[IrrigationSystemTypeId],[IrrigationSystemDescription],[AverageGallonPerDayFromGroundWater],
	          [MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription]
              ,@NewPermitWaterWithdrawalPurposeId,@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from PermitWwAgricultural
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE AQUACULTURE OR AQUARIUM
	   insert into [dbo].[PermitWwAquacultureOrAquarium] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],
												[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
												[IsRecycledWater],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],
	          [AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],[IsRecycledWater],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwAquacultureOrAquarium]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE COMMERCIAL DRINKING Or SANITARY
	   insert into [dbo].[PermitWwCommercialDrinkingOrSanitary] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],
			  [NoOfLotsOrUnits],[EstimateDescription],[TypeOfFacilityId],[AreaOrSquareFootage],[NoOfEmployees],[AvgNumberOfCustomersPerDay],
			  [RestaurantTypeId],[NoOfSeats],[NoOfSlips],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],
			  [NoOfLotsOrUnits],[EstimateDescription],[TypeOfFacilityId],[AreaOrSquareFootage],[NoOfEmployees],[AvgNumberOfCustomersPerDay],
			  [RestaurantTypeId],[NoOfSeats],[NoOfSlips],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwCommercialDrinkingOrSanitary]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	     -- CLONE CONSTRUCTION DEWATERING
	   insert into [dbo].[PermitWwConstructionDewatering] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],
												[EstimateDescription],[AreaOfExcavation],[DepthOfExcavation],[NoOfPumps],[CapacityOfPumps],
												[HoursOfOperationPerDay],[WaterRemovalExcavationTypeId],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
			  [AreaOfExcavation],[DepthOfExcavation],[NoOfPumps] ,[CapacityOfPumps],[HoursOfOperationPerDay],[WaterRemovalExcavationTypeId],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwConstructionDewatering]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE CROPS
	   insert into [dbo].[PermitWwCrops] ([CropId],[CropYieldUnitId],[IrrigationSystemTypeId],[IrrigationSystemDescription],[PermitWaterWithdrawalPurposeId],
										[IrrigatedAcres] ,[CropYieldGoal],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select [CropId] ,[CropYieldUnitId] ,[IrrigationSystemTypeId] ,[IrrigationSystemDescription],@NewPermitWaterWithdrawalPurposeId,
	        [IrrigatedAcres] ,[CropYieldGoal],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwCrops]
	   where  PermitWaterWithdrawalPurposeId = @Id
	   
	   	-- CLONE FARM PORTABLE SUPPLIES
	   insert into [dbo].[PermitWwFarmPortableSupplies] ([PermitWaterWithdrawalPurposeId],[NoOfEmployees],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[NoOfEmployees],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwFarmPortableSupplies]
	   where  PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE FOOD PROCESSING
	   insert into [dbo].[PermitWwFoodProcessing] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],
												[NoOfLotsOrUnits],[EstimateDescription],[NoOfEmployees],[SwimmingPool],[NoOfRegularUsersPerDay],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],
			  [NoOfLotsOrUnits],[EstimateDescription],[NoOfEmployees],[SwimmingPool],[NoOfRegularUsersPerDay],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwFoodProcessing]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE GOLF COURSE IRRIGATION
	   insert into [dbo].[PermitWwGolfCourseIrrigation] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
	          [NoOfIrrigatedAcres_teesOrGreens],[NoOfIrrigatedAcres_fairways],[TypeOfGrass_tees/greens],[TypeOfGrass_fairways],[GolfIrrigationSystemTypeId],[IrrigationLayoutTypeId],[IsWellwaterPumpedIntoPond],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
	          [NoOfIrrigatedAcres_teesOrGreens],[NoOfIrrigatedAcres_fairways],[TypeOfGrass_tees/greens],[TypeOfGrass_fairways],[GolfIrrigationSystemTypeId],[IrrigationLayoutTypeId],[IsWellwaterPumpedIntoPond],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwGolfCourseIrrigation]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE INSTITUTIONAL EDUCATIONAL DRINKING OR SANITARY
	   insert into [dbo].[PermitWwInstitutionalEducational_drinkingOrsanitary] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
	          [EducationInstituteTypeId],[NoOfStaff],[NoOfStudents],[IsAthleticFieldIrrigation],[NoOfIrrigatedAcres],[Chillers],[NoOfStaffWorkingMorethan20hrsPerWeek],[NoOfStaffWorkingLessthan20hrsPerWeek],[NoOfStudentsAttendingMorethan20hrsPerWeek],[NoOfStudentsAttendingLessthan20hrsPerWeek],
	          [CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
	          [EducationInstituteTypeId],[NoOfStaff],[NoOfStudents],[IsAthleticFieldIrrigation],[NoOfIrrigatedAcres],[Chillers],[NoOfStaffWorkingMorethan20hrsPerWeek],[NoOfStaffWorkingLessthan20hrsPerWeek],[NoOfStudentsAttendingMorethan20hrsPerWeek],[NoOfStudentsAttendingLessthan20hrsPerWeek],
	          @CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwInstitutionalEducational_drinkingOrsanitary]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE INSTITUTIONAL RELIGIOUS DRINKING OR SANITARY
	   insert into [dbo].[PermitWwInstitutionalReligious_DringkingOrSanitary] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
	          [NoOfParishioners],[NoOfServicesPerWeek],[Daycare],[NoOfStaffWorkingMorethan20hrsPerWeek],[NoOfStaffWorkingLessthan20hrsPerWeek],[NoOfStudentsAttendingMorethan20hrsPerWeek],[NoOfStudentsAttendingLessthan20hrsPerWeek],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],[EstimateDescription],
	          [NoOfParishioners],[NoOfServicesPerWeek],[Daycare],[NoOfStaffWorkingMorethan20hrsPerWeek],[NoOfStaffWorkingLessthan20hrsPerWeek],[NoOfStudentsAttendingMorethan20hrsPerWeek],[NoOfStudentsAttendingLessthan20hrsPerWeek],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwInstitutionalReligious_DringkingOrSanitary]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE NON AGRICULTURAL IRRIGATION
	   insert into [dbo].[PermitWwNonAgricultureIrrigation] ([PurposeDescription],[NoOfIrrigatedAcres],[NoOfDaysPerWeekIrrigated],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],[PermitWaterWithdrawalPurposeId],
	          [EstimateDescription],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select [PurposeDescription],[NoOfIrrigatedAcres],[NoOfDaysPerWeekIrrigated],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],@NewPermitWaterWithdrawalPurposeId,
	          [EstimateDescription], @CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwNonAgricultureIrrigation]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	    -- CLONE WILD LIFE PONDS
	   insert into [dbo].[PermitWwWildlifePonds] ([PermitWaterWithdrawalPurposeId],[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater],[MaximumGallonPerDayFromSurfaceWater],
	          [EstimateDescription],[NoOfPonds],[AreaOfPonds_inAcres],[DepthOfPonds_inFeet],[NoOfMonthsPerYearPondContainWater],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],
	          [EstimateDescription], [NoOfPonds],[AreaOfPonds_inAcres],[DepthOfPonds_inFeet],[NoOfMonthsPerYearPondContainWater],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [PermitWwWildlifePonds]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE LIVE STOCK
	   insert into [dbo].[PermitWwLivestock] ([Livestock],[PermitWaterWithdrawalPurposeId],[NumberOfLivestock],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select [Livestock], @NewPermitWaterWithdrawalPurposeId,[NumberOfLivestock],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [dbo].[PermitWwLivestock]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- CLONE POULTRY
	   insert into [dbo].[PermitWwPoultry] ([PoultryCoolingSystemId],[PoultryId],[PermitWaterWithdrawalPurposeId],[NumberOfHouses],[NumberOfFlocksPerYear] ,[BirdsPerFlock],[NoOfDaysPerYearFoggersUsed],
				   [CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select [PoultryCoolingSystemId] ,[PoultryId] ,@NewPermitWaterWithdrawalPurposeId ,[NumberOfHouses] ,[NumberOfFlocksPerYear],
	          [BirdsPerFlock], [NoOfDaysPerYearFoggersUsed], @CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [dbo].[PermitWwPoultry]
	   where PermitWaterWithdrawalPurposeId = @Id

	   -- Clone Waste water treatment and disposal 
	   insert into [dbo].[PermitWastewaterTreatmentDisposal] ([PermitWaterWithdrawalPurposeId],[IsWastewaterDischarge],[WaterDispersementTypeId]
      ,[GroundwaterDischargeDiscription],[PermitDischargeNumber],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[IsWastewaterDischarge],[WaterDispersementTypeId],[GroundwaterDischargeDiscription],
	          [PermitDischargeNumber],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [dbo].[PermitWastewaterTreatmentDisposal]
	   where PermitWaterWithdrawalPurposeId = @Id
	   
	   -- Clone Private Water supply  
	   insert into [dbo].[PermitWwPrivateWaterSupplier] ([PermitWaterWithdrawalPurposeId] ,[AverageGallonPerDayFromGroundWater] ,[MaximumGallonPerDayFromGroundWater] ,[AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],
	               [NoOfLotsOrUnits],[EstimateDescription],[NoOfEmployees],[SwimmingPool],[NoOfRegularUsersPerDay],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
	   select @NewPermitWaterWithdrawalPurposeId,[AverageGallonPerDayFromGroundWater],[MaximumGallonPerDayFromGroundWater],
			  [AverageGallonPerDayFromSurfaceWater] ,[MaximumGallonPerDayFromSurfaceWater],[NoOfLotsOrUnits] ,[EstimateDescription],
		      [NoOfEmployees],[SwimmingPool],[NoOfRegularUsersPerDay],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	   from [dbo].[PermitWwPrivateWaterSupplier]
	   where PermitWaterWithdrawalPurposeId = @Id

  FETCH NEXT FROM WaterUsepurposeCursor INTO @id
 END
 CLOSE WaterUsepurposeCursor
 Deallocate WaterUsepurposeCursor

 -- Clone Permit Contact and Permit Contact Information 
 declare @OldPermitContactId int
 declare @OldPermitContactInformationId int
 declare @NewPermitContactInformationId int

 declare PermitContactCursor Cursor 
 for (select	Id
      from		[dbo].[PermitContact] pc
	  where		PermitId = @PermitId)
 
 Open  PermitContactCursor

 FETCH NEXT FROM PermitContactCursor INTO @OldPermitContactId
 WHILE @@FETCH_STATUS = 0
 BEGIN

	select @OldPermitContactInformationId = PermitContactInformationId from PermitContact
	where Id = @OldPermitContactId

	if (@OldPermitContactInformationId is not null)
	BEGIN
		insert into	PermitContactInformation ([CountryId], [StateId], [IsBusiness], [Salutation], [FirstName], [LastName],
					[SecondaryName], [BusinessName], [MiddleInitial], [Address1], [Address2], [City], [ZipCode], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
		select	[CountryId], [StateId], [IsBusiness], [Salutation], [FirstName], [LastName], [SecondaryName],
				[BusinessName], [MiddleInitial], [Address1], [Address2], [City], [ZipCode], @CreatedBy, GETDATE(), @LastModifiedBy, GETDATE()
		from PermitContactInformation
		where Id = @OldPermitContactInformationId

		SELECT @NewPermitContactInformationId = SCOPE_IDENTITY()
	END

	insert into PermitContact ([ContactId], [ContactTypeId], [PermitContactInformationId], [PermitId], [IsApplicant],
      			[IsPermittee], [IsPrimary], [Active], [Sequence], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
	select	[ContactId], [ContactTypeId], @NewPermitContactInformationId, @NewPermitId, [IsApplicant], [IsPermittee], [IsPrimary],
			[Active], [Sequence], @CreatedBy, GETDATE(), @LastModifiedBy, GETDATE()
	from PermitContact
	where Id = @OldPermitContactId

 FETCH NEXT FROM PermitContactCursor INTO @OldPermitContactId
 END
 CLOSE PermitContactCursor
 DEALLOCATE PermitContactCursor

 -- Clone data Related to Map
 -- Ground Water

		 declare @OldPermitWwGroundWaterId int
		 declare @NewPermitWwGroundWaterId int

		 declare PermitWwGroundWaterCursor Cursor 
		 for (select DISTINCT  Id
			 from   [dbo].PermitWithdrawalGroundwater
				  where PermitId= @PermitId)
 
		 Open  PermitWwGroundWaterCursor

		 Fetch next From PermitWwGroundWaterCursor Into @OldPermitWwGroundWaterId
		 while @@FETCH_STATUS = 0
		 BEGIN 

				 insert into PermitWithdrawalGroundwater ([PermitId] ,[AvgGalPerDay],[MaxGalPerDay],[CreatedBy],[CreatedDate],[LastModifiedBy],[LastModifiedDate])
				 select @NewPermitId,[AvgGalPerDay],[MaxGalPerDay],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
				 from PermitWithdrawalGroundwater 
				 where Id = @OldPermitWwGroundWaterId

				 SELECT @NewPermitWwGroundWaterId = SCOPE_IDENTITY()

				 -- Clone PermitWithdrawal Ground water Detail
				
						 declare @OldPermitWwGroundWaterDetailId_1 int
						 declare @NewPermitWwGroundWaterDetailId_1 int

						 declare PermitWwGroundWaterDetailCursor Cursor 
						 for (select DISTINCT  Id
									from PermitWithdrawalGroundwaterDetail 
									where [PermitWithdrawalGroundwaterId] = @OldPermitWwGroundWaterId)
 
						 Open  PermitWwGroundWaterDetailCursor

						 Fetch next From PermitWwGroundWaterDetailCursor Into @OldPermitWwGroundWaterDetailId_1
						 while @@FETCH_STATUS = 0
						 BEGIN 
								 insert into PermitWithdrawalGroundwaterDetail ([GroundwaterTypeId],[PermitWithdrawalGroundwaterId],
										   [WatershedId],[CountyId],[IsExisting],[WellTagNumber],[WellDepthFeet],[WellDiameterInches],[UsePercent],
										   [Comment],[TaxMapNumber],[TaxMapBlockNumber],[TaxMapSubBlockNumber],[TaxMapParcel],[LotNumber],[Street],[City],[State],[Zip],[CreatedBy],
										   [CreatedDate],[LastModifiedBy] ,[LastModifiedDate])
									select [GroundwaterTypeId], @NewPermitWwGroundWaterId,[WatershedId],[CountyId] ,[IsExisting],[WellTagNumber] ,[WellDepthFeet],
										   [WellDiameterInches] ,[UsePercent],[Comment],[TaxMapNumber],[TaxMapBlockNumber],[TaxMapSubBlockNumber],[TaxMapParcel],[LotNumber],
										   [Street] ,[City] ,[State] ,[Zip] ,@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
									from PermitWithdrawalGroundwaterDetail 
									where [Id] = @OldPermitWwGroundWaterDetailId_1

								 SELECT @NewPermitWwGroundWaterDetailId_1 = SCOPE_IDENTITY()
								 
								 update #Mapper set NewPermitWwGroundWaterDetailId =  @NewPermitWwGroundWaterDetailId_1
								 where #Mapper.OldPermitWwGroundWaterDetailId = @OldPermitWwGroundWaterDetailId_1;

								 --Withdrawal Location Point 
								 declare @newWithDrawalLocationID1 int;
	  
				                 EXEC	MdeWsipsSpatial.[gis_adm].[sp_getNextObjectID]
										@TableName = N'WITHDRAWALLOCATIONPOINT',
										@newOID = @newWithDrawalLocationID1 OUTPUT

								 insert into [MdeWsipsSpatial].[gis_adm].[WITHDRAWALLOCATIONPOINT] (OBJECTID, XCoord, YCoord, PermitId,WaterWithdrawalSourceId,PermitWithdrawalId, Shape, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)
								 select  @newWithDrawalLocationID1, [XCoord],[YCoord],@NewPermitId,[WaterWithdrawalSourceId],
								         @NewPermitWwGroundWaterDetailId_1, geometry::STPointFromText('' + [SHAPE].STAsText() + '', [SHAPE].STSrid), @CreatedBy, GETDATE(), @LastModifiedBy, GETDATE()
								 from [MdeWsipsSpatial].[gis_adm].[WITHDRAWALLOCATIONPOINT]
								 where PermitWithdrawalId = @OldPermitWwGroundWaterDetailId_1 and WaterWithdrawalSourceId = 1

						 FETCH NEXT FROM PermitWwGroundWaterDetailCursor INTO @OldPermitWwGroundWaterDetailId_1
						 END
						 CLOSE PermitWwGroundWaterDetailCursor
						 Deallocate PermitWwGroundWaterDetailCursor

		 FETCH NEXT FROM PermitWwGroundWaterCursor INTO @OldPermitWwGroundWaterId
		 END
		 CLOSE PermitWwGroundWaterCursor
		 Deallocate PermitWwGroundWaterCursor

     -- Surface Water

		 declare @OldPermitWwSurfacewaterId int
		 declare @NewPermitWwSurfacewaterId int

		 declare PermitWwSurfacewaterCursor Cursor 
		 for (select DISTINCT  Id
			 from   [dbo].PermitWithdrawalSurfacewater
				  where PermitId= @PermitId)
 
		 Open  PermitWwSurfacewaterCursor

		 Fetch next From PermitWwSurfacewaterCursor Into @OldPermitWwSurfacewaterId
		 while @@FETCH_STATUS = 0
		 BEGIN 

				 insert into PermitWithdrawalSurfacewater ([PermitId] ,[AvgGalPerDay],[MaxGalPerDay],[CreatedBy],[CreatedDate],[LastModifiedBy] ,[LastModifiedDate])
				 select @NewPermitId,[AvgGalPerDay],[MaxGalPerDay] ,@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
				 from PermitWithdrawalSurfacewater 
				 where Id = @OldPermitWwSurfacewaterId

				 SELECT @NewPermitWwSurfacewaterId = SCOPE_IDENTITY()

				 -- Clone PermitWithdrawal surface water Detail
				
						 declare @OldPermitWwSurfacewaterDetailId int
						 declare @NewPermitWwSurfacewaterDetailId int

						 declare PermitWwSurfaceWaterDetailCursor Cursor 
						 for (select DISTINCT  Id
									from MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail
									where PermitWithdrawalSurfacewaterId = @OldPermitWwSurfacewaterId)
 
						 Open  PermitWwSurfaceWaterDetailCursor

						 Fetch next From PermitWwSurfaceWaterDetailCursor Into @OldPermitWwSurfacewaterDetailId
						 while @@FETCH_STATUS = 0
						 BEGIN 
								 insert into MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail (
											[SurfacewaterTypeId], [PermitWithdrawalSurfacewaterId], [WatershedId], [StateStreamId], [CountyId], [ExactLocationOfIntake], [Comment], [TaxMapNumber], [TaxMapBlockNumber], [TaxMapSubBlockNumber], [TaxMapParcel], [LotNumber], [Street], [City], [State], [Zip], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
									select	[SurfacewaterTypeId], @NewPermitWwSurfacewaterId, [WatershedId], [StateStreamId], [CountyId], [ExactLocationOfIntake], [Comment], [TaxMapNumber], [TaxMapBlockNumber], [TaxMapSubBlockNumber], [TaxMapParcel], [LotNumber], [Street], [City], [State], [Zip], @CreatedBy, GETDATE(), @LastModifiedBy, GETDATE()
									from MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail pwwgd
									where [Id] = @OldPermitWwSurfacewaterDetailId

								 SELECT @NewPermitWwSurfacewaterDetailId = SCOPE_IDENTITY()
								 
								 update #Mapper set NewPermitWwSurfaceWaterDetailId =  @NewPermitWwSurfacewaterDetailId
								 where #Mapper.OldPermitWwSurfaceWaterDetailId = @OldPermitWwSurfacewaterDetailId;

								 --Withdrawal Location Point 
								 declare @newWithDrawalLocationID int;
	  
				                 EXEC	MdeWsipsSpatial.[gis_adm].[sp_getNextObjectID]
										@TableName = N'WITHDRAWALLOCATIONPOINT',
										@newOID = @newWithDrawalLocationID OUTPUT

								insert into [MdeWsipsSpatial].[gis_adm].[WITHDRAWALLOCATIONPOINT] (OBJECTID, XCoord, YCoord, PermitId, Shape, WaterWithdrawalSourceId, PermitWithdrawalId, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)
								select  @newWithDrawalLocationID,[XCoord],[YCoord], @NewPermitId, geometry::STPointFromText('' + [SHAPE].STAsText() + '', [SHAPE].STSrid) , [WaterWithdrawalSourceId],
								        @NewPermitWwSurfacewaterDetailId, @CreatedBy, GETDATE(), @LastModifiedBy, GETDATE()
								from [MdeWsipsSpatial].[gis_adm].[WITHDRAWALLOCATIONPOINT]
								where PermitWithdrawalId = @OldPermitWwSurfacewaterDetailId and WaterWithdrawalSourceId = 2

						 FETCH NEXT FROM PermitWwSurfaceWaterDetailCursor INTO @OldPermitWwSurfacewaterDetailId
						 END
						 CLOSE PermitWwSurfaceWaterDetailCursor
						 Deallocate PermitWwSurfaceWaterDetailCursor

		 FETCH NEXT FROM PermitWwSurfacewaterCursor INTO @OldPermitWwSurfacewaterId
		 END
		 CLOSE PermitWwSurfacewaterCursor
		 Deallocate PermitWwSurfacewaterCursor


    -- clone Permit Parcels 
      insert into [dbo].[PermitParcel]
	  select @NewPermitId,[CountyId] ,[IsAdjacent],[ParcelId],[TaxMapNumber],[TaxMapBlockNumber],[TaxMapSubBlockNumber],[TaxMapParcel],[LotNumber] ,[Street],
	         [City],[State],[Zip] ,[Shape],@CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	  from [dbo].[PermitParcel]
	  where PermitId = @PermitId

    -- clone Permit Location
	  declare @oldPermitLocationID int;
	  declare @newPermitLocationID int;

	  declare PermitLocationCursor Cursor 
		for (select DISTINCT  OBJECTID
					from [MdeWsipsSpatial].[gis_adm].[PERMITLOCATION] 
					where PermitId = @PermitId)
 
		Open  PermitLocationCursor

			  Fetch next From PermitLocationCursor Into @oldPermitLocationID
			  while @@FETCH_STATUS = 0
			  BEGIN 

			  EXEC	MdeWsipsSpatial.[gis_adm].[sp_getNextObjectID]
			  	@TableName = N'PERMITLOCATION',
			  	@newOID = @newPermitLocationID OUTPUT
	   
			  insert into [MdeWsipsSpatial].[gis_adm].[PERMITLOCATION] (OBJECTID, PermitId, Shape, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)
			  select  @newPermitLocationID, @NewPermitId, [Shape], @CreatedBy, GETDATE(), @LastModifiedBy, GETDATE()
			  from [MdeWsipsSpatial].[gis_adm].[PERMITLOCATION]
			  where OBJECTID = @oldPermitLocationID

		FETCH NEXT FROM PermitLocationCursor INTO @oldPermitLocationID
		END
		CLOSE PermitLocationCursor
		Deallocate PermitLocationCursor

		INSERT INTO MdeWaterPermitting..WaterWithdrawalLocationPurpose (PermitId, PermitWaterWithdrawalPurposeId, WaterWithdrawalSourceId, PermitWithdrawalId, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)
		SELECT @NewPermitId,
		       NewPermitWaterWithdrawalPurposeId,
			   CASE WHEN NewPermitWwGroundWaterDetailId IS NULL THEN 2 ELSE 1 END,
			   CASE WHEN NewPermitWwGroundWaterDetailId IS NULL THEN NewPermitWwSurfaceWaterDetailId ELSE NewPermitWwGroundWaterDetailId END,
			   @CreatedBy,
			   GETDATE(),
			   @LastModifiedBy,
			   GETDATE()
		FROM #Mapper


	  -- clone Permit County
	  insert into PermitCounty
	  select [CountyId],@NewPermitId,[RequiresSignOff],[CountySignOff],[CountySignOffExplanation] ,[CountySignOffContactTitle],
             [CountySignOffContact] ,[CountySignOffDate] ,[UsePercent], @CreatedBy ,GETDATE() ,@LastModifiedBy ,GETDATE()
	  from PermitCounty
	  where PermitId = @PermitId
     
     
      -- Drop temp tables 
      DROP TABLE #Mapper
      
	  -- Return id of cloned permit
	  Select @NewPermitId
END

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
END
ELSE
BEGIN
	COMMIT TRANSACTION
END