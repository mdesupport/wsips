﻿CREATE PROCEDURE [dbo].[sp_EditWaterWithdrawalLocation]
(
	@id int,
	@withdrawalSourceId int,
	@withdrawalTypeId int,
	@selectedPermitWaterWithdrawalPurposeIds varchar(max),
	@stateStreamId int,
	@otherStreamName varchar(500),
	@intakeLocation varchar(max),
	@isExistingWell bit,
	@wellDepth int,
	@wellDiameter int,
	@wellTagNo varchar(20),
	@comment varchar(max),
	@user varchar(100)
)
AS

BEGIN TRANSACTION

	IF (@id IS NULL OR @user IS NULL OR @user = '')
	BEGIN
		ROLLBACK TRANSACTION
		SELECT 0 AS WithdrawalLocationObjectId
		RETURN
	END


	-- Retrieve the existing Groundwater/Surfacewater values
	declare @permitId int
	declare @existingWithdrawalSourceId int
	declare @existingPermitWithdrawalId int
	SELECT @permitId = PermitId, @existingWithdrawalSourceId = WaterWithdrawalSourceId, @existingPermitWithdrawalId = PermitWithdrawalId
	FROM MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT
	WHERE OBJECTID = @id


	-- Add/Edit Groundwater/Surfacewater locations
	declare @permitWithdrawalId int
	IF @withdrawalSourceId = 1 -- Groundwater
	BEGIN
		IF @withdrawalSourceId = @existingWithdrawalSourceId -- Same.  All we need to do is edit the existing record
		BEGIN
			UPDATE MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
			SET [GroundwaterTypeId] = @withdrawalTypeId,
				[IsExisting] = @isExistingWell,
				[WellTagNumber] = @wellTagNo,
				[WellDepthFeet] = @wellDepth,
				[WellDiameterInches] = @wellDiameter,
				[Comment] = @comment,
				[LastModifiedBy] = @user,
				[LastModifiedDate] = GETDATE()
			WHERE Id = @existingPermitWithdrawalId

			SET @permitWithdrawalId = @existingPermitWithdrawalId
		END
		ELSE -- Different.  We need to add a groundwater and drop the surface water
		BEGIN
			declare @permitWithdrawalGroundwaterCount int;
			SELECT @permitWithdrawalGroundwaterCount = COUNT(*) FROM MdeWaterPermitting..PermitWithdrawalGroundwater
			WHERE PermitId = @permitId

			IF @permitWithdrawalGroundwaterCount = 0
			BEGIN
				INSERT INTO MdeWaterPermitting..PermitWithdrawalGroundwater
					([PermitId], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
					VALUES (@permitId, @user, GETDATE(), @user, GETDATE())
			END

			DECLARE @permitWithdrawalGroundwaterId int
			SELECT TOP 1 @permitWithdrawalGroundwaterId = Id FROM MdeWaterPermitting..PermitWithdrawalGroundwater
			WHERE PermitId = @permitId

			INSERT INTO MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
			(
				[GroundwaterTypeId],
				[PermitWithdrawalGroundwaterId],
				[WatershedId],
				[CountyId],
				[IsExisting],
				[WellTagNumber],
				[WellDepthFeet],
				[WellDiameterInches],
				[Comment],
				[TaxMapNumber],
				[TaxMapBlockNumber],
				[TaxMapSubBlockNumber],
				[TaxMapParcel],
				[LotNumber],
				[Street],
				[City],
				[State],
				[Zip],
				[CreatedBy],
				[CreatedDate],
				[LastModifiedBy],
				[LastModifiedDate]
			)
			SELECT @withdrawalTypeId,
			       @permitWithdrawalGroundwaterId,
				   [WatershedId],
				   [CountyId],
				   @isExistingWell,
				   @wellTagNo,
				   @wellDepth,
				   @wellDiameter,
				   @comment,
				   [TaxMapNumber],
				   [TaxMapBlockNumber],
				   [TaxMapSubBlockNumber],
				   [TaxMapParcel],
				   [LotNumber],
				   [Street],
				   [City],
				   [State],
				   [Zip],
				   @user,
				   GETDATE(),
				   @user,
				   GETDATE()
			FROM MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail
			WHERE Id = @existingPermitWithdrawalId

			SET @permitWithdrawalId = SCOPE_IDENTITY()


			-- Perform the drop
			declare @surfaceWaterWithdrawalCount int
			DELETE FROM MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail
			WHERE Id = @existingPermitWithdrawalId

			SELECT @surfaceWaterWithdrawalCount = COUNT(*)
			FROM	MdeWaterPermitting..PermitWithdrawalSurfacewater PWSW INNER JOIN
					MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail PWSWD ON PWSW.Id = PWSWD.PermitWithdrawalSurfacewaterId
			WHERE PWSW.PermitId = @permitId

			IF @surfaceWaterWithdrawalCount = 0 -- No more detail records exist, so delete the parent record
			BEGIN
				DELETE FROM MdeWaterPermitting..PermitWithdrawalSurfacewater
				WHERE PermitId = @permitId
			END
		END
	END
	ELSE IF @withdrawalSourceId = 2 -- Surface Water
	BEGIN
		IF @withdrawalSourceId = @existingWithdrawalSourceId -- Same.  All we need to do is edit the existing record
		BEGIN
			UPDATE MdeWaterPermitting..PermitWithdrawalSurfacewaterDetail
			SET [SurfacewaterTypeId] = @withdrawalTypeId,
				[StateStreamId] = @stateStreamId,
				[OtherStreamName] = @otherStreamName,
				[ExactLocationOfIntake] = @intakeLocation,
				[Comment] = @comment,
				[LastModifiedBy] = @user,
				[LastModifiedDate] = GETDATE()
			WHERE Id = @existingPermitWithdrawalId

			SET @permitWithdrawalId = @existingPermitWithdrawalId
		END
		ELSE -- Different.  We need to add a surface water and drop the groundwater
		BEGIN
			declare @permitWithdrawalSurfacewaterCount int
			SELECT @permitWithdrawalSurfacewaterCount = COUNT(*) FROM MdeWaterPermitting..PermitWithdrawalSurfacewater
			WHERE PermitId = @permitId

			IF @permitWithdrawalSurfacewaterCount = 0
			BEGIN
				INSERT INTO MdeWaterPermitting..PermitWithdrawalSurfacewater
					([PermitId], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
					VALUES (@permitId, @user, GETDATE(), @user, GETDATE())
			END

			DECLARE @permitWithdrawalSurfacewaterId int
			SELECT TOP 1 @permitWithdrawalSurfacewaterId = Id FROM MdeWaterPermitting..PermitWithdrawalSurfacewater
			WHERE PermitId = @permitId

			INSERT INTO MdeWaterPermitting..PermitWithdrawalSurfaceWaterDetail
			(
				[SurfaceWaterTypeId],
				[PermitWithdrawalSurfacewaterId],
				[WatershedId],
				[StateStreamId],
				[OtherStreamName],
				[CountyId],
				[ExactLocationOfIntake],
				[Comment],
				[TaxMapNumber],
				[TaxMapBlockNumber],
				[TaxMapSubBlockNumber],
				[TaxMapParcel],
				[LotNumber],
				[Street],
				[City],
				[State],
				[Zip],
				[CreatedBy],
				[CreatedDate],
				[LastModifiedBy],
				[LastModifiedDate]
			)
			SELECT @withdrawalTypeId,
			       @permitWithdrawalSurfacewaterId,
				   [WatershedId],
				   @stateStreamId,
				   @otherStreamName,
				   [CountyId],
				   @intakeLocation,
				   @comment,
				   [TaxMapNumber],
				   [TaxMapBlockNumber],
				   [TaxMapSubBlockNumber],
				   [TaxMapParcel],
				   [LotNumber],
				   [Street],
				   [City],
				   [State],
				   [Zip],
				   @user,
				   GETDATE(),
				   @user,
				   GETDATE()
			FROM MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
			WHERE Id = @existingPermitWithdrawalId

			SET @permitWithdrawalId = SCOPE_IDENTITY()

			
			-- Perform the drop
			declare @groundWaterWithdrawalCount int
			DELETE FROM MdeWaterPermitting..PermitWithdrawalGroundwaterDetail
			WHERE Id = @existingPermitWithdrawalId

			SELECT @groundWaterWithdrawalCount = COUNT(*)
			FROM	MdeWaterPermitting..PermitWithdrawalGroundwater PWGW INNER JOIN
					MdeWaterPermitting..PermitWithdrawalGroundwaterDetail PWGWD ON PWGW.Id = PWGWD.PermitWithdrawalGroundwaterId
			WHERE PWGW.PermitId = @permitId

			IF @groundWaterWithdrawalCount = 0 -- No more detail records exist, so delete the parent record
			BEGIN
				DELETE FROM MdeWaterPermitting..PermitWithdrawalGroundwater
				WHERE PermitId = @permitId
			END
		END
	END


	-- Delete existing use codes for this withdrawal
	DELETE FROM MdeWaterPermitting..WaterWithdrawalLocationPurpose
	WHERE PermitId = @permitId AND WaterWithdrawalSourceId = @existingWithdrawalSourceId and PermitWithdrawalId = @existingPermitWithdrawalId
	
	-- Associate our use codes (withdrawal purposes) with the appropriate surface water or groundwater record
	IF ISNULL(@selectedPermitWaterWithdrawalPurposeIds, '') != ''
	BEGIN
		INSERT INTO MdeWaterPermitting..WaterWithdrawalLocationPurpose
			SELECT @permitId, Id, @withdrawalSourceId, @permitWithdrawalId, @user, GETDATE(), @user, GETDATE()
			FROM dbo.udfSplitIDs(@selectedPermitWaterWithdrawalPurposeIds, ',')
	END


	-- Finally, edit properties associated with the spatial table
	UPDATE MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT
	SET WaterWithdrawalSourceId = @withdrawalSourceId,
	    PermitWithdrawalId = @permitWithdrawalId,
		LastModifiedBy = @user,
		LastModifiedDate = GETDATE()
	WHERE OBJECTID = @id
	
	
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	SELECT 0 AS WithdrawalLocationObjectId
	RETURN
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT @id AS WithdrawalLocationObjectId
	RETURN
END