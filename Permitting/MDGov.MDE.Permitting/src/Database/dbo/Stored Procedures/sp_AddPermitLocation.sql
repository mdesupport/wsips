﻿CREATE PROCEDURE [dbo].[sp_AddPermitLocation]
(
	@permitId int,
	@withdrawalLocationX float,
	@withdrawalLocationY float,
	@spatialReferenceId int,
	@user varchar(100),
	@suppressOutput bit = 0,
	@countyId int output,
	@permitLocationOID int output,
	@taxMapNumber nvarchar(12) output,
	@taxMapBlockNumber nvarchar(12) output,
	@taxMapSubBlockNumber nvarchar(12) output,
	@taxMapParcel nvarchar(12) output,
	@lotNumber nvarchar(12) output,
	@parcelStreet nvarchar(500) output,
	@parcelCity nvarchar(500) output,
	@parcelState nvarchar(10) output,
	@parcelZip nvarchar(10) output
)
AS

BEGIN TRANSACTION

	IF (@withdrawalLocationX IS NULL OR @withdrawalLocationY IS NULL OR @spatialReferenceId IS NULL OR @user IS NULL OR @user = '')
	BEGIN
		ROLLBACK TRANSACTION
		IF @suppressOutput = 0
		BEGIN
			SELECT 0 AS PermitLocationObjectId
		END
		RETURN
	END


	-- Create point geometry
	declare @withdrawalLocation geometry
	SET @withdrawalLocation = geometry::Point(@withdrawalLocationX, @withdrawalLocationY, @spatialReferenceId);


	-- Determine County
	declare @countyParcelTableName nvarchar(255);
	SELECT @countyId = WSIPS_ID, @countyParcelTableName = ParcelFeatureTableName FROM MdeWsipsSpatial.gis_adm.MIDATLNCNTYS WHERE Shape.STIntersects(@withdrawalLocation) = 1


	-- Add Permit Parcel, if necessary
	-- Logic to determine which county parcel table to query parcels goes here
	declare @parcelId varchar(50);
	declare @parcelGeom geometry;
	
	declare @sql nvarchar(max);
	declare @params nvarchar(max);
	
	select @sql = N'SELECT @parcelIdOUT = ACCTID,
						@taxMapNumberOUT = MAP,
						@taxMapBlockNumberOUT = GRID,
						@taxMapSubBlockNumberOUT = BLOCK,
						@taxMapParcelOUT = PARCEL,
						@lotNumberOUT = LOT,
						@parcelGeomOUT = Shape,
						@parcelStreetOUT = MdeWaterPermitting.dbo.udfProperCase(CONCAT(LTRIM(RTRIM(PREMSNUM)), '' '', LTRIM(RTRIM(PREMSDIR)), '' '', LTRIM(RTRIM(PREMSNAM)), '' '', LTRIM(RTRIM(PREMSTYP)))),
						@parcelCityOUT = MdeWaterPermitting.dbo.udfProperCase(LTRIM(RTRIM(PREMCITY))),
						@parcelStateOUT = ''MD'',
						@parcelZipOUT = LTRIM(RTRIM(PREMZIP))
					FROM MdeWsipsSpatial.gis_adm.' + quotename(@countyParcelTableName) + ' 
					WHERE Shape.STIntersects(@withdrawalLocationIN) = 1'
	
	select @params = N'
		@withdrawalLocationIN geometry,
		@parcelIdOUT varchar(50) OUTPUT,
		@taxMapNumberOUT nvarchar(12) OUTPUT,
		@taxMapBlockNumberOUT nvarchar(12) OUTPUT,
		@taxMapSubBlockNumberOUT nvarchar(12) OUTPUT,
		@taxMapParcelOUT nvarchar(12) OUTPUT,
		@lotNumberOUT nvarchar(12) OUTPUT,
		@parcelStreetOUT nvarchar(500) OUTPUT,
		@parcelCityOUT nvarchar(500) OUTPUT,
		@parcelStateOUT nvarchar(10) OUTPUT,
		@parcelZipOUT nvarchar(10) OUTPUT,
		@parcelGeomOUT geometry OUTPUT'

	exec sp_executesql
	@sql,
	@params,
	@withdrawalLocationIN = @withdrawalLocation,
	@parcelIdOUT = @parcelId OUTPUT,
	@taxMapNumberOUT = @taxMapNumber OUTPUT,
	@taxMapBlockNumberOUT = @taxMapBlockNumber OUTPUT,
	@taxMapSubBlockNumberOUT = @taxMapSubBlockNumber OUTPUT,
	@taxMapParcelOUT = @taxMapParcel OUTPUT,
	@lotNumberOUT = @lotNumber OUTPUT,
	@parcelStreetOUT = @parcelStreet OUTPUT,
	@parcelCityOUT = @parcelCity OUTPUT,
	@parcelStateOUT = @parcelState OUTPUT,
	@parcelZipOUT = @parcelZip OUTPUT,
	@parcelGeomOUT = @parcelGeom OUTPUT

	IF ISNULL(@parcelId, '') != ''
	BEGIN
		SELECT @permitLocationOID = OBJECTID FROM MdeWsipsSpatial.gis_adm.PERMITLOCATION
		WHERE PermitId = @permitId and ParcelId = @parcelId

		IF @permitLocationOID IS NULL
		BEGIN
			INSERT INTO MdeWaterPermitting..PermitParcel
				([PermitId], [ParcelId], [TaxMapNumber], [TaxMapBlockNumber], [TaxMapSubBlockNumber], [TaxMapParcel], [LotNumber], [Street], [City], [State], [Zip], [Shape], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
				VALUES (@permitId, @parcelId, @taxMapNumber, @taxMapBlockNumber, @taxMapSubBlockNumber, @taxMapParcel, @lotNumber, @parcelStreet, @parcelCity, @parcelState, @parcelZip, @parcelGeom.STCentroid(), @user, GETDATE(), @user, GETDATE())

			-- Insert our permitlocation, which is simply the parcel copied over from the county's parcel data
			EXEC MdeWsipsSpatial.[gis_adm].[sp_getNextObjectID]
				@TableName = N'PERMITLOCATION',
				@newOID = @permitLocationOID OUTPUT

			INSERT INTO MdeWsipsSpatial.gis_adm.PERMITLOCATION
				([OBJECTID], [PermitId], [ParcelId], [Shape], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])
				VALUES (@permitLocationOID, @permitId, @parcelId, @parcelGeom, @user, GETDATE(), @user, GETDATE())
		END
	END


IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	IF @suppressOutput = 0
	BEGIN
		SELECT 0 AS PermitLocationObjectId
	END
	RETURN
END
ELSE
BEGIN
	COMMIT TRANSACTION
	IF @suppressOutput = 0
	BEGIN
		SELECT @permitLocationOID AS PermitLocationObjectId
	END
	RETURN
END