﻿CREATE PROCEDURE [dbo].[sp_MergeContacts]
       @FromContactId  int,
       @ToContactId  int
AS
BEGIN
       BEGIN TRY
              BEGIN TRANSACTION MergeContact
              -- SET NOCOUNT ON added to prevent extra result sets from
              -- interfering with SELECT statements.
              SET NOCOUNT ON;

              -- Merge Supervisor
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..Supervisor' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..Supervisor
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..Supervisor SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId

			  -- Merge Enforcement
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..Enforcement' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..Enforcement
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..Enforcement SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId

			  -- Merge DivisionChief
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..DivisionChief' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..DivisionChief
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..DivisionChief SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId

			  -- Merge ProjectManager
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..ProjectManager' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..ProjectManager
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..ProjectManager SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId


			  -- Merge PumpageReport
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..PumpageReport' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..PumpageReport
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..PumpageReport SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId


			-- Merge Secretary
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..Secretary' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..Secretary
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..Secretary SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId


			-- Merge ContactCommunicationMethod
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..ContactCommunicationMethod' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..ContactCommunicationMethod
              WHERE ContactId = @FromContactId

              -- remove the IsPrimaryEmail and IsPrimaryPhone from the contact being merged from
			  UPDATE MdeWaterPermitting..ContactCommunicationMethod SET IsPrimaryEmail = 0, IsPrimaryPhone = 0 WHERE  ContactId  = @FromContactId
              UPDATE MdeWaterPermitting..ContactCommunicationMethod SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId


			-- Merge ComplianceManager
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..ComplianceManager' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..ComplianceManager
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..ComplianceManager SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId


			  -- Merge PermitContact
              INSERT INTO ContactMergeHistory
              SELECT 'MdeWaterPermitting..PermitContact' TableName, Id PrimaryId, 
                      @FromContactId OldContactId, @ToContactId NewContactId
              FROM   MdeWaterPermitting..PermitContact
              WHERE ContactId = @FromContactId

              UPDATE MdeWaterPermitting..PermitContact SET ContactId  = @ToContactId WHERE  ContactId  = @FromContactId





			  -- make a copy of the old contact
              INSERT INTO ContactHistory
					([Id],[ContactTypeId] ,[CountryId] ,[CountyId] ,[DivisionId] ,[StateId] ,[TitleId]
					  ,[UserId] ,[Salutation] ,[FirstName] ,[MiddleInitial] ,[LastName] ,[SecondaryName] ,[BusinessName]
					  ,[Address1] ,[Address2] ,[City] ,[ZipCode] ,[Subdivision] ,[IsBusiness] ,[Office] ,[ApplicationNotification]
					  ,[EmailNotification] ,[Active] ,[CreatedBy] ,[CreatedDate] ,[LastModifiedBy] ,[LastModifiedDate])
              SELECT [Id],[ContactTypeId] ,[CountryId] ,[CountyId] ,[DivisionId] ,[StateId] ,[TitleId]
					  ,[UserId] ,[Salutation] ,[FirstName] ,[MiddleInitial] ,[LastName] ,[SecondaryName] ,[BusinessName]
					  ,[Address1] ,[Address2] ,[City] ,[ZipCode] ,[Subdivision] ,[IsBusiness] ,[Office] ,[ApplicationNotification]
					  ,[EmailNotification] ,[Active] ,[CreatedBy] ,[CreatedDate] ,[LastModifiedBy] ,[LastModifiedDate] 
              FROM MdeWaterPermitting..Contact
              WHERE Id = @FromContactId

              -- delete the old contact
              DELETE FROM MdeWaterPermitting..Contact
              WHERE Id = @FromContactId



              COMMIT
       END TRY
       BEGIN CATCH
              -- An error has occurred
              IF @@TRANCOUNT > 0
                     ROLLBACK
                     
              -- Raise an error with the details of the exception
              DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
              SELECT @ErrMsg = ERROR_MESSAGE(),
                     @ErrSeverity = ERROR_SEVERITY()
                     
              RAISERROR(@ErrMsg, @ErrSeverity, 1)
       END CATCH
END