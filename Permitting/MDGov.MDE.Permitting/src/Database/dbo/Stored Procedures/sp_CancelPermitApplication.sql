﻿CREATE PROCEDURE [dbo].[sp_CancelPermitApplication]
(
	@permitId int
)
AS

BEGIN TRANSACTION

	-- Check whether we have a valid candidate for cancelling.
	IF (@permitId IS NULL OR (SELECT COUNT(*) FROM Permit WHERE Id = @permitId AND PermitStatusId = 1) = 0)
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END

	-- Remove PermitContacts. PermitContactInformation records will be removed via cascade delete.
	DELETE FROM PermitContact WHERE PermitId = @permitId

	-- Remove WaterWithdrawalLocationPurposes.
	DELETE FROM WaterWithdrawalLocationPurpose WHERE PermitId = @permitId

	-- Remove PermitWaterWithdrawalPurposes.  Specific use types will be removed via cascade delete.
	DELETE FROM PermitWaterWithdrawalPurpose WHERE PermitId = @permitId
	
	-- Remove the WITHDRAWALLOCATIONPOINT
	DELETE FROM MdeWsipsSpatial.gis_adm.WITHDRAWALLOCATIONPOINT WHERE PermitId = @permitId

	-- Remove PermitWithdrawalGroundwater and PermitWithdrawalSurfacewater.  The detail records will be removed
	-- via cascade delete.
	DELETE FROM PermitWithdrawalGroundwater WHERE PermitId = @permitId
	DELETE FROM PermitWithdrawalSurfacewater WHERE PermitId = @permitId

	-- Remove the PermitParcel and PermitLocation records.
	DELETE FROM PermitParcel WHERE PermitId = @permitId
	DELETE FROM MdeWsipsSpatial.gis_adm.PERMITLOCATION WHERE PermitId = @permitId
		
	-- Remove any PermitCounty Records
	DELETE FROM PermitCounty WHERE PermitId = @permitId

	-- Remove actual Permit record.
	DELETE FROM Permit WHERE Id = @permitId

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END
ELSE
BEGIN
	COMMIT TRANSACTION
	RETURN
END

-- exec sp_CancelPermitApplication 52645