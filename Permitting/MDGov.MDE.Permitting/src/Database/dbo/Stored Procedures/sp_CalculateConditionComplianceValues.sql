﻿CREATE PROCEDURE [dbo].[sp_CalculateConditionComplianceValues]
(@permitId int, @year int)
AS
BEGIN

       --exec dbo.sp_CalculateConditionComplianceValues 2628, 2012 -- no supplemental group
       --exec dbo.sp_CalculateConditionComplianceValues 6408, 2012 -- supplemental group 6408 
       --exec dbo.sp_CalculateConditionComplianceValues 52777, 2013 -- supplemental group 6408 
       --exec dbo.sp_CalculateConditionComplianceValues 60900, 2012 

       -- full year one permit
       --declare @permitId int, @year int
       --set @permitid = 789
       --set @year = 1997

       -- 1/2 year (beginning) one permit
       --declare @permitId int, @year int
       --set @permitid = 60879
       --set @year = 2014

       -- get all of the condition compliance records
       select p.Id PermitId, cc.Id ConditionComplianceId, 
                                  pr.Id PumpageReportId
                                  , prd.MonthId, cast(prd.Gallons as float) Gallons
                                  , cast(isnull(pwg.AvgGalPerDay,0) + isnull(psw.AvgGalPerDay,0) as float) AvgGalPerDay
                                  , cast(isnull(pwg.MaxGalPerDay,0) + isnull(psw.MaxGalPerDay,0) as float) MaxGalPerDay
       into #temp
       from   Permit p
                     inner join 
                                  (select       PermitId
                                  from   PermitSupplementalGroup
                                  where  supplementalgroupid in
                                         (select       supplementalgroupid
                                         from   PermitSupplementalGroup
                                         where  permitid = @permitId)
                                                       and SupplementalTypeId = 2
                                  union  
                                  select @permitId) permits on permits.PermitId = p.Id
                     left outer join PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
                     left outer join PermitWithdrawalSurfacewater psw on psw.PermitId = p.Id
                     inner join PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14
                     inner join ConditionCompliance cc on pc.id = cc.PermitConditionId and year(compliancereportingenddate) = @year
                     left outer join PumpageReport pr on pr.ConditionComplianceId = cc.Id and pr.Active = 1
                     left outer join PumpageReportDetail prd on prd.PumpageReportId = pr.Id

/*
       select      *
       from #temp
       where       gallons > 0
       drop table #temp
*/

       /* Calculate the Period Usage and Average for the Specified Permit first */

                     -- the number of days needs to be 365 if there is one annual or 2 semi annual reports per permit
                     -- or 182.5 is only one semi-annual report period is reported
                     --DECLARE @numberOfDays float
                     --select @numberOfDays = case when count(*) >= 12 then 365 else 182.5 end
                     --from (select distinct MonthId from #temp where permitid = @permitid) t
       
                     -- first get the total number of gallons used for the specified permit
                     --DECLARE @totalUsage float
                     --select @totalUsage = isnull(sum(gallons),0) from #temp where permitid = @permitid

                     -- update the compliance records
                     UPDATE ConditionCompliance
                     SET           ReportAverageUsage =  t.TotalGallons/ case when ConditionReportingPeriodId = 1 then 365 else 182.5 end,
                                  ReportTotalUsage = t.TotalGallons ,
                                  LastModifiedBy = 'sp_CalculateConditionComplianceValues',
                                  LastModifiedDate = GETDATE()
                     FROM   ConditionCompliance cc
                           INNER JOIN (SELECT   cc.Id ConditionComplianceId, isnull(sum(gallons),0) TotalGallons, pc.ConditionReportingPeriodId 
                                                FROM   ConditionCompliance cc
                                                       INNER JOIN PermitCondition pc on pc.Id = cc.PermitConditionId 
                                                       INNER JOIN #temp t on t.ConditionComplianceId = cc.Id
                                                WHERE  cc.Id in (select DISTINCT ConditionComplianceId FROM #temp where permitid = @permitid)
                                                GROUP BY cc.Id, pc.ConditionReportingPeriodId ) t on t.ConditionComplianceId = cc.Id 

                     --select      cc.*
                     --from Permit p
                     --     inner join PermitCondition pc on pc.PermitId = p.Id and StandardConditionTypeId = 14
                     --     inner join ConditionCompliance cc on pc.id = cc.PermitConditionId and year(compliancereportingenddate) = 2012
                     --where       p.Id = 2628



    /* *********************************************************** */
       -- check to see if every compliance condition for the time period has a pumpage report turned in
       if (select count(*) from #temp where PumpageReportId is null) = 0
       BEGIN

                     -- now get the total number of gallons used across all permits
                     DECLARE @totalUsage float
                     select @totalUsage = sum(gallons) from #temp

                     -- the number of days needs to be 365 if there is one annual or 2 semi annual reports per permit
                     -- or 182.5 is only one semi-annual report period is reported
                     DECLARE @numberOfDays float
                     select @numberOfDays = case when count(*) >= 12 then 365 else 182.5 end
                     from (select distinct MonthId from #temp) t
             
                     --debug
                     --select      @numberOfDays
              
                     -- old code
                     --select      @numberOfDays = sum(DaysInMonth) 
                     --from (
                     --            select dbo.[DaysInMonth] (MonthId, @year) DaysInMonth
                     --            from   #temp
                     --            where  gallons > 0
                     --            group by monthid) t

                     -- get the average gallons per day allowed for all permits in the group
                     DECLARE @avgGallonsPerDayAllowed float
                     select @avgGallonsPerDayAllowed = sum(Total)
                     from   (select       permitid, max(AvgGalPerDay) Total
                                         from   #temp
                                         group by permitid) t

                     -- calculate annual Overuse
                     DECLARE @annualOveruse float
                     select @annualOveruse = ROUND(dbo.MathMax(0, CASE WHEN @avgGallonsPerDayAllowed = 0 THEN (@totalUsage/@numberOfDays) ELSE (((@totalUsage/@numberOfDays) / @avgGallonsPerDayAllowed) - 1) END) * 100, 3)
                     -- select     @totalUsage, @numberOfDays, @avgGallonsPerDayAllowed, @annualOveruse 

                     -- get the max gallons per day allowed
                     DECLARE @maxGallonsPerDayAllowed float
                     select @maxGallonsPerDayAllowed = sum(Total)
                     from   (select       permitid, max(MaxGalPerDay) Total
                                         from   #temp
                                         group by permitid) t

                     -- get the highest month and highest monthly average
                     DECLARE @HighMonth int
                     DECLARE @HighMonthAvgGalPerDay float
                     select TOP 1 @HighMonthAvgGalPerDay = Gallons/DaysInMonth, @HighMonth = MonthId
                     from   (select       top 1 MonthId, sum(gallons) Gallons, dbo.[DaysInMonth] (MonthId, @year) DaysInMonth
                                  from   #temp
                                  where  gallons > 0
                                  group by MonthId
                                  order by Gallons desc, daysInMonth desc) t

                     -- calculate monthly overuse
                     DECLARE @monthlyOveruse float
                     select TOP 1 @monthlyOveruse = ROUND(dbo.MathMax(0, CASE WHEN @maxGallonsPerDayAllowed = 0 THEN @HighMonthAvgGalPerDay ELSE ((@HighMonthAvgGalPerDay / @maxGallonsPerDayAllowed) - 1) END) * 100, 3)

                     -- update all records with the compliance 
                     UPDATE ConditionCompliance
                     SET          AnnualAverageOverusePercentage = isnull(@annualOveruse,0),
                                         MonthlyMaximumOverusePercentage = isnull(@monthlyOveruse,0),
                                         AnnualTotalUsage = @totalUsage,
                                         AnnualAverageUsage = @totalUsage/@numberOfDays,
                                         HighMonth = @HighMonth,
                                         HighMonthAvgGalPerDay = @HighMonthAvgGalPerDay, 
                                         LastModifiedBy = 'sp_CalculateConditionComplianceValues',
                                         LastModifiedDate = GETDATE()
                     WHERE  Id in (select DISTINCT ConditionComplianceId FROM #temp)
       
                     -- diagnostic
                     --select @totalUsage,@numberOfDays, @avgGallonsPerDayAllowed, @annualOveruse, @maxGallonsPerDayAllowed, @monthlyOveruse
                     -- select     @monthlyOveruse, @annualOveruse
                                  --select      *
                                  --from ConditionCompliance 
                                  --where       Id in (select ConditionComplianceId from #temp)
                     -- diagnostic
                                  --select      *
                                  --from #temp
                                  --where       gallons > 0
                                  --drop table #temp
       
       END
       ELSE
       BEGIN
                     -- update all records with the compliance 
                     UPDATE ConditionCompliance
                     SET          AnnualAverageOverusePercentage = 0,
                                         MonthlyMaximumOverusePercentage = 0,
                                         AnnualTotalUsage = 0,
                                         AnnualAverageUsage = 0,
                                         HighMonth = 0,
                                         HighMonthAvgGalPerDay = 0,
                                         LastModifiedBy = 'sp_CalculateConditionComplianceValues',
                                         LastModifiedDate = GETDATE()
                     WHERE  Id in (select DISTINCT ConditionComplianceId FROM #temp)
       END

END
