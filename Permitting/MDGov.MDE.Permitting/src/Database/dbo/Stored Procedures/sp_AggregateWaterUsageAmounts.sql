﻿CREATE PROCEDURE [dbo].[sp_AggregateWaterUsageAmounts]
	@permitId int,
	@user varchar(100)
AS
BEGIN
	declare @WaterWithdrawalSourceId int;

	--Surface Water
	declare @SurfaceWaterAvgGalPerDay bigint; 
	set @SurfaceWaterAvgGalPerDay = 0;
	declare @SurfaceWaterMaxGalPerDay bigint;
	set @SurfaceWaterMaxGalPerDay = 0;

	-- Ground Water
	declare @GroundWaterAvgGalPerDay bigint; 
	set @GroundWaterAvgGalPerDay = 0;
	declare @GroundWaterMaxGalPerDay bigint;
	set @GroundWaterMaxGalPerDay = 0;

	select @GroundWaterAvgGalPerDay = ISNULL(sum(AverageGallonPerDayFromGroundWater), 0), 
	       @SurfaceWaterAvgGalPerDay = ISNULL(sum(AverageGallonPerDayFromSurfaceWater), 0), 
		   @GroundWaterMaxGalPerDay = ISNULL(sum(MaximumGallonPerDayFromGroundWater), 0),
		   @SurfaceWaterMaxGalPerDay = ISNULL(sum(MaximumGallonPerDayFromSurfaceWater), 0)
	from   (
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwAgricultural paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
	    
	union all
	    
    select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater , paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
    from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwAquacultureOrAquarium paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
		
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwCommercialDrinkingOrSanitary paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
		
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwConstructionDewatering paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
			
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwFoodProcessing paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
			
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwGolfCourseIrrigation paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
			
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwInstitutionalEducational_drinkingOrsanitary paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
			
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwInstitutionalReligious_DringkingOrSanitary paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
			
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwNonAgricultureIrrigation paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
			
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwPrivateWaterSupplier paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId
			
	union all
		
	select paa.AverageGallonPerDayFromGroundWater, paa.AverageGallonPerDayFromSurfaceWater, paa.MaximumGallonPerDayFromGroundWater , paa.MaximumGallonPerDayFromSurfaceWater 
	from   Permit p 
			inner join PermitWaterWithdrawalPurpose pwp on pwp.PermitId = p.Id
			inner join PermitWwWildlifePonds paa on paa.PermitWaterWithdrawalPurposeId = pwp.Id
	where  p.Id = @permitId) AS T

    --select @GroundWaterAvgGalPerDay
	--select @GroundWaterMaxGalPerDay
	--select @SurfacewaterAvgGalPerDay
	--select @SurfacewaterMaxGalPerDay

	UPDATE [PermitWithdrawalGroundwater]
	SET [AvgGalPerDay] = @GroundWaterAvgGalPerDay,
		[MaxGalPerDay] = @GroundWaterMaxGalPerDay,
		[LastModifiedBy] = @user,
		[LastModifiedDate] = GETDATE()
	WHERE PermitId = @PermitId

	UPDATE [PermitWithdrawalSurfacewater]
	SET [AvgGalPerDay] = @SurfaceWaterAvgGalPerDay,
		[MaxGalPerDay] = @SurfaceWaterMaxGalPerDay,
		[LastModifiedBy] = @user,
		[LastModifiedDate] = GETDATE()
	WHERE PermitId = @permitId
END


-- exec sp_AggregateWaterUsageAmounts 51747, 0, 0, '', ''