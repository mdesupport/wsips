﻿-- ====================================================================
-- Author:           Dominic Cilento/Jon Harrison
-- Create date:		 2013-04-02
-- Description:      Generates a Report based on the input list of PermitIds
-- ====================================================================
CREATE PROCEDURE [dbo].[sp_GetPermitReport]
       @ReportParameterId  int
       
AS
BEGIN
       DECLARE @PermitIds nvarchar(max);

    SELECT @PermitIds = ReportParameterValue 
       FROM ReportParameter
       WHERE Id = @ReportParameterId;

       SELECT 
              IsNull(p.RamsWanActid, '')  AS ACTID
              , CASE
                     WHEN IsNull(RevisionNumber, '') <> ''
                     THEN p.PermitNumber + '(' + RevisionNumber + ')'
                     ELSE IsNull(p.PermitNumber, '')
              END AS PermitNumber
              , isnull([dbo].[GetContactName](2, p.Id), '') AS Applicant
              , IsNull(CONVERT(VARCHAR(10), p.ReceivedDate, 111), '') AS DateReceived
              , IsNull(L_PS.Description, '') AS PermitStatus
              , isnull([dbo].[GetContactName](20, p.Id), '') AS ProjectManager
              , IsNull(CONVERT(VARCHAR(10), p.EffectiveDate, 111), '') AS EffectiveDate
              , IsNull(CONVERT(VARCHAR(10), p.ExpirationDate, 111), '') AS ExpirationDate
              , IsNull(pwg.AvgGalPerDay, '') AS AllocatedAverageGPD 
              , IsNull(L_aqt.Description, '') AS ConfinedOrUnconfined
              , isnull([dbo].[GetContactName](24, p.Id), '') AS Supervisor
              , IsNull(cty.Description, '') AS County
              , IsNull(p.Location, '') AS StreetAddress
              , IsNull(gwd.TaxMapNumber, isnull(swd.TaxMapNumber, '')) AS TaxMapNumber
              , IsNull(gwd.TaxMapBlockNumber, isnull(swd.TaxMapBlockNumber, '')) AS TaxMapBlockNumber
              , IsNull(gwd.TaxMapParcel, isnull(swd.TaxMapParcel, '')) AS TaxMapParcel
              ----, IsNull(t6.NorthGrid, '') AS NorthGrid
              ----, IsNull(t6.EastGrid, '') AS EastGrid
              , IsNull(L_AT.Description, '') AS ApplicationType
              , IsNull(L_wp1.Description, '') AS UseType1
              , IsNull(wp1.UsePercent, '') AS UseType1Percent
              , IsNull(L_wp2.Description, '') AS UseType2
              , IsNull(wp2.UsePercent, '') AS UseType2Percent
              , IsNull(L_wp3.Description, '') AS UseType3
              , IsNull(wp3.UsePercent, '') AS UseType3Percent
              , IsNull(L_wp4.Description, '') AS UseType4
              , IsNull(wp4.UsePercent, '') AS UseType4Percent
              , IsNull(pwg.MaxGalPerDay, '') AS ReqAllocGPDMax
              , IsNull(L_wt.Description, '') AS FreshwaterOrSaltwater
              , CASE 
                     WHEN gwd.PermitId is not null
                     THEN 'Ground'
                     WHEN swd.PermitId is not null
                     THEN 'Surface'
                     ELSE NULL
              END AS SurfaceOrGround
              , IsNull(L_aq.[Key], '') AS AquiferCode
              , IsNull(L_aq.Description, '') AS AquiferName
              , IsNull(L_ss.[Key], '') AS StreamCode
              , IsNull(L_ss.Description, '') AS StreamName
             -- , IsNull(L_ws.[Key], '') AS BasinCode
			  , CASE 
                     WHEN gwd.PermitId is not null
                     THEN (SELECT [Key] FROM LU_Watershed WHERE Id = gwd.WatershedID)
                     WHEN swd.PermitId is not null
                     THEN (SELECT [Key] FROM LU_Watershed WHERE Id = swd.WatershedID)
                     ELSE NULL
              END AS BasinCode
       FROM   Permit p
              LEFT OUTER JOIN LU_PermitStatus L_PS on L_PS.Id = p.PermitStatusId
              LEFT OUTER JOIN LU_ApplicationType L_AT on L_AT.Id = p.ApplicationTypeId
              LEFT OUTER JOIN LU_WaterType L_wt ON L_wt.Id = p.WaterTypeId
              LEFT OUTER JOIN
                     (SELECT       pwg.PermitId, TaxMapNumber, TaxMapParcel, TaxMapBlockNumber, pwgd.WatershedId
                     FROM   PermitWithdrawalGroundwater pwg
                           INNER JOIN    PermitWithdrawalGroundwaterDetail pwgd
                                  ON pwgd.PermitWithdrawalGroundwaterId = pwg.Id
                                         AND    pwgd.Id = (   SELECT min(Id)
                                                                     FROM       PermitWithdrawalGroundwaterDetail
                                                                     WHERE  PermitWithdrawalGroundwaterId = pwgd.PermitWithdrawalGroundwaterId)) gwd ON gwd.PermitId = p.Id
              LEFT OUTER JOIN
                     (SELECT       pws.PermitId, TaxMapNumber, TaxMapParcel, TaxMapBlockNumber, pwsd.WatershedId
                     FROM   PermitWithdrawalSurfacewater pws
                           INNER JOIN    PermitWithdrawalSurfacewaterDetail pwsd
                                  ON pwsd.PermitWithdrawalSurfacewaterId = pws.Id
                                         AND    pwsd.Id = (   SELECT min(Id)
                                                                     FROM       PermitWithdrawalSurfacewaterDetail
                                                                     WHERE  PermitWithdrawalSurfacewaterId = pwsd.PermitWithdrawalSurfacewaterId)) swd ON swd.PermitId = p.Id
              LEFT OUTER JOIN
                     (SELECT       pc.PermitId, luc.Description
                     FROM   PermitCounty pc
                           INNER JOIN LU_County luc ON luc.Id = pc.CountyId) cty 
                     ON cty.PermitId = p.Id
              LEFT OUTER JOIN PermitWaterDispersement pwd on pwd.PermitId = p.Id and pwd.Id = (SELECT min(id) FROM PermitWaterDispersement pwd2 WHERE pwd2.PermitId = p.Id)
              LEFT OUTER JOIN LU_Aquifer L_aq on L_aq.Id = pwd.AquiferId
              LEFT OUTER JOIN LU_AquiferType L_aqt on L_aqt.Id = l_aq.AquiferTypeId 
              LEFT OUTER JOIN LU_StateStream L_ss on L_ss.Id = pwd.StateStreamId
              LEFT OUTER JOIN LU_Watershed L_ws on L_ws.Id = L_ss.WatershedId
              LEFT OUTER JOIN PermitWaterWithdrawalPurpose wp1 on wp1.PermitId = p.Id and wp1.Sequence = 1
              LEFT OUTER JOIN LU_WaterWithdrawalPurpose L_wp1 on L_wp1.Id = wp1.WaterWithdrawalPurposeId 
              LEFT OUTER JOIN PermitWaterWithdrawalPurpose wp2 on wp2.PermitId = p.Id and wp2.Sequence = 2
              LEFT OUTER JOIN LU_WaterWithdrawalPurpose L_wp2 on L_wp2.Id = wp2.WaterWithdrawalPurposeId 
              LEFT OUTER JOIN PermitWaterWithdrawalPurpose wp3 on wp3.PermitId = p.Id and wp3.Sequence = 3
              LEFT OUTER JOIN LU_WaterWithdrawalPurpose L_wp3 on L_wp3.Id = wp3.WaterWithdrawalPurposeId 
              LEFT OUTER JOIN PermitWaterWithdrawalPurpose wp4 on wp4.PermitId = p.Id and wp4.Sequence = 4
              LEFT OUTER JOIN LU_WaterWithdrawalPurpose L_wp4 on L_wp4.Id = wp4.WaterWithdrawalPurposeId 
              LEFT OUTER JOIN PermitWithdrawalGroundwater pwg on pwg.PermitId = p.Id
       
       WHERE p.Id IN (SELECT * FROM dbo.udfSplitIDs(@PermitIds, ','))
       ORDER BY PermitNumber; -- original

       -- DELETE FROM ReportParameter WHERE Id = @ReportParameterId;

END

GO


