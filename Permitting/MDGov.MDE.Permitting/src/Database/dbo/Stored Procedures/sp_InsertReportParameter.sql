﻿-- ====================================================================
-- Author:		Dominic Cilento
-- Create date: 2013-01-31
-- Description:	Inserts Report Paramter inot ReportParameter table
--				& returns the ReportParameterId
-- ====================================================================

CREATE PROCEDURE [dbo].[sp_InsertReportParameter]
		@PermitIds       varchar(max) --Change this name to be more generic
		
AS
BEGIN
		DECLARE @Sql	nvarchar(max);
		DECLARE @dbContext nvarchar(256) = 'MdeWaterPermitting'+'.dbo.'+'sp_executeSQL';
		DECLARE @ReportParameterId int;

		SET @Sql = 'INSERT INTO ReportParameter (ReportParameterValue) VALUES (''' + @PermitIds + ''');'
		
		EXEC @dbContext @Sql


		SELECT @ReportParameterId = Id 
		FROM ReportParameter 
		WHERE ReportParameterValue IN ( @PermitIds );  
		
		--EXEC ReportServer.dbo.sp_GetPermitReport @ReportParameterId

        PRINT @Sql;
	    PRINT @PermitIds;
		PRINT @ReportParameterId;
	
	SELECT @ReportParameterId
	--RETURN @@rowcount
	
END

GO