﻿CREATE VIEW [dbo].[PermitsFailedToReportTwoPeriods]
AS
	SELECT	p.*		
	FROM	(SELECT	PermitNumber, max(RevisionNumber) RevisionNumber
			FROM	(SELECT	PermitNumber, PermitConditionComplianceStatusId, RevisionNumber
					FROM	(SELECT	ROW_NUMBER() OVER ( PARTITION BY p1.PermitNumber ORDER BY cc.ComplianceReportingDueDate DESC ) AS 'RowNumber',
									p1.PermitNumber, cc.PermitConditionComplianceStatusId, p1.RevisionNumber
							FROM	Permit p1
								inner join PermitCondition pc on pc.PermitId = p1.Id and StandardConditionTypeId = 14 and pc.RequiresSelfReporting = 1
								inner join ConditionCompliance cc on pc.id = cc.PermitConditionId and cc.ComplianceReportingDueDate <= getdate() 
							) t
					WHERE	RowNumber <= 2
					) u
	
			WHERE	PermitConditionComplianceStatusId in (3,4) 
			GROUP BY PermitNumber
			HAVING COUNT(*) = 2
			) v
		INNER JOIN	Permit p on p.PermitNumber = v.PermitNumber and p.RevisionNumber = v.RevisionNumber
	WHERE PermitStatusId = 46
GO