﻿CREATE VIEW [DashboardPermit] AS 

SELECT	p.Id,
		p.PermitName,
		county.CountyCode,
		permittee.ContactId AS PermitteeContactId,
		permittee.Name AS PermitteeName,
		cat.[Description] AS PermitCategory,
		permitstatus.[Description] AS PermitStatus,
		CAST(relatedpermits.[Count] AS BIT) AS HasRelatedPermits,
		CASE WHEN p.PermitTypeId IS NULL THEN NULL WHEN p.PermitTypeId = 2 OR p.PermitTypeId = 4 THEN 1 ELSE 0 END AS IsLargePermit,
		p.RequiresAdvertising,
		CASE WHEN cat.Id IN (2, 3, 4, 5) AND p.ReceivedDate IS NOT NULL THEN DATEDIFF(day, p.ReceivedDate, GETDATE()) ELSE NULL END AS DaysPending,
		projectmanager.ContactId AS ProjectManagerContactId,
		projectmanager.Name AS ProjectManagerName,
		CASE WHEN cat.Id = 1 THEN p.SubmittedDate WHEN cat.Id IN (2,3,4,5,8) THEN p.ReceivedDate WHEN cat.Id IN(6,7) THEN p.DateOut END AS [Date]
FROM	Permit p INNER JOIN
		LU_PermitStatus permitstatus ON p.PermitStatusId = permitstatus.Id OUTER APPLY
		(
			SELECT TOP 1 cty.[Key] as CountyCode
			FROM	PermitCounty pc INNER JOIN
					LU_County cty ON pc.CountyId = cty.Id
			WHERE pc.PermitId = p.Id
		) county INNER JOIN
		LU_PermitCategory cat ON permitstatus.PermitCategoryId = cat.Id OUTER APPLY
		(
			SELECT TOP 1 pc.ContactId, CASE WHEN pci.IsBusiness = 1 THEN ISNULL(pci.BusinessName, '') ELSE ISNULL(pci.FirstName + ' ', '') + ISNULL(pci.LastName, '') END AS Name
			FROM	PermitContact pc INNER JOIN
					PermitContactInformation pci ON pc.PermitContactInformationId = pci.Id
			WHERE pc.IsPermittee = 1 AND pc.IsPrimary = 1 AND pc.PermitId = p.Id
		) permittee OUTER APPLY
		(
			SELECT TOP 1 pc.ContactId, ISNULL(c.FirstName + ' ', '') + ISNULL(c.LastName, '') AS Name
			FROM	PermitContact pc INNER JOIN
					Contact c ON pc.ContactId = c.Id
			WHERE pc.ContactTypeId = 20 AND pc.IsPrimary = 1 AND pc.PermitId = p.Id
		) projectmanager OUTER APPLY
		(
			SELECT	COUNT(*) as [Count]
			FROM	Permit
			WHERE	RefId = p.Id
		) relatedpermits

GO