﻿CREATE FUNCTION [dbo].[GetContactName] ( @ContactTypeId int, @PermitId int ) 
RETURNS VARCHAR(255)
AS
BEGIN

       DECLARE @OutputString   VARCHAR(255)

       -- look for applicant
       if @ContactTypeId = 2

              SELECT @OutputString = 
                     CASE 
                           WHEN IsNull(BusinessName, '') = '' AND IsNull(LastName, '') <> ''
                                  THEN dbo.Trim(Replace(IsNull(Salutation, '') + ' ' + IsNull(FirstName, '') + ' ' + IsNull(MiddleInitial, '') + ' ' + IsNull(LastName, ''), '  ', ' '))
                           WHEN IsNull(BusinessName, '') <> ''
                                  THEN IsNull(BusinessName, '')
                           WHEN IsNull(BusinessName, '') = '' AND IsNull(LastName, '') = ''
                                  THEN ''
                     END 
              FROM   Contact c INNER JOIN
                           PermitContact pc ON pc.ContactId = c.Id INNER JOIN
                           Permit p ON p.Id = pc.PermitId and p.Id = @PermitId
              WHERE  pc.IsApplicant = 1

       else

              SELECT @OutputString = 
                     CASE 
                           WHEN IsNull(BusinessName, '') = '' AND IsNull(LastName, '') <> ''
                                  THEN dbo.Trim(Replace(IsNull(Salutation, '') + ' ' + IsNull(FirstName, '') + ' ' + IsNull(MiddleInitial, '') + ' ' + IsNull(LastName, ''), '  ', ' '))
                           WHEN IsNull(BusinessName, '') <> ''
                                  THEN IsNull(BusinessName, '')
                           WHEN IsNull(BusinessName, '') = '' AND IsNull(LastName, '') = ''
                                  THEN ''
                     END 
              FROM   Contact c INNER JOIN
                           PermitContact pc ON pc.ContactId = c.Id INNER JOIN
                           Permit p ON p.Id = pc.PermitId and p.Id = @PermitId
              WHERE  pc.ContactTypeId = @ContactTypeId
              
              RETURN @OutputString

END
GO


