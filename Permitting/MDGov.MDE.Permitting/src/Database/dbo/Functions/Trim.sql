﻿
CREATE FUNCTION [dbo].[Trim] ( @InputString varchar(5000) ) 
RETURNS VARCHAR(5000)
AS
BEGIN

DECLARE @OutputString   VARCHAR(5000)

SET @OutputString = RTRIM(LTRIM(REPLACE(@InputString, '	', '')))
SET @OutputString = CASE 
						WHEN @OutputString = ''
						THEN Null
						ELSE @OutputString
					END	
	--BEGIN
	--	IF @OutputString = ''
	--		SET @OutputString = Null
	--	END
	--END

RETURN @OutputString

END