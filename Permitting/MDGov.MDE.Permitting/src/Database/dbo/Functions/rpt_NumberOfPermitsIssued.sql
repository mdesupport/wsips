﻿CREATE  Function [dbo].[rpt_NumberOfPermitsIssued](@beginDate  DATETIME ,@endDate  DATETIME,@effectiveDate int,@permitType VarChar(50),@ApplicationTypeId int,@appsOrPermits varchar(50)) 
returns @t Table(
	ContactId int null,
	permits int null
)
AS
BEGIN
		insert into @t
		select 
			pc.ContactId,
			count(pc.ContactId) as permits
		from PermitContact pc
			left outer join Permit p on p.id=pc.PermitId
		where 
			pc.ContactTypeId = 20 
			and pc.ContactId = any(select ContactId from ProjectManager) 
			and p.PermitTypeId is not null
			and ( 
				(@permitType is null AND (p.PermitTypeId = 1 or p.PermitTypeId = 2 or p.PermitTypeId = 3 or p.PermitTypeId = 4))
				OR (@permitType = 'small' AND (p.PermitTypeId = 1 or p.PermitTypeId = 3)) 
				OR (@permitType = 'large' AND (p.PermitTypeId = 2 or p.PermitTypeId = 4)))

			AND ((@beginDate is null AND @endDate is null) OR (p.createddate >= @beginDate AND p.createddate <= @endDate))
			AND ((@appsOrPermits = 'permits' and p.Permitstatusid=46) or (@appsOrPermits = 'apps' and p.Permitstatusid=any (  select id from lu_permitstatus where PermitCategoryId = 2 and id != 60 and id !=61)) )
			and p.ApplicationTypeId = @ApplicationTypeId
			--and DATEDIFF(day, p.EffectiveDate,GETDATE()) <= @effectiveDate
			and DATEDIFF(day, p.CreatedDate,p.EffectiveDate) <= @effectiveDate
		group by pc.ContactId

		return;
END

--select * from rpt_NumberOfPermitsIssued (null,null,12000,'large',1,'permits')