﻿CREATE FUNCTION [dbo].[DaysInMonth] (@monthId int, @year int)
RETURNS int
AS
BEGIN

	declare @days int
	declare @parsedDate datetime2

	--grab the first day of the month for that year
	set @parsedDate = CONVERT(datetime2, CAST(@monthId as varchar) + '/1/' + CAST(@year as varchar))

	--calculate the number of days
	select @days = datediff(day, dateadd(day, 1-day(@parsedDate), @parsedDate),
				  dateadd(month, 1, dateadd(day, 1-day(@parsedDate), @parsedDate)))
	
	return	@days

END