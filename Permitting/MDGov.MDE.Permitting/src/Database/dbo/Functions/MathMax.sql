﻿CREATE function [dbo].[MathMax](@val1 float, @val2 float)
returns float
as
begin
  if @val2 > @val1
    return @val2
  return isnull(@val1, @val2)
end