﻿CREATE FUNCTION [dbo].[udfSplitIDs](@String VARCHAR(max), @Delim CHAR(1))

RETURNS @output TABLE 
(Id int)
AS
BEGIN

IF RIGHT(@String, 1) <> @Delim SET @String = @String + @Delim

       WHILE CHARINDEX(@Delim, @String) > 0
              BEGIN
                   
                    INSERT INTO @output
                    VALUES (CAST(LTRIM(SUBSTRING(@String, 1, CHARINDEX(@Delim, @String) - 1)) AS INT))

                     IF CHARINDEX(@Delim, @String) = LEN(@String)
                     BEGIN
                           SET @String = ''
                     END

                     ELSE
                     BEGIN
                           SET @String = SUBSTRING(@String, CHARINDEX(@Delim, @String) + 1, LEN(@String))
                     END
              END

       RETURN

END