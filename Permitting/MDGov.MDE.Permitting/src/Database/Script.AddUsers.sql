﻿USE [MdeWaterPermitting]
GO
CREATE USER [MdeWSIPS] FOR LOGIN [MdeWSIPS] WITH DEFAULT_SCHEMA=[dbo]
GO
EXEC sp_addrolemember 'db_owner', 'MdeWSIPS'

CREATE USER [gis_adm] FOR LOGIN [gis_adm] WITH DEFAULT_SCHEMA=[dbo]
GO
EXEC sp_addrolemember 'db_owner', 'gis_adm'

GO