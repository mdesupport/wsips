﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

:r .\SeedScripts\Seed.LU_ApplicationPacketItem.sql
:r .\SeedScripts\Seed.LU_ApplicationType.sql
:r .\SeedScripts\Seed.LU_Aquifer.sql
:r .\SeedScripts\Seed.LU_AquiferType.sql
:r .\SeedScripts\Seed.LU_Watershed.sql
:r .\SeedScripts\Seed.LU_CheckListAnswer.sql
:r .\SeedScripts\Seed.LU_CommunicationMethod.sql
:r .\SeedScripts\Seed.LU_ConditionReportingPeriod.sql
:r .\SeedScripts\Seed.LU_ContactType.sql
:r .\SeedScripts\Seed.LU_Country.sql
:r .\SeedScripts\Seed.LU_State.sql
:r .\SeedScripts\Seed.LU_StateRegion.sql
:r .\SeedScripts\Seed.LU_County.sql
:r .\SeedScripts\Seed.LU_CropType.sql
:r .\SeedScripts\Seed.LU_CropYieldUnit.sql
:r .\SeedScripts\Seed.LU_Division.sql
:r .\SeedScripts\Seed.LU_DocumentType.sql
:r .\SeedScripts\Seed.LU_EducationInstituteType.sql
:r .\SeedScripts\Seed.LU_EnforcementAction.sql
:r .\SeedScripts\Seed.LU_HorticultureType.sql
:r .\SeedScripts\Seed.LU_IrrigationSystemType.sql
:r .\SeedScripts\Seed.LU_LivestockType.sql
:r .\SeedScripts\Seed.LU_Month.sql
:r .\SeedScripts\Seed.LU_NotificationType.sql
:r .\SeedScripts\Seed.LU_PermitCategory.sql
:r .\SeedScripts\Seed.LU_PermitComplianceStatus.sql
:r .\SeedScripts\Seed.LU_PermitConditionComplianceStatus.sql
:r .\SeedScripts\Seed.LU_PermitExternalStatus.sql
:r .\SeedScripts\Seed.LU_PermitHearingStatus.sql
:r .\SeedScripts\Seed.LU_PermitStatus.sql
:r .\SeedScripts\Seed.LU_PermitStatusCommunication.sql
:r .\SeedScripts\Seed.LU_PermitType.sql
:r .\SeedScripts\Seed.LU_PoultryCoolingSystem.sql
:r .\SeedScripts\Seed.LU_PoultryType.sql
:r .\SeedScripts\Seed.LU_ReportCode.sql
:r .\SeedScripts\Seed.LU_SoilType.sql
:r .\SeedScripts\Seed.LU_StandardConditionType.sql
:r .\SeedScripts\Seed.LU_PermitTemplate.sql
:r .\SeedScripts\Seed.LU_PumpageReportType.sql 
:r .\SeedScripts\Seed.LU_StandardCondition.sql
:r .\SeedScripts\Seed.LU_StateStream.sql
:r .\SeedScripts\Seed.LU_TableList.sql
:r .\SeedScripts\Seed.LU_TemplateType.sql
:r .\SeedScripts\Seed.LU_Template.sql
:r .\SeedScripts\Seed.LU_Title.sql
:r .\SeedScripts\Seed.LU_VegetableType.sql
:r .\SeedScripts\Seed.LU_ViolationType.sql
:r .\SeedScripts\Seed.LU_WaterDispersementType.sql
:r .\SeedScripts\Seed.LU_WaterType.sql
:r .\SeedScripts\Seed.LU_WaterWithdrawalEstimate.sql
:r .\SeedScripts\Seed.LU_WaterWithdrawalPurposeCategory.sql
:r .\SeedScripts\Seed.LU_WaterWithdrawalPurpose.sql
:r .\SeedScripts\Seed.LU_WaterWithdrawalSource.sql
:r .\SeedScripts\Seed.LU_WaterWithdrawalType.sql
:r .\SeedScripts\Seed.Contact.sql
:r .\SeedScripts\Seed.ContactCommunicationMethod.sql
:r .\SeedScripts\Seed.DivisionChief.sql
:r .\SeedScripts\Seed.Supervisor.sql
:r .\SeedScripts\Seed.ProjectManager.sql
:r .\SeedScripts\Seed.ProjectManagerCounty.sql
:r .\SeedScripts\Seed.LU_ZipCode.sql
:r .\SeedScripts\Seed.LU_FacilityType.sql
:r .\SeedScripts\Seed.LU_RestaurantType.sql 
:r .\SeedScripts\Seed.LU_IrrigationLayoutType.sql 
:r .\SeedScripts\Seed.LU_GolfIrrigationSystemType.sql 
:r .\SeedScripts\Seed.LU_WaterRemovalExcavationType.sql 
:r .\SeedScripts\Seed.LU_SupplementalType.sql 
:r .\SeedScripts\Seed.PermitStatusWorkflow.sql
:r .\SeedScripts\Seed.Checklist.sql
:r .\SeedScripts\Seed.ChecklistQuestions.sql
:r Script.AddUsers.sql

