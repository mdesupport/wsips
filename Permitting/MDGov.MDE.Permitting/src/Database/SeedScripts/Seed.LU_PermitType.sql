﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_PermitType] ON;


BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_PermitType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1,'Small Ag', 'Small Agricultural Permit', 10, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_PermitType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2,'Large Ag', 'Large Agricultural Permit', 20, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_PermitType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3,'Small Non-Ag', 'Small Non-Agricultural Permit', 30, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_PermitType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4,'Large Non-Ag', 'Large Non-Agricultural Permit', 40, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_PermitType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5,'EXE', 'Exemption', 50, 0, 'system', getdate(), 'system', getdate());

INSERT INTO [dbo].[LU_PermitType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (6,'Enforcement ','Enforcement  Only',60,0,'system',getdate(),'system',getdate());

COMMIT;
RAISERROR (N'[dbo].[[dbo].[LU_PermitType]]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_PermitType] OFF;