﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalPurposeCategory] ON;


BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_WaterWithdrawalPurposeCategory] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, NULL, 'Agricultural', 10, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalPurposeCategory] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, NULL, 'Non-Agricultural Irrigation', 20, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalPurposeCategory] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, NULL, 'Industrial & Mining', 30, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalPurposeCategory] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4, NULL, 'Power', 40, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalPurposeCategory] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5, NULL, 'Water Supply', 50, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalPurposeCategory] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (6, NULL, 'Commercial & Institutional', 60, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalPurposeCategory] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (7, NULL, 'Other', 70, 1, 'system', getdate(), 'system', getdate());


COMMIT;
RAISERROR (N'[dbo].[[dbo].[LU_WaterWithdrawalPurposeCategory]]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalPurposeCategory] OFF;