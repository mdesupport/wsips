﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ReportCode] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_ReportCode] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, NULL, 'Yes', 10, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_ReportCode] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, NULL, 'No', 20, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_ReportCode] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, NULL, 'Voluntary', 30, 1, 'system', GETDATE(), 'system', GETDATE());

COMMIT;
RAISERROR (N'[dbo].[LU_ReportCode]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ReportCode] OFF;