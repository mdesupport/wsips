﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_PermitTemplate] ON;
BEGIN TRANSACTION;

INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, NULL, 'Large agricultural groundwater', 10, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, NULL, 'Large agricultural surface water', 20, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, NULL, 'Small agricultural groundwater', 30, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4, NULL, 'Small agricultural surface water', 40, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5, NULL, 'Large non-agricultural groundwater', 50, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (6, NULL, 'Large non-agricultural surface water', 60, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (7, NULL, 'Small non-agricultural groundwater', 70, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (8, NULL, 'Small non-agricultural surface water', 80, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (9, NULL, 'Groundwater Heat Pump Small', 90, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (10, NULL, 'Groundwater Heat Pump Large', 100, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (11, NULL, 'Subdivisions Large (fractured rock)', 110, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (12, NULL, 'Subdivisions Large (coastal plain)', 120, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (13, NULL, 'Subdivisions Small (fractured rock)', 130, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (14, NULL, 'Subdivisions Small (coastal plain)', 140, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (15, NULL, 'Community Large Surface Water', 150, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (16, NULL, 'Community Small Groundwater Fractured Rock', 160, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (17, NULL, 'Community Small Groundwater Coastal Plain', 170, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (18, NULL, 'Community Large Groundwater Fractured Rock', 180, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (19, NULL, 'Community Large Groundwater Coastal Plain', 190, 1, 'system', getdate(), 'system', getdate());
INSERT INTO lu_permittemplate (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (20, NULL, 'Mobile withdrawals', 200, 1, 'system', getdate(), 'system', getdate());

COMMIT;
RAISERROR (N'[dbo].[LU_PermitTemplate]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_PermitTemplate] OFF;