﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [LU_PermitExternalStatus] ON;

BEGIN TRANSACTION;

INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, 'Pending County Approval', 10, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, 'Pending Initial Review', 20, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, 'Custom Package Development in Progress', 30, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4, 'Pending Response From Applicant', 40, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5, 'Technical Review in Progress', 50, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (6, 'Awaiting Publication', 60, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (7, 'Public Review in Progress', 70, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (8, 'Pending Permit Decision', 80, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (9, 'Pending Application Payment', 90, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (10, 'On Hold Due to Tax Liability', 100, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (11, 'On Hold Due to Business Status', 110, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitExternalStatus (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (12, 'Exemption in Progress', 120, 1, 'system', getdate(),'system',getdate());

COMMIT;
RAISERROR (N'[dbo].[LU_PermitExternalStatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;

SET IDENTITY_INSERT [LU_PermitExternalStatus] OFF;