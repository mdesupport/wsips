﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_StateRegion] ON;


BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_StateRegion] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, 'C', 'CENTRAL', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, 'S', 'SOUTHERN', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, 'E', 'EASTERN', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, 'W', 'WESTERN', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, 'N', 'NORTHERN', 50, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_StateRegion]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_StateRegion] OFF;