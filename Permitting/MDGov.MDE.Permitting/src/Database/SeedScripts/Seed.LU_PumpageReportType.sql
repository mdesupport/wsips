﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_PumpageReportType] ON;


BEGIN TRANSACTION;

INSERT INTO LU_PumpageReportType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (1, NULL, 'Gallons', 10, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO LU_PumpageReportType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (2, NULL, 'Acres / Inches', 20, 1, 'system', GETDATE(), 'system', GETDATE());

COMMIT;
RAISERROR (N'[dbo].[LU_PumpageReportType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_PumpageReportType] OFF;
