﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalEstimate] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, NULL, '# Hours x Gallons per Minute Pumped x 60 Minutes', 10, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, NULL, 'Flow Meter', 20, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, NULL, 'Elapsed Time Indicator', 30, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4, 'E1', 'Estimated based on last year''s data', 40, 0, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5, 'E2', 'Estimated based on some other criteria', 50, 0, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (6, 'EU', 'Estimated by user', 60, 0, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (7, 'M', 'Modified from pumpage report form', 70, 1, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (8, 'EP', 'Estimated based on the Permit Data', 80, 0, 'system', getdate(), 'system', getdate());
INSERT INTO [dbo].[LU_WaterWithdrawalEstimate] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (9, NULL, 'Other (Explain)', 90, 1, 'system', getdate(), 'system', getdate());


COMMIT;
RAISERROR (N'[dbo].[[dbo].[LU_WaterWithdrawalEstimate]]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalEstimate] OFF;