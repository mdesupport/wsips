﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

SET IDENTITY_INSERT [dbo].[ProjectManager] ON

INSERT INTO ProjectManager ([Id]
      ,[ContactId]
      ,[SupervisorId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate])
SELECT [Id]
      ,[ContactId]
      ,[SupervisorId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate] 
FROM MDEMembership.dbo.PreservedProjectManager;

DROP TABLE MDEMembership.dbo.PreservedProjectManager;

SET IDENTITY_INSERT [dbo].[ProjectManager] OFF

COMMIT;
