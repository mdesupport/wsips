﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_StandardCondition] ON;

BEGIN TRANSACTION;

INSERT INTO LU_StandardCondition (ID, PermitTemplateId, StandardConditionTypeID, RequiresSelfReporting, ConditionReportingPeriodID, OneTimeReportDays, RequiresValidation, DefaultComplianceReportingDueDate, PumpageReportTypeId, Description, Active, CreatedBy, CreatedDate, LastmodifiedBy, LastModifiedDate)
(
	SELECT ID, PermitTemplateId, StandardConditionTypeID, RequiresSelfReporting, ConditionReportingPeriodID, OneTimeReportDays, RequiresValidation, DefaultComplianceReportingDueDate, PumpageReportTypeId, Description, Active, CreatedBy, CreatedDate, LastmodifiedBy, LastModifiedDate
	FROM MdeWaterPermittingXXX..LU_StandardCondition
)
COMMIT;

RAISERROR (N'[dbo].[LU_StandardCondition]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_StandardCondition] OFF;
