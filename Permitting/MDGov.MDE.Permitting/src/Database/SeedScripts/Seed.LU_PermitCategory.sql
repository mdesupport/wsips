﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [LU_PermitCategory] ON;

BEGIN TRANSACTION;

INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, 'Applications', 10, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, 'Pending New Permits', 40, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, 'Pending Modified Permits', 60, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4, 'Pending Renewals', 50, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5, 'Pending Exemptions', 70, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (6, 'Existing Permits', 20, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (7, 'Exemptions', 30, 1, 'system', getdate(),'system',getdate());
INSERT INTO LU_PermitCategory (id, description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (8, 'Enforcement  Only',80,1,'system',getdate(),'system',getdate());
COMMIT;
RAISERROR (N'[dbo].[LU_PermitCategory]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [LU_PermitCategory] OFF;