﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_Country] ON;


BEGIN TRANSACTION;

INSERT INTO LU_Country (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (1, 'USA', 'USA', 10, 1, 'JMT', GETDATE(), 'JMT', GETDATE());
INSERT INTO LU_Country (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (2, 'CAN', 'Canada', 20, 1, 'JMT', GETDATE(), 'JMT', GETDATE());

COMMIT;
RAISERROR (N'[dbo].[LU_Country]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_Country] OFF;
