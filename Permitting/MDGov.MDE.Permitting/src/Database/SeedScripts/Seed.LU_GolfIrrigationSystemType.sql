﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_GolfIrrigationSystemType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_GolfIrrigationSystemType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Automatic', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Manual', 20, 1, 'system', GETDATE(), 'system', GETDATE() 

COMMIT;
RAISERROR (N'[dbo].[LU_GolfIrrigationSystemType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_GolfIrrigationSystemType] OFF;