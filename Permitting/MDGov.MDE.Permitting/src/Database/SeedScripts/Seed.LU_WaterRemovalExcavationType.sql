﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_WaterRemovalExcavationType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_WaterRemovalExcavationType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Well Points', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Drilled Wells', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Sump pit', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Other', 40, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_WaterRemovalExcavationType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_WaterRemovalExcavationType] OFF;