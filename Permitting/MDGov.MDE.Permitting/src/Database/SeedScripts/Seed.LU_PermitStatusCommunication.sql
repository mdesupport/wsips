﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_PermitStatusCommunication] ON 

BEGIN TRANSACTION;
GO


INSERT	[dbo].[LU_PermitStatusCommunication] (
		[Id], [PermitStatusId], [ContactTypeId], [EmailSubject], [EmailBody], [NotificationBody], [NotificationLink], [Description], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) 
SELECT	[Id], [PermitStatusId], [ContactTypeId], [EmailSubject], [EmailBody], [NotificationBody], [NotificationLink], [Description], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]
FROM	MdeWaterPermittingXXX..[LU_PermitStatusCommunication]



COMMIT;
RAISERROR (N'[dbo].[LU_PermitStatusCommunication]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_PermitStatusCommunication] OFF
GO