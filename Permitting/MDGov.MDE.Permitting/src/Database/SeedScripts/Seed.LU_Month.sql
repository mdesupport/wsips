﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_Month] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_Month] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, 'JAN', 'January', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, 'FEB', 'February', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, 'MAR', 'March', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, 'APR', 'April', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, 'MAY', 'May', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, 'JUN', 'June', 60, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, 'JUL', 'July', 70, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, 'AUG', 'August', 80, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, 'SEP', 'September', 90, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 10, 'OCT', 'October', 100, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 11, 'NOV', 'November', 110, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 12, 'DEC', 'December', 120, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_Month]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_Month] OFF;