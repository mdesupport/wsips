﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

SET IDENTITY_INSERT [dbo].[ContactCommunicationMethod] ON

INSERT INTO ContactCommunicationMethod ([Id]
      ,[CommunicationMethodId]
      ,[ContactId]
      ,[IsPrimaryEmail]
      ,[IsPrimaryPhone]
      ,[CommunicationValue]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate])
SELECT [Id]
      ,[CommunicationMethodId]
      ,[ContactId]
      ,[IsPrimaryEmail]
      ,[IsPrimaryPhone]
      ,[CommunicationValue]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate] 
FROM MDEMembership.dbo.PreservedContactCommunicationMethod;

DROP TABLE MDEMembership.dbo.PreservedContactCommunicationMethod;
SET IDENTITY_INSERT [dbo].[ContactCommunicationMethod] OFF

COMMIT;
