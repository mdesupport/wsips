﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

SET IDENTITY_INSERT [dbo].[DivisionChief] ON

INSERT INTO DivisionChief ([Id]
      ,[ContactId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate])
SELECT [Id]
      ,[ContactId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate] 
FROM MDEMembership.dbo.PreservedDivisionChief;

DROP TABLE MDEMembership.dbo.PreservedDivisionChief;

SET IDENTITY_INSERT [dbo].[DivisionChief] OFF

COMMIT;
