﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_CheckListAnswer] ON;

BEGIN TRANSACTION;

INSERT INTO LU_CheckListAnswer (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, 'Y', 'Yes', 10, 1, 'system',getdate(), 'system',getdate());
INSERT INTO LU_CheckListAnswer (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, 'N', 'No', 20, 0, 'system',getdate(), 'system',getdate());
INSERT INTO LU_CheckListAnswer (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, 'NA', 'N/A', 30, 1, 'system',getdate(), 'system',getdate());

COMMIT;
RAISERROR (N'[dbo].[LU_CheckListAnswer]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_CheckListAnswer] OFF;