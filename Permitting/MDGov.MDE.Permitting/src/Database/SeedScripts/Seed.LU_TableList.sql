﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_TableList] ON;
BEGIN TRANSACTION;

INSERT INTO LU_TableList ([Id], [Key], [Description], [Sequence], [IsLookup], [IsEditable], [Active], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) 
select 1, NULL, 'LU_ApplicationPacketItem', 1, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 2, NULL, 'LU_ApplicationType', 2, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 3, NULL, 'LU_Aquifer', 3, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 4, NULL, 'LU_AquiferType', 4, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 5, NULL, 'LU_CheckListAnswer', 5, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 6, NULL, 'LU_CommunicationMethod', 6, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 7, NULL, 'LU_ConditionReportingPeriod', 7, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 8, NULL, 'LU_ContactType', 8, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 9, NULL, 'LU_Country', 9, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 10, NULL, 'LU_County', 10, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 11, NULL, 'LU_CropType', 11, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 12, NULL, 'LU_CropYieldUnit', 12, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 13, NULL, 'LU_Division', 13, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 14, NULL, 'LU_DocumentType', 14, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 15, NULL, 'LU_EducationInstituteType', 15, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 16, NULL, 'LU_EnforcementAction', 16, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 17, NULL, 'LU_FacilityType', 17, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 18, NULL, 'LU_GolfIrrigationSystemType', 18, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 19, NULL, 'LU_HearingNotificationType', 19, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 20, NULL, 'LU_HorticultureType', 20, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 21, NULL, 'LU_IrrigationLayoutType', 21, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 22, NULL, 'LU_IrrigationSystemType', 22, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 23, NULL, 'LU_LivestockType', 23, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 24, NULL, 'LU_Month', 24, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 25, NULL, 'LU_PermitCategory', 25, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 26, NULL, 'LU_PermitComplianceStatus', 26, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 27, NULL, 'LU_PermitConditionComplianceStatus', 27, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 28, NULL, 'LU_PermitExternalStatus', 28, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 29, NULL, 'LU_PermitHearingStatus', 29, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 30, NULL, 'LU_PermitStatus', 30, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 31, NULL, 'LU_PermitStatusCommunication', 31, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 32, NULL, 'LU_PermitTemplate', 32, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 33, NULL, 'LU_PermitType', 33, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 34, NULL, 'LU_PoultryCoolingSystem', 34, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 35, NULL, 'LU_PoultryType', 35, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 36, NULL, 'LU_PumpageReportType', 36, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 37, NULL, 'LU_ReportCode', 37, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 38, NULL, 'LU_RestaurantType', 38, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 39, NULL, 'LU_SoilType', 39, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 40, NULL, 'LU_StandardCondition', 40, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 41, NULL, 'LU_StandardConditionType', 41, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 42, NULL, 'LU_State', 42, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 43, NULL, 'LU_StateRegion', 43, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 44, NULL, 'LU_StateStream', 44, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 45, NULL, 'LU_SupplementalType', 45, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 46, NULL, 'LU_TableList', 46, 1, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 47, NULL, 'LU_Template', 47, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 48, NULL, 'LU_TemplateType', 48, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 49, NULL, 'LU_Title', 49, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 50, NULL, 'LU_VegetableType', 50, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 51, NULL, 'LU_ViolationType', 51, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 52, NULL, 'LU_WaterDispersementType', 52, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 53, NULL, 'LU_WaterRemovalExcavationType', 53, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 54, NULL, 'LU_Watershed', 54, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 55, NULL, 'LU_WaterType', 55, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 56, NULL, 'LU_WaterWithdrawalEstimate', 56, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 57, NULL, 'LU_WaterWithdrawalPurpose', 57, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 58, NULL, 'LU_WaterWithdrawalPurposeCategory', 58, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 59, NULL, 'LU_WaterWithdrawalSource', 59, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 60, NULL, 'LU_WaterWithdrawalType', 60, 1, 1, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 61, NULL, '__RefactorLog', 1, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 62, NULL, 'AquiferPoly', 2, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 63, NULL, 'CheckList', 3, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 64, NULL, 'CheckListAnswers', 4, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 65, NULL, 'CheckListQuestions', 5, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 66, NULL, 'Comment', 6, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 67, NULL, 'ComplianceManager', 7, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 68, NULL, 'ComplianceStaff', 8, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 69, NULL, 'ConditionCompliance', 9, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 70, NULL, 'Contact', 10, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 71, NULL, 'ContactCommunicationMethod', 11, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 72, NULL, 'ContactHistory', 12, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 73, NULL, 'ContactMergeHistory', 13, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 74, NULL, 'CountyOfficial', 14, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 75, NULL, 'DepartmentOfNaturalResources', 15, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 76, NULL, 'DivisionChief', 16, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 77, NULL, 'Document', 17, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 78, NULL, 'DocumentByte', 18, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 79, NULL, 'Enforcement', 19, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 80, NULL, 'ExistingPermitOrApplication', 20, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 81, NULL, 'GeneratedLetter', 21, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 82, NULL, 'HearingNotification', 22, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 83, NULL, 'LetterTemplate', 23, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 84, NULL, 'LetterTemplateVariable', 24, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 85, NULL, 'Penalty', 25, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 86, NULL, 'Permit', 26, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 87, NULL, 'PermitApplicationPacket', 27, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 88, NULL, 'PermitApplicationPacketItem', 28, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 89, NULL, 'PermitCondition', 29, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 90, NULL, 'PermitContact', 30, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 91, NULL, 'PermitContactInformation', 31, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 92, NULL, 'PermitCounty', 32, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 93, NULL, 'PermitHearing', 33, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 94, NULL, 'PermitHearingNotification', 34, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 95, NULL, 'PermitParcel', 35, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 96, NULL, 'PermitSoilType', 36, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 97, NULL, 'PermitStatusHistory', 37, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 98, NULL, 'PermitStatusState', 38, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 99, NULL, 'PermitStatusWorkflow', 39, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 100, NULL, 'PermitSupplementalGroup', 40, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 101, NULL, 'PermitViolationEnforcement', 41, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 102, NULL, 'PermitViolationPenalty', 42, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 103, NULL, 'PermitWastewaterTreatmentDisposal', 43, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 104, NULL, 'PermitWaterDispersement', 44, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 105, NULL, 'PermitWaterWithdrawalMisc', 45, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 106, NULL, 'PermitWaterWithdrawalPurpose', 46, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 107, NULL, 'PermitWithdrawalGroundwater', 47, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 108, NULL, 'PermitWithdrawalGroundwaterDetail', 48, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 109, NULL, 'PermitWithdrawalSurfacewater', 49, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 110, NULL, 'PermitWithdrawalSurfacewaterDetail', 50, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 111, NULL, 'PermitWwAgricultural', 51, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 112, NULL, 'PermitWwAquacultureOrAquarium', 52, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 113, NULL, 'PermitWwCommercialDrinkingOrSanitary', 53, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 114, NULL, 'PermitWwConstructionDewatering', 54, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 115, NULL, 'PermitWwCrops', 55, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 116, NULL, 'PermitWwFarmPortableSupplies', 56, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 117, NULL, 'PermitWwFoodProcessing', 57, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 118, NULL, 'PermitWwGolfCourseIrrigation', 58, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 119, NULL, 'PermitWwHorticulture', 59, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 120, NULL, 'PermitWwInstitutionalEducational_drinkingOrsanitary', 60, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 121, NULL, 'PermitWwInstitutionalReligious_DringkingOrSanitary', 61, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 122, NULL, 'PermitWwLivestock', 62, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 123, NULL, 'PermitWwNonAgricultureIrrigation', 63, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 124, NULL, 'PermitWwPoultry', 64, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 125, NULL, 'PermitWwPrivateWaterSupplier', 65, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 126, NULL, 'PermitWwVegetables', 66, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 127, NULL, 'PermitWwWildlifePonds', 67, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 128, NULL, 'ProjectManager', 68, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 129, NULL, 'ProjectManagerCounty', 69, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 130, NULL, 'PublicComment', 70, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 131, NULL, 'PumpageReport', 71, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 132, NULL, 'PumpageReportDetail', 72, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 133, NULL, 'Report', 73, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 134, NULL, 'ReportParameter', 74, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 135, NULL, 'Secretary', 75, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 136, NULL, 'Supervisor', 76, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 137, NULL, 'SupplementalGroup', 77, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 138, NULL, 'Violation', 78, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 139, NULL, 'WaterWithdrawalLocationPurpose', 79, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
select 140, NULL, 'ZipCode', 80, 0, 0, 1, 'system', GETDATE(), 'system', GETDATE()

--INSERT INTO LU_TableList ([key], description, sequence, islookup, iseditable, active, createdby, createddate, lastmodifiedby, lastmodifieddate) 
--(
--	SELECT NULL, name, ROW_NUMBER() OVER(ORDER BY name), 1, 1, 1, 'system', GETDATE(), 'system', GETDATE()
--	FROM sys.tables
--	WHERE name like 'LU_%'
--);
----Then everything else:
--INSERT INTO LU_TableList ([key], description, sequence, islookup, iseditable, active, createdby, createddate, lastmodifiedby, lastmodifieddate) 
--(
--	SELECT NULL, name, ROW_NUMBER() OVER(ORDER BY name), 0, 0, 1, 'system', GETDATE(), 'system', GETDATE()
--	FROM sys.tables
--	WHERE name not like 'LU_%'
--);
----Need to update which are "Read Only"!
----Make LU_TableList "Read Only":
--UPDATE LU_TableList SET IsEditable = 0 WHERE Description = 'LU_TableList';

COMMIT;
RAISERROR (N'[dbo].[LU_TableList]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_TableList] OFF;