﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

SET IDENTITY_INSERT [dbo].[ProjectManagerCounty] ON

INSERT INTO ProjectManagerCounty ([Id]
      ,[ProjectManagerId]
      ,[CountyId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate])
SELECT [Id]
      ,[ProjectManagerId]
      ,[CountyId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate] 
FROM MDEMembership.dbo.PreservedProjectManagerCounty;

DROP TABLE MDEMembership.dbo.PreservedProjectManagerCounty;

SET IDENTITY_INSERT [dbo].[ProjectManagerCounty] OFF

COMMIT;