﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].LU_EducationInstituteType ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].LU_EducationInstituteType (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'School', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Daycare', 20, 1, 'system', GETDATE(), 'system', GETDATE() 

COMMIT;
RAISERROR (N'[dbo].LU_EducationInstituteType: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].LU_EducationInstituteType OFF;