﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ApplicationType] ON;

BEGIN TRANSACTION;

INSERT INTO LU_ApplicationType (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, 'NA', 'New Permit Application', 10, 1, 'system',getdate(), 'system',getdate());
INSERT INTO LU_ApplicationType (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, 'MOD', 'Modification to Permit', 20, 1, 'system',getdate(), 'system',getdate());
INSERT INTO LU_ApplicationType (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, 'RE', 'Renewal', 30, 1, 'system',getdate(), 'system',getdate());
INSERT INTO LU_ApplicationType (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4, 'NEX', 'New Exemption Application', 40, 1, 'system',getdate(), 'system',getdate());
INSERT INTO LU_ApplicationType (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5, 'PEX', 'Existing Permit to Exemption', 50, 1, 'system',getdate(), 'system',getdate());

COMMIT;
RAISERROR (N'[dbo].[LU_ApplicationType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ApplicationType] OFF;