﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_CropYieldUnit] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_CropYieldUnit] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL, 'Bushels/acre', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Tons/acre', 20, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_CropYieldUnit]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_CropYieldUnit] OFF;