﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_CropType] ON;

BEGIN TRANSACTION;

INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (1, NULL, 'Grain corn', 10, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (2, NULL, 'Soybeans', 20, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (3, NULL, 'Cantaloupes', 30, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (4, NULL, 'Cucumbers', 40, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (5, NULL, 'Lima beans', 50, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (6, NULL, 'Peas', 60, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (7, NULL, 'Small vegetables', 70, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (8, NULL, 'Sweet corn', 80, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (9, NULL, 'Sweet potatoes', 90, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (10, NULL, 'Tomatoes', 100, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (11, NULL, 'Watermelons', 110, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_CropType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (12, NULL, 'Other', 120, 1, 'system', GetDate(), 'system', GetDate())

COMMIT;
RAISERROR (N'[dbo].[LU_CropType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_CropType] OFF;