﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_WaterWithdrawalType] (id, waterwithdrawalsourceid, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, 1, NULL, 'Well', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, 1, NULL, 'Spring', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, 1, NULL, 'Groundwater pond', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, 2, NULL, 'Stream/River', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, 2, NULL, 'Lake', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, 2, NULL, 'Pond', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, 2, NULL, 'Bay', 40, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_WaterWithdrawalType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalType] OFF;