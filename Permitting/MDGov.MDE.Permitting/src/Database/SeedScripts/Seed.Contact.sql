﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

SET IDENTITY_INSERT [dbo].[Contact] ON

INSERT INTO Contact ([Id]
      ,[ContactTypeId]
      ,[CountryId]
      ,[CountyId]
      ,[DivisionId]
      ,[StateId]
      ,[TitleId]
      ,[UserId]
      ,[Salutation]
      ,[FirstName]
      ,[MiddleInitial]
      ,[LastName]
	  ,[SecondaryName]
      ,[BusinessName]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[ZipCode]
      ,[Subdivision]
      ,[IsBusiness]
      ,[Office]
	  ,[ApplicationNotification]
	  ,[EmailNotification]
	  ,[SignatureFileName]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate])
SELECT [Id]
      ,[ContactTypeId]
      ,[CountryId]
      ,[CountyId]
      ,[DivisionId]
      ,[StateId]
      ,[TitleId]
      ,[UserId]
      ,[Salutation]
      ,[FirstName]
      ,[MiddleInitial]
      ,[LastName]
	  ,[SecondaryName]
      ,[BusinessName]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[ZipCode]
      ,[Subdivision]
      ,[IsBusiness]
      ,[Office]
	  ,[ApplicationNotification]
	  ,[EmailNotification]
	  ,[SignatureFileName]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate] 
FROM MDEMembership.dbo.PreservedContact;

DROP TABLE MDEMembership.dbo.PreservedContact;

SET IDENTITY_INSERT [dbo].[Contact] OFF

COMMIT;
