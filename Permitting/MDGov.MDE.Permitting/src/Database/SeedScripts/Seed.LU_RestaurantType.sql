﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_RestaurantType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_RestaurantType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Fast food', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Sit down ', 20, 1, 'system', GETDATE(), 'system', GETDATE() 

COMMIT;
RAISERROR (N'[dbo].[LU_RestaurantType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_RestaurantType] OFF;