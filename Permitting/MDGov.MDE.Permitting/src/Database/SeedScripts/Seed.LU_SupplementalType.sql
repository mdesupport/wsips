﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_SupplementalType] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_SupplementalType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, NULL, 'Related', 10, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_SupplementalType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, NULL, 'Supplemental and Combined', 20, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_SupplementalType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, NULL, 'Supplemental Special', 30, 1, 'system', GETDATE(), 'system', GETDATE());

COMMIT;
RAISERROR (N'[dbo].[LU_SupplementalType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_SupplementalType] OFF;