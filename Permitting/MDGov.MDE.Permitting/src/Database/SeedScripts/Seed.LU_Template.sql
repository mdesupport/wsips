﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_Template] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_Template] (id, [key], description, templatetypeid, filename, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Draft Permit', 1, 'DraftPermit.dotx', 10, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Custom Package Cover Letter (Agricultural)', 2, 'CustomPackageCoverLetterAg.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Custom Package Cover Letter (Non-Agricultural)', 2, 'CustomPackageCoverLetterNonAg.dotx', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Processing Procedures and Flow Diagram (Agricultural)', 2, 'ProcessingProceduresAg.dotx', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'Processing Procedures and Flow Diagram (Non-Agricultural)', 2, 'ProcessingProceduresNonAg.dotx', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, NULL, 'Application Process Outline (Agricultural)', 2, 'ApplicationProcessOutlineAg.dotx', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, NULL, 'Application Process Outline (Non-Agricultural)', 2, 'ApplicationProcessOutlineNonAg.dotx', 60, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, NULL, 'County Tax Map', 2, 'CountyTaxMapNumbers.dotx', 70, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, NULL, 'Certification of Notification Form', 2, 'CertificationNotificationForm.dotx', 80, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 10, NULL, 'Public Notice Billing Form', 2, 'PublicNoticeBillingForm.dotx', 90, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 11, NULL, 'Acknowledgement Form Concerning Cultural and Biological Resources', 2, 'AcknowledgementFormCulturalBiological.dotx', 100, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 12, NULL, 'Aquifer Test Procedure', 2, 'AquiferTestProcedure.dotx', 110, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 13, NULL, 'Hydrogeologic Investigation Procedures - Coastal Plain Unconfined', 2, 'CoastalPlainUnconfinedHydroProc.dotx', 120, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 14, NULL, 'Hydrogeologic Investigation Procedures - Coastal Plain Confined', 2, 'CoastalPlainConfined.dotx', 130, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 15, NULL, 'Hydrogeologic Investigation Procedures - Fractured Rock (Crystalline)', 2, 'Fracrock(Crystalline)HydroProc.dotx', 140, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 16, NULL, 'Hydrogeologic Investigation Procedures - Fractured Rock (Consolidated Sediment)', 2, 'FracRock(ConsolSed)HydroProc.dotx', 150, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 17, NULL, 'Hydrogeologic Investigation Procedures - Fractured Rock (Springs)', 2, 'Fracrock(Springs)HydroProc.dotx', 160, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 18, NULL, 'Hydrogeologic Investigation Procedures - Fractured Rock (Subdivision)', 2, 'Fracrock(SubdIndWell)HydroProc.dotx', 170, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 19, NULL, 'Hydrogeologic Investigation Procedures - Surface Water', 2, 'SurfWtrHydroProc.dotx', 180, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 20, NULL, 'Demand Analysis', 2, 'DemandAnalysis.dotx', 190, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 21, NULL, 'Tier II Evaluation', 2, 'TierIIEvaluation.dotx', 200, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 22, NULL, 'Water Management Best Management Practices', 2, 'BestManagementPractices.dotx', 210, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 23, NULL, 'Capacity Management Plan Guidance', 2, 'CapacityManagementPlanGuidance.dotx', 220, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 24, NULL, 'Standard', 3, '6MonthLetterStandard.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 25, NULL, 'Harsh', 3, '6MonthLetterHarsh.dotx', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 26, NULL, 'Community Water Systems', 3, '6MonthLetterCWS.dotx', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 27, NULL, 'Withdrawal Letter Increase', 4, 'WithdrawalLetterIncrease.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 28, NULL, 'Withdrawal Letter New', 4, 'WithdrawalLetterNew.dotx', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 29, NULL, 'Decision Letter Surfacewater', 5, 'DecisionLetterSurfacewater.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 30, NULL, 'Decision Letter Groundwater', 5, 'DecisionLetterGroundwater.dotx', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 31, NULL, 'Renewal Letter - Ag Voluntary', 7, 'RenewalAGVoluntary.dotx', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 32, NULL, 'Exemption Letter - Current Owner', 7, 'ExemptionCurrentOwner.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 33, NULL, 'Draft Permit (Non-Agricultural)', 1, 'ExemptionCurrentOwner.dotx', 20, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 34, NULL, 'Pumpage Letters (Agricultural)', 8, 'PumpageLetterAg.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 35, NULL, 'Pumpage Letters (Non-Agricultural) Jan - Jun', 8, 'PumpageLetterNonAgPeriod1.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 36, NULL, 'Pumpage Letters (Non-Agricultural) Jul - Dec', 8, 'PumpageLetterNonAgPeriod2.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 37, NULL, 'Draft Permit (Agricultural) Cover', 1, 'DraftPermitAgCover.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 38, NULL, 'Draft Permit (Agricultural) Body', 1, 'DraftPermitAgBody.dotx', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 39, NULL, 'Draft Permit (Non-Agricultural) Cover', 1, 'DraftPermitNonAgCover.dotx', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 40, NULL, 'Draft Permit (Non-Agricultural) Body', 1, 'DraftPermitNonAgBody.dotx', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 41, NULL, 'Exemption Letter - New Owner', 7, 'ExemptionCurrentOwner.dotx', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 42, NULL, 'Exemption Letter - Public Water System', 7, 'ExemptionCurrentOwnerPWS.dotx', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 43, NULL, 'Renewal Letter - Current Owner', 7, 'ExpiredCurrentOwner.dotx', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 44, NULL, 'Renewal Letter - New Owner', 7, 'ExpiredNewOwner.dotx', 60, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 45, NULL, 'Failure to Report NOV - Ag', 7, 'FailureToReportNOVAg.dotx', 70, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 46, NULL, 'Failure to Report NOV - Non Ag', 7, 'FailureToReportNOVNonAg.dotx', 80, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 47, NULL, 'Over-Appropriation NOV', 7, 'OverAppropriationNOV.dotx', 90, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 48, NULL, 'Water Audit Reminder', 7, 'WaterAuditReminder.dotx', 100, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 49, NULL, 'Custom Package Cover Letter (Construction Dewatering)', 2, 'CustomPackageCoverLetterCD.dotx', 25, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 50, NULL, 'Processing Procedures (Construction Dewatering)', 2, 'ProcessingProceduresCD.dotx', 45, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 51, NULL, 'Application Process Outline (Construction Dewatering)', 2, 'ApplicationProcessOutlineCD.dotx', 65, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 53, NULL, 'Pumpage Letters (Agricultural) Acres / Inches', 8, 'PumpageLetterAgAcresInches.dotx', 10, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_Template]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_Template] OFF;