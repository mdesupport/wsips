﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ViolationType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_ViolationType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Exemption', 1, 10, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Expired', 1, 20, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Failure to Report', 1, 30, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Loss Reduction Plans', 1, 40, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'New Owner', 1, 50, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, NULL, 'Other', 1, 60, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, NULL, 'Over Appropriation', 1, 70, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, NULL, 'Reporting - Other', 1, 80, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, NULL, 'Screen Installation ', 1, 90, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 10, NULL, 'Special Condition - Other ', 1, 100, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 11, NULL, 'Voluntary Ag', 1, 110, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 12, NULL, 'Water Audits', 1, 120, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 13, NULL, 'Water Conservation Plans', 1, 130, 'system', GETDATE(), 'system', GETDATE();

COMMIT;
RAISERROR (N'[dbo].[LU_ViolationType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ViolationType] OFF;
