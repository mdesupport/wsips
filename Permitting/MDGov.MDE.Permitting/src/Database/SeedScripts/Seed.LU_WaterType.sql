﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_WaterType] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_WaterType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, NULL, 'Freshwater', 10, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_WaterType] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, NULL, 'Saltwater', 20, 1, 'system', GETDATE(), 'system', GETDATE());

COMMIT;
RAISERROR (N'[dbo].[LU_WaterType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_WaterType] OFF;