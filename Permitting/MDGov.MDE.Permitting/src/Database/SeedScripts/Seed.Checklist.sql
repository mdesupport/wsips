﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[Checklist] ON;

BEGIN TRANSACTION;
/**
INSERT INTO [dbo].[Checklist] ([Id], [PermitStatusId], [Active], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])

SELECT 1, 4, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, 5, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, 6, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, 7, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, 8, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, 9, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, 11, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, 12, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 10, 13, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 11, 14, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 12, 15, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 13, 16, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 14, 17, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 15, 18, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 16, 19, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 17, 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 18, 21, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 19, 22, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 20, 23, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 21, 24, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 22, 59, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 23, 14, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 24, 14, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 25, 14, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 26, 14, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 27, 14, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 28, 4, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 29, 14, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 30, 22, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 31, 14, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 32, 44, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 33, 6, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 34, 45, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 35, 6, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 36, 6, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 37, 58, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 38, 26, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 39, 27, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 40, 28, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 41, 29, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 42, 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 43, 31, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 44, 32, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 45, 33, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 46, 62, 1, 'system', GETDATE(), 'system', GETDATE() 
**/


--Don't rename this DB name to ETL:
INSERT [dbo].[CheckList] ([Id], [PermitStatusId], [Active], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) 
SELECT [Id], [PermitStatusId], [Active], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]
FROM MdeWaterPermittingXXX..[CheckList]


COMMIT;
RAISERROR (N'[dbo].[Checklist]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[Checklist] OFF;
