﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

SET IDENTITY_INSERT PermitStatusWorkflow ON;
/**
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (1, 3, 4, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (2, 4, 4, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (3, 1, 5, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (4, 2, 5, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (5, 3, 5, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (6, 4, 5, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (7, 4, 6, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (8, 3, 6, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (9, 2, 6, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (10, 1, 6, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (11, 4, 8, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (12, 3, 8, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (13, 2, 8, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (14, 1, 8, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (15, 2, 9, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (16, 4, 9, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (17, 4, 10, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (18, 2, 10, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (19, 4, 11, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (20, 2, 11, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (21, 1, 13, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (22, 2, 13, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (23, 3, 13, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (24, 4, 13, 10, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (25, 4, 14, 11, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (26, 2, 14, 10, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (27, 4, 15, 12, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (28, 2, 15, 11, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (29, 2, 16, 12, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (30, 4, 16, 13, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (31, 2, 17, 13, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (32, 4, 17, 14, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (33, 2, 18, 14, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (34, 4, 18, 15, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (35, 2, 19, 15, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (36, 4, 19, 16, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (37, 2, 20, 16, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (38, 4, 20, 17, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (39, 2, 21, 17, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (40, 4, 22, 19, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (41, 4, 21, 18, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (42, 2, 22, 18, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (43, 3, 22, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (44, 1, 22, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (45, 1, 23, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (46, 3, 23, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (47, 2, 23, 19, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (48, 4, 23, 20, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (49, 1, 24, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (50, 3, 24, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (51, 2, 24, 20, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (52, 4, 24, 21, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (53, 1, 7, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (54, 2, 7, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (55, 3, 7, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (56, 4, 7, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (57, 2, 12, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (58, 4, 12, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (59, 3, 25, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (60, 1, 35, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (61, 1, 36, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (62, 1, 37, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (63, 1, 38, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (64, 1, 39, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (65, 1, 34, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (66, 1, 40, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (67, 1, 41, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (68, 1, 42, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (69, 1, 43, 10, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (70, 2, 35, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (71, 2, 36, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (72, 2, 37, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (73, 2, 38, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (74, 2, 39, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (75, 2, 34, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (76, 2, 40, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (77, 2, 41, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (78, 2, 42, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (79, 2, 43, 10, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (80, 3, 35, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (81, 3, 36, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (82, 3, 37, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (83, 3, 38, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (84, 3, 39, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (85, 3, 34, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (86, 3, 40, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (87, 3, 41, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (88, 3, 42, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (89, 3, 43, 10, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (90, 4, 35, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (91, 4, 36, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (92, 4, 37, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (93, 4, 38, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (94, 4, 39, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (95, 4, 34, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (96, 4, 40, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (97, 4, 41, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (98, 4, 42, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (99, 4, 43, 10, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (100, 1, 26, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (101, 1, 27, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (102, 1, 28, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (103, 1, 29, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (104, 1, 30, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (105, 1, 31, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (106, 1, 32, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (107, 1, 33, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (108, 2, 26, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (109, 2, 27, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (110, 2, 28, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (111, 2, 29, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (112, 2, 30, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (113, 2, 31, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (114, 2, 32, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (115, 2, 33, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (116, 3, 26, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (117, 3, 27, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (118, 3, 28, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (119, 3, 29, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (120, 3, 30, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (121, 3, 31, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (122, 3, 32, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (123, 3, 33, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (124, 4, 26, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (125, 4, 27, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (126, 4, 28, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (127, 4, 29, 4, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (128, 4, 30, 5, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (129, 4, 31, 6, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (130, 4, 32, 7, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (131, 4, 33, 8, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (132, 1, 44, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (133, 1, 58, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (134, 1, 45, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (135, 2, 44, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (136, 2, 58, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (137, 2, 45, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (138, 3, 44, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (139, 3, 58, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (140, 3, 45, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (141, 4, 44, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (142, 4, 58, 2, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (143, 4, 45, 3, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (144, 1, 59, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (145, 3, 59, 10, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (146, 2, 59, 21, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (147, 4, 59, 22, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (148, 1, 62, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (149, 3, 62, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (150, 2, 62, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (151, 4, 62, 9, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (152, 1, 65, 11, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (153, 3, 65, 11, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (154, 2, 65, 11, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (155, 4, 65, 11, 'system', GETDATE(), 'system', GETDATE());
**/


--Don't rename this DB name to ETL:
INSERT INTO PermitStatusWorkflow (Id, PermitTypeId, PermitStatusId, Sequence, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)
(
	SELECT *
	FROM MdeWaterPermittingXXX..PermitStatusWorkflow
)
SET IDENTITY_INSERT PermitStatusWorkflow OFF;

COMMIT;
RAISERROR (N'[dbo].[PermitStatusWorkFlow]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;