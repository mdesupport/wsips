﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_CommunicationMethod] ON;

BEGIN TRANSACTION;

INSERT INTO [dbo].[LU_CommunicationMethod] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (1, NULL, 'Home', 2, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_CommunicationMethod] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (2, NULL, 'Mobile', 1, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_CommunicationMethod] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (3, NULL, 'Email', 5, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_CommunicationMethod] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (4, NULL, 'Office', 4, 1, 'system', GETDATE(), 'system', GETDATE());
INSERT INTO [dbo].[LU_CommunicationMethod] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate) VALUES (5, NULL, 'Fax', 3, 1, 'system', GETDATE(), 'system', GETDATE());

COMMIT;
RAISERROR (N'[dbo].[LU_CommunicationMethod]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_CommunicationMethod] OFF;