﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_County] ON;


BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_County] (id, Enf_StateRegionId, Ntw_StateRegionId, StateId, Wa_Region, Newspaper, [Key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, 4, 4, 1, 1, 'Cumberland Times News', 'AL', 'Allegany', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, 2, 2, 1, 2, 'The Capital', 'AA', 'Anne Arundel', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, 1, 1, 1, 1, 'The Avenue', 'BA', 'Baltimore', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, 1, 1, 1, 1, 'The Baltimore Sun', 'BC', 'Baltimore City', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, 2, 2, 1, 2, 'The Calvert Independent', 'CA', 'Calvert', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, 1, 1, 1, 2, 'The Cecil Whig', 'CE', 'Cecil', 60, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, 2, 2, 1, 2, 'Maryland Independent', 'CH', 'Charles', 70, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, 4, 4, 1, 1, 'Carroll County Times', 'CL', 'Carroll', 80, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, 3, 3, 1, 2, 'The Caroline Times Record', 'CO', 'Caroline', 90, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 10, 2, 2, 1, NULL, NULL, 'DC', 'District of Columbia', 100, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 11, 3, 3, 1, 2, 'The Daily Banner', 'DO', 'Dorchester', 110, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 12, 4, 4, 1, 1, 'The Frederick News', 'FR', 'Frederick', 120, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 13, 4, 4, 1, 2, 'The Republican News', 'GA', 'Garrett', 130, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 14, 1, 1, 1, 2, 'The Aegis', 'HA', 'Harford', 140, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 15, 1, 1, 1, 1, 'Howard County Times', 'HO', 'Howard', 150, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 16, 3, 3, 1, 1, 'Kent County News', 'KE', 'Kent', 160, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 17, 1, 2, 1, 1, 'The Montgomery Sentinel', 'MO', 'Montgomery', 170, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 18, 2, 2, 1, 1, 'The Prince Georges Post', 'PG', 'Prince Georges', 180, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 19, 3, 3, 1, 1, 'The Record Observer', 'QA', 'Queen Annes', 190, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 20, 2, 2, 1, 2, 'The Enterprise', 'SM', 'St. Mary''s', 200, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 21, 3, 3, 1, 2, 'Somerset Herald', 'SO', 'Somerset', 210, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 22, 3, 3, 1, 1, 'The Star Democrat', 'TA', 'Talbot', 220, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 23, 4, 4, 1, 1, 'The Morning Herald', 'WA', 'Washington', 230, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 24, 3, 3, 1, 1, 'The Daily and Sunday Times', 'WI', 'Wicomico', 240, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 25, 3, 3, 1, 1, 'Maryland Times Press', 'WO', 'Worcester', 250, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_County]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_County] OFF;