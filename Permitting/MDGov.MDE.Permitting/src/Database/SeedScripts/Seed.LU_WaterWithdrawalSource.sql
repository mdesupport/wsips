﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalSource] ON;


BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_WaterWithdrawalSource] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Groundwater', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Surface Water', 20, 1, 'system', GETDATE(), 'system', GETDATE() 

COMMIT;
RAISERROR (N'[dbo].[LU_WaterWithdrawalSource]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_WaterWithdrawalSource] OFF;