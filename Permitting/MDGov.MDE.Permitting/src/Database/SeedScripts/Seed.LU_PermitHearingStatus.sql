﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_PermitHearingStatus] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_PermitHearingStatus] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, null, 'Scheduled', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, null, 'Executed', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, null, 'Cancelled', 30, 1, 'system', GETDATE(), 'system', GETDATE() 


COMMIT;
RAISERROR (N'[dbo].[LU_PermitHearingStatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_PermitHearingStatus] OFF;
--COMMIT;
--RAISERROR (N'[dbo].[LU_PermitHearingStatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
