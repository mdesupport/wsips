﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_IrrigationSystemType] ON;

BEGIN TRANSACTION;

INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (1, NULL, 'Drip trickle', 10, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (2, NULL, 'Low pressure spray - pivot', 20, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (3, NULL, 'Low pressure spray - linear move', 30, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (4, NULL, 'Low pressure spray - buried solid set', 40, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (5, NULL, 'High pressure spray - pivot', 50, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (6, NULL, 'High pressure spray - traveling guns', 60, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (7, NULL, 'Hand move Spray - sprinklers', 70, 1, 'system', GetDate(), 'system', GetDate())
INSERT INTO LU_IrrigationSystemType (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate) VALUES (8, NULL, 'Hand move Spray - big guns', 80, 1, 'system', GetDate(), 'system', GetDate())

COMMIT;
RAISERROR (N'[dbo].[LU_IrrigationSystemType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_IrrigationSystemType] OFF;