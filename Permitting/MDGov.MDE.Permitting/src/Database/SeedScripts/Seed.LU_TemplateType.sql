﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_TemplateType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_TemplateType] (id, [key], description, destinationfilebasename, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Draft Permit', 'DraftPermit', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Custom Package', 'CustomPackage', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, '6 Month Letter', '6MonthLetter', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Withdrawal Letter', 'WithdrawalLetter', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'Decision Letter', 'DecisionlLetter', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, NULL, 'Renewal and Exemption', 'RenewalLetter', 60, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, NULL, 'Pumpage Letter', 'PumpageLetter', 70, 1, 'system', GETDATE(), 'system', GETDATE() 


COMMIT;
RAISERROR (N'[dbo].[LU_TemplateType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_TemplateType] OFF;