﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_PoultryType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_PoultryType] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL, 'Chickens', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Chickens – broilers', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Chickens – roasters', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Chickens – Laying hens', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'Chickens – Pullets', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, NULL, 'Ducks', 60, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, NULL, 'Turkeys', 70, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, NULL, 'Pheasants', 80, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, NULL, 'Other', 90, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_PoultryType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_PoultryType] OFF;