﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_EnforcementAction] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_EnforcementAction] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL, 'Consent Agreement', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Letter Sent', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Notice of Violation (NOV)', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Other', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'Referred  to AG''s Office', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, NULL, 'Resolved', 60, 1, 'system', GETDATE(), 'system', GETDATE();

COMMIT;
RAISERROR (N'[dbo].[LU_EnforcementAction]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_EnforcementAction] OFF;