﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_ConditionReportingPeriod] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_ConditionReportingPeriod] (id, [key], description, sequence, active, createdby, createddate, lastmodifiedby, lastmodifieddate)

SELECT 1, NULL, 'Annually', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Monthly', 20, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Semi-Annually', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Quarterly', 40, 0, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'One time', 50, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_ConditionReportingPeriod]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_ConditionReportingPeriod] OFF;