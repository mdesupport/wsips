﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[ChecklistQuestions] ON;

BEGIN TRANSACTION;
/**
INSERT INTO [dbo].[ChecklistQuestions] ([Id], [CheckListId], [Question], [QuestionNumber], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate])

SELECT 1, 1, 'Was the consistency verified?', 5, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, 1, 'Has the County Signed Off?', 4, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, 2, 'Application form is complete?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, 2, 'All supplimentary information has been provided?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, 3, 'If six months have passed with no response from applicant, has the six-month letter been sent?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, 4, 'Response received?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, 5, 'Applicant name is listed with SDAT Charter (if applicable)?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, 5, 'Tax map information appears accurate?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, 5, 'Has aquifer been evaluated?', 3, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 10, 5, 'Is the demand reasonable (for non-ag)?', 4, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 11, 5, 'Has demand been calculated (for ag)?', 5, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 12, 5, 'Have the results of the demand analysis been uploaded?', 6, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 13, 5, 'DNR comments requested (for SW > 5,000)?', 7, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 14, 5, 'Are all outstanding issues resolved?', 9, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 15, 6, 'Packet prepared?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 16, 7, 'Packet approved/sent?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 17, 8, 'Did the applicant submit all required documentation?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 18, 8, 'Are all documents uploaded?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 19, 9, 'Response received?', 3, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 20, 10, 'Preliminary 3R sheet and advertisement submitted to supervisor?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 21, 10, 'Are all files uploaded?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 22, 11, 'Comments received?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 23, 12, 'Preliminary 3R and advertisement approved?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 24, 13, 'IPL prepared?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 25, 14, 'Preliminary 3R and advertisement approved?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 26, 15, 'Advertisement published?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 27, 16, 'Comment period has ended?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 28, 17, 'Hearing scheduled?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 29, 18, 'Hearing held?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 30, 19, 'Comments addressed?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 31, 19, 'Permit or denial prepared?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 32, 20, 'Permit decision letter prepared?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 33, 20, 'Permit approved?', 3, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 34, 20, 'Permit Denied?', 4, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 35, 21, 'Payment received?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 36, 22, 'Permit ready for finalization and signature?', 3, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 37, 22, 'System updated?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 38, 23, 'Has DNR provided comments?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 39, 28, 'Is this consistent with local planning?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 40, 19, 'Conditions Added via the Conditions Tab?', 3, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 41, 30, 'Test Activation', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 42, 32, 'Tax Map / Parcel Verified?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 43, 32, 'No other permits on same parcel?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 44, 32, 'Has the applicant been notified of any additional information needed?', 3, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 45, 32, 'Submitted to Enforcement / Compliance Manager?', 4, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 46, 33, 'Did the applicant sumbit all required documentation?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 47, 34, 'Exemption Approved?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 48, 37, 'Did the applicant submit all required documentation?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 49, 38, 'Status confirmed or applicant notified?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 50, 39, 'Status confirmed?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 51, 40, 'Tax status verified or applicant notified?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 52, 41, 'Status confirmed?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 53, 42, 'Has applicant been notified of any additional required information?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 54, 42, 'Status confirmed with MD Charter?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 55, 42, 'Are all files updated?', 3, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 56, 43, 'Did the applicant submit all required documentation?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 57, 43, 'Are all documents uploaded?', 2, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 58, 44, 'Draft permit submitted to supervisor?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 59, 45, 'Draft permit approved by supervisor?', 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 60, 46, 'System Updated?', 1, 'system', GETDATE(), 'system', GETDATE()
**/

--Don't rename this DB name to ETL:
INSERT [dbo].[CheckListQuestions] ([Id], [CheckListId], [Question], [QuestionNumber], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) 
SELECT [Id], [CheckListId], [Question], [QuestionNumber], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]
FROM MdeWaterPermittingXXX..[CheckListQuestions]


COMMIT;
RAISERROR (N'[dbo].[ChecklistQuestions]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[ChecklistQuestions] OFF;
