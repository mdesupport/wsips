﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_WaterDispersementType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_WaterDispersementType] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL, 'Public Sewer', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'Groundwater - Subsurface', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Groundwater - Spray Irrigation', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Groundwater - Other', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'Surface Water', 50, 1, 'system', GETDATE(), 'system', GETDATE()

COMMIT;
RAISERROR (N'[dbo].[LU_WaterDispersementType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_WaterDispersementType] OFF;