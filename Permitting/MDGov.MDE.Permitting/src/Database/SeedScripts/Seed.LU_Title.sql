﻿SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IDENTITY_INSERT [dbo].[LU_Title] ON;


BEGIN TRANSACTION;
INSERT INTO [dbo].[LU_Title] (Id, [Key], Description, Sequence, Active, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate)

SELECT 1, NULL, 'Project Manager', 180, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 2, NULL, 'USACOE Project Manager', 230, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 3, NULL, 'Contract Geologist', 40, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 4, NULL, 'Investigator', 100, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 5, NULL, 'Division Chief', 70, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 6, NULL, 'Project Engineer', 160, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 7, NULL, 'Project Reviewer', 190, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 8, NULL, 'Regional Chief', 200, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 9, NULL, 'Section Chief', 220, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 10, NULL, 'Project Geologist', 170, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 11, NULL, 'Deputy Program Administrator', 60, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 12, NULL, 'Chief', 20, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 13, NULL, 'Office Secretary', 130, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 14, NULL, 'Administrator', 10, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 15, NULL, 'Planner', 150, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 16, NULL, 'Natural Res. Planner', 110, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 17, NULL, 'Office Clerk II', 120, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 18, NULL, 'Chief, Water Quality', 30, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 19, NULL, 'Office Secretary III', 140, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 20, NULL, 'Engineer', 80, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 21, NULL, 'Sanitarian', 210, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 22, NULL, 'Dam Safety Division', 50, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 23, NULL, 'Forestry Specialist', 90, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 24, NULL, 'Manager', 105, 1, 'system', GETDATE(), 'system', GETDATE() UNION ALL
SELECT 25, NULL, 'Directory', 65, 1, 'system', GETDATE(), 'system', GETDATE() 
COMMIT;
RAISERROR (N'[dbo].[LU_Title]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
SET IDENTITY_INSERT [dbo].[LU_Title] OFF;