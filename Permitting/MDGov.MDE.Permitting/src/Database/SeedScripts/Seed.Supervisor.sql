﻿SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRANSACTION;

SET IDENTITY_INSERT [dbo].[Supervisor] ON

INSERT INTO Supervisor ([Id]
      ,[ContactId]
      ,[DivisionChiefId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate])
SELECT [Id]
      ,[ContactId]
      ,[DivisionChiefId]
      ,[Active]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate]
FROM MDEMembership.dbo.PreservedSupervisor;

DROP TABLE MDEMembership.dbo.PreservedSupervisor;

SET IDENTITY_INSERT [dbo].[Supervisor] OFF

COMMIT;
