﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.GeneratedLetter
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class GeneratedLetter : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? ApprovedBy_DivisionChiefId { get; set; }

    public int? LetterTemplateId { get; set; }

    public int? PermitId { get; set; }

    public string GeneratedLetter1 { get; set; }

    public DateTime? ApprovedDate_Supervisor { get; set; }

    public int? ApprovedBy_SupervisorId { get; set; }

    public DateTime? ApprovedDate_DivisionChief { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual DivisionChief DivisionChief { get; set; }

    public virtual LetterTemplate LetterTemplate { get; set; }

    public virtual Supervisor Supervisor { get; set; }

    public virtual Permit Permit { get; set; }
  }
}
