﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.Document
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class Document : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PermitStatusId { get; set; }

    public int RefId { get; set; }

    public int RefTableId { get; set; }

    public string DocumentTitle { get; set; }

    public int DocumentByteId { get; set; }

    public int? DocumentTypeId { get; set; }

    public string Description { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual DocumentByte DocumentByte { get; set; }

    public virtual LU_TableList LU_TableList { get; set; }

    public virtual LU_DocumentType LU_DocumentType { get; set; }

    public virtual LU_PermitStatus LU_PermitStatus { get; set; }
  }
}
