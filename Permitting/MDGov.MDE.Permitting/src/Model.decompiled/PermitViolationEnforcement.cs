﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitViolationEnforcement
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitViolationEnforcement : IUpdatableEntity
  {
    public int Id { get; set; }

    public int ViolationId { get; set; }

    public int EnforcementId { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual Enforcement Enforcement { get; set; }

    public virtual Violation Violation { get; set; }
  }
}
