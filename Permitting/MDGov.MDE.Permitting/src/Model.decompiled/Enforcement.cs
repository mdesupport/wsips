﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.Enforcement
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class Enforcement : IUpdatableEntity
  {
    public int Id { get; set; }

    public int EnforcementActionId { get; set; }

    public string Comments { get; set; }

    public DateTime? ActionDate { get; set; }

    public DateTime? ResponseDueDate { get; set; }

    public DateTime? ResponseReceivedDate { get; set; }

    public int ContactId { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_EnforcementAction LU_EnforcementAction { get; set; }

    public virtual ICollection<PermitViolationEnforcement> PermitViolationEnforcements { get; set; }

    public Enforcement()
    {
      this.PermitViolationEnforcements = (ICollection<PermitViolationEnforcement>) new HashSet<PermitViolationEnforcement>();
    }
  }
}
