﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.ModelContext
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
//using System.Data.Entity.Core.Objects;
using System.Data.Objects;

namespace MDGov.MDE.Permitting.Model
{
  public class ModelContext : DbContext
  {
    public DbSet<AquiferPoly> AquiferPolies { get; set; }

    public DbSet<CheckList> CheckLists { get; set; }

    public DbSet<CheckListAnswer> CheckListAnswers { get; set; }

    public DbSet<CheckListQuestion> CheckListQuestions { get; set; }

    public DbSet<Comment> Comments { get; set; }

    public DbSet<ComplianceManager> ComplianceManagers { get; set; }

    public DbSet<ComplianceStaff> ComplianceStaffs { get; set; }

    public DbSet<ConditionCompliance> ConditionCompliances { get; set; }

    public DbSet<ContactMergeHistory> ContactMergeHistories { get; set; }

    public DbSet<CountyOfficial> CountyOfficials { get; set; }

    public DbSet<DepartmentOfNaturalResource> DepartmentOfNaturalResources { get; set; }

    public DbSet<DivisionChief> DivisionChiefs { get; set; }

    public DbSet<Document> Documents { get; set; }

    public DbSet<DocumentByte> DocumentBytes { get; set; }

    public DbSet<DocumentJob> DocumentJobs { get; set; }

    public DbSet<Enforcement> Enforcements { get; set; }

    public DbSet<ExistingPermitOrApplication> ExistingPermitOrApplications { get; set; }

    public DbSet<GeneratedLetter> GeneratedLetters { get; set; }

    public DbSet<HearingNotification> HearingNotifications { get; set; }

    public DbSet<LetterTemplate> LetterTemplates { get; set; }

    public DbSet<LetterTemplateVariable> LetterTemplateVariables { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_ApplicationPacketItem> LU_ApplicationPacketItem { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_ApplicationType> LU_ApplicationType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_Aquifer> LU_Aquifer { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_AquiferType> LU_AquiferType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_CheckListAnswer> LU_CheckListAnswer { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_CommunicationMethod> LU_CommunicationMethod { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_ConditionReportingPeriod> LU_ConditionReportingPeriod { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_ContactType> LU_ContactType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_Country> LU_Country { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_County> LU_County { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_CropType> LU_CropType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_CropYieldUnit> LU_CropYieldUnit { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_Division> LU_Division { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_DocumentType> LU_DocumentType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_EducationInstituteType> LU_EducationInstituteType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_EnforcementAction> LU_EnforcementAction { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_FacilityType> LU_FacilityType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_GolfIrrigationSystemType> LU_GolfIrrigationSystemType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_HearingNotificationType> LU_HearingNotificationType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_HorticultureType> LU_HorticultureType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_IrrigationLayoutType> LU_IrrigationLayoutType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_IrrigationSystemType> LU_IrrigationSystemType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_LivestockType> LU_LivestockType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_Month> LU_Month { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitCategory> LU_PermitCategory { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitComplianceStatus> LU_PermitComplianceStatus { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitConditionComplianceStatus> LU_PermitConditionComplianceStatus { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitExternalStatus> LU_PermitExternalStatus { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitHearingStatus> LU_PermitHearingStatus { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitStatus> LU_PermitStatus { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitStatusCommunication> LU_PermitStatusCommunication { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitTemplate> LU_PermitTemplate { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PermitType> LU_PermitType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PoultryCoolingSystem> LU_PoultryCoolingSystem { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PoultryType> LU_PoultryType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_PumpageReportType> LU_PumpageReportType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_RechargeEasement> LU_RechargeEasement { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_ReportCode> LU_ReportCode { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_RestaurantType> LU_RestaurantType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_SoilType> LU_SoilType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_StandardCondition> LU_StandardCondition { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_StandardConditionType> LU_StandardConditionType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_State> LU_State { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_StateRegion> LU_StateRegion { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_StateStream> LU_StateStream { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_SupplementalType> LU_SupplementalType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_TableList> LU_TableList { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_Template> LU_Template { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_TemplateType> LU_TemplateType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_Title> LU_Title { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_VegetableType> LU_VegetableType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_ViolationType> LU_ViolationType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterDispersementType> LU_WaterDispersementType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterManagementStrategyArea> LU_WaterManagementStrategyArea { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterRemovalExcavationType> LU_WaterRemovalExcavationType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_Watershed> LU_Watershed { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterType> LU_WaterType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterWithdrawalEstimate> LU_WaterWithdrawalEstimate { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterWithdrawalPurpose> LU_WaterWithdrawalPurpose { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterWithdrawalPurposeCategory> LU_WaterWithdrawalPurposeCategory { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterWithdrawalSource> LU_WaterWithdrawalSource { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_WaterWithdrawalType> LU_WaterWithdrawalType { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.LU_ZipCode> LU_ZipCode { get; set; }

    public DbSet<Penalty> Penalties { get; set; }

    public DbSet<PermitApplicationPacket> PermitApplicationPackets { get; set; }

    public DbSet<PermitApplicationPacketItem> PermitApplicationPacketItems { get; set; }

    public DbSet<PermitCondition> PermitConditions { get; set; }

    public DbSet<PermitCounty> PermitCounties { get; set; }

    public DbSet<PermitHearing> PermitHearings { get; set; }

    public DbSet<PermitHearingNotification> PermitHearingNotifications { get; set; }

    public DbSet<PermitSoilType> PermitSoilTypes { get; set; }

    public DbSet<PermitStatusHistory> PermitStatusHistories { get; set; }

    public DbSet<PermitStatusState> PermitStatusStates { get; set; }

    public DbSet<PermitStatusWorkflow> PermitStatusWorkflows { get; set; }

    public DbSet<PermitSupplementalGroup> PermitSupplementalGroups { get; set; }

    public DbSet<PermitViolationEnforcement> PermitViolationEnforcements { get; set; }

    public DbSet<PermitViolationPenalty> PermitViolationPenalties { get; set; }

    public DbSet<PermitWastewaterTreatmentDisposal> PermitWastewaterTreatmentDisposals { get; set; }

    public DbSet<PermitWaterDispersement> PermitWaterDispersements { get; set; }

    public DbSet<PermitWaterWithdrawalMisc> PermitWaterWithdrawalMiscs { get; set; }

    public DbSet<PermitWithdrawalGroundwater> PermitWithdrawalGroundwaters { get; set; }

    public DbSet<PermitWithdrawalSurfacewater> PermitWithdrawalSurfacewaters { get; set; }

    public DbSet<PermitWwAquacultureOrAquarium> PermitWwAquacultureOrAquariums { get; set; }

    public DbSet<PermitWwConstructionDewatering> PermitWwConstructionDewaterings { get; set; }

    public DbSet<PermitWwFarmPortableSupply> PermitWwFarmPortableSupplies { get; set; }

    public DbSet<PermitWwFoodProcessing> PermitWwFoodProcessings { get; set; }

    public DbSet<PermitWwHorticulture> PermitWwHorticultures { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.PermitWwInstitutionalEducational_drinkingOrsanitary> PermitWwInstitutionalEducational_drinkingOrsanitary { get; set; }

    public DbSet<MDGov.MDE.Permitting.Model.PermitWwInstitutionalReligious_DringkingOrSanitary> PermitWwInstitutionalReligious_DringkingOrSanitary { get; set; }

    public DbSet<PermitWwLivestock> PermitWwLivestocks { get; set; }

    public DbSet<PermitWwPrivateWaterSupplier> PermitWwPrivateWaterSuppliers { get; set; }

    public DbSet<PermitWwVegetable> PermitWwVegetables { get; set; }

    public DbSet<PermitWwWildlifePond> PermitWwWildlifePonds { get; set; }

    public DbSet<ProjectManager> ProjectManagers { get; set; }

    public DbSet<ProjectManagerCounty> ProjectManagerCounties { get; set; }

    public DbSet<PublicComment> PublicComments { get; set; }

    public DbSet<PumpageReport> PumpageReports { get; set; }

    public DbSet<Report> Reports { get; set; }

    public DbSet<ReportParameter> ReportParameters { get; set; }

    public DbSet<Secretary> Secretaries { get; set; }

    public DbSet<Supervisor> Supervisors { get; set; }

    public DbSet<WaterWithdrawalLocationPurpose> WaterWithdrawalLocationPurposes { get; set; }

    public DbSet<DashboardPermit> DashboardPermits { get; set; }

    public DbSet<SupplementalGroup> SupplementalGroups { get; set; }

    public DbSet<AdministrativeSpecialist> AdministrativeSpecialists { get; set; }

    public DbSet<Violation> Violations { get; set; }

    public DbSet<Permit> Permits { get; set; }

    public DbSet<PermitWwPoultry> PermitWwPoultries { get; set; }

    public DbSet<PermitParcel> PermitParcels { get; set; }

    public DbSet<PermitWithdrawalSurfacewaterDetail> PermitWithdrawalSurfacewaterDetails { get; set; }

    public DbSet<PermitsFailedToReportTwoPeriod> PermitsFailedToReportTwoPeriods { get; set; }

    public DbSet<Contact> Contacts { get; set; }

    public DbSet<ContactCommunicationMethod> ContactCommunicationMethods { get; set; }

    public DbSet<PermitContact> PermitContacts { get; set; }

    public DbSet<PermitContactInformation> PermitContactInformations { get; set; }

    public DbSet<PumpageReportDetail> PumpageReportDetails { get; set; }

    public DbSet<PermitWwAgricultural> PermitWwAgriculturals { get; set; }

    public DbSet<PermitWwCommercialDrinkingOrSanitary> PermitWwCommercialDrinkingOrSanitaries { get; set; }

    public DbSet<PermitWwCrop> PermitWwCrops { get; set; }

    public DbSet<PermitWwGolfCourseIrrigation> PermitWwGolfCourseIrrigations { get; set; }

    public DbSet<PermitWwNonAgricultureIrrigation> PermitWwNonAgricultureIrrigations { get; set; }

    public DbSet<PermitWaterWithdrawalPurpose> PermitWaterWithdrawalPurposes { get; set; }

    public DbSet<PermitWithdrawalGroundwaterDetail> PermitWithdrawalGroundwaterDetails { get; set; }

    public ModelContext()
      : base("name=ModelContext")
    {
      //this.\u002Ector("name=ModelContext");
      //this.get_Configuration().set_ProxyCreationEnabled(false);
        this.Configuration.ProxyCreationEnabled = false;
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      throw new UnintentionalCodeFirstException();
    }

    public virtual ObjectResult<sp_GetPermitReport_Result> sp_GetPermitReport(int? reportParameterId)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction<sp_GetPermitReport_Result>("sp_GetPermitReport", new ObjectParameter[1]
      //{
      //  reportParameterId.HasValue ? new ObjectParameter("ReportParameterId", (object) reportParameterId) : new ObjectParameter("ReportParameterId", typeof (int))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetPermitReport_Result>("sp_GetPermitReport", new ObjectParameter[1]
      {
        reportParameterId.HasValue ? new ObjectParameter("ReportParameterId", (object) reportParameterId) : new ObjectParameter("ReportParameterId", typeof (int))
      });
    }

    public virtual ObjectResult<int?> sp_InsertReportParameter(string permitIds)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction<int?>("sp_InsertReportParameter", new ObjectParameter[1]
      //{
      //  permitIds != null ? new ObjectParameter("PermitIds", (object) permitIds) : new ObjectParameter("PermitIds", typeof (string))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<int?>("sp_InsertReportParameter", new ObjectParameter[1]
      {
        permitIds != null ? new ObjectParameter("PermitIds", (object) permitIds) : new ObjectParameter("PermitIds", typeof (string))
      });
    }

    public virtual int sp_MergeContacts(int? fromContactId, int? toContactId)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction("sp_MergeContacts", new ObjectParameter[2]
      //{
      //  fromContactId.HasValue ? new ObjectParameter("FromContactId", (object) fromContactId) : new ObjectParameter("FromContactId", typeof (int)),
      //  toContactId.HasValue ? new ObjectParameter("ToContactId", (object) toContactId) : new ObjectParameter("ToContactId", typeof (int))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_MergeContacts", new ObjectParameter[2]
      {
        fromContactId.HasValue ? new ObjectParameter("FromContactId", (object) fromContactId) : new ObjectParameter("FromContactId", typeof (int)),
        toContactId.HasValue ? new ObjectParameter("ToContactId", (object) toContactId) : new ObjectParameter("ToContactId", typeof (int))
      });
    }

    public virtual ObjectResult<sp_GetWaterWithdrawalLocation_Result> sp_GetWaterWithdrawalLocation(int? id)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction<sp_GetWaterWithdrawalLocation_Result>("sp_GetWaterWithdrawalLocation", new ObjectParameter[1]
      //{
      //  id.HasValue ? new ObjectParameter("id", (object) id) : new ObjectParameter("id", typeof (int))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetWaterWithdrawalLocation_Result>("sp_GetWaterWithdrawalLocation", new ObjectParameter[1]
      {
        id.HasValue ? new ObjectParameter("id", (object) id) : new ObjectParameter("id", typeof (int))
      });
    }

    public virtual int sp_RemovePermitLocation(int? objectId)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction("sp_RemovePermitLocation", new ObjectParameter[1]
      //{
      //  objectId.HasValue ? new ObjectParameter("objectId", (object) objectId) : new ObjectParameter("objectId", typeof (int))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_RemovePermitLocation", new ObjectParameter[1]
      {
        objectId.HasValue ? new ObjectParameter("objectId", (object) objectId) : new ObjectParameter("objectId", typeof (int))
      });
    }

    public virtual int sp_RemoveWaterWithdrawalLocation(int? objectId)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction("sp_RemoveWaterWithdrawalLocation", new ObjectParameter[1]
      //{
      //  objectId.HasValue ? new ObjectParameter("objectId", (object) objectId) : new ObjectParameter("objectId", typeof (int))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_RemoveWaterWithdrawalLocation", new ObjectParameter[1]
      {
        objectId.HasValue ? new ObjectParameter("objectId", (object) objectId) : new ObjectParameter("objectId", typeof (int))
      });
    }

    public virtual ObjectResult<int?> sp_AddPermitLocation(int? permitId, double? withdrawalLocationX, double? withdrawalLocationY, int? spatialReferenceId, string user, bool? suppressOutput, ObjectParameter countyId, ObjectParameter permitLocationOID, ObjectParameter taxMapNumber, ObjectParameter taxMapBlockNumber, ObjectParameter taxMapSubBlockNumber, ObjectParameter taxMapParcel, ObjectParameter lotNumber, ObjectParameter parcelStreet, ObjectParameter parcelCity, ObjectParameter parcelState, ObjectParameter parcelZip)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction<int?>("sp_AddPermitLocation", permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int)), withdrawalLocationX.HasValue ? new ObjectParameter("withdrawalLocationX", (object) withdrawalLocationX) : new ObjectParameter("withdrawalLocationX", typeof (double)), withdrawalLocationY.HasValue ? new ObjectParameter("withdrawalLocationY", (object) withdrawalLocationY) : new ObjectParameter("withdrawalLocationY", typeof (double)), spatialReferenceId.HasValue ? new ObjectParameter("spatialReferenceId", (object) spatialReferenceId) : new ObjectParameter("spatialReferenceId", typeof (int)), user != null ? new ObjectParameter("user", (object) user) : new ObjectParameter("user", typeof (string)), suppressOutput.HasValue ? new ObjectParameter("suppressOutput", (object) suppressOutput) : new ObjectParameter("suppressOutput", typeof (bool)), countyId, permitLocationOID, taxMapNumber, taxMapBlockNumber, taxMapSubBlockNumber, taxMapParcel, lotNumber, parcelStreet, parcelCity, parcelState, parcelZip);
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<int?>("sp_AddPermitLocation", 
            permitId.HasValue ? new ObjectParameter("permitId", (object)permitId) : new ObjectParameter("permitId", typeof(int)), 
            withdrawalLocationX.HasValue ? new ObjectParameter("withdrawalLocationX", (object)withdrawalLocationX) : new ObjectParameter("withdrawalLocationX", typeof(double)), 
            withdrawalLocationY.HasValue ? new ObjectParameter("withdrawalLocationY", (object)withdrawalLocationY) : new ObjectParameter("withdrawalLocationY", typeof(double)), 
            spatialReferenceId.HasValue ? new ObjectParameter("spatialReferenceId", (object)spatialReferenceId) : new ObjectParameter("spatialReferenceId", typeof(int)), 
            user != null ? new ObjectParameter("user", (object)user) : new ObjectParameter("user", typeof(string)), 
            suppressOutput.HasValue ? new ObjectParameter("suppressOutput", (object)suppressOutput) : new ObjectParameter("suppressOutput", typeof(bool)), 
            countyId, permitLocationOID, taxMapNumber, taxMapBlockNumber, taxMapSubBlockNumber, taxMapParcel, lotNumber, parcelStreet, parcelCity, parcelState, parcelZip);
    }

    public virtual ObjectResult<int?> sp_AddWaterWithdrawalLocation(int? permitId, double? withdrawalLocationX, double? withdrawalLocationY, int? spatialReferenceId, int? withdrawalSourceId, int? withdrawalTypeId, string selectedPermitWaterWithdrawalPurposeIds, int? stateStreamId, string otherStreamName, string intakeLocation, bool? isExistingWell, Decimal? wellDepth, Decimal? wellDiameter, string wellTagNo, string comment, string user)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction<int?>("sp_AddWaterWithdrawalLocation", permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int)), withdrawalLocationX.HasValue ? new ObjectParameter("withdrawalLocationX", (object) withdrawalLocationX) : new ObjectParameter("withdrawalLocationX", typeof (double)), withdrawalLocationY.HasValue ? new ObjectParameter("withdrawalLocationY", (object) withdrawalLocationY) : new ObjectParameter("withdrawalLocationY", typeof (double)), spatialReferenceId.HasValue ? new ObjectParameter("spatialReferenceId", (object) spatialReferenceId) : new ObjectParameter("spatialReferenceId", typeof (int)), withdrawalSourceId.HasValue ? new ObjectParameter("withdrawalSourceId", (object) withdrawalSourceId) : new ObjectParameter("withdrawalSourceId", typeof (int)), withdrawalTypeId.HasValue ? new ObjectParameter("withdrawalTypeId", (object) withdrawalTypeId) : new ObjectParameter("withdrawalTypeId", typeof (int)), selectedPermitWaterWithdrawalPurposeIds != null ? new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", (object) selectedPermitWaterWithdrawalPurposeIds) : new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", typeof (string)), stateStreamId.HasValue ? new ObjectParameter("stateStreamId", (object) stateStreamId) : new ObjectParameter("stateStreamId", typeof (int)), otherStreamName != null ? new ObjectParameter("otherStreamName", (object) otherStreamName) : new ObjectParameter("otherStreamName", typeof (string)), intakeLocation != null ? new ObjectParameter("intakeLocation", (object) intakeLocation) : new ObjectParameter("intakeLocation", typeof (string)), isExistingWell.HasValue ? new ObjectParameter("isExistingWell", (object) isExistingWell) : new ObjectParameter("isExistingWell", typeof (bool)), wellDepth.HasValue ? new ObjectParameter("wellDepth", (object) wellDepth) : new ObjectParameter("wellDepth", typeof (Decimal)), wellDiameter.HasValue ? new ObjectParameter("wellDiameter", (object) wellDiameter) : new ObjectParameter("wellDiameter", typeof (Decimal)), wellTagNo != null ? new ObjectParameter("wellTagNo", (object) wellTagNo) : new ObjectParameter("wellTagNo", typeof (string)), comment != null ? new ObjectParameter("comment", (object) comment) : new ObjectParameter("comment", typeof (string)), user != null ? new ObjectParameter("user", (object) user) : new ObjectParameter("user", typeof (string)));
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<int?>("sp_AddWaterWithdrawalLocation", 
            permitId.HasValue ? new ObjectParameter("permitId", (object)permitId) : new ObjectParameter("permitId", typeof(int)), 
            withdrawalLocationX.HasValue ? new ObjectParameter("withdrawalLocationX", (object)withdrawalLocationX) : new ObjectParameter("withdrawalLocationX", typeof(double)), 
            withdrawalLocationY.HasValue ? new ObjectParameter("withdrawalLocationY", (object)withdrawalLocationY) : new ObjectParameter("withdrawalLocationY", typeof(double)), 
            spatialReferenceId.HasValue ? new ObjectParameter("spatialReferenceId", (object)spatialReferenceId) : new ObjectParameter("spatialReferenceId", typeof(int)), 
            withdrawalSourceId.HasValue ? new ObjectParameter("withdrawalSourceId", (object)withdrawalSourceId) : new ObjectParameter("withdrawalSourceId", typeof(int)), 
            withdrawalTypeId.HasValue ? new ObjectParameter("withdrawalTypeId", (object)withdrawalTypeId) : new ObjectParameter("withdrawalTypeId", typeof(int)), 
            selectedPermitWaterWithdrawalPurposeIds != null ? new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", (object)selectedPermitWaterWithdrawalPurposeIds) : new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", typeof(string)), 
            stateStreamId.HasValue ? new ObjectParameter("stateStreamId", (object)stateStreamId) : new ObjectParameter("stateStreamId", typeof(int)), 
            otherStreamName != null ? new ObjectParameter("otherStreamName", (object)otherStreamName) : new ObjectParameter("otherStreamName", typeof(string)), 
            intakeLocation != null ? new ObjectParameter("intakeLocation", (object)intakeLocation) : new ObjectParameter("intakeLocation", typeof(string)), 
            isExistingWell.HasValue ? new ObjectParameter("isExistingWell", (object)isExistingWell) : new ObjectParameter("isExistingWell", typeof(bool)), 
            wellDepth.HasValue ? new ObjectParameter("wellDepth", (object)wellDepth) : new ObjectParameter("wellDepth", typeof(Decimal)), 
            wellDiameter.HasValue ? new ObjectParameter("wellDiameter", (object)wellDiameter) : new ObjectParameter("wellDiameter", typeof(Decimal)), 
            wellTagNo != null ? new ObjectParameter("wellTagNo", (object)wellTagNo) : new ObjectParameter("wellTagNo", typeof(string)), 
            comment != null ? new ObjectParameter("comment", (object)comment) : new ObjectParameter("comment", typeof(string)), 
            user != null ? new ObjectParameter("user", (object)user) : new ObjectParameter("user", typeof(string)));
    }

    public virtual int sp_AggregateWaterUsageAmounts(int? permitId, string user)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction("sp_AggregateWaterUsageAmounts", new ObjectParameter[2]
      //{
      //  permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int)),
      //  user != null ? new ObjectParameter("user", (object) user) : new ObjectParameter("user", typeof (string))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_AggregateWaterUsageAmounts", new ObjectParameter[2]
      {
        permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int)),
        user != null ? new ObjectParameter("user", (object) user) : new ObjectParameter("user", typeof (string))
      });
    }

    public virtual ObjectResult<int?> sp_EditWaterWithdrawalLocation(int? id, int? withdrawalSourceId, int? withdrawalTypeId, string selectedPermitWaterWithdrawalPurposeIds, int? stateStreamId, string otherStreamName, string intakeLocation, bool? isExistingWell, Decimal? wellDepth, Decimal? wellDiameter, string wellTagNo, string comment, string user)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction<int?>("sp_EditWaterWithdrawalLocation", id.HasValue ? new ObjectParameter("id", (object) id) : new ObjectParameter("id", typeof (int)), withdrawalSourceId.HasValue ? new ObjectParameter("withdrawalSourceId", (object) withdrawalSourceId) : new ObjectParameter("withdrawalSourceId", typeof (int)), withdrawalTypeId.HasValue ? new ObjectParameter("withdrawalTypeId", (object) withdrawalTypeId) : new ObjectParameter("withdrawalTypeId", typeof (int)), selectedPermitWaterWithdrawalPurposeIds != null ? new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", (object) selectedPermitWaterWithdrawalPurposeIds) : new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", typeof (string)), stateStreamId.HasValue ? new ObjectParameter("stateStreamId", (object) stateStreamId) : new ObjectParameter("stateStreamId", typeof (int)), otherStreamName != null ? new ObjectParameter("otherStreamName", (object) otherStreamName) : new ObjectParameter("otherStreamName", typeof (string)), intakeLocation != null ? new ObjectParameter("intakeLocation", (object) intakeLocation) : new ObjectParameter("intakeLocation", typeof (string)), isExistingWell.HasValue ? new ObjectParameter("isExistingWell", (object) isExistingWell) : new ObjectParameter("isExistingWell", typeof (bool)), wellDepth.HasValue ? new ObjectParameter("wellDepth", (object) wellDepth) : new ObjectParameter("wellDepth", typeof (Decimal)), wellDiameter.HasValue ? new ObjectParameter("wellDiameter", (object) wellDiameter) : new ObjectParameter("wellDiameter", typeof (Decimal)), wellTagNo != null ? new ObjectParameter("wellTagNo", (object) wellTagNo) : new ObjectParameter("wellTagNo", typeof (string)), comment != null ? new ObjectParameter("comment", (object) comment) : new ObjectParameter("comment", typeof (string)), user != null ? new ObjectParameter("user", (object) user) : new ObjectParameter("user", typeof (string)));
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<int?>("sp_EditWaterWithdrawalLocation", 
            id.HasValue ? new ObjectParameter("id", (object)id) : new ObjectParameter("id", typeof(int)), 
            withdrawalSourceId.HasValue ? new ObjectParameter("withdrawalSourceId", (object)withdrawalSourceId) : new ObjectParameter("withdrawalSourceId", typeof(int)), 
            withdrawalTypeId.HasValue ? new ObjectParameter("withdrawalTypeId", (object)withdrawalTypeId) : new ObjectParameter("withdrawalTypeId", typeof(int)), 
            selectedPermitWaterWithdrawalPurposeIds != null ? new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", (object)selectedPermitWaterWithdrawalPurposeIds) : new ObjectParameter("selectedPermitWaterWithdrawalPurposeIds", typeof(string)), 
            stateStreamId.HasValue ? new ObjectParameter("stateStreamId", (object)stateStreamId) : new ObjectParameter("stateStreamId", typeof(int)), 
            otherStreamName != null ? new ObjectParameter("otherStreamName", (object)otherStreamName) : new ObjectParameter("otherStreamName", typeof(string)), 
            intakeLocation != null ? new ObjectParameter("intakeLocation", (object)intakeLocation) : new ObjectParameter("intakeLocation", typeof(string)), 
            isExistingWell.HasValue ? new ObjectParameter("isExistingWell", (object)isExistingWell) : new ObjectParameter("isExistingWell", typeof(bool)), 
            wellDepth.HasValue ? new ObjectParameter("wellDepth", (object)wellDepth) : new ObjectParameter("wellDepth", typeof(Decimal)), 
            wellDiameter.HasValue ? new ObjectParameter("wellDiameter", (object)wellDiameter) : new ObjectParameter("wellDiameter", typeof(Decimal)), 
            wellTagNo != null ? new ObjectParameter("wellTagNo", (object)wellTagNo) : new ObjectParameter("wellTagNo", typeof(string)), 
            comment != null ? new ObjectParameter("comment", (object)comment) : new ObjectParameter("comment", typeof(string)), 
            user != null ? new ObjectParameter("user", (object)user) : new ObjectParameter("user", typeof(string)));
    }

    public virtual int sp_CalculateConditionComplianceValues(int? permitId, int? year)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction("sp_CalculateConditionComplianceValues", new ObjectParameter[2]
      //{
      //  permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int)),
      //  year.HasValue ? new ObjectParameter("year", (object) year) : new ObjectParameter("year", typeof (int))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_CalculateConditionComplianceValues", new ObjectParameter[2]
      {
        permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int)),
        year.HasValue ? new ObjectParameter("year", (object) year) : new ObjectParameter("year", typeof (int))
      });
    }

    public virtual int sp_CancelPermitApplication(int? permitId)
    {
      //return ((IObjectContextAdapter) this).get_ObjectContext().ExecuteFunction("sp_CancelPermitApplication", new ObjectParameter[1]
      //{
      //  permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int))
      //});
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_CancelPermitApplication", new ObjectParameter[1]
      {
        permitId.HasValue ? new ObjectParameter("permitId", (object) permitId) : new ObjectParameter("permitId", typeof (int))
      });
    }
  }
}
