﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWithdrawalGroundwaterDetail
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWithdrawalGroundwaterDetail : IUpdatableEntity
  {
    public int Id { get; set; }

    public int GroundwaterTypeId { get; set; }

    public int PermitWithdrawalGroundwaterId { get; set; }

    public int? WatershedId { get; set; }

    public int? CountyId { get; set; }

    public bool? IsExisting { get; set; }

    public string WellTagNumber { get; set; }

    public Decimal? WellDepthFeet { get; set; }

    public Decimal? WellDiameterInches { get; set; }

    public int? UsePercent { get; set; }

    public string Comment { get; set; }

    public string TaxMapNumber { get; set; }

    public string TaxMapBlockNumber { get; set; }

    public string TaxMapSubBlockNumber { get; set; }

    public string TaxMapParcel { get; set; }

    public string LotNumber { get; set; }

    public string Street { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string Zip { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_Watershed LU_Watershed { get; set; }

    public virtual LU_WaterWithdrawalType LU_WaterWithdrawalType { get; set; }

    public virtual PermitWithdrawalGroundwater PermitWithdrawalGroundwater { get; set; }
  }
}
