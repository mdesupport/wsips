﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitCondition
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitCondition : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? ConditionReportingPeriodId { get; set; }

    public int? PermitId { get; set; }

    public int? StandardConditionTypeId { get; set; }

    public string ConditionText { get; set; }

    public bool? RequiresSelfReporting { get; set; }

    public int? OneTimeReportDays { get; set; }

    public bool? RequiresValidation { get; set; }

    public string ComplianceReportingDueDate { get; set; }

    public int? PumpageReportTypeId { get; set; }

    public int? Sequence { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<ConditionCompliance> ConditionCompliances { get; set; }

    public virtual LU_ConditionReportingPeriod LU_ConditionReportingPeriod { get; set; }

    public virtual LU_PumpageReportType LU_PumpageReportType { get; set; }

    public virtual LU_StandardConditionType LU_StandardConditionType { get; set; }

    public virtual Permit Permit { get; set; }

    public PermitCondition()
    {
      this.ConditionCompliances = (ICollection<ConditionCompliance>) new HashSet<ConditionCompliance>();
    }
  }
}
