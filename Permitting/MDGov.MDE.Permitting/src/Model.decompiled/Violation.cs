﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.Violation
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class Violation : IUpdatableEntity
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public string FileNumber { get; set; }

    public int ViolationTypeId { get; set; }

    public DateTime ViolationDate { get; set; }

    public string Description { get; set; }

    public string Status { get; set; }

    public DateTime? ResolutionDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_ViolationType LU_ViolationType { get; set; }

    public virtual ICollection<PermitViolationEnforcement> PermitViolationEnforcements { get; set; }

    public virtual ICollection<PermitViolationPenalty> PermitViolationPenalties { get; set; }

    public virtual Permit Permit { get; set; }

    public Violation()
    {
      this.PermitViolationEnforcements = (ICollection<PermitViolationEnforcement>) new HashSet<PermitViolationEnforcement>();
      this.PermitViolationPenalties = (ICollection<PermitViolationPenalty>) new HashSet<PermitViolationPenalty>();
    }
  }
}
