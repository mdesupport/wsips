﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.DashboardPermit
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using System;

namespace MDGov.MDE.Permitting.Model
{
  public class DashboardPermit
  {
    public int Id { get; set; }

    public string PermitName { get; set; }

    public string CountyCode { get; set; }

    public int? PermitteeContactId { get; set; }

    public string PermitteeName { get; set; }

    public string PermitCategory { get; set; }

    public string PermitStatus { get; set; }

    public bool? HasRelatedPermits { get; set; }

    public int? IsLargePermit { get; set; }

    public bool? RequiresAdvertising { get; set; }

    public int? DaysPending { get; set; }

    public int? ProjectManagerContactId { get; set; }

    public string ProjectManagerName { get; set; }

    public DateTime? Date { get; set; }
  }
}
