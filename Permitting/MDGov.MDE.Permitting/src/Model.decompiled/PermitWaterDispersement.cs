﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWaterDispersement
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWaterDispersement : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? AquiferId { get; set; }

    public int? PermitId { get; set; }

    public int? StateStreamId { get; set; }

    public int? WaterDispersementTypeId { get; set; }

    public string WasteWaterTreatmentOther { get; set; }

    public string DischargePermitNumber { get; set; }

    public string StreamName { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_Aquifer LU_Aquifer { get; set; }

    public virtual LU_StateStream LU_StateStream { get; set; }

    public virtual LU_WaterDispersementType LU_WaterDispersementType { get; set; }

    public virtual Permit Permit { get; set; }
  }
}
