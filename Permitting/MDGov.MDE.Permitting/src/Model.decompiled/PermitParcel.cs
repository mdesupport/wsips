﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitParcel
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using Newtonsoft.Json;
using System;
using System.Data.Spatial;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitParcel : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PermitId { get; set; }

    public int? CountyId { get; set; }

    public bool? IsAdjacent { get; set; }

    public string ParcelId { get; set; }

    public string TaxMapNumber { get; set; }

    public string TaxMapBlockNumber { get; set; }

    public string TaxMapSubBlockNumber { get; set; }

    public string TaxMapParcel { get; set; }

    public string LotNumber { get; set; }

    public string Street { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string Zip { get; set; }

    [JsonIgnore]
    public DbGeometry Shape { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_County LU_County { get; set; }

    public virtual Permit Permit { get; set; }
  }
}
