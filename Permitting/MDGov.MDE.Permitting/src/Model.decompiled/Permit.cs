﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.Permit
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class Permit : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? RefId { get; set; }

    public int? ApplicationTypeId { get; set; }

    public int? Compliance_ContactId { get; set; }

    public int? ComplianceStatusId { get; set; }

    public int? PermitStatusId { get; set; }

    public int? PermitTypeId { get; set; }

    public int? ReportCodeId { get; set; }

    public int? WaterManagementStrategyAreaId { get; set; }

    public int? WaterTypeId { get; set; }

    public int? TidalTypeId { get; set; }

    public int? AquiferId { get; set; }

    public string ApplicantIdentification { get; set; }

    public int? RamsWanActid { get; set; }

    public string PermitNumber { get; set; }

    public string RevisionNumber { get; set; }

    public string PermitName { get; set; }

    public bool? HasNtwWarning { get; set; }

    public bool? RequiresAdvertising { get; set; }

    public string PWSID { get; set; }

    public bool? IsExemptFromFees { get; set; }

    public bool? HasWtrAuditWarning { get; set; }

    public string ProjectName { get; set; }

    public bool? AreAbandonedWellsPresent { get; set; }

    public bool? IsVoluntaryPermit { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public DateTime? EffectiveDate { get; set; }

    public DateTime? ExpirationDate { get; set; }

    public DateTime? AppropriationDate { get; set; }

    public DateTime? DateOut { get; set; }

    public bool? IsInRechargeEasement { get; set; }

    public bool? IsInTier2Watershed { get; set; }

    public bool? IsInWMStrategyArea { get; set; }

    public string HB935VerifiedBy { get; set; }

    public DateTime? HB935VerifiedDate { get; set; }

    public bool? HardRockSubsurface { get; set; }

    public bool? SbdnWarning { get; set; }

    public bool? AquiferTest { get; set; }

    public string Location { get; set; }

    public string SourceDescription { get; set; }

    public string UseDescription { get; set; }

    public bool? WaterQualityAnalysis { get; set; }

    public string Description { get; set; }

    public string Remarks { get; set; }

    public string City { get; set; }

    public bool? StateWide { get; set; }

    public string SubmittedBy { get; set; }

    public DateTime? SubmittedDate { get; set; }

    public string CountyOfficialSignOffBy { get; set; }

    public DateTime? CountyOfficialSignOffDate { get; set; }

    public bool? CountyOfficialSignOffYesNo { get; set; }

    public string FormCode { get; set; }

    public string ReportCode { get; set; }

    public bool? IsSubjectToEasement { get; set; }

    public bool? HaveNotifiedEasementHolder { get; set; }

    public string ADCXCoord { get; set; }

    public string ADCYCoord { get; set; }

    public string ADCPageLetterGrid { get; set; }

    public string CountyHydroGeoMap { get; set; }

    public string USGSTopoMap { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<CheckListAnswer> CheckListAnswers { get; set; }

    public virtual ICollection<GeneratedLetter> GeneratedLetters { get; set; }

    public virtual LU_ApplicationType LU_ApplicationType { get; set; }

    public virtual LU_Aquifer LU_Aquifer { get; set; }

    public virtual LU_PermitComplianceStatus LU_PermitComplianceStatus { get; set; }

    public virtual LU_PermitStatus LU_PermitStatus { get; set; }

    public virtual LU_PermitType LU_PermitType { get; set; }

    public virtual LU_ReportCode LU_ReportCode { get; set; }

    public virtual LU_WaterType LU_WaterType { get; set; }

    public virtual ICollection<PermitSupplementalGroup> PermitSupplementalGroups { get; set; }

    public virtual ICollection<PublicComment> PublicComments { get; set; }

    public virtual ICollection<WaterWithdrawalLocationPurpose> WaterWithdrawalLocationPurposes { get; set; }

    public virtual ICollection<PermitCondition> PermitConditions { get; set; }

    public virtual ICollection<PermitSoilType> PermitSoilTypes { get; set; }

    public virtual ICollection<PermitHearing> PermitHearings { get; set; }

    public virtual ICollection<PermitApplicationPacket> PermitApplicationPackets { get; set; }

    public virtual ICollection<PermitWithdrawalSurfacewater> PermitWithdrawalSurfacewaters { get; set; }

    public virtual ICollection<PermitCounty> PermitCounties { get; set; }

    public virtual ICollection<Permit> Permit1 { get; set; }

    public virtual Permit Permit2 { get; set; }

    public virtual ICollection<PermitStatusHistory> PermitStatusHistories { get; set; }

    public virtual ICollection<Violation> Violations { get; set; }

    public virtual ICollection<PermitHearingNotification> PermitHearingNotifications { get; set; }

    public virtual ICollection<PermitWaterDispersement> PermitWaterDispersements { get; set; }

    public virtual ICollection<PermitWithdrawalGroundwater> PermitWithdrawalGroundwaters { get; set; }

    public virtual ICollection<PermitStatusState> PermitStatusStates { get; set; }

    public virtual ICollection<PermitParcel> PermitParcels { get; set; }

    public virtual Contact Contact { get; set; }

    public virtual ICollection<PermitContact> PermitContacts { get; set; }

    public virtual ICollection<PermitWaterWithdrawalPurpose> PermitWaterWithdrawalPurposes { get; set; }

    public Permit()
    {
      this.CheckListAnswers = (ICollection<CheckListAnswer>) new HashSet<CheckListAnswer>();
      this.GeneratedLetters = (ICollection<GeneratedLetter>) new HashSet<GeneratedLetter>();
      this.PermitSupplementalGroups = (ICollection<PermitSupplementalGroup>) new HashSet<PermitSupplementalGroup>();
      this.PublicComments = (ICollection<PublicComment>) new HashSet<PublicComment>();
      this.WaterWithdrawalLocationPurposes = (ICollection<WaterWithdrawalLocationPurpose>) new HashSet<WaterWithdrawalLocationPurpose>();
      this.PermitConditions = (ICollection<PermitCondition>) new HashSet<PermitCondition>();
      this.PermitSoilTypes = (ICollection<PermitSoilType>) new HashSet<PermitSoilType>();
      this.PermitHearings = (ICollection<PermitHearing>) new HashSet<PermitHearing>();
      this.PermitApplicationPackets = (ICollection<PermitApplicationPacket>) new HashSet<PermitApplicationPacket>();
      this.PermitWithdrawalSurfacewaters = (ICollection<PermitWithdrawalSurfacewater>) new HashSet<PermitWithdrawalSurfacewater>();
      this.PermitCounties = (ICollection<PermitCounty>) new HashSet<PermitCounty>();
      this.Permit1 = (ICollection<Permit>) new HashSet<Permit>();
      this.PermitStatusHistories = (ICollection<PermitStatusHistory>) new HashSet<PermitStatusHistory>();
      this.Violations = (ICollection<Violation>) new HashSet<Violation>();
      this.PermitHearingNotifications = (ICollection<PermitHearingNotification>) new HashSet<PermitHearingNotification>();
      this.PermitWaterDispersements = (ICollection<PermitWaterDispersement>) new HashSet<PermitWaterDispersement>();
      this.PermitWithdrawalGroundwaters = (ICollection<PermitWithdrawalGroundwater>) new HashSet<PermitWithdrawalGroundwater>();
      this.PermitStatusStates = (ICollection<PermitStatusState>) new HashSet<PermitStatusState>();
      this.PermitParcels = (ICollection<PermitParcel>) new HashSet<PermitParcel>();
      this.PermitContacts = (ICollection<PermitContact>) new HashSet<PermitContact>();
      this.PermitWaterWithdrawalPurposes = (ICollection<PermitWaterWithdrawalPurpose>) new HashSet<PermitWaterWithdrawalPurpose>();
    }
  }
}
