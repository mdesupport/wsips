﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitContact
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitContact : IUpdatableEntity
  {
    public int Id { get; set; }

    public int ContactId { get; set; }

    public int? ContactTypeId { get; set; }

    public int? PermitContactInformationId { get; set; }

    public int PermitId { get; set; }

    public bool IsApplicant { get; set; }

    public bool IsPermittee { get; set; }

    public bool IsPrimary { get; set; }

    public bool Active { get; set; }

    public int? Sequence { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public int? SourceId { get; set; }

    public string SourceTable { get; set; }

    public virtual Contact Contact { get; set; }

    public virtual LU_ContactType LU_ContactType { get; set; }

    public virtual Permit Permit { get; set; }

    public virtual PermitContactInformation PermitContactInformation { get; set; }
  }
}
