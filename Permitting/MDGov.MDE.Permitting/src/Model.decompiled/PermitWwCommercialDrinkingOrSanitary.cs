﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWwCommercialDrinkingOrSanitary
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWwCommercialDrinkingOrSanitary : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PermitWaterWithdrawalPurposeId { get; set; }

    public int? AverageGallonPerDayFromGroundWater { get; set; }

    public int? MaximumGallonPerDayFromGroundWater { get; set; }

    public int? AverageGallonPerDayFromSurfaceWater { get; set; }

    public int? MaximumGallonPerDayFromSurfaceWater { get; set; }

    public int? NoOfLotsOrUnits { get; set; }

    public string EstimateDescription { get; set; }

    public int? TypeOfFacilityId { get; set; }

    public Decimal? AreaOrSquareFootage { get; set; }

    public int? NoOfEmployees { get; set; }

    public int? AvgNumberOfCustomersPerDay { get; set; }

    public int? RestaurantTypeId { get; set; }

    public int? NoOfSeats { get; set; }

    public int? NoOfSlips { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_FacilityType LU_FacilityType { get; set; }

    public virtual LU_RestaurantType LU_RestaurantType { get; set; }

    public virtual PermitWaterWithdrawalPurpose PermitWaterWithdrawalPurpose { get; set; }
  }
}
