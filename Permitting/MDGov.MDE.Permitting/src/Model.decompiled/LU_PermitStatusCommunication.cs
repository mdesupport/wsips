﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_PermitStatusCommunication
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_PermitStatusCommunication : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public int? PermitStatusId { get; set; }

    public int? ContactTypeId { get; set; }

    public string EmailSubject { get; set; }

    public string EmailBody { get; set; }

    public string NotificationBody { get; set; }

    public string NotificationLink { get; set; }

    public string Description { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_ContactType LU_ContactType { get; set; }

    public virtual LU_PermitStatus LU_PermitStatus { get; set; }
  }
}
