﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.DocumentByte
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class DocumentByte : IUpdatableEntity
  {
    public int Id { get; set; }

    public byte[] Document { get; set; }

    public string MimeType { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.Document> Documents { get; set; }

    public virtual ICollection<DocumentJob> DocumentJobs { get; set; }

    public DocumentByte()
    {
      this.Documents = (ICollection<MDGov.MDE.Permitting.Model.Document>) new HashSet<MDGov.MDE.Permitting.Model.Document>();
      this.DocumentJobs = (ICollection<DocumentJob>) new HashSet<DocumentJob>();
    }
  }
}
