﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitsFailedToReportTwoPeriod
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitsFailedToReportTwoPeriod : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? RefId { get; set; }

    public int? ApplicationTypeId { get; set; }

    public int? Compliance_ContactId { get; set; }

    public int? ComplianceStatusId { get; set; }

    public int? PermitStatusId { get; set; }

    public int? PermitTypeId { get; set; }

    public int? ReportCodeId { get; set; }

    public int? WaterManagementStrategyAreaId { get; set; }

    public int? WaterTypeId { get; set; }

    public int? AquiferId { get; set; }

    public string ApplicantIdentification { get; set; }

    public int? RamsWanActid { get; set; }

    public string PermitNumber { get; set; }

    public string RevisionNumber { get; set; }

    public string PermitName { get; set; }

    public bool? HasNtwWarning { get; set; }

    public bool? RequiresAdvertising { get; set; }

    public string PWSID { get; set; }

    public bool? IsExemptFromFees { get; set; }

    public bool? HasWtrAuditWarning { get; set; }

    public string ProjectName { get; set; }

    public bool? AreAbandonedWellsPresent { get; set; }

    public bool? IsVoluntaryPermit { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public DateTime? EffectiveDate { get; set; }

    public DateTime? ExpirationDate { get; set; }

    public DateTime? AppropriationDate { get; set; }

    public DateTime? DateOut { get; set; }

    public bool? IsInRechargeEasement { get; set; }

    public bool? IsInTier2Watershed { get; set; }

    public bool? IsInWMStrategyArea { get; set; }

    public string HB935VerifiedBy { get; set; }

    public DateTime? HB935VerifiedDate { get; set; }

    public bool? HardRockSubsurface { get; set; }

    public bool? SbdnWarning { get; set; }

    public bool? AquiferTest { get; set; }

    public string Location { get; set; }

    public string SourceDescription { get; set; }

    public string UseDescription { get; set; }

    public bool? WaterQualityAnalysis { get; set; }

    public string Description { get; set; }

    public string Remarks { get; set; }

    public string City { get; set; }

    public bool? StateWide { get; set; }

    public string SubmittedBy { get; set; }

    public DateTime? SubmittedDate { get; set; }

    public string CountyOfficialSignOffBy { get; set; }

    public DateTime? CountyOfficialSignOffDate { get; set; }

    public bool? CountyOfficialSignOffYesNo { get; set; }

    public string FormCode { get; set; }

    public string ReportCode { get; set; }

    public bool? IsSubjectToEasement { get; set; }

    public bool? HaveNotifiedEasementHolder { get; set; }

    public string ADCXCoord { get; set; }

    public string ADCYCoord { get; set; }

    public string ADCPageLetterGrid { get; set; }

    public string CountyHydroGeoMap { get; set; }

    public string USGSTopoMap { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }
  }
}
