﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.DivisionChief
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class DivisionChief : IUpdatableEntity
  {
    public int Id { get; set; }

    public int ContactId { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<GeneratedLetter> GeneratedLetters { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.LU_Division> LU_Division { get; set; }

    public virtual ICollection<Supervisor> Supervisors { get; set; }

    public virtual Contact Contact { get; set; }

    public DivisionChief()
    {
      this.GeneratedLetters = (ICollection<GeneratedLetter>) new HashSet<GeneratedLetter>();
      this.LU_Division = (ICollection<MDGov.MDE.Permitting.Model.LU_Division>) new HashSet<MDGov.MDE.Permitting.Model.LU_Division>();
      this.Supervisors = (ICollection<Supervisor>) new HashSet<Supervisor>();
    }
  }
}
