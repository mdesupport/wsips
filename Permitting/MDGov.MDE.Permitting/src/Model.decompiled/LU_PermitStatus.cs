﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_PermitStatus
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_PermitStatus : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public int? PermitExternalStatusId { get; set; }

    public int? PermitCategoryId { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<CheckList> CheckLists { get; set; }

    public virtual ICollection<Comment> Comments { get; set; }

    public virtual ICollection<Document> Documents { get; set; }

    public virtual LU_PermitCategory LU_PermitCategory { get; set; }

    public virtual LU_PermitExternalStatus LU_PermitExternalStatus { get; set; }

    public virtual ICollection<PermitStatusState> PermitStatusStates { get; set; }

    public virtual ICollection<PermitStatusWorkflow> PermitStatusWorkflows { get; set; }

    public virtual ICollection<PermitStatusHistory> PermitStatusHistories { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.LU_PermitStatusCommunication> LU_PermitStatusCommunication { get; set; }

    public virtual ICollection<Permit> Permits { get; set; }

    public LU_PermitStatus()
    {
      this.CheckLists = (ICollection<CheckList>) new HashSet<CheckList>();
      this.Comments = (ICollection<Comment>) new HashSet<Comment>();
      this.Documents = (ICollection<Document>) new HashSet<Document>();
      this.PermitStatusStates = (ICollection<PermitStatusState>) new HashSet<PermitStatusState>();
      this.PermitStatusWorkflows = (ICollection<PermitStatusWorkflow>) new HashSet<PermitStatusWorkflow>();
      this.PermitStatusHistories = (ICollection<PermitStatusHistory>) new HashSet<PermitStatusHistory>();
      this.LU_PermitStatusCommunication = (ICollection<MDGov.MDE.Permitting.Model.LU_PermitStatusCommunication>) new HashSet<MDGov.MDE.Permitting.Model.LU_PermitStatusCommunication>();
      this.Permits = (ICollection<Permit>) new HashSet<Permit>();
    }
  }
}
