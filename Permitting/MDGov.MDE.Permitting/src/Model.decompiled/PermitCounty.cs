﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitCounty
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitCounty : IUpdatableEntity
  {
    public int Id { get; set; }

    public int CountyId { get; set; }

    public int PermitId { get; set; }

    public bool? RequiresSignOff { get; set; }

    public bool? CountySignOff { get; set; }

    public string CountySignOffExplanation { get; set; }

    public int? CountySignOffContactTitle { get; set; }

    public string CountySignOffContact { get; set; }

    public DateTime? CountySignOffDate { get; set; }

    public int? UsePercent { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_County LU_County { get; set; }

    public virtual Permit Permit { get; set; }
  }
}
