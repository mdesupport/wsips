﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_StateStream
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_StateStream : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public int? WatershedId { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    public string SecondaryName { get; set; }

    public int? Level { get; set; }

    public string McFlag { get; set; }

    public int? ReceivingTributaryId { get; set; }

    public int? RiverMile { get; set; }

    public int? VolumeNumber { get; set; }

    public int? SequenceNumber { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<LU_StateStream> LU_StateStream1 { get; set; }

    public virtual LU_StateStream LU_StateStream2 { get; set; }

    public virtual LU_Watershed LU_Watershed { get; set; }

    public virtual ICollection<PermitWaterDispersement> PermitWaterDispersements { get; set; }

    public virtual ICollection<PermitWithdrawalSurfacewaterDetail> PermitWithdrawalSurfacewaterDetails { get; set; }

    public LU_StateStream()
    {
      this.LU_StateStream1 = (ICollection<LU_StateStream>) new HashSet<LU_StateStream>();
      this.PermitWaterDispersements = (ICollection<PermitWaterDispersement>) new HashSet<PermitWaterDispersement>();
      this.PermitWithdrawalSurfacewaterDetails = (ICollection<PermitWithdrawalSurfacewaterDetail>) new HashSet<PermitWithdrawalSurfacewaterDetail>();
    }
  }
}
