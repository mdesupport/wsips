﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LetterTemplate
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class LetterTemplate : IUpdatableEntity
  {
    public int Id { get; set; }

    public string LetterText { get; set; }

    public string TemplateName { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<GeneratedLetter> GeneratedLetters { get; set; }

    public LetterTemplate()
    {
      this.GeneratedLetters = (ICollection<GeneratedLetter>) new HashSet<GeneratedLetter>();
    }
  }
}
