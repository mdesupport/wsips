﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_StandardCondition
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_StandardCondition : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public int? PermitTemplateId { get; set; }

    public int? StandardConditionTypeId { get; set; }

    public bool? RequiresSelfReporting { get; set; }

    public int? ConditionReportingPeriodID { get; set; }

    public int? OneTimeReportDays { get; set; }

    public bool? RequiresValidation { get; set; }

    public string DefaultComplianceReportingDueDate { get; set; }

    public int? PumpageReportTypeId { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_ConditionReportingPeriod LU_ConditionReportingPeriod { get; set; }

    public virtual LU_PermitTemplate LU_PermitTemplate { get; set; }

    public virtual LU_PumpageReportType LU_PumpageReportType { get; set; }

    public virtual LU_StandardConditionType LU_StandardConditionType { get; set; }
  }
}
