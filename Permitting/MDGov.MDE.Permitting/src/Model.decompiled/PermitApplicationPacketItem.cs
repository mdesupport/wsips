﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitApplicationPacketItem
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitApplicationPacketItem : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? ApplicationPacketItemId { get; set; }

    public int? PermitApplicationPacketId { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public DateTime? AcceptedDate { get; set; }

    public int? Sequence { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_ApplicationPacketItem LU_ApplicationPacketItem { get; set; }

    public virtual PermitApplicationPacket PermitApplicationPacket { get; set; }
  }
}
