﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.Contact
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class Contact : IUpdatableEntity
  {
    public int Id { get; set; }

    public int ContactTypeId { get; set; }

    public int? CountryId { get; set; }

    public int? CountyId { get; set; }

    public int? DivisionId { get; set; }

    public int? StateId { get; set; }

    public int? TitleId { get; set; }

    public int? UserId { get; set; }

    public string Salutation { get; set; }

    public string FirstName { get; set; }

    public string MiddleInitial { get; set; }

    public string LastName { get; set; }

    public string SecondaryName { get; set; }

    public string BusinessName { get; set; }

    public string Address1 { get; set; }

    public string Address2 { get; set; }

    public string City { get; set; }

    public string ZipCode { get; set; }

    public string Subdivision { get; set; }

    public bool IsBusiness { get; set; }

    public string Office { get; set; }

    public bool ApplicationNotification { get; set; }

    public bool EmailNotification { get; set; }

    public string SignatureFileName { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public int? SourceId { get; set; }

    public string SourceTable { get; set; }

    public virtual ICollection<AdministrativeSpecialist> AdministrativeSpecialists { get; set; }

    public virtual ICollection<ComplianceManager> ComplianceManagers { get; set; }

    public virtual ICollection<ComplianceStaff> ComplianceStaffs { get; set; }

    public virtual LU_ContactType LU_ContactType { get; set; }

    public virtual ICollection<CountyOfficial> CountyOfficials { get; set; }

    public virtual ICollection<DepartmentOfNaturalResource> DepartmentOfNaturalResources { get; set; }

    public virtual ICollection<Secretary> Secretaries { get; set; }

    public virtual LU_County LU_County { get; set; }

    public virtual LU_State LU_State { get; set; }

    public virtual ICollection<DivisionChief> DivisionChiefs { get; set; }

    public virtual ICollection<ProjectManager> ProjectManagers { get; set; }

    public virtual ICollection<Supervisor> Supervisors { get; set; }

    public virtual LU_Country LU_Country { get; set; }

    public virtual ICollection<ContactCommunicationMethod> ContactCommunicationMethods { get; set; }

    public virtual LU_Title LU_Title { get; set; }

    public virtual LU_Division LU_Division { get; set; }

    public virtual ICollection<PumpageReport> PumpageReports { get; set; }

    public virtual ICollection<PermitContact> PermitContacts { get; set; }

    public virtual ICollection<Permit> Permits { get; set; }

    public Contact()
    {
      this.AdministrativeSpecialists = (ICollection<AdministrativeSpecialist>) new HashSet<AdministrativeSpecialist>();
      this.ComplianceManagers = (ICollection<ComplianceManager>) new HashSet<ComplianceManager>();
      this.ComplianceStaffs = (ICollection<ComplianceStaff>) new HashSet<ComplianceStaff>();
      this.CountyOfficials = (ICollection<CountyOfficial>) new HashSet<CountyOfficial>();
      this.DepartmentOfNaturalResources = (ICollection<DepartmentOfNaturalResource>) new HashSet<DepartmentOfNaturalResource>();
      this.Secretaries = (ICollection<Secretary>) new HashSet<Secretary>();
      this.DivisionChiefs = (ICollection<DivisionChief>) new HashSet<DivisionChief>();
      this.ProjectManagers = (ICollection<ProjectManager>) new HashSet<ProjectManager>();
      this.Supervisors = (ICollection<Supervisor>) new HashSet<Supervisor>();
      this.ContactCommunicationMethods = (ICollection<ContactCommunicationMethod>) new HashSet<ContactCommunicationMethod>();
      this.PumpageReports = (ICollection<PumpageReport>) new HashSet<PumpageReport>();
      this.PermitContacts = (ICollection<PermitContact>) new HashSet<PermitContact>();
      this.Permits = (ICollection<Permit>) new HashSet<Permit>();
    }
  }
}
