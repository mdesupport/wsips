﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitSupplementalGroup
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitSupplementalGroup : IUpdatableEntity
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public int SupplementalGroupId { get; set; }

    public int SupplementalTypeId { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_SupplementalType LU_SupplementalType { get; set; }

    public virtual SupplementalGroup SupplementalGroup { get; set; }

    public virtual Permit Permit { get; set; }
  }
}
