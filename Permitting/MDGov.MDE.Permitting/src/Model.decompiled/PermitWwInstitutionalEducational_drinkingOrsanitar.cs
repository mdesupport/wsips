﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWwInstitutionalEducational_drinkingOrsanitary
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWwInstitutionalEducational_drinkingOrsanitary : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PermitWaterWithdrawalPurposeId { get; set; }

    public int? AverageGallonPerDayFromGroundWater { get; set; }

    public int? MaximumGallonPerDayFromGroundWater { get; set; }

    public int? AverageGallonPerDayFromSurfaceWater { get; set; }

    public int? MaximumGallonPerDayFromSurfaceWater { get; set; }

    public string EstimateDescription { get; set; }

    public int? EducationInstituteTypeId { get; set; }

    public int? NoOfStaff { get; set; }

    public int? NoOfStudents { get; set; }

    public bool? IsAthleticFieldIrrigation { get; set; }

    public int? NoOfIrrigatedAcres { get; set; }

    public bool? Chillers { get; set; }

    public int? NoOfStaffWorkingMorethan20hrsPerWeek { get; set; }

    public int? NoOfStaffWorkingLessthan20hrsPerWeek { get; set; }

    public int? NoOfStudentsAttendingMorethan20hrsPerWeek { get; set; }

    public int? NoOfStudentsAttendingLessthan20hrsPerWeek { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_EducationInstituteType LU_EducationInstituteType { get; set; }

    public virtual PermitWaterWithdrawalPurpose PermitWaterWithdrawalPurpose { get; set; }
  }
}
