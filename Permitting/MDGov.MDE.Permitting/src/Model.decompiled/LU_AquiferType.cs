﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_AquiferType
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_AquiferType : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.LU_Aquifer> LU_Aquifer { get; set; }

    public LU_AquiferType()
    {
      this.LU_Aquifer = (ICollection<MDGov.MDE.Permitting.Model.LU_Aquifer>) new HashSet<MDGov.MDE.Permitting.Model.LU_Aquifer>();
    }
  }
}
