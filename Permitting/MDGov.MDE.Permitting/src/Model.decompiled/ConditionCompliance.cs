﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.ConditionCompliance
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class ConditionCompliance : IUpdatableEntity
  {
    public int Id { get; set; }

    public int PermitConditionComplianceStatusId { get; set; }

    public int PermitConditionId { get; set; }

    public DateTime ComplianceReportingStartDate { get; set; }

    public DateTime ComplianceReportingEndDate { get; set; }

    public DateTime ComplianceReportingDueDate { get; set; }

    public double PermitAnnualAverageUsage { get; set; }

    public double AnnualAverageUsage { get; set; }

    public double AnnualAverageOverusePercentage { get; set; }

    public double MonthlyMaximumOverusePercentage { get; set; }

    public double? AnnualTotalUsage { get; set; }

    public double? ReportAverageUsage { get; set; }

    public double? ReportTotalUsage { get; set; }

    public int? HighMonth { get; set; }

    public double? HighMonthAvgGalPerDay { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<PumpageReport> PumpageReports { get; set; }

    public virtual LU_PermitConditionComplianceStatus LU_PermitConditionComplianceStatus { get; set; }

    public virtual PermitCondition PermitCondition { get; set; }

    public ConditionCompliance()
    {
      this.PumpageReports = (ICollection<PumpageReport>) new HashSet<PumpageReport>();
    }
  }
}
