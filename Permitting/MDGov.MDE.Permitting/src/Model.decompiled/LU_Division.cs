﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_Division
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_Division : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public int? CountyId { get; set; }

    public int? DivisionChiefId { get; set; }

    public int? StateId { get; set; }

    public string Name { get; set; }

    public string Address1 { get; set; }

    public string Address2 { get; set; }

    public string City { get; set; }

    public string ZipCode { get; set; }

    public string PrimaryContactNumber { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual DivisionChief DivisionChief { get; set; }

    public virtual LU_County LU_County { get; set; }

    public virtual LU_State LU_State { get; set; }

    public virtual ICollection<Contact> Contacts { get; set; }

    public LU_Division()
    {
      this.Contacts = (ICollection<Contact>) new HashSet<Contact>();
    }
  }
}
