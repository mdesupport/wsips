﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWwPoultry
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWwPoultry : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PoultryCoolingSystemId { get; set; }

    public int? PoultryId { get; set; }

    public int? PermitWaterWithdrawalPurposeId { get; set; }

    public int? NumberOfHouses { get; set; }

    public int? NumberOfFlocksPerYear { get; set; }

    public int? BirdsPerFlock { get; set; }

    public int? NoOfDaysPerYearFoggersUsed { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_PoultryCoolingSystem LU_PoultryCoolingSystem { get; set; }

    public virtual LU_PoultryType LU_PoultryType { get; set; }

    public virtual PermitWaterWithdrawalPurpose PermitWaterWithdrawalPurpose { get; set; }
  }
}
