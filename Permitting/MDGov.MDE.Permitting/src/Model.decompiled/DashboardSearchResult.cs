﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.DashboardSearchResult
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using System.Collections.Generic;
using System.Data.Spatial;

namespace MDGov.MDE.Permitting.Model
{
  public class DashboardSearchResult
  {
    public IEnumerable<int> PermitIds { get; set; }

    public DbGeometryWellKnownValue Feature { get; set; }

    public IEnumerable<DashboardPermit> DashboardPermits { get; set; }
  }
}
