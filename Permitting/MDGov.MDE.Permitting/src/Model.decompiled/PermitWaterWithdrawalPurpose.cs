﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWaterWithdrawalPurpose
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWaterWithdrawalPurpose : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PermitId { get; set; }

    public int? WaterWithdrawalPurposeId { get; set; }

    public int? UsePercent { get; set; }

    public Decimal? TotalIrrigatedAcres { get; set; }

    public int? Sequence { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_WaterWithdrawalPurpose LU_WaterWithdrawalPurpose { get; set; }

    public virtual Permit Permit { get; set; }

    public virtual ICollection<PermitWastewaterTreatmentDisposal> PermitWastewaterTreatmentDisposals { get; set; }

    public virtual ICollection<PermitWaterWithdrawalMisc> PermitWaterWithdrawalMiscs { get; set; }

    public virtual ICollection<WaterWithdrawalLocationPurpose> WaterWithdrawalLocationPurposes { get; set; }

    public virtual ICollection<PermitWwConstructionDewatering> PermitWwConstructionDewaterings { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.PermitWwInstitutionalReligious_DringkingOrSanitary> PermitWwInstitutionalReligious_DringkingOrSanitary { get; set; }

    public virtual ICollection<PermitWwWildlifePond> PermitWwWildlifePonds { get; set; }

    public virtual ICollection<PermitWwAquacultureOrAquarium> PermitWwAquacultureOrAquariums { get; set; }

    public virtual ICollection<PermitWwFarmPortableSupply> PermitWwFarmPortableSupplies { get; set; }

    public virtual ICollection<PermitWwFoodProcessing> PermitWwFoodProcessings { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.PermitWwInstitutionalEducational_drinkingOrsanitary> PermitWwInstitutionalEducational_drinkingOrsanitary { get; set; }

    public virtual ICollection<PermitWwPrivateWaterSupplier> PermitWwPrivateWaterSuppliers { get; set; }

    public virtual ICollection<PermitWwNonAgricultureIrrigation> PermitWwNonAgricultureIrrigations { get; set; }

    public virtual ICollection<PermitWwGolfCourseIrrigation> PermitWwGolfCourseIrrigations { get; set; }

    public virtual ICollection<PermitWwCommercialDrinkingOrSanitary> PermitWwCommercialDrinkingOrSanitaries { get; set; }

    public virtual ICollection<PermitWwPoultry> PermitWwPoultries { get; set; }

    public virtual ICollection<PermitWwCrop> PermitWwCrops { get; set; }

    public virtual ICollection<PermitWwHorticulture> PermitWwHorticultures { get; set; }

    public virtual ICollection<PermitWwLivestock> PermitWwLivestocks { get; set; }

    public virtual ICollection<PermitWwVegetable> PermitWwVegetables { get; set; }

    public virtual ICollection<PermitWwAgricultural> PermitWwAgriculturals { get; set; }

    public PermitWaterWithdrawalPurpose()
    {
      this.PermitWastewaterTreatmentDisposals = (ICollection<PermitWastewaterTreatmentDisposal>) new HashSet<PermitWastewaterTreatmentDisposal>();
      this.PermitWaterWithdrawalMiscs = (ICollection<PermitWaterWithdrawalMisc>) new HashSet<PermitWaterWithdrawalMisc>();
      this.WaterWithdrawalLocationPurposes = (ICollection<WaterWithdrawalLocationPurpose>) new HashSet<WaterWithdrawalLocationPurpose>();
      this.PermitWwConstructionDewaterings = (ICollection<PermitWwConstructionDewatering>) new HashSet<PermitWwConstructionDewatering>();
      this.PermitWwInstitutionalReligious_DringkingOrSanitary = (ICollection<MDGov.MDE.Permitting.Model.PermitWwInstitutionalReligious_DringkingOrSanitary>) new HashSet<MDGov.MDE.Permitting.Model.PermitWwInstitutionalReligious_DringkingOrSanitary>();
      this.PermitWwWildlifePonds = (ICollection<PermitWwWildlifePond>) new HashSet<PermitWwWildlifePond>();
      this.PermitWwAquacultureOrAquariums = (ICollection<PermitWwAquacultureOrAquarium>) new HashSet<PermitWwAquacultureOrAquarium>();
      this.PermitWwFarmPortableSupplies = (ICollection<PermitWwFarmPortableSupply>) new HashSet<PermitWwFarmPortableSupply>();
      this.PermitWwFoodProcessings = (ICollection<PermitWwFoodProcessing>) new HashSet<PermitWwFoodProcessing>();
      this.PermitWwInstitutionalEducational_drinkingOrsanitary = (ICollection<MDGov.MDE.Permitting.Model.PermitWwInstitutionalEducational_drinkingOrsanitary>) new HashSet<MDGov.MDE.Permitting.Model.PermitWwInstitutionalEducational_drinkingOrsanitary>();
      this.PermitWwPrivateWaterSuppliers = (ICollection<PermitWwPrivateWaterSupplier>) new HashSet<PermitWwPrivateWaterSupplier>();
      this.PermitWwNonAgricultureIrrigations = (ICollection<PermitWwNonAgricultureIrrigation>) new HashSet<PermitWwNonAgricultureIrrigation>();
      this.PermitWwGolfCourseIrrigations = (ICollection<PermitWwGolfCourseIrrigation>) new HashSet<PermitWwGolfCourseIrrigation>();
      this.PermitWwCommercialDrinkingOrSanitaries = (ICollection<PermitWwCommercialDrinkingOrSanitary>) new HashSet<PermitWwCommercialDrinkingOrSanitary>();
      this.PermitWwPoultries = (ICollection<PermitWwPoultry>) new HashSet<PermitWwPoultry>();
      this.PermitWwCrops = (ICollection<PermitWwCrop>) new HashSet<PermitWwCrop>();
      this.PermitWwHorticultures = (ICollection<PermitWwHorticulture>) new HashSet<PermitWwHorticulture>();
      this.PermitWwLivestocks = (ICollection<PermitWwLivestock>) new HashSet<PermitWwLivestock>();
      this.PermitWwVegetables = (ICollection<PermitWwVegetable>) new HashSet<PermitWwVegetable>();
      this.PermitWwAgriculturals = (ICollection<PermitWwAgricultural>) new HashSet<PermitWwAgricultural>();
    }
  }
}
