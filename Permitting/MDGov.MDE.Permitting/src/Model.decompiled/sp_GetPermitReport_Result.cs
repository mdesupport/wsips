﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.sp_GetPermitReport_Result
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

namespace MDGov.MDE.Permitting.Model
{
  public class sp_GetPermitReport_Result
  {
    public int ACTID { get; set; }

    public string PermitNumber { get; set; }

    public string Applicant { get; set; }

    public string DateReceived { get; set; }

    public string PermitStatus { get; set; }

    public string ProjectManager { get; set; }

    public string EffectiveDate { get; set; }

    public string ExpirationDate { get; set; }

    public long AllocatedAverageGPD { get; set; }

    public string ConfinedOrUnconfined { get; set; }

    public string Supervisor { get; set; }

    public string County { get; set; }

    public string StreetAddress { get; set; }

    public string Map { get; set; }

    public string Grid { get; set; }

    public string Parcel { get; set; }

    public string NorthGrid { get; set; }

    public string EastGrid { get; set; }

    public string ApplicationType { get; set; }

    public string UseType1 { get; set; }

    public int UseType1Percent { get; set; }

    public string UseType2 { get; set; }

    public int UseType2Percent { get; set; }

    public string UseType3 { get; set; }

    public int UseType3Percent { get; set; }

    public string UseType4 { get; set; }

    public int UseType4Percent { get; set; }

    public long ReqAllocGPDMax { get; set; }

    public string SurfaceOrGround { get; set; }

    public string AquiferCode { get; set; }

    public string AquiferName { get; set; }

    public string StreamCode { get; set; }

    public string StreamName { get; set; }

    public string BasinCode { get; set; }
  }
}
