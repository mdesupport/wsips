﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWithdrawalSurfacewaterDetail
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWithdrawalSurfacewaterDetail : IUpdatableEntity
  {
    public int Id { get; set; }

    public int SurfacewaterTypeId { get; set; }

    public int PermitWithdrawalSurfacewaterId { get; set; }

    public int? WatershedId { get; set; }

    public int? StateStreamId { get; set; }

    public string OtherStreamName { get; set; }

    public int? CountyId { get; set; }

    public string ExactLocationOfIntake { get; set; }

    public string Comment { get; set; }

    public string TaxMapNumber { get; set; }

    public string TaxMapBlockNumber { get; set; }

    public string TaxMapParcel { get; set; }

    public string TaxMapSubBlockNumber { get; set; }

    public string LotNumber { get; set; }

    public string Street { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string Zip { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_StateStream LU_StateStream { get; set; }

    public virtual LU_Watershed LU_Watershed { get; set; }

    public virtual LU_WaterWithdrawalType LU_WaterWithdrawalType { get; set; }

    public virtual PermitWithdrawalSurfacewater PermitWithdrawalSurfacewater { get; set; }
  }
}
