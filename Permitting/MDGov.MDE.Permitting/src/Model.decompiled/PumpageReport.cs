﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PumpageReport
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class PumpageReport : IUpdatableEntity
  {
    public int Id { get; set; }

    public string Name { get; set; }

    public int? ConditionComplianceId { get; set; }

    public int? ContactId { get; set; }

    public int? WaterWithdrawalEstimateId { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public string OtherWaterWithdrawalMethod { get; set; }

    public int? CropTypeId { get; set; }

    public string OtherCropDescription { get; set; }

    public string ReportYear { get; set; }

    public string SubmittedBy { get; set; }

    public string Phone { get; set; }

    public string ReportPeriod { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ConditionCompliance ConditionCompliance { get; set; }

    public virtual LU_CropType LU_CropType { get; set; }

    public virtual LU_WaterWithdrawalEstimate LU_WaterWithdrawalEstimate { get; set; }

    public virtual Contact Contact { get; set; }

    public virtual ICollection<PumpageReportDetail> PumpageReportDetails { get; set; }

    public PumpageReport()
    {
      this.PumpageReportDetails = (ICollection<PumpageReportDetail>) new HashSet<PumpageReportDetail>();
    }
  }
}
