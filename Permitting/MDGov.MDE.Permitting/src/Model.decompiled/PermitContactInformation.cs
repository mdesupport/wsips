﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitContactInformation
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitContactInformation : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? CountryId { get; set; }

    public int? StateId { get; set; }

    public bool IsBusiness { get; set; }

    public string Salutation { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string SecondaryName { get; set; }

    public string BusinessName { get; set; }

    public string MiddleInitial { get; set; }

    public string Address1 { get; set; }

    public string Address2 { get; set; }

    public string City { get; set; }

    public string ZipCode { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public int? SourceId { get; set; }

    public string SourceTable { get; set; }

    public int? ContactId { get; set; }

    public virtual LU_Country LU_Country { get; set; }

    public virtual LU_State LU_State { get; set; }

    public virtual ICollection<PermitContact> PermitContacts { get; set; }

    public PermitContactInformation()
    {
      this.PermitContacts = (ICollection<PermitContact>) new HashSet<PermitContact>();
    }
  }
}
