﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_Watershed
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Spatial;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_Watershed : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    [JsonIgnore]
    public DbGeometry Shape { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.LU_StateStream> LU_StateStream { get; set; }

    public virtual ICollection<PermitWithdrawalSurfacewaterDetail> PermitWithdrawalSurfacewaterDetails { get; set; }

    public virtual ICollection<PermitWithdrawalGroundwaterDetail> PermitWithdrawalGroundwaterDetails { get; set; }

    public LU_Watershed()
    {
      this.LU_StateStream = (ICollection<MDGov.MDE.Permitting.Model.LU_StateStream>) new HashSet<MDGov.MDE.Permitting.Model.LU_StateStream>();
      this.PermitWithdrawalSurfacewaterDetails = (ICollection<PermitWithdrawalSurfacewaterDetail>) new HashSet<PermitWithdrawalSurfacewaterDetail>();
      this.PermitWithdrawalGroundwaterDetails = (ICollection<PermitWithdrawalGroundwaterDetail>) new HashSet<PermitWithdrawalGroundwaterDetail>();
    }
  }
}
