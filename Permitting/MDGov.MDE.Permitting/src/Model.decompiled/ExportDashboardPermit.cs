﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.ExportDashboardPermit
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

namespace MDGov.MDE.Permitting.Model
{
  public class ExportDashboardPermit
  {
    public string PermitName { get; set; }

    public string ApplicantSalutation { get; set; }

    public string ApplicantFirstName { get; set; }

    public string ApplicantMiddleInitial { get; set; }

    public string ApplicantLastName { get; set; }

    public string ApplicantBusinessName { get; set; }

    public string DateReceived { get; set; }

    public string PermitStatus { get; set; }

    public string ProjectManagerSalutation { get; set; }

    public string ProjectManagerFirstName { get; set; }

    public string ProjectManagerMiddleInitial { get; set; }

    public string ProjectManagerLastName { get; set; }

    public string ProjectManagerBusinessName { get; set; }

    public string SupervisorSalutation { get; set; }

    public string SupervisorFirstName { get; set; }

    public string SupervisorMiddleInitial { get; set; }

    public string SupervisorLastName { get; set; }

    public string SupervisorBusinessName { get; set; }

    public int? ACTID { get; set; }

    public string County { get; set; }

    public string Map { get; set; }

    public string Grid { get; set; }

    public string Parcel { get; set; }

    public string NorthGrid { get; set; }

    public string EastGrid { get; set; }

    public string AquiferCode { get; set; }

    public string AquiferName { get; set; }

    public string StreamCode { get; set; }

    public string StreamName { get; set; }

    public string BasinCode { get; set; }

    public string PermitCategory { get; set; }

    public string SelectedPermitStatus { get; set; }

    public string Counties { get; set; }

    public string Aquifers { get; set; }

    public string WaterSource { get; set; }

    public string EffectiveDate { get; set; }

    public string ExpirationDate { get; set; }

    public string WithdrawlPurposes { get; set; }

    public bool AssociatedWithMe { get; set; }

    public string SearchText { get; set; }
  }
}
