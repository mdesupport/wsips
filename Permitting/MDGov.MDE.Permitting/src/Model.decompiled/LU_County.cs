﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_County
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_County : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public int? Enf_StateRegionId { get; set; }

    public int? Ntw_StateRegionId { get; set; }

    public int? StateId { get; set; }

    public int? Wa_Region { get; set; }

    public string Newspaper { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_StateRegion LU_StateRegion { get; set; }

    public virtual LU_State LU_State { get; set; }

    public virtual LU_StateRegion LU_StateRegion1 { get; set; }

    public virtual ICollection<PermitCounty> PermitCounties { get; set; }

    public virtual ICollection<MDGov.MDE.Permitting.Model.LU_Division> LU_Division { get; set; }

    public virtual ICollection<ProjectManagerCounty> ProjectManagerCounties { get; set; }

    public virtual ICollection<PermitParcel> PermitParcels { get; set; }

    public virtual ICollection<Contact> Contacts { get; set; }

    public LU_County()
    {
      this.PermitCounties = (ICollection<PermitCounty>) new HashSet<PermitCounty>();
      this.LU_Division = (ICollection<MDGov.MDE.Permitting.Model.LU_Division>) new HashSet<MDGov.MDE.Permitting.Model.LU_Division>();
      this.ProjectManagerCounties = (ICollection<ProjectManagerCounty>) new HashSet<ProjectManagerCounty>();
      this.PermitParcels = (ICollection<PermitParcel>) new HashSet<PermitParcel>();
      this.Contacts = (ICollection<Contact>) new HashSet<Contact>();
    }
  }
}
