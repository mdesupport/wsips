﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.DashboardFilterWrapper
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class DashboardFilterWrapper
  {
    public IEnumerable<DynamicFilter> Filters { get; set; }

    public bool FailedToReportTwoPeriods { get; set; }

    public DashboardSpatialFilters SpatialFilters { get; set; }
  }
}
