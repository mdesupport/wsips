﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitHearing
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitHearing : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PermitHearingStatusId { get; set; }

    public int? PermitId { get; set; }

    public int? StateId { get; set; }

    public DateTime? PermitHearingDateTime { get; set; }

    public string Address1 { get; set; }

    public string Address2 { get; set; }

    public string City { get; set; }

    public string ZipCode { get; set; }

    public string HearingOfficer { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_PermitHearingStatus LU_PermitHearingStatus { get; set; }

    public virtual LU_State LU_State { get; set; }

    public virtual Permit Permit { get; set; }
  }
}
