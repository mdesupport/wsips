﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWwGolfCourseIrrigation
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWwGolfCourseIrrigation : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? PermitWaterWithdrawalPurposeId { get; set; }

    public int? AverageGallonPerDayFromGroundWater { get; set; }

    public int? MaximumGallonPerDayFromGroundWater { get; set; }

    public int? AverageGallonPerDayFromSurfaceWater { get; set; }

    public int? MaximumGallonPerDayFromSurfaceWater { get; set; }

    public string EstimateDescription { get; set; }

    public Decimal? NoOfIrrigatedAcres_teesOrGreens { get; set; }

    public Decimal? NoOfIrrigatedAcres_fairways { get; set; }

    public string TypeOfGrass_tees_greens { get; set; }

    public string TypeOfGrass_fairways { get; set; }

    public int? GolfIrrigationSystemTypeId { get; set; }

    public int? IrrigationLayoutTypeId { get; set; }

    public bool? IsWellwaterPumpedIntoPond { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_GolfIrrigationSystemType LU_GolfIrrigationSystemType { get; set; }

    public virtual LU_IrrigationLayoutType LU_IrrigationLayoutType { get; set; }

    public virtual PermitWaterWithdrawalPurpose PermitWaterWithdrawalPurpose { get; set; }
  }
}
