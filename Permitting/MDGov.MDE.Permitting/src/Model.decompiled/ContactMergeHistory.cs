﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.ContactMergeHistory
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

namespace MDGov.MDE.Permitting.Model
{
  public class ContactMergeHistory
  {
    public int Id { get; set; }

    public string TableName { get; set; }

    public int PrimaryId { get; set; }

    public int OldContactId { get; set; }

    public int NewContactId { get; set; }
  }
}
