﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.FeatureTypes
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

namespace MDGov.MDE.Permitting.Model
{
  public enum FeatureTypes
  {
    RechargeEasement,
    WmStrategyArea,
    Watershed,
  }
}
