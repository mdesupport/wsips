﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWithdrawalSurfacewater
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWithdrawalSurfacewater : IUpdatableEntity
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public long AvgGalPerDay { get; set; }

    public long MaxGalPerDay { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual Permit Permit { get; set; }

    public virtual ICollection<PermitWithdrawalSurfacewaterDetail> PermitWithdrawalSurfacewaterDetails { get; set; }

    public PermitWithdrawalSurfacewater()
    {
      this.PermitWithdrawalSurfacewaterDetails = (ICollection<PermitWithdrawalSurfacewaterDetail>) new HashSet<PermitWithdrawalSurfacewaterDetail>();
    }
  }
}
