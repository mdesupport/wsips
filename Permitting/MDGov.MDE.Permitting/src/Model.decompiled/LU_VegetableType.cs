﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.LU_VegetableType
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class LU_VegetableType : IUpdatableEntity, ILookup
  {
    public int Id { get; set; }

    public string Key { get; set; }

    public string Description { get; set; }

    public int? Sequence { get; set; }

    public bool Active { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual ICollection<PermitWwVegetable> PermitWwVegetables { get; set; }

    public LU_VegetableType()
    {
      this.PermitWwVegetables = (ICollection<PermitWwVegetable>) new HashSet<PermitWwVegetable>();
    }
  }
}
