﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.ContactCommunicationMethod
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class ContactCommunicationMethod : IUpdatableEntity
  {
    public int Id { get; set; }

    public int CommunicationMethodId { get; set; }

    public int ContactId { get; set; }

    public bool? IsPrimaryEmail { get; set; }

    public bool? IsPrimaryPhone { get; set; }

    public string CommunicationValue { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public int? SourceId { get; set; }

    public string SourceTable { get; set; }

    public virtual Contact Contact { get; set; }

    public virtual LU_CommunicationMethod LU_CommunicationMethod { get; set; }
  }
}
