﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.StatusChange
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using System;

namespace MDGov.MDE.Permitting.Model
{
  public class StatusChange
  {
    public int? PreviousPermitStatusId { get; set; }

    public int PermitStatusId { get; set; }

    public string ChangedBy { get; set; }

    public DateTime ChangedDate { get; set; }
  }
}
