﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.sp_GetWaterWithdrawalLocation_Result
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using System;

namespace MDGov.MDE.Permitting.Model
{
  public class sp_GetWaterWithdrawalLocation_Result
  {
    public int Id { get; set; }

    public int? PermitId { get; set; }

    public double? WithdrawalLocationX { get; set; }

    public double? WithdrawalLocationY { get; set; }

    public int? SpatialReferenceId { get; set; }

    public int? WaterWithdrawalSourceId { get; set; }

    public int? WithdrawalTypeId { get; set; }

    public int? StateStreamId { get; set; }

    public string OtherStreamName { get; set; }

    public string ExactLocationOfIntake { get; set; }

    public bool? IsExistingWell { get; set; }

    public Decimal? WellDepth { get; set; }

    public Decimal? WellDiameter { get; set; }

    public string WellTagNo { get; set; }

    public string Comments { get; set; }

    public string AssociatedPermitWaterWithdrawalPurposes { get; set; }

    public string LastModifiedBy { get; set; }
  }
}
