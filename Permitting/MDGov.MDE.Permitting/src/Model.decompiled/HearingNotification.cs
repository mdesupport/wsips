﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.HearingNotification
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class HearingNotification : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? HearingNotificationTypeId { get; set; }

    public string Subject { get; set; }

    public string Message { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public bool Active { get; set; }

    public bool? PublicHearingRequested { get; set; }

    public DateTime? ExtendUntillDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public bool? IsExtended { get; set; }

    public virtual ICollection<PermitHearingNotification> PermitHearingNotifications { get; set; }

    public virtual LU_HearingNotificationType LU_HearingNotificationType { get; set; }

    public HearingNotification()
    {
      this.PermitHearingNotifications = (ICollection<PermitHearingNotification>) new HashSet<PermitHearingNotification>();
    }
  }
}
