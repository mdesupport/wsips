﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWwCrop
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Downloads\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWwCrop : IUpdatableEntity
  {
    public int Id { get; set; }

    public int? CropId { get; set; }

    public int? CropYieldUnitId { get; set; }

    public int? IrrigationSystemTypeId { get; set; }

    public string IrrigationSystemDescription { get; set; }

    public int? PermitWaterWithdrawalPurposeId { get; set; }

    public Decimal? IrrigatedAcres { get; set; }

    public int? CropYieldGoal { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public virtual LU_CropType LU_CropType { get; set; }

    public virtual LU_CropYieldUnit LU_CropYieldUnit { get; set; }

    public virtual LU_IrrigationSystemType LU_IrrigationSystemType { get; set; }

    public virtual PermitWaterWithdrawalPurpose PermitWaterWithdrawalPurpose { get; set; }
  }
}
