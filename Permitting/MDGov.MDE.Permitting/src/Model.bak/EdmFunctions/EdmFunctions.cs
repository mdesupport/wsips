﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.EdmFunctions
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Model.dll

using System;
using System.Data.Objects.DataClasses;

namespace MDGov.MDE.Permitting.Model
{
  public static class EdmFunctions
  {
    [EdmFunction("Model", "ToInt32")]
    public static int ToInt32(string s)
    {
      throw new NotSupportedException("This function is only supported for Linq to Entities queries.");
    }
  }
}
