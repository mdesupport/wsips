﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitLocation
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Model.dll

namespace MDGov.MDE.Permitting.Model
{
  public class PermitLocation
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public double WithdrawalLocationX { get; set; }

    public double WithdrawalLocationY { get; set; }

    public int SpatialReferenceId { get; set; }

    public string LastModifiedBy { get; set; }
  }
}
