//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDGov.MDE.Permitting.Model
{
    using System;
    using System.Collections.Generic;
    using MDGov.MDE.Common.Model;
    using Newtonsoft.Json;
    
    public partial class LU_State : IUpdatableEntity, ILookup
    {
        public LU_State()
        {
            this.LU_County = new HashSet<LU_County>();
            this.LU_Division = new HashSet<LU_Division>();
            this.LU_ZipCode = new HashSet<LU_ZipCode>();
            this.PermitHearings = new HashSet<PermitHearing>();
            this.Contacts = new HashSet<Contact>();
            this.PermitContactInformations = new HashSet<PermitContactInformation>();
        }
    
        public int Id { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
        public Nullable<int> Sequence { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public System.DateTime LastModifiedDate { get; set; }
    
        public virtual ICollection<LU_County> LU_County { get; set; }
        public virtual ICollection<LU_Division> LU_Division { get; set; }
        public virtual ICollection<LU_ZipCode> LU_ZipCode { get; set; }
        public virtual ICollection<PermitHearing> PermitHearings { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<PermitContactInformation> PermitContactInformations { get; set; }
    }
}
