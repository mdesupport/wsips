﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.PermitWaterWithdrawalLocation
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Model.dll

using MDGov.MDE.Common.Model;
using System;
using System.Collections.Generic;

namespace MDGov.MDE.Permitting.Model
{
  public class PermitWaterWithdrawalLocation : IUpdatableEntity
  {
    public int Id { get; set; }

    public int PermitId { get; set; }

    public double WithdrawalLocationX { get; set; }

    public double WithdrawalLocationY { get; set; }

    public int SpatialReferenceId { get; set; }

    public int WithdrawalSourceId { get; set; }

    public int WithdrawalTypeId { get; set; }

    public int? StateStreamId { get; set; }

    public string OtherStreamName { get; set; }

    public string IntakeLocation { get; set; }

    public bool? IsExistingWell { get; set; }

    public Decimal? WellDepth { get; set; }

    public Decimal? WellDiameter { get; set; }

    public string WellTagNo { get; set; }

    public string Comments { get; set; }

    public IEnumerable<int> SelectedPermitWaterWithdrawalPurposes { get; set; }

    public string CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string LastModifiedBy { get; set; }

    public DateTime LastModifiedDate { get; set; }
  }
}
