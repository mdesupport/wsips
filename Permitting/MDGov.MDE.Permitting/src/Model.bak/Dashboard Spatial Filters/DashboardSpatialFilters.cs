﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.DashboardSpatialFilters
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Model.dll

using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace MDGov.MDE.Permitting.Model
{
  public class DashboardSpatialFilters
  {
    public MapExtentFilter MapExtentFilter { get; set; }

    public PermitDistanceFilter PermitDistanceFilter { get; set; }

    public PermitFeatureFilter PermitFeatureFilter { get; set; }

    public IQueryable<Permit> Apply(IQueryable<Permit> model)
    {
      DbGeometry geom = (DbGeometry) null;
      foreach (PropertyInfo propertyInfo in ((IEnumerable<PropertyInfo>) this.GetType().GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>) (prop => prop.PropertyType.GetInterface(typeof (IDashboardSpatialFilter).Name, false) != (Type) null)))
      {
        object obj = propertyInfo.GetValue((object) this);
        if (obj != null)
        {
          if (geom == null)
          {
            geom = (obj as IDashboardSpatialFilter).GetGeometry();
          }
          else
          {
            DbGeometry geometry = (obj as IDashboardSpatialFilter).GetGeometry();
            if (geometry != null)
              geom = geom.Union(geometry);
          }
        }
      }
      if (geom == null && this.MapExtentFilter != null)
        geom = this.MapExtentFilter.GetGeometry();
      if (geom != null)
        model = model.Where<Permit>((Expression<Func<Permit, bool>>) (t => t.PermitParcels.Any<PermitParcel>((Func<PermitParcel, bool>) (pp => pp.Shape.Intersects(geom)))));
      return model;
    }
  }
}
