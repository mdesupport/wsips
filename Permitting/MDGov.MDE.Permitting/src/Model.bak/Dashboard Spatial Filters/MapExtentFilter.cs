﻿// Decompiled with JetBrains decompiler
// Type: MDGov.MDE.Permitting.Model.MapExtentFilter
// Assembly: MDGov.MDE.Permitting.Model, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E58EC4AF-49BC-4969-BCFA-DB4C0E789A72
// Assembly location: C:\Users\jfinlay\Documents\Visual Studio 2013\Projects\WSIPS_deployment_backup\WSIPS\Services\Permitting\bin\MDGov.MDE.Permitting.Model.dll

using System;
using System.Data.Spatial;

namespace MDGov.MDE.Permitting.Model
{
  public class MapExtentFilter
  {
    private DbGeometry _geometry;

    public Decimal XMin { get; set; }

    public Decimal YMin { get; set; }

    public Decimal XMax { get; set; }

    public Decimal YMax { get; set; }

    public int SRID { get; set; }

    public DbGeometry GetGeometry()
    {
      if (this._geometry == null)
        this._geometry = DbGeometry.PolygonFromText(string.Format("POLYGON(({0} {1}, {2} {1}, {2} {3}, {0} {3}, {0} {1}))", (object) this.XMin, (object) this.YMin, (object) this.XMax, (object) this.YMax), this.SRID);
      return this._geometry;
    }
  }
}
