//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDGov.MDE.Permitting.Model
{
    using System;
    using System.Collections.Generic;
    using MDGov.MDE.Common.Model;
    using Newtonsoft.Json;
    
    public partial class PermitWwInstitutionalEducational_drinkingOrsanitary : IUpdatableEntity
    {
        public int Id { get; set; }
        public Nullable<int> PermitWaterWithdrawalPurposeId { get; set; }
        public Nullable<int> AverageGallonPerDayFromGroundWater { get; set; }
        public Nullable<int> MaximumGallonPerDayFromGroundWater { get; set; }
        public Nullable<int> AverageGallonPerDayFromSurfaceWater { get; set; }
        public Nullable<int> MaximumGallonPerDayFromSurfaceWater { get; set; }
        public string EstimateDescription { get; set; }
        public Nullable<int> EducationInstituteTypeId { get; set; }
        public Nullable<int> NoOfStaff { get; set; }
        public Nullable<int> NoOfStudents { get; set; }
        public Nullable<bool> IsAthleticFieldIrrigation { get; set; }
        public Nullable<int> NoOfIrrigatedAcres { get; set; }
        public Nullable<bool> Chillers { get; set; }
        public Nullable<int> NoOfStaffWorkingMorethan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStaffWorkingLessthan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStudentsAttendingMorethan20hrsPerWeek { get; set; }
        public Nullable<int> NoOfStudentsAttendingLessthan20hrsPerWeek { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public System.DateTime LastModifiedDate { get; set; }
    
        public virtual LU_EducationInstituteType LU_EducationInstituteType { get; set; }
        public virtual PermitWaterWithdrawalPurpose PermitWaterWithdrawalPurpose { get; set; }
    }
}
